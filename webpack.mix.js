let mix = require('laravel-mix');
let Clean = require('clean-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let cleanOptions = {
    root:    __dirname + '/public',
    verbose: true,
    dry:     false,
};

if (!mix.inProduction()) {
    mix.webpackConfig({
            devtool: 'source-map',
        })
        .sourceMaps();
} else {
    mix.options({
        uglify: {
            uglifyOptions: {
                compress: {
                    warnings: false,
                    drop_console: true,
                }
            }
        },
    });
}

mix
    .webpackConfig({
        plugins: [
            new Clean([
                'assets-admin',
                'css',
                'js',
                'fonts',
                'images',
                'img',
            ], cleanOptions)
        ],
    })
    .autoload({
        jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"],
        toastr: ['toastr'],
    })
    .copyDirectory('resources/assets/admin/img', 'public/img')
    .copyDirectory('resources/assets/images', 'public/img')
    .copyDirectory('resources/assets/media', 'public/media')
    // Admin
    .sass("resources/assets/admin/sass/app.scss", 'public/assets-admin/css/app.css')
    .js([
        "node_modules/jquery/dist/jquery.min.js",
        "node_modules/popper.js/dist/popper.min.js",
        "node_modules/tooltip.js/dist/tooltip.min.js",
        "node_modules/bootstrap/dist/js/bootstrap.bundle.min.js",
        "resources/assets/admin/js/overrides.js",
        "node_modules/bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
        "node_modules/select2/dist/js/select2.full.min.js",
        "node_modules/select2/dist/js/i18n/ru.js",
        "node_modules/toastr/build/toastr.min.js",
        "node_modules/jquery.nicescroll/dist/jquery.nicescroll.min.js",
        "resources/assets/admin/modules/scroll-up-bar/dist/scroll-up-bar.min.js",

        "node_modules/codemirror/lib/codemirror.js",
        "node_modules/bs4-summernote/dist/summernote-lite.js",
        "node_modules/bs4-summernote/dist/lang/summernote-ru-RU.js",
        "node_modules/jquery-ui-dist/jquery-ui.min.js",

        "resources/assets/admin/js/sa-functions.js",

        "node_modules/chart.js/dist/Chart.min.js",
        "node_modules/sweetalert/dist/sweetalert.min.js",

        "node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js",
        "node_modules/datatables.net-select/js/dataTables.select.min.js",

        "resources/assets/admin/js/scripts.js",
        "resources/assets/admin/js/custom.js",
    ], 'public/assets-admin/js/app.js')

    // Laravel
    .js('resources/assets/laravel/js/app.js', 'public/js/laravel.js')
    .sass('resources/assets/laravel/sass/app.scss', 'public/css/laravel.css')

    // Front
    .js('resources/assets/js/app.js', 'public/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css')
    .version();
