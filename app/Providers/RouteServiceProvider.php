<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $translationRoutesUri = [
            'translations/view/{groupKey?}',
            'translations/{groupKey?}',
            'translations/add/{groupKey}',
            'translations/edit/{groupKey}',
            'translations/groups/add',
            'translations/delete/{groupKey}/{translationKey}',
            'translations/import',
            'translations/find',
            'translations/locales/add',
            'translations/locales/remove',
            'translations/publish/{groupKey}',
        ];
        $routes = [];
        foreach (Route::getRoutes() as $route) {
            /* @var $route \Illuminate\Routing\Route */
            if (!in_array($route->uri, $translationRoutesUri)) {
                $routes[] = $route;
            }
        }

        Route::setRoutes(new \Illuminate\Routing\RouteCollection);
        foreach ($routes as $route) {
            /* @var $route \Illuminate\Routing\Route */
            Route::getRoutes()->add($route);
        }

        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
