<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(191);

        // Disable translations if package "barryvdh/laravel-translation-manager" not installed
        $translations = \Module::find('Translations');
        /* @var $translations \Nwidart\Modules\Laravel\Module */
        if ($translations) {
            if (($enabled = packageExist('barryvdh/laravel-translation-manager')) != $translations->enabled()) {
                if ($enabled) {
                    $translations->enable();
                } else {
                    $translations->disable();
                }
                $translations->setActive((int)$enabled);
            }
        }

        // Autoload configs from modules
        $modules = Module::allEnabled();
        foreach ($modules as $module) {
            /* @var $module \Nwidart\Modules\Laravel\Module */
            $path = __DIR__.'/../../modules/' . $module->getName() . '/Config/config.php';
            if (file_exists($path)) {
                $this->publishes([
                    $path => config_path($module->getLowerName() . '.php'),
                ], 'config');
                $this->mergeConfigFrom(
                    $path, $module->getLowerName()
                );
                if ($name = config($module->getLowerName() . '.routes.adminPrefix')) {
                    $this->mergeConfigFrom(
                        $path, $name
                    );
                }

            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path().'/Helpers/*.php') as $filename){
            require_once($filename);
        }

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
    }
}
