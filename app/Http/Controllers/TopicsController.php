<?php

namespace App\Http\Controllers;

use Modules\Topics\Entities\Topic;
use Modules\Users\Entities\User;

class TopicsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        if ($user = \Auth::user()) {
            switch ($user->getStatus()) {
                case 's':
                    return app('App\Http\Controllers\StudentController')->topics();
                    break;
                case 't':
                    return app('App\Http\Controllers\TeacherController')->topics();
                    break;
                default:
                    abort(404);
                    break;
            }
        } else {
            return view('landing');
        }
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function find()
    {
        if (request()->ajax() && (($status = ($user = \Auth::user())->getStatus()) === 's' || $status === 't')) {
            $data = getRequest();

            if (isset($data['tag'])) {
                $topics = Topic::where('tag_id', $data['tag']);
                if ($status === 't') {
                    if (isset($data['hideSelected']) && $data['hideSelected']) {
                        $topics->where('user_id', '!=', $user->id)
                            ->where('users', 'not like', '%"' . $user->id . '":true%');
                    } elseif (isset($data['onlySelected']) && $data['onlySelected']) {
                        $topics->where(function($query) use($user) {
                            $query->where('user_id', '=', $user->id)
                                ->orWhere('users', 'like', '%"' . $user->id . '":true%');
                        });
                    }
                }

                $topics = $topics->paginate($this->usersPerPage);
                $arrTopics = [];
                foreach ($topics->getCollection() as $topic) {
                    /* @var $topic Topic */
                    $arrTopics[$topic->id] = [
                        'tag' => $topic->tag_id,
                        'name' => $topic->name,
                        'image' => url($topic->image),
                        'difficulty' => $topic->difficulty,
                        ($topic->type === Topic::TYPE_LINK ? 'link' : 'content') => $topic->content,
                    ];
                    if ($status === 't') {
                        $arrTopics[$topic->id]['author'] = $user->id === $topic->user_id;
                        $arrTopics[$topic->id]['checked'] = array_key_exists($user->id, $topic->users);
                    }
                }

                return response()->json([
                    'current' => $topics->currentPage(),
                    'last' => $topics->lastPage(),
                    'topics' => $arrTopics,
                ]);
            }
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function teachers()
    {
        if (request()->ajax() && ($status = ($user = \Auth::user())->getStatus()) === 's' && ($topic = request()->get('topic', false))) {
            $topic = Topic::find($topic);
            $userIds = array_merge(array_keys($topic->users), [$topic->user_id]);
            $users = User::where('active', true)
                ->whereIn('id', array_merge(array_keys($topic->users), [$topic->user_id]))
                ->whereHas('user_add_values', function($query){
                    $query->whereHas('user_addon', function($query){
                        $query->where('alias', 'status');
                    })->where(function($query){
                        $query->where('value', 'like', '%t%');
                    });
                })->paginate($this->usersPerPage);

            return response()->json([
                'current' => $users->currentPage(),
                'last' => $users->lastPage(),
                'users' => $users->getCollection()->toArray(),
                'userIds' => $userIds,
            ]);
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function check()
    {
        if (request()->ajax() && ($status = ($user = \Auth::user())->getStatus()) === 't') {
            $data = getRequest();
            $result = [
                'error' => 'Status not changed.',
            ];
            if (isset($data['topic']) && isset($data['checked'])) {
                $topic = Topic::find($data['topic']);
                if ($topic->user_id != $user->id) {
                    $users = $topic->users;
                    if ($data['checked']) {
                        if (!array_key_exists($user->id, $users)) {
                            $users[$user->id] = true;
                        }
                        $result = ['checked' => true];
                    } else {
                        if (array_key_exists($user->id, $users)) {
                            unset($users[$user->id]);
                        }
                        $result = ['checked' => false];
                    }
                    $topic->users = $users;
                    $topic->save();
                }
            }

            $result['count'] = Topic::where('user_id', $user->id)
                ->orWhere('users', 'like', '%"1":true%')->count('id');
            return response()->json($result);
        }

        abort(404);
    }
}
