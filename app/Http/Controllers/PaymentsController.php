<?php

namespace App\Http\Controllers;

use Modules\Payments\Entities\PaymentsHistory;
use Modules\Payments\Entities\PaymentsInfo;

class PaymentsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function student(){
        $user = \Auth::user();
        $payments = PaymentsInfo::where([
            'user_id' => $user->id,
            'paid' => true,
        ])->orderBy('from', 'ASC')->get();
        $allPayedTime = 0;
        $minutes = null;
        $periodEnd = null;
        $now = new \DateTime();
        if (!empty($user->getActivePayment()->periods)) {
            $activePeriods = $user->getActivePayment()->periods->where('to', '>', new \DateTime());
            if ($activePeriods->count()) {
                $periodEnd = $activePeriods->max('to');
                foreach ($activePeriods as $period) {
                    if ($now < $period->to) {
                        $allPayedTime += $period->available_time;
                    }
                }
                $minutes = (int)ceil($allPayedTime / 60);
            }
        }

        return view('payment.student', [
            'payments' => $payments,
            'minutes' => $minutes,
            'periodEnd' => $periodEnd,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function teacher(){
        $user = \Auth::user();
        $payments = PaymentsHistory::where([
            'user_id' => $user->id,
            'paid' => true,
        ])->orderBy('date', 'ASC')->get();
        return view('payment.teacher', [
            'payments' => $payments,
            'balance' => $user->addon('tc_balance') ?: 0,
        ]);
    }

    public function paymentResult()
    {
        $paymentId = request('payment-id');
        if (!$paymentId) {
            return redirect()->route('payments');
        }
        $order = PaymentsInfo::where([
            'id' => $paymentId,
            'user_id' => optional($user = \Auth::getUser())->id,
        ])->first();
        /* @var $order PaymentsInfo */
        if (!$order) {
            abort(404);
        }

        $view = 'payment.' . ($order->paid ? 'success' : 'error');

        $tariffInfo = $order->tariff_info;
        $tariffInfo['currency'] = $order->currency;
        if (isset($tariffInfo['simple-pay-result'])) {
            unset($tariffInfo['simple-pay-result']);
        }

        return view($view, [
            'info' => $tariffInfo,
        ]);
    }
}
