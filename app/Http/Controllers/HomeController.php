<?php

namespace App\Http\Controllers;

use Modules\Reviews\Entities\Review;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function home()
    {
        if ($user = \Auth::user()) {
            switch ($user->getStatus()) {
                case 's':
                    return app('App\Http\Controllers\StudentController')->index();
                    break;
                case 't':
                    return app('App\Http\Controllers\TeacherController')->index();
                    break;
                default:
                    return redirect()->route('user.account');
                    break;
            }
        } else {
            $reviews = Review::where([
                'active' => true,
                'lang' => app()->getLocale(),
            ])->orderByDesc('weight')->limit($this->reviewsCount)->get();
            return view('landing', [
                'reviews' => $reviews,
            ]);
        }
    }

    public function referral($token)
    {
        if (\Auth::user()) {
            abort(404);
        }

        $referal = explode('/', $token);
        $referal = end($referal);
        $refAddon = UserAddon::where('alias', 'referral_token')->first();
        if ($refAddon) {
            $referal = UserAddValue::where([
                'addon_id' => $refAddon->id,
                'value' => $referal,
            ])->first();
            if ($referal) {
                return redirect()->route('register')->withInput(['cam-ref-link' => $token]);
            }
        }
    }
}
