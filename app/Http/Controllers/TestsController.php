<?php

namespace App\Http\Controllers;

use Modules\Tests\Entities\Test;
use Modules\Tests\Entities\TestQuestion;

class TestsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $user = \Auth::user();
        $tests = Test::where('active', true)->whereHas('questions', function($query){
            $query->where('active', true);
        })->get();

        if ($tests->count()) {
            $results = $user->addon('st_tests_results');
            $getResultMsg = function($count, $errors) {
                return $count . ' ' . numEnding($count, [
                    ___('test-lang-level.question.one'),
                    ___('test-lang-level.question.two'),
                    ___('test-lang-level.question.five'),
                ])
                . '. ' . ($errors === 0
                    ? ___('test-lang-level.error.none')
                    : $errors . ' ' .  numEnding($errors, [
                        ___('test-lang-level.error.one'),
                        ___('test-lang-level.error.two'),
                        ___('test-lang-level.error.five'),
                    ]));
            };
            $getResultType = function($percents) {
                return $percents <= 50 ? 2 : (($percents <= 85 && $percents > 50) ? 1 : 0);
            };

            if (request()->isMethod('POST') && request()->ajax()) {
                $result = null;
                $testAnswers = request()->get('test', null);
                if ($testAnswers) {
                    $result = [];
                    foreach ($testAnswers as $test => $questions) {
                        $test = $tests->find($test);
                        /* @var $test Test */
                        if ($test) {
                            $result[$test->id] = [];
                            $checkAnswers = function($answers) use(&$test, &$result) {
                                foreach ($answers as $id => $answer) {
                                    $question = $test->questions->find($id);
                                    /* @var $question TestQuestion */
                                    if ($question) {
                                        $result[$test->id][$id] = $question->answers['correct_id'] == $answer;
                                    }
                                }
                            };

                            $checkAnswers($questions);
                            if (isset($results[$test->id]['questions'])) {
                                $checkAnswers($results[$test->id]['questions']);
                            }
                        }
                    }
                }
                if (is_array($result)) {
                    foreach ($result as $test => $answers) {
                        $test = $tests->find($test);
                        $countQuestions = $test->all_questions ? $test->questions->count() : $test->display_count;
                        $countErrors = 0;
                        if (count($answers) === $countQuestions) {
                            foreach ($answers as $answer) {
                                if (!$answer) {
                                    $countErrors++;
                                }
                            }
                            $percents = round((($countQuestions - $countErrors) / $countQuestions) * 100);
                            $result[$test->id] = [
                                'percents' => $percents,
                                'resultType' => $getResultType($percents),
                                'text' => $getResultMsg($countQuestions, $countErrors)
                            ];
                            $results[$test->id] = [
                                'date' => (new \DateTime())->format('d.m.Y H:i:s'),
                                'count' => $countQuestions,
                                'errors' => $countErrors,
                                'percents' => round((($countQuestions - $countErrors) / $countQuestions) * 100),
                            ];
                            \Session::put('test-' . $test->id . '-complete', new \DateTime());
                        } else {
                            unset($result[$test->id]);
                        }
                    }
                    $user->setAddon('st_tests_results', $results);
                }

                return response()->json($result);
            } else {
                $compiled = [];

                foreach($tests as $test) {
                    $exclude = isset($results[$test->id]['questions']) ? array_keys($results[$test->id]['questions']) : [];
                    $questions = $test->questions->where('active', true)->whereNotIn('id', $exclude)->values();
                    if ($test->random) {
                        $questions = $questions->shuffle();
                    }

                    $countQuestions = $test->all_questions ? $test->questions->count() : $test->display_count;
                    $countAnsweredQuestions = isset($results[$test->id]['questions']) ? count($results[$test->id]['questions']) : 0;

                    $compiled[$test->id] = [
                        'answeredQuestions' => $countAnsweredQuestions,
                        'countQuestions' => $countQuestions,
                        'questions' => [],
                    ];

                    if (
                        isset($results[$test->id]['count'])
                        && isset($results[$test->id]['errors'])
                        && isset($results[$test->id]['percents'])
                        && ($completeDate = \Session::get('test-' . $test->id . '-complete', null)) instanceof \DateTime
                        && (((time() - $completeDate->getTimestamp()) / 60 / 60 / 24) < 7)
                    ) {
                        $compiled[$test->id]['result'] = $getResultMsg($results[$test->id]['count'], $results[$test->id]['errors']);
                        $compiled[$test->id]['percents'] = $results[$test->id]['percents'];
                        $compiled[$test->id]['resultType'] = $getResultType($results[$test->id]['percents']);
                    }

                    for($i = 0; $i < ($countQuestions - $countAnsweredQuestions); $i++) {
                        $answers = isset($questions[$i]->answers['answers']) ? $questions[$i]->answers['answers'] : [];
                        $compiled[$test->id]['questions'][] = [
                            'id' => $questions[$i]->id,
                            'question' => $questions[$i]->question,
                            'answers' => shuffleWithKeys($answers),
                        ];
                    }
                }

                return view('tests', [
                    'tests' => $tests,
                    'compiled' => $compiled,
                ]);
            }
        }

        abort(404);
    }

    public function saveResults()
    {
        if (request()->ajax()) {
            $result = false;
            try {
                $user = \Auth::user();
                $results = $user->addon('st_tests_results');
                $testAnswers = request()->get('test', null);
                if (empty($results)) {
                    $results = [];
                }
                foreach ($testAnswers as $test => $questions) {
                    if (!isset($results[$test]['questions'])) {
                        $results[$test] = [
                            'questions' => $questions,
                        ];
                    } else {
                        foreach ($questions as $key => $val) {
                            $results[$test]['questions'][$key] = $val;
                        }
                    }
                }
                $user->setAddon('st_tests_results', $results);
                $result = true;
            } catch (\Exception $e) {}

            return response()->json(['result' => $result]);
        }

        abort(404);
    }
}
