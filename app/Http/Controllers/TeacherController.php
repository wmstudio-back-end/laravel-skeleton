<?php

namespace App\Http\Controllers;

use Modules\Fields\Entities\Field;
use Modules\Reservation\Entities\Reservation;
use Modules\Topics\Entities\Topic;
use Modules\Topics\Entities\TopicsGroup;
use Modules\Topics\Entities\TopicsTag;
use Modules\Users\Entities\User;

class TeacherController extends Controller
{
    protected $aspectRatio = 1.25;
    protected $imageHeight = 180;

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        return view('teacher.student-find');
    }

    public function findStudents()
    {
        if (request()->ajax()) {
            $data = getRequest();
            if (isset($data['favorites'])) {
                if (!($data['favorites'] === 'true' || $data['favorites'] === true)) {
                    unset($data['favorites']);
                } elseif (isset($data['online'])) {
                    unset($data['online']);
                }
            } elseif (isset($data['online']) && !($data['online'] === 'true' || $data['online'] === true)) {
                unset($data['online']);
            }

            $users = app('Modules\Users\Controllers\IndexController')
                ->getFindUsersQuery($data)->paginate($this->usersPerPage);

            return response()->json([
                'current' => $users->currentPage(),
                'last' => $users->lastPage(),
                'totalUsers' => $users->total(),
                'users' => $users->getCollection()->toArray(),
            ]);
        }

        abort(404);
    }

    /**
     * @param string $login
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show(string $login)
    {
        $user = User::where('login', $login)->first();
        if (!$user || !$user->hasStatus('s')) {
            abort(404);
        }

        $menu = getMenu('dbMenu');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->lastLevel = "Информация о студенте";

        return view('teacher.student-info', [
            'user' => $user,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function reservationFilter()
    {
        $data = getRequest();
        $reservations = Reservation::where('user_id', ($user = \Auth::user())->id)
            ->where('time', '>', new \DateTime())->whereNotNull('reserved_user_id');
        if (isset($data['dateFrom']) && !isset($data['dateTo'])) {
            $date = $data['dateFrom'];
            $reservations->where('time', '>', $date->format('d.m.Y 00:00'))
                ->where('time', '<', $date->format('d.m.Y 23:59'));
        } elseif(isset($data['dateFrom']) && isset($data['dateTo'])) {
            $reservations->where('time', '>', $data['dateFrom'])
                ->where('time', '<', $data['dateTo']);
        }
        if (isset($data['name'])) {
            $search = explode(' ', $data['name']);
            $reservations->whereHas('reserved', function($query) use($search) {
                $query->whereHas('user_add_values', function($query) use($search) {
                    $query->whereHas('user_addon', function($query) use($search) {
                        $query->where('alias', 'name');
                    })->where(function($query) use($search) {
                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                        foreach ($search as $val) {
                            $query->orWhere('value', 'like', '%' . $val . '%');
                        }
                    });
                })->orWhereHas('user_add_values', function($query) use($search) {
                    $query->whereHas('user_addon', function($query) use($search) {
                        $query->where('alias', 'surname');
                    })->where(function($query) use($search) {
                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                        foreach ($search as $val) {
                            $query->orWhere('value', 'like', '%' . $val . '%');
                        }
                    });
                })->orWhereHas('user_add_values', function($query) use($search) {
                    $query->whereHas('user_addon', function($query) use($search) {
                        $query->where('alias', 'middle_name');
                    })->where(function($query) use($search) {
                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                        foreach ($search as $val) {
                            $query->orWhere('value', 'like', '%' . $val . '%');
                        }
                    });
                });
            });
        }
        $reservations->orderBy('time', 'ASC');

        $result = [];
        foreach ($reservations->get() as $reservation) {
            /* @var $reservation Reservation */
            $result[] = [
                'id' => $reservation->id,
                'time' => $reservation->time->format('d.m.Y H:i'),
                'duration' => $reservation->duration,
                'reserved' => (['id' => $reservation->reserved_user_id] + $reservation->reserved->toArray()),
            ];
        }

        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function topics()
    {
        $user = \Auth::user();

        $topicsGroups = TopicsGroup::where('active', true)->get();
        $topicsTags = [];
        foreach ($topicsGroups as $group) {
            /* @var $group TopicsGroup */
            if ($group->tags->count()) {
                $topicsTags[$group->id] = [];
                foreach ($group->tags->where('active', true) as $tag) {
                    /* @var $tag TopicsTag */
                    $topicsTags[$group->id][$tag->id] = transDef($tag->name, TopicsTag::TRANS_GROUP . '.' . TopicsTag::TRANS_KEY_PREFIX . '.' . $tag->alias);
                }
            }
        }
        $countTopics = Topic::where('user_id', $user->id)
            ->orWhere('users', 'like', '%"1":true%')->count('id');

        return view('teacher.topics', [
            'topicsGroups' => $topicsGroups,
            'topicsTags' => $topicsTags,
            'countTopics' => $countTopics,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function myTopics()
    {
        if (($status = ($user = \Auth::user())->getStatus()) === 't') {
            if (request()->isMethod('POST') && request()->ajax()) {
                $data = getRequest();

                $topics = Topic::where('user_id', $user->id);
                if (isset($data['tag'])) {
                    $topics->where('tag_id', $data['tag']);
                }

                $topics = $topics->paginate($this->usersPerPage);
                $arrTopics = [];
                foreach ($topics->getCollection() as $topic) {
                    /* @var $topic Topic */
                    $arrTopics[$topic->id] = [
                        'tag' => $topic->tag_id,
                        'name' => $topic->name,
                        'image' => url($topic->image),
                        'difficulty' => $topic->difficulty,
                        ($topic->type === Topic::TYPE_LINK ? 'link' : 'content') => $topic->content,
                    ];
                }

                return response()->json([
                    'current' => $topics->currentPage(),
                    'last' => $topics->lastPage(),
                    'topics' => $arrTopics,
                ]);
            } else {
                $topicsGroups = TopicsGroup::where('active', true)->get();
                $topicsTags = [];
                foreach ($topicsGroups as $group) {
                    /* @var $group TopicsGroup */
                    if ($group->tags->count()) {
                        $topicsTags[$group->id] = [];
                        foreach ($group->tags->where('active', true) as $tag) {
                            /* @var $tag TopicsTag */
                            $topicsTags[$group->id][$tag->id] = transDef($tag->name,
                                TopicsTag::TRANS_GROUP . '.' . TopicsTag::TRANS_KEY_PREFIX . '.' . $tag->alias);
                        }
                    }
                }
                $topicsCount = Topic::where('user_id', $user->id)->count('id');

                return view('teacher.my-topics', [
                    'topicsGroups' => $topicsGroups,
                    'topicsTags' => $topicsTags,
                    'topicsCount' => $topicsCount,
                    'mimes' => Field::AVAILABLE_IMAGE_FORMATS,
                ]);
            }
        }

        abort(404);
    }

    /**
     * @param string $action
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function myTopicsEdit($action)
    {
        if (request()->ajax() && $action === 'validate') {
            $data = getRequest();
            $topicErr = ___('topics.topic-error', [
                'href' => route('topics.my'),
            ]);
            $validator = \Validator::make($data, [
                'edit' => 'required|boolean',
                'topic-id' => 'required_if:edit,1|integer|exists:topics,id',
                'topic-tag' => 'required|integer|exists:topics_tags,id',
                'topic-name' => 'required|string|min:5|max:255',
                'topic-type' => 'required|integer|in:' . Topic::TYPE_LINK . ',' . Topic::TYPE_CONTENT,
                'topic-link' => 'required_if:topic-type,' . Topic::TYPE_LINK . '|url',
                'topic-content' => 'required_if:topic-type,' . Topic::TYPE_CONTENT,
                'topic-image-type' => 'string|required_if:edit,0|in:"image/' . implode('","image/', Field::AVAILABLE_IMAGE_FORMATS) . '"',
                'topic-image-size' => 'integer|max:' . fileSizeFromStr(ini_get('upload_max_filesize')),
            ], [
                'topic-id.required_if' => $topicErr,
                'topic-id.integer' => $topicErr,
                'topic-id.exist' => $topicErr,
                'edit.required' => ___('topics.action-error', [
                    'href' => route('topics.my'),
                ]),
                'topic-link.required_if' => __('validation.required_if', [
                    'attribute' => ':attribute',
                    'other' => '"' . ___('topics.attributes.type') . '"',
                    'value' => '"' . ___('topics.attributes.link') . '"',
                ]),
                'topic-content.required_if' => __('validation.required_if', [
                    'attribute' => ':attribute',
                    'other' => '"' . ___('topics.attributes.type') . '"',
                    'value' => '"' . ___('topics.attributes.content') . '"',
                ]),
                'topic-image-type.in' => __('validation.mimes', [
                    'attribute' => ':attribute',
                    'values' => implode(',', Field::AVAILABLE_IMAGE_FORMATS),
                ]),
                'topic-image-type.required_if' => __('validation.required', [
                    'attribute' => '"' . ___('topics.attributes.image') . '"',
                ]),
                'topic-image-size.max' => __('validation.max.numeric', [
                    'attribute' => ':attribute',
                    'max' => ini_get('upload_max_filesize'),
                ]),
            ], [
                'topic-tag' => '"' . ___('topics.attributes.tag') . '"',
                'topic-name' => '"' . ___('topics.attributes.name') . '"',
                'topic-type' => '"' . ___('topics.attributes.type') . '" ('
                    . ___('topics.attributes.link')
                    . '/'
                    . ___('topics.attributes.content')
                    . ')',
                'topic-link' => '"' . ___('topics.attributes.link') . '"',
                'topic-content' => '"' . ___('topics.attributes.content') . '"',
                'topic-image-type' => '"' . ___('topics.attributes.image-type') . '"',
                'topic-image-size' => '"' . ___('topics.attributes.image-size') . '"',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            } else {
                return response()->json(['errors' => false]);
            }
        } else {
            $data = getRequest();
            switch ($action) {
                case 'add':
                    $topic = new Topic();
                    $topic->user_id = \Auth::user()->id;
                    $topic->weight = Topic::max('weight') + 1;
                    break;
                case 'edit':
                    $topic = Topic::find($data['topic-id']);
                    break;
                case 'delete':
                    $topic = Topic::find($data['topic-id']);
                    $topic->delete();
                    return redirect()->route('topics.my');
                    break;
                default:
                    abort(404);
                    break;
            }
            $topic->tag_id = $data['topic-tag'];
            $topic->difficulty = $data['topic-difficulty'];
            $topic->name = $data['topic-name'];
            $topic->type = $data['topic-type'];
            if ($data['topic-type'] == Topic::TYPE_LINK) {
                $topic->content = $data['topic-link'];
            } else {
                $topic->content = $data['topic-content'];
            }
            if (isset($data['topic-image'])) {
                if (($fileInfo = uploadFile($data['topic-image'], 'topic-img',
                        $topic->image ?: 'topic-' . (Topic::max('id') + 1) . '-cover')) !== false) {
                    $topic->image = $fileInfo['name'];
                    imgCropResize($fileInfo['path'], $fileInfo['path'], $this->imageHeight * $this->aspectRatio, $this->imageHeight);
                }
            }
            $topic->save();

            return redirect()->route('topics.my');
        }
    }
}
