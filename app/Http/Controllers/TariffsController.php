<?php

namespace App\Http\Controllers;

use Modules\Settings\Entities\Setting;
use Modules\Payments\Entities\PaymentsInfo;
use Modules\Payments\SimplePay\Merchant;
use Modules\Payments\SimplePay\Payment;

class TariffsController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricePerMinute = json_decode(setting('price_per_minute'), true)['student'];
        $countMinutes = explode("\n", setting('count_minutes'));
        $defMinutes = reset($countMinutes);
        foreach ($countMinutes as $key => $countMinute) {
            if (strpos($countMinute, '*') !== false) {
                $countMinutes[$key] = (int)str_replace('*', '', $countMinute);
                $defMinutes = (int)$countMinutes[$key];
            } else {
                $countMinutes[$key] = (int)$countMinutes[$key];
            }
        }

        $countDays = explode("\n", setting('count_days'));
        $defDays = reset($countDays);
        foreach ($countDays as $key => $days) {
            if (strpos($days, '*') !== false) {
                $countDays[$key] = (int)str_replace('*', '', $days);
                $defDays = (int)$countDays[$key];
            } else {
                $countDays[$key] = (int)$countDays[$key];
            }
        }

        $tariffs = Setting::where('name', 'like', 'tariff-%')->get()->keyBy('name');
        foreach ($tariffs as $key => $tariff) {
            /* @var $tariff Setting */
            $tariffs[$key] = json_decode($tariff->value, true);
        }
        $defaultTariff = setting('default_tariff');
        $groupDiscount = setting('group_discount');

        $tariffs = $tariffs->toArray();

        // Промокодов пока нет
        $promoDiscount = 0;
        $session = \Session::get('selected-tariff', null);
        $selected = [
            'group-discount' => old('group-discount') ?: (isset($session['group-discount']) && $session['group-discount'] ? (int)$session['group-discount'] : 'no'),
            'promo-discount' => old('promo-discount') ?: (isset($session['promo-discount']) && $session['promo-discount'] ? (int)$session['promo-discount'] : 'no'),
            'minutes-per-day' => (int)(old('minutes-per-day') ?: (isset($session['minutes-per-day']) ? $session['minutes-per-day'] : $defMinutes)),
            'lessons-per-week' => (int)(old('lessons-per-week') ?: (isset($session['lessons-per-week']) ? $session['lessons-per-week'] : $defDays)),
            'tariff-name' => old('tariff-name') ?: (isset($session['tariff-name']) ? $session['tariff-name'] : $defaultTariff),
        ];

        $now = new \DateTime();
        $activePeriodDays = $now->diff((clone $now)->modify('+1 ' . PaymentsInfo::ACTIVE_PERIOD))->days;
        foreach ($tariffs as $name => $tariff) {
            $tariffs[$name]['period-weeks'] = $now
                    ->diff((clone $now)->modify('+' . $tariff['period'] . ' month'))
                    ->days / $activePeriodDays;
        }

        return view('tariffs', [
            'pricePerMinute' => $pricePerMinute,
            'countMinutes' => $countMinutes,
            'defMinutes' => $defMinutes,
            'countDays' => $countDays,
            'defDays' => $defDays,
            'tariffs' => $tariffs,
            'selected' => $selected,
            'session' => $session,
            'groupDiscount' => $groupDiscount,
        ]);
    }

    /**
     * @param \App\User|\Modules\Users\Entities\User $user
     * @param array $data
     * @return string
     * @throws \Exception
     */
    public function continueSubscribe($user, $data)
    {
        $start = new \DateTime();
        $payment = PaymentsInfo::where([
            'user_id' => $user->id,
            'paid' => true,
        ])->where('from', '<', $start)->where('to', '>', $start)
            ->orderByDesc('to')->first();
        if ($payment) {
            $start = $payment->to->modify('+1 second');
        }
        $end = (clone $start)->modify('+' . $data['tariff']['period'] . ' ' . $data['period']);

        // Активный период (по умолчанию = неделя)
        // Количество выбранных минут в активный период
        $activePeriodMinutes = $data['minutes-per-day'] * $data['lessons-per-week'];
        // Количество дней в активный период
        $activePeriodDays = ($start)->diff((clone $start)->modify('+1 ' . PaymentsInfo::ACTIVE_PERIOD))->days;
        // Количество активных периодов в тарифе
        $activePeriodsInTariff = $start
                ->diff((clone $start)->modify('+' . $data['tariff']['period'] . ' month'))
                ->days / $activePeriodDays;
        // Количество минут в тарифе
        $countTariffMinutes = $activePeriodMinutes * $activePeriodsInTariff;
        // Количество секунд в тарифе (округляем в большую сторону)
        $countTariffSeconds = (int)ceil($countTariffMinutes * 60);
        // Цена тарифа (округляем до 2х знаков после запятой)
        $price = round(
            $data['pricePerMinute'] * $countTariffMinutes
            * ((100 - $data['tariff']['discount']) / 100)
            * ((100 - ($data['group-discount'] === 'no' ? 0 : $data['group-discount'])) / 100)
            * ((100 - ($data['promo-discount'] === 'no' ? 0 : $data['promo-discount'])) / 100)
            , 2);

        $data['tariff']['summary'] = [
            'seconds' => $countTariffSeconds,
            'price'   => $price,
        ];
        $session = \Session::get('selected-tariff', null);

        if (isset($session['order-id'])) {
            $order = PaymentsInfo::find($session['order-id']);
        }
        if (!isset($order) || !$order) {
            $order = new PaymentsInfo();
        }
        $order->uuid = uniqid('', true);
        $order->user_id = $user->id;
        $order->from = $start;
        $order->to = $end;
        $order->tariff_info = $data;
        $order->period = PaymentsInfo::ACTIVE_PERIOD;
        $order->save();

        \Session::put('selected-tariff', $data);

        try {
            //<editor-fold defaultstate="collapsed" desc="SimplePay">
            // $payment = new Payment();
            // $payment->amount = number_format($price, 2, '.', '');
            // $payment->order_id = $order->id;
            // $payment->client_name = $user->getFullName(true);
            // $payment->client_email = $user->email;
            // $payment->client_phone = $user->addon('phone');
            // $payment->description = $description;
            // $payment->client_ip = request()->ip();

            // // Создаем объект мерчант-класса SimplePay
            // $simplePay = new Merchant();// Запрос на создание платежа
            // $directPayment = $simplePay->direct_payment($payment);// Получаем ссылку на платежную страницу
            // $paymentLink = $directPayment['sp_redirect_url'];

            // // todo сделать сенсор
            // return $paymentLink;
            //</editor-fold>

            //<editor-fold desc="Яндекс.Касса">
            return app(\Modules\Payments\YandexCassa\Payment::class)->createPayment($order);
            //</editor-fold>
        } catch (\Exception $e) {
            // Если по какой-то причине не получили ссылку, удаляем ордер
            $order->delete();
            throw $e;
        }
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function subscribe()
    {
        $data = getRequest();

        $pricePerMinute = json_decode(setting('price_per_minute'), true)['student'];
        $promoDiscount = ['no'];
        $groupDiscount = ['no', setting('group_discount')];
        $minutesPerDay = explode("\n", str_replace('*', '', setting('count_minutes')));
        $lessonsPerWeek = explode("\n", str_replace('*', '', setting('count_days')));
        $tariffs = Setting::select(['name', 'value'])->where('name', 'like', 'tariff-%')->get()->keyBy('name');

        $validator = [
            'promo-discount' => 'required|in:' . implode(',', $promoDiscount),
            'group-discount' => 'required|in:' . implode(',', $groupDiscount),
            'minutes-per-day' => 'required|in:' . implode(',', $minutesPerDay),
            'lessons-per-week' => 'required|in:' . implode(',', $lessonsPerWeek),
            'tariff-name' => 'required|in:' . implode(',', $tariffs->keys()->toArray()),
        ];

        $validator = \Validator::make($data, $validator, [], ___('tariffs.attributes'));

        if (!$validator->fails()) {
            $data['pricePerMinute'] = $pricePerMinute;
            $data['group-discount'] = $data['group-discount'] === 'no' ? 0 : (int)$data['group-discount'];
            $data['promo-discount'] = $data['promo-discount'] === 'no' ? 0 : (int)$data['promo-discount'];
            $data['minutes-per-day'] = (int)$data['minutes-per-day'];
            $data['lessons-per-week'] = (int)$data['lessons-per-week'];

            $data['period'] = 'month';
            $data['tariff'] = json_decode($tariffs[$data['tariff-name']]->value, true);
            $data['tariff']['name'] = $data['tariff-name'];

            $user = \Auth::user();
            if ($user) {
                return redirect()->to($this->continueSubscribe($user, $data));
            } else {
                \Session::put('selected-tariff', $data);
                return redirect()->route('login');
            }
        } else {
            return redirect()->route('tariffs')->withErrors($validator);
        }
    }

    /**
     * @throws \Exception
     */
    public function paymentSuccess()
    {
        if (\Session::has('selected-tariff')) {
            \Session::forget('selected-tariff');
        }
        $old = old();
        $order = null;
        if (isset($old['sp_order_id'])) {
            $order = PaymentsInfo::find($old['sp_order_id']);
        } elseif (\Session::has('yac_uuid')) {
            /** @var PaymentsInfo $order */
            $order = PaymentsInfo::where('uuid', \Session::get('yac_uuid'))->first();
            \Session::forget('yac_uuid');
        }

        if ($order) {
            $tariffInfo = $order->tariff_info;
            $tariffInfo['currency'] = $order->currency;
            if (isset($tariffInfo['simple-pay-result'])) {
                unset($tariffInfo['simple-pay-result']);
            }
            return view('payment.success', [
                'old' => $old,
                'info' => $tariffInfo,
            ]);
        }

        return redirect()->route('payments');
    }

    /**
     * @throws \Exception
     */
    public function paymentError()
    {
        $old = old();
        $orderId = null;
        if ($old) {
            $selected = \Session::get('selected-tariff');
            $order = null;
            if (isset($old['sp_order_id'])) {
                $order = PaymentsInfo::find($old['sp_order_id']);
            } elseif (\Session::has('yac_uuid')) {
                $order = PaymentsInfo::where('uuid', \Session::get('yac_uuid'))->first();
                \Session::forget('yac_uuid');
            }
            if ($order) {
                if (!$selected) {
                    $selected = $order->tariff_info;
                    if (isset($selected['simple-pay-result'])) {
                        unset($selected['simple-pay-result']);
                    }
                    if (isset($selected['tariff']['summary'])) {
                        unset($selected['tariff']['summary']);
                    }
                }
                $selected['order-id'] = $order->id;
            }

            \Session::put('selected-tariff', $selected);
            return view('payment.error', [

            ]);
        }

        return redirect()->route('tariffs');
    }

    /**
     * @throws \Exception
     */
    public function paymentResult()
    {
        $simplePay = new Merchant();
        $simplePay->process_result_request();
    }
}