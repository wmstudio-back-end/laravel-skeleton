<?php

namespace App\Http\Controllers;

use Modules\Reservation\Entities\Reservation;
use Modules\Topics\Entities\TopicsGroup;
use Modules\Topics\Entities\TopicsTag;
use Modules\Users\Entities\User;

class StudentController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $getOccupancyLevel = function(){
            $countArgs = func_num_args();
            $countFull = 0;
            if ($countArgs) {
                foreach (func_get_args() as $arg) {
                    if (!empty($arg)) {
                        $countFull++;
                    }
                }
            } else {
                $countArgs = $countFull = 1;
            }

            return round($countFull / $countArgs * 100, 0);
        };

        $user = \Auth::user();
        $occupancyLevel = $getOccupancyLevel(
            $user->addon('avatar'),
            $user->addon('st_dialect'),
            $user->addon('st_lang_level'),
            $user->addon('st_learn_targets'),
            $user->addon('st_about_me'),
            $user->addon('st_lesson_type')
        );

        return view('student.teacher-find', [
            'occupancyLevel' => $occupancyLevel,
        ]);
    }

    public function findTeachers()
    {
        if (request()->ajax()) {
            $data = getRequest();
            if (isset($data['favorites'])) {
                if (!($data['favorites'] === 'true' || $data['favorites'] === true)) {
                    unset($data['favorites']);
                } elseif (isset($data['online'])) {
                    unset($data['online']);
                }
            } elseif (isset($data['online']) && !($data['online'] === 'true' || $data['online'] === true)) {
                unset($data['online']);
            }

            $users = app('Modules\Users\Controllers\IndexController')
                ->getFindUsersQuery($data)->paginate($this->usersPerPage);

            return response()->json([
                'current' => $users->currentPage(),
                'last' => $users->lastPage(),
                'totalUsers' => $users->total(),
                'users' => $users->getCollection()->toArray(),
                'dates' => [
                    'date-from' => isset($data['date-from']) ? $data['date-from'] : null,
                    'date-to' => isset($data['date-to']) ? $data['date-to'] : null,
                ],
                'favorites' => (isset($data['favorites']) && $data['favorites'] === true)
            ]);
        }

        abort(404);
    }

    /**
     * @param string $login
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show(string $login)
    {
        $user = User::where('login', $login)->first();
        if (!$user || !$user->hasStatus('t')) {
            abort(404);
        }

        $reservations = Reservation::where('user_id', $user->id)->where(function($query) {
            $query->where('reserved_user_id', \Auth::user()->id)
                ->orWhereNull('reserved_user_id');
        })->where('time', '>', new \DateTime())->orderBy('time', 'ASC')->get();

        $freeTime = [];
        foreach ($reservations as $reservation) {
            /* @var $reservation Reservation */
            if (!isset($freeTime[$reservation->time->format('d.m.Y')])) {
                $freeTime[$reservation->time->format('d.m.Y')] = [];
            }

            $freeTime[$reservation->time->format('d.m.Y')][] = [
                'id' => $reservation->id,
                'from' => $reservation->time->format('H:i'),
                'to' => $reservation->time->modify('+' . $reservation->duration . 'minutes')->format('H:i'),
                'user' => $reservation->reserved_user_id,
            ];
        }

        $menu = getMenu('dbMenu');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->lastLevel = "Информация о преподавателе";

        return view('student.teacher-info', [
            'user' => $user,
            'freeTime' => $freeTime,
            'firstTime' => $reservations->count() ? $reservations->first()->time->format('Y/m/d') : null,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function topics()
    {
        $topicsGroups = TopicsGroup::where('active', true)->get();
        $topicsTags = [];
        foreach ($topicsGroups as $group) {
            /* @var $group TopicsGroup */
            if ($group->tags->count()) {
                $topicsTags[$group->id] = [];
                foreach ($group->tags->where('active', true) as $tag) {
                    /* @var $tag TopicsTag */
                    $topicsTags[$group->id][$tag->id] = transDef($tag->name, TopicsTag::TRANS_GROUP . '.' . TopicsTag::TRANS_KEY_PREFIX . '.' . $tag->alias);
                }
            }
        }

        return view('student.topics', [
            'topicsGroups' => $topicsGroups,
            'topicsTags' => $topicsTags,
        ]);
    }
}
