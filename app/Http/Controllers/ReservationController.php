<?php

namespace App\Http\Controllers;

use Modules\Reservation\Entities\Reservation;
use Modules\Users\Entities\User;

class ReservationController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        if ($user = \Auth::user()) {
            $status = null;
            switch ($user->getStatus()) {
                case 's':
                    $status = 'student';
                    break;
                case 't':
                    $status = 'teacher';
                    break;
                default:
                    abort(404);
                    break;
            }

            if (request()->isMethod('POST')) {
                $data = getRequest();
                $reservations = Reservation::where('time', '>', new \DateTime())
                    ->whereNotNull('reserved_user_id')
                    ->orderBy('time', 'ASC');
                if ($status === 'teacher') {
                    $reservations->where('user_id', $user->id);
                } else {
                    $reservations->where('reserved_user_id', $user->id);
                }

                if (isset($data['dateFrom']) && !isset($data['dateTo'])) {
                    $date = $data['dateFrom'];
                    $reservations->where('time', '>', $date->format('Y-m-d 00:00'))
                        ->where('time', '<', $date->format('Y-m-d 23:59:59'));
                } elseif(isset($data['dateFrom']) && isset($data['dateTo'])) {
                    $reservations->where('time', '>=', $data['dateFrom'])
                        ->where('time', '<=', $data['dateTo']);
                }
                if (isset($data['name'])) {
                    $search = explode(' ', $data['name']);
                    $reservations->whereHas('reserved', function($query) use($search) {
                        $query->whereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'name');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        })->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'surname');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        })->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'middle_name');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        });
                    });
                }

                $result = [];
                foreach ($reservations->get() as $reservation) {
                    /* @var $reservation Reservation */
                    $result[] = [
                        'id' => $reservation->id,
                        'time' => $reservation->time->format('Y.m.d H:i'),
                        'duration' => $reservation->duration,
                        'reserved' => (
                            $status === 'teacher'
                                ? (['id' => $reservation->reserved_user_id] + $reservation->reserved->toArray())
                                : (['id' => $reservation->user_id] + $reservation->user->toArray())
                        ),
                    ];
                }

                return response()->json($result);
            } else {
                $reservations = Reservation::where('time', '>', new \DateTime())->orderBy('time', 'ASC');
                if ($status === 'teacher') {
                    $reservations->where('user_id', $user->id);
                } else {
                    $reservations->where('reserved_user_id', $user->id);
                }
                $reservations = $reservations->get();

                $users = [];
                $freeTime = [];
                foreach ($reservations as $reservation) {
                    /* @var $reservation Reservation */
                    if (!isset($freeTime[$reservation->time->format('d.m.Y')])) {
                        $freeTime[$reservation->time->format('d.m.Y')] = [];
                    }

                    if ($status === 'teacher' && $reservation->reserved_user_id !== null) {
                        $users[$reservation->reserved_user_id] = true;
                    } else {
                        $users[$reservation->user_id] = true;
                    }

                    $freeTime[$reservation->time->format('d.m.Y')][] = [
                        'id' => $reservation->id,
                        'from' => $reservation->time->format('H:i'),
                        'to' => $reservation->time->modify('+' . $reservation->duration . 'minutes')->format('H:i'),
                        'user' => $reservation->reserved_user_id,
                    ];
                }

                $users = User::whereIn('id', array_keys($users))->get()->keyBy('id');
                $countReserved = $reservations->where('reserved_user_id', '!=', null)->count();

                $result = [
                    'users' => $users,
                    'countReserved' => $countReserved,
                    'freeTime' => $freeTime,
                ];
                if ($status === 'teacher') {
                    $result['firstTime'] = $reservations->count() ? $reservations->first()->time->format('Y/m/d') : null;
                }
                return view($status . '.reservation', $result);
            }
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * @param \DateTime $date
     * @return array
     */
    private function getFromDate($date)
    {
        $user = \Auth::user();
        if ($user->getStatus() === 't') {
            $reservations = Reservation::where('user_id', $user->id);
        } else {
            $reservations = Reservation::where(function($query) use ($user) {
                $query->where('reserved_user_id', $user->id)
                    ->orWhereNull('reserved_user_id');
            })->whereHas('user', function($query){
                $query->where('login', app('router')->getRoutes()->match(request()->create(url()->previous()))->parameters()['login']);
            });
        }
        $reservations->where('time', '>', new \DateTime($date->format('Y-m-d 00:00')))
            ->where('time', '<', new \DateTime($date->format('Y-m-d 23:59')))
            ->orderBy('time', 'ASC');
        $reservations = $reservations->get();

        $freeTime = [];
        foreach ($reservations as $reservation) {
            /* @var $reservation Reservation */
            $freeTime[] = [
                'id' => $reservation->id,
                'from' => $reservation->time->format('H:i'),
                'to' => $reservation->time->modify('+' . $reservation->duration . 'minutes')->format('H:i'),
                'user' => $reservation->reserved_user_id,
            ];
        }

        return $freeTime;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $data = getRequest();
        if (
            request()->ajax()
            && ($user = \Auth::user())
            && $user->getStatus() === 't'
            && isset($data['date'])
            && $data['date'] > new \DateTime()
            && isset($data['duration'])
        ) {
            $reserved = Reservation::where('user_id', $user->id)
                ->where('time', '<=', $data['date'])
                ->whereRaw('(time + INTERVAL duration MINUTE) > "' . $data['date']->format('Y-m-d H:i:s') .'"')
                ->count('id');
            if ($reserved) {
                return response()->json([
                    'error' => ___('reservation.already-reserved'),
                ]);
            } else {
                $reservation = new Reservation();
                $reservation->user_id = $user->id;
                $reservation->time = $data['date'];
                $reservation->duration = $data['duration'];
                $reservation->save();

                return response()->json($this->getFromDate($data['date']));
            }
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function actions()
    {
        $data = getRequest();
        if (request()->ajax() && ($user = \Auth::user()) && isset($data['reservation-id'])) {
            $reservation = Reservation::find($data['reservation-id']);
            /* @var $reservation Reservation */
            if ($reservation) {
                $status = $user->getStatus();
                if ($status === 't' && $reservation->user_id === $user->id) {
                    $date = $reservation->time;
                    if ($reservation->reserved_user_id !== null) {
                        $reservation->reserved_user_id = null;
                        $reservation->save();
                        $res = '$reservation->reserved_user_id = null;';
                    } else {
                        $reservation->delete();
                        $res = '$reservation->delete();';
                    }

                    if (isset($data['only-action']) && $data['only-action'] == true) {
                        return response()->json(['error' => $res]);
                    } else {
                        return response()->json($this->getFromDate($date));
                    }
                } elseif ($status === 's') {
                    if ($reservation->reserved_user_id == null) {
                        $reservation->reserved_user_id = $user->id;
                    } else {
                        $reservation->reserved_user_id = null;
                    }
                    $reservation->save();

                    return response()->json($this->getFromDate($reservation->time));
                }
            }
        }

        abort(404);
    }
}
