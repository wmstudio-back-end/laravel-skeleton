<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Modules\Users\Entities\UserAction;
use App\Http\Controllers\Controller;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class VerifyController extends Controller
{
    /**
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function waitConfirm($userId)
    {
        $user = User::find($userId);
        if (!$user || $user->email_confirmed || $user->active) {
            abort(404);
        }
        return view('auth.wait-confirm', [
            'email' => $user->email,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function lastStep()
    {
        if (($token = \Session::get('login_token')) === null) {
            abort(404);
        }

        $action = UserAction::where('token', $token)->first();
        /* @var $action UserAction */
        if ($action) {
            $date = new \DateTime($action->created_at);
            $date->modify('+' . UserAction::EMAIL_CONFIRM_LEFT_HOURS . ' hours');
            if (new \DateTime() < $date && $user = User::find($action->user_id)) {
                if (request()->isMethod('POST')) {
                    $data = getRequest();
                    $data['cam-phone'] = phoneFormat($data['cam-phone-code'] . $data['cam-phone-number'], true);
                    $validator = \Validator::make($data, [
                        'cam-name' => 'required|string|regex:/^[a-zA-Z][a-zA-Z0-9- \.]*$/|max:255|min:2',
                        'cam-surname' => 'required|string|regex:/^[a-zA-Z][a-zA-Z0-9- \.]*$/|max:255|min:2',
                        'cam-phone' => 'required|regex:/^\+\d{11,13}/|phone',
                    ], [
                        'cam-name.regex' => ___('validation.name'),
                        'cam-surname.regex' => ___('validation.name'),
                    ]);

                    if (!$validator->fails()) {
                        $user->setAddon('name', $data['cam-name']);
                        $user->setAddon('surname', $data['cam-surname']);
                        $user->setAddon('phone', $data['cam-phone']);

                        $user->active = true;
                        $user->email_confirmed = true;
                        $user->save();
                        $action->delete();

                        \Auth::guard()->login($user);
                        \Session::forget('login_token');
                        return redirect($this->redirectTo());
                    } else {
                        return redirect()->route('last-step')->withErrors($validator)->withInput();
                    }
                } else {
                    $user->email_confirmed = true;
                    $user->save();
                }

                return view('auth.email-confirmed', [
                    'user' => $user,
                ]);
            }
        }

        abort(404);
    }

    /**
     * Confirm e-mail address
     *
     * @param string $token
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function emailConfirm($token = null)
    {
        \Session::put('login_token', $token);
        return redirect()->route('last-step');
    }
}
