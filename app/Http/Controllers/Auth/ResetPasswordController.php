<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Hash;
use Modules\Users\Emails\ResetPasswordEmail;
use Modules\Users\Entities\User;
use Modules\Users\Entities\UserAction;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param string $token
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function resetPassword($token = null)
    {
        $user = null;
        if ($token) {
            $token = UserAction::where([
                'action_id' => UserAction::A_RESTORE_PASSWORD,
                'token' => $token
            ])->first();
            if ($token) {
                $user = $token->user;
            }
        }
        if ($user) {
            /* @var $user User */
            if (request()->method() === 'POST') {
                $data = getRequest();

                $validator = [
                    'cam-password' => 'required|string|min:6|confirmed',
                ];
                $validator = \Validator::make($data, $validator);
                if (!$validator->fails()) {
                    $user->email_confirmed = true;
                    $user->active = true;
                    $user->password = Hash::make($data['cam-password']);
                    $user->save();

                    if (\Session::has('restore-password-id')) {
                        \Session::forget('restore-password-id');
                    }

                    UserAction::where([
                        'user_id' => $user->id,
                        'action_id' => UserAction::A_RESTORE_PASSWORD
                    ])->delete();
                    $this->guard()->login($user, true);
                    return redirect()->route('home');
                }

                return redirect()->route('password.reset', ['token' => $token->token])->withErrors($validator);
            } else {
                return view('auth.passwords.new-password', [
                    'token' => $token->token,
                ]);
            }
        } else {
            if (\Session::has('restore-password-id')) {
                return view('auth.passwords.email');
            } elseif (request()->method() === 'POST') {
                $data = getRequest();

                $validator = [
                    'cam-email' => 'required|string|email|max:255|exists:users,email',
                    'g-recaptcha-response' => 'required|captcha',
                ];
                $validator = \Validator::make($data, $validator, [
                    'cam-email.exists' => ___('validation.email-not-found', [
                        'email' => $data['cam-email'],
                    ]),
                ]);
                if (!$validator->fails()) {
                    $user = User::where([
                        'email' => $data['cam-email'],
                        'active' => true,
                    ])->first();
                    if ($user) {
                        $action = new UserAction();
                        $action->user_id = $user->id;
                        $action->action_id = UserAction::A_RESTORE_PASSWORD;
                        $action->token = md5('user_action' . (new \DateTime())->format('YmdHisu') . UserAction::A_RESTORE_PASSWORD);
                        $action->save();

                        \Mail::to($user->email)->send(new ResetPasswordEmail($action));

                        \Session::put('restore-password-id', $action->id);
                        return redirect()->route('password.reset', ['token' => null]);
                    } else {
                        $validator->errors()->add('cam-email', ___('validation.email-not-found', [
                            'email' => $data['cam-email'],
                        ]));
                    }
                }
                return view('auth.passwords.reset')->withErrors($validator);
            } else {
                return view('auth.passwords.reset');
            }
        }
    }
}
