<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'cam-login';
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $validator = $this->validateLogin($request->all());
        if ($validator->fails()) {
            return redirect()->route('login')->withErrors($validator)->withInput();
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        $validator = Validator::make(
            [],
            [$this->username() => 'required'],
            ['required' => trans('auth.failed')]
        );
        return redirect()->route('login')->withErrors($validator)->withInput([
            'cam-login' => $request->get('cam-login', ''),
            'cam-remember' => $request->get('cam-remember', 0),
        ]);
//        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @return string
     */
    protected function redirectTo()
    {
        $redirect = parent::redirectTo();
        if ($this->redirectTo === '/') {
            return $redirect;
        } else {
            return $this->redirectTo;
        }
    }

    /**
     * Validate the user login request.
     *
     * @param  array $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateLogin(array $request)
    {
        return Validator::make($request, [
            $this->username() => 'required|string',
            'cam-password' => 'required|string',
        ]);
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm(Request $request)
    {
        if (!\Session::has('login_previous_url')) {
            \Session::put('login_previous_url', url()->previous());
        }
        if (!count($request->old())) {
            Input::merge(['cam-remember' => true]);
            Input::flash();
        }
        return view('auth.login');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        if (!\Session::has('login_previous_url')) {
            \Session::put('login_previous_url', url()->previous());
        }

        $user = User::where(['login' => $request->get('cam-login')])->orWhere(['email' => $request->get('cam-login')])->first();

        if (!is_null($user) && Hash::check($request->get('cam-password'), $user->password)) {
            if (!$user->email_confirmed && !$user->active) {
                $this->redirectTo = route('wait-confirm', ['userId' => $user->id]);
                return false;
            } elseif (!$user->active) {
                if (\Session::has('login_token')) {
                    $this->redirectTo = route('last-step');
                    return false;
                } else {
                    $this->redirectTo = route('login');
                    return false;
                }
            } else {
                $this->guard()->login($user, $request->has('cam-remember'));
                return true;
            }
        }

        return false;
    }

    public function logout(Request $request)
    {
        $lang = \Session::get('lang');

        $this->guard()->logout();
        $request->session()->invalidate();

        if ($lang) {
            \Session::put('lang', $lang);
        }

        return redirect()->route('home');
    }
}
