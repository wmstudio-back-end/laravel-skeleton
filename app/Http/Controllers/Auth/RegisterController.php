<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Modules\Users\Emails\EmailConfirm;
use Modules\Users\Entities\UserAction;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        if (!\Session::has('login_previous_url')) {
            \Session::put('login_previous_url', url()->previous());
        }

        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return redirect()->route('register')->withErrors($validator)->withInput();
        }

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function redirectTo()
    {
        return $this->redirectTo;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'cam-login' => 'required|regex:/(^[a-zA-Z0-9\-\_]+$)/u|max:255|min:5|unique:users,login',
            'cam-email' => 'required|string|email|max:255|unique:users,email',
            'cam-password' => 'required|string|min:6|confirmed',
            'cam-ref-link' => 'referral',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        $user = User::create([
            'login' => $data['cam-login'],
            'email' => $data['cam-email'],
            'password' => Hash::make($data['cam-password']),
        ]);
        $this->redirectTo = route('wait-confirm', ['userId' => $user->id]);

        $date = new \DateTime();
        $token = md5($user->id . UserAction::A_EMAIL_CONFIRM . $date->format('dmYHis-u-v'));
        UserAction::create([
            'user_id' => $user->id,
            'action_id' => UserAction::A_EMAIL_CONFIRM,
            'token' => $token,
        ]);

        $sendMail = function() use($data, $date, $token) {
            \Mail::to($data['cam-email'])->send(new EmailConfirm($date, route('email-confirm', ['token' => $token])));
        };
        if (isset($data['id']) && isset($data['provider'])) {
            $addons = ['social_' . $data['provider'], 'name', 'middle_name', 'surname'];
            $addons = UserAddon::whereIn('alias', $addons)->get()->keyBy('alias');
            if (isset($addons['social_' . $data['provider']])) {
                UserAddValue::create([
                    'user_id' => $user->id,
                    'addon_id' => $addons['social_' . $data['provider']]->id,
                    'value' => $data['id'],
                ]);
            }
            if (isset($data['first_name']) && isset($addons['name'])) {
                UserAddValue::create([
                    'user_id' => $user->id,
                    'addon_id' => $addons['name']->id,
                    'value' => $data['first_name'],
                ]);
            }
            if (isset($data['middle_name']) && isset($addons['middle_name'])) {
                UserAddValue::create([
                    'user_id' => $user->id,
                    'addon_id' => $addons['middle_name']->id,
                    'value' => $data['middle_name'],
                ]);
            }
            if (isset($data['last_name']) && isset($addons['surname'])) {
                UserAddValue::create([
                    'user_id' => $user->id,
                    'addon_id' => $addons['surname']->id,
                    'value' => $data['last_name'],
                ]);
            }
            if (isset($data['social-email']) && $data['social-email'] == $user->email) {
                \Session::put('login_token', $token);
                $this->redirectTo = route('last-step');
            } else {
                $sendMail();
            }
        } else {
            $sendMail();
        }

        if (isset($data['cam-ref-link'])) {
            $referal = explode('/', $data['cam-ref-link']);
            $referal = end($referal);
            $refAddon = UserAddon::whereIn('alias', ['referral_token', 'invitees_list'])->get()->keyBy('alias');
            if (isset($refAddon['referral_token']) && isset($refAddon['invitees_list'])) {
                $referal = UserAddValue::where([
                    'addon_id' => $refAddon['referral_token']->id,
                    'value' => $referal,
                ])->first();
                if ($referal) {
                    /* @var $referal UserAddValue */
                    $invites = UserAddValue::where([
                        'user_id' => $referal->user_id,
                        'addon_id' => $refAddon['invitees_list']->id,
                    ])->first();
                    if ($invites) {
                        /* @var $invites UserAddValue */
                        $invitesList = json_decode($invites->value, true);
                    } else {
                        $invites = new UserAddValue();
                        $invites->user_id = $referal->user_id;
                        $invites->addon_id = $refAddon['invitees_list']->id;
                        $invitesList = [];
                    }
                    if (is_array($invitesList)) {
                        $invitesList[$user->id] = [];
                        $invites->value = json_encode($invitesList);
                        $invites->save();
                        $user->setAddon('st_free_time', 60 * 10);
                    }
                }
            }
        }

        $addons = UserAddon::whereIn('alias', ['lang', 'how_know_about_us', 'invitees_list'])->get()->keyBy('alias');
        if (isset($addons['lang'])) {
            $value = new UserAddValue();
            $value->user_id = $user->id;
            $value->addon_id = $addons['lang']->id;
            $value->value = \Session::get('lang', config('app.locale'));
            $value->save();
        }
        if (isset($data['cam-how-know'])) {
            if (isset($addons['how_know_about_us'])) {
                $value = new UserAddValue();
                $value->user_id = $user->id;
                $value->addon_id = $addons['how_know_about_us']->id;
                $value->value = $data['cam-how-know'];
                $value->save();
            }
        }

        return $user;
    }

    public static function transformInputNames(array $names)
    {
        $defaults = [
            'login' => 'cam-login',
            'email' => 'cam-email',
            'referral' => 'cam-ref-link',
        ];

        $result = [];
        foreach ($names as $key => $val) {
            if (isset($defaults[$key])) {
                $key = $defaults[$key];
            }
            $result[$key] = $val;
        }

        return $result;
    }
}
