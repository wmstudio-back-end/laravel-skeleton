<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;
use Modules\Sensors\Entities\SensorCall;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use Modules\Users\Entities\User;
use Modules\Users\Emails\JoinUs;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function account()
    {
        if (request()->isMethod('POST')) {
            $data = getRequest();
            $data['birthday'] = $data['birthday-day'] . '.' . $data['birthday-month'] . '.' . $data['birthday-year'];
            $user = \Auth::user();

            $first = (
                $user->addon('country') === null
                || $user->addon('city') === null
                || $user->addon('birthday') === null
                || $user->addon('natural_lang') === null
                || $user->addon('speak_langs') === null
            );
            $validator = [
                'country' => 'required|integer',
                'city' => 'required|string',
                'birthday' => 'required|date',
                'natural_lang' => 'required|string',
                'speak_langs' => 'required|array',
                'lang' => 'required|string',
            ];
            if ($user->addon('name') != $data['name']) {
                $validator['name'] = 'required|string|regex:/^[a-zA-Z][a-zA-Z0-9- \.]*$/|max:255|min:2';
            }
            if ($user->addon('surname') != $data['surname']) {
                $validator['surname'] = 'required|string|regex:/^[a-zA-Z][a-zA-Z0-9- \.]*$/|max:255|min:2';
            }
            if ($user->email != $data['email']) {
                $validator['email'] = 'required|string|email|max:255|unique:users,email';
            }

            $data['phone'] = phoneFormat($data['phone-code'] . $data['phone-number'], true);
            if ($user->addon('phone') != $data['phone']) {
                $validator['phone'] = 'required|regex:/^\+\d{11,13}/|phone';
            }

            if (isset($data['password']) || isset($data['new-password']) || isset($data['new-password_confirmation'])) {
                if (!Hash::check($data['password'], $user->password)) {
                    $data['password'] = null;
                    $validator['password'] = 'required';
                }
                $validator['new-password'] = 'required|string|min:6|confirmed';
            }

            $validator = \Validator::make($data, $validator, [
                'password.required' => ___('auth.password-failed')
            ], [
                'name.regex' => ___('validation.name'),
                'surname.regex' => ___('validation.name'),
            ]);

            if (!$validator->fails()) {
                $messages = [];
                if (isset($data['photo']) && strpos($data['photo'], 'base64,') !== false) {
                    $field = Field::where('alias', 'avatar')->first();
                    if ($field) {
                        if ($avatar = $user->addon('avatar')) {
                            $avatar = FieldsValue::where([
                                'field_id' => $field->id,
                                'alias' => $avatar,
                            ])->first();
                        } else {
                            $avatar = new FieldsValue();
                            $avatar->field_id = $field->id;
                            $avatar->alias = 'user-' . strtolower($user->login) . '-avatar';
                            $avatar->value = config('app.upload-dir') . md5(date_format(new \DateTime(), 'dmYHis') . $avatar->alias);
                        }
                        $avatar->updated_at = new \DateTime();
                        $avatar->save();
                        if (file_exists($avatar->value)) {
                            unlink($avatar->value);
                        }
                        $file = fopen($avatar->value, 'wb');
                        $base64 = explode( ',', $data['photo'] );
                        fwrite($file, base64_decode($base64[1]));
                        fclose($file);
                        imgResize($avatar->value, $avatar->value, 120);
                        $user->setAddon('avatar', $avatar->alias);
                    }
                }

                $status = isset($data['status']) ? $data['status'] : '';
                // Сенсор на запрос прав преподавать
                if (
                    $status === 'q'
                    && $user->getStatus() !== 'q'
                    && !(bool)$user->addon('tc_was_permitted')
                ) {
                    SensorCall::call('users', 'request_teach_permissions');
                }
                $canTeach = (bool)$user->addon('tc_can_teach');
                if ($status === 't') {
                    // Если статус учитель, и МОЖЕТ учить, сохраняем статус.
                    if ($canTeach) {
                        $status = 't';
                    } else {
                        $status = $user->getStatus();
                    }
                } elseif ($status === 'q' && $canTeach) {
                    // Если статус запрос прав, и МОЖЕТ учить, ставим статус учителя
                    $status = 't';
                }

                $user->setAddon('status', $status);
                $user->setAddon('name', $data['name']);
                $user->setAddon('surname', $data['surname']);
                if ($user->email !== $data['email']) {
                    SensorCall::call('users', 'email_changed');
                    $user->email = $data['email'];
                }
                if ($user->addon('phone') !== $data['phone']) {
                    SensorCall::call('users', 'phone_changed');
                    $user->setAddon('phone', $data['phone']);
                }
                if (isset($data['new-password'])) {
                    $user->password = Hash::make($data['new-password_confirmation']);
                    $user->setAddon('password', $data['password']);
                    $messages['password-was-updated'] = ___('auth.password-updated');
                }
                $user->setAddon('country', $data['country']);
                $user->setAddon('city', $data['city']);
                $user->setAddon('birthday', new \DateTime($data['birthday']));
                $user->setAddon('natural_lang', $data['natural_lang']);
                $user->setAddon('speak_langs', $data['speak_langs']);
                $user->setAddon('lang', $data['lang']);

                $socials = getAvailableSocial();
                foreach ($socials as $social) {
                    if (isset($data["social-$social-delete"])) {
                        $user->setAddon("social_$social", null);
                    }
                }
                $user->save();

                if ($first) {
                    SensorCall::call('users', 'account_completed');
                }

                return redirect()->route('user.account')->withInput($messages);
            } else {
                return redirect()->route('user.account')->withErrors($validator);
            }
        }

        return view('account', [
            'mimes' => implode(',', Field::AVAILABLE_IMAGE_FORMATS),
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function profile()
    {
        $user = \Auth::user();
        // Если профиль заполнен, нельзя удалять поля
        $profileTeacherFull = ($user->addon('tc_profession') != null && $user->addon('tc_dialect') != null
            && $user->addon('tc_exp') != null && $user->addon('tc_education') != null
            && $user->addon('tc_about_me') != null && $user->addon('tc_style') != null
            && $user->addon('tc_prefer_level') != null && $user->addon('tc_lesson_type') != null
            && $user->addon('tc_diploma_url') != null && $user->addon('tc_video_url') != null);
        if (request()->isMethod('POST')) {
            $data = getRequest();

            $validator = [];

            // Если профиль заполнен, нельзя удалять поля
            $profileStudentFull = ($user->addon('st_dialect') != null && $user->addon('st_lang_level') != null
                && $user->addon('st_learn_targets') != null && $user->addon('st_about_me') != null && $user->addon('st_lesson_type') != null);
            if ($profileStudentFull) {
                $validator['st_dialect'] = 'required|array';
                $validator['st_lang_level'] = 'required|integer';
                $validator['st_learn_targets'] = 'required|string';
                $validator['st_about_me'] = 'required|string';
                $validator['st_lesson_type'] = 'required|array';
            }

            if ($profileTeacherFull) {
                $validator['tc_profession'] = 'required|string';
                $validator['tc_dialect'] = 'required|array';
                $validator['tc_exp'] = 'required|integer';
                $validator['tc_education'] = 'required|string';
                $validator['tc_about_me'] = 'required|string';
                $validator['tc_style'] = 'required|integer';
                $validator['tc_prefer_level'] = 'required|array';
                $validator['tc_lesson_type'] = 'required|array';
                $validator['tc_video_url'] = 'required|string';
            }

            $validator = \Validator::make($data, $validator);

            if (!$validator->fails()) {
                if (isset($data['tc_diploma_url'])) {
                    if ($data['tc_diploma_url'] instanceof UploadedFile) {
                        $diplomaFile = $data['tc_diploma_url'];
                    }
                    unset($data['tc_diploma_url']);
                }
                foreach ($data as $key => $val) {
                    if (is_array($val)) {
                        $tmp = [];
                        foreach ($val as $i => $item) {
                            $tmp[$item] = true;
                        }
                        $val = $tmp;
                    }
                    $user->setAddon($key, $val);
                }

                if (isset($diplomaFile)) {
                    $fileInfo = uploadFile($diplomaFile, 'diploma',
                        strtolower($user->login) . '-education-document.' . $diplomaFile->getClientOriginalExtension());
                    if ($fileInfo !== false) {
                        $user->setAddon('tc_diploma_url', $fileInfo['name']);
                    }
                }

                // Если профиль был незаполненнмы, а стал заполненным, сигналим об этом
                if (!$profileStudentFull && $user->addon('st_dialect') != null && $user->addon('st_lang_level') != null
                    && $user->addon('st_learn_targets') != null && $user->addon('st_about_me') != null && $user->addon('st_lesson_type') != null)
                { SensorCall::call('users', 'profile_student_completed'); }
                if (!$profileTeacherFull && $user->addon('tc_profession') != null && $user->addon('tc_dialect') != null
                    && $user->addon('tc_exp') != null && $user->addon('tc_education') != null
                    && $user->addon('tc_about_me') != null && $user->addon('tc_style') != null
                    && $user->addon('tc_prefer_level') != null && $user->addon('tc_lesson_type') != null
                    && $user->addon('tc_diploma_url') != null && $user->addon('tc_video_url') != null)
                { SensorCall::call('users', 'profile_teacher_completed'); }

                return redirect()->route('user.profile');
            }

            if ($user->hasStatus('s') && ($user->hasStatus('t') || $user->hasStatus('q')) &&
                (
                    $validator->errors()->has('st_dialect')
                    || $validator->errors()->has('st_lang_level')
                    || $validator->errors()->has('st_learn_targets')
                    || $validator->errors()->has('st_about_me')
                    || $validator->errors()->has('st_lesson_type')
                ) && (
                    $validator->errors()->has('tc_profession')
                    || $validator->errors()->has('tc_dialect')
                    || $validator->errors()->has('tc_exp')
                    || $validator->errors()->has('tc_education')
                    || $validator->errors()->has('tc_about_me')
                    || $validator->errors()->has('tc_style')
                    || $validator->errors()->has('tc_prefer_level')
                    || $validator->errors()->has('tc_lesson_type')
                    || $validator->errors()->has('tc_diploma_url')
                    || $validator->errors()->has('tc_video_url')
                )
            ) {
                Input::merge([
                    'errorHeader' => ___('profile.type.teacher.error.header'),
                    'errorMessage' => ___('profile.type.teacher.error.message'),
                ]);
                Input::flash();
            }
            return redirect()->back()->withErrors($validator)->withInput();
        }
        return view('profile', [
            'profileTeacherFull' => $profileTeacherFull,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function freeTime()
    {
        $user = \Auth::user();
        if ($user && strpos($user->addon('status'), 's') !== false) {
            $freeTime = $user->addon('st_free_time') ? round($user->addon('st_free_time') / 60) : 0;
            $users = User::whereIn('id', array_keys($user->addon('invitees_list') ?: []))->get();
            $referral = $user->addon('referral_token');
            if (empty($referral)) {
                $referral = md5($user->id . 'referral' . time());
                $user->setAddon('referral_token', $referral);
            }

            return view('free-time', [
                'freeTime' => $freeTime,
                'users' => $users,
                'referral' => $referral,
            ]);
        }

        abort(404);
    }

    public function sendReferralToEmails()
    {
        if (!($user = \Auth::user()) || strpos($user->addon('status'), 's') === false) {
            abort(404);
        }

        $referral = $user->addon('referral_token');
        $emails = request()->get('emails', '');
        $emails = explode(',', $emails);
        $validEmails = [];
        $invalidEmails = [];
        foreach ($emails as $email) {
            $email = trim($email);
            $validator = \Validator::make(['email' => $email], ['email' => 'required|email']);
            if (!$validator->fails()) {
                $validEmails[] = $email;
            } else {
                $invalidEmails[] = $email;
            }
        }
        $existingUsers = User::whereIn('email', $validEmails)->select('email')->get()->pluck('email', 'email')->toArray();
        foreach ($validEmails as $key => $email) {
            if (isset($existingUsers[$email])) {
                unset($validEmails[$key]);
            }
        }
        foreach ($validEmails as $email) {
            \Mail::to($email)->send(new JoinUs(route('referral-link', ['token' => $referral])));
        }

        $validEmails = count($validEmails) ? implode(', ', $validEmails) : null;
        $invalidEmails = count($invalidEmails) ? implode(', ', $invalidEmails) : null;
        $existingUsers = count($existingUsers) ? implode(', ', $existingUsers) : null;

        return redirect()->route('free-time')->withInput([
            'validEmails' => $validEmails,
            'invalidEmails' => $invalidEmails,
            'existingUsers' => $existingUsers,
        ]);
    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function watchList($userId)
    {
        if (request()->ajax()) {
            $user = \Auth::user();
            $result = false;
            if ($user && $user->getStatus()) {
                $watchList = $user->addon('watch_list') ?: [];
                $action = request()->get('action', 'add');
                if ($action == 'delete' && isset($watchList[$userId])) {
                    unset($watchList[$userId]);
                } else {
                    $watchList[$userId] = true;
                }
                $user->setAddon('watch_list', $watchList);
                $result = true;
            }

            return response()->json($result);
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function favorites()
    {
        if (($userId = request()->get('userId', null)) && ($user = \Auth::user()) && request()->ajax()) {
            $favorites = $user->addon('favorites');
            $result = null;
            if (empty($favorites)) {
                $favorites = [$userId => true];
                $result = true;
            } else {
                if (isset($favorites[$userId])) {
                    unset($favorites[$userId]);
                    $result = false;
                } else {
                    $favorites[$userId] = true;
                    $result = true;
                }
            }
            $user->setAddon('favorites', $favorites);

            return response()->json(['result' => $result, 'count' => $user->getFavoritesCount()]);
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function rating()
    {
        if (($data = getRequest()) && ($selfUser = \Auth::user()) && ($user = User::find($data['userId'])) && request()->ajax()) {
            $rating = $user->addon('rating');
            if (empty($rating)) {
                $rating = [
                    's' => [],
                    't' => [],
                ];
            }
            $rating[$selfUser->getStatus()][$selfUser->id] = $data['rating'];
            $user->setAddon('rating', $rating);

            return response()->json(['result' => true, 'rating' => $user->getRating()]);
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function changeProfile(string $status)
    {
        if (strlen($status) === 1 && \Auth::user()->hasStatus($status)) {
            \Session::put('active-status', $status);
        }

        return redirect()->back();
    }
}
