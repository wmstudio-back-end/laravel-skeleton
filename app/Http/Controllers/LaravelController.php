<?php

namespace App\Http\Controllers;

class LaravelController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $laCurrent = \PackageVersions\Versions::getShortVersion('laravel/framework');
//        $laLast = \Cache::get('la_version');
//        var_dump($laCurrent, $laLast);
//        exit;
        $links = [
            'Главная (если авторизован, то тут же поиск студентов/преподавателей)' => route('home'),
            'Вход' => route('login'),
            'Регистрация' => route('register'),
            'Тарифы' => route('tariffs'),
            'Поддержка' => route('faq'),
            'Аккаунт' => route('user.account'),
            'Профиль' => route('user.profile'),
            'Если студент => инфо о преподавателе (ссылка может не работать. это пример)' => route('user.teacher', ['login' => 'admin']),
            'Если преподаватель => инфо о студенте (ссылка может не работать. это пример)' => route('user.student', ['login' => 'admin']),
            'Тесты' => route('tests'),
            'Темы' => route('topics'),
            'Мои темы (для преподавателей)' => route('topics.my'),
            'Бронирование' => route('user.reservation'),
            'Счет для учителей' => route('payments.history'),
            'История платежей для студентов' => route('payments'),
            'Бесплатные минуты (для студента)' => route('free-time'),
            'Историю бесед решили не делать, т.к. она есть в сообщениях, а на страницах с историей бесед '
                . 'функционалом предусмотрено только открытие этих самых сообщений. т.е. по факту страницы не нужны.'
                => '#',
        ];

        $makeupLinks = [
            'index'                 => 'Главная',
            'enter'                 => 'Вход',
            'registration'          => 'Регистрация',
            'pageTariffs'           => 'Тарифы',
            'pageSupport'           => 'Поддержка',

            'pageAccount'           => 'Аккаунт',
            'pageProfile'           => 'Профиль',
            'pageTest'              => 'Тест',
            'pageResponse'          => 'Ответ на (Как войти в систему? Что делать, если я забыл пароль?)',

            'pageTeacher'           => 'Найти студента',
            'students'              => 'Найти преподавателя',

            'pageBooking-student'   => 'Забронировано 0 (у студента)',
            'pageBooking-teacher'   => 'Забронировано 0 (у учителей)',
            'booking'               => 'Бронирование (для студентов)',
            'teacherBooking'        => 'Бронирование (для учителей)',

            'topic'                 => 'Темы (выбор для студента)',
            'teacherTopics'         => 'Темы (выбор для учителей)',
            'pageMyTheme'           => 'Мои темы (создание/удаление тем)',

            'tariffs'               => 'Тарифы (для студентов)',

            'freeMin'               => 'Бесплатные минуты (для студента)',


            'pageHistory-teacher'   => 'История (нет бесед со студентами)',
            'pageHistory-student'   => 'История (нет бесед с преподователями)',
            'teacherHistory'        => 'История (беседы со студентами)',
            'history'               => 'История (беседы с преподавателями)',

            'teacherBalance'        => 'Счет (для учителей)',

            'teacherStudent-info'   => 'Информация о студентее',
            'studentTeacher-info'   => 'Информация о преподавателе',
        ];

        $readyPages = [
            'index' => true,
            'enter' => true,
            'registration' => true,
            'pageTariffs' => true,
            'tariffs' => true,
            'pageSupport' => true,
            'pageResponse' => true,
            'pageAccount' => true,
            'pageProfile' => true,
            'pageTeacher' => true,
            'students' => true,
            'teacherStudent-info' => true,
            'studentTeacher-info' => true,
            'pageTest' => true,
            'topic' => true,
            'teacherTopics' => true,
            'pageMyTheme' => true,
            'pageBooking-teacher' => true,
            'teacherBooking' => true,
            'pageBooking-student' => true,
            'booking' => true,
            'teacherBalance' => true,
            'freeMin' => true,
            'pageHistory-teacher' => true,
            'pageHistory-student' => true,
            'teacherHistory' => true,
            'history' => true,
        ];

        $excluded = [
            'footer',
            'header',
            'footer-student',
            'footer-teacher',
            'modal',
            'scripts',
        ];

        $makeupDir = base_path('makeup/');
        $files = glob($makeupDir . '*.*');
        $lastDate = new \DateTime('1/1/70');
        foreach ($files as $key => $fName) {
            $name = $index = pathinfo($fName)['filename'];
            if (in_array($name, $excluded)) {
                continue;
            } else {
                if (isset($makeupLinks[$name])) {
                    $name = $makeupLinks[$name];
                }
                $date = new \DateTime(date("d.m.Y H:i:s", filemtime($fName)));
                if ($date > $lastDate) {
                    $lastDate = $date;
                }
                $makeupLinks[$index] = [
                    'name' => $name,
                    'url' => '/' . pathinfo($makeupDir)['basename'] . '/' . pathinfo($fName)['basename'],
                    'date' => $date,
                    'ready' => isset($readyPages[$index]),
                ];
            }
        }

        return view('laravel.stat', [
            'makeupLinks' => $makeupLinks,
            'lastDate' => $lastDate,
            'links' => $links,
        ]);
    }

    public function videoChat()
    {
        $users = \App\User::all();

        return view('laravel.chat', [
            'users' => $users,
        ]);
    }
}
