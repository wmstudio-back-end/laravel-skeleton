<?php

namespace App\Http\Controllers;

use Modules\FAQ\Entities\FAQ;

class FAQController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = [];
        foreach (FAQ::where('active', true)->groupBy('group')->select('group')->get()->pluck('group', 'group') as $group) {
            $groups[$group] = FAQ::where([
                'active' => true,
                'group' => $group,
            ])->get();
        }

        if (!count($groups)) {
            abort(404);
        }

        return view('questions', [
            'groups' => $groups,
        ]);
    }

    public function question(string $group, string $alias)
    {
        $faq = FAQ::where([
            'active' => true,
            'group' => $group,
            'alias' => $alias,
        ])->first();

        if (!$faq) {
            abort(404);
        }

        $menu = getMenu('dbMenu');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        if ($active = $menu->findBy('uri', route('faq', [], false))) {
            $active->setActive();
        }
//        $menu->lastLevel = 'Вопрос';

        return view('question', [
            'question' => $faq,
        ]);
    }

    public function useful(string $group, string $alias)
    {
        $faq = FAQ::where([
            'active' => true,
            'group' => $group,
            'alias' => $alias,
        ])->first();

        $data = getRequest();
        if (!$faq || !isset($data['useful'])) {
            abort(404);
        }

        $useful = json_decode($faq->useful, true);
        if ((bool)$data['useful']) {
            $useful['yes']++;
        } else {
            $useful['no']++;
        }
        $faq->useful = json_encode($useful);
        $faq->save();
        \Session::put('faq.' . $group . '.' . $alias, true);
        return redirect()->route('faq.question', ['group' => $group, 'alias' => $alias]);
    }
}
