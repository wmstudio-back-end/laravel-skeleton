<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Reliese\Database\Eloquent\Model as Entity;
use Illuminate\Http\RedirectResponse as Redirect;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $transManager;

    protected $usersPerPage = 8;
    protected $reviewsCount = 10;
    protected $adminEntitiesPerPage = 20;

    public function __construct()
    {
        $this->transManager = app('translation-manager');
    }

    protected function formatValidationErrors(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $status = 422;
        return [
            "message" => $status . " error",
            "errors" => [
                "message" => $validator->getMessageBag()->first(),
                "info" => [$validator->getMessageBag()->keys()[0]],
            ],
            "status_code" => $status
        ];
    }

    public static function writeRequired()
    {
        return [
            'add',
            'edit',
            'delete',
        ];
    }

    /**
     * Where to redirect users after login/register.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected function redirectTo()
    {
        $redirectTo = \Session::get('login_previous_url', null);
        if (!$redirectTo) {
            $redirectTo = $this->redirectTo;
        } else {
            if (
                $redirectTo === route('tariffs')
                && ($tariff = \Session::get('selected-tariff', null))
                && ($user = \Auth::user())
                && $user->hasStatus('s')
            ) {
                $redirectTo = app('\App\Http\Controllers\TariffsController')->continueSubscribe($user, $tariff);
            } else {
                \Session::forget('login_previous_url');
                $redirectOpts = parse_url($redirectTo);
                if (isset($redirectOpts['path'])
                    && $redirectOpts['path'] == '/' . app('router')->getCurrentRoute()->uri()) {
                    $redirectTo = $this->redirectTo;
                }
            }
        }

        return $redirectTo;
    }

    protected function sorting(string $entity, string $redirect = null)
    {
        if (is_subclass_of($entity, Entity::class)) {
            $sortable = request()->get('sortable', null);
            if (request()->ajax() && is_array($sortable) && count($sortable)) {
                try {
                    $error = false;
                    $msgHeader = 'Успешно обновлено!';
                    $message = 'Данные отсортированы.';

                    $entities = $entity::whereIn('id', $sortable)->get()->keyBy('id');
                    if ($entities && $entities->count() == count($sortable)) {
                        $entity::unguard();
                        foreach ($sortable as $index => $key) {
                            $entities[$key]->weight = $index + 1;
                            $entities[$key]->save();
                        }
                        $entity::reguard();
                    } else {
                        throw new \Exception('Группы тем не найдены');
                    }
                } catch (\Exception $e) {
                    $error = true;
                    $msgHeader = 'Ошибка!';
                    $message = $e->getMessage();
                }

                return response()->json([
                    'error' => $error, 'msgHeader' => $msgHeader, 'message' => $message,
                ]);
            } elseif (
                !request()->ajax()
                && ($entityObj = $entity::find(request()->get('up', null)))
                && ($entityPrevObj = $entity::where('weight', '<', $entityObj->weight)->orderByDesc('weight')->first())
            ) {
                $weight = $entityObj->weight;
                $entityObj->weight = $entityPrevObj->weight;
                $entityPrevObj->weight = $weight;
                $entityObj->save();
                $entityPrevObj->save();

                if ($redirect && (is_string($redirect) || $redirect instanceof Redirect)) {
                    if (is_string($redirect)) {
                        if (\Route::has($redirect)) {
                            return redirect()->route($redirect);
                        } else {
                            return redirect()->to($redirect);
                        }
                    } else {
                        return $redirect;
                    }
                } else {
                    return redirect()->back();
                }
            }

            abort(404);
        } else {
            throw new \InvalidArgumentException("\"$entity\" must be instanceof \"" . Entity::class . '"');
        }
    }
}
