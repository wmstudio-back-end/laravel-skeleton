<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Modules\Users\Entities\UserPermission as Permissions;

class PermissionAccess
{
    public function checkPermissions(Route $route)
    {
        if ($user = \Auth::user()) {
            $write = [];
            if (method_exists($route->getController(), 'writeRequired')) {
                $write = $route->getController()::writeRequired();
            }
            $write = in_array(explode('@', $route->action['controller'])[1], $write);
            $userPermissions = $user->getPermissions($route);
            if ($write) {
                return !($userPermissions < Permissions::P_WRITE);
            }

            return (bool)$userPermissions;
        } else {
            return false;
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!getPermissions()) {
            $message = ___('messages.permissions-denied');
            abort(404, $message);
        }

        return $next($request);
    }
}
