<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class AuthType extends PermissionAccess
{
    public function checkPermissions(Route $route)
    {
        if ($user = \Auth::user()) {
            if ($user->getStatus()) {
                $needStatus = isset($route->action['status']) ? $route->action['status'] : null;
                $needActiveStatus = isset($route->action['need-active-status']) ? $route->action['need-active-status'] : false;
                if ($needStatus) {
                    if ($needActiveStatus && strlen($needStatus) === 1) {
                        return $user->getStatus() === $needStatus;
                    } else {
                        $result = true;
                        $status = $user->addon('status');
                        foreach (str_split($needStatus) as $val) {
                            if (strpos($status, $val) === false) {
                                $result = false;
                                break;
                            }
                        }
                        return $result;
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $actions = app('router')->getCurrentRoute()->action;
        if (isset($actions['need-active-status']) && $actions['need-active-status']) {
            $user = \Auth::user();

            $needStatus = isset($actions['status']) ? $actions['status'] : null;
            $status = $user->getStatus();
            if ($needStatus !== $status) {
                $result = true;
                $status = $user->addon('status');
                foreach (str_split($needStatus) as $val) {
                    if (strpos($status, $val) === false) {
                        $result = false;
                        break;
                    }
                }
                if ($result) {
                    return redirect()->route('home');
                }
            }
        }

        return $next($request);
    }
}
