<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Routing\Route;

class Auth extends Authenticate
{
    public function checkPermissions(Route $route)
    {
        return \Auth::check();
    }
}
