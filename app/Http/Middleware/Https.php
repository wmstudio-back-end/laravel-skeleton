<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Https
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->secure() && config('app.ssl-enabled', true)) {
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
