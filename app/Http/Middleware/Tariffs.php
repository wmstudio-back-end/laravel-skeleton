<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Tariffs
{
    public function checkPermissions($route)
    {
        if (($user = \Auth::user()) && $user->getStatus() !== 's') {
            return false;
        }

        return true;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$this->checkPermissions(null)) {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
