<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Guest
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!\Auth::user()) {
            if (!\Session::get('guest', null)) {
                \Session::put('guest', md5(date_format(new \DateTime(), 'd.m.Y H:i:s (v - u)')));
            }
        } else {
            \Session::forget('guest');
        }

        return $next($request);
    }
}
