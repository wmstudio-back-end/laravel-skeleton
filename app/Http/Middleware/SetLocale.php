<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!($lang = \Session::get('lang', null))) {
            if ($user = \Auth::user()) {
                $lang = $user->addon('lang') ?: config('app.locale');
            } else {
                $lang = config('app.locale');
            }
            \Session::put('lang', $lang);
        }
        \App::setLocale($lang);

        return $next($request);
    }
}
