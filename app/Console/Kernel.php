<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Settings\Entities\Setting;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $json = file_get_contents('https://packagist.org/packages/laravel/framework.json');
            $maxVersion = 'v';
            $data = json_decode($json, true);
            $res = [];
            foreach ($data['package']['versions'] as $index => $version) {
                if (strpos($index, 'dev') === false && strpos($version['version_normalized'], 'dev') === false) {
                    assignArrayByPath($res, preg_replace('/[^0-9\.]/', '', $index));
                }
            }
            $getMax = function($arr) {
                if ($count = count($arr)) {
                    $arr = array_keys($arr);
                    $max = $arr[0];
                    for ($i = 1; $i < $count; $i++) {
                        if ($max < $arr[$i]) {
                            $max = $arr[$i];
                        }
                    }

                    return $max;
                }

                return null;
            };

            while (is_array($res)) {
                $max = $getMax($res);
                $maxVersion .= $max . '.';
                $res = $res[$max];
            }
            $maxVersion = substr($maxVersion, 0, -1);
            if ($maxVersion == \Cache::get('la_version')) {
                \Cache::delete('la_version');
            } else {
                \Cache::forever('la_version', ['version' => $maxVersion, 'date' => new \DateTime()]);
            }
        })->weekly();

        $schedule->call(function () {
            \Cache::forever('ya_token_update', (new \DateTime())->format('d.m.Y'));
            $yaSettings = Setting::whereIn('name', [
                'yandex_oauth_id',
                'yandex_oauth_secret',
                'yandex_metrika_token',
            ])->get()->keyBy('name');
            $tokens = json_decode($yaSettings['yandex_metrika_token']->value, true);

            if ((new \DateTime($tokens['expires_in']))->modify('-1 month') < new \DateTime()) {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://oauth.yandex.ru/token",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "grant_type=refresh_token&refresh_token=" . $tokens['refresh_token'],
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Basic " . base64_encode($yaSettings['yandex_oauth_id']->value . ':' . $yaSettings['yandex_oauth_secret']->value),
                        "cache-control: no-cache",
                        "content-type: application/x-www-form-urlencoded",
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if (!$err) {
                    $data = json_decode($response, true);
                    if (isset($data['token_type'])) {
                        unset($data['token_type']);
                    }
                    $data['expires_in'] = date_format(new \DateTime('+' . $data['expires_in'] . ' seconds'), 'd.m.Y H:i:s');
                    $yaSettings['yandex_metrika_token']->value = json_encode($data);
                    $yaSettings['yandex_metrika_token']->save();
                }
            }
        })->dailyAt('14:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
