<?php

namespace App\Console\Commands;

use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Console\Command;

class translations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:translations '
        . '{fName : Output or input file path} '
        . '{--empty : Export only empty translations} '
        . '{--import : Import instead of export} '
        . '{--delimiter= : Cell delimiter}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import/Export translations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    public function handle()
    {
        $echo = function($text, $style = 'info'){
            $styled = $text;
            if ($style) {
                if (strpos($style, '=') !== false) {
                    $styled = $style ? "<$style>$text</>" : $text;
                } else {
                    $styled = $style ? "<$style>$text</$style>" : $text;
                }

            }

            $this->output->write($styled);
        };
        $getValue = function($value){
            $value = preg_replace("/(\<(\/p|\/div|br)\>)[\ \n]*/", "$1\n", $value);
            if ($value[strlen($value) - 1] === "\n") {
                $value = substr($value, 0, -1);
            }
            return $value;
        };

        $fName = $this->argument('fName');
        $import = !!($this->option('import') ?: false);
        $delimiter = $this->option('delimiter') ?: ';';
        $empty = $this->option('empty') ?: false;

        $locales = getLocales();
        $countFields = count($locales) + 2;
        if ($import) {
            $echo("Обработка `.csv` файла...\r");

            $strings = [];
            $where = [];
            $errors = '';
            $file = fopen($fName, 'r');
            $csvCount = 0;

            $keys = $str = fgetcsv($file, 0, $delimiter);
            for($i = 0; $i < strlen($keys[0]); $i++) {
                if (preg_match('/[a-zA-Z]/', $keys[0][$i])) {
                    break;
                }
            }
            $keys[0] = substr($keys[0], $i);

            if (count($keys) != $countFields) {
                $this->error('The number of columns should be ' . $countFields);
                return;
            }
            $line = 2;
            while($str = fgetcsv($file, 0, $delimiter)) {
                $tmp = [];
                $group = null;
                $key = null;
                for($i = 0; $i < $countFields; $i++) {
                    if ($keys[$i] == 'group' || $keys[$i] == 'key') {
                        ${$keys[$i]} = isset($str[$i]) && strlen($str[$i]) ? $getValue($str[$i]) : null;
                    } else {
                        if (isset($str[$i]) && strlen($str[$i])) {
                            $tmp[$keys[$i]] = $getValue($str[$i]);
                            $csvCount++;
                        } else {
                            $tmp[$keys[$i]] = null;
                        }
                    }
                }
                if ($group && $key) {
                    if (!array_key_exists($group, $strings)) {
                        $strings[$group] = [];
                    }
                    $strings[$group][$key] = $tmp;
                    $where[] = [
                        'group' => $group,
                        'key' => $key,
                    ];
                } else {
                    $errors .= "  Не найден ключ и/или группа в строке $line\n";
                }
                $line++;
            }
            $echo("Обработка `.csv` файла... Готово\n");
            $echo("  " . numEnding($csvCount, ['Найдена', 'Найдены', 'Найдено'])
                . " $csvCount " . numEnding($csvCount, ['запись', 'записи', 'записей'])
                . ".\n", 'fg=yellow');
            if (strlen($errors)) {
                $echo($errors, 'error');
            }

            $echo("Анализ базы данных.\n");
            $translations = Translation::where(array_shift($where));
            foreach ($where as $condition) {
                $translations->orWhere(function($query) use($condition) {
                    foreach ($condition as $key => $val) {
                        $query->where($key, $val);
                    }
                });
            }
            $translations = $translations->get();
            $count = $translations->count();
            $deleted = 0;
            if ($translations->count()) {
                foreach ($translations as $key => $translation) {
                    /* @var $translation Translation */
                    if (
                        array_key_exists($translation->group, $strings)
                        && array_key_exists($translation->key, $strings[$translation->group])
                        && array_key_exists($translation->locale, $strings[$translation->group][$translation->key])
                    ) {
                        if ($translation->value === $strings[$translation->group][$translation->key][$translation->locale]) {
                            $translations->forget($key);
                        } else {
                            if (
                                empty($strings[$translation->group][$translation->key][$translation->locale])
                                || !strlen($strings[$translation->group][$translation->key][$translation->locale])
                                || $strings[$translation->group][$translation->key][$translation->locale] == null
                            ) {
                                $translation->delete();
                                $deleted++;
                            } else {
                                $translation->value = $strings[$translation->group][$translation->key][$translation->locale];
                                $translation->status = Translation::STATUS_CHANGED;
                                $translation->save();
                            }
                        }
                        unset($strings[$translation->group][$translation->key][$translation->locale]);
                        if (!count($strings[$translation->group][$translation->key])) {
                            unset($strings[$translation->group][$translation->key]);
                            if (!count($strings[$translation->group])) {
                                unset($strings[$translation->group]);
                            }
                        }
                    } else {
                        $translations->forget($key);
                    }
                    $echo("  Проанализировано " . ($key + 1) . " из " . $count . " записей.\r", 'fg=yellow');
                }
                $echo("\n");
            } else {
                $echo("  База данных пуста\n", 'fg=yellow');
            }

            $insert = [];
            $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
            foreach ($strings as $group => $keys) {
                foreach ($keys as $key => $values) {
                    foreach ($values as $lang => $value) {
                        if ($value) {
                            $insert[] = [
                                'status' => Translation::STATUS_CHANGED,
                                'locale' => $lang,
                                'group' => $group,
                                'key' => $key,
                                'value' => $value,
                                'created_at' => $timeStamp,
                                'updated_at' => $timeStamp,
                            ];
                        }
                    }
                }
            }
            if (count($insert)) {
                Translation::insert($insert);
            }

            if ($translations->count() || count($insert)) {
                $echo("Результат анализа:\n");
                if ($translations->count() - $deleted) {
                    $echo('  ' . ($translations->count() - $deleted) . " "
                        . numEnding($translations->count() - $deleted, ['запись изменена', 'записи изменены', 'записей изменено'])
                        . "\n", 'fg=blue');
                }
                if ($deleted) {
                    $echo("  $deleted "
                        . numEnding($deleted, ['запись удалена', 'записи удалены', 'записей удалено'])
                        . "\n", 'fg=red');
                }

                if (count($insert)) {
                    $echo('  ' . count($insert) . " "
                        . numEnding(count($insert), ['новая запись', 'новых записи', 'новых записей'])
                        . "\n");
                }
            } else {
                $echo("Изменений не найдено.\n", 'fg=cyan');
            }
        } else {
            $echo("Формирование таблицы переводов...\r");
            $translations = Translation::all();
            $result = [];
            foreach ($translations as $translation) {
                if (isset($locales[$translation->locale])) {
                    $value = $translation->value;
                    if ($value == null || $value == '<p><br></p>') {
                        $value = '';
                    } else {
                        $value = $getValue($value);
                    }
                    if (!isset($result[$translation->group . '.' . $translation->key])) {
                        $tmp = [
                            'group' => $translation->group,
                            'key' => $translation->key,
                        ];
                        foreach ($locales as $locale => $name) {
                            $tmp[$locale] = '';
                        }
                        $tmp[$translation->locale] = $value;
                        $result[$translation->group . '.' . $translation->key] = $tmp;
                    } else {
                        $result[$translation->group . '.' . $translation->key][$translation->locale] = $value;
                    }
                }
            }
            if ($empty) {
                foreach ($result as $key => $values) {
                    $empty = false;
                    foreach ($locales as $locale => $name) {
                        if (empty($values[$locale])) {
                            $empty = true;
                            break;
                        }
                    }
                    if (!$empty) {
                        unset($result[$key]);
                    }
                }
            }
            $echo("Формирование таблицы переводов... Готово.\n");
            $counter = 0;
            $count = count($result);
            $file = fopen($fName, 'w');
            fwrite($file, hex2bin('EFBBBF'));
            $header = ['group', 'key'];
            foreach ($locales as $locale => $name) {
                $header[] = $locale;
            }
            fputcsv($file, $header, $delimiter);
            foreach ($result as $value) {
                fputcsv($file, $value, $delimiter);
                $echo("  Обработано $counter записей из $count...\r", 'fg=yellow');
                $counter++;
            }
            fclose($file);
            $echo("  Обработано $counter записей из $count... ", 'fg=yellow');
            $echo("Готово.\n");
        }
    }
}
