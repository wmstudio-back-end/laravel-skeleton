<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class clearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:clearCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear custom cache keys';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle()
    {
        $keys = [
            'menus',
            \Modules\Settings\Constants\CurrencyList::CACHE_NAME,
        ];
        if (count($keys)) {
            $this->info('Keys was deleted:');
            foreach ($keys as $key) {
                \Cache::delete($key);
                $this->info('  <fg=yellow>' . $key . '</>');
            }
        }
    }
}
