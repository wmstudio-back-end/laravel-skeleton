<?php

namespace Modules\Fields\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Field
 * 
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $type
 * 
 * @property \Illuminate\Database\Eloquent\Collection $fields_values
 */
class Field extends Eloquent
{
    const AVAILABLE_IMAGE_FORMATS = [
        'bmp',
        'jpeg',
        'png',
    ];

    public $timestamps = false;

	protected $fillable = [
		'name',
		'alias',
		'description',
		'type'
	];

	public function fields_values()
	{
		return $this->hasMany(\Modules\Fields\Entities\FieldsValue::class, 'field_id');
	}
}
