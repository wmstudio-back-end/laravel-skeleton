<?php

namespace Modules\Fields\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FieldsValue
 *
 * @property int $id
 * @property int $field_id
 * @property string $alias
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Field $field
 */
class FieldsValue extends Eloquent
{
    protected $casts = ['id' => 'int', 'field_id' => 'int'];

    protected $fillable = ['field_id', 'alias', 'value'];

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }
}
