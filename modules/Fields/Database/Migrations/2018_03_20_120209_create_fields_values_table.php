<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldsValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fields_values', function(Blueprint $table)
		{
            $table->integer('id', true);
			$table->integer('field_id');
			$table->string('alias');
			$table->text('value');
			$table->unique(['field_id','alias'], 'field_value');
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fields_values');
	}

}
