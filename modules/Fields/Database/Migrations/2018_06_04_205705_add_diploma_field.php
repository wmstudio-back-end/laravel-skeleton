<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;

class AddDiplomaField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Field::create([
            'name' => 'Диплом',
            'alias' => 'diploma',
            'type' => 'file',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $field = Field::where('alias', 'diploma')->first();
        $values = FieldsValue::where('field_id', $field->id)->get();
        FieldsValue::unguard();
        foreach ($values as $value) {
            /* @var $value FieldsValue */
            $value->delete();
        }
        FieldsValue::reguard();
        $field->delete();
    }
}
