<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFieldsValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fields_values', function(Blueprint $table)
		{
			$table->foreign('field_id', 'fields_values_ibfk_1')->references('id')->on('fields')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fields_values', function(Blueprint $table)
		{
			$table->dropForeign('fields_values_ibfk_1');
		});
	}

}
