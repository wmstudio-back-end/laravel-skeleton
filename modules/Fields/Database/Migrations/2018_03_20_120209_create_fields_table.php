<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fields', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('alias')->unique('alias');
			$table->text('description')->nullable();
			$table->string('type');
		});

        Artisan::call('db:seed', array('--class' => \Modules\Fields\Database\Seeders\FieldsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fields');
	}

}
