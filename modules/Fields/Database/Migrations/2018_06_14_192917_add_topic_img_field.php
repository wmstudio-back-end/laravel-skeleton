<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;

class AddTopicImgField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Field::create([
            'name' => 'Картинка темы',
            'alias' => 'topic-img',
            'type' => 'image',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $field = Field::where('alias', 'topic-img')->first();
        $values = FieldsValue::where('field_id', $field->id)->get();
        FieldsValue::unguard();
        foreach ($values as $value) {
            /* @var $value FieldsValue */
            $value->delete();
        }
        FieldsValue::reguard();
        $field->delete();
    }
}
