<?php

namespace Modules\Fields\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Fields\Entities\Field;

class FieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fields = [
            [
                'id'          => 1,
                'name'        => 'Заголовок страницы (title)',
                'alias'       => 'seo_title',
                'type'        => 'input',
            ],
            [
                'id'          => 2,
                'name'        => 'Ключевые слова (keywords)',
                'alias'       => 'seo_keywords',
                'type'        => 'input',
            ],
            [
                'id'          => 3,
                'name'        => 'Описание страницы (description)',
                'alias'       => 'seo_description',
                'type'        => 'input',
            ],
            [
                'id'          => 4,
                'name'        => 'Файл',
                'alias'       => 'file',
                'type'        => 'file',
            ],
            [
                'id'          => 5,
                'name'        => 'Изображение',
                'alias'       => 'image',
                'type'        => 'image',
            ],
            [
                'id'          => 6,
                'name'        => 'Галлерея',
                'alias'       => 'gallery',
                'type'        => 'gallery',
            ],
            [
                'id'          => 7,
                'name'        => 'Схема проезда',
                'alias'       => 'map',
                'type'        => 'map',
            ],
            [
                'id'          => 8,
                'name'        => 'Логотип',
                'alias'       => 'logo',
                'type'        => 'image',
            ],
            [
                'id'          => 9,
                'name'        => 'Название формы',
                'alias'       => 'form_name',
                'type'        => 'input',
            ],
            [
                'id'          => 10,
                'name'        => 'Контент формы',
                'alias'       => 'form',
                'type'        => 'input',
            ],
        ];

        Field::unguard();
        Field::insert($fields);
        Field::reguard();
    }
}
