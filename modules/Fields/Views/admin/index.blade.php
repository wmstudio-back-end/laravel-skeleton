@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h3>
                        <b>Module</b> : {{ ucfirst(request()->route()->action['module']) }}
                    </h3>
                </div>
                <div class="card-body">
                    <h5>
                        <b>Controller</b> : {{ substr(
                            request()->route()->getActionName(),
                            strpos(request()->route()->getActionName(), 'Controllers\\') + strlen('Controllers\\'),
                            strlen(request()->route()->getActionName())
                                - strlen(substr(request()->route()->getActionName(), strpos(request()->route()->getActionName(), '@')))
                                - strpos(request()->route()->getActionName(), 'Controllers\\')
                                - strlen('Controllers\\')
                        ) }}
                    </h5>
                </div>
                <div class="card-footer">
                    <b>Action</b> : {{ substr(request()->route()->getActionName(), strpos(request()->route()->getActionName(), '@') + 1) }}
                </div>
            </div>
        </div>
    </div>
@endsection