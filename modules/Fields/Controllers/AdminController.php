<?php

namespace Modules\Fields\Controllers;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('fields::admin.index');
    }
}
