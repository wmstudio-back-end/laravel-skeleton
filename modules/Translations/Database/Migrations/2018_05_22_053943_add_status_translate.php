<?php

use Illuminate\Database\Migrations\Migration;
use Barryvdh\TranslationManager\Models\Translation;

class AddStatusTranslate extends Migration
{
    private $statusNames = [
        'student' => [
            'en' => 'Student',
            'ru' => 'Студент',
        ],
        'teacher' => [
            'en' => 'Teacher',
            'ru' => 'Преподаватель',

        ],
        'request_teach_permissions' => [
            'en' => 'Request teach permissions',
            'ru' => 'Запрос прав преподавания',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($this->statusNames as $status => $langs) {
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'en',
                'group'      => 'user',
                'key'        => 'status.' . $status,
                'value'      => $langs['en'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'ru',
                'group'      => 'user',
                'key'        => 'status.' . $status,
                'value'      => $langs['ru'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
        }
        Translation::unguard();
        Translation::insert($insert);
        Translation::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $deleteKeys = [];
        foreach ($this->statusNames as $status => $langs) {
            $deleteKeys[] = 'status.' . $status;
        }
        $translations = Translation::where('group', 'user')->whereIn('key', $deleteKeys)->get();
        Translation::unguard();
        foreach ($translations as $key) {
            $key->delete();
        }
        Translation::reguard();
    }
}
