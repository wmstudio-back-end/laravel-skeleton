<?php

use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Database\Migrations\Migration;

class RenameCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::where([
            'group' => 'dictionary',
            'key' => 'currency.rur'
        ])->update([
            'status' => Translation::STATUS_CHANGED,
            'key' => 'currency.rub'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Translation::where([
            'group' => 'dictionary',
            'key' => 'currency.rub'
        ])->update([
            'status' => Translation::STATUS_CHANGED,
            'key' => 'currency.rur'
        ]);
    }
}
