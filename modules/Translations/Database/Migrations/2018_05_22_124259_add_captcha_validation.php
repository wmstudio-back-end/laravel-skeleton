<?php

use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Database\Migrations\Migration;

class AddCaptchaValidation extends Migration
{
    private $captcha = [
        'custom.g-recaptcha-response.required' => [
            'en' => 'Please verify that you are not a robot.',
            'ru' => 'Пожалуйста, подтвердите, что вы не робот.',
        ],
        'custom.g-recaptcha-response.captcha' => [
            'en' => 'Captcha error! try again later or contact site admin.',
            'ru' => 'Captcha не пройдена! повторите попытку позже или свяжитесь с администратором сайта.',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($this->captcha as $key => $langs) {
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'en',
                'group'      => 'validation',
                'key'        => $key,
                'value'      => $langs['en'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'ru',
                'group'      => 'validation',
                'key'        => $key,
                'value'      => $langs['ru'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
        }
        Translation::unguard();
        Translation::insert($insert);
        Translation::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $translations = Translation::where('group', 'validation')
            ->whereIn('key', array_keys($this->captcha))->get();
        Translation::unguard();
        foreach ($translations as $key) {
            $key->delete();
        }
        Translation::reguard();
    }
}
