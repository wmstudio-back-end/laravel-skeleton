<?php

namespace Modules\Translations\Constants;

class Languages
{
    public const LANGUAGES = [
        "az" => [
            "eng_name" => "Azerbaijani",
            "vk_id" => 57,
            "name" => "Azərbaycan dili"
        ],
        "id" => [
            "eng_name" => "Indonesian",
            "vk_id" => 69,
            "name" => "Bahasa Indonesia"
        ],
        "bs" => [
            "eng_name" => "Bosnian",
            "vk_id" => 72,
            "name" => "Bosanski"
        ],
        "da" => [
            "eng_name" => "Danish",
            "vk_id" => 64,
            "name" => "Dansk"
        ],
        "de" => [
            "eng_name" => "German",
            "vk_id" => 6,
            "name" => "Deutsch"
        ],
        "et" => [
            "eng_name" => "Estonian",
            "vk_id" => 22,
            "name" => "Eesti"
        ],
        "en" => [
            "eng_name" => "English",
            "vk_id" => 3,
            "name" => "English"
        ],
        "es" => [
            "eng_name" => "Spanish",
            "vk_id" => 4,
            "name" => "Español"
        ],
        "eo" => [
            "eng_name" => "Esperanto",
            "vk_id" => 555,
            "name" => "Esperanto"
        ],
        "fr" => [
            "eng_name" => "French",
            "vk_id" => 16,
            "name" => "Français"
        ],
        "hr" => [
            "eng_name" => "Croatian",
            "vk_id" => 9,
            "name" => "Hrvatski"
        ],
        "it" => [
            "eng_name" => "Italian",
            "vk_id" => 7,
            "name" => "Italiano"
        ],
        "kr" => [
            "eng_name" => "Karelian",
            "vk_id" => 379,
            "name" => "Karjalan kieli"
        ],
        "sw" => [
            "eng_name" => "Swahili",
            "vk_id" => 95,
            "name" => "Kiswahili"
        ],
        "lv" => [
            "eng_name" => "Latvian",
            "vk_id" => 56,
            "name" => "Latviešu"
        ],
        "lt" => [
            "eng_name" => "Lithuanian",
            "vk_id" => 19,
            "name" => "Lietuvių"
        ],
        "la" => [
            "eng_name" => "Latin",
            "vk_id" => 666,
            "name" => "Lingua Latina"
        ],
        "hu" => [
            "eng_name" => "Hungarian",
            "vk_id" => 10,
            "name" => "Magyar"
        ],
        "dv" => [
            "eng_name" => "Dutch",
            "vk_id" => 61,
            "name" => "Nederlands"
        ],
        "no" => [
            "eng_name" => "Norwegian",
            "vk_id" => 55,
            "name" => "Norsk"
        ],
        "uz" => [
            "eng_name" => "Uzbek",
            "vk_id" => 65,
            "name" => "O‘zbek"
        ],
        "pl" => [
            "eng_name" => "Polish",
            "vk_id" => 15,
            "name" => "Polski"
        ],
        "pt" => [
            "eng_name" => "Portuguese",
            "vk_id" => 12,
            "name" => "Português"
        ],
        "ro" => [
            "eng_name" => "Romanian",
            "vk_id" => 54,
            "name" => "Română"
        ],
        "sq" => [
            "eng_name" => "Albanian",
            "vk_id" => 59,
            "name" => "Shqip"
        ],
        "sl" => [
            "eng_name" => "Slovenian",
            "vk_id" => 71,
            "name" => "Slovenščina"
        ],
        "fi" => [
            "eng_name" => "Finnish",
            "vk_id" => 5,
            "name" => "Suomi"
        ],
        "sv" => [
            "eng_name" => "Swedish",
            "vk_id" => 60,
            "name" => "Svenska"
        ],
        "tl" => [
            "eng_name" => "Tagalog",
            "vk_id" => 79,
            "name" => "Tagalog"
        ],
        "vi" => [
            "eng_name" => "Vietnamese",
            "vk_id" => 75,
            "name" => "Tiếng Việt"
        ],
        "tk" => [
            "eng_name" => "Turkmen",
            "vk_id" => 62,
            "name" => "Türkmen"
        ],
        "tr" => [
            "eng_name" => "Turkish",
            "vk_id" => 82,
            "name" => "Türkçe"
        ],
        "cs" => [
            "eng_name" => "Czech",
            "vk_id" => 21,
            "name" => "Čeština"
        ],
        "el" => [
            "eng_name" => "Greek",
            "vk_id" => 14,
            "name" => "Ελληνικά"
        ],
        "ad" => [
            "eng_name" => "Adyghe",
            "vk_id" => 106,
            "name" => "Адыгабзэ"
        ],
        "ba" => [
            "eng_name" => "Bashkir",
            "vk_id" => 51,
            "name" => "Башҡортса"
        ],
        "be" => [
            "eng_name" => "Belarusian",
            "vk_id" => 2,
            "name" => "Беларуская"
        ],
        "bg" => [
            "eng_name" => "Bulgarian",
            "vk_id" => 8,
            "name" => "Български"
        ],
        "os" => [
            "eng_name" => "Ossetian",
            "vk_id" => 91,
            "name" => "Ирон"
        ],
        "ky" => [
            "eng_name" => "Kirghiz",
            "vk_id" => 87,
            "name" => "Кыргыз тили"
        ],
        "mn" => [
            "eng_name" => "Mongolian",
            "vk_id" => 80,
            "name" => "Монгол"
        ],
        "ru" => [
            "eng_name" => "Russian",
            "vk_id" => 0,
            "name" => "Русский"
        ],
        "tt" => [
            "eng_name" => "Tatar",
            "vk_id" => 50,
            "name" => "Татарча"
        ],
        "tg" => [
            "eng_name" => "Tajik",
            "vk_id" => 70,
            "name" => "Тоҷикӣ"
        ],
        "sr" => [
            "eng_name" => "Serbian",
            "vk_id" => 11,
            "name" => "Српски"
        ],
        "cv" => [
            "eng_name" => "Chuvash",
            "vk_id" => 52,
            "name" => "Чăвашла"
        ],
        "wo" => [
            "eng_name" => "Kalmyk-Oirat",
            "vk_id" => 357,
            "name" => "Хальмг келн"
        ],
        "ud" => [
            "eng_name" => "Udmurt",
            "vk_id" => 107,
            "name" => "Удмурт"
        ],
        "uk" => [
            "eng_name" => "Ukrainian",
            "vk_id" => 1,
            "name" => "Українська"
        ],
        "kk" => [
            "eng_name" => "Kazakh",
            "vk_id" => 97,
            "name" => "Қазақша"
        ],
        "ne" => [
            "eng_name" => "Nepali",
            "vk_id" => 83,
            "name" => "नेपाली"
        ],
        "hi" => [
            "eng_name" => "Hindi",
            "vk_id" => 76,
            "name" => "हिन्दी"
        ],
        "bn" => [
            "eng_name" => "Bengali",
            "vk_id" => 78,
            "name" => "বাংলা"
        ],
        "kn" => [
            "eng_name" => "Kannada",
            "vk_id" => 94,
            "name" => "ಕನ್ನಡ"
        ],
        "si" => [
            "eng_name" => "Singhalese",
            "vk_id" => 77,
            "name" => "සිංහල"
        ],
        "th" => [
            "eng_name" => "Thai",
            "vk_id" => 68,
            "name" => "ภาษาไทย"
        ],
        "ka" => [
            "eng_name" => "Georgian",
            "vk_id" => 63,
            "name" => "ქართული"
        ],
        "my" => [
            "eng_name" => "Burmese",
            "vk_id" => 81,
            "name" => "ဗမာစာ"
        ],
        "ja" => [
            "eng_name" => "Japanese",
            "vk_id" => 20,
            "name" => "日本語"
        ],
        "zh" => [
            "eng_name" => "Chinese",
            "vk_id" => 18,
            "name" => "汉语"
        ],
        "tw" => [
            "eng_name" => "Taiwanese",
            "vk_id" => 119,
            "name" => "臺灣話"
        ],
        "ko" => [
            "eng_name" => "Korean",
            "vk_id" => 17,
            "name" => "한국어"
        ],
        "hy" => [
            "eng_name" => "Armenian",
            "vk_id" => 58,
            "name" => "Հայերեն"
        ],
        "he" => [
            "eng_name" => "Hebrew",
            "vk_id" => 99,
            "name" => "עברית"
        ],
        "ur" => [
            "eng_name" => "Urdu",
            "vk_id" => 85,
            "name" => "اردو"
        ],
        "ar" => [
            "eng_name" => "Arabic",
            "vk_id" => 98,
            "name" => "العربية"
        ],
        "fa" => [
            "eng_name" => "Persian",
            "vk_id" => 74,
            "name" => "فارسی"
        ],
        "pa" => [
            "eng_name" => "Punjabi (western)",
            "vk_id" => 90,
            "name" => "پنجابی"
        ],
    ];

    public static function getCountries(string $lang = null)
    {
        if ($lang === null) {
            $lang = \App::getLocale();
        }
        $countries_res = \Cache::get('countries.' . $lang, null);
        if ($countries_res === null) {
            $countries_res = [];
            if (isset(self::LANGUAGES[$lang])) {
                $language = self::LANGUAGES[$lang];

                $countries = curl_init();
                curl_setopt($countries, CURLOPT_URL, 'https://vk.com/select_ajax.php');
                curl_setopt($countries, CURLOPT_POST, true);
                curl_setopt($countries, CURLOPT_HTTPHEADER, [
                    'Cookie: remixlang=' . $language['vk_id'],
                ]);
                curl_setopt($countries, CURLOPT_POSTFIELDS, 'act=a_get_countries');
                curl_setopt($countries, CURLOPT_RETURNTRANSFER, true);

                $res = curl_exec($countries);
                $headers = curl_getinfo($countries);
                if (isset($headers['content_type']) && ($pos = strpos($headers['content_type'], 'charset=')) !== false) {
                    $charset = explode(' ', substr($headers['content_type'], $pos + 8))[0];
                    $res = mb_convert_encoding($res, 'utf-8', $charset);
                }
                $res = json_decode($res, 1);

                if (isset($res['countries'])) {
                    foreach ($res['countries'] as $country) {
                        $countries_res[$country[0]] = strip_tags($country[1]);
                    }
                    \Cache::forever('countries.' . $lang, $countries_res);
                }
            }
        }

        return $countries_res;
    }

    public static function getCountry($id)
    {
        $countries = self::getCountries();
        $result = null;
        if (isset($countries[$id])) {
            $result = $countries[$id];
        }

        return $result;
    }

    public static function getCities(int $country, string $str = '', string $lang = null)
    {
        if ($lang === null) {
            $lang = \App::getLocale();
        }
        $cities_res = [];
        if (isset(self::LANGUAGES[$lang])) {
            $lang = self::LANGUAGES[$lang];

            $cities = curl_init();
            curl_setopt($cities, CURLOPT_URL, 'https://vk.com/select_ajax.php');
            curl_setopt($cities, CURLOPT_POST, true);
            curl_setopt($cities, CURLOPT_HTTPHEADER, [
                'Cookie: remixlang=' . $lang['vk_id'],
            ]);
            curl_setopt($cities, CURLOPT_POSTFIELDS, 'act=a_get_cities&country=' . $country . '&str=*' . $str);
            curl_setopt($cities, CURLOPT_RETURNTRANSFER, true);

            $res = curl_exec($cities);
            $headers = curl_getinfo($cities);
            if (isset($headers['content_type']) && ($pos = strpos($headers['content_type'], 'charset=')) !== false) {
                $charset = explode(' ', substr($headers['content_type'], $pos + 8))[0];
                $res = mb_convert_encoding($res, 'utf-8', $charset);
            }
            $cities = json_decode($res, 1);
            if (empty($cities)) {
                try {
                    $cities = eval('return ' . $res . ';');
                } catch (\Exception $e) {}
            }
            foreach ($cities as $city) {
                $cities_res[$city[0]] = [
                    'region' => $city[2],
                    'name' => $city[1],
                ];
            }
        }

        return $cities_res;
    }

}