<?php

return [
    'profile'     => 'Профиль',
    'description' => '<p>В этом разделе Вы можете выбрать профиль Студент или Преподаватель</p>' .
        '<p>для того, чтобы осуществлять деятельность на сайте.</p>',
    'type'        => [
        'title'   => 'Ваш профиль',
        'student' => [
            'dialect'       => 'Ваш диалект адыгского языка',
            'lang-level'    => 'Ваш уровень адыгского языка',
            'learn-targets' => 'Цели изучения адыгского языка',
            'about-me'      => 'Обо мне',
            'lesson-type'   => 'Виды уроков',
        ],
        'teacher' => [
            'info'                 => '<p>Заполните профиль преподавателя. После заполнения и проверки Ваших данных</p>' .
                '<p> модератором, Вы сможете преподавать адыгский язык на Портале Адыгского языка.</p>',
            'info-wait-moderation' => '<p>Ваши данные были отправленны на проверку модератором.</p>',
            'profession'           => 'Моя профессия',
            'dialect'              => 'Ваш диалект адыгского языка',
            'exp'                  => 'Опыт преподавания адыгского языка',
            'education'            => 'Образование',
            'about-me'             => 'Обо мне',
            'teach-style'          => 'Мой стиль преподавания',
            'prefer-level'         => 'Я преподаю на уровнях',
            'lesson-type'          => 'Виды уроков',
            'diploma'              => [
                'title'       => 'Диплом преподавателя',
                'description' => '<p>Если у Вас есть диплом преподавателя, то Вы можете загрузить его изображение.</p>' .
                    '<p>Это повысит доверие студентов к Вам, а также повысит Ваш рейтинг.</p>',
                'upload'      => 'Загрузить диплом преподавателя',
                'limit'       => 'Максимальный размер файла :limit Мб',
                'link'        => 'Ссылка на загруженный диплом',
                'err-format'  => 'Неверный формат файла',
            ],
            'video'                => [
                'title'       => 'Видео преподавателя',
                'description' => '<p>Запишите видео для своих студентов. Расскажите о себе.</p>' .
                    '<p>Это видео позволит студентам больше узнать о Вас.</p>',
                'placeholder' => 'Ссылка на видео',
            ],
            'send-for-moderation'  => 'Отправить на модерацию',
            'error'                => [
                'header'  => 'Внимание',
                'message' => 'У вас ошибка в профиле "Преподаватель"',
            ],
        ],
    ],
];