<?php

return [
    'tariff'             => 'Тариф',
    'tariffs'            => 'Тарифы',
    'description'        => '<p>Подписаться на обучение адыгского языка просто.</p>' .
        '<p>1) Выберите способ обучения. ' . '2) Составьте план на неделю. ' . '3) Выберите срок обучение. </p>',
    'not-changed'        => 'Вы пока что не выбрали тариф',
    'method'             => 'Выберите способ обучения',
    'methods'            => [
        'personal' => [
            'name'        => 'Частный',
            'description' => 'Уроки один на один. Все функции активированы.',
        ],
        'group'    => [
            'name'        => 'Групповой',
            'description' => 'Групповые уроки. Все функции активированы.',
            'discount'    => 'Скидка :percents%',
        ],
    ],
    'studyingTime'       => 'Выберите время обучения',
    'minutesPerDay'      => 'минут в день',
    'lessonsPerWeek'     => ':count в неделю',
    'lessonEndings'      => [
        'one'  => 'раз',
        'two'  => 'раза',
        'five' => 'раз',
    ],
    'period'             => 'Выберите срок обучения',
    'change-currency'    => 'Выберите валюту',
    'periodEndings'      => [
        'one'  => 'месяц',
        'two'  => 'месяца',
        'five' => 'месяцев',
    ],
    'paymentDescription' => 'Возобновляется каждый месяц',
    'discount'           => 'Скидка',
    'discountPromo'      => 'Скидка по промокоду',
    'discountGroup'      => 'Групповая скидка',
    'result'             => [
        'header'         => 'Вы выбрали',
        'method'         => [
            'header'   => 'Вид уроков',
            'personal' => 'Частные уроки',
            'group'    => 'Групповые уроки',
        ],
        'sum'            => 'Сумма',
        'total'          => 'Итого',
        'minutesPerDay'  => 'Сколько минут в день',
        'lessonsPerWeek' => 'Количество раз в неделю',
        'period'         => 'Период подписки',
    ],
    'subscribe'          => 'Подписаться',
    'renew-subscription' => 'Продлить подписку',
    'take-money'         => 'Забрать :count :currency',
    'attributes'         => [
        'promo-discount'   => '"Скидка по промокоду"',
        'group-discount'   => '"Групповая скидка"',
        'minutes-per-day'  => '"Количество минут в день"',
        'lessons-per-week' => '"Количество занятий в неделю"',
        'tariff-name'      => '"Срок обучения"',
    ],
    'payment'            => [
        'title'   => 'Подписка на занятия',
        'success' => 'Оплата проведена успешно',
        'error'   => [
            'title'   => 'Ошибка оплаты',
            'header'  => 'Что-то пошло не так...',
            'message' => 'Платеж не прошел из-за технической ошибки',
        ],
    ],
];