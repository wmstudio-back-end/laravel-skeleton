<?php

return [
    '404'  => 'Страница не найдена',
    'csrf' => 'Срок действия страницы истек из-за неактивности.<br/><br/>' .
        '<a href=":href">Обновите</a> и повторите попытку.',
];