<?php

$name = 'Локализация';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.translations',
                    'label' => 'Просмотр языков и групп переводов',
                    'uses' => 'AdminController@index',
                ],
            ],

            [
                'method' => 'post',
                'uri' => '/locales/{action}',
                'parameters' => [
                    'as' => 'admin.translations.locales',
                    'label' => 'Добавление и удаление языков',
                    'uses' => 'AdminController@locales',
                ],
                'where' => ['action' => 'add|remove'],
            ],

            [
                'method' => 'get',
                'uri' => '/group/{group}',
                'parameters' => [
                    'as' => 'admin.translations.groups',
                    'label' => 'Просмотр группы и ее значений переводов',
                    'uses' => 'AdminController@groups',
                ],
                'where' => ['group' => '.*'],
            ],
            [
                'method' => 'post',
                'uri' => '/group/{action?}',
                'parameters' => [
                    'as' => 'admin.translations.groups.action',
                    'label' => 'Добавление и удаление групп переводов, а так-же добавление новых ключей в группу',
                    'uses' => 'AdminController@groupsAction',
                ],
                'where' => ['action' => 'add|remove'],
            ],

            [
                'method' => 'post',
                'uri' => '/group/{group}/{action}',
                'parameters' => [
                    'as' => 'admin.translations.keys',
                    'label' => 'Редактирование и удаление ключей переводов',
                    'uses' => 'AdminController@keys',
                ],
                'where' => ['group' => '.*', 'action' => 'edit|remove'],
            ],

            [
                'method' => 'post',
                'uri' => '/publish/{group?}',
                'parameters' => [
                    'as' => 'admin.translations.publish',
                    'label' => 'Публикация всех переводов или отдельных групп',
                    'uses' => 'AdminController@publish',
                ],
                'where' => ['group' => '.*'],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.translations',
                'icon'      => 'fa fa-language',
                'order'     => 70,
            ],
        ],
    ],
    'translateDefaultPrefix' => 'transDefault',
];
