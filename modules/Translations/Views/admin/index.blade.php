<?php
$canLocalesEdit = getPermissions('admin.translations.locales');
$canShowGroups  = getPermissions('admin.translations.groups');
$canGroupsEdit  = getPermissions('admin.translations.groups.action');
$canPublish     = getPermissions('admin.translations.publish');
?>

@extends(config('admin.defaults.layout'), ['old' => old()])

@section('content')
    <div class="row">
        <!-- Supported locales -->
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <div class="float-right">
                        <a data-collapse="#langs" class="btn btn-icon">
                            <i class="ion ion-minus"></i>
                        </a>
                    </div>
                    <h4>Поддерживаемые языки</h4>
                </div>
                <div class="collapse show" id="langs">
                    <div class="card-body">
                        @if($canLocalesEdit)
                            <form method="post" action="{{ route('admin.translations.locales', ['action' => 'add']) }}">
                                @csrf
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="new-locale">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-outline-secondary">Добавить</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                        <div class="row">
                            @foreach ($locales as $lang)
                                <div class="lang-item">
                                    <div class="alert alert-primary">
                                        @if($canLocalesEdit)
                                            <form method="post" action="{{ route('admin.translations.locales', ['action' => 'remove']) }}">
                                                @csrf
                                                <input type="hidden" name="remove-locale" value="{{ $lang }}">
                                                <button type="submit" class="close" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </form>
                                        @endif
                                        <strong>{{ $lang }}</strong>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Supported locales -->
        <!----------------------->
        <!----- Groups list ----->
        <div class="col-12">
            <div class="card animated fadeInLeft">
                <div class="card-header">
                    <div class="float-right">
                        <a data-collapse="#lang-groups" class="btn btn-icon">
                            <i class="ion ion-minus"></i>
                        </a>
                    </div>
                    <h4>Группы</h4>
                </div>
                <div class="collapse show" id="lang-groups">
                    <div class="card-body">
                        @if($canGroupsEdit)
                            <form method="post" action="{{ route('admin.translations.groups.action', ['action' => 'add']) }}">
                                <div class="row">
                                    @csrf
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-form-label" for="gr-name">Имя</label>
                                            <input type="text" class="form-control" required="required" id="gr-name" name="gr-name">
                                        </div>
                                    </div>
                                    <div class="col-xxl-5 col-lg-4">
                                        <div class="form-group">
                                            <label class="col-form-label" for="gr-keys">Ключи</label>
                                            <textarea class="form-control" rows="4" required="required" id="gr-keys"
                                                      name="gr-keys" placeholder="Добавьте 1 ключ на строку, без префикса группы"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xxl-1 col-lg-2">
                                        <div class="form-group text-right trans-group-save">
                                            <button type="submit" class="btn btn-primary">Создать</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                        <div class="row">
                            @foreach ($groups as $group)
                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xxl-2 transGroups">
                                    @if($canGroupsEdit)
                                        <form class="trans-group-remove" method="post" action="{{ route('admin.translations.groups.action', ['action' => 'remove']) }}">
                                            @csrf
                                            <input type="hidden" name="gr-name" value="{{ $group }}">
                                    @endif
                                        @if($canShowGroups)
                                            <a href="{{ route('admin.translations.groups', ['group' => $group]) }}"
                                                class="btn btn-primary btn-block">
                                                @if($canGroupsEdit)
                                                    <button type="submit" class="close" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                @endif
                                                <strong>{{ $group }}</strong>
                                            </a>
                                        @else
                                            <div class="btn btn-primary btn-block">
                                                @if($canGroupsEdit)
                                                    <button type="submit" class="close" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                @endif
                                                <strong>{{ $group }}</strong>
                                            </div>
                                        @endif
                                    @if($canGroupsEdit)
                                        </form>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----- Groups list ----->
    </div>
@endsection

@if($canPublish && ($needPublish || $needPublishDefaults))
    @section('sub-action')
        @if($needPublishDefaults)
            <form method="post" action="{{ route('admin.translations.publish') }}">
                @csrf
                <button type="submit" name="publish-defaults" value="true" class="btn btn-sm btn-warning pull-right mr-2">
                    Требуется опубликовать значения по умолчанию
                </button>
            </form>
        @endif
        @if($needPublish)
            <form method="post" action="{{ route('admin.translations.publish') }}">
                @csrf
                <button type="submit" class="btn btn-sm btn-success pull-right">Опубликовать</button>
            </form>
        @endif
    @endsection
@endif