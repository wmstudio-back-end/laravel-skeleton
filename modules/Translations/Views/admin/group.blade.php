<?php
$canGroupsEdit  = getPermissions('admin.translations.groups.action');
$canKeysEdit    = getPermissions('admin.translations.keys');
$canPublish     = getPermissions('admin.translations.publish');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInUp">
                <div class="collapse show" id="lang-keys">
                    <div class="card-body">
                        @if($canGroupsEdit)
                            <form method="post" class="row" action="{{ route('admin.translations.groups.action', ['action' => 'add']) }}">
                                @csrf
                                <input type="hidden" name="gr-name" value="{{ $group }}">
                                <div class="col-10">
                                    <div class="form-group">
                                        <label class="col-form-label" for="gr-keys">Создать ключи</label>
                                        <textarea class="form-control" rows="3" required="required" id="gr-keys"
                                                  name="gr-keys" placeholder="Добавьте 1 ключ на строку, без префикса группы"></textarea>
                                    </div>
                                </div>
                                <div class="col-2 content-center">
                                    <button type="submit" class="btn btn-primary">Создать</button>
                                </div>
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped table-hover parse-data-table">
                                <thead>
                                    <tr>
                                        {{--<th class="text-center no-sort">
                                            <div class="custom-checkbox custom-control">
                                                <input type="checkbox" data-checkboxes="mygroup" data-checkbox-role="dad" class="custom-control-input" id="checkbox-all">
                                                <label for="checkbox-all" class="custom-control-label"></label>
                                            </div>
                                        </th>--}}
                                        <th>Ключ</th>
                                        @foreach($locales as $lang)
                                            <th class="text-center no-sort">{{ $lang }}</th>
                                        @endforeach
                                        @if($canKeysEdit)
                                            <th class="text-center no-sort">Удалить</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($translations as $key => $translation)
                                        <tr data-id="{{ $key }}" class="no-sort">
                                            {{--<td width="40">
                                                <div class="custom-checkbox custom-control">
                                                    <input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox-{{ $key }}">
                                                    <label for="checkbox-{{ $key }}" class="custom-control-label"></label>
                                                </div>
                                            </td>--}}
                                            <td>{{ $key }}</td>
                                            @foreach($locales as $locale)
                                                <?php $t = isset($translation[$locale]) ? $translation[$locale] : null ?>
                                                <?php /* @var $t \Barryvdh\TranslationManager\Models\Translation */ ?>
                                                <td class="text-center">
                                                    @if($canKeysEdit)
                                                        <button
                                                            type="button"
                                                            class="btn btn-link open-trans-modal"
                                                            data-group="{{ $group }}"
                                                            data-key="{{ $key }}"
                                                            data-locale="{{ $locale }}"
                                                            data-value="{{ ($t && $t->value) ? $t->value : '' }}"
                                                        >
                                                    @else
                                                        <button
                                                            type="button"
                                                            class="btn btn-link"
                                                            @if ($t && $t->value && mb_strlen($t->value, 'UTF-8') > 50)
                                                                data-container="body"
                                                                data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-html="true"
                                                                title="{{ $t->value }}"
                                                            @endif
                                                        >
                                                    @endif
                                                        @if($t && $t->value)
                                                            <?php $value = (mb_strlen($t->value, 'UTF-8') > 50)
                                                                ? mb_substr($t->value, 0, 50, 'UTF-8') . '...'
                                                                : $t->value; ?>
                                                            {!! htmlentities($value, ENT_QUOTES, 'UTF-8', false) !!}
                                                        @else
                                                            Empty
                                                        @endif
                                                    </button>
                                                    {{--@if($canKeysEdit)
                                                        @section('modals')
                                                            @parent
                                                            @include('translations::admin.partials.modal', [
                                                                'group' => $group,
                                                                'key' => $key,
                                                                'locale' => $locale,
                                                                'value' => ($t && $t->value) ? $t->value : '',
                                                                'class' => 'trans-modal',
                                                            ])
                                                        @stop
                                                    @endif--}}
                                            @endforeach
                                            @if($canKeysEdit)
                                                <td class="text-center">
                                                    <form method="post" class="transRemoveAjax" action="{{ route('admin.translations.keys', [
                                                        'group' => $group,
                                                        'action' => 'remove'
                                                    ]) }}">
                                                        @csrf
                                                        <input type="hidden" name="key" value="{{ $key }}">
                                                        <button type="submit" class="btn btn-action btn-danger"><i class="fa fa-times"></i></button>
                                                    </form>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    <div class="modal fade trans-modal" tabindex="-1" role="dialog" id="trans-key-modal">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <textarea id="trans-original-value" class="original" style="display: none;"></textarea>
                <form id="trans-form" method="post" class="transEditAjax" data-action="{{ route('admin.translations.keys', [
                            'group' => 'replace-group',
                            'action' => 'edit'
                        ]) }}">
                    @csrf
                    <input id="trans-key" type="hidden" name="key">
                    <input id="trans-locale" type="hidden" name="locale">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <textarea id="trans-value" name="value" class="form-control summernote" rows="20"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@if($canPublish && $needPublish)
    @section('sub-action')
        <form method="post" action="{{ route('admin.translations.publish', ['group' => $group]) }}">
            @csrf
            <button type="submit" class="btn btn-sm btn-success pull-right">Опубликовать</button>
        </form>
    @endsection
@endif