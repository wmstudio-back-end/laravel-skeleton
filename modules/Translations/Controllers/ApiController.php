<?php

namespace Modules\Translations\Controllers;

use App\Http\Controllers\Controller;
use Modules\Translations\Constants\Languages;

class ApiController extends Controller
{
    public function getCities()
    {
        if (request()->ajax()) {
            $data = getRequest();
            $county = isset($data['country']) ? $data['country'] : null;
            $search = isset($data['search']) ? $data['search'] : '';
            $cities = [
                'error' => ___('global.first-select-country'),
            ];
            if ($county) {
                $cities = Languages::getCities($county, $search);
            }

            return response()->json($cities);
        }

        abort(404);
    }
}
