<?php

namespace Modules\Translations\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public static function writeRequired()
    {
        return [
            'keys',
            'publish',
            'locales',
            'groupsAction',
        ];
    }

    /**
     * @param array $array
     * @param array $keys
     * @param string $prefix
     */
    private function assignArrayToKeys(array $array, array &$keys, string $prefix = '')
    {
        foreach ($array as $key => $val) {
            $currPrefix = $prefix === '' ? $key :  $prefix . '.' . $key;
            if (is_array($val)) {
                $this->assignArrayToKeys($val, $keys, $currPrefix);
            } else {
                $keys[$currPrefix] = $val;
            }
        }
    }

    public function index()
    {
        $locales = $this->transManager->getLocales();
        $groups = Translation::groupBy('group');
        $excludedGroups = $this->transManager->getConfig('exclude_groups');
        if($excludedGroups){
            $groups->whereNotIn('group', $excludedGroups);
        }
        $groups = $groups->select('group')->get()->pluck('group', 'group');
        if ($groups instanceof Collection) {
            $groups = $groups->all();
        }

        $defKeys = [];
        $defCount = 0;
        foreach (config('transDefault') as $group => $config){
            $defKeys[$group] = [];
            $this->assignArrayToKeys($config, $defKeys[$group]);
            $defCount = $defCount + count($defKeys[$group]);
        }
        $defTrans = Translation::where('locale', config('app.locale'))
            ->where(function ($query) use($defKeys) {
                foreach ($defKeys as $group => $keys) {
                    $query->orWhere(function ($query) use($group, $keys) {
                        $query->where('group', $group)
                            ->whereIn('key', array_keys($keys));
                    });
                }
            });
        $needPublishDefaults = !($defCount == $defTrans->count('id'));
        $needPublish = (bool)Translation::where('status', Translation::STATUS_CHANGED)->whereNotNull('value')->count('id');

        return view('translations::admin.index', [
            'locales' => $locales,
            'groups' => $groups,
            'needPublish' => $needPublish,
            'needPublishDefaults' => $needPublishDefaults,
        ]);
    }

    public function locales($action, Request $request)
    {
        switch ($action) {
            case 'add':
                $locales = $this->transManager->getLocales();
                $newLocale = str_replace([], '-', trim($request->input('new-locale')));
                if (!$newLocale || in_array($newLocale, $locales)) {
                    return redirect()->back();
                }
                $this->transManager->addLocale($newLocale);
                return redirect()->back();
                break;
            case 'remove':
                if ($lang = $request->input('remove-locale')) {
                    // Не удаляем локаль, а игнорируем.
                    $files = app('files');
                    $ignoreFilePath = storage_path( '.ignore_locales' );
                    $ignoredLocates = [];
                    if ( $files->exists( $ignoreFilePath ) ) {
                        $result = json_decode($files->get($ignoreFilePath));

                        $ignoredLocates = ($result && is_array($result)) ? $result : [];
                    }

                    $ignoredLocates = array_merge($ignoredLocates, [$lang]);
                    $files->put( $ignoreFilePath, json_encode($ignoredLocates));

                    // $this->transManager->removeLocale($lang);
                }
                return redirect()->back();
                break;
            default:
                abort(404);
                break;
        }
    }

    public function groups($group)
    {
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.translations')->setActive();
        $menu->lastLevel = "Группа \"$group\"";

        $locales = $this->transManager->getLocales();
        $keys = Translation::where('group', $group)->orderBy('key', 'asc')->get();
        $translations = [];
        foreach($keys as $translation){
            $translations[$translation->key][$translation->locale] = $translation;
        }
        $needPublish = (bool)Translation::where([
            'status' => Translation::STATUS_CHANGED,
            'group' => $group,
        ])->whereNotNull('value')->count('id');

        return view('translations::admin.group', [
            'locales' => $locales,
            'group' => $group,
            'translations' => $translations,
            'needPublish' => $needPublish,
        ]);
    }

    public function groupsAction($action, Request $request)
    {
        switch ($action) {
            case 'add':
                $group = $request->input('gr-name');
                if ($group) {
                    $keys = explode("\n", $request->get('gr-keys'));

                    foreach($keys as $key){
                        $key = trim($key);
                        if($group && $key){
                            $this->transManager->missingKey('*', $group, $key);
                        }
                    }
                    return redirect()->route('admin.translations.groups', ['group' => $group]);
                } else {
                    return redirect()->back();
                }
                break;
            case 'remove':
                $group = $request->input('gr-name');
                Translation::where('group', $group)->delete();
                return redirect()->route('admin.translations');
                break;
            default:
                abort(404);
                break;
        }
    }

    public function keys($group, $action, Request $request)
    {
        $data = getRequest();
        switch ($action) {
            case 'edit':
                $edit = function() use($data, $group) {
                    $key = Translation::where([
                        'locale' => $data['locale'],
                        'group' => $group,
                        'key' => $data['key'],
                    ])->first();
                    if (!$key) {
                        $key = new Translation();
                        $key->locale = $data['locale'];
                        $key->group = $group;
                        $key->key = $data['key'];
                    }
                    $key->status = Translation::STATUS_CHANGED;
                    if (isset($data['new-key']) && $data['new-key']) {
                        $key->key = $data['new-key'];
                    }
                    if (array_key_exists('value', $data)) {
                        $key->value = $data['value'];
                    }
                    $key->save();
                };
                if ($request->ajax()) {
                    $error = false;
                    $msgHeader = 'Успешно обновлено!';
                    $message = 'Обновлена 1 запись';
                    try {
                        $edit();
                    } catch (\Exception $e) {
                        $error = true;
                        $msgHeader = 'Ошибка!';
                        $message = $e->getMessage();
                    }
                    return response()->json([
                        'error' => $error, 'msgHeader' => $msgHeader, 'message' => $message,
                    ]);
                } else {
                    $edit();
                }
                break;
            case 'remove':
                $remove = function() use($data, $group) {
                    $keys = Translation::where([
                        'group' => $group,
                        'key' => $data['key'],
                    ])->get();
                    Translation::unguard();
                    foreach ($keys as $key) {
                        $key->delete();
                    }
                    Translation::reguard();
                };

                if ($request->ajax()) {
                    $error = false;
                    $msgHeader = 'Успешно обновлено!';
                    $message = 'Удалена 1 запись';
                    try {
                        $remove();
                    } catch (\Exception $e) {
                        $error = true;
                        $msgHeader = 'Ошибка!';
                        $message = $e->getMessage();
                    }
                    return response()->json([
                        'error' => $error, 'msgHeader' => $msgHeader, 'message' => $message,
                    ]);
                } else {
                    $remove();
                }
                break;
            default:
                abort(404);
                break;
        }

        return redirect()->route('admin.translations.groups', ['group' => $group]);
    }

    private function publishDefaults()
    {
        $defKeys = [];
        $defCount = 0;
        foreach (config('transDefault') as $group => $config){
            $defKeys[$group] = [];
            $this->assignArrayToKeys($config, $defKeys[$group]);
            $defCount = $defCount + count($defKeys[$group]);
        }
        $defTrans = Translation::where('locale', config('app.locale'))
            ->where(function ($query) use($defKeys) {
                foreach ($defKeys as $group => $keys) {
                    $query->orWhere(function ($query) use($group, $keys) {
                        $query->where('group', $group)
                            ->whereIn('key', array_keys($keys));
                    });
                }
            });
        $defTrans = $defTrans->get();
        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($defKeys as $group => $keys) {
            foreach ($keys as $key => $value) {
                if ($defTrans->where('group', $group)->where('key', $key)->count() === 0) {
                    $insert[] = [
                        'status'     => Translation::STATUS_CHANGED,
                        'locale'     => config('app.locale'),
                        'group'      => $group,
                        'key'        => $key,
                        'value'      => $value,
                        'created_at' => $timeStamp,
                        'updated_at' => $timeStamp,
                    ];
                }
            }
        }
        Translation::unguard();
        Translation::insert($insert);
        Translation::reguard();
    }

    public function publish($group = '*', Request $request)
    {
        if ($group === '*') {
            if ((bool)$request->get('publish-defaults', false)) {
                $this->publishDefaults();
            }
        }

        $error = false;
        $msgHeader = 'Успешно обновлено!';
        $message = $group =='*' ? 'Все переводы успешно опубликованы' : "Группа $group успешно опубликована";
        try {
            $json = false;

            if($group === '_json') {
                $json = true;
            }

            $this->transManager->exportTranslations($group, $json);
        } catch (\Exception $e) {
            $error = true;
            $msgHeader = 'Ошибка!';
            $message = $e->getMessage();
        }
        if ($request->ajax()) {
            return response()->json([
                'error' => $error,
                'msgHeader' => $msgHeader,
                'message' => $message,
            ]);
        } else {
            Input::replace([
                'toastr.error' => $error,
                'toastr.msgHeader' => $msgHeader,
                'toastr.message' => $message,
            ]);
            return redirect()->route('admin.translations')->withInput();
        }
    }
}
