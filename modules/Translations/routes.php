<?php

Route::post('/api/get-cities', [
    'middleware' => [
        'web',
        'auth',
        'lang',
    ],
    'as' => 'api.get-cities',
    'uses' => 'Modules\Translations\Controllers\ApiController@getCities',
]);

//Route::group(['middleware' => 'web', 'prefix' => 'translations', 'namespace' => 'Modules\Translations\Controllers'], function()
//{
//    Route::get('/', 'TranslationsController@index');
//});
