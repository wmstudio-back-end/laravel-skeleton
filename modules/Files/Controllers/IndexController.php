<?php

namespace Modules\Files\Controllers;

use App\Http\Controllers\Controller;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;
use Modules\Users\Entities\User;

class IndexController extends Controller
{
    public function index($file)
    {
        $fields = Field::whereIn('type', ['file', 'image'])->get()->keyBy('id');

        $field = FieldsValue::where('alias', $file)->whereIn('field_id', $fields->keys())->first();

        if ($field && file_exists($field->value)) {
            return response()->file($field->value);
        } else {
            try {
                if ($fields[$field->field_id]->alias == 'avatar') {
                    return response()->file('.' . User::NO_AVATAR);
                }
            } catch (\Exception $e) {}
            abort(404);
        }
    }
}
