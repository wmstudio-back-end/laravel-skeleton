<?php

namespace Modules\Files\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;

class AdminController extends Controller
{
    private $filesPerPage;

    public static function writeRequired()
    {
        return [
            'upload',
            'edit',
            'delete',
        ];
    }

    public function index()
    {
        $fields = Field::whereIn('alias', ['file', 'image'])->get()->keyBy('id');

        $files = FieldsValue::whereIn('field_id', $fields->keys())->orderBy('alias', 'ASC')->paginate($this->filesPerPage);

        return view('files::admin.index', [
            'files' => $files,
            'userFiles' => false,
        ]);
    }

    public function userFiles()
    {
        $fields = Field::whereIn('type', ['file', 'image'])->whereNotIn('alias', ['file', 'image'])->get()->keyBy('id');

        $files = FieldsValue::whereIn('field_id', $fields->keys())->orderBy('alias', 'ASC')->paginate($this->filesPerPage);

        return view('files::admin.index', [
            'files' => $files,
            'userFiles' => true,
        ]);
    }
    
    public function upload(Request $request)
    {
        $data = getRequest();
        $validator = \Validator::make($data, [
            'files' => 'required',
        ]);

        $data = $data['files'];
        $error = false;
        if (!$validator->fails() && $request->hasfile('files')) {
            foreach($data as $key => $file) {
                $upload = !(uploadFile($file) === false);
                if (!$error) {
                    $error = $upload;
                }
            }

            Input::replace([
                'toastr.error' => false,
                'toastr.msgHeader' => 'Файлы успешно загружены.',
                'toastr.message' => count($data) . ' '
                    . numEnding(count($data), ['файл', 'файла', 'файлов']) . ' успешно '
                    . numEnding(count($data), ['загружен', 'загружено', 'загружены']). '.',
            ]);
            return redirect()->back()->withInput();
        }

        if ($validator->fails() || $error) {
            Input::replace([
                'toastr.error' => true,
                'toastr.msgHeader' => 'В ходе загрузки произошли ошибки.',
                'toastr.message' => 'Некоторые файлы могли быть не загружены.',
            ]);
            return redirect()->back()->withInput();
        } else {
            return redirect()->back();
        }
    }

    public function edit(Request $request)
    {
        $data = getRequest();
        try {
            $error = false;
            $message = 'Файл успешно изменен.';
            $newAlias = null;

            if (!isset($data['fileId'])) {
                $error = true;
                $message = 'Файл не найден.';
            } else {
                $file = FieldsValue::find($data['fileId']);
                if ($file === null) {
                    $error = true;
                    $message = 'Файл не найден.';
                } else {
                    $file->alias = $data['fileNewAlias'];
                    $file->save();
                    $newAlias = $data['fileNewAlias'];
                }
            }
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }

        if ($request->ajax()) {
            $json = [
                'error' => $error,
                'message' => $message,
            ];
            if (isset($newAlias)) {
                $json['newAlias'] = $newAlias;
            }
            return response()->json($json);
        } else {
            return redirect()->back();
        }
    }

    public function delete(Request $request)
    {
        $data = getRequest();
        try {
            switch ($data['action']) {
                case 'deleteFile':
                    $error = false;
                    $message = 'Файл успешно удален.';

                    if (!isset($data['fileId'])) {
                        $error = true;
                        $message = 'Файл не найден.';
                    } else {
                        $file = FieldsValue::find($data['fileId']);
                        if ($file == null) {
                            $error = true;
                            $message = 'Файл не найден.';
                        } else {
                            if (file_exists($file->value)) {
                                unlink($file->value);
                            }
                            $file->delete();
                        }
                    }
                    break;
                case 'deleteManyFiles':
                    $error = true;
                    $message = 'Один или несколько файлов было не найдено.';

                    if (isset($data['data'])) {
                        $files = FieldsValue::whereIn('id', $data['data'])->get();
                        if ($files) {
                            FieldsValue::unguard();
                            foreach ($files as $file) {
                                /* @var $file FieldsValue */
                                if (file_exists($file->value)) {
                                    unlink($file->value);
                                }
                                $file->delete();
                            }
                            FieldsValue::reguard();

                            $error = false;
                            $message = $files->count() . ' '
                                . numEnding($files->count(), ['файл', 'файла', 'файлов']) . ' '
                                . numEnding($files->count(), ['был', 'были', 'было'])
                                . ' успешно '
                                . numEnding($files->count(), ['удален', 'удалены', 'удалено']) . '.';
                        }
                    }
                    break;
                default:
                    $error = true;
                    $message = 'Неизвестное действие.';
                    break;
            }
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }

        if ($request->ajax()) {
            $json = [
                'error' => $error,
                'message' => $message,
            ];
            if (isset($newAlias)) {
                $json['newAlias'] = $newAlias;
            }
            return response()->json($json);
        } else {
            return redirect()->back();
        }
    }
}
