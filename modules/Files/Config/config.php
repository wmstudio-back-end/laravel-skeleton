<?php

$name = 'Файлы';

return [
    'name' => $name,
    'routes' => [
        'last' => [
            'method' => 'get',
            'uri' => '/{file}',
            'parameters' => [
                'as' => 'files',
                'uses' =>'IndexController@index',
                'no-index' => true,
            ],
            'where' => ['file' => '.+'],
        ],
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.files',
                    'label' => 'Просмотр загруженных файлов',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'get',
                'uri' => '/user-files',
                'parameters' => [
                    'as' => 'admin.user-files',
                    'label' => 'Просмотр загруженных пользовательских файлов (аватирки, документы об образовании)',
                    'uses' => 'AdminController@userFiles',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/upload',
                'parameters' => [
                    'as' => 'admin.files.upload',
                    'label' => 'Загрузка новых файлов',
                    'uses' => 'AdminController@upload',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/edit',
                'parameters' => [
                    'as' => 'admin.files.edit',
                    'label' => 'Редактирование файлов',
                    'uses' => 'AdminController@edit',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/delete',
                'parameters' => [
                    'as' => 'admin.files.delete',
                    'label' => 'Удаление файлов',
                    'uses' => 'AdminController@delete',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => $name,
                'uri'   => '#',
                'icon'  => 'fa fa-list-alt',
                'pages' => [
                    [
                        'label' => 'Административные',
                        'route' => 'admin.files',
                        'icon'  => 'fa fa-address-card',
                    ],
                    [
                        'label' => 'Пользовательские',
                        'route' => 'admin.user-files',
                        'icon'  => 'fa fa-user',
                    ],
                ],
                'order' => 90,
            ],
        ],
    ],
];
