<?php
$canUpload = getPermissions('admin.files.upload');
$canEdit = getPermissions('admin.files.edit');
$canDelete = getPermissions('admin.files.delete');
$canShow = $userFiles ? getPermissions('admin.files') : getPermissions('admin.user-files');
?>

@extends(config('admin.defaults.layout'), ['old' => old()])

@section('content')
    <div class="row">
        @if($canUpload)
            <div class="col-md-6">
                <div class="card card-primary card-files animated fadeInLeft">
                    <div class="card-header">
                        <h4>Добавить файлы</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.files.upload') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-8 col-sm-9 col-md-8 col-lg-9 col-xxl-10">
                                    <div class="custom-file custom-file-sm">
                                        <input type="file" class="custom-file-input" id="customFile" name="files[]" multiple="multiple">
                                        <label class="custom-file-label" for="customFile">Выберите файлы</label>
                                    </div>
                                </div>
                                <div class="col-4 col-sm-3 col-md-4 col-lg-3 col-xxl-2 text-right">
                                    <button id="files-upload" type="submit" class="btn-primary btn btn-sm">Загрузить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        @if($canDelete)
            <div class="col-md-6">
                <div class="card card-info card-files animated fadeInRight">
                    <div class="card-header">
                        <h4>Действия с файлами</h4>
                    </div>
                    <div class="card-body">
                        <button id="files-sel-all" type="button" class="btn-primary btn btn-sm">Выделить все</button>
                        <button id="files-sel-inverse" type="button" class="btn-info btn btn-sm">Инверсия выделения</button>
                        <button id="files-sel-delete" type="button" class="btn-danger btn btn-sm" data-csrf="{{ csrf_token() }}" data-action="{{ route('admin.files.delete') }}">Удалить выбранные</button>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-lg-12 animated fadeInUp">
            @foreach($files as $file)
            <? /* @var $file \Modules\Fields\Entities\FieldsValue */ ?>
            <div class="file-box">
                @if($canEdit || $canDelete)
                    <div class="control-btns">
                        @if($canDelete)
                            <form method="post" action="{{ route('admin.files.delete') }}" class="file-delete-form">
                                @csrf
                                <input type="hidden" name="action" value="deleteFile">
                                <input type="hidden" name="fileId" value="{{ $file->id }}">
                        @endif
                            @if ((file_exists($file->value)))
                                @if($canEdit)
                                    <button type="button" class="control-btn-edit btn btn-info" data-id="{{ $file->id }}" data-alias="{{ $file->alias }}">
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                @endif
                                @if($canDelete)
                                    <button type="submit" class="control-btn-delete btn btn-danger"><i class="fa fa-times"></i></button>
                                @endif
                            @else
                                @if($canDelete)
                                    <button type="submit" class="control-btn-delete btn btn-warning"><i class="fa fa-times"></i></button>
                                @endif
                            @endif
                        @if($canDelete)
                            </form>
                        @endif
                    </div>
                @endif
                <div class="file{{ (file_exists($file->value)) ? '' : ' error' }}">
                    <a class="control-file-link" href="{!! (file_exists($file->value)) ? url($file->alias) . '" target="_blank' : '#' !!}">
                        <span class="corner"></span>
                        @if (file_exists($file->value))
                            <?php
                                $isImage = false;
                                $mime = mime_content_type($file->value);
                                if (strpos($mime, 'image') !== false && (
                                        strpos($mime, 'jpeg') !== false
                                        || strpos($mime, 'bmp') !== false
                                        || strpos($mime, 'png') !== false
                                    )
                                ) {
                                    $isImage = true;
                                }
                            ?>
                            @if ($isImage)
                                <div class="image">
                                    <img alt="image" class="img-responsive" src="{{ url($file->alias) }}">
                                </div>
                            @else
                                <div class="icon">
                                    <i class="fa fa-file"></i>
                                </div>
                            @endif
                        @else
                            <div class="icon">
                                <i class="fa fa-ban"></i><br>
                                <span>Файл не найден</span>
                            </div>
                        @endif
                        <div
                            class="file-name"
                            @if(strlen($file->alias) > 20)
                                data-toggle="tooltip" data-placement="bottom" data-title="{{ $file->alias }}"
                            @endif
                        >
                            {{ $file->alias }}
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="pull-right">
                {{ $files->links() }}
            </div>
        </div>
    </div>
@endsection

@if($canShow)
    @section('sub-action')
        <a href="{{ $userFiles ? route('admin.files') : route('admin.user-files') }}" class="btn btn-sm btn-primary pull-right">
            {{ $userFiles ? 'Административные файлы' : 'Пользовательские файлы' }}
        </a>
    @endsection
@endif

@if ($canEdit)
    @section('modals')
        @parent
        <div class="modal inmodal fade" tabindex="-1" role="dialog" id="modal-file-edit">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="inmodal-header-content">
                            <i class="fa fa-pencil modal-icon"></i>
                            <h4 id="modal-file-title" class="modal-title"></h4>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="modal-file-form" method="post" action="{{ route('admin.files.edit') }}">
                        @csrf
                        <input type="hidden" name="action" value="editFile">
                        <input id="modal-file-id" type="hidden" name="fileId">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="modal-file-new-alias">Новое имя файла</label>
                                <input id="modal-file-new-alias" name="fileNewAlias" type="text" class="form-control" placeholder="Новое имя файла">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-action" data-dismiss="modal">Закрыть</button>
                            <button type="submit" class="btn btn-primary btn-action">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @stop
@endif