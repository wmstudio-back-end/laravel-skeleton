<?php

$name = 'Почта';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.emails',
                    'label' => 'Сообщения и обратная связь',
                    'uses' => 'AdminController@index',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.emails',
                'icon'      => 'fa fa-envelope',
                'order'     => 10,
            ],
        ],
    ],
];
