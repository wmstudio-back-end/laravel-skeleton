<?php

namespace Modules\Emails\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Email
 * 
 * @property int $id
 * @property int $type_id
 * @property string $flags
 * @property string $from_url
 * @property string $sender_name
 * @property string $sender_email
 * @property string $subject
 * @property string $message
 * @property bool $viewed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Email extends Eloquent
{
	protected $casts = [
		'type_id' => 'int',
		'viewed' => 'bool'
	];

	protected $fillable = [
		'type_id',
		'flags',
		'from_url',
		'sender_name',
		'sender_email',
		'subject',
		'message',
		'viewed'
	];
}
