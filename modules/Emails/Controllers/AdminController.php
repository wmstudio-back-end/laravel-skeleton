<?php

namespace Modules\Emails\Controllers;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('emails::admin.index');
    }
}
