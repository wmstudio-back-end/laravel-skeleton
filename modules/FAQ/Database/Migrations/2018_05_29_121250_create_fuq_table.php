<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserPermission;

class CreateFuqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('alias');
            $table->string('group');
            $table->unique(['alias', 'group'], 'alias-group');
            $table->string('name');
            $table->text('content');
            $table->string('form_name')->nullable();
            $table->boolean('active');
            $table->string('useful');
            $table->dateTime('date_active');
            $table->timestamps();
        });

        $hasPermissions = UserPermission::where([
            'link_to' => UserPermission::LINK_GROUP,
            'parent_id' => 1,
            'route_name' => 'admin/faq',
        ])->count('id');
        if (!$hasPermissions) {
            UserPermission::create([
                'link_to' => UserPermission::LINK_GROUP,
                'parent_id' => 1,
                'route_name' => 'admin/faq',
                'permission' => UserPermission::P_WRITE,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faq');
    }
}
