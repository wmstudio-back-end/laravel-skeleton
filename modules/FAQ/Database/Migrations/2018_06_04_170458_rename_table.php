<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Barryvdh\TranslationManager\Models\Translation;
use Modules\Users\Entities\UserPermission;

class RenameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('faq')) {
            Schema::rename('fuq', 'faq');
        }
        $translates = Translation::where('key', 'like', '%fuq%')
            ->orWhere('group', 'fuq')->get();
        Translation::unguard();
        foreach ($translates as $translate) {
            /* @var $translate Translation */
            $translate->group = str_replace('fuq', 'faq', $translate->group);
            $translate->key = str_replace('fuq', 'faq', $translate->key);
            $translate->save();
        }
        Translation::reguard();

        $permissions = UserPermission::where('route_name', 'like', 'fuq')->get();
        UserPermission::unguard();
        foreach ($permissions as $permission) {
            /* @var $permission UserPermission */
            $permission->route_name = str_replace('fuq', 'faq', $permission->route_name);
            $permission->save();
        }
        UserPermission::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('faq', 'fuq');
    }
}
