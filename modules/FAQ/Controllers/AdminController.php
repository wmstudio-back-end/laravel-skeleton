<?php

namespace Modules\FAQ\Controllers;

use Illuminate\Http\Request;
use Barryvdh\TranslationManager\Models\Translation;
use Modules\FAQ\Entities\FAQ;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagesPerPage = 40;

        $faq = FAQ::paginate($pagesPerPage);

        return view('faq::admin.index', [
            'faq' => $faq,
        ]);
    }

    /**
     * @param int|string $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function edit($id, Request $request)
    {
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.faq')->setActive();

        if ($id == 'new') {
            $menu->lastLevel = 'Создать';
            $faq = new FAQ();
        } else {
            $menu->lastLevel = 'Редактировать';
            $faq = FAQ::find($id);
        }

        if ($faq) {
            $locales = $this->transManager->getLocales();
            $defLang = config('app.locale');
            $keys = Translation::where('group', FAQ::TRANS_GROUP)
                ->whereIn('locale', $locales)
                ->where('key', 'like', $faq->group . '.' . $faq->alias . '.%')->get();
            $translations = [];
            foreach ($locales as $lang) {
                $translations[$lang] = [
                    'name' => '',
                    'content' => FAQ::EMPTY_CONTENT,
                ];
            }
            foreach($keys as $translation){
                /* @var $translation Translation */
                $tmp = str_replace($faq->group . '.' . $faq->alias . '.', '', $translation->key);
                assignArrayByPath($translations[$translation->locale], $tmp, $translation);
            }

            if ($request->isMethod('POST')) {
                $data = getRequest();

                $validator = [
                    'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $faq->alias ? '|unique:pages' : ''),
                    'group' => 'string|required',
                    'date' => 'required|date',
                    'name-' . $defLang => 'required|string',
                ];
                $empty = true;
                foreach ($data['content'][$defLang] as $type) {
                    $emptyBlock = false;
                    foreach ($type as $item) {
                        if (empty($item)) {
                            $emptyBlock = true;
                            break;
                        }
                    }
                    if (!$emptyBlock) {
                        $empty = false;
                        break;
                    }
                }
                if ($empty) {
                    $validator['content-' . $defLang] = 'message|required';
                }

                $validator = \Validator::make($data, $validator, [
                    'content-' . $defLang . '.required' => 'Хотябы один блок у языка по умолчанию должен быть заполнен.',
                ]);

                if (!$validator->fails()) {
                    setSeo($faq->alias, str_slug($data['alias']), [$data['seo_title'], $data['seo_keywords'], $data['seo_description']]);

                    $faq->alias = str_slug($data['alias']);
                    $faq->group = str_slug($data['group']);
                    $faq->name = $data['name-' . $defLang];
                    $faq->content = json_encode($data['content'][$defLang]);
                    $faq->form_name = $data['form'];
                    $faq->active = $data['active'];
                    $faq->date_active = $data['date'];
                    $faq->save();

                    Translation::unguard();
                    foreach ($locales as $lang) {
                        if (empty($data['name-' . $lang])) {
                            if ($translations[$lang]['name'] instanceof Translation) {
                                $translations[$lang]['name']->delete();
                            }
                        } else {
                            if (!($translations[$lang]['name'] instanceof Translation)) {
                                $translation = new Translation();
                                $translation->status = Translation::STATUS_CHANGED;
                                $translation->locale = $lang;
                                $translation->group = FAQ::TRANS_GROUP;
                                $translation->key = $faq->group . '.' . $faq->alias . '.name';
                            } else {
                                $translation = $translations[$lang]['name'];
                                $translation->status = Translation::STATUS_CHANGED;
                            }
                            $translation->value = $data['name-' . $lang];
                            $translation->save();
                        }

                        $content = $data['content'][$lang];
                        $setTranslation = function($type, $key) use($faq, $lang, $content, &$translations) {
                            if (empty($content[$type][$key])) {
                                if (isset($translations[$lang]['content'][$type][$key])
                                        && $translations[$lang]['content'][$type][$key] instanceof Translation) {
                                    $translations[$lang]['content'][$type][$key]->delete();
                                }
                            } else {
                                if (!isset($translations[$lang]['content'][$type][$key])
                                        || !($translations[$lang]['content'][$type][$key] instanceof Translation)) {
                                    $translation = new Translation();
                                    $translation->status = Translation::STATUS_CHANGED;
                                    $translation->locale = $lang;
                                    $translation->group = FAQ::TRANS_GROUP;
                                    $translation->key = $faq->group . '.' . $faq->alias . ".content.$type.$key";
                                } else {
                                    $translation = $translations[$lang]['content'][$type][$key];
                                    $translation->status = Translation::STATUS_CHANGED;
                                }
                                $translation->value = $content[$type][$key];
                                $translation->save();
                            }
                            unset($translations[$lang]['content'][$type][$key]);
                        };

                        foreach ($content as $type => $values) {
                            foreach ($values as $key => $value) {
                                $setTranslation($type, $key);
                            }
                        }

                        if (count($translations[$lang]['content']['advice'])) {
                            foreach ($translations[$lang]['content']['advice'] as $advice) {
                                if ($advice instanceof Translation) {
                                    $advice->delete();
                                }
                            }
                        }
                        if (count($translations[$lang]['content']['manual'])) {
                            foreach ($translations[$lang]['content']['manual'] as $manual) {
                                if ($manual instanceof Translation) {
                                    $manual->delete();
                                }
                            }
                        }
                    }
                    Translation::reguard();

                    return redirect()->route('admin.faq');
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }

            $formsList = getFormsList();
            $seo       = getSeo('faq.' . $faq->alias);
            $groups = array_keys(\DB::table('faq')->select('group')->distinct()->get()->keyBy('group')->toArray());

            return view('faq::admin.edit', [
                'faq' => $faq,
                'forms' => $formsList,
                'seo' => $seo,
                'groups' => $groups,
                'locales' => $locales,
                'defLang' => $defLang,
                'translations' => $translations
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function delete(int $id)
    {
        $faq = FAQ::find($id);
        if ($faq) {
            setSeo($faq->alias);
            $translations = Translation::where('group', FAQ::TRANS_GROUP)
                ->where('key', 'like', $faq->group . '.' . $faq->alias . '.%')->get();
            Translation::unguard();
            foreach ($translations as $translation) {
                $translation->delete();
            }
            Translation::reguard();
            $faq->delete();
            return redirect()->route('admin.faq');
        } else {
            abort(404);
        }
    }
}
