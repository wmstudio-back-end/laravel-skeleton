<?php

namespace Modules\FAQ\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class IndexController extends Controller
{
    private $questionCacheDir = 'storage/app/questions';

    /**
     * @return \Illuminate\Http\Response
     */
    public function question()
    {
        $user = \Auth::user();
        $data = getRequest();
        if (isset($data['ask-phone'])) {
            $data['ask-phone'] = phoneFormat($data['ask-phone'], true);
        }

        $validator = [
            'ask-question' => 'string|required',
        ];

        if (!$user) {
            $validator['ask-name'] = 'required|string';
            $validator['ask-email'] = 'required|string|email|max:255';
            $validator['ask-phone'] = 'required|regex:/^\+\d{11}/';
        }

        $validator = \Validator::make($data, $validator, [], [
            'ask-question' => '"' . ___('questions.modal.question') . '"',
            'ask-name' => '"' . ___('questions.modal.name') . '"',
            'ask-email' => '"' . ___('questions.modal.email') . '"',
            'ask-phone' => '"' . ___('questions.modal.phone') . '"',
        ]);

        if (!$validator->fails()) {
            $name = $user ? $user->getFullName() : $data['ask-name'];
            $email = $user ? $user->email : $data['ask-email'];
            $phone = phoneFormat($user ? $user->addon('phone') : $data['ask-phone']);
            $msg = trim($data['ask-question']) . "\n\n"
                . "----------------------------------------\n"
                . 'Имя:     ' . $name . "\n"
                . 'E-Mail:  ' . $email . "\n"
                . 'Телефон: ' . $phone;
            if ($mailTo = setting('email_notify')) {
                \Mail::raw($msg, function($message) use ($mailTo) {
                    $title = setting('main_title');
                    $from = ($from = env('MAIL_FROM_NAME')) ? $from . ' | ' . $title : $title;
                    $message->from(env('MAIL_FROM_ADDRESS'), $from)->to($mailTo)
                        ->subject('Вопрос в поддержку');
                });
            } else {
                $questionCacheDir = base_path($this->questionCacheDir);
                if (!file_exists($questionCacheDir)) {
                    mkdir($questionCacheDir);
                }
                $fName = str_slug((new \DateTime())->format('Y-m-d-H-i-s-') . $name);
                file_put_contents($questionCacheDir . '/' . $fName, $msg);
            }
            Input::merge([
                'errorMessage' => ___('questions.ask-result'),
            ]);
            Input::flash();
        } else {
            $errors = '';
            foreach ($validator->errors()->toArray() as $key => $error) {
                $errors .= array_shift($error) . '<br>';
            }
            $errors = substr($errors, 0, -4);
            Input::merge([
                'errorHeader' => ___('profile.type.teacher.error.header'),
                'errorMessage' => $errors,
            ]);
            Input::flash();
        }

        return redirect()->route('faq');
    }
}
