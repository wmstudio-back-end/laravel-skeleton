<?php
$getValue = function($value) {
    if ($value instanceof \Barryvdh\TranslationManager\Models\Translation) {
        return $value->value;
    } else {
        return $value;
    }
}
?>

<div class="row">
    <div class="form-group col-12">
        <label class="required-label">Информационное окно</label>
        <input
                type="text"
                class="form-control mb-2"
                name="content[{{ $lang }}][info][title]"
                placeholder="Заголовок для информационного окна"
                value="{{ isset(old('content')[$lang]['info']['title'])
                    ? old('content')[$lang]['info']['title']
                    : $getValue($value['info']['title']) ?: $lang == $defLang
                        ? $default['info']['title']
                        : '' }}"
        >
        <textarea
                name="content[{{ $lang }}][info][content]"
                class="form-control"
                placeholder="Содержание для информационного окна"
                rows="3"
        >{{ isset(old('content')[$lang]['info']['content'])
            ? old('content')[$lang]['info']['content']
            : str_replace("<br>", "\n", $getValue($value['info']['content']) ?: $lang == $defLang
                        ? $default['info']['content']
                        : '') }}</textarea>
    </div>
</div>
<div class="row faq-can-delete">
    <div class="form-group col-12">
        <label class="required-label">Советы</label>
        <div data-class="faq-advice" class="faq-type">
            <?php $advice = array_shift($value['advice']); ?>
            <textarea
                    name="content[{{ $lang }}][advice][]"
                    class="form-control mb-2"
                    data-id="0"
                    placeholder="Совет"
                    rows="3"
            >{{ isset(old('content')[$lang]['advice'][0])
                    ? old('content')[$lang]['advice'][0]
                    : (str_replace("<br>", "\n", $getValue($advice))
                        ?: ($lang == $defLang
                            ? $default['advice'][0]
                            : ''
                        )
                    ) }}</textarea>
            @for($i = 0; $i < $maxAdvice - 1; $i++)
                <div class="textarea-can-delete">
                    <textarea
                            name="content[{{ $lang }}][advice][]"
                            class="form-control can-delete mb-2"
                            data-id="{{ $i + 1 }}"
                            placeholder="Совет"
                            rows="3"
                    >{{ isset(old('content')[$lang]['advice'][$i + 1])
                        ? old('content')[$lang]['advice'][$i + 1]
                        : (isset($value['advice'][$i]) ?
                            str_replace("<br>", "\n", $getValue($value['advice'][$i]))
                            : ($lang == $defLang && isset($default['advice'][$i])
                                ? $default['advice'][$i]
                                : ''
                            )
                        ) }}</textarea>
                    <button type="button" class="btn btn-danger btn-action btn-sm textarea-delete" tabindex="-1">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            @endfor
        </div>
        <button type="button" class="btn btn-success btn-action btn-sm pull-right faq-add-textarea">
            <i class="fa fa-plus"></i> Добавить
        </button>
    </div>
</div>
<div class="row faq-can-delete">
    <div class="form-group col-12">
        <label class="required-label">Инструкции</label>
        <div data-class="faq-manual" class="faq-type">
            <?php $manual = array_shift($value['manual']); ?>
            <textarea
                    name="content[{{ $lang }}][manual][]"
                    class="form-control mb-2"
                    data-id="0"
                    placeholder="Инструкция"
                    rows="3"
            >{{ isset(old('content')[$lang]['manual'][0])
                    ? old('content')[$lang]['manual'][0]
                    : (str_replace("<br>", "\n", $getValue($manual))
                        ?: ($lang == $defLang
                            ? $default['manual'][0]
                            : ''
                        )
                    ) }}</textarea>
            @for($i = 0; $i < $maxManual - 1; $i++)
                <div class="textarea-can-delete">
                    <textarea
                            name="content[{{ $lang }}][manual][]"
                            class="form-control can-delete mb-2"
                            data-id="{{ $i + 1 }}"
                            placeholder="Инструкция"
                            rows="3"
                    >{{ isset(old('content')[$lang]['manual'][$i + 1])
                        ? old('content')[$lang]['manual'][$i + 1]
                        : (isset($value['manual'][$i])
                            ? str_replace("<br>", "\n", $getValue($value['manual'][$i]))
                            : ($lang == $defLang && isset($default['manual'][$i])
                                ? $default['manual'][$i]
                                : ''
                            )
                        ) }}</textarea>
                    <button type="button" class="btn btn-danger btn-action btn-sm textarea-delete" tabindex="-1">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            @endfor
        </div>
        <button type="button" class="btn btn-success btn-action btn-sm pull-right faq-add-textarea">
            <i class="fa fa-plus"></i> Добавить
        </button>
    </div>
</div>