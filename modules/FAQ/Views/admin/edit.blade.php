@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                @if($lastLevel = getMenu('admin')->lastLevel)
                    <div class="card-header">
                        <h4>{{ $lastLevel . ' вопрос' }}</h4>
                    </div>
                @endif
                <div class="card-body">
                    <?php /* @var $faq \Modules\FAQ\Entities\FAQ */ ?>
                    <form method="post" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <label>SEO</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_title" class="col-md-2 col-form-label">Заголовок</label>
                            <div class="col-md-10">
                                <input id="seo_title" name="seo_title" type="text" class="form-control{{ $errors->has('seo_title') ? ' is-invalid' : '' }}" value="{{ old('seo_title') ?: $seo['seo_title'] }}">
                                @if ($errors->has('seo_title'))
                                    <div class="invalid-feedback">{{ str_replace('seo_title ', '', $errors->first('seo_title')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_keywords" class="col-md-2 col-form-label">Ключевые слова</label>
                            <div class="col-md-10">
                                <input id="seo_keywords" name="seo_keywords" type="text" class="form-control{{ $errors->has('seo_keywords') ? ' is-invalid' : '' }}" value="{{ old('seo_keywords') ?: $seo['seo_keywords'] }}">
                                @if ($errors->has('seo_keywords'))
                                    <div class="invalid-feedback">{{ str_replace('seo_keywords ', '', $errors->first('seo_keywords')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_description" class="col-md-2 col-form-label">Описание</label>
                            <div class="col-md-10">
                                <input id="seo_description" name="seo_description" type="text" class="form-control{{ $errors->has('seo_description') ? ' is-invalid' : '' }}" value="{{ old('seo_description') ?: $seo['seo_description'] }}">
                                @if ($errors->has('seo_description'))
                                    <div class="invalid-feedback">{{ str_replace('seo_description ', '', $errors->first('seo_description')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <label>Параметры <i>(поля, помеченные красной звездочкой оязательные к заполнению)</i></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alias" class="col-md-2 col-form-label required-label">Адрес</label>
                            <div class="col-md-10">
                                <input
                                    id="alias"
                                    name="alias"
                                    type="text"
                                    class="form-control{{ $errors->has('alias') ? ' is-invalid' : '' }}"
                                    value="{{ old('alias') ?: $faq->alias }}"
                                    required="required"
                                >
                                @if ($errors->has('alias'))
                                    <div class="invalid-feedback">{{ str_replace('alias ', '', $errors->first('alias')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <label>
                                    Группа заполняются в виде slug<i>(латинские символы, символ тире "-", символ земля "_")</i>.<br>
                                    Переводы для групп находятся (или нужно создать) в локализации, в группе faq с ключем "groups.{имя группы}".<br>
                                    Например группа настройки (settings):<br>
                                    <b>Локализация -> {{ \Modules\FAQ\Entities\FAQ::TRANS_GROUP }} -> ключ "groups.settings"</b>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="group" class="col-md-2 col-form-label required-label">Группа</label>
                            <div class="col-md-10{{ $errors->has('group') ? ' has-invalid' : '' }}">
                                <select id="group" name="group" class="form-control selectpicker select-with-input" data-live-search="true" required="required">
                                    <option value=""{{ (old('group') == null && $faq->group == null) ? ' selected="selected"' : '' }}>Выберите...</option>
                                    @foreach ($groups as $group)
                                        <option
                                            value="{{ $group }}"
                                            {{ ((old('group') == $group) ? ' selected="selected"' :
                                                ($faq->group == $group ? ' selected="selected"' : '')) }}
                                        >{{ ($trans = transDef($group, 'faq.groups.' . $group)) == $group ? $group : $trans . ' (' . $group . ')' }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('group'))
                                    <div class="invalid-feedback">{{ str_replace('group ', '', $errors->first('group')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Название</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-name-{{ $lang }}-tab" data-toggle="tab" href="#lang-name-{{ $lang }}"
                                               role="tab" aria-controls="lang-name-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-name-{{ $lang }}" role="tabpanel" aria-labelledby="lang-name-tab">
                                            <input
                                                name="name-{{ $lang }}"
                                                type="text"
                                                class="form-control{{ $errors->has('name-' . $lang) ? ' is-invalid' : '' }}"
                                                value="{{ old('name-' . $lang) ?: isset($translations[$lang]['name'])
                                                    ? ($translations[$lang]['name'] instanceof \Barryvdh\TranslationManager\Models\Translation
                                                        ? $translations[$lang]['name']->value ?: ($defLang == $lang ? $faq->name : '')
                                                        : $translations[$lang]['name'] ?: ($defLang == $lang ? $faq->name : '')
                                                    ) : '' }}"
                                                @if($lang === $defLang)required="required"@endif
                                            >
                                        </div>
                                        @if ($errors->has('name-' . $lang))
                                            <div class="invalid-feedback">{{ str_replace("name-$lang ", '', $errors->first('name-' . $lang)) }}</div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('content-' . $defLang) ? ' has-invalid' : '' }}">
                            <label class="col-md-2 col-form-label required-label">Контент</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-content-{{ $lang }}-tab" data-toggle="tab" href="#lang-content-{{ $lang }}"
                                               role="tab" aria-controls="lang-content-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    <?php
                                        $maxAdvice = 0;
                                        $maxManual = 0;
                                        $default = json_decode($faq->content, true);
                                        foreach ($locales as $lang) {
                                            if (!isset($translations[$lang]['content'])) {
                                                if ($defLang == $lang) {
                                                    $translations[$lang]['content'] = $default;
                                                } else {
                                                    $translations[$lang]['content'] = Modules\FAQ\Entities\FAQ::EMPTY_CONTENT;
                                                }
                                            }
                                            if ($maxAdvice < count($translations[$lang]['content']['advice'])) {
                                                $maxAdvice = count($translations[$lang]['content']['advice']);
                                            }
                                            if ($maxManual < count($translations[$lang]['content']['manual'])) {
                                                $maxManual = count($translations[$lang]['content']['manual']);
                                            }
                                        }
//                                        exit;
                                    ?>
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                            id="lang-content-{{ $lang }}" role="tabpanel" aria-labelledby="lang-content-tab">
                                            @include('faq::admin.partials.content', [
                                                'value' => $translations[$lang]['content'],
                                                'lang' => $lang,
                                                'defLang' => $defLang,
                                                'default' => $default,
                                                'maxAdvice' => $maxAdvice,
                                                'maxManual' => $maxManual,
                                            ])
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @if ($errors->has('content-' . $defLang))
                                <div class="offset-md-2 col-md-10 invalid-feedback">{{ $errors->first('content-' . $defLang) }}</div>
                            @endif
                        </div>
                        <div class="form-group row">
                            <label for="form" class="col-md-2 col-form-label">Форма</label>
                            <div class="col-md-10">
                                <select id="form" name="form" class="form-control selectpicker" data-live-search="true">
                                    <option value=""{{ (old('form') == null && $faq->form_name == null) ? ' selected="selected"' : '' }}>Нет</option>
                                    @foreach ($forms as $formAlias => $formName)
                                        <option value="{{ $formAlias }}"{{ ((old('form') == $formAlias) ? ' selected="selected"' : ($faq->form_name == $formAlias ? ' selected="selected"' : '')) }}>
                                            {{ $formName }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('form'))
                                    <div class="invalid-feedback">{{ str_replace('form ', '', $errors->first('form')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-1" value="active">
                                    <input type="checkbox" class="custom-control-input" id="active" name="active" value="1"
                                           @if ($faq->active)checked="checked"@endif>
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label required-label">Дата</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input id="date" name="date" type="text" class="form-control datetimepicker" value="{{ date_format($faq->date_active, 'd.m.Y H:i') }}" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-action btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection