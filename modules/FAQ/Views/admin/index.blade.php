<?php
$canEdit = getPermissions('admin.faq.edit');
$canDelete = getPermissions('admin.faq.delete');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список вопросов и ответов</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.faq.edit', ['id' =>'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="col-lg-12 project-list">
                        <table class="table table-hover">
                            <tbody>
                            @if (count($faq) == 0)
                                <tr>
                                    <td style="text-align: center;">
                                        Ни однго вопроса пока не созданно.
                                    </td>
                                </tr>
                            @else
                                @foreach ($faq as $question)
                                    <? /* @var $question \Modules\FAQ\Entities\FAQ */ ?>
                                    <tr>
                                        <td>
                                            <? if ($question->active) {
                                                $status = 'Активна';
                                                $activeClass = 'badge-success';
                                            } else {
                                                $status = 'Не активна';
                                                $activeClass = 'badge-danger';
                                            }
                                            ?>
                                            <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                        </td>
                                        <td class="project-title">
                                            <h4>{{ $question->name }}</h4>
                                            <small>Дата активации {{ date_format($question->date_active, 'd.m.Y  H:i:s') }}</small>
                                        </td>
                                        <td class="project-group">
                                            <b>Группа:</b><br>
                                            <small>{{ transDef($question->group, 'faq.groups.' . $question->group) }}</small>
                                        </td>
                                        <td class="project-alias">
                                            <b>Адрес:</b><br>
                                            <small><a href="{{ route('faq.question', [
                                                'group' => $question->group,
                                                'alias' => $question->alias,
                                            ]) }}" target="_blank">{{ route('faq.question', [
                                                'group' => $question->group,
                                                'alias' => $question->alias,
                                            ]) }}</a></small>
                                        </td>
                                        <td class="project-group">
                                            <b>Полезность:</b><br>
                                            <?php $useful = json_decode($question->useful, true); ?>
                                            <span class="badge badge-success">{{ ($useful['yes'] ? '+' : '') . $useful['yes'] }}</span>
                                            <span class="badge badge-danger">{{ ($useful['no'] ? '-' : '') . $useful['no'] }}</span>
                                        </td>
                                        <td class="project-actions">
                                            @if ($canDelete)
                                                <form action="{{ route('admin.faq.delete', ['id' =>$question->id]) }}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-action btn-danger pull-right"><i class="fa fa-times"></i> Удалить</button>
                                                </form>
                                            @endif
                                            @if ($canEdit)
                                                <a href="{{ route('admin.faq.edit', ['id' =>$question->id]) }}"
                                                   class="btn btn-action btn-warning pull-right"><i class="fa fa-pencil"></i> Редактировать</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $faq->links() }}
                    </div>
                    @if ($canEdit)
                        <a href="{{ route('admin.faq.edit', ['id' =>'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
