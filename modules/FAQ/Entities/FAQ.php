<?php

namespace Modules\FAQ\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Page
 *
 * @property int $id
 * @property string $alias
 * @property string $group
 * @property string $name
 * @property string $content
 * @property string $form_name
 * @property bool $active
 * @property string $useful
 * @property \Carbon\Carbon $date_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class FAQ extends Eloquent
{
    const TRANS_GROUP = 'faq';

    protected $table = 'faq';

    protected $casts = ['active' => 'bool'];

    protected $dates = ['date_active'];

    protected $fillable = ['alias', 'group', 'name', 'content', 'form_name', 'active', 'useful', 'date_active'];

    const EMPTY_CONTENT = [
        'info' => [
            'title' => '',
            'content' => '',
        ],
        'advice' => [
            0 => '',
        ],
        'manual' => [
            0 => '',
        ],
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('content', $attributes)) {
            $this->content = json_encode(self::EMPTY_CONTENT);
        }
        if (!array_key_exists('useful', $attributes)) {
            $this->useful = json_encode([
                'yes' => 0,
                'no' => 0,
            ]);
        }
        if (!array_key_exists('active', $attributes)) {
            $this->active = true;
        }
        if (!array_key_exists('date_active', $attributes)) {
            $this->date_active = new \DateTime();
        }
    }
}
