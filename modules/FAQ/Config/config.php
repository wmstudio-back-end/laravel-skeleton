<?php

$name = 'F.A.Q.';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.faq',
                    'label' => 'Просмотр списка вопросов и ответов',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/edit/{id}',
                'parameters' => [
                    'as' => 'admin.faq.edit',
                    'label' => 'Создание и редактирование вопросов и ответов',
                    'uses' => 'AdminController@edit',
                ],
                'where' => ['id' => 'new|[0-9]+'],
            ],
            [
                'method' => 'post',
                'uri' => '/delete/{id}',
                'parameters' => [
                    'as' => 'admin.faq.delete',
                    'label' => 'Удаление вопросов и ответов',
                    'uses' => 'AdminController@delete',
                ],
                'where' => ['id' => '[0-9]+'],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.faq',
                'icon'      => 'fa fa-question-circle-o',
                'order'     => 40,
            ],
        ],
    ],
];
