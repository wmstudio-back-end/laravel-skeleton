<?php

namespace Modules\Users\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Auth;
use Modules\Users\Entities\User;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class SocialController extends Controller
{
    protected function getDriver($provider)
    {
        $driver = \Socialite::driver($provider);
        if (in_array('redirectUrl', get_class_methods(\Socialite::driver($provider)))) {
            $driver->redirectUrl(route('social.callback', ['provider' => $provider]));
        }

        return $driver;
    }

    public function provider($provider)
    {
        if (!\Session::has('login_previous_url')) {
            \Session::put('login_previous_url', url()->previous());
        }

        $data = getRequest();
        if (\Auth::user() && !isset($data['redirect-to'])) {
            \Session::put('login_previous_url', $data['redirect-to']);
            $result = redirect()->to($this->redirectTo());
        }

        if (isset($data['cam-ref-link'])) {
            \Session::put('login_referral', $data['cam-ref-link']);
        }

        if (!isset($result)) {
            $result = $this->getDriver($provider)->redirect();
        }

        if (request()->ajax()) {
            return response()->json(['redirect' => $result->getTargetUrl()]);
        } else {
            return $result;
        }
    }

    private function getUserInfo($provider)
    {
        $user = $this->getDriver($provider)->user();
        $result = [
            'id' => null,
            'login' => null,
            'email' => null,
            'first_name' => null,
            'middle_name' => null,
            'last_name' => null,
            'provider' => $provider,
        ];

        /* @var $user \Laravel\Socialite\AbstractUser */
        $result['id'] = $user->getId();
        $result['login'] = $user->getNickname();
        $result['email'] = $user->getEmail();

        $name = splitName($user->getName());
        if (!empty($name['first_name'])) {
            $result['first_name'] = $name['first_name'];
        }
        if (!empty($name['middle_name'])) {
            $result['middle_name'] = $name['middle_name'];
        }
        if (!empty($name['last_name'])) {
            $result['last_name'] = $name['last_name'];
        }

        switch ($provider) {
            case 'vkontakte':
                /* @var $user \Laravel\Socialite\AbstractUser */
                if (empty($result['email']) && isset($user->accessTokenResponseBody['email'])) {
                    $result['email'] = $user->accessTokenResponseBody['email'];
                }
                if (isset($user->user['first_name'])) {
                    $result['first_name'] = $user->user['first_name'];
                }
                if (isset($user->user['last_name'])) {
                    $result['last_name'] = $user->user['last_name'];
                }
                break;
            case 'facebook':
                break;
            case 'odnoklassniki':
                if (isset($user->user['first_name'])) {
                    $result['first_name'] = $user->user['first_name'];
                }
                if (isset($user->user['last_name'])) {
                    $result['last_name'] = $user->user['last_name'];
                }
                break;
            case 'twitter':
                break;
            case 'google':
                if (isset($user->user['name']['givenName'])) {
                    $result['first_name'] = $user->user['name']['givenName'];
                }
                if (isset($user->user['name']['familyName'])) {
                    $result['last_name'] = $user->user['name']['familyName'];
                }
                break;
            case 'instagram':
                break;
        }

        return $result;
    }

    public function callback($provider)
    {
        try {
            $userInfo = $this->getUserInfo($provider);
            $authUser = \Auth::user();
            $user = null;
            if (!empty($userInfo['id'])) {
                $addon = UserAddon::where('alias', 'social_' . $provider)->first();
                if ($addon) {
                    $value = UserAddValue::where([
                        'addon_id' => $addon->id,
                        'value' => $userInfo['id'],
                    ])->first();
                    if ($value) {
                        $user = $value->user;
                    }
                }
            }
            if (!$user && !$authUser && !empty($userInfo['email'])) {
                $user = User::where('email', $userInfo['email'])->first();
            }

            $setAddon = function($user) use($userInfo, $provider) {
                $addon = UserAddon::where('alias', 'social_' . $provider)->first();
                if ($addon) {
                    $value = new UserAddValue();
                    $value->user_id = $user->id;
                    $value->addon_id = $addon->id;
                    $value->value = $userInfo['id'];
                    $value->save();
                }

                \Auth::guard()->login($user);
                return redirect()->to($this->redirectTo());
            };
            if ($user) {
                if ($authUser) {
                    if ($authUser->id === $user->id) {
                        return $setAddon($user);
                    } else {
                        return redirect()->to($this->redirectTo())->withInput([
                            'errorHeader' => ___('auth.error'),
                            'errorMessage' => ___('auth.social-is-used'),
                        ]);
                    }
                } else {
                    \Auth::guard()->login($user);
                    return redirect()->to($this->redirectTo());
                }
            } else {
                if (!$authUser) {
                    if (\Session::has('login_referral')) {
                        $userInfo['referral'] = \Session::get('login_referral');
                        \Session::forget('login_referral');
                    }
                    $userInfo['social-email'] = $userInfo['email'];

                    return redirect()->route('register')->withInput(RegisterController::transformInputNames($userInfo));
                } else {
                    return $setAddon($authUser);
                }
            }
        } catch (\Exception $e) {
            $input['social-error'] = ___('auth.social-error', [
                'provider' => $provider
            ]);
            if (\Session::has('login_referral')) {
                $input['referral'] = \Session::get('login_referral');
                \Session::forget('login_referral');
            }
            return redirect()->route('login')->withInput($input);
        }
    }
}
