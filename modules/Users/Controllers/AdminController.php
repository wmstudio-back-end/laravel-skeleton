<?php

namespace Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Support\Facades\Input;
use Modules\Users\Entities\User;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddSettings;
use Modules\Users\Entities\UserAddValue;
use Modules\Users\Entities\UserPermission as Permissions;
use Modules\Users\Entities\UserPermissionsGroup as Groups;

class AdminController extends Controller
{
    protected $usersPerPage = 40;

    public static function writeRequired()
    {
        return [
            'edit',
            'delete',
            'groupsEdit',
            'add',
            'groupsDelete',
            'settings',
            'listGroup',
            'requestTeacherPermissions',
        ];
    }

    public function index()
    {
        $users = User::paginate($this->usersPerPage);

        return view('users::admin.index', [
            'users' => $users,
        ]);
    }

    public function show(int $id)
    {
        return $this->edit($id, true);
    }

    public function edit(int $id, $show = false)
    {
        $user = User::find($id);

        if ($user) {
            $menu = getMenu('admin');
            /* @var $menu \Modules\MCms\Navigation\Navigation */
            $menu->findBy('route', 'admin.users')->setActive();
            $menu->lastLevel = ($show ? 'Пользователь "' : 'Редактирование пользователя "') . $user->login . '"';

            if (request()->isMethod('POST')) {
                $data = getRequest();
                if (!isset($data['status'])) {
                    $data['status'] = [];
                }
                if (is_array($data['status'])) {
                    $data['status'] = implode('', $data['status']);
                }

                $validator = \Validator::make([], []);

                if ($data['new-password']) {
                    if ($data['new-password-confirm'] && $data['new-password'] == $data['new-password-confirm']) {
                        $user->password = \Hash::make($data['new-password']);
                    } elseif ($data['new-password-confirm'] && $data['new-password'] != $data['new-password-confirm']) {
                        $validator->errors()->add('new-password-confirm', 'Пароли должны совпадать');
                    }
                }
                if (isset($data['login'])) {
                    if ($data['login'] != $user->login) {
                        $findUser = User::where('login', $data['login'])->first();
                        if ($findUser && $findUser->id != $user->id) {
                            $validator->errors()->add('login', 'Пользователь с таким логином уже существует');
                        } else {
                            $user->login = $data['login'];
                        }
                    }
                } else {
                    $validator->errors()->add('login', 'Поле "Логин" не может быть пустым');
                }
                if (isset($data['email'])) {
                    if ($data['email'] != $user->email) {
                        $findUser = User::where('email', $data['email'])->first();
                        if ($findUser && $findUser->id != $user->id) {
                            $validator->errors()->add('email', 'Пользователь с таким Email-адресом уже существует');
                        } else {
                            $user->email = $data['email'];
                        }
                    }
                } else {
                    $validator->errors()->add('email', 'Поле "Email" не может быть пустым');
                }

                if (isset($data['email_confirmed'])) {
                    $user->email_confirmed = $data['email_confirmed'];
                }

                if (isset($data['active'])) {
                    $user->active = $data['active'];
                }

                if ($validator->errors()->isNotEmpty()) {
                    return redirect()->back()->withInput()->withErrors($validator);
                }

                $user->save();

                foreach ($data as $key => $val) {
                    if (is_array($val)) {
                        $tmp = [];
                        foreach ($val as $item) {
                            $tmp[$item] = true;
                        }
                        $data[$key] = json_encode($tmp);
                    }
                }

                UserAddValue::unguard();
                foreach (UserAddon::all() as $add) {
                    /* @var $add UserAddon */
                    if (array_key_exists($add->alias, $data)) {
                        $addValue = UserAddValue::where([
                            'user_id' => $user->id,
                            'addon_id' => $add->id,
                        ])->first();
                        if ($data[$add->alias] === null) {
                            if ($addValue) {
                                $addValue->delete();
                            }
                        } else {
                            if (!$addValue) {
                                $addValue = new UserAddValue();
                                $addValue->user_id = $user->id;
                                $addValue->addon_id = $add->id;
                            }
                            if ($add->alias == 'referral_token' && $addValue->value != $data[$add->alias]) {
                                $addValue->value = md5($user->id . 'referral' . time());
                            } else {
                                $addValue->value = $data[$add->alias];
                            }
                            $addValue->save();
                        }
                    }
                }
                UserAddValue::reguard();

                return redirect()->route('admin.users.show', ['id' => $user->id])->withInput([]);
            }

            return view('users::admin.edit', [
                'user' => $user,
                'show' => $show,
            ]);
        } else {
            abort(404);
        }
    }

    public function delete($id)
    {
        $user = User::find($id);

        if ($user) {
            $user->delete();
            return redirect()->route('admin.users');
        } else {
            abort(404);
        }
    }

    public function groups()
    {
        $groups = Groups::orderBy('name', 'ASC')->get();

        return view('users::admin.groups', [
            'groups' => $groups,
        ]);
    }

    public function groupsEdit($id)
    {
        if (request()->isMethod('POST')) {
            $data = getRequest();
            if ($id === 'new') {
                $validator = \Validator::make($data, [
                    'group-name' => 'string|required|min:4|max:255',
                ]);

                if (!$validator->fails()) {
                    $group = new Groups();
                    $group->name = $data['group-name'];
                    $group->active = true;
                    $group->save();

                    return redirect()->route('admin.users.group.edit', ['id' => $group->id]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            } else {
                $group = Groups::find($id);
                if ($group) {
                    $setPermissions = setPermissions(Permissions::LINK_GROUP, $group->id, $data);

                    if ($setPermissions === true) {
                        return redirect()->back();
                    } else {
                        $errMsg = '';
                        if (isset($setPermissions['header'])) {
                            $errMsg = $setPermissions['header'] . '<br>';
                            unset($setPermissions['header']);
                        }

                        $last = array_pop($setPermissions);
                        foreach ($setPermissions as $val) {
                            $errMsg .= ' * ' . $val . "<br>";
                        }
                        $errMsg .= ' * ' . $last;

                        Input::replace([
                            'toastr.error' => true,
                            'toastr.msgHeader' => 'Ошибка.',
                            'toastr.message' => $errMsg,
                        ]);
                        return redirect()->back()->withInput();
                    }
                } else {
                    abort(404);
                }
            }
        } else {
            $group = Groups::find($id);
            if ($group) {
                $menu = getMenu('admin');
                /* @var $menu \Modules\MCms\Navigation\Navigation */
                $menu->findBy('route', 'admin.users.groups')->setActive();
                $menu->lastLevel = 'Редактирование группы';

                $routes = app('router')->getRoutes();
                $prefix = 'admin';
                $adminRoutes = [];
                $activeRoutes = [];
                $menu = getMenu('admin');
                foreach ($routes as $route) {
                    /* @var $route \Illuminate\Routing\Route */
                    if (substr($route->getName(), 0, strlen($prefix)) == $prefix) {
                        $moduleName = $route->getPrefix();
                        if (strpos($moduleName, 'admin/') !== false) {
                            $moduleName = str_replace('admin/', '', $moduleName);
                        }
                        $moduleName = config($moduleName . '.name') ?: $moduleName;

                        if (!isset($adminRoutes[$route->getPrefix()]['name'])) {
                            $adminRoutes[$route->getPrefix()]['name'] = $moduleName;
                        }

                        if ($menuEl = $menu->findBy('route', $route->getName())) {
                            $order = $menuEl->getOrder();
                            if (!$order && ($parent = $menuEl->getParent()) instanceof \Modules\MCms\Navigation\Page\AbstractPage) {
                                $order = $parent->getOrder();
                            }
                            $adminRoutes[$route->getPrefix()]['order'] = $order;
                        }

                        $adminRoutes[$route->getPrefix()]['routes'][$route->getName()] = isset($route->action['label']) ? $route->action['label'] : '';
                        $activeRoutes[$route->getPrefix()] = true;
                        $activeRoutes[$route->getName()] = true;
                    }
                }

                uasort($adminRoutes, function($prev, $curr){
                    $prevOrder = isset($prev['order']) ? $prev['order'] : 9999;
                    $currOrder = isset($curr['order']) ? $curr['order'] : 9999;

                    if ($prevOrder == $currOrder) {
                        return 0;
                    }
                    return ($prevOrder < $currOrder) ? -1 : 1;
                });

                $activeRoutes = array_keys($activeRoutes);

                $permissions = Permissions::where([
                    'link_to' => Permissions::LINK_GROUP,
                    'parent_id' => $group->id,
                ])->whereIn('route_name', $activeRoutes)->get()->keyBy('route_name');

                return view('users::admin.groups-edit', [
                    'groupObj' => $group,
                    'routes' => $adminRoutes,
                    'permissions' => $permissions,
                ]);
            } else {
                abort(404);
            }
        }
    }

    public function groupsDelete($id)
    {
        $group = Groups::find($id);
        if ($group) {
            $permissions = Permissions::where([
                'link_to' => Permissions::LINK_GROUP,
                'parent_id' => $group->id,
            ])->get();

            Permissions::unguard();
            foreach ($permissions as $permission) {
                $permission->delete();
            }
            Permissions::reguard();
            $group->delete();

            return redirect()->route('admin.users.groups');
        } else {
            abort(404);
        }
    }

    public function listGroup($id)
    {
        $group = Groups::find($id);
        if ($group) {
            if (request()->isMethod('POST')) {
                $data = getRequest();
                switch ($data['action']) {
                    case 'get-users':
                        $search = explode(' ', $data['search']);
                        $users = User::where('permission_group', '!=',  $group->id)
                            ->where(function($query) use($search) {
                                $query->whereHas('user_add_values', function($query) use($search) {
                                    $query->whereHas('user_addon', function($query) use($search) {
                                        $query->where('alias', 'name');
                                    })->where(function($query) use($search) {
                                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                                        foreach ($search as $val) {
                                            $query->orWhere('value', 'like', '%' . $val . '%');
                                        }
                                    });
                                })->orWhereHas('user_add_values', function($query) use($search) {
                                    $query->whereHas('user_addon', function($query) use($search) {
                                        $query->where('alias', 'surname');
                                    })->where(function($query) use($search) {
                                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                                        foreach ($search as $val) {
                                            $query->orWhere('value', 'like', '%' . $val . '%');
                                        }
                                    });
                                })->orWhereHas('user_add_values', function($query) use($search) {
                                    $query->whereHas('user_addon', function($query) use($search) {
                                        $query->where('alias', 'middle_name');
                                    })->where(function($query) use($search) {
                                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                                        foreach ($search as $val) {
                                            $query->orWhere('value', 'like', '%' . $val . '%');
                                        }
                                    });
                                })->orWhereHas('user_add_values', function($query) use($search) {
                                    $query->whereHas('user_addon', function($query) use($search) {
                                        $query->where('alias', 'phone');
                                    })->where(function($query) use($search) {
                                        $query->where('value', 'like', '%' . array_shift($search) . '%');
                                    });
                                })->orWhere(function($query) use($search) {
                                    $query->where('login', 'like', '%' . array_shift($search) . '%');
                                    foreach ($search as $val) {
                                        $query->orWhere('login', 'like', '%' . $val . '%');
                                    }
                                })->orWhere('email', 'like', '%' . array_shift($search) . '%');
                            })->get();

                        $result = [];
                        if ($users) {
                            foreach ($users as $user) {
                                /* @var $user User */
                                $result[] = [
                                    'id' => $user->id, 'text' => $user->getFullName(),
                                ];
                            }
                        }

                        return response()->json(['list' => $result]);
                        break;
                    case 'add-user':
                        $setGroup = $group->id;
                    case 'removeUser':
                        if (!isset($setGroup)) {
                            $setGroup = -1;
                        }
                        $user = User::find($data['id']);
                        if ($user) {
                            if ($user->id == \Auth::user()->id) {
                                $msgHeader = 'Ошибка.';
                                $message = 'В целях безопасности, запрещено перемещать себя между группами.';
                                if (request()->ajax()) {
                                    return response()->json([
                                        'error' => true,
                                        'msgHeader' => $msgHeader,
                                        'message' => $message,
                                    ]);
                                } else {
                                    Input::replace([
                                        'toastr.error' => true,
                                        'toastr.msgHeader' => $msgHeader,
                                        'toastr.message' => $message,
                                    ]);

                                    return redirect()->back()->withInput();
                                }
                            } else {
                                $user->permission_group = $setGroup;
                                $user->save();

                                if (request()->ajax()) {
                                    return response()->json(['error' => false]);
                                } else {
                                    return redirect()->back();
                                }
                            }
                        } else {
                            $msgHeader = 'Ошибка.';
                            $message = 'Пользователь не найден.';
                            if (request()->ajax()) {
                                return response()->json([
                                    'error' => true,
                                    'msgHeader' => $msgHeader,
                                    'message' => $message,
                                ]);
                            } else {
                                Input::replace([
                                    'toastr.error' => true,
                                    'toastr.msgHeader' => $msgHeader,
                                    'toastr.message' => $message,
                                ]);

                                return redirect()->back()->withInput();
                            }
                        }
                        break;
                }
            } else {
                $menu = getMenu('admin');
                /* @var $menu \Modules\MCms\Navigation\Navigation */
                $menu->findBy('route', 'admin.users.groups')->setActive();
                $menu->lastLevel = 'Редактирование списка пользователей группы';

                $users = User::where('permission_group', $group->id)
                    ->paginate($this->adminEntitiesPerPage * 2);
                return view('users::admin.list-group', [
                    'group' => $group,
                    'users' => $users,
                ]);
            }
        } else {
            abort(404);
        }
    }

    public function settings()
    {
        $settings = UserAddSettings::all()->keyBy('control_type');

        $transKeys = [];
        foreach ($settings as $setting) {
            foreach ($setting->value as $key) {
                $transKeys[$setting->control_type . '.' . $key] = $key;
            }
        }
        $translates = Translation::where([
            'locale' => config('app.locale'),
            'group' => UserAddSettings::TRANS_GROUP,
        ])->whereIn('key', array_keys($transKeys));
        if (request()->isMethod('POST')) {
            $translates = $translates->get()->keyBy('key');
            $transCount = $translates->count();
        } else {
            $transCount = $translates->count('id');
        }
        $needPublish = count($transKeys) != $transCount;

        if (request()->isMethod('POST')) {
            $data = getRequest();
            if (count($data)) {
                if (isset($data['publish'])) {
                    if ($needPublish) {
                        foreach ($translates as $key => $translate) {
                            if (isset($transKeys[$key])) {
                                unset($transKeys[$key]);
                            }
                        }
                        $insertTrans = [];
                        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
                        foreach ($transKeys as $key => $val) {
                            $insertTrans[] = [
                                'status'     => Translation::STATUS_CHANGED,
                                'locale'     => config('app.locale'),
                                'group'      => UserAddSettings::TRANS_GROUP,
                                'key'        => $key,
                                'value'      => $val,
                                'created_at' => $timeStamp,
                                'updated_at' => $timeStamp,
                            ];
                        }
                        Translation::unguard();
                        Translation::insert($insertTrans);
                        Translation::reguard();
                    }
                } else {
                    UserAddSettings::unguard();
                    foreach ($data as $key => $val) {
                        $value = $settings[$key]->value;
                        $new = [];
                        if (isset($val['new'])) {
                            $new = $val['new'];
                            unset($val['new']);
                        }
                        foreach ($val as $id => $val) {
                            if (isset($value[$id])) {
                                unset($value[$id]);
                            }
                        }
                        foreach ($new as $val) {
                            array_push($value, str_slug($val));
                        }
                        $settings[$key]->value = $value;
                        $settings[$key]->save();
                    }
                    UserAddSettings::reguard();
                }
            }
            return redirect()->back();
        }

        return view('users::admin.settings', [
            'settings' => $settings,
            'needPublish' => $needPublish,
        ]);
    }

    public function requestTeacherPermissions()
    {
        if (request()->isMethod('POST')) {
            $result = request()->get('result', false) ?: false;
            $user = User::find(request()->get('user-id', null));
            if ($user) {
                /* @var $user User */
                $user->setAddon('tc_was_permitted', true);
                $user->setAddon('tc_can_teach', $result);
                if ($result) {
                    $status = $user->addon('status');
                    if (strpos($status, 's') !== false) {
                        $result = 'st';
                    } else {
                        $result = 't';
                    }
                    $user->setAddon('status', $result);
                }
            }
            return redirect()->route('admin.users.request-t-p');
        }

        $users = usersWithTeacherRequest()->paginate($this->usersPerPage);
        $fields = [
            'tc_about_me',
            'tc_dialect',
            'tc_diploma_url',
            'tc_education',
            'tc_exp',
            'tc_lesson_type',
            'tc_prefer_level',
            'tc_profession',
            'tc_style',
        ];
        $fillFields = [];
        foreach ($users as $user) {
            $fillFields[$user->id] = 0;
            foreach ($fields as $field) {
                if (!empty($user->addon($field))) {
                    $fillFields[$user->id]++;
                }
            }
        }

        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.users')->setActive();
        $menu->lastLevel = 'Список пользователей, отправивших запрос на права преподавателя';

        return view('users::admin.set-status', [
            'users' => $users,
            'fillFields' => $fillFields,
            'fieldsCount' => count($fields),
        ]);
    }
}
