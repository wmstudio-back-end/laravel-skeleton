<?php

namespace Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use Modules\Users\Entities\User;

class IndexController extends Controller
{
    protected $searchLimit = 30;

    protected function success()
    {
        if (request()->ajax()) {
            return ['success' => true];
        } else {
            return redirect()->back();
        }
    }

    public function selectLang($lang)
    {
        $locates = app('translation-manager')->getLocales();
        if (in_array($lang, $locates)) {
            \Session::put('lang', $lang);
            if ($user = \Auth::user()) {
                $value = $user->addon('lang', true);

                if ($value) {
                    $value->value = $lang;
                    $value->save();
                }
            }
        }

        return $this->success();
    }

    public function setCurrency()
    {
        if ($currency = request()->post('currency')) {
            \Session::put('user-currency', $currency);

            return $this->success();
        } else {
            if (request()->ajax()) {
                return ['error' => 'The "currency" parameter is empty'];
            } else {
                return redirect()->back();
            }
        }
    }

    /**
     * @param array $filter
     * @param array $opts
     * @return \Illuminate\Database\Eloquent\Builder|null
     */
    public function getFindUsersQuery($filter, $opts = [])
    {
        $user = \Auth::user();
        $status = $user->getStatus();
        $query = null;
        if ($status) {
            $status = (isset($opts['status']) && ($opts['status'] === 't' || $opts['status'] === 's'))
                ? $opts['status']
                : ($status === 't' ? 's' : 't');
            $prefix = 'st_';

            $query = User::where('active', true)
                ->where('id', '!=', $user->id)
                ->whereHas('user_add_values', function($query) use ($status) {
                    $query->whereHas('user_addon', function($query) {
                        $query->where('alias', 'status');
                    })->where(function($query) use ($status) {
                        $query->where('value', 'like', "%$status%");
                    });
                });

            if (isset($filter['favorites'])) {
                $query->whereIn('id', array_keys($user->getFavorites()));
            }
            if (isset($filter['online'])) {
                $online = getOnlineUsers();
                $online = is_array($online) && count($online) ? array_keys($online) : [''];
                $query->whereIn('id', $online);
            }

            switch ($status) {
                case 's':
                    if (isset($filter['dialect']) || isset($filter['no-dialect'])) {
                        $query->where(function($query) use ($filter) {
                            $noDialect = isset($filter['no-dialect']);
                            if (isset($filter['dialect'])) {
                                $dialect = array_keys($filter['dialect']);
                                $query->whereHas('user_add_values', function($query) use ($dialect, $noDialect) {
                                    $query->where(function($query) use ($dialect, $noDialect) {
                                        $query->whereHas('user_addon', function($query) use ($dialect, $noDialect) {
                                            $query->where('alias', 'st_dialect');
                                        })->where(function($query) use ($dialect, $noDialect) {
                                            $query->where('value', 'like', '%"' . array_shift($dialect) . '":true%');
                                            foreach ($dialect as $val) {
                                                $query->orWhere('value', 'like', "%\"$val\":true%");
                                            }
                                            if ($noDialect) {
                                                $query->orWhere('value', '[]');
                                            }
                                        });
                                    });
                                });
                                if ($noDialect) {
                                    $query->orWhereDoesntHave('user_add_values', function($query) {
                                        $query->whereHas('user_addon', function($query) {
                                            $query->where('alias', 'st_dialect');
                                        });
                                    });
                                }
                            } else {
                                $query->whereHas('user_add_values', function($query) {
                                    $query->where(function($query) {
                                        $query->whereHas('user_addon', function($query) {
                                            $query->where('alias', 'st_dialect');
                                        })->where(function($query) {
                                            $query->orWhere('value', '[]');

                                        });
                                    });
                                })->orWhereDoesntHave('user_add_values', function($query) {
                                    $query->whereHas('user_addon', function($query) {
                                        $query->where('alias', 'st_dialect');
                                    });
                                });
                            }
                        });
                    }
                    if (isset($filter['lang-level'])) {
                        $langLevel = array_keys($filter['lang-level']);
                        $query->whereHas('user_add_values', function($query) use($langLevel) {
                            $query->whereHas('user_addon', function($query) use($langLevel) {
                                $query->where('alias', 'st_lang_level');
                            })->where(function($query) use($langLevel) {
                                $query->where('value', array_shift($langLevel));
                                foreach ($langLevel as $val) {
                                    $query->orWhere('value', $val);
                                }
                            });
                        });
                    }
                    break;
                case 't':
                    $prefix = 'tc_';
                    if (isset($filter['dialect'])) {
                        $dialect = array_keys($filter['dialect']);
                        $query->whereHas('user_add_values', function($query) use($dialect) {
                            $query->whereHas('user_addon', function($query) use($dialect) {
                                $query->where('alias', 'tc_dialect');
                            })->where(function($query) use($dialect) {
                                $query->where('value', 'like', '%"' . array_shift($dialect) . '":true%');
                                foreach ($dialect as $val) {
                                    $query->orWhere('value', 'like', "%\"$val\":true%");
                                }
                            });
                        });
                    }
                    if (isset($filter['teach-style'])) {
                        $teachStyle = array_keys($filter['teach-style']);
                        $query->whereHas('user_add_values', function($query) use($teachStyle) {
                            $query->whereHas('user_addon', function($query) use($teachStyle) {
                                $query->where('alias', 'tc_style');
                            })->where(function($query) use($teachStyle) {
                                $query->where('value', array_shift($teachStyle));
                                foreach ($teachStyle as $val) {
                                    $query->orWhere('value', $val);
                                }
                            });
                        });
                    }
                    break;
            }

            if (isset($filter['lesson-type'])) {
                $lessonType = array_keys($filter['lesson-type']);
                $query->whereHas('user_add_values', function($query) use($lessonType, $prefix) {
                    $query->whereHas('user_addon', function($query) use($lessonType, $prefix) {
                        $query->where('alias', "${prefix}lesson_type");
                    })->where(function($query) use($lessonType) {
                        $query->where('value', 'like', '%"' . array_shift($lessonType) . '":true%');
                        foreach ($lessonType as $val) {
                            $query->orWhere('value', 'like', "%\"$val\":true%");
                        }
                    });
                });
            }

            if (isset($filter['speak-langs'])) {
                $speakLanguages = array_keys($filter['speak-langs']);
                $query->whereHas('user_add_values', function($query) use($speakLanguages) {
                    $query->whereHas('user_addon', function($query) use($speakLanguages) {
                        $query->where('alias', 'speak_langs');
                    })->where(function($query) use($speakLanguages) {
                        $query->where('value', 'like', '%"' . array_shift($speakLanguages) . '":true%');
                        foreach ($speakLanguages as $val) {
                            $query->orWhere('value', 'like', "%\"$val\":true%");
                        }
                    });
                });
            }

            if (isset($filter['search'])) {
                $search = explode(' ', $filter['search']);
                $query->where(function($query) use($search, $opts) {
                    $query->whereHas('user_add_values', function($query) use($search) {
                        $query->whereHas('user_addon', function($query) use($search) {
                            $query->where('alias', 'name');
                        })->where(function($query) use($search) {
                            $query->where('value', 'like', '%' . array_shift($search) . '%');
                            foreach ($search as $val) {
                                $query->orWhere('value', 'like', "%$val%");
                            }
                        });
                    })->orWhereHas('user_add_values', function($query) use($search) {
                        $query->whereHas('user_addon', function($query) use($search) {
                            $query->where('alias', 'surname');
                        })->where(function($query) use($search) {
                            $query->where('value', 'like', '%' . array_shift($search) . '%');
                            foreach ($search as $val) {
                                $query->orWhere('value', 'like', "%$val%");
                            }
                        });
                    })->orWhereHas('user_add_values', function($query) use($search) {
                        $query->whereHas('user_addon', function($query) use($search) {
                            $query->where('alias', 'middle_name');
                        })->where(function($query) use($search) {
                            $query->where('value', 'like', '%' . array_shift($search) . '%');
                            foreach ($search as $val) {
                                $query->orWhere('value', 'like', "%$val%");
                            }
                        });
                    });
                    if (isset($opts['search-login']) && $opts['search-login']) {
                        $query->orWhere('login', 'like', '%' . implode(' ', $search) . '%');
                    }
                    if (isset($opts['search-email']) && $opts['search-email']) {
                        $query->orWhere('email', 'like', '%' . implode(' ', $search) . '%');
                    }
                    if (isset($opts['search-phone']) && $opts['search-phone']) {
                        $query->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'phone');
                            })->where('value', 'like', '%' . phoneFormat(implode(' ', $search), true) . '%');
                        });
                    }
                });
            }

            if (isset($filter['excluded'])) {
                $query->whereNotIn('id', $filter['excluded']);
            }
        }

        return $query;
    }

    public function findFavoritesOnlineUsers()
    {
        if (request()->ajax()) {
            $data = getRequest();
            $filter = [
//                'favorites' => true,
                'online' => true,
                'search' => isset($data['search']) ? $data['search'] : '',
            ];
            if (isset($data['excluded'])) {
                $filter['excluded'] = $data['excluded'];
            }
            $users = $this->getFindUsersQuery($filter, [
                'search-login' => true,
                'status' => 's',
            ])->get();

            $result = [];
            foreach ($users as $user) {
                /* @var $user User */
                $result[$user->id] = [
                    'name'   => $user->getFullName(),
                    'avatar' => $user->getAvatar(),
                ];
            }

            return response()->json($result);
        }

        abort(404);
    }
}
