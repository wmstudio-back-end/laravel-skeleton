<?php

Route::group(['middleware' => ['web', 'lang'], 'namespace' => 'Modules\Users\Controllers'], function() {
    Route::get('/select-locate/{lang}', 'IndexController@selectLang');
    Route::post('/set-currency', 'IndexController@setCurrency');

    $social = implode('|', getAvailableSocial());

    if ($social) {
        Route::post('/social/{provider}', ['as' => 'social', 'uses' => 'SocialController@provider'])
            ->where(['provider' => $social]);
        Route::get('/social/{provider}/callback', ['as' => 'social.callback', 'uses' => 'SocialController@callback', 'no-index' => true])
            ->where(['provider' => $social]);
    }

    Route::post('/find-favorites-online-users', ['as' => 'find-f-o-users', 'uses' => 'IndexController@findFavoritesOnlineUsers',
        'status' => 't', 'need-active-status' => true]);
});
