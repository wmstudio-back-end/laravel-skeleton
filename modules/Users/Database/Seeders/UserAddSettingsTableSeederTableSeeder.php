<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\UserAddSettings;

class UserAddSettingsTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'control_type' => 'dialect',
                'name' => 'Диалект',
                'value' => [
                    1 => 'kuban',
                    2 => 'crimean',
                    3 => 'turkish',
                ],
            ],
            [
                'control_type' => 'lang_level',
                'name' => 'Уровень владения языком',
                'value' => [
                    1 => 'beginning',
                    2 => 'elementary',
                    3 => 'speaking',
                    4 => 'average',
                    5 => 'advanced',
                ]
            ],
            [
                'control_type' => 'interest_topics',
                'name' => 'Интересные темы',
                'value' => [
                    1 => 'science_and_technology',
                    2 => 'sport',
                    3 => 'language_and_culture',
                    4 => 'world_news',
                    5 => 'entertainment_and_lifestyle',
                    6 => 'business',
                    7 => 'literature',
                ]
            ],
            [
                'control_type' => 'tc_exp',
                'name' => 'Опыт преподавания',
                'value' => [
                    1 => 'less_one',
                    2 => 'one-five',
                    3 => 'five-ten',
                    4 => 'ten-fifteen',
                    5 => 'fifteen-twenty',
                    6 => 'more_twenty',
                ]
            ],
            [
                'control_type' => 'tc_style',
                'name' => 'Стиль преподавания',
                'value' => [
                    1 => 'cheerful_and_sociable',
                    2 => 'scientist_and_knowledgeable',
                    3 => 'demanding',
                ]
            ],
        ];

        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($settings as $setting) {
            $insert[] = [
                'control_type' => $setting['control_type'],
                'default_name' => $setting['name'],
                'value'        => json_encode($setting['value']),
                'created_at'   => $timeStamp,
                'updated_at'   => $timeStamp,
            ];
        }
        UserAddSettings::unguard();
        UserAddSettings::insert($insert);
        UserAddSettings::reguard();
    }
}
