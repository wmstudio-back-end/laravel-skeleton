<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Users\Entities\UserAddon;

class UserAddonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAddons = [
            [
                'id' => -1,
                'alias' => 'avatar',
                'description' => 'Фотография',
                'group' =>'account',
                'weight' => 0,
                'control_type' => 'image'
            ],
            [
                'alias' => 'surname',
                'description' => 'Фамилия',
                'group' =>'account',
                'weight' => 1,
                'control_type' => 'input'
            ],
            [
                'alias' => 'name',
                'description' => 'Имя',
                'group' =>'account',
                'weight' => 2,
                'control_type' => 'input'
            ],
            [
                'alias' => 'middle_name',
                'description' => 'Отчество',
                'group' =>'account',
                'weight' => 3,
                'control_type' => 'input'
            ],
            [
                'alias' => 'phone',
                'description' => 'Телефон',
                'group' =>'account',
                'weight' => 5,
                'control_type' => 'input'
            ],
            [
                'alias' => 'lang',
                'description' => 'Язык интерфейса',
                'group' =>'account',
                'weight' => 8,
                'control_type' => 'lang'
            ],
            [
                'alias' => 'invitees_list',
                'description' => 'Список приглашенных пользователей',
                'group' =>'account',
                'weight' => 9,
                'control_type' => 'invitees_list'
            ],
            [
                'alias' => 'status',
                'description' => 'Статус пользователя',
                'group' =>'account',
                'weight' => 10,
                'control_type' => 'status'
            ],
            [
                'alias' => 'country',
                'description' => 'Страна пользователя',
                'group' =>'settings',
                'weight' => 1,
                'control_type' => 'country'
            ],
            [
                'alias' => 'city',
                'description' => 'Город пользователя',
                'group' =>'settings',
                'weight' => 2,
                'control_type' => 'city'
            ],
            [
                'alias' => 'birthday',
                'description' => 'Дата рождения',
                'group' =>'settings',
                'weight' => 3,
                'control_type' => 'date'
            ],
            [
                'alias' => 'natural_lang',
                'description' => 'Родной язык пользователя',
                'group' =>'settings',
                'weight' => 4,
                'control_type' => 'lang'
            ],
            [
                'alias' => 'speak_langs',
                'description' => 'Языки, на которых пользователь разговаривает',
                'group' =>'settings',
                'weight' => 5,
                'control_type' => 'speak_langs'
            ],
            [
                'alias' => 'st_dialect',
                'description' => 'Диалект студента',
                'group' =>'student',
                'weight' => 1,
                'control_type' => 'dialect'
            ],
            [
                'alias' => 'st_lang_level',
                'description' => 'Уровень владения языком',
                'group' =>'student',
                'weight' => 2,
                'control_type' => 'lang_level'
            ],
            [
                'alias' => 'st_learn_targets',
                'description' => 'Цели изучения',
                'group' =>'student',
                'weight' => 3,
                'control_type' => 'textarea'
            ],
            [
                'alias' => 'st_about_me',
                'description' => 'О студенте',
                'group' =>'student',
                'weight' => 4,
                'control_type' => 'textarea'
            ],
            [
                'alias' => 'st_interest_topics',
                'description' => 'Интересные темы для студента',
                'group' =>'student',
                'weight' => 5,
                'control_type' => 'interest_topics'
            ],
            [
                'alias' => 'tc_profession',
                'description' => 'Профессия преподавателя',
                'group' =>'teacher',
                'weight' => 1,
                'control_type' => 'input'
            ],
            [
                'alias' => 'tc_exp',
                'description' => 'Опыт преподавания',
                'group' =>'teacher',
                'weight' => 2,
                'control_type' => 'tc_exp'
            ],
            [
                'alias' => 'tc_dialect',
                'description' => 'Диалект преподавателя',
                'group' =>'teacher',
                'weight' => 3,
                'control_type' => 'dialect'
            ],
            [
                'alias' => 'tc_education',
                'description' => 'Образование',
                'group' =>'teacher',
                'weight' => 4,
                'control_type' => 'education'
            ],
            [
                'alias' => 'tc_about_me',
                'description' => 'О преподавателе',
                'group' =>'teacher',
                'weight' => 5,
                'control_type' => 'textarea'
            ],
            [
                'alias' => 'tc_interest_topics',
                'description' => 'Интересные темы для преподавателя',
                'group' =>'teacher',
                'weight' => 6,
                'control_type' => 'interest_topics'
            ],
            [
                'alias' => 'tc_style',
                'description' => 'Стиль преподавания',
                'group' =>'teacher',
                'weight' => 7,
                'control_type' => 'tc_style'
            ],
            [
                'alias' => 'tc_prefer_level',
                'description' => 'Предпочитаемый уровень студента',
                'group' =>'teacher',
                'weight' => 8,
                'control_type' => 'prefer_level'
            ],
            [
                'alias' => 'tc_video_url',
                'description' => 'Ссылка на видео',
                'group' =>'teacher',
                'weight' => 9,
                'control_type' => 'video_url'
            ],
        ];

        UserAddon::unguard();
        UserAddon::create(array_shift($userAddons));
        UserAddon::insert($userAddons);
        UserAddon::reguard();
    }
}
