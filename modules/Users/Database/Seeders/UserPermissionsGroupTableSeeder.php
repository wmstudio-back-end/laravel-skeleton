<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Support\Facades\Route;
use Illuminate\Database\Seeder;
use Modules\Users\Entities\User;
use Modules\Users\Entities\UserPermission;
use Modules\Users\Entities\UserPermissionsGroup;

class UserPermissionsGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        $permissionsGroups = [
            [
                'id'         => -1,
                'name'       => 'Пользователи',
                'active'     => true,
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ],
            [
                'id'         => 1,
                'name'       => 'Администраторы',
                'active'     => true,
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ],
        ];
        UserPermissionsGroup::unguard();
        UserPermissionsGroup::insert($permissionsGroups);
        UserPermissionsGroup::reguard();
        
        User::create([
            'id' => 1,
            'login' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => \Hash::make('123456'),
            'permission_group' => 1,
            'active' => true,
        ]);

        $adminRoutes = [];
        $defPrefix = config('admin.defaults.prefix') ?: 'admin';
        foreach (\Module::all() as $module) {
            /* @var $module \Nwidart\Modules\Laravel\Module */
            $config = include($module->getExtraPath('Config/config.php'));
            if (isset($config['routes']['admin'])) {
                $prefix = isset($config['routes']['adminPrefix']) ? $config['routes']['adminPrefix'] : $module->getLowerName();
                if ($defPrefix != $prefix) {
                    $prefix = $defPrefix . '/' . $prefix;
                }
                $adminRoutes[$prefix] = true;
            }
        }

        $insertPermissions = [];
        foreach ($adminRoutes as $route => $true) {
            $insertPermissions[] = [
                'link_to'    => UserPermission::LINK_GROUP,
                'parent_id'  => 1,
                'route_name' => $route,
                'permission' => UserPermission::P_WRITE,
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
        }
        UserPermission::unguard();
        UserPermission::insert($insertPermissions);
        UserPermission::reguard();
    }
}
