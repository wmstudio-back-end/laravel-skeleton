<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddSettings;
use Barryvdh\TranslationManager\Models\Translation;

class AddHowKnowAboutUs extends Migration
{
    private $howKnow = [
        'control_type' => 'how_know_about_us',
        'default_name' => 'Как Вы о нас узнали?',
        'value' => [
            1 => 'friends',
            2 => 'search',
            3 => 'internet',
            4 => 'paper',
            5 => 'social',
        ],
    ];

    private $trans = [
        'friends' => [
            'en' => 'From friends, acquaintances',
            'ru' => 'От друзей, знакомых',
        ],
        'search' => [
            'en' => 'Search on the Internet',
            'ru' => 'Поиск в Интернете',
        ],
        'internet' => [
            'en' => 'Internet advertising',
            'ru' => 'Реклама в Интернете',
        ],
        'paper' => [
            'en' => 'Advertising in newspapers, magazines',
            'ru' => 'Реклама в газетах, журналах',
        ],
        'social' => [
            'en' => 'Advertising in social networks',
            'ru' => 'Реклама в социальных сетях',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserAddSettings::create($this->howKnow);

        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($this->howKnow['value'] as $key) {
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'en',
                'group'      => UserAddSettings::TRANS_GROUP,
                'key'        => $this->howKnow['control_type'] . '.' . $key,
                'value'      => $this->trans[$key]['en'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
            $insert[] = [
                'status'     => Translation::STATUS_CHANGED,
                'locale'     => 'ru',
                'group'      => UserAddSettings::TRANS_GROUP,
                'key'        => $this->howKnow['control_type'] . '.' . $key,
                'value'      => $this->trans[$key]['ru'],
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
        }
        Translation::unguard();
        Translation::insert($insert);
        Translation::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        UserAddSettings::where('control_type', $this->howKnow['control_type'])->first()->delete();

        $keys = [];
        foreach ($this->howKnow['value'] as $key) {
            $keys[] = $this->howKnow['control_type'] . '.' . $key;
        }
        $translations = Translation::where('group', UserAddSettings::TRANS_GROUP)
            ->whereIn('key', $keys)->get();
        Translation::unguard();
        foreach ($translations as $key) {
            $key->delete();
        }
        Translation::reguard();
    }
}
