<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_add_settings', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('control_type')->unique();
            $table->string('default_name');
            $table->text('value');
            $table->timestamps();
        });

        Artisan::call('db:seed', array('--class' => \Modules\Users\Database\Seeders\UserAddSettingsTableSeederTableSeeder::class));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_add_settings');
    }
}
