<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserAddValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_add_values', function(Blueprint $table)
		{
			$table->foreign('user_id', 'user_add_values_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('addon_id', 'user_add_values_ibfk_2')->references('id')->on('user_addons')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});

        Artisan::call('db:seed', array('--class' => \Modules\Users\Database\Seeders\UserPermissionsGroupTableSeeder::class));
        Artisan::call('db:seed', array('--class' => \Modules\Users\Database\Seeders\UserAddonsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_add_values', function(Blueprint $table)
		{
			$table->dropForeign('user_add_values_ibfk_1');
			$table->dropForeign('user_add_values_ibfk_2');
		});
	}

}
