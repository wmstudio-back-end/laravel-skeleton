<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Modules\Users\Entities\UserPermissionsGroup as Group;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('user_add_values', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('addon_id')->index('addon_id');
            $table->text('value');
            $table->unique(['user_id','addon_id']);
            $table->timestamps();
        });

        Schema::create('user_addons', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('alias');
            $table->text('description')->nullable();
            $table->string('group');
            $table->integer('weight');
            $table->string('control_type');
            $table->unique('alias');
        });

        Schema::create('user_permissions_group', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('name');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('user_permissions', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('link_to');
            $table->integer('parent_id');
            $table->string('route_name');
            $table->integer('permission')->default(1);
            $table->unique(['link_to','parent_id','route_name']);
            $table->timestamps();
        });

        Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
            $table->string('login')->unique();
			$table->text('email');
			$table->string('password');
			$table->integer('permission_group')->default(Group::DEFAULT);
			$table->boolean('active')->default(false);
			$table->rememberToken();
            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('user_add_values');
        Schema::drop('user_addons');
        Schema::drop('user_permissions');
        Schema::drop('user_permissions_group');
		Schema::drop('users');
	}

}
