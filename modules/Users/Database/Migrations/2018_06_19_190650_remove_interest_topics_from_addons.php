<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;
use Modules\Users\Entities\UserAddSettings;
use Barryvdh\TranslationManager\Models\Translation;

class RemoveInterestTopicsFromAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        $addons = UserAddon::where('control_type', 'interest_topics')->get()->keyBy('id');
        $values = UserAddValue::whereIn('addon_id', $addons->keys())->get();
        foreach ($values as $value) {
            $value->delete();
        }
        foreach ($addons as $addon) {
            $addon->delete();
        }
        $addSetting = UserAddSettings::where('control_type', 'interest_topics')->first();
        if ($addSetting) {
            $addSetting->delete();
        }
        $translations = Translation::where(function($query){
            $query->where('group', 'addons-settings')->where('key', 'like', '%interest_topics%');
        })->orWhere(function($query){
            $query->where('group', 'profile')->where('key', 'like', '%interest_topics%');
        })->get();
        foreach ($translations as $trans) {
            $trans->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
