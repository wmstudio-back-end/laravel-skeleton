<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;

class RenameAddon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $addon = UserAddon::where('alias', 'st_free_time')->first();
        if ($addon) {
            /* @var $addon UserAddon */
            $addon->description = 'Бесплатные секунды';
            $addon->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $addon = UserAddon::where('alias', 'st_free_time')->first();
        if ($addon) {
            /* @var $addon UserAddon */
            $addon->description = 'Бесплатные минуты';
            $addon->save();
        }
    }
}
