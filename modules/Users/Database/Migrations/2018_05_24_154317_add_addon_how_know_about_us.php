<?php

use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;
use Barryvdh\TranslationManager\Models\Translation;
use Illuminate\Database\Migrations\Migration;

class AddAddonHowKnowAboutUs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserAddon::insert([
            [
                'alias' => 'how_know_about_us',
                'description' => 'Как о нас узнали',
                'group' => 'account',
                'weight' => 0,
                'control_type' => 'hidden',
            ],
            [
                'alias' => 'referral_token',
                'description' => 'Токен для реферральной ссылки',
                'group' => 'account',
                'weight' => 11,
                'control_type' => 'referral_token',
            ],
        ]);
        Translation:: insert([
            [
                'status' => Translation::STATUS_CHANGED,
                'locale' => 'en',
                'group' => 'validation',
                'key' => 'invalid-referral',
                'value' => 'Referral link is not valid.',
            ],
            [
                'status' => Translation::STATUS_CHANGED,
                'locale' => 'ru',
                'group' => 'validation',
                'key' => 'invalid-referral',
                'value' => 'Реферальная ссылка недействительна.',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $addons = UserAddon::whereIn('alias', ['how_know_about_us', 'referral_token'])->get()->keyBy('id');
        $values = UserAddValue::whereIn('addon_id', array_keys($addons->toArray()))->get();
        foreach ($values as $value) {
            $value->delete();
        }
        foreach ($addons as $addon) {
            $addon->delete();
        }
        $translations = Translation::where([
            'group' => 'validation',
            'key' => 'invalid-referral',
        ])->get();
        foreach ($translations as $translate) {
            $translate->delete();
        }
    }
}
