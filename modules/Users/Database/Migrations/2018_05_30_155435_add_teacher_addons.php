<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class AddTeacherAddons extends Migration
{
    private $statusWeight;
    private $sql;

    public function __construct()
    {
        $status = UserAddon::where('alias', 'status')->first();
        if ($status) {
            $this->statusWeight = $status->weight;
        } else {
            $this->statusWeight = UserAddon::where('group', 'account')->max('weight');
        }
        $this->sql = 'UPDATE ' . env('DB_PREFIX') . 'user_addons as t '
            . 'SET `weight`=`weight` {sign} 2 WHERE t.group = "account" AND t.weight > ' . $this->statusWeight;
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement(str_replace('{sign}', '+', $this->sql));
        UserAddon::insert([
            [
                'alias' => 'tc_can_teach',
                'description' => 'Подтверждение прав преподавать.',
                'group' => 'account',
                'weight' => $this->statusWeight + 1,
                'control_type' => 'checkbox',
            ],
            [
                'alias' => 'tc_was_permitted',
                'description' => 'Запрос прав преподавателя обработан.',
                'group' => 'account',
                'weight' => $this->statusWeight + 2,
                'control_type' => 'checkbox',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $addons = UserAddon::where('group', 'account')
            ->whereIn('alias', ['tc_can_teach', 'tc_was_permitted'])->get()->keyBy('id');
        $values = UserAddValue::whereIn('addon_id', $addons->keys())->get();
        foreach ($values as $value) {
            $value->delete();
        }
        foreach ($addons as $addon) {
            $addon->delete();
        }
        \DB::statement(str_replace('{sign}', '-', $this->sql));
    }
}
