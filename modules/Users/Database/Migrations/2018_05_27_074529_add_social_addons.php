<?php

use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;
use Illuminate\Database\Migrations\Migration;

class AddSocialAddons extends Migration
{
    private $providers = [
        'vkontakte',
        'facebook',
        'odnoklassniki',
        'twitter',
        'google',
        'instagram',
    ];
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $max = UserAddon::where('group', 'account')->max('weight');
        $insert = [];
        foreach (getAvailableSocial() as $provider) {
            $insert[] = [
                'alias' => 'social_' . $provider,
                'description' => ucfirst($provider) . ' ID',
                'group' => 'account',
                'weight' => ++$max,
                'control_type' => 'input',
            ];
        }
        UserAddon::unguard();
        UserAddon::insert($insert);
        UserAddon::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $providers = $this->providers;
        foreach ($providers as $key => $val) {
            $providers[$key] = 'social_' . $val;
        }
        $addons = UserAddon::whereIn('alias', $providers)->get()->keyBy('id');
        $values = UserAddValue::whereIn('addon_id', array_keys($addons->toArray()))->get();
        foreach ($values as $value) {
            $value->delete();
        }
        foreach ($addons as $addon) {
            $addon->delete();
        }
    }
}
