<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddSettings;
use Modules\Users\Entities\UserAddValue;
use Modules\Users\Entities\UserAddon;

class AddUserAddSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $studentMax = UserAddon::where('group', 'student')->max('weight');
        $teacherMax = UserAddon::where('group', 'teacher')->max('weight');
        $insert = [
            [
                'alias' => 'st_lesson_type',
                'description' => 'Предпочитаемые виды уроков для студента',
                'group' => 'student',
                'weight' => ++$studentMax,
                'control_type' => 'lesson_type',
            ],
            [
                'alias' => 'tc_lesson_type',
                'description' => 'Предпочитаемые виды уроков для преподавателя',
                'group' => 'teacher',
                'weight' => ++$teacherMax,
                'control_type' => 'lesson_type',
            ],
            [
                'alias' => 'tc_diploma_url',
                'description' => 'Ссылка на файл диплома',
                'group' => 'teacher',
                'weight' => ++$teacherMax,
                'control_type' => 'file_url',
            ],
        ];
        UserAddon::unguard();
        UserAddon::insert($insert);
        foreach(UserAddon::where('control_type', 'video_url')->get() as $addon) {
            /* @var $addon UserAddon */
            $addon->control_type = 'file_url';
            $addon->save();
        }
        UserAddon::reguard();
        UserAddSettings::create([
            'control_type' => 'lesson_type',
            'default_name' => 'Виды уроков',
            'value' => [
                1 => 'speech',
                2 => 'grammar',
                3 => 'for-children',
                4 => 'business'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $addons = UserAddon::whereIn('alias', ['st_lesson_type', 'tc_lesson_type', 'tc_diploma_url'])->get()->keyBy('id');
        $values = UserAddValue::whereIn('addon_id', $addons->keys())->get();
        $setting = UserAddSettings::where('control_type', 'lesson_type')->first();
        UserAddValue::unguard();
        foreach ($values as $value) {
            /* @var $value UserAddValue */
            $value->delete();
        }
        UserAddValue::reguard();
        $setting->delete();
        foreach ($addons as $addon) {
            /* @var $addon UserAddon */
            $addon->delete();
        }
    }
}
