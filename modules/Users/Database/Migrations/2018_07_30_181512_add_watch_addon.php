<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;

class AddWatchAddon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserAddon::create([
            'alias' => 'watch_list',
            'description' => 'Список пользователей, за чьими статуасми пользователдь следит',
            'group' => 'account',
            'weight' => UserAddon::where('group', 'account')->max('weight') + 1,
            'control_type' => 'invitees_list',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $addon = UserAddon::where('alias', 'watch_list')->first();
        if ($addon) {
            $addon->delete();
        }
    }
}
