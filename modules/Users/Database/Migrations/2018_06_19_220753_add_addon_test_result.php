<?php

use Modules\Users\Entities\UserAddon;
use Illuminate\Database\Migrations\Migration;

class AddAddonTestResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        UserAddon::create([
            'alias' => 'st_tests_results',
            'description' => 'Результаты тестирований',
            'group' => 'student',
            'weight' => (UserAddon::where('group', 'student')->max('weight') + 1),
            'control_type' => 'hidden',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $addon = UserAddon::where('alias', 'st_tests_results')->first();
        if ($addon) {
            $values = \Modules\Users\Entities\UserAddValue::where('addon_id', $addon->id)->get();
            foreach ($values as $value) {
                $value->delete();
            }
            $addon->delete();
        }
    }
}
