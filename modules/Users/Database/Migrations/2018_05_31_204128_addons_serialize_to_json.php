<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddValue;

class AddonsSerializeToJson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (UserAddValue::all() as $value) {
            /* @var $value UserAddValue */
            try {
                $value->value = json_encode(unserialize($value->value));
                $value->save();
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
