<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;

class AddUserAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $weight = UserAddon::where('group', 'account')->max('weight');
        UserAddon::insert([
            [
                'alias' => 'rating',
                'description' => 'Рейтинг пользователя',
                'group' => 'account',
                'weight' => ++$weight,
                'control_type' => 'hidden',
            ],
            [
                'alias' => 'favorites',
                'description' => 'Избранные пользователи',
                'group' => 'account',
                'weight' => ++$weight,
                'control_type' => 'hidden',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $addons = UserAddon::whereIn('alias', ['rating', 'favorites'])->get();
        foreach ($addons as $addon) {
            $addon->delete();
        }
    }
}
