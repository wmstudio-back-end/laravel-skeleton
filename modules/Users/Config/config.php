<?php

$name = 'Пользователи';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            // Users
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.users',
                    'label' => 'Просмотр списка пользователей',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'get',
                'uri' => '/{id}',
                'parameters' => [
                    'as' => 'admin.users.show',
                    'label' => 'Просмотр информации о пользователях',
                    'uses' => 'AdminController@show',
                ],
                'where' => ['id' => '\d+'],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/edit/{id}',
                'parameters' => [
                    'as' => 'admin.users.edit',
                    'label' => 'Редактирование пользователей',
                    'uses' => 'AdminController@edit',
                ],
                'where' => ['id' => '\d+'],
            ],
            [
                'method' => 'post',
                'uri' => '/delete/{id}',
                'parameters' => [
                    'as' => 'admin.users.delete',
                    'label' => 'Удаление пользователей',
                    'uses' => 'AdminController@delete',
                ],
                'where' => ['id' => '\d+'],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/request-teacher-permissions',
                'parameters' => [
                    'as' => 'admin.users.request-t-p',
                    'label' => 'Обработка запроса прав на преподавание',
                    'uses' => 'AdminController@requestTeacherPermissions',
                ],
            ],

            // Addons
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/settings',
                'parameters' => [
                    'as' => 'admin.users.settings',
                    'label' => 'Редактирование пользовательских дополнений',
                    'uses' => 'AdminController@settings',
                ],
            ],

            // Groups
            [
                'method' => 'get',
                'uri' => '/groups',
                'parameters' => [
                    'as' => 'admin.users.groups',
                    'label' => 'Просмотр списка групп прав доступа',
                    'selfEdit' => false,
                    'uses' => 'AdminController@groups',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/group/{id}',
                'parameters' => [
                    'as' => 'admin.users.group.edit',
                    'label' => 'Создание и редактирование групп прав доступа',
                    'selfEdit' => false,
                    'uses' => 'AdminController@groupsEdit',
                ],
                'where' => ['id' => 'new|\-?\d+'],
            ],
            [
                'method' => 'post',
                'uri' => '/group/delete/{id}',
                'parameters' => [
                    'as' => 'admin.users.group.delete',
                    'label' => 'Удаление групп прав доступа',
                    'selfEdit' => false,
                    'uses' => 'AdminController@groupsDelete',
                ],
                'where' => ['id' => '\d+'],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/group/list/{id}',
                'parameters' => [
                    'as' => 'admin.users.list-group',
                    'label' => 'Редактирование списка пользователей у группы',
                    'uses' => 'AdminController@listGroup',
                ],
                'where' => ['id' => '\-?\d+'],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => $name,
                'uri' => '#',
                'icon'  => 'fa fa-user',
                'pages' => [
                    [
                        'label' => 'Список пользователей',
                        'route' => 'admin.users',
                        'icon'  => 'fa fa-list-ul',
                    ],
                    [
                        'label' => 'Настройки доп. полей',
                        'route' => 'admin.users.settings',
                        'icon'  => 'fa fa-puzzle-piece',
                    ],
                    [
                        'label' => 'Группы',
                        'route' => 'admin.users.groups',
                        'icon'  => 'fa fa-group',
                    ],
                ],
                'order' => 100,
            ],
        ],
    ],

    'social' => [
        'vkontakte' => \SocialiteProviders\VKontakte\VKontakteExtendSocialite::class,
        'odnoklassniki' => JhaoDa\SocialiteProviders\Odnoklassniki\OdnoklassnikiExtendSocialite::class,
        'twitter' => \SocialiteProviders\Twitter\TwitterExtendSocialite::class,
        'google' => \SocialiteProviders\Google\GoogleExtendSocialite::class,
        'instagram' => \SocialiteProviders\Instagram\InstagramExtendSocialite::class,
    ]
];
