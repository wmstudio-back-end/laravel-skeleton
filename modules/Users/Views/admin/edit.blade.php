<?php $permissions = getPermissions(); ?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <?php $show = isset($show) ? $show : false; ?>
                @if(!$show)
                    <form method="post" action="{{ route('admin.users.edit', ['id' => $user->id]) }}">
                        @csrf
                @endif
                <div class="card-body">
                    <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                        <?php /* @var $user \Modules\Users\Entities\User */
                        $addonsByGroups = $user->getAddonsByGroups(true);
                        $groups = array_keys($addonsByGroups);
                        $tmp = [
                            'login' => [
                                'value' => $user->login,
                                'description' => 'Логин',
                                'control-type' => 'input',
                            ],
                            'email' => [
                                'value' => $user->email,
                                'description' => 'Email',
                                'control-type' => 'input',
                            ],
                            'email_confirmed' => [
                                'value' => $user->email_confirmed,
                                'description' => 'E-Mail подтвержден',
                                'control-type' => 'checkbox',
                            ],
                            'active' => [
                                'value' => $user->active,
                                'description' => 'Активность',
                                'control-type' => 'checkbox',
                            ],
                            'date-creation' => [
                                'value' => $user->created_at,
                                'description' => 'Дата регистрации',
                                'control-type' => 'date_register',
                            ],
                        ];

                        if (!$show) {
                            $tmp['new-password'] = [
                                'value' => null,
                                'description' => 'Новый пароль',
                                'control-type' => 'password',
                            ];
                            $tmp['new-password-confirm'] = [
                                'value' => null,
                                'description' => 'Подтверждение пароля',
                                'control-type' => 'password',
                            ];
                        }

                        $addonsByGroups['account'] = array_merge($tmp, $addonsByGroups['account']);
                        ?>
                        @foreach($groups as $group)
                            <li class="nav-item">
                                <a class="nav-link{{ reset($groups) == $group ? ' active' : '' }}"
                                   id="group-{{ $group }}-tab" data-toggle="tab" href="#group-{{ $group }}"
                                   role="tab" aria-controls="group-{{ $group }}"
                                   aria-selected="true">{{ \Modules\Users\Constants\Cambly::$addGroups[$group] }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                        @foreach($addonsByGroups as $group => $addons)
                            <div class="tab-pane fade{{ reset($groups) == $group ? ' show active' : '' }}"
                                 id="group-{{ $group }}" role="tabpanel" aria-labelledby="contact-tab">
                                @foreach($addons as $name => $addon)
                                    @include('users::admin.partials.' . $addon['control-type'], [
                                        'name' => $name,
                                        'description' => $addon['description'],
                                        'value' => $addon['value'],
                                        'noValue' => '---',
                                        'permissions' => $permissions,
                                        'show' => $show,
                                    ])
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer text-right">
                    @if((getPermissions('admin.users.edit')))
                        @if($show)
                            <form method="post" action="{{ route('admin.users.delete', ['id' => $user->id]) }}">
                                @csrf
                                <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn btn-action btn-primary">Редактировать</a>
                                <button type="submit" class="btn btn-action btn-danger">Удалить</button>
                            </form>
                        @else
                            <button type="submit" class="btn btn-action btn-success">Сохранить</button>
                            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}" class="btn btn-action btn-warning">Отмена</a>
                        @endif
                    @endif
                </div>
                @if(!$show)
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@if (getPermissions('admin.users.edit'))
    @section('sub-action')
        @if($show)
            <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn btn-action btn-primary">Редактировать</a>
        @else
            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}" class="btn btn-action btn-warning">Отмена</a>
        @endif
    @endsection
@endif