<?php
$canShow      = getPermissions('admin.users.show');
$canEdit      = getPermissions('admin.users.edit');
$canDelete    = getPermissions('admin.users.delete');
$canPermitted = getPermissions('admin.users.request-t-p');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список пользователей</h4>
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive project-list">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Имя</th>
                                <th>E-Mail</th>
                                <th>Дата создания</th>
                                <th>Статус</th>
                                <th>Тип</th>
                                @if($canEdit || $canDelete)
                                    <th><div class="pull-right">Действия</div></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                            <?php /* @var $user \Modules\Users\Entities\User */ ?>
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>
                                        @if($canShow)
                                            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->getFullName() }}</a>
                                        @else
                                            {{ $user->getFullName() }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($canShow)
                                            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->email }}</a>
                                        @else
                                            {{ $user->email }}
                                        @endif
                                    </td>
                                    <td>{{ date_format($user->created_at, 'd.m.Y') }}</td>
                                    <td>
                                        <div class="badge badge-{{ ($user->active) ? 'success' : 'danger' }}">{{ ($user->active) ? 'Активный' : 'Не активный' }}</div>
                                    </td>
                                    <td>{{ \Modules\Users\Constants\Cambly::getStatus($user->getAddons()['status']['value']) }}</td>
                                    @if(($canEdit || $canDelete))
                                        <td class="project-actions">
                                            @if($canDelete)
                                                <form method="post" action="{{ route('admin.users.delete', ['id' => $user->id]) }}">
                                                    @csrf
                                            @endif
                                                @if($canDelete)
                                                    <button type="submit" class="btn btn-action btn-danger pull-right">Удалить</button>
                                                @endif
                                                @if($canEdit)
                                                    <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="btn btn-action btn-primary pull-right">Редактировать</a>
                                                @endif
                                            @if($canDelete)
                                                </form>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@if ($canPermitted)
    @section('sub-action')
        <a href="{{ route('admin.users.request-t-p') }}" class="btn btn-action btn-primary">
            Список пользователей, отправивших запрос на права преподавателя
        </a>
    @endsection
@endif