<?php
$canShow = getPermissions('admin.users.show');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список пользователей, отправивших запрос на права преподавателя</h4>
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive project-list">
                        <table class="table table-striped">
                            @if($users->count() == 0)
                                <tr>
                                    <td style="text-align: center;">
                                        Пользователи не найдены.
                                    </td>
                                </tr>
                            @else
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Имя</th>
                                    <th>E-Mail</th>
                                    <th>Дата создания</th>
                                    <th>Статус</th>
                                    <th>Заполнено полей профиля</th>
                                    <th><div class="pull-right">Действия</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $key => $user)
                                <?php /* @var $user \Modules\Users\Entities\User */ ?>
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>
                                            @if($canShow)
                                                <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->getFullName() }}</a>
                                            @else
                                                {{ $user->getFullName() }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($canShow)
                                                <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->email }}</a>
                                            @else
                                                {{ $user->email }}
                                            @endif
                                        </td>
                                        <td>{{ date_format($user->created_at, 'd.m.Y') }}</td>
                                        <td>
                                            <div class="badge badge-{{ ($user->active) ? 'success' : 'danger' }}">{{ ($user->active) ? 'Активный' : 'Не активный' }}</div>
                                        </td>
                                        <td>{{ $fillFields[$user->id] }} / {{ $fieldsCount }}</td>
                                        <td class="project-actions">
                                            <form method="post" action="{{ route('admin.users.request-t-p') }}">
                                                @csrf
                                                <input type="hidden" name="user-id" value="{{ $user->id }}">
                                                <button type="submit" name="result" value="true" class="btn btn-action btn-success pull-right">Подтвердить</button>
                                                <button type="submit" class="btn btn-action btn-danger pull-right">Отказать</button>
                                                @if($canShow)
                                                    <a href="{{ route('admin.users.show', ['id' => $user->id]) }}" class="btn btn-action btn-primary pull-right">
                                                        Посмотреть профиль
                                                    </a>
                                                @endif
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection