<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <input
                id="{{ $name }}"
                type="text"
                class="form-control"
                readonly="readonly"
                value="{{ $value ? date_format(new DateTime($value), 'd.m.Y') : $noValue }}"
        >
    </div>
</div>