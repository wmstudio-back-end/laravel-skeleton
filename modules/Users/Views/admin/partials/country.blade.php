@if($show || !$permissions)
    <?php $value = \Modules\Translations\Constants\Languages::getCountry($value) ?: $noValue ?>
    @include('users::admin.partials.input')
@else
    <?php $value = $value !== null ? (int)$value : null; ?>
    @include('users::admin.partials.select', ['list' => \Modules\Translations\Constants\Languages::getCountries()])
@endif