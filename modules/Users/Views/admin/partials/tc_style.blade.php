<?php $tcStyle = \Modules\Users\Entities\UserAddSettings::where('control_type', 'tc_style')->first()->transValue(); ?>
@if($show || !$permissions)
    <?php $value = (isset($tcStyle[$value]) ? $tcStyle[$value] : $noValue) ?>
    @include('users::admin.partials.input')
@else
    <?php $value = $value !== null ? (int)$value : null; ?>
    @include('users::admin.partials.select', ['list' => $tcStyle])
@endif