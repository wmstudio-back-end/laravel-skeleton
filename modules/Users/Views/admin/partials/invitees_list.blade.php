<?php
$userIds = $value ? json_decode($value, true) : [];
$users = \Modules\Users\Entities\User::whereIn('id', array_keys($userIds))->get()->keyBy('id');
/* @var $user \Modules\Users\Entities\User */
?>
<div class="form-group row">
    <label class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        @if(count($userIds))
            @foreach($userIds as $id => $value)
                <div class="add-invitees">
                    <div class="alert alert-info">
                        @if(isset($users[$id]))
                            <strong><a href="{{ route('admin.users.show', ['id' => $id]) }}">{{ $users[$id]->getFullName() }}</a></strong>
                        @else
                            <strong>Пользователь с id{{ $id }} был удален.</strong>
                        @endif
                    </div>
                </div>
            @endforeach
        @else
            <div class="add-invitees">
                <div class="alert alert-info">
                    <strong>Пользователь еще никого не пригласил</strong>
                </div>
            </div>
        @endif
    </div>
</div>
