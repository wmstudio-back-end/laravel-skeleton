<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <input
            id="{{ $name }}"
            name="{{ $name }}"
            type="text"
            class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
            @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
            value="{{ old($name) ?: $value ?: ($show ? $noValue : '') }}"
            @if(isset($placeholder))placeholder="{{ $placeholder }}"@endif
            @if(isset($tooltip))data-toggle="tooltip" title="{{ $tooltip }}"@endif
            @if($value != null)
                data-toggle="popover"
                data-trigger="focus"
                data-title="Ссылка на файл"
                data-placement="left"
                data-html="true"
                data-content="{{ '<a href="' . url($value) . '" target="_blank">' . $value . '</a>' }}"
            @endif
        >
        @include('users::admin.partials.error')
    </div>
</div>