@if($show || !$permissions)
    <?php $value = Modules\Users\Constants\Cambly::getStatus($value) ?: $noValue ?>
    @include('users::admin.partials.input')
@else
    @php
    $list = Modules\Users\Constants\Cambly::$defStatusNames;
    foreach ($list as $status => $item) {
        $list[$status] = Modules\Users\Constants\Cambly::getStatus($status);
    }
    @endphp
    @include('users::admin.partials.select', ['list' => $list])
@endif