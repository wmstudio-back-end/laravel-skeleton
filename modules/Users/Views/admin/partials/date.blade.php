<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <input
                id="{{ $name }}"
                name="{{ $name }}"
                type="text"
                @if($show || !$permissions || (isset($readonly) && $readonly))
                    class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
                    readonly="readonly"
                @else()
                    class="form-control datepicker{{ $errors->has($name) ? ' is-invalid' : '' }}"
                @endif
                value="{{ old($name) ? date_format(new DateTime(old($name)), 'd.m.Y')
                    : $value ? date_format(new DateTime($value), 'd.m.Y') : ($show ? $noValue : '') }}"
        >
        @include('users::admin.partials.error')
    </div>
</div>