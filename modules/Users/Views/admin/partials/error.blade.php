@if ($errors->has($name))
    <div class="invalid-feedback">{{ str_replace($name .' ', '', $errors->first($name)) }}</div>
@endif