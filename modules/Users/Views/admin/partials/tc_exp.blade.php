<?php $tcExp = \Modules\Users\Entities\UserAddSettings::where('control_type', 'tc_exp')->first()->transValue() ?>
@if($show || !$permissions)
    <?php $value = (isset($tcExp[$value]) ? $tcExp[$value] : $noValue) ?>
    @include('users::admin.partials.input')
@else
    <?php $value = $value !== null ? (int)$value : null; ?>
    @include('users::admin.partials.select', ['list' => $tcExp])
@endif