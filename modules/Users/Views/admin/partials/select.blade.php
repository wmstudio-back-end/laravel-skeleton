<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <select
                id="{{ $name }}"
                name="{{ $name }}"
                class="form-control selectpicker"
                @if($errors->has($name))data-class="is-invalid"@endif
                @if(count($list) < 10)data-live-search="false"@endif
                @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
        >
            <option value=""{{ (old($name) === null && $value === null) ? ' selected="selected"' : '' }}>{{ $noValue }}</option>
            @foreach ($list as $key => $val)
                <option
                        value="{{ $key }}"
                        {{ ((old($name) === $key) ? ' selected="selected"' : ($value === $key ? ' selected="selected"' : '')) }}
                >{{ $val }}</option>
            @endforeach
        </select>
        @include('users::admin.partials.error')
    </div>
</div>