<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <input
                id="{{ $name }}"
                name="{{ $name }}"
                type="password"
                class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
                @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
                value="{{ old($name) ?: $value }}"
        >
        @include('users::admin.partials.error')
    </div>
</div>