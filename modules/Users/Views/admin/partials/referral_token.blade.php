<?php
    $includeMsg = 'Измените текущий, для автоматической генерации нового.';
    $vars['placeholder'] = $includeMsg;
    if (!$show && $permissions) {
        $vars['tooltip'] = $includeMsg;
    }
?>
@include('users::admin.partials.input', $vars)