<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <textarea
                id="{{ $name }}"
                name="{{ $name }}"
                class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
                @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
        >{{ old($name) ?: $value ?: ($show ? $noValue : '') }}</textarea>
        @include('users::admin.partials.error')
    </div>
</div>