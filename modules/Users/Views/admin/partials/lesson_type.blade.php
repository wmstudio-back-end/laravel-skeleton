<?php
$value = $value ? json_decode($value, true) : $value;
$list = \Modules\Users\Entities\UserAddSettings::where('control_type', 'lesson_type')->first()->transValue();
if (!$value) {
    $value = $list;
    foreach ($value as $key => $val) {
        $value[$key] = false;
    }
}
?>
@include('users::admin.partials.checkboxes')