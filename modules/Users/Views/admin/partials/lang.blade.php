<?php $languages = getLocales() ?>
@if($show || !$permissions)
    <?php $value = isset($languages[$value]) ? $languages[$value] : $noValue ?>
    @include('users::admin.partials.input')
@else
    @include('users::admin.partials.select', ['list' => $languages])
@endif