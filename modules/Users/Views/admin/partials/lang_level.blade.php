<?php $langLevel = \Modules\Users\Entities\UserAddSettings::where('control_type', 'lang_level')->first()->transValue() ?>
@if($show || !$permissions)
    <?php $value = isset($langLevel[$value]) ? $langLevel[$value] : $noValue ?>
    @include('users::admin.partials.input')
@else
    <?php $value = $value !== null ? (int)$value : null; ?>
    @include('users::admin.partials.select', ['list' => $langLevel])
@endif