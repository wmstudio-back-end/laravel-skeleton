<?php
$value = $value ? json_decode($value, true) : $value;
$list = getLocales();
if (!$value) {
    $value = $list;
    foreach ($value as $key => $lang) {
        $value[$key] = false;
    }
}
?>
@include('users::admin.partials.checkboxes')
