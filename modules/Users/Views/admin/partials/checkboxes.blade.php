<div class="form-group row">
    <label for="{{ $name }}" class="col-md-2 col-form-label">{{ $description }}</label>
    <div class="col-md-10">
        <input type="hidden" name="checkbox-{{ $name }}" value="{{ $name }}[]">
        @foreach($list as $key => $lang)
            <div class="custom-control custom-checkbox middle">
                <input
                        type="checkbox"
                        class="custom-control-input"
                        @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
                        id="{{ $name }}-{{ $key }}"
                        name="{{ $name }}[]"
                        value="{{ $key }}"
                        {{ old($name, null) !== null && is_array(old($name))
                            ? (in_array($key, old($name)) ? 'checked="checked"' : '')
                            : (is_array($value) && isset($value[$key]) && $value[$key])
                                 ? 'checked="checked"' : ''
                        }}
                >
                <label
                        @if(!$show && $permissions && !(isset($readonly) && $readonly))for="{{ $name }}-{{ $key }}"@endif
                        class="custom-control-label"
                >{{ $lang }}</label>
            </div>
        @endforeach
        @include('users::admin.partials.error')
    </div>
</div>