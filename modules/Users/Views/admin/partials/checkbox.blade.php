<div class="form-group row">
    <label
        @if(!$show && $permissions && !(isset($readonly) && $readonly))for="{{ $name }}"@endif
        class="col-md-2 col-form-label"
    >{{ $description }}</label>
    <div class="col-md-10">
        <div class="custom-control custom-checkbox middle">
            <input type="hidden" name="checkbox-{{ $name }}" value="{{ $name }}">
            <input
                    type="checkbox"
                    class="custom-control-input"
                    @if($show || !$permissions || (isset($readonly) && $readonly))readonly="readonly"@endif
                    id="{{ $name }}"
                    name="{{ $name }}"
                    value="1"
                    {{ old($name, null) !== null
                        ? (old($name) ? 'checked="checked"' : '')
                        : $value ? 'checked="checked"' : ''
                    }}
            >
            <label
                    @if(!$show && $permissions && !(isset($readonly) && $readonly))for="{{ $name }}"@endif
                    class="custom-control-label"
            >{{ $description }}</label>
        </div>
        @include('users::admin.partials.error')
    </div>
</div>