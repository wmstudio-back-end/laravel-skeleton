<?php
$canDelete = getPermissions('admin.users.group.delete');
$canEditListUsers = getPermissions('admin.users.list-group');
?>

@extends(config('admin.defaults.layout'), ['old' => old()])

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>{{ $groupObj->name }}</h4>
                    @if($canDelete)
                        <form method="post" action="{{ route('admin.users.group.delete', ['id' => $groupObj->id]) }}">
                            @csrf
                            <button type="submit" class="btn btn-action btn-danger pull-right">
                                <span class="fa fa-times"></span>
                                Удалить
                            </button>
                        </form>
                    @endif
                </div>
                <form method="post" action="{{ route('admin.users.group.edit', ['id' => $groupObj->id]) }}">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input indeterminate" data-checked="-1">
                                            <label class="custom-control-label">Запретить доступ</label>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input indeterminate" data-checked="0">
                                            <label class="custom-control-label">Как у родителя (как у модуля)</label>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input indeterminate" data-checked="1">
                                            <label class="custom-control-label">Разрешить доступ</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3">
                                <ul class="nav nav-pills flex-column" role="tablist">
                                    @foreach($routes as $group => $params)
                                        <li class="nav-item">
                                            <a
                                                class="nav-link{{ reset($routes) == $params ? ' active show' : '' }}"
                                                id="tab-{{ str_replace('/', '-', $group) }}"
                                                data-toggle="tab"
                                                href="#{{ str_replace('/', '-', $group) }}"
                                                role="tab"
                                            >{{ $params['name'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-12 col-sm-12 col-md-9">
                                <div class="tab-content">
                                    @foreach($routes as $group => $params)
                                        <div
                                            class="tab-pane fade{{ reset($routes) == $params ? ' active show' : '' }}"
                                            id="{{ str_replace('/', '-', $group) }}"
                                            role="tabpanel"
                                            aria-labelledby="tab-{{ str_replace('/', '-', $group) }}"
                                        >
                                            <div class="form-group row">
                                                <label
                                                        for="module-{{ str_replace('/', '-', $group) }}"
                                                        class="col-md-3 col-form-label"
                                                >Глобальные разрешения</label>
                                                <div class="col-md-3">
                                                    <select
                                                            id="module-{{ str_replace('/', '-', $group) }}"
                                                            name="{{ $group }}"
                                                            class="form-control selectpicker"
                                                            data-live-search="false"
                                                    >
                                                        @foreach(\Modules\Users\Entities\UserPermission::$permissionNames as $key => $permissionName)
                                                            <option
                                                                value="{{ $key }}"
                                                                    {{ ((old(str_replace('/', '-', $group)) == $key)
                                                                        ? ' selected="selected"' :
                                                                        (isset($permissions[$group])
                                                                        && $permissions[$group]->permission == $key
                                                                        ? ' selected="selected"' : '')) }}
                                                            >{{ $permissionName }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has(str_replace('/', '-', $group)))
                                                        <div class="invalid-feedback">
                                                            {{ str_replace(
                                                                    str_replace('/', '-', $group) . ' ',
                                                                    '',
                                                                    $errors->first(str_replace('/', '-', $group))
                                                            ) }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            @foreach($params['routes'] as $route => $label)
                                                <div class="form-group row">
                                                    <div class="col-12">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="hidden" name="checkbox-{{ str_replace('.', ',', $route) }}" value="{{ $route }}">
                                                            <?php
                                                                $dataChecked = 0;
                                                                if (isset($permissions[$route])) {
                                                                    switch ($permissions[$route]->permission) {
                                                                        case \Modules\Users\Entities\UserPermission::P_DENY:
                                                                            $dataChecked = -1;
                                                                            break;
                                                                        case \Modules\Users\Entities\UserPermission::P_WRITE:
                                                                            $dataChecked = 1;
                                                                            break;
                                                                        default:
                                                                            $dataChecked = 0;
                                                                            break;
                                                                    }
                                                                }
                                                            ?>
                                                            <input
                                                                type="checkbox"
                                                                class="custom-control-input indeterminate"
                                                                id="route-{{ str_replace('.', '-', $route) }}"
                                                                name="{{ str_replace('.', ',', $route) }}"
                                                                data-checked="{{ $dataChecked }}"
                                                                value="1"
                                                            >
                                                            <label class="custom-control-label" for="route-{{ str_replace('.', '-', $route) }}">{{ $label }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-action btn-primary">
                            <span class="fa fa-check"></span>
                            Сохранить
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@if($canEditListUsers)
    @section('sub-action')
        <a href="{{ route('admin.users.list-group', ['id' => $groupObj->id]) }}" class="btn btn-sm btn-success pull-right">Список пользователей</a>
    @endsection
@endif