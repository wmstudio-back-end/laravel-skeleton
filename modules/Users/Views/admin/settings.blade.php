@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Настройки пользовательских дополнений</h4>
                </div>
                <div class="card-body">
                    <div class="col-12">
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <label>
                                    Здесь поля заполняются в виде slug<i>(латинские символы, символ тире "-", символ земля "_")</i>.<br>
                                    Переводы для этих полей находятся (или нужно создать) в локализации, в группе addons-settings.<br>
                                    Например диалект кубанский:<br>
                                    <b>Локализация -> {{ \Modules\Users\Entities\UserAddSettings::TRANS_GROUP }} -> ключ "dialect.kuban"</b>
                                </label>
                            </div>
                        </div>
                        <form method="post" action="{{ route('admin.users.settings') }}" class="user-add-setting">
                            @csrf
                            @foreach($settings as $setting)
                                <?php /* @var $setting \Modules\Users\Entities\UserAddSettings */ ?>
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">
                                        <b>{{ $setting->default_name }}</b><br>
                                        Ключ: "<i>{{ $setting->control_type }}</i>"
                                    </label>
                                    <div class="col-md-10" id="{{ $setting->control_type }}">
                                        {{--<input type="hidden" name="checkbox-{{ $name }}" value="{{ $name }}[]">--}}
                                        @foreach($setting->value as $key => $val)
                                            <div class="custom-control custom-checkbox middle">
                                                <input
                                                    type="checkbox"
                                                    class="custom-control-input"
                                                    data-checked="false"
                                                    id="{{ $setting->control_type }}-{{ $key }}"
                                                    name="{{ $setting->control_type }}[{{ $key }}]"
                                                    value="{{ $key }}"
                                                >
                                                <label
                                                    for="{{ $setting->control_type }}-{{ $key }}"
                                                    class="custom-control-label"
                                                >{{ $val }}</label>
                                            </div>
                                        @endforeach
                                        @if ($errors->has($setting->control_type))
                                            <div class="invalid-feedback">{{ str_replace($setting->control_type .' ', '', $errors->first($setting->control_type)) }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="input-group small col-md-10 offset-md-2">
                                        <input
                                            data-for="{{ $setting->control_type }}"
                                            class="form-control"
                                            placeholder="Добавить {{mb_strtolower($setting->default_name, 'utf-8')}}"
                                        >
                                        <div class="input-group-append">
                                            <button class="btn btn-primary user-add-setting" type="button" data-for="{{ $setting->control_type }}">Добавить</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group row">
                                <div class="col-md-4 offset-md-2">
                                    <button type="submit" class="btn btn-action btn-primary">
                                        <span class="fa fa-check"></span>
                                        Сохранить
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-footer text-right">

                </div>
            </div>
        </div>
    </div>
@endsection

@if($needPublish)
    @section('sub-action')
        <form method="post" action="{{ route('admin.users.settings') }}">
            @csrf
            <input type="hidden" name="publish" value="1">
            <button type="submit" class="btn btn-sm btn-success pull-right">Опубликовать ключи по умолчанию</button>
        </form>
    @endsection
@endif