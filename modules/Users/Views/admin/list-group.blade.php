@extends(config('admin.defaults.layout'), ['old' => old()])

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>{{ $group->name }}</h4>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('admin.users.list-group', ['id' => $group->id]) }}">
                        @csrf
                        <input type="hidden" name="action" value="addUser">

                    </form>
                    <form method="post" action="{{ route('admin.users.list-group', ['id' => $group->id]) }}">
                        @csrf
                        <input type="hidden" name="action" value="addUser">
                        <div class="form-group">
                            <select
                                name="user-id"
                                class="form-control selectpicker ajax-select"
                                data-live-search="true"
                                data-action="{{ route('admin.users.list-group', ['id' => $group->id]) }}"
                                data-extra-name="action"
                                data-extra-value="get-users"
                                data-token="{{ csrf_token() }}"
                                data-placeholder="Поиск по полям: Логин, E-Mail, Имя, Фамилия, Отчество, Номер телефона"
                                data-select="listGroup"
                            ></select>
                        </div>
                    </form>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-form-label">
                                @if($group->id !== -1)
                                    При удалении пользователя, ему будет назначена группа по умолчанию (Пользователи)
                                @else
                                    Нельзя удалить пользователя из группы по умолчаеию
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($users as $user)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 transGroups">
                                <a class="btn btn-primary btn-block" href="{{ route('admin.users.show', ['id' => $user->id]) }}">
                                    @if($group->id !== -1)
                                        <form method="post" action="{{ route('admin.users.list-group', ['id' => $group->id]) }}">
                                            @csrf
                                            <input type="hidden" name="action" value="removeUser">
                                            <button type="submit" class="close" aria-label="Close" name="id" value="{{ $user->id }}">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </form>
                                    @endif
                                    <strong>{{ $user->getFullName() }}</strong>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer text-right">
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
