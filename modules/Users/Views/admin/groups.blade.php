<?php
$canEdit = getPermissions('admin.users.group.edit');
$canListEdit = getPermissions('admin.users.list-group');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        @if($canEdit)
            <div class="col-12">
                <div class="card animated fadeInRight">
                    <div class="card-header">
                        <h4>Создать группу</h4>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('admin.users.group.edit', ['id' => 'new']) }}">
                            @csrf
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control is-valid" name="group-name"
                                           placeholder="Имя группы" required="required">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-success" type="submit">Добавить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
        @foreach($groups as $group)
            <?php /* @var $group \Modules\Users\Entities\UserPermissionsGroup */ ?>
            <div class="col-12 col-sm-6 col-xl-4 col-xxl-3">
                <div class="card card-sm-4">
                    <div class="card-icon bg-{{ $group->active ? 'primary' : 'danger' }}"><i class="ion ion-person"></i></div>
                    <div class="card-wrap">
                        @if($canEdit)
                            <a href="{{ route('admin.users.group.edit', ['id' => $group->id]) }}">
                        @elseif($canListEdit)
                            <a href="{{ route('admin.users.list-group', ['id' => $group->id]) }}">
                        @endif
                            <div class="card-header">
                                <?php $countUsers = $group->getCountUsers(); ?>
                                <h4>
                                    {{ $countUsers }}
                                    {{ numEnding($countUsers, ['пользователь', 'пользователя', 'пользователей']) }}
                                </h4>
                            </div>
                            <div class="card-body">{{ $group->name }}</div>
                        @if($canEdit)
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection