<?php

namespace Modules\Users\Constants;

class Cambly
{
    public static $defStatusNames = [
        's' => 'student',
        't' => 'teacher',
        'q' => 'request_teach_permissions',
    ];

    public static $addGroups = [
        'account' => 'Аккаунт',
        'settings' => 'Настройки',
        'student' => 'Студент',
        'teacher' => 'Преподаватель',
    ];

    public static $timeZones = [
        "GMT-11:00",
        "GMT-10:00",
        "GMT-09:30",
        "GMT-09:00",
        "GMT-08:00",
        "GMT-07:00",
        "GMT-06:00",
        "GMT-05:00",
        "GMT-04:30",
        "GMT-04:00",
        "GMT-03:30",
        "GMT-03:00",
        "GMT-02:00",
        "GMT-01:00",
        "GMT",
        "GMT+01:00",
        "GMT+02:00",
        "GMT+03:00",
        "GMT+03:30",
        "GMT+04:00",
        "GMT+04:30",
        "GMT+05:00",
        "GMT+05:30",
        "GMT+05:45",
        "GMT+06:00",
        "GMT+06:30",
        "GMT+07:00",
        "GMT+08:00",
        "GMT+08:30",
        "GMT+08:45",
        "GMT+09:00",
        "GMT+09:30",
        "GMT+10:00",
        "GMT+10:30",
        "GMT+11:00",
        "GMT+12:00",
        "GMT+12:45",
        "GMT+13:00",
        "GMT+14:00",
    ];

    public static function getStatus($status)
    {
        if ($status) {
            $result = '';
            for ($i = 0; $i < strlen($status) - 1; $i++) {
                if (isset(self::$defStatusNames[$status[$i]])) {
                    $trans = transDef(self::$defStatusNames[$status[$i]], 'user.status.' . self::$defStatusNames[$status[$i]]);
                    $result .= $trans . ', ';
                }
            }
            $lastChr = substr($status[$i], -1);
            if (isset(self::$defStatusNames[$lastChr])) {
                $trans = transDef(self::$defStatusNames[$lastChr], 'user.status.' . self::$defStatusNames[$lastChr]);
                $result .= $trans;
            }

            return $result ?: '---';
        } else {
            return '---';
        }
    }
}