<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserPermissionsGroup
 * 
 * @property int $id
 * @property string $name
 * @property bool $active
 *
 * @package Modules\Users\Entities
 */
class UserPermissionsGroup extends Eloquent
{
    const DEFAULT = -1;

	protected $table = 'user_permissions_group';

	protected $casts = ['active' => 'bool'];

	protected $fillable = ['name', 'active'];

    public function getCountUsers()
    {
        return \DB::table('users')->where('permission_group', $this->id)->count();
	}
}
