<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAddon
 * 
 * @property int $id
 * @property string $alias
 * @property string $description
 * @property string $group
 * @property int $weight
 * @property string $control_type
 * 
 * @property \Illuminate\Database\Eloquent\Collection $user_add_values
 *
 * @package Modules\Users\Entities
 */
class UserAddon extends Eloquent
{
	public $timestamps = false;

	protected $fillable = ['id', 'alias', 'description', 'group', 'weight', 'control_type'];

	public function user_add_values()
	{
		return $this->hasMany(UserAddValue::class, 'addon_id');
	}
}
