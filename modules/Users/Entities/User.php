<?php

namespace Modules\Users\Entities;

use Illuminate\Support\Facades\Route;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\Payments\Entities\PaymentsInfo;
use Modules\Payments\Entities\PaymentsTime;
use Modules\Reservation\Entities\Reservation;
use Modules\Translations\Constants\Languages;
use Modules\Reviews\Entities\Review;

/**
 * Class User
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $password
 * @property int $permission_group
 * @property bool $email_confirmed
 * @property bool $active
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $user_actions
 * @property \Illuminate\Database\Eloquent\Collection $user_add_values
 */
class User extends Authenticatable
{
    use Notifiable;

    const NO_AVATAR = '/img/no-avatar.png';
    const MAX_DIPLOMA_FILE_SIZE = 5; // Mb

    protected $casts = ['permission_group' => 'int', 'email_confirmed' => 'bool', 'active' => 'bool'];

    protected $hidden = ['password', 'remember_token'];

    protected $fillable = ['login', 'email', 'password', 'permission_group', 'email_confirmed', 'active', 'remember_token'];

    private $addons = null;
    private $hiddenAddons = [];

    private $payedTime = null;

    public function user_actions()
    {
        return $this->hasMany(UserAction::class, 'user_id');
    }

    public function user_add_values()
    {
        return $this->hasMany(UserAddValue::class, 'user_id');
    }

    /**
     * @param string $name
     * @param string|array|\DateTime $value
     * @return bool
     * @throws \Exception
     */
    public function setAddon($name, $value)
    {
        $addon = UserAddon::where('alias', $name)->first();
        $result = false;
        if ($addon) {
            $addValue = $this->user_add_values->where('addon_id', $addon->id)->first();
            /* @var $addValue UserAddValue */
            if ($value !== null) {
                if (is_array($value)) {
                    $value = json_encode($value);
                } elseif ($value instanceof \DateTime) {
                    $value = $value->format('d.m.Y');
                }
                if ($addValue) {
                    $addValue->value = $value;
                    $addValue->save();
                    $result = true;
                } else {
                    $addValue = new UserAddValue();
                    $addValue->user_id = $this->id;
                    $addValue->addon_id = $addon->id;
                    $addValue->value = $value;
                    $addValue->save();
                    $result = true;
                }
            } else {
                if ($addValue) {
                    $addValue->delete();
                    $result = true;
                } else {
                    $result = true;
                }
            }
            if ($result === true) {
                if (isset($this->addons[$addon->group][$addon->alias])) {
                    $this->addons[$addon->group][$addon->alias]['value'] = $value;
                }
            }
        }

        return $result;
    }

    /**
     * @param $name
     * @param bool $returnClass
     * @return string|UserAddValue|null
     * @throws \InvalidArgumentException
     */
    public function addon($name, $returnClass = false)
    {
        if ($returnClass) {
            $result = null;
            $addon = UserAddon::where('alias', $name)->first();
            if ($addon) {
                $result = $this->user_add_values->where('addon_id', $addon->id)->first();
                if (!$result) {
                    $result = new UserAddValue();
                    $result->user_id = $this->id;
                    $result->addon_id = $addon->id;
                }
                return $result;
            }
            throw new \InvalidArgumentException('Unknown addon "' . $name . '"');
        } else {
            $addons = $this->getAddons(true, true);
            if (isset($addons[$name])) {
                try {
                    $arrValue = json_decode($addons[$name]['value'], true);
                } catch (\Exception $e) {}
                return isset($arrValue) ? $arrValue : $addons[$name]['value'];
            } else {
                throw new \InvalidArgumentException('Unknown addon "' . $name . '"');
            }
        }
    }

    /**
     * @param bool $all
     * @param bool $hidden
     * @return array
     */
    public function getAddons($all = false, $hidden = false)
    {
        $allAddons = [];
        foreach ($this->getAddonsByGroups($all, $hidden) as $addons) {
            $allAddons = array_merge($allAddons, $addons);
        }
        return $allAddons;
    }

    /**
     * @param bool $all
     * @param bool $hidden
     * @return array
     */
    public function getAddonsByGroups($all = false, $hidden = false)
    {
        if (!$this->addons) {
            $addValues = [];
            foreach ($this->user_add_values as $value) {
                /* @var $value UserAddValue */
                $addValues[$value->user_addon->alias] = $value->value;
            }
            $addons = [];
            foreach (UserAddon::orderBy('group', 'ASC')->orderBy('weight', 'ASC')->get() as $addon) {
                /* @var $addon UserAddon */
                $addons[$addon->group][$addon->alias] = [
                    'value' => isset($addValues[$addon->alias]) ? $addValues[$addon->alias] : null,
                    'description' => $addon->description,
                    'control-type' => $addon->control_type,
                ];
                if ($addon->id < 0) {
                    $this->hiddenAddons[] = $addon->alias;
                }
            }

            $this->addons = $addons;
        }

        if ($all) {
            return $this->addons;
        } else {
            $result = $this->addons;
            foreach ($result as $group => $addons) {
                if ($hidden) {
                    $result[$group] = array_intersect_key($addons, array_flip($this->hiddenAddons));
                    if (!count($result[$group])) {
                        unset($result[$group]);
                    }
                } else {
                    foreach ($this->hiddenAddons as $addon) {
                        if (isset($result[$group][$addon])) {
                            unset($result[$group][$addon]);
                        } elseif ($hidden) {
                            unset($result[$group][$addon]);
                        }
                    }
                }
            }

            return $result;
        }
    }

    public function getFullName($middleName = false)
    {
        $addons = $this->getAddons();
        if ($addons['name']['value']) {
            $result = $addons['name']['value'];
            if ($middleName && $addons['middle_name']['value']) {
                $result .= ' ' . $addons['middle_name']['value'];
            }
            if ($addons['surname']['value']) {
                $result = $addons['surname']['value'] . ' ' . $result;
            }
            return $result;
        } else {
            return $this->login;
        }
    }

    public function getStatus() {
        if ($this->hasStatus('t')) {
            return 't';
        } elseif ($this->hasStatus('q')) {
            return 'q';
        } elseif ($this->hasStatus('s')) {
            return 's';
        } else {
            return null;
        }
    }

    public function hasStatus(string $status)
    {
        return strpos($this->addon('status'), $status) !== false;
    }

    private function favorites()
    {
        $favorites = $this->addon('favorites') ?: [];
        $status = $this->getStatus();
        $result = null;
        if ($status) {
            if ($status === 't') {
                $status = 's';
            } else {
                $status = 't';
            }
            $result = User::whereIn('id', array_keys($favorites))->where('active', true)
                ->whereHas('user_add_values', function($query) use ($status) {
                    $query->whereHas('user_addon', function($query) {
                        $query->where('alias', 'status');
                    })->where(function($query) use ($status) {
                        $query->where('value', 'like', '%' . $status . '%');
                    });
                })->select('id');
        }

        return $result;
    }

    public function getFavorites()
    {
        $result = [];
        $favorites = $this->favorites();
        if ($favorites) {
            $users = $favorites->get()->keyBy('id');
            foreach ($users as $id => $user) {
                $result[$id] = true;
            }
        }

        return $result;
    }

    public function getFavoritesCount()
    {
        $result = 0;
        $favorites = $this->favorites();
        if ($favorites) {
            $result = $favorites->count('id');
        }

        return $result;
    }

    public function getRating($status = null)
    {
        $result = 0;
        if (($user = \Auth::user()) && ($status = $status ?: $user->getStatus())) {
            $rating = $this->addon('rating');
            if (isset($rating[$status]) && count($rating[$status])) {
                foreach ($rating[$status] as $value) {
                    $result += $value;
                }
                $result = round($result / count($rating[$status]), 2);
            }
        }

        return $result;
    }

    public function getWatchList($userId = null)
    {
        $status = $this->getStatus();
        $users = [];
        if ($status) {
            $status = $status === 't' ? 's' : 't';
            $watchList = $this->addon('watch_list') ?: [];
            $users = User::where('active', true)
                ->whereIn('id', array_keys($watchList))
                ->whereHas('user_add_values', function($query) use($status) {
                    $query->whereHas('user_addon', function($query) {
                        $query->where('alias', 'status');
                    })->where(function($query) use($status) {
                        $query->where('value', 'like', '%' . $status . '%');
                    });
                })->select('id');
            if ($userId) {
                return !!$users->where('id', $userId)->first();
            }
            $users = $users->get()->keyBy('id');
            foreach ($users as $key => $val) {
                $users[$key] = false;
            }
        }

        return $users;
    }

    /**
     * @param string|array $route
     * @return int|mixed
     */
    public function getPermissions($route)
    {
        if (!$route instanceof \Illuminate\Routing\Route) {
            $route = app('router')->getRoutes()->getByName($route);
        }

        $permission = null;
        if ($route instanceof \Illuminate\Routing\Route) {
            if ($this->permission_group > 0) {
                $linkTo = UserPermission::LINK_GROUP;
                $parentId = $this->permission_group;
                $active = false;
                $group = UserPermissionsGroup::find($this->permission_group);
                if ($group) {
                    $active = $group->active;
                }
            } else {
                $linkTo = UserPermission::LINK_USER;
                $parentId = $this->id;
                $active = true;
            }

            if (!$active) {
                $permission = null;
            } else {
                $permission = UserPermission::where([
                    'link_to' => $linkTo,
                    'parent_id' => $parentId,
                    'route_name' => $route->getName(),
                ])->first();
                if (!$permission) {
                    $permission = UserPermission::where([
                        'link_to' => $linkTo,
                        'parent_id' => $parentId,
                        'route_name' => $route->getPrefix(),
                    ])->first();
                }
            }
        }

        if ($permission) {
            return $permission->permission;
        }

        return UserPermission::P_DENY;
    }

    /**
     * @return bool
     */
    public function hasAdminPermissions()
    {
        $hasAdmin = false;

        if ($this->permission_group !== 0) {
            $permissions = UserPermission::where([
                'link_to' => UserPermission::LINK_GROUP,
                'parent_id' => $this->permission_group,
            ])->get();
        } else {
            $permissions = UserPermission::where([
                'link_to' => UserPermission::LINK_USER,
                'parent_id' => $this->id,
            ])->get();
        }

        $prefixes = \Cache::get('prefixes');
        foreach ($permissions as $permission) {
            /* @var $permission UserPermission */
            if ($permission->permission) {
                if (isset($prefixes[$permission->route_name])) {
                    $hasAdmin = true;
                    break;
                }
                if ($route = Route::getRoutes()->getByName($permission->route_name)) {
                    $uri = $route->uri();
                    if ($uri == 'admin' || substr($uri, 0, 6) == 'admin/') {
                        $hasAdmin = true;
                        break;
                    }
                }
            }
        }

        return $hasAdmin;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->addon('avatar')
            ? url($this->addon('avatar'))
            : url(self::NO_AVATAR);
    }

    /**
     * @param bool $all
     * @return array
     * @throws \Exception
     */
    public function toArray($all = false)
    {
        $result = [
            'id' => $this->id,
            'login' => $this->login,
        ];
        if ($all) {
            $result['email'] = $this->email;
        }
        $user = \Auth::user();
        if ($user) {
            $status = $user->getStatus();
            if ($status === 't' && $this->hasStatus('s')) {
                $status = 's';
            } elseif ($status === 's' && $this->hasStatus('t')) {
                $status = 't';
            } else {
                $status = $this->getStatus();
            }
        } else {
            $status = $this->getStatus();
        }
        foreach ($this->getAddons(false, false) as $key => $addon) {
            $prefix = substr($key, 0, 3);
            if ($prefix !== 'st_' && $prefix !== 'tc_') {
                $prefix = false;
            }
            if ($all || $prefix === false || ($status === 's' && $prefix === 'st_') || ($status === 't' && $prefix === 'tc_')) {
                $result[$key] = $this->addon($key);
            }
        }
        $result['status'] = $status;

        $socials = getAvailableSocial();
        foreach ($socials as $social) {
            $result['social_' . $social] = !($result['social_' . $social] === null);
        }
        $result['avatar'] = $this->getAvatar();
        $result['country'] = html_entity_decode(Languages::getCountry($result['country']));

        $result['speak_langs'] = array_intersect_key(getLocales(), ($result['speak_langs'] ?: []));

        if ($all || $status === 's') {
            $result['st_dialect'] = UserAddSettings::where('control_type', 'dialect')
                ->first()->transValue($result['st_dialect']);
            $result['st_lang_level'] = UserAddSettings::where('control_type', 'lang_level')
                ->first()->transValue($result['st_lang_level']);
            $result['st_lesson_type'] = UserAddSettings::where('control_type', 'lesson_type')
                ->first()->transValue($result['st_lesson_type']);
            unset($result['rating']);
        }
        if ($all || $status === 't') {
            $result['rating'] = $this->getRating('s');
            $result['tc_video_url'] = $result['tc_video_url'] ? url($result['tc_video_url']) : null;
            $result['tc_diploma_url'] = $result['tc_diploma_url'] ? url($result['tc_diploma_url']) : null;

            $result['tc_exp'] = UserAddSettings::where('control_type', 'tc_exp')
                ->first()->transValue($result['tc_exp']);
            $result['tc_dialect'] = UserAddSettings::where('control_type', 'dialect')
                ->first()->transValue($result['tc_dialect']);
            $result['tc_style'] = UserAddSettings::where('control_type', 'tc_style')
                ->first()->transValue($result['tc_style']);
            $result['tc_prefer_level'] = UserAddSettings::where('control_type', 'lang_level')
                ->first()->transValue($result['tc_prefer_level']);
            $result['tc_lesson_type'] = UserAddSettings::where('control_type', 'lesson_type')
                ->first()->transValue($result['tc_lesson_type']);

            $now = new \DateTime();
            $busy = Reservation::where('user_id', $this->id)
                ->select('time')
                ->whereNotNull('reserved_user_id')
                ->where(function($query) use($now) {
                    $query->where('time', '>', $now)
                        ->orWhere(function($query) use($now) {
                            $query->where('time', '<', $now)
                                ->whereRaw('time + interval duration minute > "' . $now->format('Y-m-d H:i:s') . '"');
                        });
                })
                ->orderBy('time', 'ASC')
                ->first();
            if ($busy) {
                $busy = round(($busy->time->getTimestamp() - $now->getTimestamp()) / 60);
                $busy = $busy < 60 ? ($busy < 0 ? true : $busy) : false;
            } else {
                $busy = false;
            }

            $result['busy'] = $busy;
        }
        $result['chats'] = findUserDialogs($this);

        foreach ($result as $key => $value) {
            if ($value === null) {
                $result[$key] = '';
            }
        }

        if (!$all) {
            unset($result['phone']);
            unset($result['invitees_list']);
        }

        return $result;
    }

    /**
     * @return PaymentsInfo|null
     * @throws \Exception
     */
    public function getActivePayment() {
        if ($this->getStatus() !== 's') {
            throw new \Exception('User is not a student');
        }

        $now = new \DateTime();
        $payment = PaymentsInfo::where([
            'user_id' => $this->id,
            'paid' => true,
        ])->where('from', '<', $now)->where('to', '>', $now)
            ->orderByDesc('to')->first();

        return $payment ?: null;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getFreeTime() {
        if ($this->getStatus() !== 's') {
            throw new \Exception('User is not a student');
        }

        $freeTime = $this->addon('st_free_time');

        return (!$freeTime || $freeTime < 0) ? 0 : (int)$freeTime;
    }

    /**
     * @param null $userId
     * @param null $time
     * @return PaymentsTime|null
     */
    private function getPeriod($userId = null, $time = null) {
        if (!$userId) {
            $userId = $this->id;
        }
        if (!$time instanceof \DateTime) {
            $time = new \DateTime();
        }

        return PaymentsTime::where('from', '<=', $time)->where('to', '>', $time)
            ->whereHas('info', function($query) use($userId) {
                $query->where([
                    'user_id' => $userId,
                    'paid' => true,
                ]);
            })
            ->orderBy('from', 'ASC')->first();
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getPayedTime() {
        if ($this->getStatus() !== 's') {
            throw new \Exception('User is not a student');
        }

        $now = new \DateTime();
        if (is_array($this->payedTime) && $this->payedTime['date'] === $now->format('d.m.Y')) {
            $seconds = $this->payedTime['time'];
        } else {
            $seconds = 0;
            $period = $this->getPeriod($this->id, $now);

            if ($period) {
                $seconds = $period->available_time;
                $this->payedTime = [
                    'date' => $now->format('d.m.Y'),
                    'time' => $period->available_time,
                ];
            }
        }

        return $seconds < 0 ? 0: $seconds;
    }

    /**
     * @param User|int $teacher
     * @param string|int $time
     * @return $this
     * @throws \Exception
     */
    public function useTime($teacher, $time)
    {
        if ($this->getStatus() !== 's') {
            throw new \Exception('User is not a student');
        }
        if ($teacher instanceof User) {
            $teacherId = $teacher->id;
        } else {
            $user = User::find($teacher);
            if (!$user) {
                $user = User::where('login', $teacher);
            }

            if ($user) {
                $teacherId = $user->id;
            } else {
                throw new \Exception('Teacher not found');
            }
        }

        $now = new \DateTime();
        $period = $this->getPeriod($teacherId, $now);
        if ($period) {
            $payedTime = $period->available_time - $time;
            if ($payedTime < 0) {
                $freeTime = $this->getFreeTime() + $payedTime;
                if ($freeTime < 0) {
                    $nextPeriod = PaymentsTime::where([
                        'payment_id' => $period->payment_id,
                        'from' => $period->to,

                    ])->orderBy('from', 'ASD')->first();
                    if ($nextPeriod && !(($nextPeriod->available_time + $freeTime) < 0)) {
                        $nextPeriod->available_time = $nextPeriod->available_time + $freeTime;
                        $nextPeriod->save();
                    }
                    $freeTime = 0;
                }
                $this->setAddon('st_free_time', $freeTime);
                $payedTime = 0;
            }
            $period->available_time = $payedTime;
            $period->save();
            $this->payedTime = [
                'date' => $now->format('d.m.Y'),
                'time' => $payedTime,
            ];
        }

        return $this;
    }

    public function getOnlineCount()
    {
        $status = $this->getStatus();
        $result = false;

        if ($status) {
            $online = getOnlineUsers();
            $online = (is_array($online) && count($online)) ? array_keys($online) : [''];

            $status = $status === 't' ? 's' : 't';

            $result = User::where('active', true)
                ->where('id', '!=', $this->id)
                ->whereIn('id', $online)
                ->whereHas('user_add_values', function($query) use($status) {
                    $query->whereHas('user_addon', function($query){
                        $query->where('alias', 'status');
                    })->where(function($query) use ($status) {
                        $query->where('value', 'like', '%' . $status . '%');
                    });
                })->count('id');
        }

        return $result;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getAvailableTime() {
        if ($this->getStatus() !== 's') {
            throw new \Exception('User is not a student');
        }

        return $this->getFreeTime() + $this->getPayedTime();
    }

    public function delete()
    {
        $addValues = UserAddValue::where('user_id', $this->id)->get();
        UserAddValue::unguard();
        foreach ($addValues as $value) {
            /* @var $value UserAddValue */
            $value->delete();
        }
        UserAddValue::reguard();

        $actions = UserAction::where('user_id', $this->id)->get();
        UserAction::unguard();
        foreach ($actions as $action) {
            /* @var $action UserAction */
            $action->delete();
        }
        UserAction::reguard();

        $reviews = Review::where('user_id', $this->id)->get();
        Review::unguard();
        foreach ($reviews as $review) {
            /* @var $review Review */
            $review->delete();
        }
        Review::reguard();

        parent::delete();
    }
}
