<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAction
 *
 * @property int $id
 * @property int $user_id
 * @property int $action_id
 * @property string $token
 * @property \Carbon\Carbon $created_at
 *
 * @property User $user
 */
class UserAction extends Eloquent
{
    public const A_EMAIL_CONFIRM             = 1;
    public const EMAIL_CONFIRM_LEFT_HOURS    = 24 * 1;
    public const A_RESTORE_PASSWORD          = 2;
    public const RESTORE_PASSWORD_LEFT_HOURS = self::EMAIL_CONFIRM_LEFT_HOURS;

	public $timestamps = false;

	protected $casts = ['user_id' => 'int', 'action_id' => 'int'];

	protected $hidden = ['token'];

	protected $fillable = ['user_id', 'action_id', 'token'];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (empty($this->created_at)) {
            $this->created_at = new \DateTime();
        }
    }
}
