<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\MCms\Entities\JsonCast;

/**
 * Class UserAddSettings
 *
 * @property string $control_type
 * @property string $default_name
 * @property array $value
 *
 * @package Modules\Users\Entities
 */
class UserAddSettings extends Eloquent
{
    use JsonCast;

    const TRANS_GROUP = 'addons-settings';

    protected $casts = ['value' => 'json'];

	protected $fillable = ['control_type', 'default_name', 'value'];

    public function transValue($keysInArr = null)
    {
        $result = [];
        if (is_array($this->value)) {
            if (is_int($keysInArr)) {
                if (isset($this->value[$keysInArr])) {
                    $result = transDef($this->value[$keysInArr], self::TRANS_GROUP . '.' . $this->control_type . '.' . $this->value[$keysInArr]);
                } else {
                    $result = null;
                }
            } else {
                foreach ($this->value as $key => $value) {
                    if (is_array($keysInArr) && !isset($keysInArr[$key])) {
                        continue;
                    }
                    $result[$key] = transDef($value, self::TRANS_GROUP . '.' . $this->control_type . '.' . $value);
                }
            }
        }

        return $result;
	}
}
