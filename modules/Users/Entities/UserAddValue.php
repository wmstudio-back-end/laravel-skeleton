<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAddValue
 * 
 * @property int $user_id
 * @property int $addon_id
 * @property string $value
 * 
 * @property User $user
 * @property UserAddon $user_addon
 *
 * @package Modules\Users\Entities
 */
class UserAddValue extends Eloquent
{
	protected $casts = ['user_id' => 'int', 'addon_id' => 'int'];

	protected $fillable = ['user_id', 'addon_id', 'value'];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function user_addon()
	{
		return $this->belongsTo(UserAddon::class, 'addon_id');
	}
}
