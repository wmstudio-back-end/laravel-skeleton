<?php

namespace Modules\Users\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserPermission
 * 
 * @property int $link_to
 * @property int $parent_id
 * @property string $route_name
 * @property int $permission
 *
 * @package Modules\Users\Entities
 */
class UserPermission extends Eloquent
{
    const LINK_USER   = 1;
    const LINK_GROUP  = 2;

    const P_WRITE     = 2;
    const P_READ      = 1;
    const P_DENY      = 0;

    public static $permissionNames = [
        self::P_DENY  => 'Запретить',
        self::P_READ  => 'Чтение',
        self::P_WRITE => 'Запись',
    ];

	protected $casts = ['link_to' => 'int', 'parent_id' => 'int', 'permission' => 'int'];

	protected $fillable = ['link_to', 'parent_id', 'route_name', 'permission'];
}
