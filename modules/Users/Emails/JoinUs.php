<?php

namespace Modules\Users\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class JoinUs extends Mailable
{
    use Queueable, SerializesModels;

    private $referralUrl;

    /**
     * Create a new message instance.
     *
     * @param string $referralUrl
     */
    public function __construct($referralUrl)
    {
        $this->referralUrl = $referralUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from = ($from = env('MAIL_FROM_NAME')) ? $from . ' | ' . setting('main_title') : setting('main_title');
        $name = \Auth::user()->getFullName();
        $siteName = setting('main_title') ?: config('app.name', 'Laravel');
        return $this->from(env('MAIL_FROM_ADDRESS'), $from)
            ->subject(___('global.mails.join-us.subject', [
                'name' => $name,
                'sitename' => $siteName,
            ]))
            ->view('mails.join-us', [
                'name' => \Auth::user()->getFullName(),
                'siteName' => $siteName,
                'referralUrl' => $this->referralUrl,
            ]);
    }
}
