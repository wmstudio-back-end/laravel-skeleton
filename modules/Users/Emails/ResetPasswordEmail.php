<?php

namespace Modules\Users\Emails;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Users\Entities\UserAction;

class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $action;

    /**
     * Create a new message instance.
     *
     * @param int|UserAction $action
     */
    public function __construct($action)
    {
        if ($action instanceof UserAction) {
            $this->action = $action;
        } elseif ($action = UserAction::find($action)) {
            $this->action = $action;
        } else {
            throw new \InvalidArgumentException('Unknown action|action id');
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date = new \DateTime('+' . UserAction::RESTORE_PASSWORD_LEFT_HOURS . ' hours');
        Carbon::setLocale(app()->getLocale());
        $timeLeft = Carbon::create(
            $date->format('Y'),
            $date->format('m'),
            $date->format('d'),
            $date->format('H'),
            $date->format('i'),
            $date->format('s')
        )->diffForHumans(null, true, false, 2);
        return $this->from(env('MAIL_FROM_ADDRESS'), setting('main_title'))
            ->subject(___('global.mails.email-reset.subject'))
            ->view('mails.email-reset', [
                'name' => $this->action->user->getFullName(),
                'confirmUrl' => route('password.reset', ['token' => $this->action->token]),
                'timeLeft' => $timeLeft,
            ]);
    }
}
