<?php

namespace Modules\Users\Emails;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Users\Entities\UserAction;

class EmailConfirm extends Mailable
{
    use Queueable, SerializesModels;

    private $date;

    private $confirmUrl;

    /**
     * Create a new message instance.
     *
     * @param \DateTime|string $date
     * @param string $confirmUrl
     */
    public function __construct($date, $confirmUrl)
    {
        if ($date instanceof \DateTime) {
            $this->date = $date->format('d.m.Y H:i');
        } else {
            $this->date = $date;
        }
        $this->confirmUrl = $confirmUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $date = new \DateTime('+' . UserAction::EMAIL_CONFIRM_LEFT_HOURS . ' hours');
        Carbon::setLocale(app()->getLocale());
        $timeLeft = Carbon::create(
            $date->format('Y'),
            $date->format('m'),
            $date->format('d'),
            $date->format('H'),
            $date->format('i'),
            $date->format('s')
        )->diffForHumans(null, true, false, 2);
        $from = ($from = env('MAIL_FROM_NAME')) ? $from . ' | ' . setting('main_title') : setting('main_title');
        return $this->from(env('MAIL_FROM_ADDRESS'), $from)
            ->subject(___('global.mails.email-confirm.subject'))
            ->view('mails.email-confirm', [
                'date' => $this->date,
                'confirmUrl' => $this->confirmUrl,
                'timeLeft' => $timeLeft,
            ]);
    }
}
