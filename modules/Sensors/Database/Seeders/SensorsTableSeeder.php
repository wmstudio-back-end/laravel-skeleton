<?php

namespace Modules\Sensors\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Sensors\Entities\Sensor;

class SensorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sensors = [
            [
                'id'          => 1,
                'name'        => 'Формы для связи',
                'description' => 'Все действия с формами на сайте',
                'alias'       => 'forms',
                'weight'      => 1,
                'active'      => true,
            ],
            [
                'id'          => 2,
                'name'        => 'Ссылки на скачивание',
                'description' => 'Ссылки на скачивание презентаций и предложений',
                'alias'       => 'download',
                'weight'      => 2,
                'active'      => true,
            ],
        ];

        Sensor::unguard();
        Sensor::insert($sensors);
        Sensor::reguard();
    }
}
