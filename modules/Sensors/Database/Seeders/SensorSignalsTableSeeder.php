<?php

namespace Modules\Sensors\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Sensors\Entities\SensorSignal;

class SensorSignalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $signals = [
            [
                'id'                  => 1,
                'sensor_id'           => 1,
                'name'                => 'Обратная связь',
                'description'         => 'Срабатывает, когда посетитель отправляет отзыв из формы обратной связи.',
                'backend_description' => 'Обратная связь',
                'alias'               => 'feedback_send',
                'weight'              => 1,
                'active'              => true,
            ],
            [
                'id'                  => 2,
                'sensor_id'           => 1,
                'name'                => 'Заказ',
                'description'         => 'Срабатывает, когда посетитель отправляет форму заказа.',
                'backend_description' => 'Заказ',
                'alias'               => 'order_send',
                'weight'              => 2,
                'active'              => true,
            ],
        ];

        SensorSignal::unguard();
        SensorSignal::insert($signals);
        SensorSignal::reguard();
    }
}
