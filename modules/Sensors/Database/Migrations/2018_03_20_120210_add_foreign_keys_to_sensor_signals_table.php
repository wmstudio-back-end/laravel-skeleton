<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSensorSignalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sensor_signals', function(Blueprint $table)
		{
			$table->foreign('sensor_id', 'sensor_signals_ibfk_1')->references('id')->on('sensors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});

        Artisan::call('db:seed', array('--class' => \Modules\Sensors\Database\Seeders\SensorSignalsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sensor_signals', function(Blueprint $table)
		{
			$table->dropForeign('sensor_signals_ibfk_1');
		});
	}

}
