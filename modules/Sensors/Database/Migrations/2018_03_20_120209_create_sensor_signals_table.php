<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSensorSignalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensor_signals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('sensor_id')->index('sensor_id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->string('backend_description');
			$table->string('alias');
			$table->integer('weight');
			$table->boolean('active');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sensor_signals');
	}

}
