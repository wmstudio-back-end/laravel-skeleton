<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSensorCallsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sensor_calls', function(Blueprint $table)
		{
			$table->foreign('sensor_id', 'sensor_calls_ibfk_1')->references('id')->on('sensors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('signal_id', 'sensor_calls_ibfk_2')->references('id')->on('sensor_signals')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sensor_calls', function(Blueprint $table)
		{
			$table->dropForeign('sensor_calls_ibfk_1');
			$table->dropForeign('sensor_calls_ibfk_2');
		});
	}

}
