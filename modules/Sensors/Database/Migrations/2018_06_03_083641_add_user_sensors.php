<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Sensors\Entities\Sensor;
use Modules\Sensors\Entities\SensorSignal;
use Modules\Sensors\Entities\SensorCall;

class AddUserSensors extends Migration
{
    private $signals = [
        'request_teach_permissions' => [
            'name' => 'Запрос прав на преподавание',
            'description' => 'Срабатывает когда в первый раз на странице "аккаунт" ставят галочку "Преподаватель"',
            'backend_description' => 'Запросили права на преподавание',
        ],
        'teach_permissions_confirmed' => [
            'name' => 'Подтверждение прав на преподавание',
            'description' => 'Срабатывает кодга пользователю подтверждают права преподавать.',
            'backend_description' => 'Разрешено преподавать',
        ],
        'teach_permissions_denied' => [
            'name' => 'Отказ в правах на преподавание',
            'description' => 'Срабатывает кодга пользователю запрещают права преподавать.',
            'backend_description' => 'Запрещено преподавать',
        ],
        'phone_changed' => [
            'name' => 'Изменение номера телефона',
            'description' => 'Срабатывает когда пользователь меняет номер телефона',
            'backend_description' => 'Изменили номера телефона',
        ],
        'email_changed' => [
            'name' => 'Изменение E-Mail адреса',
            'description' => 'Срабатывает когда пользователь меняет E-Mail',
            'backend_description' => 'Изменили E-Mail адрес',
        ],
        'account_completed' => [
            'name' => 'Заполнены все данные на странице аккаунт',
            'description' => 'Срабатывает когда пользователь заполняет все обязательные поля',
            'backend_description' => 'Заполлнили аккаунт',
        ],
        'profile_student_completed' => [
            'name' => 'Заполнены все данные в профиле студента',
            'description' => 'Срабатывает когда пользователь заполняет все обязательные поля',
            'backend_description' => 'Заполнили профиль студента',
        ],
        'profile_teacher_completed' => [
            'name' => 'Заполнены все данные в профиле перподавателя',
            'description' => 'Срабатывает когда пользователь заполняет все обязательные поля',
            'backend_description' => 'Заполнили профиль преподавателя',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sensor = Sensor::create([
            'name' => 'Действия пользователей',
            'description' => 'Все регистрируемые действия пользователей',
            'alias' => 'users',
            'weight' => Sensor::all()->max('id') + 1,
            'active' => true,
        ]);

        $weight = 0;
        $insert = [];
        foreach ($this->signals as $alias => $signal) {
            $insert[] = [
                'sensor_id' => $sensor->id,
                'name' => $signal['name'],
                'description' => $signal['description'],
                'backend_description' => $signal['backend_description'],
                'alias' => $alias,
                'weight' => ++$weight,
                'active' => true,
            ];
        }
        SensorSignal::unguard();
        SensorSignal::insert($insert);
        SensorSignal::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $sensor = Sensor::where('alias', 'users')->first();
        $signals = SensorSignal::whereIn('alias', array_keys($this->signals))->get()->keyBy('id');
        $calls = SensorCall::where('sensor_id', $sensor->id)->whereIn('signal_id', $signals->keys())->get();
        SensorCall::unguard();
        foreach ($calls as $call) {
            /* @var $call SensorCall */
            $call->delete();
        }
        SensorCall::reguard();
        SensorSignal::unguard();
        foreach ($signals as $signal) {
            /* @var $signal SensorSignal */
            $signal->delete();
        }
        SensorSignal::reguard();
        $sensor->delete();
    }
}
