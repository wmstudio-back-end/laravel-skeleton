<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSensorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sensors', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->text('description');
			$table->string('alias');
			$table->integer('weight');
			$table->boolean('active');
		});

        Artisan::call('db:seed', array('--class' => \Modules\Sensors\Database\Seeders\SensorsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sensors');
	}

}
