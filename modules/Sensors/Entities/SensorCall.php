<?php

namespace Modules\Sensors\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;

/**
 * Class SensorCall
 * 
 * @property int $id
 * @property int $signal_id
 * @property int $sensor_id
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * 
 * @property Sensor $sensor
 * @property SensorSignal $signal
 * @property User $user
 */
class SensorCall extends Eloquent
{
    public $timestamps = false;

	protected $casts = [
	    'signal_id' => 'int',
        'sensor_id' => 'int',
        'user_id' => 'int',
    ];

	protected $fillable = [
	    'signal_id',
        'sensor_id',
        'user_id',
    ];

	public function sensor()
	{
		return $this->belongsTo(Sensor::class, 'sensor_id');
	}

    public function signal()
    {
        return $this->belongsTo(SensorSignal::class, 'signal_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function call(string $sensor, string $signal, $user = null)
    {
        $result = null;
        $sensor = Sensor::where([
            'alias' => $sensor,
            'active' => true,
        ])->first();
        if ($sensor) {
            $signal = SensorSignal::where([
                'alias' => $signal,
                'active' => true,
            ])->first();
            if ($signal) {
                $userId = null;
                if (is_int($user)) {
                    $userId = null;
                } elseif(is_string($user)) {
                    $user = User::where('login', $user)->first();
                    if ($user) {
                        $userId = $user->id;
                    }
                } elseif ($user instanceof User) {
                    $userId = $user->id;
                }
                $result = self::create([
                    'sensor_id'  => $sensor->id,
                    'signal_id'  => $signal->id,
                    'user_id'    => $userId,
                    'created_at' => new \DateTime(),
                ]);
            }
        }

        return $result;
    }
}
