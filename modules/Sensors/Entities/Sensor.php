<?php

namespace Modules\Sensors\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class Sensor
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $alias
 * @property int $weight
 * @property bool $active
 * 
 * @property \Illuminate\Database\Eloquent\Collection $sensor_calls
 * @property \Illuminate\Database\Eloquent\Collection $sensor_signals
 */
class Sensor extends Eloquent
{
    public $timestamps = false;

    protected $casts = ['weight' => 'int', 'active' => 'bool'];

	protected $fillable = ['name', 'description', 'alias', 'weight', 'active'];

	public function sensor_calls()
	{
		return $this->hasMany(\Modules\Sensors\Entities\SensorCall::class, 'sensor_id');
	}

	public function sensor_signals()
	{
		return $this->hasMany(\Modules\Sensors\Entities\SensorSignal::class, 'sensor_id');
	}
}
