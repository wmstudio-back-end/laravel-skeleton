<?php

namespace Modules\Sensors\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class SensorSignal
 * 
 * @property int $id
 * @property int $sensor_id
 * @property string $name
 * @property string $description
 * @property string $backend_description
 * @property string $alias
 * @property int $weight
 * @property bool $active
 * 
 * @property \Modules\Sensors\Entities\Sensor $sensor
 * @property \Illuminate\Database\Eloquent\Collection $sensor_calls
 */
class SensorSignal extends Eloquent
{
    public $timestamps = false;

	protected $casts = ['sensor_id' => 'int', 'weight' => 'int', 'active' => 'bool'];

	protected $fillable = ['sensor_id', 'name', 'description', 'backend_description', 'alias', 'weight', 'active'];

	public function sensor()
	{
		return $this->belongsTo(\Modules\Sensors\Entities\Sensor::class, 'sensor_id');
	}

	public function sensor_calls()
	{
		return $this->hasMany(\Modules\Sensors\Entities\SensorCall::class, 'signal_id');
	}
}
