<?php

namespace Modules\Reservation\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;

/**
 * Class Reservation
 * 
 * @property int $id
 * @property int $user_id
 * @property \Carbon\Carbon $time
 * @property int $duration
 * @property int $reserved_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 * @property User|null $reserved
 *
 * @package App\Models
 */
class Reservation extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
        'duration' => 'int',
		'reserved_user_id' => 'int'
	];

	protected $dates = [
		'time'
	];

	protected $fillable = [
		'user_id',
		'time',
        'duration',
		'reserved_user_id',
	];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function reserved()
    {
        return $this->belongsTo(User::class, 'reserved_user_id');
    }
}
