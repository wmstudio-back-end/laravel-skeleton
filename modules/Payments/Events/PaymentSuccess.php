<?php

namespace Modules\Payments\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Payments\Entities\PaymentsInfo;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class PaymentSuccess
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /* @var $order PaymentsInfo */
    public $order;

    /**
     * Create a new event instance.
     *
     * @param $order
     * @throws \Exception
     */
    public function __construct($order)
    {
        if ($order instanceof PaymentsInfo) {
            $this->order = $order;
        } else {
            if (is_int($order)) {
                $this->order = PaymentsInfo::find($order);
            } else {
                $this->order = PaymentsInfo::where('uuid', $order)->first();
            }
        }

        if (!$this->order) {
            throw new \Exception('Order not found');
        }
    }
}
