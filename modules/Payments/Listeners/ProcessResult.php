<?php

namespace Modules\Payments\Listeners;

use Modules\Payments\Entities\PaymentsInfo;
use Modules\Payments\Entities\PaymentsTime;
use Modules\Payments\Events\PaymentSuccess as Event;

class ProcessResult
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Event $event
     * @return void
     */
    public function handle(Event $event)
    {
        $order = $event->order;
        $tariffInfo = $order->tariff_info;

        $start = $order->from;
        $end = $order->to;

        $dates = [];
        $date = (clone $start);
        while($date < $end) {
            $dates[] = $date;
            $date = (clone $date)->modify('+1 ' . PaymentsInfo::ACTIVE_PERIOD);
        }
        if (end($dates) < $end) {
            $dates[] = $end;
        }

        $activePeriodMinutes = $tariffInfo['minutes-per-day'] * $tariffInfo['lessons-per-week'];
        $tariffSeconds = $tariffInfo['tariff']['summary']['seconds'];
        $periodSeconds = $activePeriodMinutes * 60; // in seconds
        $periods = [];
        for ($i = 1; $i < count($dates); $i++) {
            $periods[] = [
                'payment_id' => $order->id,
                'from' => $dates[$i - 1],
                'to' => $dates[$i],
                'available_time' => $tariffSeconds > $periodSeconds ? $periodSeconds : $tariffSeconds,
                'created_at' => $start,
                'updated_at' => $start,
            ];
            $tariffSeconds -= $periodSeconds;
        }

        // Если в последнюю неделю осталось
        //   меньше половины времени активного периода
        //   то присоединяем ее к предпоследней
        end($periods);
        if ($periods[key($periods)]['available_time'] < ($periodSeconds / 2)) {
            $periods[key($periods) - 1]['to'] = $periods[key($periods)]['to'];
            $periods[key($periods) - 1]['available_time'] += $periods[key($periods)]['available_time'];
            unset($periods[key($periods)]);
        }
        PaymentsTime::insert($periods);
    }
}
