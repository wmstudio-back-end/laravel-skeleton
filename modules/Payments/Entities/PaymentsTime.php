<?php

namespace Modules\Payments\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PaymentsTime
 * 
 * @property int $id
 * @property int $payment_id
 * @property \Carbon\Carbon $from
 * @property \Carbon\Carbon $to
 * @property int $available_time
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property PaymentsInfo $info
 *
 * @package App\Models
 */
class PaymentsTime extends Eloquent
{
    protected $table = 'payments_time';

	protected $casts = [
		'payment_id' => 'int',
        'available_time' => 'int',
	];

	protected $dates = [
		'from',
		'to',
	];

	protected $fillable = [
		'payment_id',
		'from',
		'to',
		'available_time',
	];

    public function info()
    {
        return $this->belongsTo(PaymentsInfo::class, 'payment_id');
    }
}
