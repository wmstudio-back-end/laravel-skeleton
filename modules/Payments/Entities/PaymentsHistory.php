<?php

namespace Modules\Payments\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;
use Modules\MCms\Entities\JsonCast;

/**
 * Class PaymentsHistory
 * 
 * @property int $id
 * @property int $user_id
 * @property \Carbon\Carbon $date
 * @property array $payment_info
 * @property float $cash
 * @property bool $paid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property User $teacher
 *
 * @package App\Models
 */
class PaymentsHistory extends Eloquent
{
    use JsonCast;

    protected $table = 'payments_history';

	protected $casts = [
		'user_id' => 'int',
        'payment_info' => 'json',
        'cash' => 'float',
		'paid' => 'bool',
	];

	protected $dates = [
		'date',
	];

	protected $fillable = [
		'user_id',
		'date',
		'payment_info',
        'cash',
		'paid',
	];

    protected $attributes = [
        'paid' => false,
        'cash' => 0,
    ];

    public function teacher()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('payment_info', $attributes)) {
            $this->payment_info = [];
        }
    }
}
