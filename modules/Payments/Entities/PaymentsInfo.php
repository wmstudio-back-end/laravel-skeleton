<?php

namespace Modules\Payments\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;
use Modules\MCms\Entities\JsonCast;

/**
 * Class PaymentsInfo
 *
 * @property int $id
 * @property string $uuid
 * @property int $user_id
 * @property \Carbon\Carbon $from
 * @property \Carbon\Carbon $to
 * @property array $tariff_info
 * @property array $additional_info
 * @property string $period
 * @property bool $paid
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $student
 * @property \Illuminate\Database\Eloquent\Collection $periods
 *
 * @property string $currency
 */
class PaymentsInfo extends Eloquent
{
    use JsonCast;

    const ACTIVE_PERIOD = 'week';

    protected $table = 'payments_info';

	protected $casts = [
		'user_id' => 'int',
        'tariff_info' => 'json',
        'additional_info' => 'json',
		'paid' => 'bool',
	];

	protected $dates = [
		'from',
		'to',
	];

	protected $fillable = [
		'user_id',
		'from',
		'to',
		'tariff_info',
		'additional_info',
		'period',
		'paid',
	];

    protected $attributes = [
        'paid' => false,
    ];

    public function student()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function periods()
    {
        return $this->hasMany(PaymentsTime::class, 'payment_id');
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('tariff_info', $attributes)) {
            $this->tariff_info = [];
        }
        if (!array_key_exists('additional_info', $attributes)) {
            $this->additional_info = [];
        }
    }

    public function getCurrencyAttribute()
    {
        $currency = 'rub';
        try {
            $currency = $this->tariff_info['simple-pay-result']['sp_init_currency'];
        } catch (\Exception $e) {
            try {
                $currency = strtolower($this->additional_info['amount']['currency']);
            } catch (\Exception $e) {}
        }

        return transDef($currency, "dictionary.currency.$currency");
    }
}
