<?php

namespace Modules\Payments\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;

/**
 * Class PaymentsTimeLog
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $student_id
 * @property bool $group_dialog
 * @property \Carbon\Carbon $time_start
 * @property \Carbon\Carbon $time_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 *
 * @package App\Models
 */
class PaymentsTimeLog extends Eloquent
{
	protected $casts = [
		'teacher_id'   => 'int',
		'student_id'   => 'int',
        'group_dialog' => 'bool',
	];

	protected $dates = [
		'time_start',
		'time_end',
	];

	protected $fillable = [
		'teacher_id',
		'student_id',
        'group_dialog',
		'time_start',
		'time_end',
	];

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id')->whereHas('user_add_values', function($query){
            $query->whereHas('user_addon', function($query){
                $query->where('alias', 'status');
            })->where(function($query){
                $query->where('value', 'like', '%t%');
            });
        });
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id')->whereHas('user_add_values', function($query){
            $query->whereHas('user_addon', function($query){
                $query->where('alias', 'status');
            })->where(function($query){
                $query->where('value', 'like', '%s%');
            });
        });
    }
}
