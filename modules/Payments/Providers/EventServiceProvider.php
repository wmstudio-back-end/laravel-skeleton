<?php

namespace Modules\Payments\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Payments\Events\PaymentSuccess;
use Modules\Payments\Listeners\ProcessResult;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        PaymentSuccess::class => [
            ProcessResult::class,
        ],
    ];
}