<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;

class AddPaymentsHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->dateTime('date');
            $table->json('payment_info');
            $table->float('cash');
            $table->boolean('paid');
            $table->timestamps();
        });

        Schema::table('payments_history', function(Blueprint $table) {
            $table->foreign('user_id', 'payments_history_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        $settingGroup = SettingsGroup::where('alias', 'tariffs')->first();
        if ($settingGroup) {
            Setting::create([
                'group_id' => $settingGroup->id,
                'header_name' => 'Минимальная сумма вывода',
                'name' => 'min_amount',
                'value' => 100,
                'html_control_type' => 'input',
                'weight' => Setting::where('group_id', $settingGroup->id)->max('weight') + 1,
            ]);
        }
        $settingGroups = SettingsGroup::whereIn('id', [1, 2])->get()->keyBy('id');
        $settingGroups[1]->alias = 'settings';
        $settingGroups[1]->save();
        foreach ($settingGroups[2]->settings as $setting) {
            $setting->delete();
        }
        $settingGroups[2]->delete();
        $settings = Setting::whereIn('name', ['lockscreen_enabled', 'lockscreen_timeout'])->get();
        foreach ($settings as $setting) {
            $setting->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $setting = Setting::where('name', 'min_amount')->first();
        if ($setting) {
            $setting->delete();
        }
        Schema::table('payments_history', function(Blueprint $table) {
            $table->dropForeign('payments_history_ibfk_1');
        });
        Schema::dropIfExists('payments_history');
    }
}
