<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUidToPaymentsInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments_info', function (Blueprint $table) {
            $table->string('uuid', 191)->nullable()->unique()->after('id');
            $table->text('additional_info')->after('tariff_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments_info', function (Blueprint $table) {
            $table->dropColumn('uuid');
            $table->dropColumn('additional_info');
        });
    }
}
