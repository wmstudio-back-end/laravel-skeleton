<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;
use Modules\Sensors\Entities\Sensor;
use Modules\Sensors\Entities\SensorSignal;
use Modules\Sensors\Entities\SensorCall;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up()
    {
        Schema::create('payments_info', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->dateTime('from');
            $table->dateTime('to');
            $table->json('tariff_info');
            $table->string('period');
            $table->boolean('paid');
            $table->timestamps();
        });
        Schema::table('payments_info', function(Blueprint $table) {
            $table->foreign('user_id', 'payments_info_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::create('payments_time', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('payment_id');
            $table->dateTime('from');
            $table->dateTime('to');
            $table->integer('available_time');
            $table->timestamps();
        });
        Schema::table('payments_time', function(Blueprint $table) {
            $table->foreign('payment_id', 'payments_time_ibfk_1')->references('id')->on('payments_info')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        Schema::create('payments_time_log', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('teacher_id');
            $table->integer('student_id');
            $table->dateTime('time_start');
            $table->dateTime('time_end');
            $table->timestamps();
        });
        Schema::table('payments_time_log', function(Blueprint $table) {
            $table->foreign('teacher_id', 'payments_time_log_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('student_id', 'payments_time_log_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });

        UserAddon::insert([
            [
                'alias' => 'tc_balance',
                'description' => 'Балланс преподователя',
                'group' => 'teacher',
                'weight' => UserAddon::where('group', 'teacher')->max('weight') + 1,
                'control_type' => 'hidden',
            ],
            [
                'alias' => 'st_free_time',
                'description' => 'Бесплатные минуты',
                'group' => 'student',
                'weight' => UserAddon::where('group', 'student')->max('weight') + 1,
                'control_type' => 'input',
            ],
        ]);

        $sensor = Sensor::where('alias', 'download')->first();
        if ($sensor) {
            $signals = SensorSignal::where('sensor_id', $sensor->id)->get();
            $calls = SensorCall::where('sensor_id', $sensor->id)->get();
            foreach ($calls as $call) {
                $call->delete();
            }
            foreach ($signals as $signal) {
                $signal->delete();
            }
            $sensor->delete();

            $sensors = Sensor::all();
            $weight = 0;
            foreach ($sensors as $sensor) {
                $sensor->weight = ++$weight;
                $sensor->save();
            }
        }

        Schema::table('sensor_calls', function(Blueprint $table) {
            $table->integer('user_id')->nullable()->after('sensor_id');
        });
        Schema::table('sensor_calls', function(Blueprint $table) {
            $table->foreign('user_id', 'sensor_calls_ibfk_3')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        Schema::table('sensor_calls', function(Blueprint $table) {
            $table->dropForeign('sensor_calls_ibfk_3');
            $table->dropColumn('user_id');
        });

        $addons = UserAddon::whereIn('alias', ['tc_balance', 'st_free_time'])->get()->keyBy('id');
        if ($addons) {
            $values = UserAddValue::where('addon_id', $addons->keys())->get();
            foreach ($values as $value) {
                $value->delete();
            }
            foreach ($addons as $addon) {
                $addon->delete();
            }
        }

        Schema::table('payments_time_log', function(Blueprint $table) {
            $table->dropForeign('payments_time_log_ibfk_1');
            $table->dropForeign('payments_time_log_ibfk_2');
        });
        Schema::dropIfExists('payments_time_log');

        Schema::table('payments_time', function(Blueprint $table) {
            $table->dropForeign('payments_time_ibfk_1');
        });
        Schema::dropIfExists('payments_time');

        Schema::table('payments_info', function(Blueprint $table) {
            $table->dropForeign('payments_info_ibfk_1');
        });
        Schema::dropIfExists('payments_info');
    }
}
