<?php

namespace Modules\Payments\YandexCassa;

use YandexCheckout\Client;
use YandexCheckout\Model\Receipt;
use YandexCheckout\Model\ReceiptItem;
use YandexCheckout\Model\MonetaryAmount;
use Modules\Payments\Entities\PaymentsInfo;
use Modules\Payments\Events\PaymentSuccess;
use YandexCheckout\Model\PaymentMethodType;
use YandexCheckout\Model\PaymentData\PaymentDataFactory;
use YandexCheckout\Request\Payments\CreatePaymentRequest;
use YandexCheckout\Model\ConfirmationAttributes\ConfirmationAttributesRedirect;

class Payment
{
    const CALLBACK = 'payment/result';
    private $client;

    public function __construct()
    {
        $this->client = new Client();
        $key = env('YAC_SHOP_ID');
        $secret = env('YAC_SEKRET_KEY');
        if (empty($key) && empty($secret)) {
            throw new \Exception('Yandex.Kassa settings are not specified.');
        }
        $this->client->setAuth($key, $secret);
    }

    public function createPayment(PaymentsInfo $order, $method = PaymentMethodType::BANK_CARD)
    {
        /** @var \App\User $user */
        $user = \Auth::user();
        if (!$user) {
            throw new \Exception('User not found.');
        }

        $email = $user->email;
        $phone = ($phone = $user->addon('phone')) ? substr(phoneFormat($phone, true), 1) : null;

        $additional_info = $order->additional_info;
        $additional_info['description'] = "Подписка пользователя id=$user->id ({$user->getFullName()})"
            . ' на тарифф ' . transDef(
                $order->tariff_info['period'],
                "dictionary.{$order->tariff_info['period']}",
                [],
                config('app.locale')
            )
            . "({$order->tariff_info['minutes-per-day']} " . numEnding(
                $order->tariff_info['minutes-per-day'], [
                    'минута',
                    'минуты',
                    'минут',
                ]
            ) . " в день/{$order->tariff_info['lessons-per-week']} " . numEnding(
                $order->tariff_info['lessons-per-week'], [
                    'занятие',
                    'занятия',
                    'занятий',
                ]
            ) . " в неделю)";

        $price = $order->tariff_info['tariff']['summary']['price'];

        $amount = new MonetaryAmount();
        $amount->setValue($price);
        $amount->setCurrency('RUB');

        $paymentData = new PaymentDataFactory();
        $paymentData = $paymentData->factory($method);
        $paymentData->offsetSet('email', $email);
        $paymentData->offsetSet('phone', $phone);

        $callbackUrl = url(env('YAC_RESULT_URL') ?: self::CALLBACK) . "?payment-id={$order->id}";
        $confirmation = new ConfirmationAttributesRedirect();
        $confirmation->setReturnUrl($callbackUrl);

        $receiptItem = new ReceiptItem();
        $receiptItem->setDescription($additional_info['description']);
        $receiptItem->setQuantity(1);
        $receiptItem->setPrice($amount);
        $receiptItem->setVatCode(1);

        $receipt = new Receipt();
        $receipt->setEmail($user->email);
        $receipt->setItems([$receiptItem]);

        $payment = new CreatePaymentRequest();
        $payment->setAmount($amount);
        $payment->setDescription("Заказ №{$order->id}");
        $payment->setCapture(true);
        //$payment->setPaymentToken();
        $payment->setPaymentMethodData($paymentData);
        $payment->setConfirmation($confirmation);
        $payment->setReceipt($receipt);

        $paymentResponse = $this->client->createPayment($payment, $order->uuid);

        $confirmationUrl = optional($paymentResponse->getConfirmation())->_confirmationUrl;
        if (empty($confirmationUrl)) {
            throw new \Exception('Ошибка оплаты, не получена ссылка на платежную форму');
        }

        $order->uuid = $paymentResponse->getId();
        \Session::put('yac_uuid', $order->uuid);
        $additional_info['status'] = $paymentResponse->getStatus();
        $additional_info['method'] = $paymentResponse->getPaymentMethod()->getType();
        $order->additional_info = $additional_info;
        $order->save();

        return $confirmationUrl;
    }

    public function notification()
    {
        $data = request()->all();
        if (!isset($data['object'])) {
            throw new \Exception('Invalid request data.');
        }
        $data = $data['object'];
        $order = PaymentsInfo::where('uuid', optional($data)['id'])->first();
        if (!$order) {
            throw new \Exception('Order not found.');
        }

        if ($order) {
            /* @var $order PaymentsInfo */
            $additional_info = $order->additional_info;
            $additional_info['status'] = optional($data)['status'];
            $additional_info['paid'] = optional($data)['paid'];
            $additional_info =
                $additional_info
                + ['amount' => optional($data)['amount']]
                + ['authorization_details' => optional($data)['authorization_details']]
                + ['payment_method' => optional($data)['payment_method']]
                + ['recipient' => optional($data)['recipient']]
                + ['refunded_amount' => optional($data)['refunded_amount']];
            $order->additional_info = $additional_info;
            if ($order->additional_info['paid']) {
                $order->paid = true;
            }
            $order->save();

            event(new PaymentSuccess($order));
        }
    }
}