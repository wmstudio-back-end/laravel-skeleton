<?php

namespace Modules\Payments\SimplePay;

use Modules\Payments\Entities\PaymentsInfo;
use Modules\Payments\Events\PaymentSuccess;

/**
 * Ваш интерфейс для работы с SimplePay.
 * Здесь прописаны все основные настройки, а также определяются обработчики
 * оповещений, получаемых от SimplePay по факту осуществления платежа, либо при
 * отказе в его проведении.
 */
class Merchant extends AbstractMerchant {

    protected
        /**
         * Идентификатор торговой точки в SimplePay
         * Этот параметр можно узнать по адресу: https://secure.simplepay.pro/merchant/#tab_outlets
         */
        $outlet_id,
        /**
         * Секретный ключ торговой точки,
         * Этот параметр можно изменить по адресу: https://secure.simplepay.pro/merchant/#tab_outlets
         */
        $secret_key,
        /**
         * Секретный ключ точки для Result, используется для подписи Result-уведомлений
         * Этот параметр можно изменить по адресу: https://secure.simplepay.pro/merchant/#tab_outlets
         */
        $secret_key_result,
        /**
         * Адрес Result URL на Вашем сайте.
         */
        $result_url,
        /*
         * Алгоритм хеширования подписей. Безопаснее использовать SHA256.
         * Этот параметр можно изменить по адресу: https://secure.simplepay.pro/merchant/#tab_outlets
         */
        $hash_algo = "MD5";

    /*
     * Этот параметр отвечает за опцию SSL Verifypeer. Отключать рекомендуется
     * только в случае, если у Вас по какой-то причине на сервере нет корневых
     * сертификатов.
     */
    protected
        $strong_ssl = true;

    /**
     * Merchant constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->outlet_id = env('SIMPLE_PAY_OUTLET_ID');
        $this->secret_key = env('SIMPLE_PAY_SECRET_KEY');
        $this->secret_key_result = env('SIMPLE_PAY_SECRET_KEY_RESULT');
        $this->result_url = env('SIMPLE_PAY_RESULT_URL');
        if (!$this->result_url) {
            $this->result_url = route('payment.result');
        }

        if ($this->outlet_id == null
            || $this->secret_key == null
            || $this->secret_key_result == null
            || $this->hash_algo == null
        ) {
            throw new \Exception('Error in Modules\Payments\SimplePay\Merchant constructor. Check env `SIMPLE_PAY` constants.');
        }
    }

    /**
     * Ваш обработчик успешного платежа. Здесь Вы можете изменить статус заказа
     * в своей системе, активировать услугу.
     * @param int $order_id В этот параметр будет передан номер заказа в системе магазина
     * @param array $request_params В этот параметр будет полный набор полученных
     * в уведомлении параметров.
     * @throws \Exception
     */
    function process_success($order_id, $request_params) {
        if (isset($request_params['sp_result']) && $request_params['sp_result']) {
            $payment = PaymentsInfo::find($order_id);
            if ($payment) {
                $payment->paid = true;
                $tariffInfo = $payment->tariff_info;
                $tariffInfo['simple-pay-result'] = $request_params;
                $payment->tariff_info = $tariffInfo;
                $payment->save();

                event(new PaymentSuccess($payment));
            } else {
                throw new \Exception('Order not found');
            }
        }
    }

    /**
     * Ваш обработчик в случае отказа в проведении платежа, либо при его отмене.
     * @param int $order_id В этот параметр будет передан номер заказа в системе магазина
     * @param array $request_params В этот параметр будет полный набор полученных
     * в уведомлении параметров.
     * @throws \Exception
     */
    function process_fail($order_id, $request_params) {
        if (!isset($request_params['sp_result']) || (isset($request_params['sp_result']) && !$request_params['sp_result'])) {
            $payment = PaymentsInfo::find($order_id);
            if ($payment) {
                $payment->delete();
            }
        }
    }

}
