<?php

namespace Modules\Pages\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthIfGet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->isMethod('GET') && !empty($request->all())) {
            return app('App\Http\Middleware\SetLocale')->handle($request, $next);
        }
        return $next($request);
    }
}
