<?php

namespace Modules\Pages\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class AppendMiddleware
{
    public function checkPermissions(Route $route)
    {
        if (($alias = $route->parameter('alias')) && ($entity = $route->getAction('entity'))) {
            $entity = $entity::where('alias', $alias)->first();
            if ($entity && $entity->middleware) {
                $actions = $route->action;
                switch ($entity->middleware) {
                    case 'auth': case 'auth.type':
                        $middleware = $entity->middleware;
                        break;
                    case 'auth.type.s':
                        $middleware = 'auth.type';
                        $actions['status'] = 's';
                        $actions['need-active-status'] = true;
                        break;
                    case 'auth.type.t':
                        $middleware = 'auth.type';
                        $actions['status'] = 't';
                        $actions['need-active-status'] = true;
                        break;
                }
                $route->setAction($actions);
                $class = app(app('Illuminate\Contracts\Http\Kernel')->getRouteMiddleware($middleware));
                if (in_array('checkPermissions', get_class_methods($class))) {
                    return $class->checkPermissions($route);
                }
            }
        }

        return true;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (($alias = $request->route('alias')) && ($entity = $request->route()->getAction('entity'))) {
            $entity = $entity::where('alias', $alias)->first();
            if ($entity && $entity->middleware) {
                $actions = $request->route()->action;
                unset ($actions['middleware'][array_search(self::class, $actions['middleware'])]);
                switch ($entity->middleware) {
                    case 'auth':
                        $actions['middleware'][] = 'auth';
                        break;
                    case 'auth.type':
                        $actions['middleware'][] = 'auth.type';
                        break;
                    case 'auth.type.s':
                        $actions['middleware'][] = 'auth.type';
                        $actions['status'] = 's';
                        $actions['need-active-status'] = true;
                        break;
                    case 'auth.type.t':
                        $actions['middleware'][] = 'auth.type';
                        $actions['status'] = 't';
                        $actions['need-active-status'] = true;
                        break;
                }
                $request->route()->setAction($actions);
            }
        }

        if (getPermissions($request->route())) {
            return $next($request);
        }

        abort(404);
    }
}
