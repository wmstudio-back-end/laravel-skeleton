<?php

Route::group(['middleware' => ['web', 'lang', \Modules\Pages\Middleware\AppendMiddleware::class],
    'prefix' => 'page', 'namespace' => 'Modules\Pages\Controllers', 'entity' => \Modules\Pages\Entities\Page::class], function()
{
    Route::get('/{alias}', ['as' => 'static-page', 'uses' => 'IndexController@index']);
});
