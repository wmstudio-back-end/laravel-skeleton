<?php

$name = 'Страницы';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.pages',
                    'label' => 'Просмотр списка статических страниц',
                    'uses' => 'AdminController@index',
                    'middleware' => Modules\Pages\Middleware\AuthIfGet::class,
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/edit/{page}',
                'parameters' => [
                    'as' => 'admin.pages.edit',
                    'label' => 'Создание и редактирование страниц',
                    'uses' => 'AdminController@edit',
                ],
                'where' => ['page' => 'new|[0-9]+'],
            ],
            [
                'method' => 'post',
                'uri' => '/delete/{page}',
                'parameters' => [
                    'as' => 'admin.pages.delete',
                    'label' => 'Удаление страниц',
                    'uses' => 'AdminController@delete',
                ],
                'where' => ['page' => '[0-9]+'],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/seo/{name?}',
                'parameters' => [
                    'as' => 'admin.pages.seo',
                    'label' => 'Настройка SEO внутренних страниц сайта',
                    'uses' => 'AdminController@seo',
                    'middleware' => Modules\Pages\Middleware\AuthIfGet::class,
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.pages',
                'icon'      => 'fa fa-television',
                'order'     => 30,
            ],
        ],
    ],
];
