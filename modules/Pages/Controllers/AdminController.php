<?php

namespace Modules\Pages\Controllers;

use Barryvdh\TranslationManager\Models\Translation;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;
use Modules\Pages\Entities\Page;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public static function writeRequired()
    {
        return [
            'edit',
            'delete',
            'seo',
        ];
    }

    private $middlewares = [
        'auth'        => 'Пользователь должен быть авторизован',
        'auth.type'   => 'Пользователь должен быть авторизован, и один из профилей должен быть заполнен.',
        'auth.type.s' => 'Пользователь должен быть авторизован как студент.',
        'auth.type.t' => 'Пользователь должен быть авторизован как преподаватель.',
    ];

    private $excludePrefixes = [
        'api',
        'admin',
        'static-page',
    ];

    private $excludeMiddleware = [
        'auth',
        'auth.type',
        'perm',
    ];

    private $seoRoutes = [];

    public function index()
    {
        if ($id = request('preview', null)) {
            return $this->preview($id);
        }

        $pagesPerPage = 40;

        $pages = Page::paginate($pagesPerPage);

        return view('pages::admin.index', [
            'pages' => $pages,
        ]);
    }

    private function preview(int $page)
    {
        $page = Page::find($page);
        if ($page) {
            return view('pages::admin.preview', [
                'page' => $page,
            ]);
        } else {
            abort(404);
        }
    }

    public function edit($page)
    {
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.pages')->setActive();

        if ($page == 'new') {
            $menu->lastLevel = 'Создать';
            $page = new Page();
        } else {
            $menu->lastLevel = 'Редактировать';
            $page = Page::find($page);
        }

        if ($page) {
            $locales = $this->transManager->getLocales();
            $defLang = config('app.locale');
            $keys = Translation::where('group', Page::TRANS_GROUP)->whereIn('key', [
                $page->alias . '.name',
                $page->alias . '.description',
                $page->alias . '.content',
            ])->get();
            $translations = [];
            foreach($keys as $translation){
                /* @var $translation Translation */
                $tmp = explode('.', $translation->key);
                $translations[$translation->locale][end($tmp)] = $translation;
            }
            foreach ($locales as $lang) {
                if (!isset($translations[$lang])) {
                    $translations[$lang]['name'] = new Translation();
                    $translations[$lang]['description'] = new Translation();
                    $translations[$lang]['content'] = new Translation();
                } else {
                    if (!isset($translations[$lang]['name'])) {
                        $translations[$lang]['name'] = new Translation();
                    }
                    if (!isset($translations[$lang]['description'])) {
                        $translations[$lang]['description'] = new Translation();
                    }
                    if (!isset($translations[$lang]['content'])) {
                        $translations[$lang]['content'] = new Translation();
                    }
                }
            }

            if (request()->isMethod('POST')) {
                $data = getRequest();

                $validator = [
                    'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $page->alias ? '|unique:pages' : ''),
                    'date' => 'required|date',
                    'content-' . $defLang => 'required|string',
                    'name-' . $defLang => 'required|string',
                    'middleware' => 'nullable|in:' . implode(',', array_keys($this->middlewares)),
                ];
                foreach ($locales as $lang) {
                    if (isset($data['description-' . $lang])) {
                        if (!isset($data['description-' . $defLang])) {
                            $validator['description-' . $defLang] = 'required|string';
                            break;
                        }
                    }
                }

                $validator = \Validator::make($data, $validator, [
                    'description-' . $defLang . '.required' => 'Поле обязательно для заполнения, '
                        . 'если заполнено описание для другого языка.',
                    'middleware.in' => 'Выбранное значение ошибочно',
                ]);

                if (!$validator->fails()) {
                    setSeo($page->alias, str_slug($data['alias']), [$data['seo_title'], $data['seo_keywords'], $data['seo_description']]);

                    $page->alias = str_slug($data['alias']);
                    $page->name = $data['name-' . $defLang];
                    if (isset($data['description-' . $defLang])) {
                        $page->description = $data['description-' . $defLang];
                    } else {
                        $page->description = null;
                    }
                    $page->content = $data['content-' . $defLang];
//                    $page->form_name = $data['form'];
                    $page->middleware = $data['middleware'];
                    $page->active = $data['active'];
                    $page->date_active = $data['date'];
                    $page->save();

                    Translation::unguard();
                    foreach ($translations as $lang => $translation) {
                        if (array_key_exists('name-' . $lang, $data) && !empty($data['name-' . $lang])) {
                            $translation['name']->status = Translation::STATUS_CHANGED;
                            $translation['name']->locale = $lang;
                            $translation['name']->group = Page::TRANS_GROUP;
                            $translation['name']->key = $page->alias . '.name';
                            $translation['name']->value = $data['name-' . $lang];
                            $translation['name']->save();
                        } else {
                            $translation['name']->delete();
                        }

                        if (array_key_exists('description-' . $lang, $data) && !empty($data['description-' . $lang])) {
                            $translation['description']->status = Translation::STATUS_CHANGED;
                            $translation['description']->locale = $lang;
                            $translation['description']->group = Page::TRANS_GROUP;
                            $translation['description']->key = $page->alias . '.description';
                            $translation['description']->value = $data['description-' . $lang];
                            $translation['description']->save();
                        } else {
                            $translation['description']->delete();
                        }

                        if (array_key_exists('content-' . $lang, $data) && !empty($data['content-' . $lang])) {
                            $translation['content']->status = Translation::STATUS_CHANGED;
                            $translation['content']->locale = $lang;
                            $translation['content']->group = Page::TRANS_GROUP;
                            $translation['content']->key = $page->alias . '.content';
                            $translation['content']->value = $data['content-' . $lang];
                            $translation['content']->save();
                        } else {
                            $translation['content']->delete();
                        }
                    }
                    Translation::reguard();

                    return redirect()->route('admin.pages');
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }

//            $formsList = getFormsList();
            $seo = getSeo($page->alias);

            return view('pages::admin.edit', [
                'page' => $page,
//                'forms' => $formsList,
                'seo' => $seo,
                'locales' => $locales,
                'defLang' => $defLang,
                'translations' => $translations,
                'middlewares' => $this->middlewares,
            ]);
        } else {
            abort(404);
        }
    }

    public function delete(int $page)
    {
        $page = Page::find($page);
        if ($page) {
            setSeo($page->alias);
            $translations = Translation::where('group', Page::TRANS_GROUP)->whereIn('key', [
                $page->alias . '.name',
                $page->alias . '.content',
            ])->get();
            Translation::unguard();
            foreach ($translations as $translation) {
                $translation->delete();
            }
            Translation::reguard();
            $page->delete();
            return redirect()->route('admin.pages');
        } else {
            abort(404);
        }
    }

    private function getSeoRoutes()
    {
        if (count($this->seoRoutes)) {
            return $this->seoRoutes;
        }

        $seo = [];
        $fields = Field::whereIn('alias', ['seo_title', 'seo_keywords', 'seo_description'])->get()->keyBy('id');
        if ($fields) {
            foreach ($fields as $field) {
                $seo[$field->alias] = null;
            }
        }

        $routes = [];
        $aliases = [];
        foreach (app('router')->getRoutes() as $route) {
            /* @var $route \Illuminate\Routing\Route */
            if (($name = $route->getName()) && in_array('GET', $route->methods())) {
                if (count(array_intersect($this->excludeMiddleware, $route->gatherMiddleware()))) {
                    continue;
                }
                if (array_key_exists('no-index', $route->action) && $route->action['no-index'] === true) {
                    continue;
                }
                $validRoute = true;
                foreach ($this->excludePrefixes as $prefix) {
                    if ($name === $prefix || substr($name, 0, strlen($prefix) + 1) === $prefix . '.') {
                        $validRoute = false;
                        break;
                    }
                }
                if (!$validRoute) {
                    continue;
                }

                $routes[$route->getName()] = [
                    'name' => $route->getName(),
                    'label' => isset($route->action['label']) ? $route->action['label'] : $route->getName(),
                    'action' => $route->getActionName(),
                    'seo' => $seo,
                ];
                $aliases[$route->getActionName()] = $route->getName();
            }
        }

        if ($fields) {
            $arrSeo = FieldsValue::whereIn('field_id', $fields->keys())
                ->whereIn('alias', array_keys($aliases))->get();
            if ($arrSeo) {
                foreach ($arrSeo as $value) {
                    $routes[$aliases[$value->alias]]['seo'][$fields[$value->field_id]->alias] = $value->value;
                }
            }
        }

        return $this->seoRoutes = $routes;
    }

    public function seo($name = null)
    {
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.pages')->setActive();

        $seoRoutes = $this->getSeoRoutes();

        if ($name === null) {
            $menu->lastLevel = 'Настройка SEO внутренних страниц';
            return view('pages::admin.seo', [
                'seoRoutes' => $seoRoutes,
            ]);
        } elseif (isset($seoRoutes[$name])) {
            if (request()->isMethod('POST')) {
                $data = getRequest();
                setSeo($seoRoutes[$name]['action'], $seoRoutes[$name]['action'], $data);

                return redirect()->route('admin.pages.seo');
            } else {
                $menu->lastLevel = 'Настройка SEO внутренних страниц';
                $menu->lastLevel = [
                    [
                        'label' => 'Настройка SEO внутренних страниц',
                        'uri'   => route('admin.pages.seo'),
                    ],
                    [
                        'label' => 'Редактировть SEO',
                    ]
                ];

                return view('pages::admin.seo-edit', [
                    'page' => $seoRoutes[$name],
                ]);
            }
        }

        abort(404);
    }
}
