<?php

namespace Modules\Pages\Controllers;

use Modules\Pages\Entities\Page;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index($alias)
    {
        $page = Page::where('alias', $alias)->first();

        return view('pages::index', [
            'page' => $page,
        ]);
    }
}
