@php
    /* @var $page Modules\Pages\Entities\Page */
    $seo = getSeo($page->alias);
    if (!isset($seo['seo_title'])) {
        $seo['seo_title'] = transDef($page->name, \Modules\Pages\Entities\Page::TRANS_GROUP . '.' . $page->alias . '.name');
    }
@endphp

@include('partials.seo', ['seo' => $seo])

@php $layout = Auth::check() ? 'main' : 'main-no-auth'; @endphp
@extends('layouts.' . $layout)

@section('content')
    <div class="subscription">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! transDef($page->name, \Modules\Pages\Entities\Page::TRANS_GROUP . '.' . $page->alias . '.name') !!}</h6>
        </div>
        @if(!empty($page->description))
            <div class="booking-subtitle">
                @foreach(explode("\n", transDef($page->description, \Modules\Pages\Entities\Page::TRANS_GROUP . '.' . $page->alias . '.description')) as $str)
                    <p>{{ str_replace("\r", '', $str) }}</p>
                @endforeach
            </div>
        @endif
    </div>

    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="questionsSheet">
                {!! transDef($page->content, \Modules\Pages\Entities\Page::TRANS_GROUP . '.' . $page->alias . '.content') !!}
            </div>
        </div>
    </div>
@endsection