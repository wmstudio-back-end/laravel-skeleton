@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                @if($lastLevel = getMenu('admin')->lastLevel)
                    <div class="card-header">
                        <h4>{{ $page['label'] }}</h4>
                    </div>
                @endif
                <div class="card-body">
                    <form method="post" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <label><b>Внимание!!!</b> Если заголовок заполнен, то он будет поставлен <b>вместо</b> заголовка из меню!</label>
                                <label>Заголовок созданный из меню мультиязычный, поэтому <b>не рекомендуется</b> использовать SEO Title</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_title" class="col-md-2 col-form-label">Заголовок</label>
                            <div class="col-md-10">
                                <input
                                    id="seo_title"
                                    name="seo_title"
                                    type="text"
                                    class="form-control{{ $errors->has('seo_title') ? ' is-invalid' : '' }}"
                                    value="{{ old('seo_title') ?: $page['seo']['seo_title'] }}"
                                    placeholder="SEO Title"
                                >
                                @if ($errors->has('seo_title'))
                                    <div class="invalid-feedback">{{ str_replace('seo_title ', '', $errors->first('seo_title')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_keywords" class="col-md-2 col-form-label">Ключевые слова</label>
                            <div class="col-md-10">
                                <input
                                    id="seo_keywords"
                                    name="seo_keywords"
                                    type="text"
                                    class="form-control{{ $errors->has('seo_keywords') ? ' is-invalid' : '' }}"
                                    value="{{ old('seo_keywords') ?: $page['seo']['seo_keywords'] }}"
                                    placeholder="SEO Keywords"
                                >
                                @if ($errors->has('seo_keywords'))
                                    <div class="invalid-feedback">{{ str_replace('seo_keywords ', '', $errors->first('seo_keywords')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_description" class="col-md-2 col-form-label">Описание</label>
                            <div class="col-md-10">
                                <input
                                    id="seo_description"
                                    name="seo_description"
                                    type="text"
                                    class="form-control{{ $errors->has('seo_description') ? ' is-invalid' : '' }}"
                                    value="{{ old('seo_description') ?: $page['seo']['seo_description'] }}"
                                    placeholder="SEO Description"
                                >
                                @if ($errors->has('seo_description'))
                                    <div class="invalid-feedback">{{ str_replace('seo_description ', '', $errors->first('seo_description')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-action btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection