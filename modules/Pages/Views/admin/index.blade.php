<?php
$canEdit = getPermissions('admin.pages.edit');
$canDelete = getPermissions('admin.pages.delete');
$canEditSeo = getPermissions('admin.pages.seo');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список страниц</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.pages.edit', ['page' => 'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="col-lg-12 project-list">
                        <table class="table table-hover">
                            <tbody>
                            @if (count($pages) == 0)
                                <tr>
                                    <td style="text-align: center;">
                                        Ни одной страницы пока не созданно.
                                    </td>
                                </tr>
                            @else
                                @foreach ($pages as $page)
                                    <? /* @var $page \Modules\Pages\Entities\Page */ ?>
                                    <tr>
                                        <td>
                                            <? if ($page->active) {
                                                $status = 'Активна';
                                                $activeClass = 'badge-success';
                                            } else {
                                                $status = 'Не активна';
                                                $activeClass = 'badge-danger';
                                            }
                                            ?>
                                            <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                        </td>
                                        <td class="project-title">
                                            <h4>{{ $page->name }}</h4>
                                            <small>Дата активации {{ date_format($page->date_active, 'd.m.Y  H:i:s') }}</small>
                                        </td>
                                        <td class="project-alias">
                                            <b>Адрес страницы:</b><br>
                                            <small>
                                                <a href="{{ route('static-page', ['alias' => $page->alias]) }}" target="_blank">
                                                    {{ route('static-page', ['alias' => $page->alias]) }}
                                                </a>
                                            </small>
                                        </td>
                                        <td class="project-content-preview">
                                            <small>{{ (mb_strlen($page->content) > 432)
                                                ? mb_substr(strip_tags($page->content), 0, 432) . '.....'
                                                : strip_tags($page->content)
                                            }}</small>
                                        </td>
                                        <td class="project-actions">
                                            @if ($canDelete)
                                                <form action="{{ route('admin.pages.delete', ['page' => $page->id]) }}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-action btn-danger pull-right">
                                                        <i class="fa fa-times"></i>
                                                        Удалить
                                                    </button>
                                                </form>
                                            @endif
                                            @if ($canEdit)
                                                <a href="{{ route('admin.pages.edit', ['page' => $page->id]) }}"
                                                    class="btn btn-action btn-warning pull-right">
                                                    <i class="fa fa-pencil"></i>
                                                    Редактировать
                                                </a>
                                            @endif
                                            <a href="{{ route('admin.pages', ['preview' => $page->id]) }}"
                                                class="btn btn-action btn-info pull-right">
                                                <i class="fa fa-eye"></i>
                                                Предпросмотр
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $pages->links() }}
                    </div>
                    @if ($canEdit)
                        <a href="{{ route('admin.pages.edit', ['page' => 'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@if($canEditSeo)
    @section('sub-action')
        <a href="{{ route('admin.pages.seo') }}" class="btn btn-sm btn-primary pull-right">
            Настройка SEO
        </a>
    @endsection
@endif