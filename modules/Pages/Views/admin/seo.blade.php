@php
$getSeo = function($page, $seo) {
    if (isset($page['seo'][$seo])) {
        return $page['seo'][$seo];
    } else {
        return null;
    }
}
@endphp

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список страниц</h4>
                </div>
                <div class="card-body">
                    <div class="col-lg-12 pages-seo">
                        <table class="table table-hover">
                            <tbody>
                            @foreach ($seoRoutes as $page)
                                <? /* @var $page \Modules\Pages\Entities\Page */ ?>
                                <tr>
                                    <td>
                                        <b>{{ $page['label'] }}</b>
                                    </td>
                                    <td>
                                        <i>{{ app('router')->getRoutes()->getByName($page['name'])->uri() }}</i>
                                    </td>
                                    <td class="seo-statuses">
                                        <div class="badge badge-{{ $getSeo($page, 'seo_title') ? 'success' : 'danger' }}">
                                            SEO Title
                                        </div>
                                        <div class="badge badge-{{ $getSeo($page, 'seo_keywords') ? 'success' : 'danger' }}">
                                            SEO Keywords
                                        </div>
                                        <div class="badge badge-{{ $getSeo($page, 'seo_description') ? 'success' : 'danger' }}">
                                            SEO Description
                                        </div>
                                    </td>
                                    <td class="seo-actions">
                                        <a href="{{ route('admin.pages.seo', ['name' => $page['name']]) }}"
                                                class="btn btn-sm btn-primary pull-right">
                                            <i class="fa fa-pencil"></i>
                                            Редактировать
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection