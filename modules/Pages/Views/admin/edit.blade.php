@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                @if($lastLevel = getMenu('admin')->lastLevel)
                    <div class="card-header">
                        <h4>{{ $lastLevel . ' страницу' }}</h4>
                    </div>
                @endif
                <div class="card-body">
                    <?php /* @var $page \Modules\Pages\Entities\Page */ ?>
                    <form method="post" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <label>SEO</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_title" class="col-md-2 col-form-label">Заголовок</label>
                            <div class="col-md-10">
                                <input id="seo_title" name="seo_title" type="text" class="form-control{{ $errors->has('seo_title') ? ' is-invalid' : '' }}" value="{{ old('seo_title') ?: $seo['seo_title'] }}">
                                @if ($errors->has('seo_title'))
                                    <div class="invalid-feedback">{{ str_replace('seo_title ', '', $errors->first('seo_title')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_keywords" class="col-md-2 col-form-label">Ключевые слова</label>
                            <div class="col-md-10">
                                <input id="seo_keywords" name="seo_keywords" type="text" class="form-control{{ $errors->has('seo_keywords') ? ' is-invalid' : '' }}" value="{{ old('seo_keywords') ?: $seo['seo_keywords'] }}">
                                @if ($errors->has('seo_keywords'))
                                    <div class="invalid-feedback">{{ str_replace('seo_keywords ', '', $errors->first('seo_keywords')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="seo_description" class="col-md-2 col-form-label">Описание</label>
                            <div class="col-md-10">
                                <input id="seo_description" name="seo_description" type="text" class="form-control{{ $errors->has('seo_description') ? ' is-invalid' : '' }}" value="{{ old('seo_description') ?: $seo['seo_description'] }}">
                                @if ($errors->has('seo_description'))
                                    <div class="invalid-feedback">{{ str_replace('seo_description ', '', $errors->first('seo_description')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <label>Параметры <i>(поля, помеченные красной звездочкой оязательные к заполнению)</i></label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="middleware" class="col-md-2 col-form-label">Права доступа</label>
                            <div class="col-md-10">
                                <select id="middleware" name="middleware" class="form-control selectpicker" data-live-search="false">
                                    <option value=""{{ (old('middleware') == null && $page->middleware == null) ? ' selected="selected"' : '' }}>Видна всем</option>
                                    @foreach ($middlewares as $middleware => $description)
                                        <option
                                            value="{{ $middleware }}"
                                            {{
                                                old('middleware')
                                                    ? (old('middleware') == $middleware
                                                        ? ' selected="selected"'
                                                        : '')
                                                    : ($page->middleware == $middleware
                                                        ? ' selected="selected"'
                                                        : '')
                                            }}>
                                            {{ $description }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('middleware'))
                                    <div class="invalid-feedback">{{ str_replace('middleware ', '', $errors->first('middleware')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alias" class="col-md-2 col-form-label required-label">Адрес страницы</label>
                            <div class="col-md-10">
                                <input id="alias" name="alias" type="text" class="form-control{{ $errors->has('alias') ? ' is-invalid' : '' }}" value="{{ old('alias') ?: $page->alias }}" required="required">
                                @if ($errors->has('alias'))
                                    <div class="invalid-feedback">{{ str_replace('alias ', '', $errors->first('alias')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Название страницы</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="namesTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-name-{{ $lang }}-tab" data-toggle="tab" href="#lang-name-{{ $lang }}"
                                               role="tab" aria-controls="lang-name-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="namesTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-name-{{ $lang }}" role="tabpanel" aria-labelledby="lang-name-tab">
                                            <input
                                                name="name-{{ $lang }}"
                                                type="text"
                                                class="form-control{{ $errors->has('name-' . $lang) ? ' is-invalid' : '' }}"
                                                value="{{ old('name-' . $lang) ?: $translations[$lang]['name']->value }}"
                                                @if($lang === $defLang)required="required"@endif
                                            >
                                            @if ($errors->has('name-' . $lang))
                                                <div class="invalid-feedback">
                                                    {{ str_replace("name-$lang ", '', $errors->first('name-' . $lang)) }}
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Описание страницы</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="descriptionsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-description-{{ $lang }}-tab" data-toggle="tab" href="#lang-description-{{ $lang }}"
                                               role="tab" aria-controls="lang-description-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="descriptionsTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-description-{{ $lang }}" role="tabpanel" aria-labelledby="lang-description-tab">
                                            <textarea
                                                    name="description-{{ $lang }}"
                                                    class="form-control{{ $errors->has('description-' . $lang) ? ' is-invalid' : '' }}"
                                                    rows="3"
                                            >{{ old('description-' . $lang) ?: $translations[$lang]['description']->value }}</textarea>
                                            @if ($errors->has('description-' . $lang))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('description-' . $lang) }}
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Контент страницы</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="contentTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-content-{{ $lang }}-tab" data-toggle="tab" href="#lang-content-{{ $lang }}"
                                               role="tab" aria-controls="lang-content-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="contentTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-content-{{ $lang }}" role="tabpanel" aria-labelledby="lang-content-tab">
                                            <textarea
                                                name="content-{{ $lang }}"
                                                class="form-control summernote{{ $errors->has('content-' . $lang) ? ' is-invalid' : '' }}"
                                                @if($lang === $defLang)required="required"@endif
                                                rows="5"
                                            >{{ old('content-' . $lang) ?: $translations[$lang]['content']->value }}</textarea>
                                            @if ($errors->has('content-' . $lang))
                                                <div class="invalid-feedback">
                                                    {{ str_replace("content-$lang ", '', $errors->first('content-' . $lang)) }}
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        {{--<div class="form-group row">
                            <label for="form" class="col-md-2 col-form-label">Форма</label>
                            <div class="col-md-10">
                                <select id="form" name="form" class="form-control selectpicker" data-live-search="true">
                                    <option value=""{{ (old('form') == null && $page->form_name == null) ? ' selected="selected"' : '' }}>Нет</option>
                                    @foreach ($forms as $formAlias => $formName)
                                        <option value="{{ $formAlias }}"{{ ((old('form') == $formAlias) ? ' selected="selected"' : ($page->form_name == $formAlias ? ' selected="selected"' : '')) }}>
                                            {{ $formName }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('form'))
                                    <div class="invalid-feedback">{{ str_replace('form ', '', $errors->first('form')) }}</div>
                                @endif
                            </div>
                        </div>--}}
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-1" value="active">
                                    <input type="checkbox" class="custom-control-input" id="active" name="active" value="1"
                                           @if ($page->active)checked="checked"@endif>
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date" class="col-md-2 col-form-label required-label">Дата</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input id="date" name="date" type="text" class="form-control datetimepicker" value="{{ date_format($page->date_active, 'd.m.Y H:i') }}" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-action btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection