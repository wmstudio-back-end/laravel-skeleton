<?php

namespace Modules\Pages\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Page
 * 
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property string $description
 * @property string $content
 * @property string $form_name
 * @property string $middleware
 * @property bool $active
 * @property \Carbon\Carbon $date_active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Page extends Eloquent
{
    const TRANS_GROUP = 'pages';

	protected $casts = ['active' => 'bool'];

	protected $dates = ['date_active'];

	protected $fillable = ['alias', 'name', 'description', 'content', 'form_name', 'middleware', 'active', 'date_active'];

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('active', $attributes)) {
            $this->active = true;
        }
        if (!array_key_exists('date_active', $attributes)) {
            $this->date_active = new \DateTime();
        }
    }
}
