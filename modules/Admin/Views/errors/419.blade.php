@extends('admin::layouts.errors')

@section('title', '419')

@section('content')
    <h1>419</h1>
    <div class="page-description">{!! ___('messages.csrf', ['href' => url()->current()]) !!}</div>

    <div class="mt-3 error-layout">
        <a href="{{ route('admin.dashboard') }}">🗠 {!! ___('dictionary.home') !!}</a>
        <button type="button" class="btn btn-link" onclick="window.history.back()">⤴ {!! ___('dictionary.back') !!}</button>
    </div>
@endsection