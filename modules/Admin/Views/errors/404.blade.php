@extends('admin::layouts.errors')

@section('title', '404')

@section('content')
    <h1>404</h1>
    <div class="page-description">{!! ___('messages.404') !!}</div>

    <div class="mt-3 error-layout">
        <a href="{{ route('admin.dashboard') }}">🗠 {!! ___('dictionary.home') !!}</a>
        <button type="button" class="btn btn-link" onclick="window.history.back()">⤴ {!! ___('dictionary.back') !!}</button>
    </div>
@endsection