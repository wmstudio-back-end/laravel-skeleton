@if (isset($pages) && !is_null($pages))
    @foreach ($pages as $page)
        <?php /* @var $page \Modules\MCms\Navigation\Page\AbstractPage */ ?>
        @if(!$page->accept() || $page->isStatic()) @continue @endif
        @if(!$page->hasPages())
            <li{!! $page->isActive(true) ? ' class="active"' : '' !!}>
                <a
                    href="{{ $page->getHref() }}"
                    {{ ($page->getTarget() != "") ? 'target="' . $page->getTarget() . '"' : '' }}
                >
                    @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                    <span>{!! $page->getLabel() !!}</span>
                </a>
            </li>
        @else
            <li{!! ($page->isActive(true)) ? ' class="active"' : '' !!}>
                <a
                    href="#"
                    class="has-dropdown"
                    onclick="event.preventDefault()"
                    {!! ($page->getTarget() != "") ? 'target="' . $page->getTarget() . '"' : '' !!}
                >
                    @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                    <span>{!! $page->getLabel() !!}</span>
                </a>
                <ul class="menu-dropdown"{!! ($page->isActive(true)) ? '' : ' style="display: none;"' !!}>
                    @include('admin::partials.submenu', ['pages' => $page->getPages()])
                </ul>
            </li>
        @endif
    @endforeach
@endif