@if (isset($breadcrumbs) && count($breadcrumbs))
    <ol class="breadcrumb">
        @for($i = 0; $i < count($breadcrumbs) - 1; $i++)
            <li class="breadcrumb-item">
                @if($breadcrumbs[$i]['uri'] === null || $breadcrumbs[$i]['uri'] === '#')
                    {{ $breadcrumbs[$i]['label'] }}
                @else
                    <a href="{{ ($breadcrumbs[$i]['uri'] !== null) ? $breadcrumbs[$i]['uri'] : '#' }}">{{ $breadcrumbs[$i]['label'] }}</a>
                @endif
            </li>
        @endfor
        @if (isset($breadcrumbs[$i]))
            <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumbs[$i]['label'] }}</li>
        @endif
    </ol>
@endif