@if (isset($menuMame) && !is_null($menuMame))
    @foreach(getMenu($menuMame) as $page)
        @if (!$page->accept()) @continue @endif
        @if ($page->isStatic())
            <li class="menu-header">
                @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                <span>{!! $page->getLabel() !!}</span>
            </li>
        @elseif (!$page->hasPages())
            <li{!! $page->isActive(true) ? ' class="active"' : '' !!}>
                <a
                    href="{{ $page->getHref() }}"
                    {{ ($page->getTarget() != "") ? ' target=' . $page->getTarget() : '' }}
                >
                    @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                    <span>{!! $page->getLabel() !!}</span>
                </a>
            </li>
        @else
            @if ($page->childAccept())
                <li{!! $page->isActive(true) ? ' class="active"' : '' !!}>
                    <a
                        href="#"
                        class="has-dropdown"
                        onclick="event.preventDefault()"
                        {{ ($page->getTarget() != "") ? ' target="' . $page->getTarget() . '"' : '' }}
                    >
                        @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                        <span>{!! $page->getLabel() !!}</span>
                    </a>
                    <ul class="menu-dropdown"{!! ($page->isActive(true)) ? '' : ' style="display: none;"' !!}>
                        @include('admin::partials.submenu', ['pages' => $page->getPages()])
                    </ul>
                </li>
            @endif
        @endif
    @endforeach
@endif