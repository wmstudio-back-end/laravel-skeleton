@extends(config('admin.defaults.layout'))

@if($metrika !== null)
    @section('content')
        <div class="row">
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-sm-3 animated fadeInLeft">
                    <div class="card-icon bg-primary">
                        <i class="ion ion-person"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Количество посетителей</h4>
                        </div>
                        <div class="card-body">
                            {{ $metrika['total']['users'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-sm-3 animated fadeInLeft">
                    <div class="card-icon bg-warning">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Количество новых посетителей</h4>
                        </div>
                        <div class="card-body">
                            {{ $metrika['total']['newUsers'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-sm-3 animated fadeInRight">
                    <div class="card-icon bg-info">
                        <i class="ion ion-eye"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Глубина просмотров</h4>
                        </div>
                        <div class="card-body">
                            {{ $metrika['total']['pageDepth'] }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12">
                <div class="card card-sm-3 animated fadeInRight">
                    <div class="card-icon bg-success">
                        <i class="ion ion-record"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Пользователей онлайн</h4>
                        </div>
                        <div class="card-body">
                            <small>{{ getCountOnlineUsers() }}</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card animated fadeInUp">
                    <div class="card-header">
                        <h4>Статистика за период с <i>{{ $metrika['period']['start'] }}</i> по <i>{{ $metrika['period']['end'] }}</i>{!!
                        isset($metrika['demo']) && $metrika['demo'] ? " <b>[демонстрационные данные]</b>" : ''
                    !!}</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="metrika-line-chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script type="text/javascript">
            window.metrikaData = {
                users: [{{ $metrika['users'] }}],
                newUsers: [{{ $metrika['newUsers'] }}],
                pageDepth: [{{ $metrika['pageDepth'] }}],
                labels: [{!! $metrika['dates'] !!}],
            }
        </script>
    @stop
@endif
