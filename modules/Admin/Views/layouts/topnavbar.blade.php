<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li>
                <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg">
                    <i class="ion ion-navicon-round"></i>
                </a>
            </li>
            <li>
                <a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none">
                    <i class="ion ion-search"></i>
                </a>
            </li>
        </ul>
        {{--<div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
            <button class="btn" type="submit"><i class="ion ion-search"></i></button>
        </div>--}}
    </form>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown dropdown-list-toggle">
            <?php $notifications = \Modules\Admin\Controllers\IndexController::getNotifications(); ?>
            <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg{{ count($notifications) ? '' : ' hidden' }}">
                <i class="ion ion-ios-bell-outline"></i>
                <span class="badge badge-danger notifications-count">{{ count($notifications) }}</span>
            </a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">Уведомления</div>
                <div class="dropdown-list-content">
                    <input type="hidden" id="ajaxToken" value="{{ csrf_token() }}">
                    @foreach($notifications as $notification)
                        <form method="post" action="/admin" class="ajaxNotifications">
                            <input type="hidden" name="_token" value="{{ $notification['csrf'] }}">
                            <input type="hidden" name="redirect" value="{{ url()->current() }}">
                            <input type="hidden" name="action" value="{{ $notification['action'] }}">
                            <button class="btn btn-link dropdown-item" type="submit">
                                @if (isset($notification['img']))
                                    <img alt="image" src="{{ $notification['img'] }}" class="rounded-circle dropdown-item-img">
                                @elseif (isset($notification['icon']))
                                    <i class="{{ $notification['icon'] }}"></i>
                                @endif
                                <div class="dropdown-item-desc">
                                    {!! $notification['text'] !!}
                                </div>
                            </button>
                        </form>
                    @endforeach
                </div>
            </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
                <i class="ion ion-android-person d-lg-none"></i>
                <div class="d-sm-none d-lg-inline-block">{{ Auth::user()->getFullName() }}</div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('admin.users.show', ['id' => Auth::user()->id ]) }}" class="dropdown-item has-icon">
                    <i class="ion ion-android-person"></i> Профиль
                </a>
                <a href="{{ route('home') }}" class="dropdown-item has-icon" target="_blank">
                    <i class="ion ion-earth"></i> На сайт
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="dropdown-item has-icon">
                        <i class="ion ion-log-out"></i> Выход
                    </button>
                </form>
            </div>
        </li>
    </ul>
</nav>