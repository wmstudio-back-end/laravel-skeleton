<?php $user = Auth::user(); ?>
<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.dashboard') }}">Stisla Lite</a>
        </div>
        <div class="sidebar-user">
            <div class="sidebar-user-picture">
                <img alt="image" src="{{ $user->getAvatar() }}">
            </div>
            <div class="sidebar-user-details">
                <div class="user-name">{{ $user->login }}</div>
                @if($user->permission_group !== 0)
                    <div class="user-role">
                        {{ \Modules\Users\Entities\UserPermissionsGroup::find($user->permission_group)->name }}
                    </div>
                @endif
            </div>
        </div>
        <ul class="sidebar-menu">
            @include('admin::partials.menu', ['menuMame' => 'admin'])
        </ul>
    </aside>
</div>