@php($old = old())
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <link rel="shortcut icon" href="{{ asset('admin-favicon.png') }}" type="image/x-icon">
    <?php
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $title = $menu->render([
            'onlyText' => true,
            'reverse' => true,
            'separator' => '|',
            'firstLevel' => 1
        ]);
        $adminTitle = config('admin.name');
    ?>
    @if ($title)
        <title>{{ $title }} | {{ $adminTitle }}</title>
    @else
        <title>{{ $adminTitle }}</title>
    @endif

    <link rel="stylesheet" href="{!! mix('assets-admin/css/app.css') !!}" />
    @yield('styles')
</head>
<body>

<div id="app">
    <div class="main-wrapper">
        @include('admin::layouts.topnavbar')
        @include('admin::layouts.navigation')

        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    @include('admin::partials.breadcrumbs', ['breadcrumbs' => $menu->render()])
                    @if(trim($__env->yieldContent('sub-action')))
                        <div class="header-action">
                            @yield('sub-action')
                        </div>
                    @endif
                </div>
                @yield('content')
            </section>
        </div>
        @include('admin::layouts.footer')
</div>
</div>

<script src="{!! mix('assets-admin/js/app.js') !!}" type="text/javascript"></script>

@yield('scripts')
@if (isset($old['toastr.msgHeader']) && isset($old['toastr.message']))
    <script type="text/javascript">
        var type = '{{ (isset($old['toastr.error']) && $old['toastr.error']) ? 'error' : 'success' }}',
            header = '{{ $old['toastr.msgHeader'] ?: '' }}',
            message = '{!! $old['toastr.message'] ?: '' !!}';

        if (header !== '' && message !== '') {
            window.toastr[type](message, header, {timeOut: 5000});
        }
    </script>
@endif
@yield('modals')
</body>
</html>