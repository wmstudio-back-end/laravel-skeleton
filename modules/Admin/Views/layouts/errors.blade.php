<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <link rel="shortcut icon" href="{{ asset('admin-favicon.png') }}" type="image/x-icon">
    <title>404 | {{ config('admin.name') }}</title>

    <?php $adminTitle = config('admin.name'); ?>
    @if (trim($__env->yieldContent('title')) && trim($__env->yieldContent('title')) != config('admin.name'))
        <title>@yield('title') | {{ $adminTitle }}</title>
    @else
        <title>{{ $adminTitle }}</title>
    @endif

    <link rel="stylesheet" href="{!! mix('assets-admin/css/app.css') !!}" />
    @yield('styles')
</head>
<body>

<div id="app">
    <section class="section">
        <div class="container mt-5">
            <div class="page-error">
                <div class="page-inner">
                    @yield('content')
                </div>
            </div>
            <div class="simple-footer mt-5">
                Copyright © Stisla 2018
            </div>
        </div>
    </section>
</div>

<script src="{!! mix('assets-admin/js/app.js') !!}" type="text/javascript"></script>

@yield('scripts')
</body>
</html>