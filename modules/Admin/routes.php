<?php

$prefix = config('admin.defaults.prefix') ?: 'admin';
$prefixes = \Cache::get('prefixes');
if (!isset($prefixes[$prefix])) {
    $prefixes[$prefix] = true;
}
$modules = Module::allEnabled();
$lastRoutes = [];

$setRoute = function($route, $module, $prefix = null, $needPermissions = true) {
    $middleware = array_values(array_unique(array_merge(
        ['web'],
        $needPermissions
            ? (is_array($defMiddleware = config('admin.defaults.middleware'))
                ? $defMiddleware
                : ['auth', 'perm'])
            : [],
        isset($route['parameters']['middleware'])
            ? (is_array($route['parameters']['middleware'])
                ? $route['parameters']['middleware']
                : [$route['parameters']['middleware']]
            )
            : []
    )));
    extract($route);
    if (isset($method) && isset($uri) && isset($parameters)) {
        if ($prefix) {
            $parameters['prefix'] = $prefix;
        }
        $parameters['middleware'] = $middleware;
        if (isset($parameters['uses'])) {
            $parameters['uses'] = "Modules\\$module\\Controllers\\" . $parameters['uses'];
        }
        if (!isset($where)) {
            $where = [];
        }
        if ($method === 'match') {
            if (isset($match)) {
                Route::$method($match, $uri, $parameters)->where($where);
                unset($method, $match, $uri, $parameters);
            }
        } else {
            Route::$method($uri, $parameters)->where($where);
            unset($method, $uri, $parameters);
        }
    }
};
foreach ($modules as $module) {
    /* @var $module \Nwidart\Modules\Laravel\Module */
    if (config($module->getLowerName() . '.routes.last')) {
        if (!isset($lastRoutes[$module->getName()])) {
            $lastRoutes[$module->getName()] = [];
        }
        $lastRoutes[$module->getName()] = config($module->getLowerName() . '.routes.last');
    }
    $routes = config($module->getLowerName() . '.routes.admin');
    if (is_array($routes)) {
        $modulePrefix = config($module->getLowerName() . '.routes.adminPrefix') ?: $module->getLowerName();
        if ($prefix === $modulePrefix) {
            $currentPrefix = $prefix;
        } else {
            $currentPrefix = $prefix . '/' . $modulePrefix;
        }

        if (!isset($prefixes[$currentPrefix])) {
            $prefixes[$currentPrefix] = true;
        }

        foreach ($routes as $route) {
            $setRoute($route, $module->getName(), $currentPrefix);
        }
    }
}

\Cache::forever('prefixes', $prefixes);

if (count($lastRoutes)) {
    foreach ($lastRoutes as $module => $routes) {
        if (is_array($routes)) {
            if (isset($routes['method']) && isset($routes['uri']) && isset($routes['parameters'])) {
                $routes = [$routes];
            }
            foreach ($routes as $route) {
                $setRoute($route, $module, null, false);
            }
        }
    }
}