<?php

return [
    'name' => 'Панель управления',
    'defaults' => [
        'middleware' => [
            'auth',
            'perm',
        ],
        'prefix' => 'admin',
        'layout' => 'admin::layouts.master',
        'homeRoute' => 'admin.dashboard',
    ],
    'routes' => [
        'admin' => [
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.dashboard',
                    'label' => 'Главная страница панели управления и уведомления',
                    'uses' => 'IndexController@index',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            'startLevel' => 'admin.dashboard',
            [
                'label' => 'Меню',
                'order' => -1,
                'type' => 'label'
            ],
            [
                'label' => 'Главная',
                'route' => 'admin.dashboard',
                'icon' => 'fa fa-th-large',
                'order' => 1,
            ],
        ],
    ],
];
