<?php

namespace Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Users\Entities\User;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $data = getRequest();
        if ($data) {
            $notifications = self::getNotifications($data);
            if ($request->ajax()) {
                return response()->json($notifications);
            } else {
                if ($notifications instanceof \Illuminate\Http\RedirectResponse) {
                    return $notifications;
                }
            }
        }

        $metrika = \Modules\Settings\Controllers\AdminController::getMetrika();
        if ($metrika) {
            foreach ($metrika['pageDepth'] as $key => $val) {
                $metrika['pageDepth'][$key] = str_replace(',', '.', round($val, 2));
            }
            $metrika['users'] = implode(", ", $metrika['users']);
            $metrika['newUsers'] = implode(", ", $metrika['newUsers']);
            $metrika['pageDepth'] = implode(" ", $metrika['pageDepth']);
            $metrika['pageDepth'] = str_replace(' ', ', ', str_replace(',', '.', $metrika['pageDepth']));
            $metrika['dates'] = "\"" . implode("\", \"", $metrika['dates']) . "\"";
        }
        return view('admin::index', [
            'metrika' => $metrika,
        ]);
    }

    public static function getNotifications($data = null)
    {
        if ($data === null) {
            $data = getRequest();
        }

        $notifications = [];
        if (getPermissions('admin.dashboard')) {
            if (isset($data['action']) && $data['action']) {
                switch ($data['action']) {
                    case 'get':
                        break;
                    case 'la-viewed':
                        \Cache::delete('la_version');
                        break;
                }
            }

            if (isset($data['redirect']) && $data['redirect']) {
                return redirect()->to($data['redirect']);
            }

            $usersWithTeacherRequest = usersWithTeacherRequest()->count('id');
            if ($usersWithTeacherRequest) {
                $usersWithTeacherRequest = [
                    'csrf' => csrf_token(),
                    'action' => 'null',
                    'icon' => 'fa fa-user-plus fa-c-info',
                    'text' => "<b>$usersWithTeacherRequest</b> " . numEnding($usersWithTeacherRequest, [
                            'Пользователь',
                            'Пользователя',
                            'Пользователей',
                        ]) . numEnding($usersWithTeacherRequest, [
                            ' отправил',
                            ' отправили',
                            ' отправили',
                        ]) . " запрос на права преподавать",
                ];
                if (getPermissions('admin.users.request-t-p')) {
                    $usersWithTeacherRequest['text'] .= '<div class="time"><a href="' . route('admin.users.request-t-p') . '">Перейти</a></div>';
                }
                $notifications[] = $usersWithTeacherRequest;
            }

            $laCurrent = explode('@', \PackageVersions\Versions::getVersion('laravel/framework'))[0];
            $laLast = \Cache::get('la_version');
            if ($laLast === null || $laLast['version'] == $laCurrent) {
                if ($laLast) {
                    \Cache::delete('la_version');
                }
            } else {
                $notifications[] = [
                    'csrf' => csrf_token(),
                    'action' => 'la-viewed',
                    'icon' => 'fa fa-info fa-c-info',
                    'text' => "<b>laravel/framework " . $laCurrent . "</b>" .
                        "может быть обновлен до версии <b>" . $laLast['version'] . "</b>" .
                        "<div class=\"time\">" . dateDiff($laLast['date']) . "</div>",
                ];
            }
        }

        return $notifications;
    }
}
