<?php

namespace Modules\Tests\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\TranslationManager\Models\Translation;
use Modules\Tests\Entities\Test;
use Modules\Tests\Entities\TestQuestion;

class AdminController extends Controller
{
    public static function writeRequired()
    {
        return [
            'edit',
            'sort',
            'questionEdit',
            'questionSort',
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::paginate($this->adminEntitiesPerPage);

        return view('tests::admin.index', [
            'tests' => $tests,
        ]);
    }

    /**
     * @param string $action
     * @param int $test
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function edit($action, $test = null)
    {
        if ($action === 'delete' && $test !== null && ($test = Test::find($test))) {
            $test->delete();
            return redirect()->back();
        }

        switch ($action) {
            case 'add':
                $test = new Test();
                $test->weight = Test::all()->max('weight') + 1;
                break;
            case 'edit':
                $test = Test::find($test);
                break;
            default:
                abort(404);
                break;
        }

        if ($test) {
            $locales = $this->transManager->getLocales();
            $defLang = config('app.locale');

            $keys = Translation::where([
                'group' => Test::TRANS_GROUP,
                'key' => $test->alias,
            ])->get();
            $translations = [];
            foreach($keys as $translation){
                /* @var $translation Translation */
                $translations[$translation->locale] = $translation;
            }
            if (count($translations) != count($locales)) {
                foreach ($locales as $lang) {
                    if (!isset($translations[$lang])) {
                        $translations[$lang] = new Translation();
                    }
                }
            }
            if (request()->isMethod('POST')) {
                $data = getRequest();

                $data['alias'] = str_slug($data['alias']);
                $validator = [
                    'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $test->alias ? '|unique:tests' : ''),
                    'all-questions' => 'bool|required',
                    'random' => 'bool|required',
                    'active' => 'bool|required',
                    'test-name-' . $defLang => 'required|string',
                ];
                if (isset($data['all-questions']) && !$data['all-questions']) {
                    $validator['display-count'] = 'integer|min:1' . ($test->questions->count() ? ('|max:' . $test->questions->count()) : '');
                }

                $validator = \Validator::make($data, $validator, [
                    'display-count.min' => 'Количество отображаемых вопросов не может быть меньше :min',
                    'display-count.max' => 'Количество отображаемых вопросов не может быть больше количества вопросов (:max)',
                ]);

                if (!$validator->fails()) {
                    $test->alias = $data['alias'];
                    $test->name = $data['test-name-' . $defLang];
                    $test->all_questions = $data['all-questions'];
                    $test->random = $data['random'];
                    if (isset($data['display-count'])) {
                        $test->display_count = $data['display-count'];
                    }
                    $test->active = $data['active'];
                    $test->save();

                    Translation::unguard();
                    foreach ($translations as $lang => $translation) {
                        if (array_key_exists('test-name-' . $lang, $data)) {
                            $translation->status = Translation::STATUS_CHANGED;
                            $translation->locale = $lang;
                            $translation->group = Test::TRANS_GROUP;
                            $translation->key = $test->alias;
                            $translation->value = $data['test-name-' . $lang];
                            $translation->save();
                        } else {
                            $translation->delete();
                        }
                    }
                    Translation::reguard();

                    return redirect()->route('admin.tests');
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            } else {
                $edit = ($action === 'edit');
                $menu = getMenu('admin');
                $menu->findBy('route', 'admin.tests')->setActive();
                $menu->lastLevel = $edit ? 'Редактировть' : 'Создать';

                return view('tests::admin.edit', [
                    'edit' => $edit,
                    'test' => $test,
                    'locales' => $locales,
                    'defLang' => $defLang,
                    'translations' => $translations,
                ]);
            }
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function sort()
    {
        return $this->sorting(Test::class, 'admin.tests');
    }

    public function questions($test)
    {
        $test = Test::find($test);

        if ($test) {
            $questions = TestQuestion::where('test_id', $test->id)->paginate($this->adminEntitiesPerPage);

            $menu = getMenu('admin');
            $menu->findBy('route', 'admin.tests')->setActive();
            $menu->lastLevel = $test->name;

            return view('tests::admin.questions', [
                'test' => $test,
                'questions' => $questions,
            ]);
        }

        abort(404);
    }

    /**
     * @param int $test
     * @param string $action
     * @param int $question
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function questionEdit($test, $action, $question = null)
    {
        $test = Test::find($test);
        if ($test) {
            /* @var $test */
            if ($action === 'delete' && $question !== null && ($question = TestQuestion::find($question))) {
                $question->delete();
                return redirect()->back();
            }

            switch ($action) {
                case 'add':
                    $question = new TestQuestion();
                    $question->test_id = $test->id;
                    $question->weight = TestQuestion::where('test_id', $test->id)->max('weight') + 1;
                    break;
                case 'edit':
                    $question = TestQuestion::find($question);
                    break;
                default:
                    abort(404);
                    break;
            }

            if ($question) {
                if (request()->isMethod('POST')) {
                    $data = getRequest();

                    $validator = \Validator::make($data, [
                        'question' => 'string|required',
                        'answers' => 'array|required',
                        'correct-answer' => 'integer|required',
                        'active' => 'bool|required',
                    ]);

                    if (!$validator->fails()) {
                        $question->question = $data['question'];
                        $question->answers = [
                            'correct_id' => (int)$data['correct-answer'],
                            'answers' => $data['answers'],
                        ];
                        $question->active = $data['active'];
                        $question->save();

                        return redirect()->route('admin.test.questions', ['test' => $test->id]);
                    } else {
                        return redirect()->back()->withErrors($validator)->withInput();
                    }
                } else {
                    $edit = ($action === 'edit');

                    $menu = getMenu('admin');
                    $menu->findBy('route', 'admin.tests')->setActive();
                    $menu->lastLevel = [
                        [
                            'label' => $test->name,
                            'uri'   => route('admin.test.questions', ['test' => $test->id]),
                        ],
                        [
                            'label' => $edit ? 'Редактировть' : 'Создать'
                        ]
                    ];

                    return view('tests::admin.question-edit', [
                        'edit' => $edit,
                        'test' => $test,
                        'question' => $question,
                    ]);
                }
            }
        }

        abort(404);
    }

    /**
     * @param int $test
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function questionsSort($test)
    {
        return $this->sorting(TestQuestion::class, route('admin.test.questions', ['test' => $test]));
    }
}
