<?php /* @var $question \Modules\Tests\Entities\TestQuestion */ ?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>{{ $edit ? 'Редактирование' : 'Создание' }} вопроса</h4>
                </div>
                <div class="card-body">
                    <form action="{{ $edit
                            ? route('admin.test.question.edit', ['test' => $test->id, 'action' => 'edit', 'question' => $question->id])
                            : route('admin.test.question.edit', ['test' => $test->id, 'action' => 'add'])
                    }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="question" class="col-md-2 col-form-label required-label">Вопрос</label>
                            <div class="col-md-10">
                                <textarea
                                    id="question"
                                    name="question"
                                    type="text"
                                    class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}"
                                    placeholder="Вопрос"
                                    required="required"
                                >{{ old('question') ?: $question->question }}</textarea>
                                @if ($errors->has('question'))
                                    <div class="invalid-feedback">{{ str_replace('question ', '', $errors->first('question')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Ответы</label>
                            <div class="col-md-10 question-answers">
                                <?php
                                    $answers = $question->answers['answers'];
                                    if (!count($answers)) {
                                        $answers = [
                                            ''
                                        ];
                                    }
                                ?>
                                <div class="form-group">
                                    <div class="custom-control custom-radio question-correct-answer">
                                        <input
                                            id="answer0"
                                            type="radio"
                                            tabindex="-1"
                                            value="0"
                                            name="correct-answer"
                                            class="custom-control-input"
                                            @if($question->answers['correct_id'] === 0)checked="checked"@endif
                                        >
                                        <label class="custom-control-label" for="answer0">Верный</label>
                                    </div>
                                    <textarea
                                            name="answers[]"
                                            type="text"
                                            class="form-control mb-2{{ $errors->has('answers') ? ' is-invalid' : '' }}"
                                            placeholder="Ответ №1"
                                            rows="3"
                                            required="required"
                                    >{{ !empty($old[0]) ? $old[0] : array_shift($answers) }}</textarea>
                                </div>
                                <div class="question-extra-answers">
                                    @foreach ($answers as $key => $answer)
                                        <div class="form-group">
                                            <button type="button" class="btn btn-danger btn-action btn-sm question-delete-answer" tabindex="-1">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <div class="custom-control custom-radio question-correct-answer">
                                                <input
                                                        id="answer{{ $key + 1 }}"
                                                        type="radio"
                                                        tabindex="-1"
                                                        value="{{ $key + 1 }}"
                                                        name="correct-answer"
                                                        class="custom-control-input"
                                                        @if($question->answers['correct_id'] === ($key + 1))checked="checked"@endif
                                                >
                                                <label class="custom-control-label" for="answer{{ $key + 1 }}">Верный</label>
                                            </div>
                                            <textarea
                                                name="answers[]"
                                                type="text"
                                                class="form-control mb-2{{ $errors->has('answers') ? ' is-invalid' : '' }}"
                                                placeholder="Ответ №{{ $key + 2 }}"
                                                rows="3"
                                                required="required"
                                            >{{ !empty($old[$key]) ? $old[$key] : $answer }}</textarea>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-12">
                                    <button type="button" class="btn btn-success btn-action btn-sm pull-right question-add-answer">
                                        <i class="fa fa-plus"></i> Добавить
                                    </button>
                                </div>
                                @if ($errors->has('answers'))
                                    <div class="invalid-feedback">{{ str_replace('answers ', '', $errors->first('answers')) }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-active" value="active">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="active"
                                            name="active"
                                            value="1"
                                            {{ old('active', null) !== null
                                                ? (old('active') ? 'checked="checked"' : '')
                                                : $question->active ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection