<?php /* @var $test \Modules\Tests\Entities\Test */ ?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>{{ $edit ? 'Редактирование' : 'Создание' }} теста</h4>
                </div>
                <div class="card-body">
                    <form action="{{ $edit
                            ? route('admin.test.edit', ['action' => 'edit', 'test' => $test->id])
                            : route('admin.test.edit', ['action' => 'add'])
                    }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="alias" class="col-md-2 col-form-label required-label">Указатель</label>
                            <div class="col-md-10">
                                <input
                                        id="alias"
                                        name="alias"
                                        type="text"
                                        class="form-control{{ $errors->has('alias') ? ' is-invalid' : '' }}"
                                        value="{{ old('alias') ?: $test->alias }}"
                                        placeholder="Нужен для переводов (alias)"
                                        required="required"
                                >
                                @if ($errors->has('alias'))
                                    <div class="invalid-feedback">{{ str_replace('alias ', '', $errors->first('alias')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Название</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-name-{{ $lang }}-tab" data-toggle="tab" href="#lang-name-{{ $lang }}"
                                               role="tab" aria-controls="lang-name-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-name-{{ $lang }}" role="tabpanel" aria-labelledby="lang-name-tab">
                                            <input
                                                    name="test-name-{{ $lang }}"
                                                    type="text"
                                                    class="form-control{{ $errors->has('test-name-' . $lang) ? ' is-invalid' : '' }}"
                                                    value="{{ old('test-name-' . $lang) ?:
                                                        $translations[$lang]->value ?: ($defLang == $lang ? $test->name : '')
                                                    }}"
                                                    @if($lang === $defLang)required="required"@endif
                                            >
                                        </div>
                                    @endforeach
                                </div>
                                @if ($errors->has('test-name-' . $defLang))
                                    <div class="invalid-feedback">{{ str_replace("test-name-$defLang ", '', $errors->first('test-name-' . $defLang)) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-random" value="random">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="random"
                                            name="random"
                                            value="1"
                                            {{ old('random', null) !== null
                                                ? (old('random') ? 'checked="checked"' : '')
                                                : $test->random ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="random">Перемешивать вопросы</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-all-questions" value="all-questions">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="all-questions"
                                            name="all-questions"
                                            value="1"
                                            data-test-all-questions
                                            {{ old('all-questions', null) !== null
                                                ? (old('all-questions') ? 'checked="checked"' : '')
                                                : $test->all_questions ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label required-label" for="all-questions">Показывать все вопросы</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="display-count" class="col-md-2 col-form-label">Количество отображаемых вопросов</label>
                            <div class="col-md-10">
                                <input
                                        id="display-count"
                                        name="display-count"
                                        type="number"
                                        min="1"
                                        @if($test->questions->count())max="{{ $test->questions->count() }}"@endif
                                        class="form-control{{ $errors->has('display-count') ? ' is-invalid' : '' }}"
                                        value="{{ old('display-count') ?: $test->display_count }}"
                                        placeholder="Количество отображаемых вопросов"
                                        required="required"
                                        data-test-display-count
                                        @if($test->all_questions)disabled="disabled"@endif
                                >
                                @if ($errors->has('display-count'))
                                    <div class="invalid-feedback">{{ str_replace('display-count ', '', $errors->first('display-count')) }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-active" value="active">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="active"
                                            name="active"
                                            value="1"
                                            {{ old('active', null) !== null
                                                ? (old('active') ? 'checked="checked"' : '')
                                                : $test->active ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection