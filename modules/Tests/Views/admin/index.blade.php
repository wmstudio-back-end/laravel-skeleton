<?php
$canSort      = getPermissions('admin.tests.sort');
$canEdit      = getPermissions('admin.test.edit');
$canShowTests = getPermissions('admin.topics.tags');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список тестов</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.test.edit', ['action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            @if($tests->count())
                                <thead>
                                <tr>
                                    @if($canSort)
                                        <th class="text-center"><i class="ion ion-ios-keypad"></i></th>
                                    @endif
                                    <th>Название</th>
                                    <th>Статус</th>
                                    @if($canEdit || $canSort)
                                        <th class="text-right">Действия</th>
                                    @endif
                                </tr>
                                </thead>
                            @endif
                            <tbody
                                    id="menu-sortable"
                                    class="sortable-with-handle"
                                    @if($canSort)
                                        data-action="{{ route('admin.tests.sort') }}"
                                        data-token="{{ csrf_token() }}"
                                    @endif
                            >
                            @if($tests->count())
                                @foreach($tests as $test)
                                    <?php /* @var $test \Modules\Tests\Entities\Test */ ?>
                                    <tr data-id="{{ $test->id }}">
                                        @if($canSort)
                                            <td class="text-center">
                                                <div class="sort-handler">
                                                    <i class="ion ion-ios-keypad"></i>
                                                </div>
                                            </td>
                                        @endif
                                        <td>
                                            @if($canShowTests)
                                                <a href="{{ route('admin.test.questions', ['test' => $test->id]) }}">{{ $test->name }}</a>
                                            @else
                                                <p>{{ $test->name }}</p>
                                            @endif
                                        </td>
                                        <td>
                                            <? if ($test->active) {
                                                $status = 'Активна';
                                                $activeClass = 'badge-success';
                                            } else {
                                                $status = 'Не активна';
                                                $activeClass = 'badge-danger';
                                            }
                                            ?>
                                            <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                        </td>
                                        @if($canEdit || $canSort)
                                            <td class="text-right">
                                                @if($canSort)
                                                    <form
                                                        action="{{ route('admin.tests.sort') }}"
                                                        method="post"
                                                        class="inline-form after-sort-hide-first"
                                                        @if($test->weight < 2)style="display: none"@endif
                                                    >
                                                        @csrf
                                                        <button type="submit" name="up" value="{{ $test->id }}" class="btn btn-action btn-info">
                                                            <i class="fa fa-arrow-up"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                @if($canEdit)
                                                    <a href="{{ route('admin.test.edit', ['test' => $test->id, 'action' => 'edit']) }}"
                                                       class="btn btn-action btn-primary">Редактировать</a>
                                                    <form
                                                        action="{{ route('admin.test.edit', ['test' => $test->id, 'action' => 'delete']) }}"
                                                        method="post"
                                                        class="inline-form"
                                                    >
                                                        @csrf
                                                        <button type="submit" class="btn btn-action btn-danger">Удалить</button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td class="text-center">Ни одного теста пока не созданно.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $tests->links() }}
                    </div>
                    @if ($canEdit)
                        <a href="{{ route('admin.test.edit', ['action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection