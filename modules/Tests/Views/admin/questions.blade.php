<?php
$canSort = getPermissions('admin.test.questions.sort');
$canEdit = getPermissions('admin.test.question.edit');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список вопросов</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.test.question.edit', ['test' => $test->id, 'action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            @if($questions->count())
                                <thead>
                                <tr>
                                    @if($canSort)
                                        <th class="text-center"><i class="ion ion-ios-keypad"></i></th>
                                    @endif
                                    <th>Вопрос</th>
                                    <th>Ответ</th>
                                    <th>Статус</th>
                                    @if($canEdit || $canSort)
                                        <th class="text-right">Действия</th>
                                    @endif
                                </tr>
                                </thead>
                            @endif
                            <tbody
                                    id="menu-sortable"
                                    class="sortable-with-handle"
                                    @if($canSort)
                                        data-action="{{ route('admin.test.questions.sort', ['test' => $test->id]) }}"
                                        data-token="{{ csrf_token() }}"
                                    @endif
                            >
                            @if($questions->count())
                                @foreach($questions as $question)
                                    <?php /* @var $question \Modules\Tests\Entities\TestQuestion */ ?>
                                    <tr data-id="{{ $question->id }}">
                                        @if($canSort)
                                            <td class="text-center">
                                                <div class="sort-handler">
                                                    <i class="ion ion-ios-keypad"></i>
                                                </div>
                                            </td>
                                        @endif
                                        <td>
                                            <p>{{ mb_strimwidth($question->question, 0, 100, '...') }}</p>
                                        </td>
                                        <td>
                                            <?php
                                                $answer = '---';
                                                if (isset($question->answers['answers'][$question->answers['correct_id']])) {
                                                    $answer = $question->answers['answers'][$question->answers['correct_id']];
                                                }
                                            ?>
                                            <p>{{ mb_strimwidth($answer, 0, 100, '...') }}</p>
                                        </td>
                                        <td>
                                            <? if ($question->active) {
                                                $status = 'Активна';
                                                $activeClass = 'badge-success';
                                            } else {
                                                $status = 'Не активна';
                                                $activeClass = 'badge-danger';
                                            }
                                            ?>
                                            <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                        </td>
                                        @if($canEdit || $canSort)
                                            <td class="text-right">
                                                @if($canSort)
                                                    <form
                                                        action="{{ route('admin.test.questions.sort', ['test' => $test->id]) }}"
                                                        method="post"
                                                        class="inline-form after-sort-hide-first"
                                                        @if($question->weight < 2)style="display: none"@endif
                                                    >
                                                        @csrf
                                                        <button type="submit" name="up" value="{{ $question->id }}" class="btn btn-action btn-info">
                                                            <i class="fa fa-arrow-up"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                @if($canEdit)
                                                    <a href="{{ route('admin.test.question.edit', [
                                                        'test' => $test->id,
                                                        'action' => 'edit',
                                                        'question' => $question->id,
                                                    ]) }}"
                                                       class="btn btn-action btn-primary">Редактировать</a>
                                                    <form
                                                        action="{{ route('admin.test.question.edit', ['test' => $test->id, 'action' => 'delete']) }}"
                                                        method="post"
                                                        class="inline-form"
                                                    >
                                                        @csrf
                                                        <button type="submit" class="btn btn-action btn-danger">Удалить</button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td class="text-center">Ни одного вопроса пока не созданно.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $questions->links() }}
                    </div>
                    @if ($canEdit)
                        <a href="{{ route('admin.test.question.edit', ['test' => $test->id, 'action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection