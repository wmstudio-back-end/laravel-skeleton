<?php

namespace Modules\Tests\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class Test
 * 
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property bool $all_questions
 * @property bool $random
 * @property int $display_count
 * @property bool $active
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $questions
 *
 * @package App\Models
 */
class Test extends Eloquent
{
    const TRANS_GROUP = 'tests';

	protected $casts = [
		'all_questions'   => 'bool',
		'random'          => 'bool',
		'count_questions' => 'int',
		'active'          => 'bool',
		'weight'          => 'int'
	];

	protected $fillable = [
		'alias',
		'name',
		'all_questions',
		'random',
		'display_count',
		'active',
		'weight'
	];

	protected $attributes = [
	    'all_questions' => true,
        'random'        => false,
        'display_count' => 1,
        'active'        => true,
    ];

	public function questions()
	{
		return $this->hasMany(TestQuestion::class, 'test_id');
	}
}
