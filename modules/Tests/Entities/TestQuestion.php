<?php

namespace Modules\Tests\Entities;

use Modules\MCms\Entities\JsonCast;
use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class TestQuestion
 * 
 * @property int $id
 * @property int $test_id
 * @property string $question
 * @property array $answers
 * @property bool $active
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Test $test
 *
 * @package App\Models
 */
class TestQuestion extends Eloquent
{
    use JsonCast;

	protected $casts = [
		'test_id' => 'int',
		'active' => 'bool',
		'weight' => 'int',
        'answers' => 'json',
	];

	protected $fillable = [
		'test_id',
		'question',
		'answers',
		'active',
		'weight',
	];

	protected $attributes = [
	    'active' => true,
    ];

	public function test()
	{
		return $this->belongsTo(Test::class, 'test_id');
	}

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('answers', $attributes)) {
            $this->answers = [
                'correct_id' => 0,
                'answers' => [],
            ];
        }
    }
}
