<?php

$name = 'Тесты';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.tests',
                    'label' => 'Просмотр Тестов',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/{action}/{test?}',
                'parameters' => [
                    'as' => 'admin.test.edit',
                    'label' => 'Создание, редактирование и удаление тестов',
                    'uses' => 'AdminController@edit',
                ],
                'where' => ['action' => 'add|edit|delete'],
            ],
            [
                'method' => 'post',
                'uri' => '/sort',
                'parameters' => [
                    'as' => 'admin.tests.sort',
                    'label' => 'Сортировка тестов',
                    'uses' => 'AdminController@sort',
                ],
            ],

            [
                'method' => 'get',
                'uri' => '/test/{test}/questions',
                'parameters' => [
                    'as' => 'admin.test.questions',
                    'label' => 'Просмотр вопросов теста',
                    'uses' => 'AdminController@questions',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/test/{test}/questions/{action}/{question?}',
                'parameters' => [
                    'as' => 'admin.test.question.edit',
                    'label' => 'Создание, редактирование и удаление вопросов теста',
                    'uses' => 'AdminController@questionEdit',
                ],
                'where' => ['action' => 'add|edit|delete'],
            ],
            [
                'method' => 'post',
                'uri' => '/test/{test}/questions/sort',
                'parameters' => [
                    'as' => 'admin.test.questions.sort',
                    'label' => 'Сортировка вопросов теста',
                    'uses' => 'AdminController@questionsSort',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => $name,
                'route'     => 'admin.tests',
                'icon'  => 'fa fa fa-list-ol',
                'order' => 65,
            ],
        ],
    ],
];
