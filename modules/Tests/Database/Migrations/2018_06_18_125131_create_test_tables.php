<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserPermission;

class CreateTestTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('alias')->unique('alias');
            $table->string('name');
            $table->boolean('all_questions')->default(true);
            $table->boolean('random')->default(false);
            $table->integer('display_count')->default(0);
            $table->boolean('active')->default(true);
            $table->integer('weight');
            $table->timestamps();
        });

        Schema::create('test_questions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('test_id')->index('test_id');
            $table->text('question');
            $table->json('answers');
            $table->boolean('active')->default(true);
            $table->integer('weight');
            $table->timestamps();
        });

        $hasPermissions = UserPermission::where([
            'link_to' => UserPermission::LINK_GROUP,
            'parent_id' => 1,
            'route_name' => 'admin/tests',
        ])->count('id');
        if (!$hasPermissions) {
            UserPermission::create([
                'link_to' => UserPermission::LINK_GROUP,
                'parent_id' => 1,
                'route_name' => 'admin/tests',
                'permission' => UserPermission::P_WRITE,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_questions');
        Schema::dropIfExists('tests');
    }
}
