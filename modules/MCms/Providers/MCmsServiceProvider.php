<?php

namespace Modules\MCms\Providers;

use Illuminate\Support\ServiceProvider;

class MCmsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(__DIR__.'/../Helpers/*.php') as $filename) {
            require_once($filename);
        }
        $this->registerCustomValidators($this->app->validator);
    }

    protected function registerCustomValidators(\Illuminate\Validation\Factory $validator)
    {
        $files = scandir(base_path() . '/modules/MCms/Rules');
        unset($files[0], $files[1]);
        foreach ($files as $key => $file) {
            $file = pathinfo($file)['filename'];
            $validator->extend(strtolower($file), 'Modules\MCms\Rules\\' . $file . '@validate', ('\Modules\MCms\Rules\\' . $file . '::message')());
        }
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/mcms');

        $sourcePath = __DIR__.'/../Views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/mcms';
        }, \Config::get('view.paths')), [$sourcePath]), 'mcms');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
