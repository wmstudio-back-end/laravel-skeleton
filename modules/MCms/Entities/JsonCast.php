<?php

namespace Modules\MCms\Entities;

trait JsonCast
{
    public function toJson($array = [])
    {
        $json = json_encode($array, JSON_UNESCAPED_UNICODE);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw \Illuminate\Database\Eloquent\JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
	}

    public function fromJson($value, $asObject = false)
    {
        if (is_string($value) && strlen($value)) {
            while (!is_array($value)) {
                $value = json_decode($value, !$asObject);
            }
        }

        return $value;
	}
}
