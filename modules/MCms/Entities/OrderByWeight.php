<?php

namespace Modules\MCms\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderByWeight
 */
class OrderByWeight extends Eloquent
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderByWeightScope);
    }
}
