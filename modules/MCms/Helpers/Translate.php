<?php

/**
 * @param string $key
 * @param array $replace
 * @param string|null $locate
 * @return string
 */
function ___($key, $replace = [], $locate = null) {
    $trans = trans($key, $replace, $locate);
    if ($trans == $key) {
        $trans = config(config('translations.translateDefaultPrefix') . '.' . $key);
        if ($trans) {
            foreach ($replace as $key => $val) {
                $trans = str_replace(':' . $key, $val, $trans);
            }
        } else {
            $trans = $key;
        }
    }
    return $trans;
};

/**
 * @param string $default
 * @param string $key
 * @param array $replace
 * @param string|null $locate
 * @return string
 */
function transDef($default, $key, $replace = [], $locate = null) {
    $trans = trans($key, $replace, $locate);
    if ($trans == $key) {
        $trans = $default;
        foreach ($replace as $key => $val) {
            $trans = str_replace(':' . $key, $val, $trans);
        }
    }
    return $trans;
};

function getLocales() {
    $locales = [];
    $settingsLocales = Modules\Settings\Entities\SettingsGroup::where('alias', 'languages')->first()->settings->keyBy('header_name');
    foreach (app('translation-manager')->getLocales() as $locale) {
        $locales[$locale] = isset($settingsLocales[$locale]) ? $settingsLocales[$locale]->value : $locale;
    }

    return $locales;
}