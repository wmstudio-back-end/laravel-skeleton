<?php

use Modules\MCms\Navigation\Navigation;
use Modules\Menu\Entities\Menu as DBMenu;
use Modules\MCms\Exception\InvalidArgumentException;

class Menu
{
    private static $menus = null;

    public static function getMenu($alias = null)
    {
        if (self::$menus === null) {
            self::$menus = \Cache::get('menus', null);
        }
        if (self::$menus === null) {
            // Config Menu
            $modules = Module::allEnabled();
            $menus = [];
            foreach ($modules as $module) {
                $config = config($module->getLowerName());
                $navigation = isset($config['navigation']) ? $config['navigation'] : [];
                foreach ($navigation as $name => $items) {
                    if (isset($items['startLevel'])) {
                        $menus[$name]['startLevel'] = $items['startLevel'];
                        unset($items['startLevel']);
                    }
                    foreach ($items as $key => $item) {
                        $menus[$name][] = $item;
                    }
                }
            }

            // DB Menu
            $menus['dbMenu'] = [];
            foreach (DBMenu::where('active', true)->get() as $menu) {
                /* @var $menu \Modules\Menu\Entities\Menu */
                $element = [
                    'label' => 'menu.' . $menu->alias,
                    'order' => $menu->weight,
                ];
                if ($menu->url === 'null' || $menu->url === '#') {
                    $element['type'] = 'label';
                } else {
                    $element['uri'] = $menu->url;
                }

                if ($menu->icon) {
                    $element['icon'] = $menu->icon;
                }
                if ($menu->class) {
                    $element['class'] = $menu->class;
                }
                $menus['dbMenu'][] = $element;
            }

            foreach ($menus as $key => $menu) {
                $startLevel = null;
                if (isset($menu['startLevel'])) {
                    $startLevel = $menu['startLevel'];
                    unset($menu['startLevel']);
                }
                $menus[$key] = new Navigation($menu);
                if ($startLevel) {
                    $menus[$key]->startLevel = $startLevel;
                }
            }
            self::$menus = $menus;
            \Cache::forever('menus', $menus);
        }

        if ($alias) {
            if (isset(self::$menus[$alias])) {
                return self::$menus[$alias];
            } else {
                throw new InvalidArgumentException("Invalid argument: '$alias' not found in navigation array");
            }
        } else {
            return self::$menus;
        }
    }
}

/**
 * Возвращает массив меню, элемент которого впоследствее надо передать в генератор
 *
 * @param $alias string|null
 *        Если есть имя, то возвращается не все меню, а указанный
 * @return Navigation
 * @throws InvalidArgumentException
 */
function getMenu($alias = null)
{
    return Menu::getMenu($alias);
}