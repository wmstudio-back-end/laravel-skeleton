<?php

use Illuminate\Routing\Route;
use Modules\Users\Entities\UserPermission as Permissions;

/**
 * @param $route Route|string
 * @return int
 */
function getPermissions($route = null)
{
    if ($route === null) {
        $route = app('router')->getCurrentRoute();
    }

    if (is_string($route)) {
        $routeName = $route;
        if (!($route = app('router')->getRoutes()->getByName($route))) {
            if (validUrl($routeName)) {
                $route = app('router')->getRoutes()->match(request()->create($routeName));
                if ($route->getName() === 'files') {
                    throw new \InvalidArgumentException('Route "' . $routeName . '" not found');
                }
            } else {
                throw new \InvalidArgumentException('Route "' . $routeName . '" not found');
            }
        }
    }

    if ($route instanceof Route) {
        $middleware = $route->gatherMiddleware();
        if (is_string($middleware)) {
            $middleware = [$middleware];
        } elseif (!is_array($middleware)) {
            $middleware = [];
        }
        $hasPermissions = true;
        foreach ($middleware as $class) {
            $class = class_exists($class) ? app($class) : app(app('Illuminate\Contracts\Http\Kernel')->getRouteMiddleware($class));
            if (in_array('checkPermissions', get_class_methods($class))) {
                $hasPermissions = $class->checkPermissions($route);
            }
            if (!$hasPermissions) {
                break;
            }
        }

        return $hasPermissions;
    } else {
        throw new \InvalidArgumentException('Route must be instanceof "Illuminate\Routing\Route" or string');
    }
}

function setPermissions($linkTo, $parentId, $data)
{
    if (!in_array($linkTo, [Permissions::LINK_USER, Permissions::LINK_GROUP])) {
        throw new \InvalidArgumentException('$linkTo must be one of Permissions link type (see in Modules\Users\Entities\UserPermission)');
    }

    $permissions = Permissions::where([
        'link_to' => $linkTo,
        'parent_id' => $parentId,
    ])->whereIn('route_name', array_keys($data))->get()->keyBy('route_name');

    $update = [];
    Permissions::unguard();
    foreach ($data as $route => $value) {
        if ($value != 0) {
            if (strpos($route, '.') !== false) {
                switch ($value) {
                    case 1:
                        $value = 2;
                        break;
                    case -1:
                        $value = 0;
                        break;
                    default:
                        $value = 1;
                        break;
                }
                $update[$route] = $value;
            } else {
                if ((($linkTo == Permissions::LINK_GROUP && \Auth::user()->permission_group == $parentId)
                        || $linkTo == Permissions::LINK_USER && \Auth::user()->id == $parentId
                    ) && $route == 'admin/users' && $value < Permissions::P_WRITE) {
                    $errors['permissions'] = 'В целях безопасности, вы не можете запретить себе редактировать права';
                    continue;
                }
                if (!isset($permissions[$route])) {
                    $permissions[$route] = new Permissions();
                    $permissions[$route]->link_to = $linkTo;
                    $permissions[$route]->parent_id = $parentId;
                    $permissions[$route]->route_name = $route;
                }
                $permissions[$route]->permission = $value;
                $permissions[$route]->save();
            }
        } else {
            if ((($linkTo == Permissions::LINK_GROUP && \Auth::user()->permission_group == $parentId)
                    || $linkTo == Permissions::LINK_USER && \Auth::user()->id == $parentId
                ) && $route == 'admin/users' && $value < Permissions::P_WRITE) {
                $errors['permissions'] = 'В целях безопасности, вы не можете запретить себе редактировать права';
                continue;
            }
            if (isset($permissions[$route])) {
                $permissions[$route]->delete();
            }
        }
    }
    Permissions::reguard();

    Permissions::unguard();
    foreach ($update as $route => $value) {
        $routeEl = app('router')->getRoutes()->getByName($route);
        $prefix = $routeEl->getPrefix();
        $permission = Permissions::where([
            'link_to' => $linkTo,
            'parent_id' => $parentId,
            'route_name' => $prefix,
        ])->first();
        $oldValue = $permission ? $permission->permission : Permissions::P_DENY;
        if ($oldValue != $value || (isset($permissions[$route]) && $permissions[$route]->permission != $value)) {
            if ((($linkTo == Permissions::LINK_GROUP && \Auth::user()->permission_group == $parentId)
                    || $linkTo == Permissions::LINK_USER && \Auth::user()->id == $parentId
                ) && isset($routeEl->action['selfEdit']) && !$routeEl->action['selfEdit'] && $value < Permissions::P_WRITE) {
                if (isset($routeEl->action['label'])) {
                    if (!isset($errors['header'])) {
                        $errors['header'] = 'В целях безопасности, вы не можете запретить себе:';
                    }
                    $errors[] = $routeEl->action['label'];
                } else {
                    $errors['permissions'] = 'В целях безопасности, вы не можете запретить себе редактировать права';
                }
                continue;
            }
            if (!isset($permissions[$route])) {
                $permissions[$route] = new Permissions();
                $permissions[$route]->link_to = $linkTo;
                $permissions[$route]->parent_id = $parentId;
                $permissions[$route]->route_name = $route;
            }
            $permissions[$route]->permission = $value;
            $permissions[$route]->save();
        }
    }
    Permissions::reguard();

    $permissions = Permissions::where([
        'link_to' => $linkTo,
        'parent_id' => $parentId,
    ])->whereIn('route_name', array_keys($data))->get()->keyBy('route_name');
    Permissions::unguard();
    foreach ($permissions as $permission) {
        /* @var $permission Permissions */
        if (strpos($permission->route_name, '.') !== false) {
            $prefix = app('router')->getRoutes()->getByName($permission->route_name)->getPrefix();
            $parentPermission = Permissions::where([
                'link_to' => $linkTo,
                'parent_id' => $parentId,
                'route_name' => $prefix,
            ])->first();
            if (($parentPermission && $parentPermission->permission == $permission->permission)
                || !$parentPermission && $permission->permission == Permissions::P_DENY) {
                $permission->delete();
            }
        }
    }
    Permissions::reguard();

    return isset($errors) ? $errors : true;
}

function getAvailableAdminRoutes() {
    $result = [];
    if ($user = \Auth::user()) {
        $linkTo = $user->permission_group === 0 ? Permissions::LINK_USER : Permissions::LINK_GROUP;
        $parentId = $user->permission_group === 0 ? $user->id : $user->permission_group;
        $permissions = Permissions::where([
            'link_to' => $linkTo,
            'parent_id' => $parentId,
        ])->where('permission', '>', 0)
            ->select('route_name')
            ->get();
        $menu = getMenu('admin');
        $sortedPermissions = [];
        $notSortedPermissions = [];
        foreach ($permissions as $permission) {
            $route = $permission->route_name == 'admin' ? 'admin.dashboard' : $permission->route_name;
            if (strpos($route, '/') !== false) {
                $route = str_replace('/', '.', $route);
            }
            if (($tmpRoute = app('router')->getRoutes()->getByName($route)) && !in_array('GET', $tmpRoute->methods())) {
                continue;
            }
            $menuEl = $menu->findBy('route', $route);
            if (!$menuEl) {
                $tmpRoute = explode('.', $route);
                if (count($tmpRoute) > 1) {
                    $tmpRoute = $tmpRoute[0] . '.' . $tmpRoute[1];
                    $menuEl = $menu->findBy('route', $tmpRoute);
                }
            }
            if ($menuEl) {
                while ($menuEl->getParent() instanceof Modules\MCms\Navigation\Page\AbstractPage) {
                    $menuEl = $menuEl->getParent();
                }
                if ($order = $menuEl->getOrder()) {
                    if (!array_key_exists($order, $sortedPermissions)) {
                        $sortedPermissions[$order] = [];
                    }
                    $sortedPermissions[$order][] = $route;
                } else {
                    $notSortedPermissions[] = $route;
                }
            }
        }
        ksort($sortedPermissions);
        foreach ($sortedPermissions as $key => $routes) {
            if (count($routes) == 1) {
                $result[] = array_shift($routes);
            } else {
                asort($routes);
                foreach ($routes as $route) {
                    $result[] = $route;
                }
            }
        }
        $result = array_merge($result, $notSortedPermissions);
    }

    return $result;
}