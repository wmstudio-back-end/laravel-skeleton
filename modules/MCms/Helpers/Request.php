<?php

use Illuminate\Support\Facades\Input;

/**
 * Возвращает массив параметров исключая _token
 *   с отрицательными значениями для input type checkbox
 *   и конвертированными датами из строк
 *   для цикличной обработки результатов
 *   т.е. если используется фиксированный обработчик конкретной формы,
 *   нет нужды пользоваться этим инструментом
 *
 * Производит автозамену в именах параметров
 * `,` => `.`
 * `,,` => `,`
 *
 * Для конвертации даты из строки, поле `name` должно
 *   содержать `date`
 *
 * Прим:
 *   <input type="checkbox" name="setting-date" value="11.11.11">
 *
 * Для использования checkbox=false нужно добавить в форму
 *   <input type="hidden" name="checkbox-{index}" value="{name input type checkbox}">
 *
 * Прим:
 *   <input type="hidden" name="checkbox-1" value="test">
 *   <input type="checkbox" name="test" value="1">
 *
 * Это говорит о том, что параметр "test" обязательный.
 *   И, если его нет, ему присваевается отрицательное значение
 *
 * @return array
 */
function getRequest()
{
    $checkDate = function($val) {
        if (!$val) {
            return $val;
        }

        try {
            if (is_string($val) && strlen($val) >= 6) {
                $date = new \DateTime($val);
                return $date;
            }
            throw new InvalidArgumentException();
        } catch (\Exception $e) {
            return $val;
        }
    };

    $replace = app('request')->except('_token');
    $data = [];
    foreach ($replace as $key => $val) {
        $data[str_replace([',', '..'], ['.', ','], $key)] = $val;
    }
    unset($replace);

    foreach ($data as $key => $val) {
        if (strpos($key, 'checkbox-') !== false) {
            if (!isset($data[$data[$key]])) {
                if (strpos($data[$key], '[]') !== false) {
                    $arrKey = str_replace('[]', '', $data[$key]);
                    if (!isset($data[$arrKey])) {
                        $data[$arrKey] = [];
                        Input::merge([$arrKey => []]);
                    }
                } else {
                    $data[$data[$key]] = 0;
                    Input::merge([$data[$key] => 0]);
                }
            }
            unset($data[$key]);
        } else {
            if (strpos($key, 'date') !== false && !is_array($val)) {
                $data[$key] = $checkDate($val);
            } else {
                $data[$key] = $val;
            }
        }
    }
    Input::flash();
    return $data;
}