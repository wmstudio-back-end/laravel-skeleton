<?php

namespace Modules\MCms\Helpers;

class Curl
{
    private static function parseResponse($string)
    {
        $headers = [];
        $content = '';
        $str = strtok($string, "\n");
        $h = null;
        while ($str !== false) {
            if ($h and trim($str) === '') {
                $h = false;
                continue;
            }
            if ($h !== false and strpos($str, ':') !== false) {
                $h = true;
                list($headername, $headervalue) = explode(':', trim($str), 2);
                $headername = strtolower($headername);
                $headervalue = ltrim($headervalue);
                if (isset($headers[$headername]))
                    $headers[$headername] .= ',' . $headervalue;
                else
                    $headers[$headername] = $headervalue;
            }
            if ($h === false) {
                $content .= $str."\n";
            }
            $str = strtok("\n");
        }
        return [
            $headers,
            trim($content),
        ];
    }

    private static function curl_create($url, $headers = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_HEADER, true);

        return $curl;
    }

    private static function curl_return($curl)
    {
        list($headers, $body) = self::parseResponse(curl_exec($curl));

        $error = curl_error($curl);
        if (empty($error)) {
            $error = null;
        }

        if (isset($headers['content-type']) && strpos($headers['content-type'], 'application/json') !== false) {
            try {
                $body = json_decode($body, true);
            } catch (\Exception $e) {}
        }

        return [
            'headers' => $headers,
            'body'    => $body,
            'error'   => $error,
        ];
    }

    public static function get($url, $headers = [])
    {
        $curl = self::curl_create($url, $headers);
        return self::curl_return($curl);
    }

    public static function post($url, $form, $headers = [])
    {
        $curl = self::curl_create($url, $headers);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($form));

        return self::curl_return($curl);
    }


}