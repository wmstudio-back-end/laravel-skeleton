<?php

use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;

/**
 * Возвращяет массив форм в виде [alias => formName]
 *
 * @return array
 */
function getFormsList()
{
    $fields = Field::where(['alias' => 'form_name'])->get();
    $fieldIDs = [];
    foreach ($fields as $field) {
        /* @var $field Field */
        $fieldIDs[$field->id] = $field->alias;
    }

    $forms = [];
    if (count($fieldIDs) > 0) {
        $arrForms = FieldsValue::whereIn('field_id', array_keys($fieldIDs))->get();
        foreach ($arrForms as $fieldValue) {
            /* @var $fieldValue FieldsValue */
            $forms[$fieldValue->alias] = $fieldValue->value;
        }
    }

    return $forms;
}

/**
 * Возвращяет массив seo в виде [seo_* => seo_value]
 *
 * @param string|null $alias
 * @return array
 */
function getSeo($alias = null)
{
    $seo = [];
    $fields = Field::whereIn('alias', ['seo_title', 'seo_keywords', 'seo_description'])->get()->keyBy('id');
    if ($fields) {
        foreach ($fields as $field) {
            $seo[$field->alias] = null;
        }

        $arrSeo = FieldsValue::whereIn('field_id', $fields->keys())
            ->where('alias', $alias)->get();
        if ($arrSeo) {
            foreach ($arrSeo as $value) {
                $seo[$fields[$value->field_id]->alias] = $value->value;
            }
        }
    }

    return $seo;
}

/**
 * Обновляет SEO информацию в базе данных
 * если ($alias != null && $newAlias == null), то удалит записи по alias
 * Возвращяет true|false
 *
 * @param string $alias
 * @param string $newAlias
 * @param array $options
 * @return bool
 * @throws \Exception
 */
function setSeo($alias = null, $newAlias = null, $options = [])
{
    $params = [
        'seo_title'       => isset($options['seo_title']) ? $options['seo_title']
            : (isset($options['title']) ? $options['title']
                : (isset($options[0]) ? $options[0] : null)),
        'seo_keywords'    => isset($options['seo_keywords']) ? $options['seo_keywords']
            : (isset($options['keywords']) ? $options['keywords']
                : (isset($options[1]) ? $options[1] : null)),
        'seo_description' => isset($options['seo_description']) ? $options['seo_description']
            : (isset($options['description']) ? $options['description']
                : (isset($options[2]) ? $options[2] : null)),
    ];
    $result = false;

    $fields = Field::whereIn('alias', ['seo_title', 'seo_keywords', 'seo_description'])->get();
    $fieldIDs = [];
    foreach ($fields as $field) {
        /* @var $field Field */
        $fieldIDs[$field->id] = $field->alias;
    }

    if (count($fieldIDs) > 0) {
        $arrSeo = FieldsValue::whereIn('field_id', array_keys($fieldIDs))
            ->where('alias', $alias)->get();
        if (count($arrSeo)) {
            if ($newAlias) {
                FieldsValue::unguard();
                foreach ($arrSeo as $fieldValue) {
                    /* @var $fieldValue FieldsValue */
                    if ($params[$fieldIDs[$fieldValue->field_id]]) {
                        if ($alias != $newAlias)
                            $fieldValue->alias = $newAlias;
                        $fieldValue->value = $params[$fieldIDs[$fieldValue->field_id]];
                        $fieldValue->save();
                    } else {
                        $fieldValue->delete();
                    }
                    unset($params[$fieldIDs[$fieldValue->field_id]]);
                }
                if (count($params)) {
                    goto create;
                }
                FieldsValue::reguard();
                $result = true;
            } else {
                FieldsValue::unguard();
                foreach ($arrSeo as $fieldValue) {
                    /* @var $fieldValue FieldsValue */
                    $fieldValue->delete();
                }
                FieldsValue::reguard();
                $result = true;
            }
        } else {
            FieldsValue::unguard();
            create:
            foreach ($fieldIDs as $key => $val) {
                if (isset($params[$fieldIDs[$key]]) && $params[$fieldIDs[$key]]) {
                    $fieldValue = new FieldsValue();
                    $fieldValue->field_id = $key;
                    $fieldValue->alias = $newAlias;
                    $fieldValue->value = $params[$fieldIDs[$key]];
                    $fieldValue->save();
                }
            }
            FieldsValue::reguard();
            $result = true;
        }
    }

    return $result;
}