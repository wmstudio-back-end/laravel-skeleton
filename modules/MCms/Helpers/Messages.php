<?php

use Modules\Users\Entities\User;
use Modules\Messages\Entities\Message;
use Modules\Translations\Constants\Languages;

/**
 * @param int|string|User|null $user
 * @param int|false $count
 * @return array|int
 * @throws \Exception
 */
function findUserDialogs($user = null, $count = false) {
    $result = ($count === false) ? 0 : [];
    if ($user === null) {
        $user = \Auth::user();
    } elseif (is_int($user)) {
        $user = User::find($user);
    } elseif (is_string($user)) {
        $user = User::where(function($query) use($user) {
            $query->where('login', $user)->orWhere('email', $user);
        })->where('active', true)->first();
    } elseif (!($user instanceof User)) {
        $user = null;
    }
    $status = $user->getStatus();
    if ($user && $status) {
        $status = $status === 't' ? 's' : 't';
        $senders = Message::select([
            'sender_id as user_id',
            'created_at',
        ])->where('receiver_id', $user->id)->whereHas('sender', function($query) use ($status) {
            $query->whereHas('user_add_values', function($query) use ($status) {
                $query->whereHas('user_addon', function($query) {
                    $query->where('alias', 'status');
                })->where(function($query) use ($status) {
                    $query->where('value', 'like', "%$status%");
                });
            });
        });
        $users = Message::select([
            'receiver_id as user_id',
            'created_at',
        ])->where('sender_id', $user->id)->whereHas('receiver', function($query) use ($status) {
            $query->whereHas('user_add_values', function($query) use ($status) {
                $query->whereHas('user_addon', function($query) {
                    $query->where('alias', 'status');
                })->where(function($query) use ($status) {
                    $query->where('value', 'like', "%$status%");
                });
            });
        })->union($senders);


        if ($count === false) {
            $result = \DB::table(\DB::raw("({$users->toSql()}) as sub"))
                ->mergeBindings($users->getQuery())
                ->distinct('user_id')
                ->count('user_id');

        } else {
            $sub = \DB::table(\DB::raw("({$users->toSql()}) as sub"))
                ->mergeBindings($users->getQuery())
                ->orderByDesc('created_at');
            $userIds = \DB::table(\DB::raw("({$sub->toSql()}) as res"))
                ->mergeBindings($users->getQuery())
                ->select('user_id')
                ->groupBy('user_id')
                ->paginate($count);
            $result = [
                'current' => $userIds->currentPage(),
                'last' => $userIds->lastPage(),
                'users' => [],
            ];
            $userIds = $userIds->getCollection()->keyBy('user_id');
            $users = User::whereIn('id', $userIds->keys())->get()->keyBy('id');
            foreach ($userIds as $id => $val) {
                if (!isset($users[$id])) {
                    unset($userIds[$id]);
                } else {
                    $userIds[$id] = [
                        'login' => $users[$id]->login,
                        'name' => $users[$id]->getFullName(),
                        'country' => Languages::getCountry($users[$id]->addon('country')),
                        'avatar' => $users[$id]->getAvatar(),
                        'msgPage' => 0,
                        'msgLastPage' => null,
                        'newMessages' => 0,
                    ];
                }
            }
            $result['users'] = $userIds->toArray();
        }
    } elseif (!$user) {
        throw new \Exception('user not found');
    }

    return $result;
}