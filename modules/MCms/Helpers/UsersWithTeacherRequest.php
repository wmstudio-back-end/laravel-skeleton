<?php

use Modules\Users\Entities\User;

function usersWithTeacherRequest() {
    $result = null;
    $user = Auth::user();
    if ($user && $user->hasAdminPermissions()) {
        $result = User::where('active', true)
            ->whereHas('user_add_values', function($query) {
                $query->whereHas('user_addon', function($query) {
                    $query->where('alias', 'status');
                })->where(function($query) {
                    $query->where('value', 'like', "%q%");
                });
            })->where(function($query) {
                $query->whereHas('user_add_values', function($query) {
                    $query->whereHas('user_addon', function($query) {
                        $query->where('alias', 'tc_was_permitted');
                    })->where(function($query) {
                        $query->where('value', false);
                    });
                })->orWhereDoesntHave('user_add_values', function($query) {
                    $query->whereHas('user_addon', function($query) {
                        $query->where('alias', 'tc_was_permitted');
                    });
                });
            });
    }

    return $result;
}