<?php

/**
 * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
 *
 * @param  $number integer
 *         Число на основе которого нужно сформировать окончание
 * @param  $endingArray array[$var1, $var2, $var3]
 *         Массив слов или окончаний для чисел (1, 4, 5), например ['яблоко', 'яблока', 'яблок']
 * @return String
 */
function numEnding($number, $endingArray) {
    $number = $number % 100;
    if ($number>=11 && $number<=19) {
        $ending=$endingArray[2];
    }
    else {
        $i = $number % 10;
        switch ($i)
        {
            case (1): $ending = $endingArray[0]; break;
            case (2):
            case (3):
            case (4): $ending = $endingArray[1]; break;
            default: $ending=$endingArray[2];
        }
    }
    return $ending;
}

/**
 * Возвращает разницу между датами в виде текста
 *   в опциях указывается возвращать часы/минуты/секунды или нет
 *
 * @param DateTime $dateOld
 * @param DateTime|null $date
 * @param array $options
 * @return string
 * @throws Exception
 */
function dateDiff(\DateTime $dateOld, \DateTime $date = null, array $options = []) {
    if ($date === null) {
        $date = new DateTime();
    }

    $opts = [
        'hours' => true,
        'minutes' => false,
        'seconds' => false,
        'case' => true,
    ];

    $endings = [
        'years' => [
            ___('dictionary.endings.year.one'),
            ___('dictionary.endings.year.two'),
            ___('dictionary.endings.year.five'),
        ],
        'months' => [
            ___('dictionary.endings.month.one'),
            ___('dictionary.endings.month.two'),
            ___('dictionary.endings.month.five'),
        ],
        'days' => [
            ___('dictionary.endings.day.one'),
            ___('dictionary.endings.day.two'),
            ___('dictionary.endings.day.five'),
        ],
        'hours' => [
            ___('dictionary.endings.hour.one'),
            ___('dictionary.endings.hour.two'),
            ___('dictionary.endings.hour.five'),
        ],
        'minutes' => [
            true => [
                ___('dictionary.endings.minute.one'),
                ___('dictionary.endings.minute.two'),
                ___('dictionary.endings.minute.five'),
            ],
            false => [
                ___('dictionary.endings.minute.case.one'),
                ___('dictionary.endings.minute.case.two'),
                ___('dictionary.endings.minute.case.five'),
            ],
        ],
        'seconds' => [
            true => [
                ___('dictionary.endings.second.one'),
                ___('dictionary.endings.second.two'),
                ___('dictionary.endings.second.five'),
            ],
            false => [
                ___('dictionary.endings.second.case.one'),
                ___('dictionary.endings.second.case.two'),
                ___('dictionary.endings.second.case.five'),
            ],
        ],
    ];

    if (isset($options['hours']))
        $opts['hours'] = (bool)$options['hours'];
    if (isset($options['minutes']))
        $opts['minutes'] = (bool)$options['minutes'];
    if (isset($options['seconds']))
        $opts['seconds'] = (bool)$options['seconds'];
    if (isset($options['case']))
        $opts['case'] = (bool)$options['case'];


    $diff = date_diff($dateOld, $date);

    $res = ($diff->y ? $diff->y . ' ' . numEnding($diff->y, $endings['years']) : '');
    $res .= ($diff->m ? ($res == '' ? '' : ' ') . $diff->m . ' ' . numEnding($diff->m, $endings['months']) : '');
    $res .= ($diff->d ? ($res == '' ? '' : ' ') . $diff->d . ' ' . numEnding($diff->d, $endings['days']) : '');
    $res .= (($diff->h && $opts['hours']) ? ($res == '' ? '' : ' ') . $diff->h . ' ' . numEnding($diff->h, $endings['hours']) : '');
    $res .= (($diff->i && ($opts['minutes'] || $res == '')) ? ($res == '' ? '' : ' ') . $diff->i . ' ' . numEnding($diff->i, $endings['minutes'][$opts['case']]) : '');
    $res .= (($diff->s && ($opts['seconds'] || $res == '')) ? ($res == '' ? '' : ' ') . $diff->s . ' ' . numEnding($diff->s, $endings['seconds'][$opts['case']]) : '');

    if ($opts['case']) {
        $res .= ' ' . ___('dictionary.time-ago');
    }

    return $res;
}

/**
 * composer requires:
 *   "muglug/package-versions"
 *
 * @param string $package
 * @return bool
 */
function packageExist(string $package) {
    try {
        \PackageVersions\Versions::getVersion($package);
        return true;
    } catch (Exception $e) {
        if ($module = \Module::find($package)) {
            return $module->enabled();
        }
        return false;
    }
}

/**
 * Проверяет является ли URL роутом
 *
 * @param array|string $routes
 * @return bool
 */
function isRoute($routes = null) {
    $result = false;
    if (is_string($routes)) {
        $routes = [$routes];
    }
    if (is_array($routes)) {
        $current = \Route::getCurrentRoute()->getName();
        foreach ($routes as $route) {
            if ($route == $current) {
                $result = true;
                break;
            }
        }
    }

    return $result;
}

/**
 * Функция возвращает номер телефона в формате "+0 (000) 000-00-00"
 *
 * @param string $phone   Число на основе которого нужно сформировать окончание
 * @param bool $clear     Если true вернет номер телефона без пробелов и скобочек
 * @param bool $returnArr Вернуть масив [cc, number]
 *
 * @return String|null
 */
function phoneFormat($phone, $clear = false, $returnArr = false) {
    if ($clear) {
        $result = preg_replace('/[^0-9]/', '', $phone);
        $result = (substr($result, 0, 1) == 8) ? '+7' . substr($result, 1) : '+' . $result;
    } else {
        $clearPhone = preg_replace('/[^0-9]/', '', $phone);
        $ccl = 1;
        if (strlen($clearPhone) > 11) {
            if (strlen($clearPhone) == 12) {
                $ccl = 2;
            } elseif (strlen($clearPhone) == 13) {
                $ccl = 3;
            }
        }
        if (preg_match('/^(\d{' . $ccl . '})(\d{3})(\d{3})(\d{2})(\d{2})$/', preg_replace('/[^0-9]/', '', $phone), $matches)) {
            if ($returnArr) {
                $result = [
                    'cc'     => $matches[1],
                    'number' => $matches[2] . $matches[3] . $matches[4] . $matches[5],
                ];
            } else {
                $result = '+' . ((substr($matches[1], 0, 1) == 8) ? '7' . substr($matches[1], 1) : $matches[1])
                    . ' (' . $matches[2] . ') ' . $matches[3] . '-' . $matches[4] . '-' . $matches[5];
            }
        } else {
            if ($returnArr) {
                $result = [
                    'cc'     => '',
                    'number' => $phone,
                ];
            } else {
                $result = $phone;
            }
        }
    }

    return $result;
}

/**
 * Функция возвращает массив с активными соц.сетями
 * для oauth авторизации/регистрации
 *
 * @return array
 */
function getAvailableSocial() {
    $userConfig = config('users', []) ?:
        file_exists(base_path('modules/Users/Config/config.php'))
            ? require base_path('modules/Users/Config/config.php')
            : [];

    $social = isset($userConfig['social']) ? array_keys($userConfig['social']) : [];

    $first = array_shift($social);
    array_unshift($social, 'facebook');
    array_unshift($social, $first);

    foreach ($social as $key => $value) {
        $config = config("services.$value");
        foreach ($config as $setting) {
            if ($setting == null) {
                unset($social[$key]);
                break;
            }
        }
    }

    return $social;
}

/**
 * Возвращает массив в формате
 * [
 *   'first_name'  => '',
 *   'middle_name' => '',
 *   'last_name'   => '',
 * ]
 * или false
 *
 * @param string $name
 * @return array|false
 */
function splitName($name) {
    $name = trim(preg_replace('/[\ ]+/', ' ', $name));
    $parts = explode(' ', $name);

    if (empty($parts)) {
        return false;
    }

    $name = [];
    $name['first_name'] = $parts[0];
    $name['middle_name'] = (isset($parts[2])) ? $parts[1] : '';
    $name['last_name'] = (isset($parts[2])) ? $parts[2] : ( isset($parts[1]) ? $parts[1] : '');

    return $name;
}

/**
 * Превращает строку в ассоциативный массив и заполняет значение
 * Прим:
 *   'array.key.second'
 * Результат:
 *   [
 *       'array' => [
 *           'key' => [
 *               'second' = $value
 *           ]
 *       ]
 *   ]
 *
 * @param array $arr
 * @param string $path
 * @param $value
 * @param string $separator
 */
function assignArrayByPath(&$arr, $path, $value = null, $separator = '.') {
    $keys = explode($separator, $path);

    foreach ($keys as $key) {
        $arr = &$arr[$key];
    }

    $arr = $value;
};

/**
 * Функция перемешивает массив, сохраняя при этом ключи
 *   если $returnArray = false,
 *   то перемешивает входящий массив и возврашает boolean
 *
 * @param array $array
 * @param bool $returnArray
 * @return array|bool
 * @throws Exception
 */
function shuffleWithKeys(&$array, $returnArray = true) {
    try {
        if ($returnArray) {
            $result = 'tmp';
            $tmp = [];
        } else {
            $result = 'array';
        }
        $keys = array_keys($array);
        srand((float)microtime() * 1000000);
        shuffle($keys);
        foreach ($keys as $key) {
            $value = $array[$key];
            if (isset($$result[$key])) {
                unset($$result[$key]);
            }
            $$result[$key] = $value;
        }
        if ($returnArray) {
            return $$result;
        } else {
            return true;
        }
    } catch (\Exception $e) {
        if ($returnArray) {
            throw $e;
        } else {
            return false;
        }
    }
}

/**
 * Возвращает в байтах из строки.
 * Прим:
 *   1  = 1
 *   1k = 1 024
 *   1m = 1 048 576
 *   1g = 1 073 741 824
 *
 * @param string $str
 * @return int
 */
function fileSizeFromStr($str) {
    $str = preg_replace('/\.+/', '.', str_replace(',', '.', $str));
    $multiplier = 1;
    if (strpos($str, 'k') !== false || strpos($str, 'K') !== false) {
        $multiplier = 1024;
    } elseif (strpos($str, 'm') !== false || strpos($str, 'M') !== false) {
        $multiplier = 1024 * 1024;
    } elseif (strpos($str, 'g') !== false || strpos($str, 'G') !== false) {
        $multiplier = 1024 * 1024 * 1024;
    }
    $str = preg_replace('~[^0-9\.0-9$]+~', '', $str);
    $str = (int)round((strpos($str, '.') !== false ? (float)$str : (int)$str) * $multiplier);

    return $str;
}

/**
 * Проверяет является ли строка url - адресом
 *
 * @param string $url
 * @return bool
 */
function validUrl($url) {
    $regex = "((https?|ftp)\:\/\/)?";                                       // SCHEME
    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?";  // User and Pass
    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})";                                // Host or IP
    $regex .= "(\:[0-9]{2,5})?";                                            // Port
    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?";                                // Path
    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?";                   // GET Query
    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?";                                // Anchor

    return (bool)preg_match("/^$regex$/i", $url);
}

/**
 * @param string $url
 * @return string|false
 */
function getYoutubeID($url) {
    if (strlen($url) > 11 && preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|'
            . '.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
        return $match[1];
    }

    return false;
}

function getCurrencies() {
    $activeCurrencies = json_decode(setting('currencies', '[]'), true);
    foreach ($activeCurrencies as $currency => $active) {
        if (!$active) {
            unset($activeCurrencies[$currency]);
        } else {
            $activeCurrencies[$currency] = \Modules\Settings\Constants\CurrencyList::getCurrency($currency);
        }
    }
    if (empty($activeCurrencies)) {
        $activeCurrencies = [
            'RUB' => \Modules\Settings\Constants\CurrencyList::getCurrency('rub'),
            'USD' => \Modules\Settings\Constants\CurrencyList::getCurrency('usd'),
        ];
    }

    return $activeCurrencies;
}