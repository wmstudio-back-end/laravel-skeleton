<?php

use Illuminate\Http\UploadedFile;
use Modules\Fields\Entities\Field;
use Modules\Fields\Entities\FieldsValue;

/**
 * @param UploadedFile $file
 * @param string $type
 * @param string $overwriteOrNewFileName
 * @return array|bool
 */
function uploadFile(UploadedFile $file, string $type = 'file', string $overwriteOrNewFileName = '')
{
    if ($type == 'file' || $type == 'image') {
        $fileFields = \Cache::get('file-fields', null);
        if ($fileFields === null) {
            $fileFields = Field::whereIn('alias', ['file', 'image'])->get()->keyBy('id');
            \Cache::forever('file-fields', $fileFields);
        }
        $fields = $fileFields;
    } else {
        $fields = Field::where('alias', $type)->get()->keyBy('id');
    }

    if ($fields) {
        try {
            $uploadDir = config('app.upload-dir');
            if ($overwriteOrNewFileName === '') {
                $fName = str_replace('\\', '/', preg_replace('/[\"\'\*\:\<\>\?\|]+/', '', $file->getClientOriginalName()));
                $prefix = 1;
                $arrFiles = [];
                while (FieldsValue::where('alias', $fName)->whereIn('field_id', $fields->keys())->first() != null
                    || in_array($fName, $arrFiles)) {
                    $info = pathinfo($fName);
                    $dir = (string)substr($info['dirname'], 2);
                    $dir = (strlen($dir) > 0) ? $dir . '/' : $dir;
                    $fName = $dir . preg_replace('/_{0,1}\([\d]+\)/', '', $info['filename']) . "_($prefix)." . $info['extension'];
                    $prefix++;
                }
                $arrFiles[] = $fName;
                $path = md5(date_format(new \DateTime(), 'dmYHis') . $fName);
                $file->move($uploadDir, $path);
                $path = $uploadDir . $path;
                $field = new FieldsValue();
                $field->alias = $fName;
                $field->value = $path;
            } else {
                $fName = $overwriteOrNewFileName;
                $field = FieldsValue::where('alias', $fName)->whereIn('field_id', $fields->keys())->first();
                if (!$field) {
                    $path = $uploadDir . md5(date_format(new \DateTime(), 'dmYHis') . $fName);
                    $field = new FieldsValue();
                    $field->alias = $fName;
                    $field->value = $path;
                } else {
                    $path = $field->value;
                }
                if (file_exists($path)) {
                    unlink($path);
                }
                $file->move($uploadDir, pathinfo($path)['basename']);
            }
            if ($type == 'file' || $type == 'image') {
                $mime = mime_content_type($field->value);
                if (strpos($mime, 'image') !== false && (
                        strpos($mime, 'jpeg') !== false
                        || strpos($mime, 'bmp') !== false
                        || strpos($mime, 'png') !== false
                    )
                ) {
                    $field->field_id = $fields->firstWhere('alias', 'image')->id;
                } else {
                    $field->field_id = $fields->firstWhere('alias', 'file')->id;
                }
            } else {
                $field->field_id = $fields->first()->id;
            }
            $field->save();

            return ['name' => $fName, 'path' => $path];
        } catch (\Exception $e) {
            return false;
        }
    }
}