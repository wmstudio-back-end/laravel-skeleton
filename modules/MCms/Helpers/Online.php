<?php

use Illuminate\Support\Facades\Redis;

function getOnlineUsers() {
    try {
        return Redis::hgetall(config('database.redis.online-key'));
    } catch (\Exception $e) {
        return [];
    }
}

function getCountOnlineUsers() {
    try {
        return Redis::hlen(config('database.redis.online-key'));
    } catch (\Exception $e) {
        return 0;
    }
}

function isUserOnline($userId) {
    try {
        return Redis::hget(config('database.redis.online-key'), $userId) !== null;
    } catch (\Exception $e) {
        return false;
    }
}
