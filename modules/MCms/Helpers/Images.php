<?php

/**
 * Сжимает размер изображения по ширине
 *   если $maxIsHeight = true, то по высоте
 *
 * @param string $fName
 * @param string $fNewName
 * @param integer $size
 * @param bool $maxIsHeight
 *
 * @return bool
 */
function imgResize($fName, $fNewName, $size, $maxIsHeight = false)
{
    list($iWidth, $iHeight, $type) = getimagesize($fName); // Получаем размеры и тип изображения (число)
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа

    if ($ext) {
        $func = 'imagecreatefrom' . $ext;
        $imgOrig = $func($fName); // Создаём дескриптор для работы с исходным изображением
    } else {
        throw new \InvalidArgumentException('Incorrect image'); // Выводим ошибку, если формат изображения недопустимый
    }

    $woh = (!$maxIsHeight) ? $iWidth : $iHeight;

    if ($woh <= $size) {
        $aw = $iWidth;
        $ah = $iHeight;
    } else {
        if (!$maxIsHeight) {
            $aw = $size;
            $ah = $size * $iHeight / $iWidth;
        } else {
            $aw = $size * $iWidth / $iHeight;
            $ah = $size;
        }
    }
    $imgNew = imagecreatetruecolor($aw, $ah);
    imagecopyresampled($imgNew, $imgOrig, 0, 0, 0, 0, $aw, $ah, $iWidth, $iHeight);
    $func = 'image' . $ext;
    return $func($imgNew, $fNewName);
}

/**
 * Функция снаачала сжимает ижображение до указанной ширины или высоты (в зависимости от того, что больше)
 *   потом обрезает по второму параметру (ширина/высота)
 *
 * @param string $fName
 * @param string $fNewName
 * @param int $newWidth
 * @param int|null $newHeight
 * @return bool
 */
function imgCropResize($fName, $fNewName, int $newWidth, int $newHeight = null)
{
    if ($newHeight === null) {
        $newHeight = $newWidth;
    }
    $types = array("", "gif", "jpeg", "png"); // Массив с типами изображений
    list($iWidth, $iHeight, $type) = getimagesize($fName); // Получаем размеры и тип изображения (число)
    $ext = $types[$type]; // Зная "числовой" тип изображения, узнаём название типа

    if ($ext) {
        $func = 'imagecreatefrom' . $ext; // Получаем название функции, соответствующую типу, для создания изображения
        $imgOrig = $func($fName); // Создаём дескриптор для работы с исходным изображением
        $aspectRatio = $newWidth / $newHeight;
        if ($iHeight < ($iWidth / $aspectRatio)) {
            $imgOrig = imagecrop($imgOrig, [
                'x' => 0,
                'y' => 0,
                'width' => $iHeight * $aspectRatio,
                'height' => $iHeight,
            ]);
            $imgOrig = imagescale($imgOrig, $newWidth);
        } else {
            $imgOrig = imagescale($imgOrig, $newWidth);
            $imgOrig = imagecrop($imgOrig, [
                'x' => 0,
                'y' => 0,
                'width' => $newWidth,
                'height' => $newHeight,
            ]);
        }
        $func = 'image' . $ext; // Получаем функция для сохранения результата
        return $func($imgOrig, $fNewName); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
    } else {
        throw new \InvalidArgumentException('Incorrect image'); // Выводим ошибку, если формат изображения недопустимый
    }

    $func = 'image' . $ext; // Получаем функция для сохранения результата
    return $func($imgNew, $fNewName); // Сохраняем изображение в тот же файл, что и исходное, возвращая результат этой операции
}