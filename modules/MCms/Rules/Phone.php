<?php

namespace Modules\MCms\Rules;

use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class Phone
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     * @return bool
     */
    public function validate($attribute, $value)
    {
        if ($value == null) {
            return true;
        } else {
            $result = false;
            $addon = UserAddon::where('alias', 'phone')->first();
            if ($addon) {
                $value = UserAddValue::where([
                    'addon_id' => $addon->id,
                    'value' => $value,
                ]);
                if ($user = \Auth::user()) {
                    $value->where('user_id', '!=', $user->id);
                }
                $value = $value->first();
                if (!$value) {
                    $result = true;
                }
            }

            return $result;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public static function message()
    {
        return __('validation.unique');
    }
}
