<?php

namespace Modules\MCms\Rules;

use Modules\Users\Entities\UserAddon;
use Modules\Users\Entities\UserAddValue;

class Referral
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param string $value
     * @return bool
     */
    public function validate($attribute, $value)
    {
        if ($value == null) {
            return true;
        } else {
            $result = false;

            $referal = explode('/', $value);
            $referal = end($referal);
            $refAddon = UserAddon::where('alias', 'referral_token')->first();
            if ($refAddon) {
                $referal = UserAddValue::where([
                    'addon_id' => $refAddon->id,
                    'value' => $referal,
                ])->first();
                if ($referal) {
                    $result = true;
                }
            }

            return $result;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public static function message()
    {
        return __('validation.invalid-referral');
    }
}
