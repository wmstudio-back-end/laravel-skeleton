<?php

namespace Modules\MCms\Navigation\Page;

/**
 * Represents a page with empty uri
 */
class Label extends AbstractPage
{

    /**
     * Returns hash
     *
     * @return string
     */
    public function getHref()
    {
        return '#';
    }

    /**
     * Returns whether page should be considered active or not
     *
     * This method will compare the page properties against the request uri.
     *
     * @param bool $recursive
     *            [optional] whether page should be considered
     *            active if any child pages are active. Default is
     *            false.
     * @return bool whether page should be considered active or not
     */
    public function isActive($recursive = false)
    {
        if (! $this->active) {
            return false;
        }

        return parent::isActive($recursive);
    }
}
