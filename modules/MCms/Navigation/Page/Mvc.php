<?php

namespace Modules\MCms\Navigation\Page;

use Modules\MCms\Exception;
use Illuminate\Support\Facades\Route;

/**
 * Represents a page that is defined using controller, action, route
 * name and route params to assemble the href
 *
 * The two constants defined were originally provided via the zend-mvc class
 * ModuleRouteListener; to remove the requirement on that component, they are
 * reproduced here.
 */
class Mvc extends AbstractPage
{
    /**
     * URL query part to use when assembling URL
     *
     * @var array|string
     */
    protected $query;

    /**
     * Params to use when assembling URL
     *
     * @see getHref()
     * @var array
     */
    protected $params = [];

    /**
     * RouteInterface name to use when assembling URL
     *
     * @see getHref()
     * @var string
     */
    protected $route;

    /**
     * Cached href
     *
     * The use of this variable minimizes execution time when getHref() is
     * called more than once during the lifetime of a request. If a property
     * is updated, the cache is invalidated.
     *
     * @var string
     */
    protected $hrefCache;

    // Accessors:

    /**
     * Returns whether page should be considered active or not
     *
     * This method will compare the page properties against the route matches
     * composed in the object.
     *
     * @param  bool $recursive  [optional] whether page should be considered
     *                          active if any child pages are active. Default is
     *                          false.
     * @return bool             whether page should be considered active or not
     */
    public function isActive($recursive = false)
    {
        if (! $this->active) {
            if (Route::currentRouteName() === $this->getRoute()) {
                $this->active = true;
                return $this->active;
            } else {
                return parent::isActive($recursive);
            }
        }

        return parent::isActive($recursive);
    }

    /**
     * Returns href for this page
     *
     * This method uses {@link RouteStackInterface} to assemble
     * the href based on the page's properties.
     *
     * @see RouteStackInterface
     * @return string  page href
     * @throws Exception\DomainException if no router is set
     */
    public function getHref()
    {
        if ($this->hrefCache) {
            return $this->hrefCache;
        }

        $params = $this->getParams();

        switch (true) {
            case ($this->getRoute() !== null):
                $name = $this->getRoute();
                break;
            default:
                throw new Exception\DomainException('No route name could be found');
        }

        if (null === ($fragment = $this->getFragment())) {
            $fragment = '';
        } else {
            $fragment = '#' . $fragment;
        }

        if (null === ($query = $this->getQuery())) {
            $query = [];
        }

        $url = route($name, array_merge($params, $query)) . $fragment;

        return $this->hrefCache = $url;
    }

    /**
     * Sets URL query part to use when assembling URL
     *
     * @see getHref()
     * @param  array|string|null $query    URL query part
     * @return self   fluent interface, returns self
     */
    public function setQuery($query)
    {
        $this->query      = $query;
        $this->hrefCache  = null;
        return $this;
    }

    /**
     * Returns URL query part to use when assembling URL
     *
     * @see getHref()
     *
     * @return array|string|null  URL query part (as an array or string) or null
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Sets params to use when assembling URL
     *
     * @see getHref()
     * @param  array|null $params [optional] page params. Default is null
     *                            which sets no params.
     * @return Mvc  fluent interface, returns self
     */
    public function setParams(array $params = null)
    {
        $this->params = empty($params) ? [] : $params;
        $this->hrefCache = null;
        return $this;
    }

    /**
     * Returns params to use when assembling URL
     *
     * @see getHref()
     *
     * @return array  page params
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Sets route name to use when assembling URL
     *
     * @see getHref()
     *
     * @param  string $route Route name to use when assembling URL.
     * @return Mvc Fluent interface, returns self.
     * @throws Exception\InvalidArgumentException If invalid $route is given.
     */
    public function setRoute($route)
    {
        if (null !== $route && (! is_string($route) || strlen($route) < 1)) {
            throw new Exception\InvalidArgumentException(
                'Invalid argument: $route must be a non-empty string or null'
            );
        }

        $this->route     = $route;
        $this->hrefCache = null;
        return $this;
    }

    /**
     * Returns route name to use when assembling URL
     *
     * @see getHref()
     *
     * @return string  route name
     */
    public function getRoute()
    {
        return $this->route;
    }

    // Public methods:

    /**
     * Returns an array representation of the page
     *
     * @return array  associative array containing all page properties
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'params' => $this->getParams(),
                'route'  => $this->getRoute(),
                'query'  => $this->getQuery(),
            ]
        );
    }
}
