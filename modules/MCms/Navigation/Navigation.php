<?php

namespace Modules\MCms\Navigation;

use Traversable;
use Modules\MCms\Exception;

/**
 * A simple container class for {@link Modules\MCms\Navigation\Page} pages
 */
class Navigation extends AbstractContainer
{
    public $onlyText = false;
    public $skipEmpty = false;
    public $startLevel;
    public $firstLevel = 0;
    public $separator = '/';
    public $lastLevel = null;

    /**
     * Creates a new navigation container
     *
     * @param  array|Traversable $pages    [optional] pages to add
     * @throws Exception\InvalidArgumentException  if $pages is invalid
     */
    public function __construct($pages = null)
    {
        if ($pages && (! is_array($pages) && ! $pages instanceof Traversable)) {
            throw new Exception\InvalidArgumentException(
                'Invalid argument: $pages must be an array, an '
                . 'instance of Traversable, or null'
            );
        }

        if ($pages) {
            $this->addPages($pages);
        }
    }

    private function childs($parent, &$tree)
    {
        foreach ($parent as $page) {
            if ($page->isActive(true)) {
                $tree[] = [
                    'label' => $page->getLabel(),
                    'uri' => $page->isStatic() ? null : $page->getHref(),
                ];
                if ($page->hasPages()) {
                    childs($page->getPages(), $tree);
                }
                break;
            }
        }
    }

    /**
     * Возвращает отрендеренный текст или массив для последующего рендеринга
     *
     * @param array $options
     * @return array|string
     */
    public function render($options = [])
    {
        $tree = [];
        $opts = [
            'onlyText' => isset($options['onlyText']) ? $options['onlyText'] : $this->onlyText,
            'skipEmpty' => isset($options['skipEmpty']) ? $options['skipEmpty'] : $this->skipEmpty,
            'firstLevel' => isset($options['firstLevel']) ? $options['firstLevel'] : $this->firstLevel,
            'separator' => isset($options['separator']) ? $options['separator'] : $this->separator,
            'lastLevel' => isset($options['lastLevel']) ? $options['lastLevel'] : $this->lastLevel,
            'reverse' => isset($options['reverse']) ? $options['reverse'] : false,
        ];

        if ($this->startLevel) {
            $startLevel = $this->findBy('route', $this->startLevel);
            $tree[] = [
                'label' => $startLevel->getLabel(),
                'uri' => $startLevel->isStatic() ? '#' : $startLevel->getHref(),
            ];
        }

        foreach ($this as $page) {
            if ($page->isActive(true) && (!isset($startLevel) || (isset($startLevel) && $page != $startLevel))) {
                $current = [
                    'label' => $page->getLabel(),
                    'uri' => $page->isStatic() ? '#' : $page->getHref(),
                ];
                if (!($opts['skipEmpty'] && ($page->isStatic() || trim($page->getHref()) === '' || trim($page->getHref()) === '#'))) {
                    $tree[] = $current;
                }
                if ($page->hasPages()) {
                    $this->childs($page->getPages(), $tree);
                }
                break;
            }
        }

        if ($opts['lastLevel']) {
            if (is_string($opts['lastLevel'])) {
                $tree[] = [
                    'label' => $opts['lastLevel'],
                    'uri' => null,
                ];
            } elseif (is_array($opts['lastLevel'])) {
                foreach ($opts['lastLevel'] as $lastLevel) {
                    if (isset($lastLevel['label'])) {
                        $tree[] = [
                            'label' => $lastLevel['label'],
                            'uri' => isset($lastLevel['uri']) ? $lastLevel['uri'] : '#',
                        ];
                    }
                }
            }
        }

        for ($i = 0; $i < $opts['firstLevel']; $i++) {
            unset($tree[$i]);
        }

        if ($opts['reverse']) {
            $tree = array_reverse($tree);
        }

        if ($opts['onlyText']) {
            $res = '';
            for ($i = 0; $i < count($tree) - 1; $i++) {
                $res .= $tree[$i]['label'] . ' ' . $opts['separator'] . ' ';
            }

            if (isset($tree[$i])) {
                $res .= $tree[$i]['label'];
            }

            return $res;
        } else {
            return $tree;
        }
    }
}
