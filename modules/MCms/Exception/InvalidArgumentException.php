<?php

namespace Modules\MCms\Exception;

/**
 * Navigation invalid argument exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements
    ExceptionInterface
{
}
