<?php

namespace Modules\MCms\Exception;

/**
 * Navigation domain exception
 */
class DomainException extends \DomainException implements ExceptionInterface
{
}
