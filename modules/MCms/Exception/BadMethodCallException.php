<?php

namespace Modules\MCms\Exception;

/**
 * Navigation bad method call exception
 */
class BadMethodCallException extends \BadMethodCallException implements
    ExceptionInterface
{
}
