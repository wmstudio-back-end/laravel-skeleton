<?php

namespace Modules\MCms\Exception;

/**
 * Navigation out of bounds exception
 */
class OutOfBoundsException extends \OutOfBoundsException implements ExceptionInterface
{
}
