### Config modules

For just-generated modules you need to delete from ServiceProvider

```bash
public function boot()
    {
        ...
        $this->registerConfig();
        ...
    }
    
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('module.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'module'
        );
    }
```

Because the register config files is made in `\App\Providers\AppServiceProvider@boot`

This is done to ensure that all configuration files are automatically loaded before module initialization

And now from any module you can get config of any module

#### Navigation

navigation is `zendframework/zend-navigation` adapted for laravel

For more information read [official documentation](https://docs.zendframework.com/zend-navigation/intro/)