<?php

$name = 'Отзывы';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.reviews',
                    'label' => 'Просмотр списка отзывов',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/edit',
                'parameters' => [
                    'as' => 'admin.reviews.edit',
                    'label' => 'Редактирование отзывов',
                    'uses' => 'AdminController@edit',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/delete',
                'parameters' => [
                    'as' => 'admin.reviews.delete',
                    'label' => 'Удаление отзывов',
                    'uses' => 'AdminController@delete',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.reviews',
                'icon'      => 'fa fa-commenting-o',
                'order'     => 50,
            ],
        ],
    ],
];
