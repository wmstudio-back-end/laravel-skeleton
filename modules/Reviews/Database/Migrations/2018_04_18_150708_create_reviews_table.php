<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reviews', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->index('user_id');
			$table->string('lang', 5)->default('ru');
			$table->text('content');
			$table->boolean('active')->default(false);
            $table->boolean('edited')->default(false);
            $table->integer('weight')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reviews');
        $permissions = UserPermission::where('route_name', 'like', '%reviews%')->get();
        UserPermission::unguard();
        foreach ($permissions as $permission) {
            $permission->delete();
        }
        UserPermission::reguard();
	}

}
