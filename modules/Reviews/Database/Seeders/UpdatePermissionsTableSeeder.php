<?php

namespace Modules\Reviews\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Reviews\Entities\Review;
use Modules\Users\Entities\User;
use Modules\Users\Entities\UserPermission;

class UpdatePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasPermissions = UserPermission::where([
            'link_to' => UserPermission::LINK_GROUP,
            'parent_id' => 1,
            'route_name' => 'admin/reviews',
        ])->count('id');
        if (!$hasPermissions) {
            UserPermission::create([
                'link_to' => UserPermission::LINK_GROUP,
                'parent_id' => 1,
                'route_name' => 'admin/reviews',
                'permission' => UserPermission::P_WRITE,
            ]);
        }

        if (env('APP_ENV', 'local') != 'production') {
            $user = User::first();
            if ($user) {
                $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
                Review::insert([
                    [
                        'user_id' => $user->id,
                        'content' => "Это отзыв\nвот!",
                        'weight' => 1,
                        'created_at' => $timeStamp,
                        'updated_at' => $timeStamp,
                    ],
                    [
                        'user_id' => $user->id,
                        'content' => "<div class=\"post-text\" itemprop=\"text\">\r\n\r\n<p>I have a folder I\\'ve placed "
                            . "in the /public folder in my Laravel site . The path is:</p > \r\n\r\n<pre class=\"lang-php "
                            . "prettyprint prettyprinted\" style=\"\"><code><span class=\"str\">/public/</span><span class"
                            . "=\"pln\">test</span></code></pre>\r\n\r\n<p>the \"test\" folder has a file called index.html"
                            . " that is just a static resource I want to load from the public folder (I don\\'t want to "
                            . "create a view for this within the framework--too much to explain).</p>\r\n\r\n<p>I\\'ve made"
                            . " a route like this:</p>\r\n\r\n<pre class=\"lang-php prettyprint prettyprinted\" style=\"\">"
                            . "<code><span class=\"typ\">Route</span><span class=\"pun\">::</span><span class=\"kwd\">get"
                            . "</span><span class=\"pun\">(</span><span class=\"str\">\\'/test\\'</span><span class=\"pun\">"
                            . ",</span><span class=\"pln\"> </span><span class=\"kwd\">function</span><span class=\"pun\">()"
                            . "</span><span class=\"pln\"> </span><span class=\"pun\">{</span><span class=\"pln\">\r\n  "
                            . "</span><span class=\"kwd\">return</span><span class=\"pln\"> </span><span class=\"typ\">File"
                            . "</span><span class=\"pun\">::</span><span class=\"kwd\">get</span><span class=\"pun\">"
                            . "(</span><span class=\"pln\">public_path</span><span class=\"pun\">()</span><span class"
                            . "=\"pln\"> </span><span class=\"pun\">.</span><span class=\"pln\"> </span><span class"
                            . "=\"str\">\\'/test/index.html\\'</span><span class=\"pun\">);</span><span class=\"pln\">"
                            . "\r\n</span><span class=\"pun\">});</span></code></pre>\r\n\r\n<p>I\\'m not able to get this "
                            . "to work. I just get an error from Chrome saying this page has a redirect loop.</p>\r\n\r\n"
                            . "<p>I can access it this way:</p>\r\n\r\n<pre class=\"lang-php prettyprint prettyprinted\" "
                            . "style=\"\"><code><span class=\"pln\">http</span><span class=\"pun\">:</span><span class="
                            . "\"com\">//www.example.com/test/index.html</span></code></pre>\r\n\r\n<p>But I can\\'t use "
                            . "the .html extension anywhere. The file must be visible as:</p>\r\n\r\n<pre class=\"lang-php "
                            . "prettyprint prettyprinted\" style=\"\"><code><span class=\"pln\">http</span><span class="
                            . "\"pun\">:</span><span class=\"com\">//www.example.com/test/</span></code></pre>\r\n\r\n<p>"
                            . "How can I serve a single HTML page from the Laravel public folder without having to use the "
                            . ".html extension?</p>\r\n</div>",
                        'weight' => 2,
                        'created_at' => $timeStamp,
                        'updated_at' => $timeStamp,
                    ],
                ]);
            }
        }
    }
}
