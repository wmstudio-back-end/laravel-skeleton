<?php

namespace Modules\Reviews\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Reviews\Entities\Review;

class AdminController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviewsPerPage = 40;

        $reviews = Review::orderBy('lang', 'ASC')->orderByDesc('weight')->paginate($reviewsPerPage);

        return view('reviews::admin.index', [
            'reviews' => $reviews,
        ]);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data = getRequest();
        if (isset($data['action'])) {
            switch ($data['action']) {
                case 'editContent':
                    $id = $data['reviewId'];
                    unset($data['reviewId']);

                    $error = false;
                    $msgHeader = 'Обзор изменен.';
                    $message = 'Запись успешно обновлена.';

                    try {
                        $review = Review::find($id);
                        if ($review) {
                            $review->content = $data['reviewContent'];
                            $review->edited = true;
                            $review->save();
                        } else {
                            throw new \Exception('Review ' . $id . 'is not found', 404);
                        }
                    } catch (\Exception $e) {
                        $error = true;
                        $msgHeader = 'Ошибка';
                        $message = $e->getMessage();
                    }

                    if (request()->ajax()) {
                        return response()->json([
                            'error' => $error,
                            'msgHeader' => $msgHeader,
                            'message' => $message,
                        ]);
                    } else {
                        Input::replace([
                            'toastr.error' => $error,
                            'toastr.msgHeader' => $msgHeader,
                            'toastr.message' => $message,
                        ]);
                        return redirect()->route('admin.reviews')->withInput();
                    }
                    break;
                case 'upReviews':
                    $error = false;
                    $message = 'Порядок успешно изменен.';

                    try {
                        $max = \DB::table('reviews')->max('weight');
                        $reviews = Review::whereIn('id', $data['reviewIds'])->orderBy('weight')->get();
                        if ($reviews && $reviews->count()) {
                            Review::unguard();
                            foreach ($reviews as $review) {
                                /* @var $review Review */
                                $review->weight = ++$max;
                                $review->save();
                            }
                            Review::reguard();
                        } else {
                            throw new \Exception('Reviews not found', 404);
                        }
                    } catch (\Exception $e) {
                        $error = true;
                        $message = $e->getMessage();
                    }

                    if (request()->ajax()) {
                        return response()->json([
                            'error' => $error,
                            'message' => $message,
                        ]);
                    } else {
                        Input::replace([
                            'toastr.error' => $error,
                            'toastr.message' => $message,
                        ]);
                        return redirect()->route('admin.reviews')->withInput();
                    }
                    break;
                case 'toggleActive':
                    $error = false;
                    $message = 'Активность успешно изменена.';

                    try {
                        $reviews = Review::whereIn('id', $data['reviewIds'])->get();
                        if ($reviews && $reviews->count()) {
                            Review::unguard();
                            foreach ($reviews as $review) {
                                /* @var $review Review */
                                $review->active = !$review->active;
                                $review->save();
                            }
                            Review::reguard();
                        } else {
                            throw new \Exception('Reviews not found', 404);
                        }
                    } catch (\Exception $e) {
                        $error = true;
                        $message = $e->getMessage();
                    }

                    if (request()->ajax()) {
                        return response()->json([
                            'error' => $error,
                            'message' => $message,
                        ]);
                    } else {
                        Input::replace([
                            'toastr.error' => $error,
                            'toastr.message' => $message,
                        ]);
                        return redirect()->route('admin.reviews')->withInput();
                    }
                    break;
            }
        } else {
            if (request()->ajax()) {
                return response()->json([
                    'error' => true,
                    'msgHeader' => 'Ошибка.',
                    'message' => 'Неизвестный "action".',
                ]);
            } else {
                abort(404);
            }
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $error = false;
        $message = 'Отзывы успешно удалены.';
        try {
            $data = getRequest();
            if (isset($data['reviewIds'])) {
                $reviewIds = $data['reviewIds'];
            } else {
                throw new \Exception('Reviews not found', 404);
            }

            $reviews = Review::whereIn('id', $reviewIds)->get();
            if ($reviews && $reviews->count()) {
                Review::unguard();
                foreach ($reviews as $review) {
                    /* @var $review Review */
                    $review->delete();
                }
                Review::reguard();
            } else {
                throw new \Exception('Reviews not found', 404);
            }
        } catch (\Exception $e) {
            $error = true;
            $message = $e->getMessage();
        }

        if (request()->ajax()) {
            return response()->json([
                'error' => $error,
                'message' => $message,
            ]);
        } else {
            Input::replace([
                'toastr.error' => $error,
                'toastr.message' => $message,
            ]);
            return redirect()->route('admin.reviews')->withInput();
        }
    }
}
