<?php

namespace Modules\Reviews\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Modules\Reviews\Entities\Review;

class IndexController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function addReview()
    {
        $user = \Auth::user();
        if (request()->method() === 'POST' && ($status = $user->getStatus())) {
            $data = getRequest();

            $validator = [
                'rev-rating' => 'required|integer|between:1,' . Review::MAX_RATING,
                'rev-content' => 'required|string',
            ];

            $validator = \Validator::make($data, $validator, [], [

            ]);

            if (!$validator->fails()) {
                $review = new Review();
                $review->user_id = $user->id;
                $review->lang = $user->addon('lang');
                $review->content = $data['rev-content'];
                $review->rating = $data['rev-rating'];
                $review->save();
            } else {
                $errors = '';
                foreach ($validator->errors()->toArray() as $key => $error) {
                    $errors .= array_shift($error) . '<br>';
                }
                $errors = substr($errors, 0, -4);
                Input::merge([
                    'errorHeader' => ___('profile.type.teacher.error.header'),
                    'errorMessage' => $errors,
                ]);
                Input::flash();
            }

            return redirect()->back();
        }

        abort(404);
    }
}
