<?php
$canEdit   = getPermissions('admin.reviews.edit');
$canDelete = getPermissions('admin.reviews.delete');
?>

@extends(config('admin.defaults.layout'), ['old' => old()])

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список отзывов</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @if($canEdit || $canDelete)
                            <div class="col-12 text-right pb-3">
                                <h6 class="pull-left">Действия с отмеченными</h6>
                                @if($canEdit)
                                    <button
                                        id="review-toggle-active"
                                        type="button"
                                        class="btn btn-action btn-warning"
                                        data-action="{{ route('admin.reviews.edit') }}"
                                        data-post-action="toggleActive"
                                        data-token="{{ csrf_token() }}"
                                    >Изменить активность</button>
                                    <button
                                        id="review-up"
                                        type="button"
                                        class="btn btn-action btn-primary"
                                        data-action="{{ route('admin.reviews.edit') }}"
                                        data-post-action="upReviews"
                                        data-token="{{ csrf_token() }}"
                                    >Поднять</button>
                                @endif
                                @if($canDelete)
                                    <button
                                        id="review-delete"
                                        type="button"
                                        class="btn btn-action btn-danger"
                                        data-action="{{ route('admin.reviews.delete') }}"
                                        data-token="{{ csrf_token() }}"
                                    >Удалить</button>
                                @endif
                            </div>
                        @endif
                        <div class="col-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <th class="text-center">
                                            <div class="custom-checkbox custom-control">
                                                <input
                                                    type="checkbox"
                                                    data-checkboxes="mygroup"
                                                    data-checkbox-role="dad"
                                                    class="custom-control-input"
                                                    id="checkbox-all"
                                                >
                                                <label for="checkbox-all" class="custom-control-label"></label>
                                            </div>
                                        </th>
                                        <th>Пользователь</th>
                                        <th class="text-center">Язык</th>
                                        <th class="text-right"><i class="fa fa-pencil text-success"></i></th>
                                        <th>Отзыв</th>
                                        <th class="text-center">Оценка</th>
                                        <th class="text-right">Активность</th>
                                    </tr>
                                    @foreach($reviews as $review)
                                        <?php /* @var $review \Modules\Reviews\Entities\Review */ ?>
                                        <tr data-id="{{ $review->id }}">
                                            <td width="40" class="align-middle">
                                                <div class="custom-checkbox custom-control">
                                                    <input
                                                        type="checkbox"
                                                        data-checkboxes="mygroup"
                                                        class="custom-control-input review-selected"
                                                        id="checkbox-{{ $review->id }}"
                                                    >
                                                    <label for="checkbox-{{ $review->id }}" class="custom-control-label"></label>
                                                </div>
                                            </td>
                                            <td class="align-middle">{{ $review->user->getFullName() }}</td>
                                            <td class="text-center align-middle">{{ $review->lang }}</td>
                                            <td class="text-right align-middle">
                                                <i class="fa {{ $review->edited ? 'fa-pencil text-success' : 'fa-id-card-o text-primary' }}"></i>
                                            </td>
                                            <td class="align-middle">
                                                <?php
                                                $preview = preg_replace(
                                                    ["~</p>~", "~</div>~", "~<br>~", "~<br\/>~"],
                                                    ["</p>\n", "</div>\n", "<br>\n", "<br\/>\n"],
                                                    strip_tags($review->content)
                                                );
                                                $preview = preg_replace("!\n+!", "\n", str_replace("\r\n", "\n", $preview));
                                                if (mb_strlen($preview, 'utf-8') > 100) {
                                                    $preview = mb_substr($preview, 0, 100, 'utf-8') . '...';
                                                }
                                                ?>
                                                <button
                                                    type="button"
                                                    class="review-edit-btn btn btn-link p-0 pre-text"
                                                    data-id="{{ $review->id }}" data-content="{{ $review->content }}"
                                                >{{ $preview }}</button>
                                            </td>
                                            <td class="text-center align-middle">{{ $review->rating }}/{{
                                                \Modules\Reviews\Entities\Review::MAX_RATING }}</td>
                                            <td class="text-right align-middle">
                                                <? if ($review->active) {
                                                    $status = 'Активен';
                                                    $activeClass = 'badge-success';
                                                } else {
                                                    $status = 'Не активен';
                                                    $activeClass = 'badge-danger';
                                                }
                                                ?>
                                                <div class="review-active badge {{ $activeClass }}">{{ $status }}</div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        {{ $reviews->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-review-edit">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $canEdit ? 'Редактировать отзыв' : 'Просмотр отзыва' }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @if ($canEdit)
                    <form id="modal-review-form" method="post" action="{{ route('admin.reviews.edit') }}">
                        @csrf
                        <input type="hidden" name="action" value="editContent">
                        <input id="modal-review-id" type="hidden" name="reviewId">
                @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea
                                id="modal-review-content"
                                name="reviewContent"
                                class="form-control summernote"
                                placeholder="Отзыв"
                                rows="17"
                                @if(!$canEdit)
                                    readonly="readonly"
                                    disabled="disabled"
                                @endif
                            ></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-action" data-dismiss="modal">Закрыть</button>
                        @if ($canEdit)
                            <button type="submit" class="btn btn-primary btn-action">Сохранить</button>
                        @endif
                    </div>
                @if ($canEdit)
                    </form>
                @endif
            </div>
        </div>
    </div>
@stop
