<?php

namespace Modules\Reviews\Entities;

use Modules\Users\Entities\User;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CmsReview
 *
 * @property int $id
 * @property int $user_id
 * @property string $lang
 * @property string $content
 * @property int $rating
 * @property bool $active
 * @property bool $edited
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 *
 * @package App\Models
 */
class Review extends Eloquent
{
    const MAX_RATING = 10;

    protected $casts = [
        'user_id' => 'int',
        'rating' => 'int',
        'active' => 'bool',
        'edited' => 'bool',
        'weight' => 'int',
    ];

    protected $fillable = [
        'user_id',
        'lang',
        'content',
        'rating',
        'active',
        'edited',
        'weight',
    ];

    protected $attributes = [
        'active' => false,
        'edited' => false,
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
