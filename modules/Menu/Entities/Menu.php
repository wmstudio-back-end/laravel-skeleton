<?php

namespace Modules\Menu\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class Menu
 *
 * @property int $id
 * @property int $parent
 * @property string $alias
 * @property string $name
 * @property string $icon
 * @property string $class
 * @property string $url
 * @property bool $active
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $children
 */
class Menu extends Eloquent
{
    // todo Сделать мульти-уровневый список
    const MULTI_LEVEL_ENABLED = false;

    const TRANS_GROUP = 'menu';

    public $table = 'menu';

	protected $attributes = [
	    'active' => true,
    ];

    protected $casts = ['parent' => 'int', 'active' => 'bool', 'weight' => 'int'];

	protected $fillable = [
//	    'parent',
        'alias',
        'name',
        'url',
            /**
             * If comment this fields,
             * they will be hidden in the settings
             *     admin panel -> menu -> edit
             */
            //'icon',
            'class',
        'active',
        'weight',
    ];

    public function children()
    {
        return $this->hasMany(self::class, 'parent');
    }
}
