<?php

$name = 'Меню';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.menu',
                    'label' => 'Просмотр списка меню',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/sort',
                'parameters' => [
                    'as' => 'admin.menu.sort',
                    'label' => 'Сортировка списка меню',
                    'uses' => 'AdminController@sort',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/edit/{id}',
                'parameters' => [
                    'as' => 'admin.menu.edit',
                    'label' => 'Добавление и редактирование элеменетов меню',
                    'uses' => 'AdminController@edit',
                ],
                'where' => ['id' => 'new|\d+'],
            ],
            [
                'method' => 'post',
                'uri' => '/delete',
                'parameters' => [
                    'as' => 'admin.menu.delete',
                    'label' => 'Удаление элементов меню',
                    'uses' => 'AdminController@delete',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label'     => $name,
                'route'     => 'admin.menu',
                'icon'      => 'fa fa-list',
                'order'     => 20,
            ],
        ],
    ],
];
