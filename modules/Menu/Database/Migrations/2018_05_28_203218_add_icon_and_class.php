<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIconAndClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu', function (Blueprint $table) {
            $table->integer('parent')->after('id')->nullable();
            $table->string('icon')->after('url')->nullable();
            $table->string('class')->after('icon')->nullable();
            $table->foreign('parent', 'parent_node')->references('id')->on('menu')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('menu', function (Blueprint $table) {
            $table->dropForeign('parent_node');
            $table->dropColumn('parent');
            $table->dropColumn('icon');
            $table->dropColumn('class');
        });
    }
}
