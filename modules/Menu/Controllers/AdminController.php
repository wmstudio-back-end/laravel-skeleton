<?php

namespace Modules\Menu\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Menu\Entities\Menu;
use Barryvdh\TranslationManager\Models\Translation;

class AdminController extends Controller
{
    public static function writeRequired()
    {
        return [
            'sort',
            'edit',
            'delete',
        ];
    }

    public function index()
    {
        $menus = Menu::all();

        return view('menu::admin.index', [
            'menus' => $menus,
        ]);
    }

    public function sort(Request $request)
    {
        if ($request->ajax()) {
            try {
                $error = false;
                $msgHeader = 'Успешно обновлено!';
                $message = 'Данные отсортированы.';

                $sortable = $request->get('sortable', null);
                $menus = Menu::whereIn('id', $sortable)->get()->keyBy('id');
                if ($menus && $menus->count() == count($sortable)) {
                    Menu::unguard();
                    foreach ($sortable as $index => $key) {
                        $menus[$key]->weight = $index + 1;
                        $menus[$key]->save();
                    }
                    Menu::reguard();
                } else {
                    throw new \Exception('Элементы меню не найдены');
                }
            } catch (\Exception $e) {
                $error = true;
                $msgHeader = 'Ошибка!';
                $message = $e->getMessage();
            }
            \Cache::delete('menus');

            return response()->json([
                'error' => $error, 'msgHeader' => $msgHeader, 'message' => $message,
            ]);
        }

        abort(404);
    }

    public function edit($id, Request $request)
    {
        $menu = getMenu('admin');
        /* @var $menu \Modules\MCms\Navigation\Navigation */
        $menu->findBy('route', 'admin.menu')->setActive();

        if ($id == 'new') {
            $menu->lastLevel = 'Создать';
            $menu = new Menu();
            $menu->weight = \DB::table('menu')->max('weight') + 1;
        } else {
            $menu->lastLevel = 'Редактировать';
            $menu = Menu::find($id);
        }

        if ($menu) {
            $locales = $this->transManager->getLocales();
            $defLang = config('app.locale');
            $keys = Translation::where([
                'group' => Menu::TRANS_GROUP,
                'key' => $menu->alias,
            ])->get();
            $translations = [];
            foreach($keys as $translation){
                /* @var $translation Translation */
                $translations[$translation->locale] = $translation;
            }
            if (count($translations) != count($locales)) {
                foreach ($locales as $lang) {
                    if (!isset($translations[$lang])) {
                        $translations[$lang] = new Translation();
                    }
                }
            }

            if ($request->isMethod('POST')) {
                $data = getRequest();

                $validator = \Validator::make($data, [
                    'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $menu->alias ? '|unique:menu' : ''),
                    'url' => 'string|required',
                    'icon' => 'regex:/^[a-z][\S\s]+$/u|nullable',
                    'class' => 'regex:/^[a-z][\S\s]+$/u|nullable',
                    'active' => 'bool|required',
                    'menu-name-' . $defLang => 'required|string',
                ]);

                if (!$validator->fails()) {
                    $data['alias'] = str_slug($data['alias']);
                    if (
                        $menu->alias != $data['alias']
                        || $menu->url != $data['url']
                        || $menu->active != $data['active']
                        || (
                            in_array('icon', $menu->getFillable())
                            && array_key_exists('icon', $data)
                            && $menu->icon != $data['icon']
                        ) || (
                            in_array('class', $menu->getFillable())
                            && array_key_exists('class', $data)
                            && $menu->class != $data['class']
                        )
                    ){
                        \Cache::delete('menus');
                    }

                    $menu->alias = $data['alias'];
                    $menu->name = $data['menu-name-' . $defLang];
                    $menu->url = $data['url'];
                    if (in_array('icon', $menu->getFillable()) && array_key_exists('icon', $data)) {
                        $menu->icon = $data['icon'];
                    }
                    if (in_array('class', $menu->getFillable()) && array_key_exists('class', $data)) {
                        $menu->class = $data['class'];
                    }
                    $menu->active = $data['active'];
                    $menu->save();

                    Translation::unguard();
                    foreach ($translations as $lang => $translation) {
                        if (array_key_exists('menu-name-' . $lang, $data)) {
                            $translation->status = Translation::STATUS_CHANGED;
                            $translation->locale = $lang;
                            $translation->group = Menu::TRANS_GROUP;
                            $translation->key = $menu->alias;
                            $translation->value = $data['menu-name-' . $lang];
                            $translation->save();
                        } else {
                            $translation->delete();
                        }
                    }
                    Translation::reguard();

                    return redirect()->route('admin.menu');
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }

            return view('menu::admin.edit', [
                'menu' => $menu,
                'locales' => $locales,
                'defLang' => $defLang,
                'translations' => $translations,
            ]);
        } else {
            abort(404);
        }
    }

    public function delete()
    {
        $id = request()->get('id', null);

        $menu = Menu::find($id);
        if ($menu) {
            $translations = Translation::where([
                'group' => Menu::TRANS_GROUP,
                'key' => $menu->alias,
            ])->get();
            Translation::unguard();
            foreach ($translations as $translation) {
                $translation->delete();
            }
            Translation::reguard();
            $menu->delete();
            \Cache::delete('menus');
            return redirect()->route('admin.menu');
        } else {
            abort(404);
        }
    }
}
