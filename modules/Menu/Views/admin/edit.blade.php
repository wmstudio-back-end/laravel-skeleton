@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                @if($lastLevel = getMenu('admin')->lastLevel)
                    <div class="card-header">
                        <h4>{{ $lastLevel . ' элемент меню' }}</h4>
                    </div>
                @endif
                <div class="card-body">
                    <?php /* @var $menu \Modules\Menu\Entities\Menu */ ?>
                    <form method="post" class="form-horizontal">
                        @csrf
                        <div class="form-group row">
                            <label for="alias" class="col-md-2 col-form-label required-label">Указатель</label>
                            <div class="col-md-10">
                                <input
                                        id="alias"
                                        name="alias"
                                        type="text"
                                        class="form-control{{ $errors->has('alias') ? ' is-invalid' : '' }}"
                                        value="{{ old('alias') ?: $menu->alias }}"
                                        placeholder="Нужен для переводов (alias)"
                                        required="required"
                                >
                                @if ($errors->has('alias'))
                                    <div class="invalid-feedback">{{ str_replace('alias ', '', $errors->first('alias')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Текст</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-name-{{ $lang }}-tab" data-toggle="tab" href="#lang-name-{{ $lang }}"
                                               role="tab" aria-controls="lang-name-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-name-{{ $lang }}" role="tabpanel" aria-labelledby="lang-name-tab">
                                            <input
                                                name="menu-name-{{ $lang }}"
                                                type="text"
                                                class="form-control{{ $errors->has('menu-name-' . $lang) ? ' is-invalid' : '' }}"
                                                value="{{ old('menu-name-' . $lang) ?:
                                                    $translations[$lang]->value ?: ($defLang == $lang ? $menu->name : '')
                                                }}"
                                                @if($lang === $defLang)required="required"@endif
                                            >
                                        </div>
                                        @if ($errors->has('menu-name-' . $lang))
                                            <div class="invalid-feedback">
                                                {{ str_replace("menu-name-$lang ", '', $errors->first('menu-name-' . $lang)) }}
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="url" class="col-md-2 col-form-label required-label">Url</label>
                            <div class="col-md-10">
                                <input
                                    id="url"
                                    name="url"
                                    type="text"
                                    class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                    value="{{ old('url') ?: $menu->url }}"
                                    required="required"
                                >
                                @if ($errors->has('url'))
                                    <div class="invalid-feedback">{{ str_replace("url ", '', $errors->first('url')) }}</div>
                                @endif
                            </div>
                        </div>
                        @if(in_array('icon', $menu->getFillable()))
                            <div class="form-group row">
                                <label for="icon" class="col-md-2 col-form-label">Icon class</label>
                                <div class="col-md-10">
                                    <input
                                        id="icon"
                                        name="icon"
                                        type="text"
                                        class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}"
                                        value="{{ old('icon') ?: $menu->icon }}"
                                    >
                                    @if ($errors->has('icon'))
                                        <div class="invalid-feedback">{{ str_replace("icon ", '', $errors->first('icon')) }}</div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        @if(in_array('class', $menu->getFillable()))
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-2">
                                    <label>Добавьте класс <b>hidden</b> для того, чтобы не отображать элемент меню (для мультиязычных заголовков)</label>
                                    <label>Добавьте класс <b>footer-hidden</b> для того, чтобы не отображать элемент меню
                                        <b>только</b> в нижнем колонтитуле (footer)</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="class" class="col-md-2 col-form-label">Class</label>
                                <div class="col-md-10">
                                    <input
                                        id="class"
                                        name="class"
                                        type="text"
                                        class="form-control{{ $errors->has('class') ? ' is-invalid' : '' }}"
                                        value="{{ old('class') ?: $menu->class }}"
                                    >
                                    @if ($errors->has('class'))
                                        <div class="invalid-feedback">{{ str_replace("class ", '', $errors->first('class')) }}</div>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-1" value="active">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="active"
                                            name="active"
                                            value="1"
                                            {{ old('active', null) !== null
                                                ? (old('active') ? 'checked="checked"' : '')
                                                : $menu->active ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection