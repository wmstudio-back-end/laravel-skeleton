<?php
$canSort = getPermissions('admin.menu.sort');
$canEdit = getPermissions('admin.menu.edit');
$canDelete = getPermissions('admin.menu.delete');
?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список элементов меню</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.menu.edit', ['id' => 'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    @if($canDelete)
                        <form method="post" action="{{ route('admin.menu.delete') }}">
                            @csrf
                    @endif
                    <div class="table-responsive">
                        <table class="table table-hover">
                            @if($menus->count())
                                <thead>
                                    <tr>
                                        @if($canSort)
                                            <th class="text-center"><i class="ion ion-ios-keypad"></i></th>
                                        @endif
                                        <th>Текст ссылки</th>
                                        <th>Ссылка</th>
                                        <th>Статус</th>
                                        @if($canEdit || $canDelete)
                                            <th class="text-right">Действия</th>
                                        @endif
                                    </tr>
                                </thead>
                            @endif
                            <tbody
                                    id="menu-sortable"
                                    class="sortable-with-handle"
                                    @if($canSort)
                                        data-action="{{ route('admin.menu.sort') }}"
                                        data-token="{{ csrf_token() }}"
                                    @endif
                            >
                                @if($menus->count())
                                    @foreach($menus as $menu)
                                        <?php /* @var $menu \Modules\Menu\Entities\Menu */ ?>
                                        <tr data-id="{{ $menu->id }}">
                                            @if($canSort)
                                                <td class="text-center">
                                                    <div class="sort-handler">
                                                        <i class="ion ion-ios-keypad"></i>
                                                    </div>
                                                </td>
                                            @endif
                                            <td>
                                                @php $classes = explode(' ', $menu->class); @endphp
                                                <a
                                                    href="{{ $canEdit ? route('admin.menu.edit', ['id' => $menu->id]) : $menu->url }}"
                                                    {{ in_array('hidden', $classes)
                                                        ? ' class=hidden-class'
                                                        : (in_array('footer-hidden', $classes)
                                                            ? ' class=hidden-footer-class'
                                                            : '')
                                                    }}
                                                >{{ $menu->name }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ url($menu->url) }}" target="_blank">{{ $menu->url }}</a>
                                            </td>
                                            <td>
                                                <? if ($menu->active) {
                                                    $status = 'Активна';
                                                    $activeClass = 'badge-success';
                                                } else {
                                                    $status = 'Не активна';
                                                    $activeClass = 'badge-danger';
                                                }
                                                ?>
                                                <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                            </td>
                                            @if($canEdit || $canDelete)
                                                <td class="text-right">
                                                    @if($canEdit)
                                                        <a href="{{ route('admin.menu.edit', ['id' => $menu->id]) }}" class="btn btn-action btn-primary">Редактировать</a>
                                                    @endif
                                                    @if($canDelete)
                                                        <button type="submit" name="id" value="{{ $menu->id }}" class="btn btn-action btn-danger">Удалить</button>
                                                    @endif
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @else
                                    <tr><td class="text-center">Ни одного элемента меню пока не созданно.</td></tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                    @if($canDelete)
                        </form>
                    @endif
                </div>
                @if ($canEdit)
                    <div class="card-footer">
                        <a href="{{ route('admin.menu.edit', ['id' => 'new']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection