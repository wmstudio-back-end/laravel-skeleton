<?php

namespace Modules\Messages\Controllers;

use App\Http\Controllers\Controller;
use Modules\Messages\Entities\Message;
use Modules\Translations\Constants\Languages;
use Modules\Users\Entities\User;

class IndexController extends Controller
{
    protected $usersPerPage = 30;
    protected $messagesPerPage = 50;

    /**
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function getUsers()
    {
        if (request()->ajax()) {
            return response()->json(findUserDialogs(\Auth::user(), $this->usersPerPage));
        }

        abort(404);
    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function getMessages($userId)
    {
        if (request()->ajax()) {
            $user = \Auth::user();

            $messages = Message::where(function($query) use($user, $userId) {
                $query->where(function($query) use($user, $userId) {
                    $query->where([
                        'sender_id' => $user->id,
                        'receiver_id' => $userId,
                    ]);
                })->orWhere(function($query) use($user, $userId) {
                    $query->where([
                        'sender_id' => $userId,
                        'receiver_id' => $user->id,
                    ]);
                });
            })->whereNull('deleted_at')
                ->orderByDesc('created_at')
                ->paginate($this->messagesPerPage);

            $result = [
                'current' => $messages->currentPage(),
                'last' => $messages->lastPage(),
                'messages' => $messages->getCollection()->toArray(),
            ];

            return response()->json($result);
        }

        abort(404);
    }

    /**
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function getUserInfo($userId)
    {
        if (request()->ajax()) {
            $user = User::find($userId);
            if ($user) {
                $user = [
                    'login' => $user->login,
                    'name' => $user->getFullName(),
                    'country' => Languages::getCountry($user->addon('country')),
                    'avatar' => $user->getAvatar(),
                    'msgPage' => 0,
                    'msgLastPage' => null,
                    'newMessages' => 0,
                ];
            } else {
                $user = null;
            }

            return response()->json($user);
        }

        abort(404);
    }
}
