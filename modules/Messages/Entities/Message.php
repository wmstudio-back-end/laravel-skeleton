<?php

namespace Modules\Messages\Entities;

use Reliese\Database\Eloquent\Model as Eloquent;
use Modules\Users\Entities\User;

/**
 * Class Menu
 *
 * @property int $id
 * @property int $sender_id
 * @property int $receiver_id
 * @property string $message
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $children
 */
class Message extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'sender_id' => 'int',
        'receiver_id' => 'int',
        'status' => 'int',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

	protected $fillable = [
        'sender_id',
        'receiver_id',
        'message',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    public function toArray($full = false)
    {
        $result = [
            'id' => $this->id,
            'sender_id' => $this->sender_id,
            'receiver_id' => $this->receiver_id,
            'message' => $this->message,
            'status' => $this->status,
            'created_at' => $this->created_at->format('Y/m/d H:i:s'),
        ];

        if ($full) {
            $result['updated_at'] = $this->updated_at->format('Y/m/d H:i:s');
            $result['deleted_at'] = $this->deleted_at->format('Y/m/d H:i:s');
        }

        return $result;
    }
}
