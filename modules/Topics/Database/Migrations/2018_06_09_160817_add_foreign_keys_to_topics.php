<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics_tags', function(Blueprint $table) {
            $table->foreign('group_id', 'topics_tags_ibfk_1')->references('id')->on('topics_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
        Schema::table('topics', function(Blueprint $table) {
            $table->foreign('tag_id', 'topics_ibfk_1')->references('id')->on('topics_tags')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'topics_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function(Blueprint $table) {
            $table->dropForeign('topics_ibfk_2');
            $table->dropForeign('topics_ibfk_1');
        });
        Schema::table('topics_tags', function(Blueprint $table) {
            $table->dropForeign('topics_tags_ibfk_1');
        });
    }
}
