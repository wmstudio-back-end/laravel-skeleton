<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Users\Entities\UserPermission;
use Modules\Topics\Entities\TopicsGroup;

class CreateTopicsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics_groups', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('alias')->unique('alias');
            $table->string('name');
            $table->boolean('active');
            $table->integer('weight');
            $table->timestamps();
        });

        Schema::create('topics_tags', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('group_id')->index('group_id');
            $table->string('alias')->unique('alias');
            $table->string('name');
            $table->boolean('active');
            $table->integer('weight');
            $table->timestamps();
        });

        Schema::create('topics', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('tag_id')->index('tag_id');
            $table->integer('user_id')->index('user_id');
            $table->string('name');
            $table->string('image');
            $table->smallInteger('difficulty');
            $table->smallInteger('type');
            $table->text('content');
            $table->boolean('active');
            $table->integer('weight');
            $table->json('users');
            $table->timestamp('moderated_at')->nullable();
            $table->timestamps();
        });

        $hasPermissions = UserPermission::where([
            'link_to' => UserPermission::LINK_GROUP,
            'parent_id' => 1,
            'route_name' => 'admin/topics',
        ])->count('id');
        if (!$hasPermissions) {
            UserPermission::create([
                'link_to' => UserPermission::LINK_GROUP,
                'parent_id' => 1,
                'route_name' => 'admin/topics',
                'permission' => UserPermission::P_WRITE,
            ]);
        }

        if (env('APP_ENV', 'local') != 'production') {
            $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
            TopicsGroup::insert([
                [
                    'alias'      => 'promote_care',
                    'name'       => 'Продвинуть свою карьеру',
                    'active'     => true,
                    'weight'     => 2,
                    'created_at' => $timeStamp,
                    'updated_at' => $timeStamp,
                ],
                [
                    'alias'      => 'start_conversationer',
                    'name'       => 'Начать беседу',
                    'active'     => true,
                    'weight'     => 1,
                    'created_at' => $timeStamp,
                    'updated_at' => $timeStamp,
                ],
                [
                    'alias'      => 'prepare_for_exam',
                    'name'       => 'Подготовиться к экзамену',
                    'active'     => true,
                    'weight'     => 3,
                    'created_at' => $timeStamp,
                    'updated_at' => $timeStamp,
                ],
                [
                    'alias'      => 'improve_lang',
                    'name'       => 'Улучшить свой Адыгский',
                    'active'     => true,
                    'weight'     => 4,
                    'created_at' => $timeStamp,
                    'updated_at' => $timeStamp,
                ],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
        Schema::dropIfExists('topics_tags');
        Schema::dropIfExists('topics_groups');
    }
}
