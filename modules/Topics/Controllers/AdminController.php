<?php

namespace Modules\Topics\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\TranslationManager\Models\Translation;
use Modules\Topics\Entities\Topic;
use Modules\Topics\Entities\TopicsGroup;
use Modules\Topics\Entities\TopicsTag;
use Modules\Users\Entities\User;

class AdminController extends Controller
{
    public static function writeRequired()
    {
        return [
            'groupsEdit',
            'tagsEdit',
            'topicEdit',
        ];
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = TopicsGroup::paginate($this->adminEntitiesPerPage);

        return view('topics::admin.index', [
            'groups' => $groups,
        ]);
    }

    /**
     * @param $action
     * @param null $group
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function groupsEdit($action, $group = null)
    {
        if ($action === 'delete' && $group !== null && ($group = TopicsGroup::find($group))) {
            $group->delete();
            return redirect()->back();
        }

        switch ($action) {
            case 'add':
                $group = new TopicsGroup();
                $group->weight = TopicsGroup::all()->max('weight') + 1;
                break;
            case 'edit':
                $group = TopicsGroup::find($group);
                break;
            default:
                abort(404);
                break;
        }
        if ($group) {
            $locales = $this->transManager->getLocales();
            $defLang = config('app.locale');

            $keys = Translation::where([
                'group' => TopicsGroup::TRANS_GROUP,
                'key' => TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias,
            ])->get();
            $translations = [];
            foreach($keys as $translation){
                /* @var $translation Translation */
                $translations[$translation->locale] = $translation;
            }
            if (count($translations) != count($locales)) {
                foreach ($locales as $lang) {
                    if (!isset($translations[$lang])) {
                        $translations[$lang] = new Translation();
                    }
                }
            }
            if (request()->isMethod('POST')) {
                $data = getRequest();

                $data['alias'] = str_slug($data['alias']);
                $validator = \Validator::make($data, [
                    'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $group->alias ? '|unique:topics_groups' : ''),
                    'active' => 'bool|required',
                    'group-name-' . $defLang => 'required|string',
                ]);

                if (!$validator->fails()) {
                    $group->alias = $data['alias'];
                    $group->name = $data['group-name-' . $defLang];
                    $group->active = $data['active'];
                    $group->save();

                    Translation::unguard();
                    foreach ($translations as $lang => $translation) {
                        if (array_key_exists('group-name-' . $lang, $data)) {
                            $translation->status = Translation::STATUS_CHANGED;
                            $translation->locale = $lang;
                            $translation->group = TopicsGroup::TRANS_GROUP;
                            $translation->key = TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias;
                            $translation->value = $data['group-name-' . $lang];
                            $translation->save();
                        } else {
                            $translation->delete();
                        }
                    }
                    Translation::reguard();

                    return redirect()->route('admin.topics');
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            } else {
                $edit = ($action === 'edit');
                $menu = getMenu('admin');
                $menu->findBy('route', 'admin.topics')->setActive();
                $menu->lastLevel = $edit ? 'Редактировть' : 'Создать';

                return view('topics::admin.group-edit', [
                    'edit' => $edit,
                    'group' => $group,
                    'locales' => $locales,
                    'defLang' => $defLang,
                    'translations' => $translations,
                ]);
            }
        }

        abort(404);
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function groupsSort()
    {
        return $this->sorting(TopicsGroup::class, 'admin.topics');
    }

    /**
     * @param int $group
     * @return \Illuminate\Http\Response
     */
    public function tags($group)
    {
        $group = TopicsGroup::find($group);
        if ($group) {
            $tags = TopicsTag::where('group_id', $group->id)->paginate($this->adminEntitiesPerPage);

            $menu = getMenu('admin');
            $menu->findBy('route', 'admin.topics')->setActive();
            $menu->lastLevel = $group->name;

            return view('topics::admin.tags', [
                'group' => $group->id,
                'tags' => $tags,
            ]);
        }

        abort(404);
    }

    /**
     * @param int $group
     * @return \Illuminate\Http\Response
     */
    public function tagsSort($group)
    {
        return $this->sorting(TopicsTag::class, route('admin.topics.tags', ['group' => $group]));
    }

    /**
     * @param int $group
     * @param string $action
     * @param int|null $tag
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function tagEdit($group, $action, $tag = null)
    {
        $group = TopicsGroup::find($group);
        if ($group) {
            if ($action === 'delete' && $tag !== null && ($tag = TopicsTag::where(['id' => $tag, 'group_id' => $group->id])->first())) {
                $tag->delete();
                return redirect()->back();
            }

            switch ($action) {
                case 'add':
                    $tag = new TopicsTag();
                    $tag->group_id = $group->id;
                    $tag->weight = TopicsTag::where('group_id', $group->id)->max('weight') + 1;
                    break;
                case 'edit':
                    $tag = TopicsTag::where(['id' => $tag, 'group_id' => $group->id])->first();
                    break;
                default:
                    abort(404);
                    break;
            }
            if ($tag) {
                $locales = $this->transManager->getLocales();
                $defLang = config('app.locale');

                $keys = Translation::where([
                    'group' => TopicsTag::TRANS_GROUP,
                    'key' => TopicsTag::TRANS_KEY_PREFIX . '.' . $tag->alias,
                ])->get();
                $translations = [];
                foreach($keys as $translation){
                    /* @var $translation Translation */
                    $translations[$translation->locale] = $translation;
                }
                if (count($translations) != count($locales)) {
                    foreach ($locales as $lang) {
                        if (!isset($translations[$lang])) {
                            $translations[$lang] = new Translation();
                        }
                    }
                }
                if (request()->isMethod('POST')) {
                    $data = getRequest();

                    $data['alias'] = str_slug($data['alias']);
                    $validator = \Validator::make($data, [
                        'alias' => 'string|required|min:4|max:255' . ($data['alias'] != $tag->alias ? '|unique:topics_tags' : ''),
                        'active' => 'bool|required',
                        'tag-name-' . $defLang => 'required|string',
                    ]);

                    if (!$validator->fails()) {
                        $tag->alias = $data['alias'];
                        $tag->name = $data['tag-name-' . $defLang];
                        $tag->active = $data['active'];
                        $tag->save();

                        Translation::unguard();
                        foreach ($translations as $lang => $translation) {
                            if (array_key_exists('tag-name-' . $lang, $data)) {
                                $translation->status = Translation::STATUS_CHANGED;
                                $translation->locale = $lang;
                                $translation->group = TopicsTag::TRANS_GROUP;
                                $translation->key = TopicsTag::TRANS_KEY_PREFIX . '.' . $tag->alias;
                                $translation->value = $data['tag-name-' . $lang];
                                $translation->save();
                            } else {
                                $translation->delete();
                            }
                        }
                        Translation::reguard();

                        return redirect()->route('admin.topics.tags', ['group' => $group->id]);
                    } else {
                        return redirect()->back()->withErrors($validator)->withInput();
                    }
                } else {
                    $edit = ($action === 'edit');
                    $menu = getMenu('admin');
                    $menu->findBy('route', 'admin.topics')->setActive();
                    $menu->lastLevel = [
                        [
                            'label' => $group->name,
                            'uri' => route('admin.topics.tags', ['group' => $group->id]),
                        ],
                        [
                            'label' => ($edit ? 'Редактировть' : 'Создать') . ' тему',
                        ],
                    ];

                    return view('topics::admin.tag-edit', [
                        'edit' => $edit,
                        'group' => $group,
                        'tag' => $tag,
                        'locales' => $locales,
                        'defLang' => $defLang,
                        'translations' => $translations,
                    ]);
                }
            }
        } else {
            abort(404);
        }
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function topics()
    {
        if (request()->ajax()) {
            if ($search = request()->get('search', null)) {
                $search = explode(' ', $search);
                try {
                    $users = User::where(function($query) use($search) {
                        $copySearch = $search;
                        $query->where('login', 'like', '%' . array_shift($copySearch) . '%');
                        foreach ($copySearch as $val) {
                            $query->orWhere('login', 'like', '%' . $val . '%');
                        }
                        foreach ($search as $val) {
                            $query->orWhere('email', 'like', '%' . $val . '%');
                        }
                        $query->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'name');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        })->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'surname');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        })->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'middle_name');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        })->orWhereHas('user_add_values', function($query) use($search) {
                            $query->whereHas('user_addon', function($query) use($search) {
                                $query->where('alias', 'phone');
                            })->where(function($query) use($search) {
                                $query->where('value', 'like', '%' . array_shift($search) . '%');
                                foreach ($search as $val) {
                                    $query->orWhere('value', 'like', '%' . $val . '%');
                                }
                            });
                        });
                    });

                    if ($ignore = request()->get('ignore', null)) {
                        $users->where('id', '!=', $ignore);
                    }
                    $users = $users->get();

                    $result = [];
                    foreach ($users as $user) {
                        /* @var $user User */
                        $result[] = [
                            'id' => $user->id,
                            'text' => $user->getFullName(),
                        ];
                    }

                    return response()->json(['list' => $result]);
                } catch (\Exception $e) {
                    return response()->json(['error' => $e->getMessage()]);
                }
            } else {
                return response()->json(['error' => 'Пустая строка поиска']);
            }
        } else {
            $group = request()->get('group', null);
            $tag = request()->get('tag', null);
            $user = request()->get('user', null);

            $groups = TopicsGroup::all()->keyBy('id');
            if (!$group) {
                $group = $groups->first();
                if ($group) {
                    $group = $group->id;
                }
            } else {
                $group = $groups->find($group);
                if ($group) {
                    $group = $group->id;
                }
            }

            if ($tag) {
                $tagIds = TopicsTag::find($tag, ['id']);
                if ($tagIds) {
                    $tagIds = $tagIds->toArray();
                }
            }
            if (!isset($tagIds) || !$tagIds) {
                $tagIds = TopicsTag::select('id');
                if ($group) {
                    $tagIds->where('group_id', $group);
                }
                $tagIds = $tagIds->get()->keyBy('id')->keys()->toArray();
            }

            $topics = Topic::whereIn('tag_id', $tagIds);
            if ($user) {
                $topics->where('user_id', $user);
            }

            $topics = $topics->paginate($this->adminEntitiesPerPage);
            $userObj = User::find($user);

            $menu = getMenu('admin');
            $menu->lastLevel = [];
            if ($group && isset($groups[$group])) {
                array_push($menu->lastLevel, [
                    'label' => $groups[$group]->name,
                    'uri' => route('admin.topics.list', ['group' => $group]),
                ]);

                if ($tag && isset($groups[$group]) && ($tagObj = $groups[$group]->tags->find($tag))) {
                    array_push($menu->lastLevel, [
                        'label' => 'Тема: ' . $tagObj->name,
                        'uri' => route('admin.topics.list', ['group' => $group, 'tag' => $tag]),
                    ]);
                }
            }
            if ($user) {
                array_push($menu->lastLevel, [
                    'label' => 'Пользователь: ' . $userObj->getFullName(),
                    'uri' => route('admin.topics.list', ['group' => $group, 'user' => $user]),
                ]);
            }

            return view('topics::admin.topics', [
                'topics' => $topics,
                'groups' => $groups,
                'selected' => [
                    'group' => $group,
                    'tag' => $tag,
                    'user' => $userObj,
                ],
            ]);
        }
    }

    /**
     * @param string $action
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function topicEdit($action, $id)
    {
        $topic = Topic::find($id);
        if ($topic) {
            if ($action === 'delete' && request()->isMethod('POST')) {
                $topic->delete();
                return redirect()->back();
            } elseif ($action === 'edit') {
                if (request()->isMethod('POST')) {
                    $data = getRequest();

                    $validator = [
                        'topic-tag'         => 'required|integer',
                        'topic-name'        => 'required|string|min:4',
                        'topic-image'       => 'required|string',
                        'topic-difficulty'  => 'required|integer|min:1|max:5',
                        'topic-type'        => 'required|integer|in:' . Topic::TYPE_LINK . ',' . Topic::TYPE_CONTENT,
                        'topic-users'       => 'nullable|array',
                        'topic-active'      => 'required|boolean'
                    ];
                    if ($data['topic-type'] === Topic::TYPE_LINK) {
                        $validator['topic-link'] = 'required|string';
                    }
                    if ($data['topic-type'] === Topic::TYPE_CONTENT) {
                        $validator['topic-content'] = 'required|string';
                    }

                    $validator = \Validator::make($data, $validator);

                    if (!$validator->fails()) {
                        $topic->tag_id = $data['topic-tag'];
                        $topic->name = $data['topic-name'];
                        $topic->image = $data['topic-image'];
                        $topic->difficulty = $data['topic-difficulty'];
                        $topic->type = $data['topic-type'];
                        if ($data['topic-type'] == Topic::TYPE_LINK) {
                            $topic->content = $data['topic-link'];
                        } else {
                            $topic->content = $data['topic-content'];
                        }
                        $topic->users = $data['topic-users'];
                        $topic->active = $data['topic-active'];
                        $topic->save();

                        return redirect()->route('admin.topics.list', ['group' => $topic->tag->group->id, 'tag' => $topic->tag->id, 'user' => $topic->user_id]);
                    } else {
                        return redirect()->back()->withErrors($validator)->withInput();
                    }
                }

                $groups = TopicsGroup::all();
                $menu = getMenu('admin');
                $menu->findBy('route', 'admin.topics.list')->setActive();
                $menu->lastLevel = 'Редактирование темы';

                return view('topics::admin.topic-edit', [
                    'topic' => $topic,
                    'groups' => $groups,
                ]);
            }
        }

        abort(404);
    }
}
