<?php

$name = 'Темы';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/',
                'parameters' => [
                    'as' => 'admin.topics',
                    'label' => 'Просмотр групп тем',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'get',
                'uri' => '/group/{group}',
                'parameters' => [
                    'as' => 'admin.topics.tags',
                    'label' => 'Просмотр тегов группы тем',
                    'uses' => 'AdminController@tags',
                ],
            ],

            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/topics',
                'parameters' => [
                    'as' => 'admin.topics.list',
                    'label' => 'Просмотр тем, созданных пользователями',
                    'uses' => 'AdminController@topics',
                ],
            ],
            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/topic/{action}/{id}',
                'parameters' => [
                    'as' => 'admin.topics.list.edit',
                    'label' => 'Редактирование и удаление тем, созданных пользователями',
                    'uses' => 'AdminController@topicEdit',
                ],
                'where' => ['action' => 'edit|delete'],
            ],

            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/groups/{action}/{group?}',
                'parameters' => [
                    'as' => 'admin.topics.group.edit',
                    'label' => 'Добавление, редактирование и удаление групп тем',
                    'uses' => 'AdminController@groupsEdit',
                ],
                'where' => ['action' => 'add|edit|delete']
            ],
            [
                'method' => 'post',
                'uri' => '/groups/sort',
                'parameters' => [
                    'as' => 'admin.topics.groups.sort',
                    'label' => 'Сортировка групп тем',
                    'uses' => 'AdminController@groupsSort',
                ],
            ],

            [
                'method' => 'match',
                'match' => ['get', 'post'],
                'uri' => '/group/{group}/{action}-tag/{tag?}',
                'parameters' => [
                    'as' => 'admin.topics.tag.edit',
                    'label' => 'Добавление, редактирование и удаление тегов в группе тем',
                    'uses' => 'AdminController@tagEdit',
                ],
                'where' => ['action' => 'add|edit|delete'],
            ],
            [
                'method' => 'post',
                'uri' => '/group/{group}/sort-tags',
                'parameters' => [
                    'as' => 'admin.topics.tags.sort',
                    'label' => 'Сортировка тегов в группе тем',
                    'uses' => 'AdminController@tagsSort',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => $name,
                'uri'   => '#',
                'icon'  => 'fa fa-newspaper-o',
                'pages' => [
                    [
                        'label' => 'Группы тем',
                        'route' => 'admin.topics',
                        'icon'  => 'fa fa-object-group',
                    ],
                    [
                        'label' => 'Список тем',
                        'route' => 'admin.topics.list',
                        'icon'  => 'fa fa-th-list',
                    ],
                ],
                'order' => 60,
            ],
        ],
    ],
];
