<?php /* @var $group \Modules\Topics\Entities\TopicsGroup */ ?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>{{ $edit ? 'Редактирование' : 'Создание' }} группы</h4>
                </div>
                <div class="card-body">
                    <form action="{{ $edit
                            ? route('admin.topics.group.edit', ['action' => 'edit', 'group' => $group->id])
                            : route('admin.topics.group.edit', ['action' => 'add'])
                    }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="alias" class="col-md-2 col-form-label required-label">Указатель</label>
                            <div class="col-md-10">
                                <input
                                        id="alias"
                                        name="alias"
                                        type="text"
                                        class="form-control{{ $errors->has('alias') ? ' is-invalid' : '' }}"
                                        value="{{ old('alias') ?: $group->alias }}"
                                        placeholder="Нужен для переводов (alias)"
                                        required="required"
                                >
                                @if ($errors->has('alias'))
                                    <div class="invalid-feedback">{{ str_replace('alias ', '', $errors->first('alias')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Текст</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    @foreach($locales as $lang)
                                        <li class="nav-item">
                                            <a class="nav-link{{ $defLang == $lang ? ' active' : '' }}"
                                               id="lang-name-{{ $lang }}-tab" data-toggle="tab" href="#lang-name-{{ $lang }}"
                                               role="tab" aria-controls="lang-name-{{ $lang }}"
                                               aria-selected="true">{{ $lang }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    @foreach($locales as $lang)
                                        <div class="tab-pane fade{{ $defLang == $lang ? ' show active' : '' }}"
                                             id="lang-name-{{ $lang }}" role="tabpanel" aria-labelledby="lang-name-tab">
                                            <input
                                                    name="group-name-{{ $lang }}"
                                                    type="text"
                                                    class="form-control{{ $errors->has('group-name-' . $lang) ? ' is-invalid' : '' }}"
                                                    value="{{ old('group-name-' . $lang) ?:
                                                        $translations[$lang]->value ?: ($defLang == $lang ? $group->name : '')
                                                    }}"
                                                    @if($lang === $defLang)required="required"@endif
                                            >
                                        </div>
                                        @if ($errors->has('group-name-' . $lang))
                                            <div class="invalid-feedback">{{ str_replace("group name $lang ", '', $errors->first('group-name-' . $lang)) }}</div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-1" value="active">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="active"
                                            name="active"
                                            value="1"
                                            {{ old('active', null) !== null
                                                ? (old('active') ? 'checked="checked"' : '')
                                                : $group->active ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection