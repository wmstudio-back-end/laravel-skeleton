<?php $canEdit = getPermissions('admin.topics.list.edit'); ?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список тем</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-md-12 col-lg-4">
                            <label for="topic-group">Группа</label>
                            <select
                                    id="topic-group"
                                    name="topic-group"
                                    class="form-control selectpicker topic-filter"
                                    @if($groups->count() < 10)data-live-search="false"@endif
                            >
                                @if ($groups->count())
                                    @foreach ($groups as $group)
                                        <?php /* @var $group \Modules\Topics\Entities\TopicsGroup */ ?>
                                        <option
                                                value="{{ $group->id }}"
                                                @if($selected['group'] == $group->id)selected="selected"@endif
                                        >{{ $group->name }}</option>
                                    @endforeach
                                @else
                                    <option value="" selected="selected">Групп нет</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-lg-4">
                            <label for="topic-tag">Тема</label>
                            <select
                                    id="topic-tag"
                                    name="topic-tag"
                                    class="form-control selectpicker topic-filter"
                                    @if($groups->count() && $groups[$selected['group']]->tags->count() < 10)data-live-search="false"@endif
                            >
                                @if ($groups->count() && $groups[$selected['group']]->tags->count())
                                    <option value="" @if(!$selected['tag'])selected="selected"@endif>Нет</option>
                                    @foreach ($groups[$selected['group']]->tags as $tag)
                                        <?php /* @var $tag \Modules\Topics\Entities\TopicsTag */ ?>
                                        <option
                                                value="{{ $tag->id }}"
                                                @if($selected['tag'] == $tag->id)selected="selected"@endif
                                        >{{ $tag->name }}</option>
                                    @endforeach
                                @else
                                    <option value="" selected="selected">Тем нет</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-lg-4">
                            <label for="topic-user">Пользователь</label>
                            <select
                                    id="topic-user"
                                    name="topic-user"
                                    class="form-control selectpicker ajax-select topic-filter"
                                    data-action="{{ route('admin.topics.list') }}"
                                    data-token="{{ csrf_token() }}"
                                    data-minimum-input="5"
                                    data-placeholder="Нет"
                                    data-allow-clear="true"
                            >
                                @if($selected['user'])
                                    <option value="{{ $selected['user']->id }}" selected="selected" >{{ $selected['user']->getFullName() }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        @if($topics->count())
                            @foreach($topics as $topic)
                                <?php /* @var $topic \Modules\Topics\Entities\Topic */ ?>
                                <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xxl-2">
                                    <div class="card animated fadeInUp">
                                        <div class="card-header topic-header">
                                            @if($selected['tag'])
                                                <h4>{{ $topic->name }}</h4>
                                            @else
                                                <h4>{{ $topic->tag->name }}</h4>
                                            @endif
                                            <div class="dropdown pull-right">
                                                <button
                                                    class="btn btn-action dropdown-toggle"
                                                    type="button"
                                                    data-toggle="dropdown"
                                                ></button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a
                                                        class="dropdown-item topic-edit"
                                                        href="{{ route('admin.topics.list.edit', ['action' => 'edit', 'id' => $topic->id]) }}"
                                                    >Редактировать</a>
                                                    <form
                                                        action="{{ route('admin.topics.list.edit', ['action' => 'delete', 'id' => $topic->id]) }}"
                                                        method="post"
                                                    >
                                                        @csrf
                                                        <button class="dropdown-item topic-delete">Удалить</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            @if(!$selected['tag'])
                                                <p class="text-center">{{ $topic->name }}</p>
                                            @endif
                                            <div class="topic-img-container">
                                                <img src="{{ url($topic->image) }}" class="topic-img">
                                            </div>
                                            <?php
                                                $difficulty = '';
                                                $delimiter = '-';
                                                for ($i = 0; $i < $topic->difficulty; $i++) {
                                                    $difficulty .= '&#11201;' . $delimiter;
                                                }
                                                if ($len = strlen($difficulty)) {
                                                    $difficulty = substr($difficulty, 0, $len - strlen($delimiter));
                                                }
                                            ?>
                                            <small class="badge badge-warning mt-2">
                                                Сложность: {!! $difficulty !!}
                                            </small>
                                        </div>
                                        <div class="card-footer text-right">
                                            @if(!$selected['user'])
                                                <div>
                                                    @if(getPermissions('admin.users.show'))
                                                        <a
                                                            href="{{ route('admin.users.show', ['id' => $topic->user->id]) }}"
                                                            class="badge btn-block badge-info mb-1"
                                                        >{{ $topic->user->getFullName() }}</a>
                                                    @else
                                                        <small>{{ $topic->user->getFullName() }}</small>
                                                    @endif
                                                </div>
                                            @endif
                                            @if($topic->moderated_at)
                                                <div><small class="badge btn-block badge-warning mb-1">Модерировано: {{ $topic->moderated_at->format('d.m.Y H:i') }}</small></div>
                                            @endif
                                            <div><small class="badge btn-block badge-info mb-1">Изменено: {{ $topic->updated_at->format('d.m.Y H:i') }}</small></div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="col-12">
                                <center><h4>Темы не найдены</h4></center>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $topics->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection