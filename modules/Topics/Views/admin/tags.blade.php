<?php
$canSort     = getPermissions('admin.topics.tags.sort');
$canEdit     = getPermissions('admin.topics.tag.edit');
?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Список тем</h4>
                    @if ($canEdit)
                        <a href="{{ route('admin.topics.tag.edit', ['group' => $group, 'action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            @if($tags->count())
                                <thead>
                                <tr>
                                    @if($canSort)
                                        <th class="text-center"><i class="ion ion-ios-keypad"></i></th>
                                    @endif
                                    <th>Имя</th>
                                    <th>Статус</th>
                                    @if($canEdit || $canSort)
                                        <th class="text-right">Действия</th>
                                    @endif
                                </tr>
                                </thead>
                            @endif
                            <tbody
                                    id="menu-sortable"
                                    class="sortable-with-handle"
                                    @if($canSort)
                                        data-action="{{ route('admin.topics.tags.sort', ['group' => $group, 'page' => $tags->currentPage()]) }}"
                                        data-token="{{ csrf_token() }}"
                                    @endif
                            >
                            @if($tags->count())
                                @foreach($tags as $tag)
                                    <?php /* @var $tag \Modules\Topics\Entities\TopicsTag */ ?>
                                    <tr data-id="{{ $tag->id }}">
                                        @if($canSort)
                                            <td class="text-center">
                                                <div class="sort-handler">
                                                    <i class="ion ion-ios-keypad"></i>
                                                </div>
                                            </td>
                                        @endif
                                        <td>
                                            @if($canEdit)
                                                <a href="{{ route('admin.topics.tag.edit', ['group' => $group, 'action' => 'edit', 'tag' => $tag->id]) }}">
                                                    {{ $tag->name }}
                                                </a>
                                            @else
                                                <p>{{ $tag->name }}</p>
                                            @endif
                                        </td>
                                        <td>
                                            <? if ($tag->active) {
                                                $status = 'Активна';
                                                $activeClass = 'badge-success';
                                            } else {
                                                $status = 'Не активна';
                                                $activeClass = 'badge-danger';
                                            }
                                            ?>
                                            <div class="badge {{ $activeClass }}">{{ $status }}</div>
                                        </td>
                                        @if($canEdit || $canSort)
                                            <td class="text-right">
                                                @if($canSort)
                                                    <form
                                                            action="{{ route('admin.topics.tags.sort', ['group' => $group]) }}"
                                                            method="post"
                                                            class="inline-form after-sort-hide-first"
                                                            @if($tag->weight < 2)style="display: none"@endif
                                                    >
                                                        @csrf
                                                        <button type="submit" name="up" value="{{ $tag->id }}" class="btn btn-action btn-info">
                                                            <i class="fa fa-arrow-up"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                @if($canEdit)
                                                    <a href="{{ route('admin.topics.tag.edit', ['group' => $group, 'action' => 'edit', 'tag' => $tag->id]) }}"
                                                       class="btn btn-action btn-primary">Редактировать</a>
                                                    <form
                                                        action="{{ route('admin.topics.tag.edit', ['group' => $group, 'action' => 'delete', 'tag' => $tag->id]) }}"
                                                        method="post"
                                                        class="inline-form"
                                                    >
                                                        @csrf
                                                        <button type="submit" class="btn btn-action btn-danger">Удалить</button>
                                                    </form>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr><td class="text-center">Ни одной темы пока не созданно.</td></tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-left">
                        {{ $tags->links() }}
                    </div>
                    @if ($canEdit)
                        <a href="{{ route('admin.topics.tag.edit', ['group' => $group, 'action' => 'add']) }}"
                           class="btn btn-success pull-right"><i class="fa fa-plus"></i> Добавить</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection