<?php /* @var $topic \Modules\Topics\Entities\Topic */ ?>
@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-header">
                    <h4>Редактирование темы</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.topics.list.edit', ['action' => 'edit', 'id' => $topic->id]) }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Создатель темы</label>
                            <div class="col-md-10">
                                <p class="form-control-static">{{ $topic->user->getFullName() }}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="topic-tag" class="col-md-2 col-form-label required-label">Тема</label>
                            <div class="col-md-10">
                                <select
                                    id="topic-tag"
                                    name="topic-tag"
                                    class="form-control selectpicker"
                                >
                                    @if ($groups->count())
                                        @foreach ($groups as $group)
                                            <?php /* @var $group \Modules\Topics\Entities\TopicsGroup */ ?>
                                            <optgroup label="{{ $group->name }}">
                                                @foreach($group->tags as $tag)
                                                    <?php /* @var $tag \Modules\Topics\Entities\TopicsTag */ ?>
                                                    <option
                                                            value="{{ $tag->id }}"
                                                            @if($topic->tag_id === $tag->id)selected="selected"@endif
                                                    >{{ $tag->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    @else
                                        <option value="" selected="selected">Групп нет</option>
                                    @endif
                                </select>
                                @if ($errors->has('topic-tag'))
                                    <div class="invalid-feedback">{{ str_replace('topic tag ', '', $errors->first('topic-tag')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="topic-name" class="col-md-2 col-form-label required-label">Заголовок</label>
                            <div class="col-md-10">
                                <input
                                    id="topic-name"
                                    name="topic-name"
                                    type="text"
                                    class="form-control{{ $errors->has('topic-name') ? ' is-invalid' : '' }}"
                                    value="{{ old('topic-name') ?: $topic->name }}"
                                    placeholder="Заголовок"
                                    required="required"
                                >
                                @if ($errors->has('topic-name'))
                                    <div class="invalid-feedback">{{ str_replace('topic name ', '', $errors->first('topic-name')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="topic-image" class="col-md-2 col-form-label required-label">Изображение</label>
                            <div class="col-md-10">
                                <input
                                    id="topic-image"
                                    name="topic-image"
                                    type="text"
                                    class="form-control{{ $errors->has('topic-image') ? ' is-invalid' : '' }}"
                                    value="{{ old('topic-image') ?: $topic->image }}"
                                    placeholder="URL Изображения"
                                    required="required"
                                >
                                @if ($errors->has('topic-image'))
                                    <div class="invalid-feedback">{{ str_replace('topic image ', '', $errors->first('topic-image')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="topic-difficulty" class="col-md-2 col-form-label required-label">Сложность</label>
                            <div class="col-1">
                                <input
                                    id="topic-difficulty"
                                    name="topic-difficulty"
                                    min="1" max="5"
                                    type="number"
                                    class="form-control{{ $errors->has('topic-difficulty') ? ' is-invalid' : '' }}"
                                    value="{{ old('topic-difficulty') ?: $topic->difficulty ?: 1 }}"
                                    placeholder="Сложность (1-5)"
                                    required="required"
                                />
                                @if ($errors->has('topic-difficulty'))
                                    <div class="invalid-feedback">{{ str_replace('topic difficulty ', '', $errors->first('topic-difficulty')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label required-label">Содержание</label>
                            <div class="col-md-10">
                                <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                                    <?php $currentType = $topic->type ?: \Modules\Topics\Entities\Topic::TYPE_LINK; ?>
                                    @foreach(\Modules\Topics\Entities\Topic::TYPES as $type => $label)
                                        <li class="nav-item">
                                            <a class="nav-link topic-type-select{{ $currentType == $type ? ' active' : '' }}"
                                                id="topic-type-{{ $type }}-tab" data-toggle="tab" href="#topic-type-{{ $type }}"
                                                role="tab" aria-controls="topic-type-{{ $type }}"
                                                aria-selected="true"
                                            >
                                                <input type="radio" name="topic-type" value="{{ $type }}"{{
                                                    $currentType == $type ? ' checked="checked"' : ''
                                                }} class="hidden">{{ $label }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content tab-bordered collapse show" id="groupsTabContent">
                                    @foreach(\Modules\Topics\Entities\Topic::TYPES as $type => $label)
                                        <div class="tab-pane fade{{ ($topic->type && $topic->type == $type)
                                                || (!$topic->type && \Modules\Topics\Entities\Topic::TYPE_LINK == $type)
                                                    ? ' show active' : '' }}"
                                            id="topic-type-{{ $type }}"
                                            role="tabpanel"
                                            aria-labelledby="lang-name-tab"
                                        >
                                            @if($type == \Modules\Topics\Entities\Topic::TYPE_LINK)
                                                <input
                                                    name="topic-link"
                                                    type="text"
                                                    class="form-control{{ $errors->has('topic-content') ? ' is-invalid' : '' }}"
                                                    value="{{ $topic->type == $type ? (old('topic-content') ?: $topic->content) : '' }}"
                                                    required="required"
                                                    @if($type != $topic->type)readonly="readonly"@endif
                                                >
                                            @else
                                                <textarea
                                                    name="topic-content"
                                                    rows="3"
                                                    class="form-control{{ $errors->has('topic-content') ? ' is-invalid' : '' }}"
                                                    @if($type != $topic->type)readonly="readonly"@endif
                                                    required="required"
                                                >{{ $topic->type == $type ? (old('topic-content') ?: $topic->content) : '' }}</textarea>
                                            @endif
                                        </div>
                                    @endforeach
                                    @if ($errors->has('topic-link'))
                                        <div class="invalid-feedback">{{ str_replace('topic link ', '', $errors->first('topic-link')) }}</div>
                                    @endif
                                    @if ($errors->has('topic-content'))
                                        <div class="invalid-feedback">{{ str_replace('topic content ', '', $errors->first('topic-content')) }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="topic-users" class="col-md-2 col-form-label">Пользователи</label>
                            <div class="col-md-10">
                                <select
                                    id="topic-users"
                                    name="topic-users[]"
                                    class="form-control selectpicker ajax-select{{ $errors->has('topic-users') ? ' is-invalid' : '' }}"
                                    data-class="multiple-info"
                                    data-live-search="true"
                                    multiple="multiple"
                                    data-action="{{ route('admin.topics.list') }}"
                                    data-token="{{ csrf_token() }}"
                                    data-minimum-input="5"
                                    data-placeholder="Нет"
                                    data-extra-name="ignore"
                                    data-extra-value="{{ $topic->user_id }}"
                                >
                                    <?php $old = old('topic-users', null); ?>
                                    @foreach (\Modules\Users\Entities\User::whereIn('id', $topic->users)->get() as $user)
                                        <option
                                                value="{{ $user->id }}"
                                                {{ (is_array($old) ? (in_array($user->id, $old) ? ' selected="selected"' : '')
                                                    : (in_array($user->id, $topic->users) ? ' selected="selected"' : '')) }}
                                        >{{ $user->getFullName() }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('topic-users'))
                                    <div class="invalid-feedback">{{ str_replace('topic users ', '', $errors->first('topic-users')) }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="checkbox-1" value="topic-active">
                                    <input
                                            type="checkbox"
                                            class="custom-control-input"
                                            id="topic-active"
                                            name="topic-active"
                                            value="1"
                                            {{ old('topic-active', null) !== null
                                                ? (old('topic-active') ? 'checked="checked"' : '')
                                                : $tag->active ? 'checked="checked"' : ''
                                            }}
                                    >
                                    <label class="custom-control-label" for="topic-active">Активность</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-4 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    <span class="fa fa-check"></span>
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection