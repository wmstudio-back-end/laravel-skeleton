<?php

namespace Modules\Topics\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;
use Modules\Users\Entities\User;

/**
 * Class Topic
 * 
 * @property int $id
 * @property int $tag_id
 * @property int $user_id
 * @property string $name
 * @property string image
 * @property int $difficulty
 * @property int $type
 * @property string $content
 * @property bool $active
 * @property int $weight
 * @property array $users
 * @property \Carbon\Carbon $moderated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property TopicsTag  $tag
 * @property User       $user
 *
 * @package App\Models
 */
class Topic extends Eloquent
{
    const TYPE_LINK = 1;
    const TYPE_CONTENT = 2;
    const TYPES = [
        self::TYPE_LINK => 'Ссылка',
        self::TYPE_CONTENT => 'Текст',
    ];

    const ASPECT_RATIO = 1.25; // 5/4;

	protected $casts = [
		'tag_id'       => 'int',
		'user_id'      => 'int',
        'difficulty'   => 'int',
        'type'         => 'int',
		'active'       => 'bool',
		'weight'       => 'int',
        'moderated_at' => 'date',
	];

	protected $fillable = [
		'tag_id',
		'user_id',
		'name',
        'image',
        'difficulty',
        'type',
        'content',
		'active',
		'weight',
        'users',
        'moderated_at',
	];

	public function tag()
	{
		return $this->belongsTo(TopicsTag::class, 'tag_id');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

    public function getUsersAttribute()
    {
        try {
            $result = json_decode($this->attributes['users'], true);
        } catch (\Exception $e) {
            $result = [];
        }
        return $result;
    }

    public function setUsersAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['users'] = json_encode($value);
        }
	}

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!array_key_exists('active', $attributes)) {
            $this->active = true;
        }
        if (!array_key_exists('users', $attributes)) {
            $this->users = [];
        }
    }
}
