<?php

namespace Modules\Topics\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class TopicsGroup
 * 
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property bool $active
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $tags
 *
 * @package App\Models
 */
class TopicsGroup extends Eloquent
{
    const TRANS_GROUP = 'topics';
    const TRANS_KEY_PREFIX = 'groups';

	protected $casts = [
		'active' => 'bool',
		'weight' => 'int',
	];

	protected $fillable = [
	    'alias',
		'name',
		'active',
		'weight',
	];

	protected $attributes = [
	    'active' => true,
    ];

	public function tags()
	{
		return $this->hasMany(TopicsTag::class, 'group_id');
	}
}
