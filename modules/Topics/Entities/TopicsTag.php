<?php

namespace Modules\Topics\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class TopicsTag
 * 
 * @property int $id
 * @property int $group_id
 * @property string $alias
 * @property string $name
 * @property bool $active
 * @property int $weight
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property TopicsGroup $group
 * @property \Illuminate\Database\Eloquent\Collection $topics
 *
 * @package App\Models
 */
class TopicsTag extends Eloquent
{
    const TRANS_GROUP = TopicsGroup::TRANS_GROUP;
    const TRANS_KEY_PREFIX = 'tags';

	protected $casts = [
		'group_id'  => 'int',
		'active'    => 'bool',
		'weight'    => 'int',
	];

	protected $fillable = [
		'group_id',
        'alias',
		'name',
		'active',
		'weight',
	];

    protected $attributes = [
        'active' => true,
    ];

	public function group()
	{
		return $this->belongsTo(TopicsGroup::class, 'group_id');
	}

	public function topics()
	{
		return $this->hasMany(Topic::class, 'tag_id');
	}
}
