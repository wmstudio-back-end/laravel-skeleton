<?php

namespace Modules\Settings\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class SettingsGroup
 * 
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property int $weight
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $settings
 */
class SettingsGroup extends Eloquent
{
    public $timestamps = false;

	protected $casts = ['weight' => 'int'];

	protected $fillable = ['name', 'alias', 'weight'];

	public function settings()
	{
		return $this->hasMany(\Modules\Settings\Entities\Setting::class, 'group_id');
	}
}
