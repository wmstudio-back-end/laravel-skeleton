<?php

namespace Modules\Settings\Entities;

use Modules\MCms\Entities\OrderByWeight as Eloquent;

/**
 * Class Setting
 * 
 * @property int $id
 * @property int $group_id
 * @property string $header_name
 * @property string $name
 * @property string $value
 * @property string $html_control_type
 * @property int $weight
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Modules\Settings\Entities\SettingsGroup $settings_group
 */
class Setting extends Eloquent
{
    public $timestamps = false;

	protected $casts = ['group_id' => 'int', 'weight' => 'int'];

	protected $fillable = ['group_id', 'header_name', 'name', 'value', 'html_control_type', 'weight'];

	public function settings_group()
	{
		return $this->belongsTo(\Modules\Settings\Entities\SettingsGroup::class, 'group_id');
	}
}
