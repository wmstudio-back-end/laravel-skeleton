<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Settings\Entities\Setting;
use Modules\Settings\Entities\SettingsGroup;

class AddLangSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        $groups = SettingsGroup::find([3,4]);
        SettingsGroup::unguard();
        foreach ($groups as $group) {
            /* @var $group SettingsGroup */
            $group->delete();
        }
        SettingsGroup::create([
            'id'     => 3,
            'name'   => 'Самоназвание языков',
            'alias'  => 'languages',
            'weight' => 3,
        ]);
        SettingsGroup::reguard();

        $langs = [
            'en' => 'English',
            'ar' => 'العربية',
            'ru' => 'Русский',
            'tr' => 'Türkçe',
        ];

        $insert = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($langs as $name => $lang) {
            $insert[] = [
                'group_id'          => 3,
                'header_name'       => $name,
                'name'              => 'lang-' . $name,
                'value'             => $lang,
                'html_control_type' => 'input',
                'weight'            => 0,
                'updated_at'        => $timeStamp,
            ];
        }
        Setting::insert($insert);
    }
}
