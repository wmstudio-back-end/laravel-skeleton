<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Settings\Entities\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'group_id'          => 1,
                'header_name'       => 'Заголовок сайта',
                'name'              => 'main_title',
                'value'             => 'Заголовок сайта',
                'html_control_type' => 'input',
                'weight'            => 1,
            ],
            [
                'group_id'          => 2,
                'header_name'       => 'Эл. адрес для отправки отчетов на сайте',
                'name'              => 'email_for_error_reporting',
                'value'             => 'support@site.com',
                'html_control_type' => 'input',
                'weight'            => 1,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Включить LockScreen',
                'name'              => 'lockscreen_enabled',
                'value'             => 0,
                'html_control_type' => 'checkbox',
                'weight'            => 2,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Время ожидания LockScreen (сек)',
                'name'              => 'lockscreen_timeout',
                'value'             => 600,
                'html_control_type' => 'input',
                'weight'            => 3,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Включить Яндекс Метрику',
                'name'              => 'yandex_metrika_enabled',
                'value'             => false,
                'html_control_type' => 'checkbox',
                'weight'            => 4,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Яндекс OAuth ID',
                'name'              => 'yandex_oauth_id',
                'value'             => null,
                'html_control_type' => 'input',
                'weight'            => 5,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Яндекс OAuth Secret',
                'name'              => 'yandex_oauth_secret',
                'value'             => null,
                'html_control_type' => 'input',
                'weight'            => 6,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Токены для Яндекс Метрики',
                'name'              => 'yandex_metrika_token',
                'value'             => null,
                'html_control_type' => 'yandex_oauth_tokens',
                'weight'            => 7,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'ID счетчика Яндекс Метрики',
                'name'              => 'yandex_metrika_id',
                'value'             => null,
                'html_control_type' => 'input',
                'weight'            => 8,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Код счётчика Яндекс Метрики',
                'name'              => 'yandex_metrika_javascript',
                'value'             => null,
                'html_control_type' => 'textarea',
                'weight'            => 9,
            ],
            [
                'group_id'          => 1,
                'header_name'       => 'Демо режим Яндекс Метрики',
                'name'              => 'yandex_metrika_demo',
                'value'             => true,
                'html_control_type' => 'checkbox',
                'weight'            => 10,
            ],
        ];

        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($settings as $key => $field) {
            $settings[$key]['updated_at'] = $timeStamp;
        }
        Setting::unguard();
        Setting::insert($settings);
        Setting::reguard();
    }
}
