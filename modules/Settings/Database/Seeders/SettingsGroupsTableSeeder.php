<?php

namespace Modules\Settings\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Settings\Entities\SettingsGroup;

class SettingsGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'id'     => 1,
                'name'   => 'Параметры сайта',
                'weight' => 1,
            ],
            [
                'id'     => 2,
                'name'   => 'Автоматические письма',
                'weight' => 2,
            ],
            [
                'id'     => 3,
                'name'   => 'Рассылка',
                'weight' => 3,
            ],
            [
                'id'     => 4,
                'name'   => 'Социальные кнопки и виджеты',
                'weight' => 4,
            ],
        ];

        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($groups as $key => $field) {
            $groups[$key]['updated_at'] = $timeStamp;
        }
        SettingsGroup::unguard();
        SettingsGroup::insert($groups);
        SettingsGroup::reguard();
    }
}
