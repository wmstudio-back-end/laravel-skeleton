<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('settings', function(Blueprint $table)
		{
			$table->foreign('group_id', 'settings_ibfk_1')->references('id')->on('settings_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});

        Artisan::call('db:seed', array('--class' => \Modules\Settings\Database\Seeders\SettingsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('settings', function(Blueprint $table)
		{
			$table->dropForeign('settings_ibfk_1');
		});
	}

}
