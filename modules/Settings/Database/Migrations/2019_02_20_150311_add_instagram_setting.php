<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\Setting;
use Modules\Settings\Entities\SettingsGroup;

class AddInstagramSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = SettingsGroup::where('alias', 'social')->select('id')->first()->id;
        Setting::create([
            'group_id' => $group,
            'header_name' => 'Ссылка на группу/аккаунт Instagram',
            'name' => 'social_in',
            'html_control_type' => 'input',
            'weight' => (Setting::where('group_id', $group)->max('weight') + 1),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Setting::where('name', 'social_in')->delete();
    }
}
