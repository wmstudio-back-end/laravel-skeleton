<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;

class AddEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = SettingsGroup::where('alias', 'settings')->first();
        Setting::insert([
            'group_id' => $group->id,
            'header_name' => 'E-Mail для уведомлений',
            'name' => 'email_notify',
            'value' => '',
            'html_control_type' => 'input',
            'weight' => Setting::where('group_id', $group->id)->max('weight') + 1,
            'updated_at' => new \DateTime(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $setting = Setting::where('name', 'email_notify')->first();
        if ($setting) {
            $setting->delete();
        }
    }
}
