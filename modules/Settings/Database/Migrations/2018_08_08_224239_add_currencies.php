<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;

class AddCurrencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = SettingsGroup::create([
            'name' => 'Валюты',
            'alias' => 'currencies',
            'weight' => SettingsGroup::max('weight') + 1,
        ]);
        Setting::insert([
            'group_id' => $group->id,
            'header_name' => 'Активные валюты',
            'name' => 'currencies',
            'value' => '[]',
            'html_control_type' => 'currencies',
            'weight' => Setting::where('group_id', $group->id)->max('weight') + 1,
            'updated_at' => new \DateTime(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $setting = Setting::where('name', 'currencies')->first();
        if ($setting) {
            if ($setting->settings_group) {
                $setting->settings_group->delete();
            }
            $setting->delete();
        }
    }
}
