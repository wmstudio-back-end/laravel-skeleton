<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;

class AddSocialSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_groups', function(Blueprint $table)
        {
            $table->string('alias')->unique()->change()->nullable(false);
        });

        $group = SettingsGroup::create([
            'name' => 'Социальные кнопки и виджеты',
            'alias' => 'social',
            'weight' => 2,
        ]);
        $weight = 0;
        $now = new DateTime();
        Setting::insert([
            [
                'group_id' => $group->id,
                'header_name' => 'Ссылка на группу/аккаунт Вконтакте',
                'name' => 'social_vk',
                'value' => '',
                'html_control_type' => 'input',
                'weight' => ++$weight,
                'updated_at' => $now,
            ],
            [
                'group_id' => $group->id,
                'header_name' => 'Ссылка на группу/аккаунт Facebook',
                'name' => 'social_fb',
                'value' => '',
                'html_control_type' => 'input',
                'weight' => ++$weight,
                'updated_at' => $now,
            ],
            [
                'group_id' => $group->id,
                'header_name' => 'Ссылка на группу/аккаунт Twitter',
                'name' => 'social_tw',
                'value' => '',
                'html_control_type' => 'input',
                'weight' => ++$weight,
                'updated_at' => $now,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $settings = Setting::whereIn('name', ['social_vk', 'social_fb', 'social_tw'])->get();
        foreach ($settings as $setting) {
            $setting->delete();
        }
        $group = SettingsGroup::where('alias', 'social')->first();
        if ($group) {
            $group->delete();
        }
        Schema::table('settings_groups', function(Blueprint $table)
        {
            $table->dropUnique('settings_groups_alias_unique');
        });
    }
}
