<?php

use Barryvdh\TranslationManager\Models\Translation;
use Carbon\Carbon;
use Modules\Settings\Entities\Setting;
use Modules\Settings\Entities\SettingsGroup;
use Illuminate\Database\Migrations\Migration;

class AddPersonalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $email = Setting::where('name', 'email_notify')->first();
        $group = SettingsGroup::where('alias', 'settings')->select('id')->first();
        /* @var $email Setting */
        Setting::create([
            'group_id' => $group->id,
            'header_name' => 'Ссылка на страницу/файл c согласием на обработку персональных данных',
            'name' => 'personal_data',
            'value' => '',
            'html_control_type' => 'input',
            'weight' => $email->weight,
//            'updated_at' => new Carbon(),
        ]);
        $email->weight++;
        $email->save();

        $personalDataTexts = Translation::where([
            'group' => 'auth',
            'key' => 'personal-data-processing',
        ])->get();
        if ($personalDataTexts->count()) {
            foreach ($personalDataTexts as $translation) {
                /* @var $translation Translation */
                $data = $translation->toArray();
                $data['key'] .= '-with-link';
                if (Translation::where([
                    'group' => 'auth',
                    'locale' => $data['locale'],
                    'key' => $data['key'],
                ])->count()) {
                    continue;
                }

                $delimiter = '{new_line}';
                $value = explode($delimiter, mb_substr(($value = preg_replace('/\<\/p\>[ \n]*\<p\>/i', $delimiter, $data['value'])), 0, mb_strlen($value) - 4));
                if (count($value) == 2) {
                    unset($data['id'], $data['created_at'], $data['updated_at']);
                    $data['status'] = Translation::STATUS_CHANGED;

                    $value[0] .= '</p>';
                    $value[1] = "<p><a href=\":link\" target=\"_blank\">{$value[1]}</a></p>";
                    $data['value'] = implode("\n", $value);
                    Translation::create($data);
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Setting::where('name', 'personal_data')->delete();
        $email = Setting::where('name', 'email_notify')->first();
        /* @var $email Setting */
        $email->weight--;
        $email->save();

        Translation::where([
            'group' => 'auth',
            'key' => 'personal-data-processing-with-link',
        ])->delete();
    }
}
