<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\Setting;

class UpdateTariffSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $settings = Setting::whereIn('name', [
            'pricePerMinute',
            'default-tariff',
            'countMinutes',
            'countDays',
            'group-discount',
        ])->get()->keyBy('name');

        if (isset($settings['pricePerMinute'])) {
            $settings['pricePerMinute']->name = 'price_per_minute';
            $settings['pricePerMinute']->html_control_type = 'price_per_minute';
            $settings['pricePerMinute']->value = json_encode(['student' => $settings['pricePerMinute']->value, 'teacher' => 1.8]);
            $settings['pricePerMinute']->save();
        }
        if (isset($settings['default-tariff'])) {
            $settings['default-tariff']->name = 'default_tariff';
            $settings['default-tariff']->html_control_type = 'default_tariff';
            $settings['default-tariff']->save();
        }
        if (isset($settings['countMinutes'])) {
            $settings['countMinutes']->name = 'count_minutes';
            $settings['countMinutes']->save();
        }
        if (isset($settings['countDays'])) {
            $settings['countDays']->name = 'count_days';
            $settings['countDays']->save();
        }
        if (isset($settings['group-discount'])) {
            $settings['group-discount']->name = 'group_discount';
            $settings['group-discount']->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $settings = Setting::whereIn('name', [
            'price_per_minute',
            'default_tariff',
            'count_minutes',
            'count_days',
            'group_discount',
        ])->get()->keyBy('name');

        if (isset($settings['price_per_minute'])) {
            $settings['price_per_minute']->name = 'pricePerMinute';
            $settings['price_per_minute']->html_control_type = 'input';
            $settings['price_per_minute']->value = json_decode($settings['price_per_minute']->value, true)['student'];
            $settings['price_per_minute']->save();
        }
        if (isset($settings['default_tariff'])) {
            $settings['default_tariff']->name = 'default-tariff';
            $settings['default_tariff']->html_control_type = 'default-tariff';
            $settings['default_tariff']->save();
        }
        if (isset($settings['count_minutes'])) {
            $settings['count_minutes']->name = 'countMinutes';
            $settings['count_minutes']->save();
        }
        if (isset($settings['count_days'])) {
            $settings['count_days']->name = 'countDays';
            $settings['count_days']->save();
        }
        if (isset($settings['group_discount'])) {
            $settings['group_discount']->name = 'group-discount';
            $settings['group_discount']->save();
        }
    }
}
