<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\Setting;

class AddLangSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings_groups', function(Blueprint $table)
        {
            $table->text('alias')->nullable(true)->after('name');
        });
        Artisan::call('db:seed', array('--class' => \Modules\Settings\Database\Seeders\AddLangSettings::class));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws \Exception
     */
    public function down()
    {
        $settings = Setting::whereIn('group_id', [3])->get();
        Setting::unguard();
        foreach ($settings as $setting) {
            /* @var $setting Setting */
            $setting->delete();
        }
        Setting::reguard();

        Schema::table('settings_groups', function(Blueprint $table)
        {
            $table->dropColumn('alias');
        });
    }
}
