<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;
use Barryvdh\TranslationManager\Models\Translation;

class AddTariffsSettings extends Migration
{
    protected $group;
    protected $settings;
    protected $translates = [
        'month'     => 'Месяц',
        'quarter'   => 'Квартал',
        'semester'  => 'Семестр',
        'year'      => 'Год',
    ];

    public function __construct()
    {
        $this->group = [
            'name'   => 'Тарифы',
            'alias'  => 'tariffs',
            'weight' => SettingsGroup::all()->max('weight') + 1,
        ];

        $weight = 0;
        $this->settings = [
            'pricePerMinute' => [
                'header_name' => 'Цена за минуту',
                'value' => 2,
                'html_control_type' => 'input',
                'weight' => ++$weight,
            ],
            'default-tariff' => [
                'header_name' => 'Тарифф по умолчанию',
                'value' => 'tariff-2',
                'html_control_type' => 'default-tariff',
                'weight' => ++$weight,
            ],
            'countMinutes' => [
                'header_name' => 'Количество минут <small>(каждое значение в новой строке. значение по умолчанию отметить `*` прим: "30*")</small>',
                'value' => "15\n30\n60*\n120",
                'html_control_type' => 'textarea',
                'weight' => ++$weight,
            ],
            'countDays' => [
                'header_name' => 'Количество уроков в неделю <small>(каждое значение в новой строке. значение по умолчанию отметить `*` прим: "1*")</small>',
                'value' => "1*\n3\n5\n7",
                'html_control_type' => 'textarea',
                'weight' => ++$weight,
            ],
            'tariff-1' => [
                'header_name' => 'Тарифф 1',
                'value' => json_encode([
                    'name' => 'month',
                    'period' => 1,
                    'discount' => 0,
                ]),
                'html_control_type' => 'tariff',
                'weight' => ++$weight,
            ],
            'tariff-2' => [
                'header_name' => 'Тарифф 2',
                'value' => json_encode([
                    'name' => 'quarter',
                    'period' => 3,
                    'discount' => 15,
                ]),
                'html_control_type' => 'tariff',
                'weight' => ++$weight,
            ],
            'tariff-3' => [
                'header_name' => 'Тарифф 3',
                'value' => json_encode([
                    'name' => 'semester',
                    'period' => 6,
                    'discount' => 20,
                ]),
                'html_control_type' => 'tariff',
                'weight' => ++$weight,
            ],
            'tariff-4' => [
                'header_name' => 'Тарифф 4',
                'value' => json_encode([
                    'name' => 'year',
                    'period' => 12,
                    'discount' => 25,
                ]),
                'html_control_type' => 'tariff',
                'weight' => ++$weight,
            ],
            'group-discount' => [
                'header_name' => 'Скидка для групповых занятий (%)',
                'value' => 50,
                'html_control_type' => 'input',
                'weight' => ++$weight,
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = SettingsGroup::create($this->group);

        $insertSettings = [];
        $timeStamp = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($this->settings as $name => $setting) {
            $insertSettings[] = [
                'group_id'          => $group->id,
                'header_name'       => $setting['header_name'],
                'name'              => $name,
                'value'             => $setting['value'],
                'html_control_type' => $setting['html_control_type'],
                'weight'            => $setting['weight'],
                'updated_at'        => $timeStamp,
            ];
        }
        Setting::unguard();
        Setting::insert($insertSettings);
        Setting::reguard();

        $insertTrans = [];
        foreach ($this->translates as $key => $value) {
            $insertTrans[] = [
                'status' => Translation::STATUS_CHANGED,
                'locale' => 'ru',
                'group' => 'dictionary',
                'key' => $key,
                'value' => $value,
                'created_at' => $timeStamp,
                'updated_at' => $timeStamp,
            ];
        }
        Translation::unguard();
        Translation::insert($insertTrans);
        Translation::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $translates = Translation::where('group', 'dictionary')
            ->whereIn('key', array_keys($this->translates))->get();
        foreach ($translates as $translate) {
            $translate->delete();
        }

        $settings = Setting::whereIn('name', array_keys($this->settings))->get();
        Setting::unguard();
        foreach ($settings as $setting) {
            $setting->delete();
        }
        Setting::reguard();

        $group = SettingsGroup::where('alias', $this->group['alias'])->first();
        $group->delete();
    }
}
