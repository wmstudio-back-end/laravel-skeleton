<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings_groups', function(Blueprint $table) {
			$table->integer('id', true);
			$table->string('name');
			$table->integer('weight');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
		});

        Artisan::call('db:seed', array('--class' => \Modules\Settings\Database\Seeders\SettingsGroupsTableSeeder::class));
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings_groups');
	}

}
