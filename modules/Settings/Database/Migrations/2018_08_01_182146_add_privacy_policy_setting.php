<?php

use Illuminate\Database\Migrations\Migration;
use Modules\Settings\Entities\SettingsGroup;
use Modules\Settings\Entities\Setting;

class AddPrivacyPolicySetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $group = SettingsGroup::where('alias', 'settings')->first();
        if ($group) {
            Setting::create([
                'group_id' => $group->id,
                'header_name' => 'Ссылка на страницу/файл с политикой конфиденциальности',
                'name' => 'privacy_policy',
                'html_control_type' => 'input',
                'weight' => Setting::where('group_id', $group->id)->max('weight') + 1,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down()
    {
        $setting = Setting::where('name', 'privacy_policy')->first();
        if ($setting) {
            $setting->delete();
        }
    }
}
