<?php
    $canUpdate = isset($canUpdate) ? $canUpdate : false;
    /* @var $setting Modules\Settings\Entities\Setting */
    $value = json_decode($setting->value, true);
    $list = \Modules\Settings\Constants\CurrencyList::getCurrency();
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-12 custom-fg-cb ajaxSubmit">
    @csrf
@else
    <div class="form-group col-md-12 custom-fg-cb">
@endif
    <div class="custom-control custom-checkbox middle">
        <input type="hidden" name="checkbox-{{ $setting->name }}" value="{{ $setting->name }}[]">
        <div class="row">
            @foreach($list as $key => $val)
                <div class="col-md-2">
                    <div class="custom-control custom-checkbox middle">
                        <input
                            type="checkbox"
                            class="custom-control-input"
                            id="{{ $setting->name }}-{{ $key }}"
                            name="{{ $setting->name }}[{{ $key }}]"
                            value="1"
                            {{ old($setting->name, null) !== null && is_array(old($setting->name))
                                ? (in_array($key, old($setting->name)) ? 'checked="checked"'
                                    : (is_array($value) && isset($value[$key]) && $value[$key])
                                        ? 'checked="checked"' : '')
                                : (is_array($value) && isset($value[$key]) && $value[$key])
                                     ? 'checked="checked"' : ''
                            }}
                        >
                        <label
                            @if($canUpdate)for="{{ $setting->name }}-{{ $key }}"@endif
                            class="custom-control-label limited"
                        >({!! $val['symbol'] !!}) {{ $val['name'] }}</label>
                    </div>
                </div>
            @endforeach
        </div>
        @if($canUpdate)
            <button class="btn btn-sm btn-primary pull-right" type="submit">Сохранить</button>
        @endif
    </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif