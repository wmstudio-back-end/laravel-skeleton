<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
$tariffs = \Modules\Settings\Entities\Setting::where('name', 'like', 'tariff-%')->get();
$list = [];
foreach ($tariffs as $tariff) {
    /* @var $tariff Modules\Settings\Entities\Setting */
    try {
        $name = json_decode($tariff->value, true)['name'];
    } catch (Exception $e) {
        $name = 'Ошибка получения имени';
    }
    $list[$tariff->name] = transDef($name, 'dictionary.' . $name);
}
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6">
@endif
    <div class="form-group">
        <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
        <div class="input-group small">
            <select
                id="{{ $setting->name }}"
                name="{{ $setting->name }}"
                class="form-control selectpicker"
                data-live-search="false"
                @if(!$canUpdate)readonly="readonly"@endif
            >
                @foreach ($list as $key=> $val)
                    <option
                            value="{{ $key }}"
                            {{ $setting->value === $key ? ' selected="selected"' : '' }}
                    >{{ $val }}</option>
                @endforeach
            </select>
            @if($canUpdate)
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Сохранить</button>
                </div>
            @endif
        </div>
    </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif