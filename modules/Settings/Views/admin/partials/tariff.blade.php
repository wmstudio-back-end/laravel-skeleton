<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
$value = json_decode($setting->value, true);
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6">
@endif
        <div class="form-group">
            <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
            <div class="input-group small">
                @php $example = (isset($value['name']) && !empty($value['name'])) ? $value['name'] : 'month'; @endphp
                <input
                    type="text"
                    class="form-control"
                    id="{{ $setting->name }}"
                    name="{{ $setting->name }}[name]"
                    placeholder="{{ $placeholder }}"
                    data-toggle="tooltip"
                    data-html="true"
                    data-title="{{ 'Ключ для перевода в формате slug (группа dictionary)<br>'
                        . '<em>Прим:</em> <u>' . $example . '</u><br>'
                        . '<b>Локализация -> dictionary -> ключ "' . $example . '"</b>' }}"
                    value="{{ $value['name'] }}"
                    @if(!$canUpdate)readonly="readonly"@endif
                >
                <input
                    type="text"
                    class="form-control"
                    name="{{ $setting->name }}[period]"
                    data-toggle="tooltip"
                    data-title="{{ 'Период (количество месяцев)' }}"
                    placeholder="{{ $placeholder }}"
                    value="{{ $value['period'] }}"
                    @if(!$canUpdate)readonly="readonly"@endif
                >
                <input
                    type="text"
                    class="form-control"
                    name="{{ $setting->name }}[discount]"
                    data-toggle="tooltip"
                    data-title="{{ 'Скидка (%)' }}"
                    placeholder="{{ $placeholder }}"
                    value="{{ $value['discount'] }}"
                    @if(!$canUpdate)readonly="readonly"@endif
                >
                @if($canUpdate)
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Сохранить</button>
                    </div>
                @endif
            </div>
        </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif