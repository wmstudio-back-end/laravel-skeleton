<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
/* @var $setting Modules\Settings\Entities\Setting */
$tokens = [];
$hasTokens = false;
try {
    $tokens = json_decode($setting->value, true);
} catch (Exception $e) {}
if (isset($tokens['access_token']) && isset($tokens['refresh_token']) && isset($tokens['expires_in'])) {
    $hasTokens = true;
    $hasExpired = !(new DateTime($tokens['expires_in']) > new DateTime());
}

if (!$hasTokens || $hasExpired) {
    $tokensAvailable = false;
    $yaSettings = \Modules\Settings\Entities\Setting::whereIn('name', [
        'yandex_oauth_id',
        'yandex_oauth_secret',
        'yandex_metrika_id',
        'yandex_metrika_javascript',
    ])->get()->keyBy('name');
    if ($yaSettings['yandex_oauth_id']->value != null && $yaSettings['yandex_oauth_secret']->value != null
        && $yaSettings['yandex_metrika_id']->value != null && $yaSettings['yandex_metrika_javascript']->value != null) {
        $tokensAvailable = true;
    }
}

?>
<div class="form-group col-md-6">
    <div class="form-group">
    <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
        @if($hasTokens && !$hasExpired)
            <div class="input-group small">
                <button class="btn btn-sm btn-success" disabled="disabled" type="submit">Токены получены</button>
            </div>
            <label>
                Токен истекает через <b><i>{{ dateDiff(new \DateTime($tokens['expires_in']), null, ['hours' => false, 'case' => false]) }}</i></b>
                @if($updated = \Cache::get('ya_token_update'))<br>Последний раз обновлялся <b><i>{{ $updated }}</i></b>@endif
            </label>
        @else
            <div class="input-group small">
                @if ($tokensAvailable)
                    <a href="{{ \Modules\Settings\Controllers\AdminController::getYaOauthUrl() }}" class="btn btn-sm btn-warning" type="submit">Получить токены</a>
                @else
                    <button class="btn btn-sm btn-danger" disabled="disabled" type="submit">Для получения токенов, заполните данные Яндекс OAuth и Яндекс Метрика</button>
                @endif
            </div>
        @endif
    </div>
</div>