<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 custom-fg-cb ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6 custom-fg-cb">
@endif
    <div class="custom-control custom-checkbox middle">
        <input type="hidden" name="checkbox-1" value="{{ $setting->name }}">
        <input
            type="checkbox"
            class="custom-control-input"
            id="{{ $setting->name }}"
            name="{{ $setting->name }}"
            value="1"
            @if ($setting->value)checked="checked"@endif
            @if(!$canUpdate)readonly="readonly"@endif
        >
        <label
            class="custom-control-label"
            @if($canUpdate)for="{{ $setting->name }}"@endif
        >{!! $setting->header_name !!}</label>
        @if($canUpdate)
            <button class="btn btn-sm btn-primary pull-right" type="submit">Сохранить</button>
        @endif
    </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif