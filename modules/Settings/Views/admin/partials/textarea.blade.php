<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6">
@endif
    <div class="form-group">
        <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
        <textarea
            class="form-control"
            id="{{ $setting->name }}"
            name="{{ $setting->name }}"
            placeholder="{{ $placeholder }}"
            @if(!$canUpdate)readonly="readonly"@endif
            rows="5"
        >{!! $setting->value !!}</textarea>
        @if($canUpdate)
            <div class="pull-right" style="margin-top: 10px;">
                <button class="btn btn-sm btn-primary" type="submit">Сохранить</button>
            </div>
        @endif
    </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif