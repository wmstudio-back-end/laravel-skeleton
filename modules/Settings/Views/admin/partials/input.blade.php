<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6">
@endif
    <div class="form-group">
        <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
        <div class="input-group small">
            <input
                type="text"
                class="form-control"
                id="{{ $setting->name }}"
                name="{{ $setting->name }}"
                placeholder="{{ $placeholder }}"
                value="{{ $setting->value }}"
                @if(!$canUpdate)readonly="readonly"@endif
            >
            @if($canUpdate)
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Сохранить</button>
                </div>
            @endif
        </div>
    </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif