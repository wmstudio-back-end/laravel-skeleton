<?php
$canUpdate = isset($canUpdate) ? $canUpdate : false;
$value = json_decode($setting->value, true);
/* @var $setting Modules\Settings\Entities\Setting */
?>
@if($canUpdate)
    <form method="post" role="form" action="{{ route('admin.settings.update') }}" class="form-group col-md-6 ajaxSubmit">
        @csrf
@else
    <div class="form-group col-md-6">
@endif
        <div class="form-group">
            <label for="{{ $setting->name }}">{!! $setting->header_name !!}</label>
            <div class="input-group small">
                @php $example = (isset($value['name']) && !empty($value['name'])) ? $value['name'] : 'month'; @endphp
                <input
                    type="text"
                    class="form-control"
                    id="{{ $setting->name }}"
                    name="{{ $setting->name }}[student]"
                    placeholder="{{ $placeholder }}"
                    data-toggle="tooltip"
                    data-title="{{ 'Для студента' }}"
                    value="{{ $value['student'] }}"
                    @if(!$canUpdate)readonly="readonly"@endif
                >
                <input
                    type="text"
                    class="form-control"
                    name="{{ $setting->name }}[teacher]"
                    data-toggle="tooltip"
                    data-title="{{ 'Для преподавателя' }}"
                    placeholder="{{ $placeholder }}"
                    value="{{ $value['teacher'] }}"
                    @if(!$canUpdate)readonly="readonly"@endif
                >
                @if($canUpdate)
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Сохранить</button>
                    </div>
                @endif
            </div>
        </div>
@if($canUpdate)
    </form>
@else
    </div>
@endif