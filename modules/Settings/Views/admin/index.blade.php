<?php
$canUpdate = getPermissions('admin.settings.update');
?>

@extends(config('admin.defaults.layout'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card animated fadeInRight">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="groupsTab" role="tablist">
                        @foreach($groups as $group)
                            <?php /* @var $group \Modules\Settings\Entities\SettingsGroup */ ?>
                            <li class="nav-item">
                                <a class="nav-link{{ $groups[0]->id == $group->id ? ' active' : '' }}"
                                   id="group-{{ $group->id }}-tab" data-toggle="tab" href="#group-{{ $group->id }}"
                                   role="tab" aria-controls="group-{{ $group->id }}"
                                   aria-selected="true">{{ $group->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content tab-bordered collapse show">
                        @foreach($groups as $group)
                            <?php /* @var $group \Modules\Settings\Entities\SettingsGroup */ ?>
                            <div class="tab-pane fade{{ $groups[0]->id == $group->id ? ' show active' : '' }}"
                                 id="group-{{ $group->id }}" role="tabpanel" aria-labelledby="contact-tab">
                                @if ($group->alias == 'languages' && count($locales))
                                    <?php $settings = $group->settings->keyBy('name'); ?>
                                    <div class="row">
                                        @foreach ($locales as $lang)

                                                <?php
                                                    if (isset($settings['lang-' . $lang])) {
                                                        $setting = $settings['lang-' . $lang];
                                                    } else {
                                                        $setting = new \Modules\Settings\Entities\Setting();
                                                        $setting->name = 'lang-' . $lang;
                                                        $setting->header_name = $lang;
                                                    }
                                                ?>
                                                    @include('settings::admin.partials.input', [
                                                        'setting' => $setting,
                                                        'placeholder' => trim(preg_replace('|\<[a-zA-Z]+[1-6]?[\s[^>]*]?'
                                                            .'\s?/?\>[\s\S]*\<\/[a-zA-Z]+[1-6]?[\s[^>]*]?\s?/?\>|'
                                                            , '', $setting->header_name)),
                                                    ])
                                        @endforeach
                                    </div>
                                @else
                                    @if (count($group->settings))
                                        <div class="row">
                                            @foreach ($group->settings as $setting)
                                                <?php /* @var $setting Modules\Settings\Entities\Setting */ ?>
                                                @include('settings::admin.partials.' . $setting->html_control_type, [
                                                    'setting' => $setting,
                                                    'placeholder' => trim(preg_replace('|\<[a-zA-Z]+[1-6]?[\s[^>]*]?'
                                                            .'\s?/?\>[\s\S]*\<\/[a-zA-Z]+[1-6]?[\s[^>]*]?\s?/?\>|'
                                                            , '', $setting->header_name)),
                                                ])
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="no-content"></div>
                                    @endif
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection