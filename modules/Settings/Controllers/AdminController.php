<?php

namespace Modules\Settings\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\MCms\Helpers\Curl;
use Modules\Settings\Entities\Setting;
use Modules\Settings\Entities\SettingsGroup;

class AdminController extends Controller
{
    private const TOKEN_SECRET = 'ca53c8a434ea6590c82fa54b580338b5';

    public static function writeRequired()
    {
        return [
            'update',
        ];
    }

    public function index($secret = null)
    {
        if ($secret != null) {
            if ($secret == self::TOKEN_SECRET && $code = request()->get('code', null)) {
                $curl = curl_init();
                $yaSettings = Setting::whereIn('name', [
                    'yandex_oauth_id',
                    'yandex_oauth_secret',
                    'yandex_metrika_token',
                ])->get()->keyBy('name');

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://oauth.yandex.ru/token",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=" . $code,
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Basic " . base64_encode($yaSettings['yandex_oauth_id']->value . ':' . $yaSettings['yandex_oauth_secret']->value),
                        "cache-control: no-cache",
                        "content-type: application/x-www-form-urlencoded",
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if (!$err) {
                    $data = json_decode($response, true);
                    if (!isset($data['error'])) {
                        if (isset($data['token_type'])) {
                            unset($data['token_type']);
                        }
                        $data['expires_in'] = date_format(new \DateTime('+' . $data['expires_in'] . ' seconds'), 'd.m.Y H:i:s');
                        $yaSettings['yandex_metrika_token']->value = json_encode($data);
                        $yaSettings['yandex_metrika_token']->save();
                    }
                    return redirect()->route('admin.settings');
                }
            } else {
                abort(404);
            }
        }

        $groups = SettingsGroup::all();
        $locales = $this->transManager->getLocales();

        return view('settings::admin.index', [
            'locales' => $locales,
            'groups' => $groups,
        ]);
    }

    public function update(Request $request)
    {
        $data = getRequest();

        if ($request->ajax()) {
            try {
                $error = false;
                $msgHeader = 'Успешно обновлено!';
                $counter = 0;
                foreach ($data as $key => $val) {
                    $setting = Setting::where('name', $key)->first();
                    if (!$setting && strpos($key, 'lang-') !== false) {
                        $setting = new Setting();
                        $setting->group_id = 3;
                        $setting->name = $key;
                        $setting->header_name = str_replace('lang-', '', $key);
                        $setting->html_control_type = 'input';
                        $setting->weight = 0;
                    }
                    $setting->value = is_array($val) ? json_encode($val) : $val;
                    $setting->save();
                    $counter++;
                }
                $message = numEnding($counter, ['Обновлена', 'Обновлены', 'Обновлены'])
                    . " $counter " . numEnding($counter, ['запись', 'записи', 'записей']);
            } catch (\Exception $e) {
                $error = true;
                $msgHeader = 'Ошибка!';
                $message = $e->getMessage();
            }

            return response()->json([
                'error' => $error, 'msgHeader' => $msgHeader, 'message' => $message,
            ]);
        } else {
            foreach ($data as $key => $val) {
                $setting = Setting::where('name', $key)->first();
                if (!$setting && strpos($key, 'lang-') !== false) {
                    $setting = new Setting();
                    $setting->group_id = 3;
                    $setting->name = $key;
                    $setting->header_name = str_replace('lang-', '', $key);
                    $setting->html_control_type = 'input';
                    $setting->weight = 0;
                }
                $setting->value = is_array($val) ? json_encode($val) : $val;
                $setting->save();
            }

            return redirect()->route('admin.settings');
        }

    }

    public function groups()
    {
        return view('settings::admin.index');
    }

    public static function getYaOauthUrl()
    {
        return 'https://oauth.yandex.ru/authorize?'
            . 'response_type=code'
            . '&client_id=' . setting('yandex_oauth_id')
            . '&redirect_uri='. route('admin.settings', ['secret' => self::TOKEN_SECRET]);
    }

    protected static function showMetrikaError($message)
    {
        if (is_array($message)) {
            $message = implode("<br>", $message);
        }
        if (empty($message)) {
            return;
        }
        Input::replace([
            'toastr.error' => true,
            'toastr.msgHeader' => 'Ошибка Яндекс.Метрики.',
            'toastr.message' => $message,
        ]);
        Input::flash();
    }

    /**
     * @param \DateTime|null $date1
     * @param \DateTime|null $date2
     *
     * @return array
     * @throws \Exception
     */
    public static function getMetrika($date1 = null, $date2 = null)
    {
        if ($date1 === null) {
            $date1 = (new \DateTime('-1 month'))->format('Ymd');
        }
        if ($date2 === null) {
            $date2 = (new \DateTime())->format('Ymd');
        }

        $yaSettings = Setting::whereIn('name', [
            'yandex_metrika_enabled',
            'yandex_metrika_demo',
            'yandex_metrika_id',
            'yandex_metrika_token',
        ])->get()->keyBy('name');

        if ($yaSettings['yandex_metrika_enabled']->value && $yaSettings['yandex_metrika_demo']->value) {
            $date1 = new \DateTime($date1);
            $date2 = new \DateTime($date2);
            $result = [
                'demo' => true,
                'period' => [
                    'start' => $date1->format('d.m.Y'),
                    'end'   => $date2->format('d.m.Y'),
                ],
                'total' => [
                    'users' => 0,
                    'newUsers' => 0,
                    'pageDepth' => 0,
                ],
                'users' => [],
                'newUsers' => [],
                'pageDepth' => [],
                'dates' => [],
            ];
            while ($date1 < $date2) {
                $users = rand(0, 50);
                $newUsers = rand(0, 50);
                $pageDepth = rand(($users || $newUsers ? 1 : 0), 1000) / 100;
                $result['users'][] = $users;
                $result['newUsers'][] = $newUsers;
                $result['pageDepth'][] = $pageDepth;
                $result['dates'][] = $date1->format('d.m.Y');

                $result['total']['users'] += $users;
                $result['total']['newUsers'] += $newUsers;
                $result['total']['pageDepth'] += $pageDepth;
                $date1->modify('+1 day');
            }

            $result['total']['pageDepth'] = round($result['total']['pageDepth'] / count($result['dates']), 8);
            return $result;
        }

        try {
            $tokens = json_decode($yaSettings['yandex_metrika_token']->value, true);
        } catch (\Exception $e) {
            $tokens = [];
        }
        if (
            $yaSettings['yandex_metrika_enabled']->value
            && isset($tokens['access_token'])
            && isset($tokens['refresh_token'])
            && isset($tokens['expires_in'])
            && (new \DateTime($tokens['expires_in']) > new \DateTime())
        ) {
            $url = 'https://api-metrika.yandex.ru/stat/v1/data/bytime'
                . '?ids=' . $yaSettings['yandex_metrika_id']->value
                . '&metrics=ym:s:users,ym:s:newUsers,ym:s:pageDepth'
                . '&group=day'
                . '&date1=' . $date1
                . '&date2=' . $date2
            ;

            $response = Curl::get($url, [
                'Authorization: OAuth ' . $tokens['access_token'],
            ]);
            $result = $response['body'];

            $errMessages = [];
            if ($response['error']) {
                $errMessages[] = $response['error'];
            }

            if (isset($result['errors'])) {
                if (is_array($result['errors'])) {
                    foreach ($result['errors'] as $error) {
                        $errMessages[] = $error['message'];
                    }
                }
            }
            if (!empty($errMessages)) {
                self::showMetrikaError($errMessages);
                return null;
            }

            $converted = [
                'period' => [
                    'start' => (new \DateTime($result['query']['date1']))->format('d.m.Y'),
                    'end'   => (new \DateTime($result['query']['date2']))->format('d.m.Y'),
                ],
            ];
            foreach ($result['query']['metrics'] as $i => $metric) {
                $metric = str_replace('ym:s:', '', $metric);
                $metrics[] = $metric;
                $converted['total'][$metric] = $result['totals'][0][$i];
                $converted[$metric] = $result['data'][0]['metrics'][$i];
            }

            foreach ($result['time_intervals'] as $i => $time_interval) {
                if (count($time_interval) ==2 && $time_interval[0] == $time_interval[1]) {
                    $converted['dates'][$i] = (new \DateTime($time_interval[0]))->format('d.m.Y');
                }
            }
            return $converted;
        } else {
            return null;
        }
    }
}
