<?php

$name = 'Настройки';

return [
    'name' => $name,
    'routes' => [
        'admin' => [
            [
                'method' => 'get',
                'uri' => '/{secret?}',
                'parameters' => [
                    'as' => 'admin.settings',
                    'label' => 'Просмотр настроек',
                    'uses' => 'AdminController@index',
                ],
            ],
            [
                'method' => 'post',
                'uri' => '/update',
                'parameters' => [
                    'as' => 'admin.settings.update',
                    'label' => 'Редактирование настроек',
                    'uses' => 'AdminController@update',
                ],
            ],
        ],
    ],
    'navigation' => [
        'admin' => [
            [
                'label' => $name,
                'route'     => 'admin.settings',
                'icon'  => 'fa fa-cogs',
                'order' => 900,
            ],
        ],
    ],
];
