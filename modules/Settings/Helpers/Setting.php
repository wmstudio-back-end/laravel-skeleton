<?php

use Modules\Settings\Entities\Setting;

/**
 * Возвращает настройку из бд по ее имени
 *
 * @param string $name
 * @param $default
 * @return string
 */
function setting($name, $default = null) {
    $setting = Setting::where('name', $name)->first();
    if ($setting) {
        /* @var $setting \Modules\Settings\Entities\Setting */
        return $setting->value;
    }

    return $default;
};


function settings($names = []) {
    $settings = [];
    if (count($names)) {
        $settings = Setting::whereIn('name', $names)->get()->keyBy('name');
        foreach ($settings as $key => $setting) {
            /* @var $setting \Modules\Settings\Entities\Setting */
            $settings[$key] = $setting->value;
        }
    }

    return $settings;
}

/**
 * @return string
 */
function metrikaCounter() {
    $settings = settings([
        'yandex_metrika_enabled',
        'yandex_metrika_javascript',
    ]);

    if ($settings['yandex_metrika_enabled'] && $settings['yandex_metrika_javascript']) {
        return $settings['yandex_metrika_javascript'];
    }

    return '';
}
