<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['no-index' => true], function(){
    Route::group(['prefix' => 'api'], function(){
        Route::middleware('auth:api')->get('/user', function (Request $request) {
            return $request->user();
        });
    });

//    Route::any('/payment/result', ['as' => 'payment.result', 'uses' => 'TariffsController@paymentResult']);
    Route::any('/payment/yandex/notification', [
        'as' => 'payment.result.yandex',
        'uses' => '\Modules\Payments\YandexCassa\Payment@notification'
    ]);
});