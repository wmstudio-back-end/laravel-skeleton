<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['lang']], function() {
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@home', 'label' => 'Лэндинг | Главная']);
    Route::group(['no-index' => true], function() {
        Route::get('/email-confirm/{token}', ['as' => 'email-confirm', 'uses' => 'Auth\VerifyController@emailConfirm']);
        Route::match(['get', 'post'], '/register/last-step', ['as' => 'last-step', 'uses' => 'Auth\VerifyController@lastStep']);
        Route::get('/wait-confirm/{userId}', ['as' => 'wait-confirm', 'uses' => 'Auth\VerifyController@waitConfirm']);
    });

    // Authentication Routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm', 'label' => 'Вход в систему']);
    Route::post('login', 'Auth\LoginController@login');

    // Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm', 'label' => 'Регистрация в системе']);
    Route::post('register', 'Auth\RegisterController@register');

    // Reset password
    Route::match(['get', 'post'], 'password/reset/{token?}', ['as' => 'password.reset',
        'uses' => 'Auth\ResetPasswordController@resetPassword', 'label' => 'Сброс пароля']);

    Route::get('/tariffs', ['middleware' => \App\Http\Middleware\Tariffs::class,
        'as' => 'tariffs', 'uses' => 'TariffsController@index', 'label' => 'Тарифы']);
    Route::post('/tariff/subscribe', ['middleware' => \App\Http\Middleware\Tariffs::class,
        'as' => 'tariff.subscribe', 'uses' => 'TariffsController@subscribe']);
    Route::get('/questions', ['as' => 'faq', 'uses' => 'FAQController@index', 'label' => 'FAQ (Список вопросов)']);
    Route::get('/question/{group}/{alias}', ['as' => 'faq.question', 'uses' => 'FAQController@question', 'label' => 'FAQ (Вопрос)']);
    Route::post('/question/useful/{group}/{alias}', ['as' => 'faq.useful', 'uses' => 'FAQController@useful']);

    Route::group(['middleware' => ['auth']], function() {
        Route::match(['get', 'post'], '/account', ['as' => 'user.account', 'uses' => 'UserController@account']);
        Route::get('/status/change/{status}', ['as' => 'user.status.change', 'uses' => 'UserController@changeProfile',])
            ->where(['status' => 's|t']);

        Route::group(['middleware' => ['auth.type']], function() {
            Route::get('/payments/history', ['as' => 'payments.history', 'uses' => 'PaymentsController@teacher',
                'status' => 't', 'need-active-status' => true]);
            Route::get('/payments', ['as' => 'payments', 'uses' => 'PaymentsController@student',
                'status' => 's', 'need-active-status' => true]);
            Route::any('/payment/result', ['as' => 'payment.result', 'uses' => 'PaymentsController@paymentResult',
                'status' => 's', 'need-active-status' => true]);

            Route::match(['get', 'post'], '/profile', ['as' => 'user.profile', 'uses' => 'UserController@profile']);
            Route::get('/student/{login}', ['as' => 'user.student', 'uses' => 'TeacherController@show',
                'status' => 't', 'need-active-status' => true]);
            Route::get('/teacher/{login}', ['as' => 'user.teacher', 'uses' => 'StudentController@show',
                'status' => 's', 'need-active-status' => true]);

            Route::post('/find/students', ['as' => 'find.students', 'uses' => 'TeacherController@findStudents', 'status' => 't']);
            Route::post('/find/teachers', ['as' => 'find.teachers', 'uses' => 'StudentController@findTeachers', 'status' => 's']);

            Route::match(['get', 'post'], '/tests', ['as' => 'tests', 'uses' => 'TestsController@index',
                'status' => 's', 'need-active-status' => true]);
            Route::post('/save-test-results', ['as' => 'tests.save-results', 'uses' => 'TestsController@saveResults',
                'status' => 's', 'need-active-status' => true]);

            Route::match(['get', 'post'], '/reservation', ['as' => 'user.reservation', 'uses' => 'ReservationController@index']);
            Route::post('/reservation/add', ['as' => 'user.reservation.add', 'uses' => 'ReservationController@add',
                'status' => 't', 'need-active-status' => true]);
            Route::post('/reservation/actions', ['as' => 'user.reservation.actions', 'uses' => 'ReservationController@actions']);

            Route::get('/topics', ['as' => 'topics', 'uses' => 'TopicsController@index']);
            Route::post('/find-topics', ['as' => 'topics.find', 'uses' => 'TopicsController@find']);
            Route::post('/topics/get-teachers', ['as' => 'topics.teachers', 'uses' => 'TopicsController@teachers',
                'status' => 's', 'need-active-status' => true]);
            Route::post('/topics/check', ['as' => 'topics.check', 'uses' => 'TopicsController@check',
                'status' => 't', 'need-active-status' => true]);
            Route::match(['get', 'post'], '/my-topics', ['as' => 'topics.my', 'uses' => 'TeacherController@myTopics',
                'status' => 't', 'need-active-status' => true]);
            Route::post('/my-topics/{action}', ['as' => 'topics.my.edit', 'uses' => 'TeacherController@myTopicsEdit',
                'status' => 't', 'need-active-status' => true])->where(['action' => 'add|edit|delete|validate']);

            Route::post('/api/chat/users', ['as' => 'chat.users',
                'uses' => '\Modules\Messages\Controllers\IndexController@getUsers']);
            Route::post('/api/chat/{userId}/messages', ['as' => 'chat.messages',
                'uses' => '\Modules\Messages\Controllers\IndexController@getMessages']);
            Route::post('/api/chat/{userId}/user-info', ['as' => 'chat.user-info',
                'uses' => '\Modules\Messages\Controllers\IndexController@getUserInfo']);

            Route::get('/free-time', ['as' => 'free-time', 'uses' => 'UserController@freeTime',
                'status' => 's', 'need-active-status' => true]);
            Route::post('/send-invitations', ['as' => 'send-invitations', 'uses' => 'UserController@sendReferralToEmails',
                'status' => 's', 'need-active-status' => true]);
            Route::post('/watch-list/{userId}', ['as' => 'watch-list', 'uses' => 'UserController@watchList']);
            Route::post('/favorites', ['as' => 'favorites', 'uses' => 'UserController@favorites']);
            Route::post('/rating', ['as' => 'rating', 'uses' => 'UserController@rating']);

            Route::post('/send-review', ['as' => 'send.review', 'uses' => '\Modules\Reviews\Controllers\IndexController@addReview']);
        });
    });
});

Route::group(['no-index' => true], function() {
    Route::post('/ask-question', ['as' => 'question.ask', 'uses' => '\Modules\FAQ\Controllers\IndexController@question']);

    Route::match(['get', 'post'], '/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    Route::get('/referral/{token}', ['as' => 'referral-link', 'uses' => 'HomeController@referral']);

//    Route::get('/stat', ['as' => 'stat', 'uses' => 'LaravelController@index']);
//    Route::get('/demo-chat', ['as' => 'videoChat', 'uses' => 'LaravelController@videoChat']);
//    Route::get('/test-chat', function(){
//        return view('test-chat');
//    });
//    Route::get('/test-chats', function(){
//        return view('test-chats');
//    });
});
