let Server                   = function() {};
Server.prototype.async       = require('async');
Server.prototype.cluster     = require('cluster');
Server.prototype.os          = require('os');
Server.prototype.fs          = require('fs');
Server.prototype.mysql       = require('mysql');
Server.prototype.controllers = [];
Server.prototype.server_util = require('./app/modules/server_util')();
Server.prototype.exec        = require("child_process").exec;
Server.prototype.start       = function() {
    let self = this;
    this.async.series(
        [
            function(callback) {
                // Модуль выдающий ошибку по номеру в требуемом формате
                global.getError = require('./app/modules/errors');
                callback(null, 'Error module: [OK]');
            },
            function(callback) {
                // Инициализируем cache, загрузка конфига сервера в систему cache
                self.cacheModule = require('./app/modules/cache');
                callback(null, 'Module Cache + Config: [OK]');
            },
            function(callback) {
                self.dbModule = require('./app/modules/database');
                self.dbModule.newConn(callback);
            },
            function(callback) {
                self.exec("php ../artisan cms:get-session-info", function(error, stdout, stderr) {
                    if(error != null) {
                        console.log(error);
                    }
                    global.laravelConfig          = JSON.parse(stdout);
                    global.laravelLoginWebPostfix = null;

                    callback(null, 'laravelConfig is load')
                })

            },
            function(callback) {
                global.Server.fs.readdir(global.dirRoot + '/app/controllers', function(err, files) {
                    global.Server.client_controllers = [];
                    for(let ActFile in files) {
                        if(files[ActFile][0] === '.') continue;
                        global.Server.client_controllers[files[ActFile].split('.')[0]] =
                            require(global.dirRoot + '/app/controllers/' + files[ActFile])
                    }

                    callback(null, 'client_controllers INIT')
                })
            },
            function(callback) {
                // Загрузка контроллеров инициализация вебсокета
                require('./master/servers/serverws').initialize(callback)
                // callback(null,'WS INIT')
            }

        ],

        // Callback для последовательного исполнения
        function(err, results) {
            // console.log("<--------process------->",global.type)
            if(err) {
                console.log(err);
            } else {
                console.log(results)
            }
        }
    );
};

module.exports = Server;