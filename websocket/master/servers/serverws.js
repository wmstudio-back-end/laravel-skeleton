cookie = require('cookie');

let laravelSession = require('node-laravel-session');
let fs             = require('fs');
let unserialize    = require('php-unserialize').unserialize;
let request        = require('request');
let User           = require('../modules/User');
global.io          = {};
global.USERS       = {};
exports.initialize = initialize;

function initialize(callback) {
    let options = {
            port: process.env.port,
        },
        https   = false;
    if(fs.existsSync(process.env.NODE_SSL_KEY) && fs.existsSync(process.env.NODE_SSL_CERT)) {
        options.key  = fs.readFileSync(process.env.NODE_SSL_KEY);
        options.cert = fs.readFileSync(process.env.NODE_SSL_CERT);
        https        = true;
    }
    let server   = require('https').createServer(options);
    let ioServer = require('socket.io');
    global.io    = new ioServer();
    global.io.attach(server);
    server.listen(process.env.port,{pingTimeout: 7000, pingInterval: 10000});
    global.io.sockets.on('connection', onConnect);

    let address = server.address();
    if(address.address === '::' && address.family === 'IPv6') {
        address.address = '127.0.0.1';
    }
    address = address.address;
    console.log('Server running at ' + (https ? 'https' : 'http') + '://' + address + ':' + process.env.port);
}

onConnect = function(ws) {
    isToken(ws).then(setEvents, function(){
        console.log('UNKNOWN USER. DISCONNECT!');
        ws.disconnect();
    })
};
setEvents = function(data) {
    console.log('USER CONNECT ID', data.ws.uid);
    if (!global.USERS[data.ws.uid]){
        new User(data);
    } else {
        global.USERS[data.ws.uid].addSocket(data.ws);
    }

    data.ws.emit('auth', {
        id: data.ws.uid,
        name: global.USERS[data.ws.uid].fullName || null,
    });
    data.ws.on('message', onMessage);
    data.ws.on('disconnect', onClose);

    data.ws.on('RTC/sendData', function (sendData) {
        let uid = sendData.uid,
            message = sendData.message;
        if (global.USERS[uid] == null) {
            return;
        }
        switch (message.type){
            case 'offer':
                global.USERS[data.ws.uid].rtc.offer = message;
                break;
            case 'answer':
                global.USERS[data.ws.uid].rtc.answer = message;
                break;
            case 'candidate':
                if (message.id === 'audio'){
                    global.USERS[data.ws.uid].rtc.candidate.audio = message;
                }
                if (message.id === 'video'){
                    global.USERS[data.ws.uid].rtc.candidate.video = message;
                }
                break;
            default:
                console.log('RTC/remoteData: unknown message type', message);
                break;
        }

        //global.USERS[1].ws[0].emit('rtc', message);
        //global.USERS[2].ws[0].emit('rtc', message);
        //     if (ws.uid==1){
        //         console.log('rtc???????????????',ws.uid,message.type)
        //         global.USERS[2].ws[0].emit('rtc', message);
        //     }
        //     if (ws.uid==2){
        //         console.log('rtc???????????????',ws.uid,message.type)
        //         global.USERS[1].ws[0].emit('rtc', message);
        //     }
        console.log('send data from ' + data.ws.uid + ' (' + message.type + ')');
        if (global.USERS[data.ws.uid].roomId === global.USERS[uid].roomId) {
            let roomId = global.USERS[data.ws.uid].roomId;
            if (global.USERS[uid].socketTalking) {
                console.log('send data to ' + uid + ' (' + message.type + ')');
                global.USERS[uid].socketTalking.emit('RTC/remoteData', {remoteUid: data.ws.uid, message});
            } else {
                console.log('socketTalking not found');
            }

            // /**
            //  * отправить всем в комнате
            //  */
            // let uids = Object.keys(global.rtcRooms[roomId].users);
            // for (let i = 0; i < uids.length; i++) {
            //     if (global.USERS[uids[i]]&&data.ws.uid!=uids[i]) {
            //         //console.log('SEND TO '+global.rtcRooms[roomId].users[i])
            //         global.USERS[uids[i]].socketCall.ws.emit('RTC/sendData', {message, remoteUid: data.ws.uid});
            //         for (let j = 0; j < global.USERS[global.rtcRooms[roomId].users[i]].ws.length; j++) {
            //             // if (global.USERS[global.rtcRooms[roomId].users[i]].ws[j].calls.hasOwnProperty(data.ws.uid))
            //             // {
            //             //
            //             // }
            //             // global.USERS[global.rtcRooms[roomId].users[i]].ws[j].emit('RTC/sendData', {message, remoteUid: data.ws.uid});
            //         }
            //     }
            //
            // }
        }
        // data.ws.broadcast.emit('rtc', {message, remoteUid: data.ws.uid}); // should be room only
    });

    // ws.on('create or join', function (room) {
    //     console.log('create or join???????????????')
    //     var numClients = io.sockets.clients(room).length;
    //
    //     log('Room ' + room + ' has ' + numClients + ' client(s)');
    //     log('Request to create or join room', room);
    //
    //     if(numClients == 0) {
    //         ws.join(room);
    //         ws.emit('created', room);
    //     }
    //
    //     else if(numClients == 1) {
    //         io.sockets.in(room).emit('join', room);
    //         socket.join(room);
    //         socket.emit('joined', room);
    //     }
    //
    //     else { // max two clients
    //         socket.emit('full', room);
    //     }
    //
    //     ws.emit('emit(): client ' + socket.id + ' joined room ' + room);
    //     ws.broadcast.emit('broadcast(): client ' + socket.id + ' joined room ' + room);
    // });
};

onRtc = function(data){
    console.log('rtc???????????????',message);
    ws.broadcast.emit('rtc', message); // should be room only
};

onMessage = function(data) {
    let method = data.method.split('/');
    console.log("METHOD", method[0] + "/" + method[1] + " " + new Date(), data)
    if(global.Server.client_controllers[method[0]] && global.Server.client_controllers[method[0]][method[1]]) {
        global.Server.client_controllers[method[0]][method[1]](this, data.data, function(err, res) {
            let req = {
                method:    data.method,
                data:      res,
                error:     err,
                requestId: data.requestId
            };
            this.emit('message', req)
        }.bind(this))
    }
};

onClose = function(ws) {

    if (this.uid&&global.USERS[this.uid]){
        global.USERS[this.uid].closeSocket(this)
    }


};

let laravelLoginWebPostfix;
isToken = function(ws) {
    return new Promise((resolve, reject) => {
        let path= '../storage/framework/sessions/',
            wsCookies = cookie.parse(ws.handshake.headers.cookie);
        if (wsCookies[laravelConfig.cookieName] != null){
            let laCookie = wsCookies[laravelConfig.cookieName],
                sessionKey = laravelSession.getSessionKey(laCookie, laravelConfig.appKey);
            if(fs.existsSync(path + sessionKey)) {
                let session = unserialize(fs.readFileSync(path + sessionKey, 'utf8'));
                let userId  = null;
                let status = null;
                if(laravelLoginWebPostfix == null) {
                    for(let key in session) {
                        if(key.indexOf('login_web_') !== -1) {
                            laravelLoginWebPostfix = key.split('login_web_')[1];
                            userId                 = session[key];
                        }
                    }
                } else {
                    if(session['login_web_' + laravelLoginWebPostfix] != null) {
                        userId = session['login_web_' + laravelLoginWebPostfix];
                    }
                }
                if (session['active-status'] != null) {
                    status = session['active-status'];
                }
                if (userId != null) {
                    global.db.query(
                        `SELECT 
                          (SELECT login FROM ${global.dbPrefix}users WHERE id = ${userId}) as login,
                          (SELECT av.value FROM ${global.dbPrefix}user_add_values av
                            LEFT JOIN ${global.dbPrefix}users u on u.id = av.user_id
                            LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                          WHERE
                            u.id = ${userId}
                            AND a.alias = 'name'
                          ) as name,
                          (SELECT av.value FROM ${global.dbPrefix}user_add_values av
                            LEFT JOIN ${global.dbPrefix}users u on u.id = av.user_id
                            LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                          WHERE
                            u.id = ${userId}
                            AND a.alias = 'surname'
                          ) as surname,
                          (SELECT av.value FROM ${global.dbPrefix}user_add_values av
                            LEFT JOIN ${global.dbPrefix}users u on u.id = av.user_id
                            LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                          WHERE
                            u.id = ${userId}
                            AND a.alias = 'middle_name'
                          ) as middle_name,
                          (SELECT av.value FROM ${global.dbPrefix}user_add_values av
                            LEFT JOIN ${global.dbPrefix}users u on u.id = av.user_id
                            LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                          WHERE
                            u.id = ${userId}
                            AND a.alias = 'status'
                          ) as status`,
                        function(error, result) {
                            if (error == null) {
                                result = result.length ? result[0] : null;
                                if (result) {
                                    let userData = {
                                        login: result.login,
                                        name: result.name,
                                        surname: result.surname,
                                        middle_name: result.middle_name,
                                        status: result.status,
                                    };

                                    if (status == null || userData.status.indexOf(status) === -1) {
                                        if (userData.status.indexOf('t') !== -1) {
                                            status = 't';
                                        } else if (userData.status.indexOf('s') !== -1) {
                                            status = 's';
                                        } else {
                                            status = null;
                                        }
                                    }
                                    if (status != null) {
                                        ws.uid = parseInt(userId);
                                        ws.uStatus = status;
                                        resolve({ws, userData});
                                    } else {
                                        reject(null);
                                    }
                                } else {
                                    reject(null);
                                }
                            } else {
                                reject(null);
                            }
                        }
                    );
                } else {
                    reject(null);
                }
            } else {
                reject(null);
            }
        } else {
            reject(null);
        }
    })
};
