let paymentUpdate = require('./paymentUpdate');

module.exports = User;

function User(data) {
    this.timer = 0;
    this.uid = data.ws.uid;
    this.roomId = null;
    this.ws = [];
    this.status = data.userData.status;
    this.fullName = '';
    this.socketCall = {};
    this.socketTalking = false;
    this.callControl = {
        freeTime: 0,
        start: false,
        startTime: null,
        startCallTime: null,
        timer: null,
    };

    this.rtc = {
        offer:  null,
        answer: null,
        candidate: {
            audio: null,
            video: null,
        },
    };
    if (this.status.indexOf('s') !== -1) {
        global.UCOUNTERS.students++;
    }
    if (this.status.indexOf('t') !== -1) {
        global.UCOUNTERS.teachers++;
    }

    global.USERS[data.ws.uid] = this;
    for (let uid in global.USERS) {
        if (global.USERS[uid] === this.uid) {
            continue;
        }
        for (let i = 0; i < global.USERS[uid].ws.length; i++) {
            if (global.USERS[uid].ws[i].observ.indexOf(this.uid) !== -1) {
                global.USERS[uid].ws[i].emit('user_online', {
                    uid: this.uid,
                    online: true,
                    count: FC.getOnlineUsers(global.USERS[uid].ws[i]),
                });
            }
        }
    }
    global.redis.hset(global.redis.onlineKey, this.uid, '');
    this.addSocket(data.ws).setName(data.userData);
}

User.prototype.addSocket = function (ws) {
    if (this.timer) {
        console.log('ClearTimeOut');
        clearTimeout(this.timer);
        this.timer = false;
    }

    ws.observ = [];
    ws.calls = {};
    this.ws.push(ws);
    return this;
};

User.prototype.onExitRoom = function () {
    if (global.rtcRooms[this.roomId] == null) {
        return;
    }
    if (this.callControl.start) {
        let adminUid = global.rtcRooms[this.roomId].adminUid;
        if (adminUid !== this.uid) {
            paymentUpdate(
                adminUid,
                this.uid,
                this.callControl.freeTime,
                this.callControl.startTime,
                this.callControl.startCallTime,
                global.rtcRooms[this.roomId].groupDialog
            ).then(function (stack) {
                for (let i in stack) {
                    for (let module in stack[i]) {
                        stack[i][module] = JSON.stringify(stack[i][module]);
                    }
                }
                console.log(stack);
            }).catch(console.log);
        }
    }
    this.callControl.freeTime = 0;
    this.callControl.start = false;
    this.callControl.startTime = null;
    this.callControl.startCallTime = null;
    if (this.callControl.timer) {
        clearTimeout(this.callControl.timer);
        this.callControl.timer = null;
    }
};

User.prototype.exitRoom = function () {
    if (global.rtcRooms[this.roomId] == null) {
        return;
    }
    console.log("EXIT ROOOMMMM!", this.uid);

    global.rtcRooms[this.roomId].detachUser(this.uid);

    this.roomId = null;
    this.socketCall = {};
    this.socketTalking = false;
};

User.prototype.isCall = function () {
    let uids = Object.keys(this.socketCall);
    let iscall = true;
    for (let i = 0; i < uids.length; i++) {
        if (this.socketCall[uids].status === 0) {
            iscall = false;
        }
    }
};

User.prototype.notify = function (uid, data) {
    if (this.socketCall[uid]) {
        if (Array.isArray(this.socketCall[uid].userWs)) {
            for (let i = 0; i < this.socketCall[uid].userWs.length; i++) {
                this.socketCall[uid].userWs[i].emit('message', data);
            }
        } else {
            this.socketCall[uid].userWs.emit('message', data);
        }
    }
};

// запоминаем вкладку с которой звоним
User.prototype.setTabCall = function (ws, uid, status, adminUid) {
    // status - 0 => был исходящий звонок от this.uid  до uid
    // status - 1 => был входящий звонок от uid до this.uid
    // status - 2 => происходит разговор  this.uid  и  uid
    this.socketCall[uid] = {
        uid:      uid,
        userWs:   ws,
        status:   status,
        adminUid: adminUid,
    };
};

User.prototype.closeSocket = function (ws) {
    let isDelete = false;
    let deleteWs = function (i) {
        console.log('DELETE WS!!!!!');
        isDelete = true;
        global.USERS[this.uid].ws.splice(i, 1);
    }.bind(this);
    // global.USERS[this.uid].ws.splice(i,1);
    // setTimeout(deleteWs.bind(this),1000)

    for (let i = 0; i < global.USERS[this.uid].ws.length; i++) {
        if (global.USERS[this.uid].ws[i].conn.id === ws.conn.id) {
            if (Object.keys(global.USERS[this.uid].socketCall) > 0) {
                // проверяем, небыло ли звонков именно с этого сокета
                let uids = Object.keys(this.socketCall);
                console.log('проверяем, небыло ли звонков именно с этого сокета');

                for (let j = 0; j < uids.length; j++) {
                    if (this.socketCall[uids[j]].status === 0 && this.socketCall[uids[j]].userWs.conn.id === ws.conn.id) {
                        //если с сокета был незавершенный Исходящий звонок
                        // оповещаем пользователя, которому мы звонили, что звонок сброшен

                        global.Server.client_controllers['RTC']['abortCall'](
                            global.USERS[this.uid].ws[i],
                            {
                                uid: uids[j]
                            },
                            function () {
                                delete this.socketCall[uids[j]];
                                delete global.USERS[uids[j]].socketCall[ws.uid];
                                deleteWs(i);
                            }.bind(this));
                    } else if (this.socketCall[uids[j]].status === 1) {
                        //проверяем, остались ли сокеты на которые звонок прошел
                        if (this.socketCall[uids[j]].userWs.length > 1) {
                            console.log(Object.keys(this.socketCall[uids[j]].userWs));
                            for (var key in this.socketCall[uids[j]].userWs) {
                                if (this.socketCall[uids[j]].userWs[key].conn.id === ws.conn.id) {
                                    this.socketCall[uids[j]].userWs.splice(key, 1)
                                }
                            }
                        } else {
                            //отправляем звонящему сообщение что звонок сброшен
                            global.Server.client_controllers['RTC']['answerCall'](
                                global.USERS[this.uid].ws[i],
                                {
                                    uid: uids[j],
                                    answer: false
                                },
                                function () {
                                    // console.log(Object.keys(this.socketCall))
                                    delete this.socketCall[uids[j]];
                                    delete global.USERS[uids[j]].socketCall[ws.uid];
                                    deleteWs(i);
                                }.bind(this));
                        }
                    } else {
                        deleteWs(i);
                    }
                }
            } else if (global.USERS[this.uid].socketTalking !== false) {
                // проверяем статус разговора
                if (
                    global.USERS[this.uid].socketTalking
                    && global.USERS[this.uid].socketTalking.conn.id === ws.conn.id
                    && global.USERS[this.uid].roomId)
                {
                    global.USERS[this.uid].exitRoom();
                }
                deleteWs(i);
            }

            if (!isDelete) {
                deleteWs(i);
            }
            break;
        }

    }
    if (global.USERS[this.uid].ws.length === 0 && !global.USERS[this.uid].timer) {
        console.log('SetTimeout For ', this.uid);
        this.socketCall = {};
        this.socketTalking = false;

        // выходим из комнаты
        this.exitRoom();
        global.USERS[this.uid].timer = setTimeout(function () {
            console.log('Exec Timeout For', this.uid);

            if (this.status.indexOf('s') !== -1) {
                global.UCOUNTERS.students--;
            }
            if (this.status.indexOf('t') !== -1) {
                global.UCOUNTERS.teachers--;
            }

            for (let uid in global.USERS) {
                if (uid === this.uid) {
                    continue;
                }
                for (let i = 0; i < global.USERS[uid].ws.length; i++) {
                    if (global.USERS[uid].ws[i].observ.indexOf(this.uid) !== 1) {
                        global.USERS[uid].ws[i].emit('user_online', {
                            uid: this.uid,
                            online: false,
                            count: FC.getOnlineUsers(global.USERS[uid].ws[i]),
                        });
                    }
                }
            }
            global.redis.hdel(global.redis.onlineKey, this.uid);

            delete global.USERS[this.uid];
        }.bind(this), (process.env.APP_ENV === 'production' ? 15000 : 1000));
    }
};

User.prototype.setName = function (user, middleName) {
    let result = '';
    if (user.name != null && user.name !== '') {
        result = user.name;
        if (middleName === true && user.middle_name != null && user.middle_name !== '') {
            result = result + ' ' + user.middle_name;
        }
        if (user.surname != null && user.surname !== '') {
            result = user.surname + ' ' + result;
        }
    } else {
        if (user.login != null && user.login !== '') {
            result = user.login;
        }
    }
    this.fullName = result;
    return this;
};

User.prototype.endTimeCallback = function() {
    // this = ws
    console.log('disconnect user by timeout');
    let user = global.USERS[this.uid];
    if (user.callControl.timer != null) {
        clearTimeout(user.callControl.timer);
        user.callControl.timer = null;
    }
    if (this.connected) {
        if (user.socketTalking) {
            user.socketTalking.emit('message', {
                method:    'RTC/onDestroy',
                data:      global.Errors.getError('master/user', ERR_STUDENT_TIME_EXPIRED),
                error:     null,
                requestId: 1,
            });
        }
        user.exitRoom();
    }
};