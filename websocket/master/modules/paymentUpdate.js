const dateFormat = require('dateformat');

let noDiscount = 0;
function getGroupDiscount() {
    return new Promise((resolve, reject) => {
        global.db.query(
            `SELECT value FROM ${global.dbPrefix}settings
            WHERE name = "group_discount"`,
            function(error, result) {
                if (error != null) {
                    resolve(noDiscount);
                } else if (result.length) {
                    resolve(result[0].value);
                } else {
                    resolve(noDiscount);
                }
            }
        )
    });
}

function getPrice(){
    return new Promise((resolve, reject) => {
        global.db.query(
            `SELECT value FROM ${global.dbPrefix}settings
            WHERE name = "price_per_minute"`,
            function(error, result) {
                if (error != null) {
                    reject(error);
                } else if (result.length) {
                    let price = JSON.parse(result[0].value);
                    if (price.teacher != null) {
                        resolve(price.teacher);
                    } else {
                        reject('Price not found!!!');
                    }
                } else {
                    reject('Price not found!!!');
                }
            }
        )
    });
}

function addMoneyToTeacher(teacherId, usedSeconds, groupDialog) {
    return new Promise((resolve, reject) => {
        let callback = function(discount){
            getPrice().then(function(price){
                let minutes = (usedSeconds / 60);
                price = (price * minutes * ((100 - discount) / 100)).toFixed(2);
                global.db.query(
                    `SELECT av.id, av.value
                    FROM ${global.dbPrefix}user_add_values av
                    LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                    WHERE
                      a.alias = "tc_balance"
                      AND av.user_id = ${teacherId}`,
                    function(error, result) {
                        if (error != null) {
                            reject(error);
                        } else {
                            if (result.length) {
                                price = (parseFloat(price) + parseFloat(result[0].value)).toFixed(2);
                                global.db.query(
                                    `UPDATE ${global.dbPrefix}user_add_values
                                    SET value = ${price}
                                    WHERE id = ${result[0].id}`,
                                    function(error, result) {
                                        if (error != null) {
                                            reject(error);
                                        } else {
                                            resolve(result);
                                        }
                                    }
                                )
                            } else {
                                global.db.query(
                                    `INSERT INTO ${global.dbPrefix}user_add_values(
                                      user_id,
                                      addon_id,
                                      value,
                                      created_at,
                                      updated_at
                                    ) VALUES (
                                      ${teacherId},
                                      (SELECT id FROM ${global.dbPrefix}user_addons WHERE alias = "tc_balance" LIMIT 1),
                                      ${price},
                                      CURRENT_TIMESTAMP,
                                      CURRENT_TIMESTAMP
                                    )`,
                                    function(error, result) {
                                        if (error != null) {
                                            reject(error);
                                        } else {
                                            resolve(result);
                                        }
                                    }
                                )
                            }
                        }
                    }
                );
            }).catch(reject);
        };
        if (groupDialog) {
            getGroupDiscount().then(callback).catch(reject)
        } else {
            callback(noDiscount);
        }
    });
}

function logTime(teacherId, studentId, groupDialog, timeStart, timeEnd) {
    return new Promise((resolve, reject) => {
        global.db.query(
            `INSERT INTO ${global.dbPrefix}payments_time_log(
              teacher_id,
              student_id,
              group_dialog,
              time_start,
              time_end,
              created_at,
              updated_at
            ) VALUES (
              ${teacherId},
              ${studentId},
              ${groupDialog === true},
              "${dateFormat(timeStart, 'yyyy-mm-dd HH:MM:ss')}",
              "${dateFormat(timeEnd, 'yyyy-mm-dd HH:MM:ss')}",
              CURRENT_TIMESTAMP,
              CURRENT_TIMESTAMP
            )`,
            function(error, result) {
                if (error != null) {
                    reject(error);
                } else {
                    resolve(result);
                }
            }
        );
    });
}

function saveTime(paymentsTimeId, timeLeft) {
    return new Promise((resolve, reject) => {
        global.db.query(
            `UPDATE ${global.dbPrefix}payments_time
            SET available_time = ${timeLeft}
            WHERE id = ${paymentsTimeId}`,
            function(error, result) {
                if (error != null) {
                    reject(error);
                } else {
                    resolve(result)
                }
            }
        )
    });
}

function updateFreeTime(userId, addTime) {
    return new Promise((resolve, reject) => {
        global.db.query(
            `SELECT av.id, av.value as free_rime FROM ${global.dbPrefix}user_add_values av
            LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
            WHERE
              a.alias = "st_free_time"
              AND av.user_id = ${userId}`,
            function(error, result) {
                if (error != null) {
                    reject(error);
                } else {
                    if (result.length) {
                        addTime = parseInt(result[0].free_rime) + addTime;
                        global.db.query(
                            `UPDATE ${global.dbPrefix}user_add_values
                            SET value = ${addTime} WHERE id = ${result[0].id}`,
                            function(error, result) {
                                if (error != null) {
                                    reject(error);
                                } else {
                                    resolve(result);
                                }
                            }
                        )
                    } else {
                        global.db.query(
                            `INSERT INTO ${global.dbPrefix}user_add_values(user_id, addon_id, value, created_at, updated_at)
                            VALUES(
                              ${userId},
                              (SELECT id FROM ${global.dbPrefix}user_addons WHERE alias = "st_free_time" LIMIT 1),
                              ${addTime},
                              CURRENT_TIMESTAMP,
                              CURRENT_TIMESTAMP
                            )`,
                            function(error, result) {
                                if (error != null) {
                                    reject(error);
                                } else {
                                    resolve(result);
                                }
                            }
                        )
                    }
                }
            }
        );
    })
}

let paymentUpdate = function(teacherId, studentId, freeTime, startTime, startCallTime, groupDialog) {
    return new Promise((resolve, reject) => {
        let endTime = new Date();
        let usedSeconds = parseInt((endTime - startCallTime) / 1000);
        let stack = [];

        startTime = dateFormat(startTime, 'yyyy-mm-dd HH:MM:ss');
        global.db.query(
            `SELECT pt.id, pt.available_time FROM ${global.dbPrefix}payments_time pt
            LEFT JOIN ${global.dbPrefix}payments_info pi on pi.id = pt.payment_id
            WHERE
              pi.user_id = ${studentId}
              AND pt.from < "${startTime}" AND pt.to > "${startTime}"`,
            function(error, result) {
                if (error != null) {
                    reject(error);
                } else if (result.length) {
                    let paymentsTimeId = result[0].id;
                    let timeLeft = parseInt(result[0].available_time - usedSeconds);
                    let saveTimeCallback = function(result) {
                        stack.push({saveTimeCallback: result});
                        logTime(teacherId, studentId, groupDialog, startCallTime, endTime)
                            .then(function(result){
                                stack.push({logTime: result});
                                addMoneyToTeacher(teacherId, usedSeconds)
                                    .then(function(result){
                                        stack.push({addMoneyToTeacher: result});
                                        resolve(stack);
                                    })
                                    .catch(reject);
                            })
                            .catch(reject);
                    };
                    if (timeLeft < 0) {
                        updateFreeTime(studentId, timeLeft)
                            .then(function(result){
                                stack.push({updateFreeTime: result});
                                timeLeft = 0;
                                saveTime(paymentsTimeId, timeLeft).then(saveTimeCallback).catch(reject);
                            })
                            .catch(reject);
                    } else {
                        saveTime(paymentsTimeId, timeLeft).then(saveTimeCallback).catch(reject);
                    }
                } else {
                    let timeLeft = parseInt(0 - usedSeconds);
                    updateFreeTime(studentId, timeLeft)
                        .then(function(result){
                            stack.push({updateFreeTime: result});
                            logTime(teacherId, studentId, groupDialog, startCallTime, endTime)
                                .then(function(result){
                                    stack.push({logTime: result});
                                    addMoneyToTeacher(teacherId, usedSeconds)
                                        .then(function(result){
                                            stack.push({addMoneyToTeacher: result});
                                            resolve(stack);
                                        })
                                        .catch(reject);
                                })
                                .catch(reject);
                        })
                        .catch(reject);
                }
            }
        );
    });
};

module.exports = paymentUpdate;