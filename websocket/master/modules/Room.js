const waitSeconds = 10;
module.exports.waitSeconds = waitSeconds;

function Room(data) {
    this.users = data.users || {};
    this.adminUid = data.adminUid || null;
    this.roomId = data.roomId || new global.ObjectID().toString();
    this.groupDialog = data.groupDialog || false;
    global.rtcRooms[this.roomId] = this;
}

Room.prototype.detachUser = function(uid) {
    delete this.users[uid];
    global.USERS[uid].onExitRoom();

    let resp = {
        method:    '',
        data:      {},
        error:     null,
        requestId: 1,
    };
    let uids = Object.keys(global.rtcRooms[this.roomId].users);
    if (this.adminUid === uid || uids.length < 2) {
        console.log('ROOM CLOSE!!!!!!!!!');
        resp.method = 'RTC/onDestroy';
        // высылаем всем в комнате событие о закрытии комнаты
        for (let i = 0; i < uids.length; i++) {
            global.USERS[uids[i]].onExitRoom();
            global.USERS[uids[i]].socketTalking.emit('message',resp);
            global.USERS[uids[i]].socketTalking = false;
            global.USERS[uids[i]].socketCall = {};
            global.USERS[uids[i]].roomId = null;
        }
        delete global.rtcRooms[this.roomId];
    } else {
        resp.method = 'RTC/onReject';
        resp.data = {remoteUid: uid};
        // высылаем всем в комнате событие
        for (let i = 0; i < uids.length; i++) {
            if (global.USERS[uids[i]].socketTalking) {
                global.USERS[uids[i]].socketTalking.emit('message',resp);
            }
        }
    }
};

Room.prototype.addUser = function(uid, status = 0) {
    if (uid !== this.adminUid) {
        let studentWs = global.USERS[uid].socketTalking;
        // Выставляем таймер на 10 секунд для инициализации видео/аудио потока
        //  * Если не ответили, то сбрасываем трубку.
        global.USERS[studentWs.uid].callControl.timer= setTimeout(
            global.USERS[studentWs.uid].endTimeCallback.bind(studentWs),
            waitSeconds * 1000
        );
    }

    uid = parseInt(uid);

    global.USERS[uid].roomId = this.roomId;
    if (Object.keys(this.users).length > 2) {
        this.groupDialog = true;
    }
    for (let roomUid in this.users) {
        roomUid = parseInt(roomUid);
        if (global.USERS[roomUid] != null && roomUid !== uid) {
            // оповещаем всех в комнате что появился новый пользователь
            global.USERS[roomUid].socketTalking.emit("message", {
                method:    'RTC/onJoin',
                data:      {
                    remoteUid:   uid,
                    remoteName:  global.USERS[uid].fullName,
                    createOffer: false,
                },
                error:     null,
                requestId: 1,
            });
            // оповещаем пользователя о всех пользователях в комнате
            global.USERS[uid].socketTalking.emit("message", {
                method:    'RTC/onJoin',
                data:      {
                    remoteUid:   roomUid,
                    remoteName:  global.USERS[roomUid].fullName,
                    createOffer: true,
                },
                error:     null,
                requestId: 1,
            });
        }
    }
    this.users[uid] = status;
};

Room.prototype.setAdmin = function(uid){
    this.adminUid = uid;
};

module.exports.Room = Room;