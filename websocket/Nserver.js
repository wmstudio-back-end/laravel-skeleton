let http   = require('http');
let server = http.createServer();
let io     = require('socket.io')(server);
let board  = require('rtc-switch')();
let port   = process.env.PORT || 3000;

io.on('connection', function(socket) {
    let peer = board.connect();

    socket.on('rtc-signal', peer.process);
    peer.on('data', function(data) {
        socket.emit('rtc-signal', data);
    });
});

server.listen(port, function(err) {
    if(err) {
        return console.error('could not start server: ', err);
    }

    console.log('server running @ http://localhost:' + port);
});