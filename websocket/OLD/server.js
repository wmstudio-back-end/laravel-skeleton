let config = require('./config');

let port = config.port || 7166;
let server;
if (config.ssl.enable) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
    let https = require('https');

    let fs = require('fs');
    let privateKey = fs.readFileSync(config.ssl.key, 'utf8');
    let certificate = fs.readFileSync(config.ssl.cert, 'utf8');

    let options = {
        key: privateKey,
        cert: certificate,
        port: port,
    };

    server = https.createServer(options);
} else {
    let http = require('http');
    server = http.createServer({
        port: port,
    });
}

let io = require('socket.io')(server,{
    pingInterval: 10000,
    pingTimeout: 15000
});

let startMsg = '';
server.listen(port, process.env.IP || '0.0.0.0', function(error){
    let addr = server.address();

    if (addr.address === '0.0.0.0') {
        addr.address = 'localhost';
    }

    let domainURL = (config.ssl.enable ? 'https' : 'http') + '://' + addr.address + ':' + addr.port + '/';

    startMsg += "--------------------------\n";
    startMsg += "socket.io is listening at:\n";
    startMsg += "\x1b[31m  " + domainURL + "\x1b[0m\n";

    if (config.ssl.enable) {
        startMsg += "\x1b[31m  ssl enabled\x1b[0m\n";
    }

    if (addr.address !== 'localhost' && !config.ssl.enable) {
        startMsg += "Warning:\n";
        startMsg += "\x1b[31mPlease use SSL to make sure audio, video and screen demos can work as well.\x1b[0m\n";
    }
    startMsg += "--------------------------\n";
    if (config.logOnlyLast) {
        process.stdout.write('\033c');
    }
    console.log(startMsg);
});

let request = require('request');

let users = {
    length: 0,
    online: {},
};


io.on('connection', function(socket){
    function api(options, callback) {
        let get = {
            url: socket.handshake.headers.origin + '/api/sockets/token',
            headers: {
                'Cookie'  : socket.handshake.headers.cookie,
                'X-Requested-With': 'XMLHttpRequest',
                'api-key' : options.apiKey,
            }
        };
        request(get, function(error, response, body){
            if (!error) {
                let token = JSON.parse(body).token;
                if (options.url === undefined) {
                    options.url = '';
                }

                options.url = socket.handshake.headers.origin + '/api/sockets' + (options.url === '' ? '' : '/') + options.url;
                if (options.headers === undefined) {
                    options.headers = {};
                }

                if (options.form === undefined) {
                    options.form = {}
                }
                options.form['_token'] = token;

                options.headers['Cookie'] = socket.handshake.headers.cookie;
                options.headers['X-Requested-With'] = 'XMLHttpRequest';
                options.headers['api-key'] = options.apiKey;
                request.post(options, callback);
            }
        });
    }
    function cookies(){
        let tmp = socket.handshake.headers.cookie.split(';'),
            cookies = {};
        for (let i in tmp) {
            tmp[i] = tmp[i].split('=');
            cookies[tmp[i][0].trim()] = tmp[i][1];
        }

        return cookies;
    }
    function sendOnline(){
        io.emit('online', users.online);
    }

    api({
        form: {
            actions: []
        }
    }, function(error, response, body){
        if (!error) {
            let res = JSON.parse(body);
            if (res.error || res.id === undefined) {
                socket.disconnect();
            } else {
                socket.emit('consoleMsg', 'Connected to socket.io.');
            }

            if (res.name == null) {
                res.name = res.id;
            }
            if (users[res.id] == null) {
                users[res.id] = res;
                users[res.id]['connections'] = [socket];
                users.online[res.id] = config.userStatus.free;
                users.length++;
                if (config.log) {
                    let msg = "User \"%s\" connected\nCount of connected users: %s";
                    if (config.logOnlyLast) {
                        process.stdout.write('\033c');
                        console.log(startMsg + msg, users[res.id].name, users.length);
                    } else {
                        console.log(msg + "\n", users[res.id].name, users.length);
                    }
                }
            } else {
                users[res.id]['connections'].push(socket);
            }
            socket.userId = res.id;
            socket.canStream = false;
            socket.emit('getStatus');
            sendOnline();
        }
    });

    /*socket.on('message', function(data){
        if (data.message !== undefined && ((data.to === undefined && config.messageToAll) || (data.to !== undefined && users[data.to] !== undefined))) {
            let from = getCurrentUser(socket),
                time = new Date();

            if (data.to === undefined && config.messageToAll) {
                for (let id in users) {
                    for (let i in users[id].connections) {
                        users[id].connections[i].emit('message', {
                            from: from,
                            time: time,
                            message: data.message,
                        });
                    }
                }
                if (config.log) {
                    console.log('message from ' + from + ' to all: ' + data.message);
                }
            } else if (data.to !== undefined) {
                for (let i in users[data.to].connections) {
                    users[data.to].connections[i].emit('message', {
                        from: from,
                        time: time,
                        message: data.message,
                    });
                }
                if (config.log) {
                    console.log('message from ' + from + ' to ' + users[data.to].name + ': ' + data.message);
                }
            }
        }
    });*/

    socket.on('setStatus', function(status){
        if (status && users.online[socket.userId] != null) {
            let hasStatus = false;
            for (let i in config.userStatus) {
                if (config.userStatus[i] === status) {
                    hasStatus = true;
                    break;
                }
            }

            if (hasStatus) {
                users.online[socket.userId] = status;
                if (status === config.userStatus.canStream) {
                    socket.canStream = true;
                }
                sendOnline();
            }
        }
    });

    socket.on('streamInit', function(data){
        if (users.online[socket.userId] != null) {
            users.online[socket.userId] = config.userStatus.busy;
        }
        if (data.userId && users[data.userId] && data.data) {
            for (let i in users[data.userId].connections) {
                if (users[data.userId].connections[i].canStream) {
                    users.online[users[data.userId].connections[i].userId] = config.userStatus.busy;
                    users[data.userId].connections[i].callTo = socket.userId;
                    socket.callTo = data.userId;
                    users[data.userId].connections[i].emit('streamInit', {
                        userId: socket.userId,
                        data: data.data,
                    });
                    break;
                }
            }
        }
        sendOnline();
    });
    socket.on('streamClose', streamClose);
    function streamClose() {
        if (users.online[socket.userId] != null) {
            users.online[socket.userId] = config.userStatus.canStream;
        }
        let userId = socket.callTo;
        delete socket.callTo;
        if (userId != null && users[userId] != null) {
            users.online[userId] = config.userStatus.canStream;
            for (let key in users[userId].connections) {
                users[userId].connections[key].emit('streamClose');
                if (users[userId].connections[key].callTo != null) {
                    delete users[userId].connections[key].callTo;
                }
            }
        }
        sendOnline();
    }

    socket.on('disconnect', function(){
        for (let i in users[socket.userId].connections) {
            if (users[socket.userId].connections[i].id === socket.id) {
                if (users[socket.userId].connections[i].callTo != null) {
                    streamClose();
                }
                users[socket.userId].connections.splice(i, 1);
                if (users[socket.userId].connections.length === 0) {
                    let user = users[socket.userId].name;
                    users.length--;
                    delete users[socket.userId];
                    delete users.online[socket.userId];
                    if (config.log) {
                        let msg = "User \"%s\" disconnected\nCount of connected users: %s";
                        if (config.logOnlyLast) {
                            process.stdout.write('\033c');
                            console.log(startMsg + msg + "\n" + socket.userId, user, users.length);
                        } else {
                            console.log(msg + "\n", user, users.length);
                        }
                    }
                    stop = true;
                    break;
                }
            }
        }
        sendOnline();
    });
});