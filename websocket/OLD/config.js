let fs = require('fs');
let ssl = {
    enable: false,
};

if (fs.existsSync(__dirname + '/ssl.js')) {
    ssl = require('./ssl');
    if (!fs.existsSync(ssl.key) || !fs.existsSync(ssl.cert)) {
        ssl.enable = false;
    }
}

module.exports = {
    apiKey: 'djy2wyn87n1XadBacE9iuFe6D3NaeslMjr2eQjN7Sr0DgSKV0clR4wcVupdoWt7HCbr20apAcrLcvrYEdCiTnWrb3BJzn5lcRHvnxkTreye2QW60wc2zDOQi2C7x3ZyV',
    port: 9090,
    ssl: ssl,
    messageToAll: true,
    log: true,
    logOnlyLast: true,
    userStatus: {
        busy:      0,
        free:      1,
        canStream: 2,
    },
};