require('dotenv').config({path: '../.env'});
let defPort = 9090;
if (process.env.APP_ENV === 'production') {
    process.env.port = process.env.MIX_NODE_PROD_PORT || defPort;
} else {
    process.env.port = process.env.MIX_NODE_DEV_PORT || defPort;
}

let Errors = require('./app/modules/errors');
global.Errors   = new Errors();

Server        = require('./server');
global.argv   = require('minimist')(process.argv.slice(2));
global.config = global.argv;
console.log(global.config);
global.rtcRooms  = {};
global.ObjectID  = require('mongodb').ObjectID;
global.dirRoot   = __dirname;
global.USERS     = {};
global.UCOUNTERS = {teachers: 0, students: 0};
FC               = require('./app/modules/functions');
log              = function(type, uid, data) {
    console.log("[" + type + "]" + "{" + uid + "}", data)
};

const redis            = require('redis');
global.redis           = redis.createClient({
    host:     process.env.REDIS_HOST || '127.0.0.1',
    port:     process.env.REDIS_PORT || 6379,
    password: process.env.REDIS_PASSWORD || '',
});
global.redis.onlineKey = process.env.REDIS_ONLINE_KEY || 'onlineUsers';
global.redis.del(global.redis.onlineKey);
process.on('uncaughtException', function(err) {
    console.log(fullLine('UNCAUGHT ERROR START'));
    console.log(new Date());
    console.log(err.message);
    console.log(err.stack);
    console.log(fullLine('UNCAUGHT ERROR END'));
});
process.stdin.resume();

// Чистим Redis перед выходом
function exitHandler(options) {
    if(options.exit) {
        // Рассылаем на все подписки, что пользователь стал оффлайн
        // for(let uid in global.USERS) {
        //     for (let ws = 0; ws < global.USERS[uid].ws.length; ws++) {
        //         for (let observ = 0; observ < global.USERS[uid].ws[ws].observ.length; observ++) {
        //             global.USERS[uid].ws[ws].emit('user_online', {
        //                 uid: global.USERS[uid].ws[ws].observ[observ],
        //                 online: false,
        //                 count: 0,
        //             });
        //         }
        //     }
        // }
        console.log('\nclear Redis Online Users', global.redis.del(global.redis.onlineKey));
        process.exit();
    }
}

//do something when app is closing
process.on('exit', exitHandler.bind(null, {cleanup: true}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit: true}));

// catches "kill pid" (for example: nodemon restart)
process.on('SIGUSR1', exitHandler.bind(null, {exit: true}));
process.on('SIGUSR2', exitHandler.bind(null, {exit: true}));

//catches uncaught exceptions
// process.on('uncaughtException', exitHandler.bind(null, {exit: true}));
// Чистим Redis перед выходом

global.Server = new Server();
global.Server.start();

function fullLine(msg) {
    if (typeof process.stdout.getWindowSize !== 'function') {
        return `<-------- ${msg} -------->`;
    }
    let wSize = process.stdout.getWindowSize()[0];
    if(msg.length >= wSize) {
        return msg;
    }

    let line = '<';
    for(let i = 0; i < wSize - 2; i++) {
        line += '-';
    }
    line += '>';
    line = line.substr(0, ((wSize / 2) - (msg.length + 2) / 2) - 1) +
        ' ' + msg + ' ' +
        line.substr(((wSize / 2) + (msg.length + 2) / 2) - 1);

    return line;
}



