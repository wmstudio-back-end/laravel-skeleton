/**
 * Формирование кода ошибки (1234)
 *  Первые 1-2 цифры - код модуля(файла, класса) из которого вызван
 *  Вторые 2 цифры - код ошибки
 */

ERR_UNKNOWN_ERROR             = 1;
ERR_USER_OFFLINE              = 2;
ERR_USER_BUSY                 = 3;
ERR_USER_DOES_NOT_HAVE_CALLS  = 4;
ERR_USER_DID_NOT_CALL_YOU     = 5;
ERR_CALL_IS_NOT_ALLOWED       = 6;
ERR_CONNECTION_DOES_NOT_EXIST = 7;
ERR_ROOM_NOT_FOUND            = 8;
ERR_STUDENT_TIME_EXPIRED      = 9;
ERR_USER_STATUS               = 10;
ERR_DATABASE_QUERY            = 11;

let codes = {
    modules:  {
        rtc:            1,
        "master/user":  2,
    },
    messages: {
        ERR_UNKNOWN_ERROR:             "unknown error",
        ERR_USER_OFFLINE:              "user offline",
        ERR_USER_BUSY:                 "user busy",
        ERR_USER_DOES_NOT_HAVE_CALLS:  "user doesn't have calls",
        ERR_USER_DID_NOT_CALL_YOU:     "user didn't call you",
        ERR_CALL_IS_NOT_ALLOWED:       "call is not allowed",
        ERR_CONNECTION_DOES_NOT_EXIST: "connection does not exist",
        ERR_ROOM_NOT_FOUND:            "room not found",
        ERR_STUDENT_TIME_EXPIRED:      "user time has expired",
        ERR_USER_STATUS:               "user status error",
        ERR_DATABASE_QUERY:            "error during sql query exec",
    },
};

module.exports = codes;
