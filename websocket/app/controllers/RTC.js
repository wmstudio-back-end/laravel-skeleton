const dateFormat  = require('dateformat');
const { Room, waitSeconds } = require('../../master/modules/Room');

module.exports.videoInit          = videoInit;
module.exports.call               = call;
module.exports.abortCall          = abortCall;
module.exports.secondaryCall      = secondaryCall;
module.exports.abortSecondaryCall = abortSecondaryCall;
module.exports.answerCall         = answerCall;
module.exports.rejectCall         = rejectCall;
module.exports.sendMessage        = sendMessage;

/**
 * Пользователь отправляет текстовое сообщение в комнату
 */
function sendMessage(ws, mess, response) {
    let roomId = global.USERS[ws.uid].roomId;
    if (roomId != null && global.rtcRooms[roomId] != null) {
        let data = {
            method:    'RTC/onMessage',
            data:      {
                uid:     ws.uid,
                name:    global.USERS[ws.uid].fullName,
                message: mess.message,
            },
            error:     null,
            requestId: 1,
        };
        let uids = Object.keys(global.rtcRooms[roomId].users);
        for (let uid in global.rtcRooms[roomId].users) {123456

            uid = parseInt(uid);
            if (uid !== ws.uid && global.USERS[uid] != null && global.USERS[uid].socketTalking != null) {
                global.USERS[uid].socketTalking.emit('message', data);
            }
        }
        response(null, {result: true});
    } else {
        response(global.Errors.getError('rtc', ERR_ROOM_NOT_FOUND), null);
    }

}

/**
 * Пользователь оповещает, что видео инициировалось.
 *  Стартуем таймер.
 */
function videoInit(ws, mess, response) {
    let roomId = global.USERS[ws.uid].roomId;
    if (roomId != null && global.rtcRooms[roomId] != null) {
        let adminId = global.rtcRooms[roomId].adminUid;
        if (adminId != null && ws.uid !== adminId && !global.USERS[ws.uid].callControl.start) {
            global.USERS[ws.uid].callControl.start = true;
            if (global.USERS[ws.uid].callControl.timer != null) {
                clearTimeout(global.USERS[ws.uid].callControl.timer);
            }
            global.USERS[ws.uid].callControl.startCallTime = new Date();
            global.USERS[ws.uid].callControl.timer
                = setTimeout(global.USERS[ws.uid].endTimeCallback.bind(ws), global.USERS[ws.uid].callControl.freeTime * 1000);
        }
    }
}

/**
 * Сбросить вторичный исходящий звонок
 *  Пользователь [ws.uid] прекратил звонок для [mess.uid]
 *  (userId => result:bool, error)
 */
function abortSecondaryCall(ws, mess, response) {
    if (global.USERS[mess.uid] == null) {
        response(global.Errors.getError('rtc', USER_OFFLINE), null);
        return;
    }
    //данные для инициатора звонка
    let resp                          = {
        method:    'RTC/onDestroy',
        data:      {},
        error:     null,
        requestId: 1,
    };
    global.USERS[mess.uid].socketCall = false;
    // Отправляем во все вкладки пользователя [mess.uid] которому звоним,  событие что звонок отклонили
    for (let i = 0; i < global.USERS[mess.uid].ws.length; i++) {
        global.USERS[mess.uid].ws[i].emit('message', resp);
    }
    response(null, {result: true});
}

/**
 * Вторичный исходящий звонок
 *  (userId => result:bool, error)
 */
function secondaryCall(ws, mess, response) {
    if (global.USERS[mess.uid] == null) {
        response(global.Errors.getError('rtc', ERR_USER_OFFLINE), null);
        return;
    }
    // если принимающий звонок пользователь занят
    if (Object.keys(global.USERS[mess.uid].socketCall).length>0) {
        response(global.Errors.getError('rtc', ERR_USER_BUSY), null);
        return;
    }
    // если принимающий звонок пользователь занят
    if (global.USERS[mess.uid].socketTalking) {
        response(global.Errors.getError('rtc', ERR_USER_BUSY), null);
        return;
    }
    // если сокетов у пользователя нет
    if (!global.USERS[mess.uid].ws[0]) {
        response(global.Errors.getError('rtc', ERR_CONNECTION_DOES_NOT_EXIST), null);
        return;
    }

    let adminUid = getAdmin(ws, global.USERS[mess.uid].ws[0], true);
    // Если звонок не разрешен
    if (adminUid !== ws.uid) {
        response(global.Errors.getError('rtc', ERR_CALL_IS_NOT_ALLOWED), null);
        return;
    }

    let nextEvent = function() {
        // Запоминаем сокет, с которой был осуществлен звонок, пользователя, которому звоним и статус
        global.USERS[ws.uid].setTabCall(ws, mess.uid, 0, adminUid);

        let resp = {
            method:    'RTC/onCall',
            data:      {
                remoteUid:  ws.uid,
                sound:      true,
            },
            error:     null,
            requestId: 1,
        };
        let remoteWs = [];
        // Отправляем во все вкладки принимающего событие входящего звонка
        for (let i = 0; i < global.USERS[mess.uid].ws.length; i++) {
            remoteWs.push(global.USERS[mess.uid].ws[i]);
            //отправляем событие входящего звонка на сокет принимающего
            global.USERS[mess.uid].ws[i].emit('message', resp);
            // проигрываем звук только на первой вкладке
            resp.data.sound = false;
        }
        //Делаем пометку что у пользователя есть входящий звонок
        console.log('Делаем пометку что у пользователя есть входящий звонок',remoteWs.length);
        global.USERS[mess.uid].setTabCall(remoteWs,ws.uid, 1, adminUid);
        //ответ инициатору вторичного звонка
        response(null, {result: true});
    };

    let studentUid = ws.uid === adminUid ? mess.uid : ws.uid;
    canCall(studentUid, adminUid).then(function(){
        nextEvent();
    }).catch(function(error){
        response(error, null);
    });
}

/**
 * Положить трубку во время звонка
 *  Пользователь [ws.uid] положит трубку во время звонка
 *  ({} => result:bool, error)
 */
function rejectCall(ws, mess, response) {
    if (!global.USERS[ws.uid].roomId) {
        response(global.Errors.getError('rtc', ERR_ROOM_NOT_FOUND), null);
        return;
    }
    //выходим из комнаты
    global.USERS[ws.uid].exitRoom();
    response(null, {result: true});
}

/**
 * Ответ на входящий звонок
 *  Пользователь [ws.uid] принял входящий звонок или отказался от него
 *  (userId, answer[true/false] => result:bool, error)
 */
function answerCall(ws, mess, response) {
    if (global.USERS[mess.uid] == null) {
        response(global.Errors.getError('rtc', ERR_USER_OFFLINE), null);
        return;
    }

    if (
        global.USERS[ws.uid].socketCall[mess.uid] == null
        || global.USERS[mess.uid].socketCall[ws.uid] == null
        || global.USERS[mess.uid].socketCall[ws.uid].userWs == null
        || global.USERS[mess.uid].socketCall[ws.uid].userWs.uid == null
    ) {
        response(global.Errors.getError('rtc', ERR_USER_DID_NOT_CALL_YOU), null);
        return;
    }

    // находим админа
    let adminUid = global.USERS[mess.uid].socketCall[ws.uid].adminUid;
    console.log('adminUid', adminUid);

    let respAnswer = {};

    // звонок ПРИНЯТ
    if (mess.answer) {
        //если комната у звонящего уже существует, значит добавляем в текущую комнату
        console.log("START ANSWER TRUE");
        // Запоминаем сокет, на котором был принят звонок
        global.USERS[ws.uid].socketTalking = ws;
        // Меняем статус у звонящего, что звонок состоялся
        global.USERS[mess.uid].socketTalking = global.USERS[mess.uid].socketCall[ws.uid].userWs;
        // оповещаем все вкладки кроме текущей, что звонок принят в текущей вкладке
        for (let i = 0; i < global.USERS[ws.uid].socketCall[mess.uid].userWs.length; i++) {
            if (ws.conn.id !== global.USERS[ws.uid].socketCall[mess.uid].userWs[i].conn.id) {
                global.USERS[ws.uid].socketCall[mess.uid].userWs[i].emit('message',{
                    method:    'RTC/onDestroy',
                    data:      {},
                    error:     null,
                    requestId: 1,
                })
            }
        }


        respAnswer = {result:true};
        let respCaller = {
            method:    'RTC/onAnswer',
            data:      {
                remoteUid:  ws.uid,
                admin:      adminUid === mess.uid,
            },
            error:     null,
            requestId: 1,
        };
        if (mess.uid !== adminUid) {
            respCaller.data.timeLeft = global.USERS[mess.uid].callControl.freeTime + waitSeconds;
        } else {
            respAnswer.timeLeft = global.USERS[ws.uid].callControl.freeTime + waitSeconds;
        }

        //проверяем, существует ли уже комната
        if (!global.USERS[mess.uid].roomId) {
            //если комната не существует
            // создаем новую комнату для пользователей

            let room = new Room({adminUid});
            // добавляем в комнату пользователей
            room.addUser(mess.uid,0);
            room.addUser(ws.uid,1);

            respAnswer.admin = adminUid === ws.uid;
            response(null, respAnswer);
        } else {
            //добавляем пользователя в комнату
            global.rtcRooms[global.USERS[mess.uid].roomId].addUser(ws.uid, 1);
            response(null, respAnswer);
        }

        // оповещаем вкладку исходящего звонка что вызов принят
        global.USERS[mess.uid].socketTalking.emit("message", respCaller);

        //обнуляем статусы о активном дозвоне
        global.USERS[mess.uid].socketCall = {};
        global.USERS[ws.uid].socketCall = {};
    } else {
        // звонок ОТКЛОНЕН
        let resp = {
            method:    '',
            data:      {},
            error:     null,
            requestId: 1,
        };

        if (global.USERS[mess.uid].roomId == null) {
            resp.method = 'RTC/onDestroy';
            resp.data   = {};
        } else {
            resp.method = 'RTC/onReject';
            resp.data   = {
                remoteUid: ws.uid
            };
        }

        //оповещаем вкладку исходящего звонка что вызов отклонен
        global.USERS[mess.uid].notify(ws.uid,resp);
        //оповещаем вкладку входящего звонка что вызов отклонен
        global.USERS[ws.uid].notify(mess.uid,resp);


        delete global.USERS[mess.uid].socketCall[ws.uid];
        delete global.USERS[ws.uid].socketCall[mess.uid];
        response(null, respAnswer);
    }
}

/**
 * Сбросить первичный исходящий звонок
 *  Пользователь [ws.uid] прекратил звонок для [mess.uid]
 *  (userId => result:bool, error)
 */
function abortCall(ws, mess, response) {
    // удаляем все звонки
    delete global.USERS[mess.uid].socketCall[ws.uid];
    delete global.USERS[ws.uid].socketCall[mess.uid];
    if (global.USERS[mess.uid] == null) {
        response(global.Errors.getError('rtc', ERR_USER_OFFLINE), null);
        return;
    }
    // данные для инициатора звонка
    let resp = {
        method:    'RTC/onDestroy',
        data:      {},
        error:     null,
        requestId: 1,
    };

    // Отправляем во все вкладки пользователя [mess.uid] которому звоним,  событие что звонок отклонили
    for (let i = 0; i < global.USERS[mess.uid].ws.length; i++) {
        global.USERS[mess.uid].ws[i].emit('message', resp);
    }
    response(null, {result: true});
}

/**
 * Первичный исходящий звонок
 *  (userId => result:bool, error)
 */

function call(ws, mess, response) {
    // проверяем, онлайн пользователь которому звоним
    if (global.USERS[mess.uid] == null) {
        response(global.Errors.getError('rtc', ERR_USER_OFFLINE), null);
        return;
    }

    if (global.USERS[mess.uid].ws.length === 0) {
        response(global.Errors.getError('rtc', ERR_USER_OFFLINE), null);
        return;
    }

    // если принимающий звонок пользователь занят
    if (Object.keys(global.USERS[mess.uid].socketCall).length>0) {
        response(global.Errors.getError('rtc', ERR_USER_BUSY), null);
        return;
    }
    if (global.USERS[mess.uid].socketTalking) {
        response(global.Errors.getError('rtc', ERR_USER_BUSY), null);
        return;
    }

    // Получаем админа
    let adminUid = getAdmin(ws, global.USERS[mess.uid].ws[0], null);
    // Если звонок не разрешен
    if (adminUid === false) {
        response(global.Errors.getError('rtc', ERR_CALL_IS_NOT_ALLOWED), null);
        return;
    }

    let nextEvent = function() {
        // Запоминаем сокет, с которой был осуществлен ИСХОДЯЩИЙ звонок, пользователя, которому звоним и статус
        global.USERS[ws.uid].setTabCall(ws, mess.uid, 0, adminUid);

        let resp = {
            method:    'RTC/onCall',
            data:      {
                remoteUid:  ws.uid,
                sound:      true,
            },
            error:     null,
            requestId: 1,
        };

        let remoteWs = [];
        // Отправляем во все вкладки принимающего событие входящего звонка
        for (let i = 0; i < global.USERS[mess.uid].ws.length; i++) {
            remoteWs.push(global.USERS[mess.uid].ws[i]);
            //отправляем событие входящего звонка на сокет принимающего
            global.USERS[mess.uid].ws[i].emit('message', resp);
            // проигрываем звук только на первой вкладке
            resp.data.sound = false;
        }
        //Делаем пометку что у пользователя есть входящий звонок
        global.USERS[mess.uid].setTabCall(remoteWs, ws.uid, 1, adminUid);
        //ответ инициатору звонка
        response(null, {result: true});
    };

    let studentUid = ws.uid === adminUid ? mess.uid : ws.uid;
    canCall(studentUid, adminUid).then(function(){
        nextEvent();
    }).catch(function(error){
        response(error, null);
    });
}

function getAdmin(callerWs, answererWs, callerIsAdmin) {
    let caller = {
        uid:        callerWs.uid,
        wsStatus:   callerWs.uStatus,
        userStatus: global.USERS[callerWs.uid].status,
    }, answerer  = {
        uid:        answererWs.uid,
        wsStatus:   answererWs.uStatus,
        userStatus: global.USERS[answererWs.uid].status,
    };

    if (callerIsAdmin === true) {
        if (answerer.wsStatus === 's' || answerer.userStatus.indexOf('s') !== -1) {
            return callerWs.uid;
        } else {
            return false;
        }
    }

    if (caller.wsStatus === answerer.wsStatus) {
        if (caller.wsStatus === 't' && answerer.userStatus.indexOf('s') !== -1) {
            return caller.uid;
        } else if (caller.wsStatus === 's' && answerer.userStatus.indexOf('t') !== -1) {
            return answerer.uid;
        } else {
            return false;
        }
    } else {
        if (caller.wsStatus === 't') {
            return caller.uid;
        } else {
            return answerer.uid;
        }
    }
}

function canCall(studentUid, teacherUid) {
    let now = new Date(),
        formattedNow = dateFormat(now, 'yyyy-mm-dd HH:MM:ss');
    let teacherIsBusyForStudent = function() {
        return new Promise((resolve, reject) => {
            global.db.query(
                `SELECT reserved_user_id as uid
                FROM ${global.dbPrefix}reservations
                WHERE
                  user_id = ${teacherUid}
                  AND time < "${formattedNow}"
                  AND (time + INTERVAL duration MINUTE) > "${formattedNow}"`,
                function(error, result) {
                    if (error != null) {
                        reject(error);
                    } else {
                        if (result.length) {
                            let reservedUids = {};
                            for (let i in result) {
                                reservedUids[result[i].uid] = true;
                            }
                            if (reservedUids[studentUid] === true) {
                                resolve(false);
                            } else {
                                resolve(true);
                            }
                        } else {
                            resolve(false);
                        }
                    }
                }
            );
        });
    };

    return new Promise((resolve, reject) => {
        new Promise((resolve, reject) => {
            global.db.query(
                `SELECT IFNULL(
                  (SELECT pt.available_time FROM ${global.dbPrefix}payments_time pt
                  LEFT JOIN ${global.dbPrefix}payments_info pi on pi.id = pt.payment_id
                  WHERE
                    pi.user_id = ${studentUid}
                    AND pt.from < "${formattedNow}" AND pt.to > "${formattedNow}"
                  ) , 0
                ) + IFNULL(
                  (SELECT av.value as free_rime FROM ${global.dbPrefix}user_add_values av
                  LEFT JOIN ${global.dbPrefix}user_addons a on a.id = av.addon_id
                  WHERE
                    a.alias = "st_free_time"
                    AND av.user_id = ${studentUid}
                  ), 0
                ) as available_time`,
                function(error, result) {
                    if (error != null) {
                        reject(error);
                    } else {
                        if (result.length === 1) {
                            resolve(parseInt(result[0].available_time));
                        } else {
                            resolve(0);
                        }
                    }
                }
            )
        }).then(function(seconds) {
            if (seconds) {
                global.USERS[studentUid].callControl.freeTime = seconds;
                global.USERS[studentUid].callControl.startTime = now;
                if (teacherUid == null) {
                    resolve();
                } else {
                    teacherIsBusyForStudent().then(function(busy){
                        if (busy) {
                            global.USERS[studentUid].callControl.freeTime = 0;
                            global.USERS[studentUid].callControl.startTime = null;
                            if (global.USERS[studentUid].callControl.timer != null) {
                                clearTimeout(global.USERS[studentUid].callControl.timer);
                                global.USERS[studentUid].callControl.timer = null;
                            }
                            reject({
                                ...global.Errors.getError('rtc', ERR_USER_BUSY),
                                ...{uid: teacherUid, name: global.USERS[teacherUid].fullName}
                            });
                        } else {
                            resolve();
                        }
                    }).catch(reject);
                }
            } else {
                reject({
                    ...global.Errors.getError('rtc', ERR_STUDENT_TIME_EXPIRED),
                    ...{uid: studentUid, name: global.USERS[studentUid].fullName}
                })
            }
        }).catch(reject);
    });
}