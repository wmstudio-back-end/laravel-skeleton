module.exports.setObserv = setObserv;
module.exports.addObserv = addObserv;
// module.exports.getOnline = getOnline;
// module.exports.getCountOnline = getCountOnline;

/**
 * Set user monitoring.
 * @param {Object} webSocket object,
 * @param {Object} incoming data.
 * @callback callback function
 */
function setObserv(ws, mess, response) {
    for (let i in mess.uids) {
        mess.uids[i] = parseInt(mess.uids[i]);
    }
    ws.observ = mess.uids.filter((value, index, self)=>{return self.indexOf(value) === index;})
    let t = [];
    for(let i = 0; i < mess.uids.length; i++) {
        global.USERS.hasOwnProperty(mess.uids[i].toString()) ? t.push({
            uid:    mess.uids[i],
            online: true
        }) : t.push({uid: mess.uids[i], online: false})
    }
    response(null, {users: t});
}
/**
 * Add user monitoring.
 * @param {Object} webSocket object,
 * @param {Object} incoming data.
 * @callback callback function
 */
function addObserv(ws, mess, response) {
    for (let i in mess.uids) {
        mess.uids[i] = parseInt(mess.uids[i]);
        if (ws.observ.indexOf(mess.uids[i]) === -1) {
            ws.observ.push(mess.uids[i]);
        }
    }
    let t = [];
    for(let i = 0; i < mess.uids.length; i++) {
        global.USERS[mess.uids[i]] != null ? t.push({
            uid:    mess.uids[i],
            online: true
        }) : t.push({uid: mess.uids[i], online: false})
    }
    response(null, {users: t});
}
/**
 * Set user monitoring.
 * @param {Object} webSocket object,
 * @param {Object} incoming data.
 * @callback callback function
 */
function getOnline(ws, mess, response) {
    let uids = [];
    let excluded = {};
    let limit = mess.limit || 1;
    if (typeof mess.excluded === 'object') {
        if (Array.isArray(mess.excluded) || Object.keys(mess.excluded)[0] === 0) {
            for (let i in mess.excluded) {
                excluded[mess.excluded[i]] = true;
            }
        } else {
            for (let i in mess.excluded) {
                excluded[i] = true;
            }
        }
    } else if (typeof mess.excluded === 'number') {
        excluded[mess.excluded] = true;
    }

    let counter = 0,
        findStatus = ws.uStatus === 't' ? 's' : 't';
    for(let id in global.USERS) {
        if (counter < limit) {
            if (
                id == ws.uid
                || excluded[id] != null
                || global.USERS[id].status.indexOf(findStatus) === -1
            ) {
                continue;
            } else {
                uids.push(id);
            }
        } else {
            break;
        }
        counter++;
    }
    response(null, {
        uids: uids,
        count: FC.getOnlineUsers(ws),
    });
}
function getCountOnline(ws, mess, response) {
    response(null, {
        count: FC.getOnlineUsers(ws),
    });
}
