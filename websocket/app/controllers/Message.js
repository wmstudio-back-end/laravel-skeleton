module.exports.sendMessage = sendMessage;
module.exports.getMessages = getMessages;

function sendMessage(ws, mess, response) {
    let data = {
            sender_id:   ws.uid,
            receiver_id: mess.id,
            message:     mess.text,
            status:      1,
        },
        createdAt = (new Date()).toLocaleString('ru').replace(/-/g, '/');
    global.db.query(
        `INSERT INTO ${global.dbPrefix}messages SET ?`, data,
        function(error, result) {
            data.id = result && result.insertId ? result.insertId : null;
            if (global.USERS[mess.id]) {
                console.log('!!!!!!!!!!!!!!!!!!!!!!!!');
                console.log(global.USERS[mess.id]);
                data.created_at = createdAt;
                let resp = {
                    method:    'Message/sendMessage',
                    data:      data,
                    error:     null,
                    requestId: 1
                };
                for (let i = 0; i < global.USERS[mess.id].ws.length; i++) {
                    global.USERS[mess.id].ws[i].emit('message', resp);

                }
            }
            response(error, data);
        }
    );
}

function getMessages(ws, mess, response) {

    let sql = `SELECT * FROM ${global.dbPrefix}messages
        WHERE (sender=${parseInt(mess.id)} AND receiver=${parseInt(ws.uid)})
            OR (sender=${parseInt(ws.uid)} AND receiver=${parseInt(mess.id)})
        LIMIT ${mess.skip}, ${mess.limit}`;
    global.db.query(sql, function(error, result) {
        response(error, result);
    });
}
