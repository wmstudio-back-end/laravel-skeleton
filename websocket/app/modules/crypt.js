let key = 'Match3';

exports.decrypt = function(data) {
    let count = data.length;
    let ba    = new Buffer(count);
    for(let i = 0; i < count; i++) {
        let val = data.readInt8(i);
        val -= count;
        val -= get_char_obf(key, i % key.length);
        val += 1024;
        val %= 256;
        ba.writeInt8(val, i);
    }
    return ba.toString("UTF-8");
};

exports.encrypt = function(data) {
    let count = data.length;
    let ba    = new Buffer(count);
    for(let i = 0; i < count; i++) {
        let val = data.readInt8(i);
        if(val < 0) {
            val += 256;
        }
        val += get_char_obf(key, i % key.length);
        val += count;
        val %= 256;
        if(val > 127) {
            val -= 256;
        }
        ba.writeInt8(val, i);
    }
    return ba;
};

function get_char_obf(str, chr) {
    return str.charCodeAt(chr);
}

/*exports.decrypt = function(data)
{
    let decrypted_str = '';
	let j = 0;
	for (let i=0; i < data.length; i++)
	{
        //Получаем код символа зашифрованой строки в кодировке UTF-8
        let a = data.charCodeAt(i);//getCodePoint(data, i);
        //Получаем код символа ключа в кодировке UTF-8
        let b = key.charCodeAt(j);//getCodePoint(key, j);
        j++;
        if (j >= key.length)
        {
            j = 0;
        }
        //Находим разницу между двумя кодами символов
        a = a - b;
        //Берем остаток от деления на колличество всех символов в кодировке UTF-8
        a %= 65536;
        //Делаем бинарный сдвиг вправо
        a = a - 25;
        a = a / 3;
        //Получаем символ по его коду в кодировке UTF-8
        decrypted_str = decrypted_str+String.fromCharCode(a);
    }
	return decrypted_str; 
};*/

/*exports.encrypt = function(data)
{
    let tkey = [];
    let keyLength = key.length;
    
    for (let k = 0; k < keyLength; k++)
    { 
        tkey[k] = key.charCodeAt(k);//getCodePoint(key, k);
    }
    
	let encrypted_str = '';
	let j = 0;
	for (let i = 0; i < data.length; i++)
	{
		//Получаем код символа строки в кодировке UTF-8
		let a = data.charCodeAt(i);//getCodePoint(data, i);
        
		//Делаем бинарный сдвиг влево
		a = a * 3 + 25;
        
		//Получаем код символа ключа в кодировке UTF-8
		let b = tkey[j];
		j++;
		if (j >= keyLength)
		{
			j = 0;
		}
		//Находим сумму между двумя кодами символов и остаток от деления на колличество всех символов в кодировке UTF-8
		a = ( a + b ) % 65536;
		//Получаем символ по его коду в кодировке UTF-8 и записываем в результирующую строку
		encrypted_str = encrypted_str+String.fromCharCode( a );
	}

	return encrypted_str;
};*/