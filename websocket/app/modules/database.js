/**
 *
 * @param {Function} callback
 */
exports.newConn = function(callback) {

    global.db       = {};
    global.dbPrefix = process.env.DB_PREFIX || '';
    mysql           = require("mysql");
    global.db       = mysql.createConnection({
        host:     process.env.DB_HOST || "localhost",
        port:     process.env.DB_PORT || 3306,
        database: process.env.DB_DATABASE,
        user:     process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
    });
    function handleDisconnect(myconnection){
        myconnection.on('error', function(err){

            console.log('\nRe-connecting lost connection: ' +err.stack);
            global.db.destroy();

            global.db = mysql.createConnection(global.db.config);
            handleDisconnect(global.db);
            connection.connect();

        });
    }
    handleDisconnect(global.db);
    callback(null, 'Module Database: [OK]');
    //db.createUser( { user: "project",pwd: "project",roles: [ { role: "clusterAdmin", db: "project" },{ role: "readAnyDatabase", db: "project" },"readWrite"] } )
    //db.createUser({user: "project",pwd: "project",roles:[{ role: "readWrite", db: "project" }]})
    //MongoClient.connect("mongodb://"+DB_CONFIG.DB_USER+":"+DB_CONFIG.DB_PWD+"@"+DB_CONFIG.DB_HOST+":27017/"+DB_CONFIG.DB_NAME, function(err, db) {
    // Server.mysql.connect('mongodb://project:project@localhost:27017/project', function(err, db) {
    //     console.log(err)
    //     // Now you can use the database in the db variable
    //     //db = db.db('project')
    //     //assert.equal(null, err);
    //     global.db.CoverLetter = db.collection('CoverLetter')
    //     global.db.user_cart = db.collection('user_cart')
    //     global.db.schoolUserData = db.collection('school_user_data')
    //     global.db.subscribeVacancy = db.collection('subscribeVacancy')
    //     global.db.CompanyProgects = db.collection('Company-progects')
    //     global.db.files = db.collection('files')
    //     global.db.recommendation= db.collection('recommendation')
    //     global.db.companyReviews= db.collection('Company_reviews')
    //     global.db.commentsReply= db.collection('PublicCompanyComments_Replys')
    //     global.db.comments= db.collection('PublicCompanyComments')
    //     global.db.workExp = db.collection('work-exp')
    //     global.db.users = db.collection('users')
    //     global.db.dialogs = db.collection('dialogs')
    //     global.db.admin_proof = db.collection('admin_proof')
    //     global.db.schools = db.collection('schools')
    //     global.db.schools_edu = db.collection('schools_edu')
    //     global.db.resumeNotes = db.collection('resume_notes')
    //     global.db.resume = db.collection('resume')
    //     global.db.vacancy = db.collection('vacancy')
    //     global.db.vacancy_service = db.collection('vacancy_service')
    //     global.db.subaccount = db.collection('subaccount')
    //     global.db.locations = db.collection('locations')
    //     global.db.savedSearches = db.collection('savedSearches')
    //     global.db.messages = db.collection('messages')
    //     global.db.options = db.collection('options')
    //     global.db.bookmarks = db.collection('bookmarks')
    //     global.db.packages = db.collection('packages')
    //     global.db.userOrder = db.collection('user_order')
    //     global.db.promocodes = db.collection('promocodes')
    //     global.db.notification = db.collection('notification')
    //     global.db.options_notification = db.collection('options_notification')
    //     global.db.oauth = db.collection('oauth')
    //     global.db.events = db.collection('events')
    //
    //     global.db.options.find({}).toArray(function(err, data) {
    //         if (err) {
    //             console.log('[ALERT] prototype.GeUserInfo pcDB.users')
    //             console.error(err);
    //             callback(err,null)
    //         } else {
    //
    //
    //
    //             // console.log("sssssssssssssssss")
    //             // for(let key in data[0]){
    //             //     if (key=='languages'){continue}
    //             //     if (key=='Relation'){continue}
    //             //     if (key=='access'){continue}
    //             //
    //             //     console.log("data[0][key]  "+key,data[0][key])
    //             //     if (Array.isArray(data[0][key])){
    //             //         data[3].translate.ru[key] = {}
    //             //         data[3].translate.en[key] = {}
    //             //         data[3].translate.es[key] = {}
    //             //         for (let i = 0; i < data[0][key].length; i++) {
    //             //             data[3].translate.ru[key][data[0][key][i]['id']] = data[0][key][i]['text'];
    //             //             data[3].translate.en[key][data[0][key][i]['id']] = data[0][key][i]['text'];
    //             //             data[3].translate.es[key][data[0][key][i]['id']] = data[0][key][i]['text'];
    //             //
    //             //
    //             //         }
    //             //
    //             //     }
    //             //
    //             //
    //             //
    //             //     //data[3].translate.ru[key] = {"0":"sadasdasd"}
    //             // }
    //             global.cache.search = {}
    //             global.cache.options = {}
    //             for (let i = 0; i < data.length; i++) {
    //                 console.log(data[i]._id)
    //                 if (data[i]._id=='withoutTrasnslate'||data[i]._id=='translate'||data[i]._id=='settings'){
    //                     global.cache.options = Object.assign(global.cache.options,data[i])
    //                 }
    //
    //                 if (data[i]._id=='search'){
    //                     global.cache.search = Object.assign(global.cache.search,data[i])
    //                 }
    //
    //             }
    //
    //             global.api.post({},'getFooterMenu',function (err,data) {
    //                 if (err){
    //                     console.error(err)
    //                 }
    //                 if (data&&data.hasOwnProperty('menu_codes')){
    //                     global.cache.options.footerMenu = data.menu_objects
    //                 }
    //                 if (data&&data.hasOwnProperty('translate')){
    //                     let keys = Object.keys(data.translate)
    //                     for(k=0;k<keys.length;k++){
    //                         global.cache.options.translate[keys[k]] = Object.assign(global.cache.options.translate[keys[k]],data.translate[keys[k]])
    //                     }
    //                 }
    //                 console.log('getFooterMenu',data)
    //             })
    //
    //             // global.cache.search = Object.assign({},data[4])
    //             global.db.options_notification.find({}).toArray(function(err, data) {
    //                 global.cache.options.notifications = data
    //             })
    //             global.db.packages.find({}).toArray(function(err, data) {
    //                 global.cache.packages = data
    //             })
    //             global.db.promocodes.find({}).toArray(function(err, data) {
    //                 global.cache.promocodes = data
    //             })
    //             global.db.promocodes.find({}).toArray(function(err, data) {
    //                 global.cache.promocodes = data
    //             })
    //             // global.db.options.update({_id:ObjectId("59ca12176e537a165a06cc26")},{$set:{translate3:data[3].translate}},function(err,data){
    //             //
    //             // })
    //             callback(null, 'Module Database: [OK]');
    //         }
    //     })
    //
    // });
};
