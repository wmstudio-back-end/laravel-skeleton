const codes = require('../system/errors_data');

let Errors = function(){
    this.errors = {};
    for (let module in codes.modules) {
        for (let error in codes.messages) {
            let errCode = codes.modules[module].toString(),
                errorCode = eval(error);
            if (errorCode < 10) {
                errCode += '0';
            }
            errCode += errorCode.toString();
            this.errors[errCode] = '[' + module + '] => ' + codes.messages[error];
        }
    }
};

Errors.prototype.getError = function(module, error) {
    if (codes.modules[module] == null) {
        throw new Error('Unknown error module.');
    }
    let errCode = codes.modules[module].toString();
    if (error != null) {
        if (error < 10) {
            errCode += '0';
        }
        errCode += error.toString();
    }

    if (this.errors[errCode] != null) {
        return {
            errorCode: errCode,
            errorMessage: this.errors[errCode],
        }
    } else {
        throw new Error('Unknown error.');
    }
};

module.exports = Errors;