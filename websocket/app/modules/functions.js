/**
 * Возвращает arr3 пересечений в массивах
 *
 * @param {array} arr1 Массив в котором происходит поиск
 * @param {array} arr2 Массив который нужно сопоставить
 * @return {number} arr3 пересечения массивов.
 */
module.exports.Intersec = function(arr1, arr2) {
    let idx = 0, arr3 = [];
    for(let i = 0; i < arr2.length; i++) {
        idx = arr1.indexOf(arr2[i]);
        if(idx >= 0) arr3.push(arr1[i]);
    }
    return arr3;
};

module.exports.maxValueKey = function(arr, key) {
    arr.sort(function(a, b) {
        return b[key] - a[key];
    });
    return arr[0][key];
};

// Распарсить строку GET  1 вложенности
module.exports.parse_str = function(str, array) {
    let glue1 = '=';
    let glue2 = '&';

    let array2 = str.split(glue2);
    let array3 = [];
    for(let x = 0; x < array2.length; x++) {
        let tmp                  = array2[x].split(glue1);
        array3[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
    }

    if(array) {
        return array;
    } else {
        return array3;
    }
};

// Перевод density
module.exports.densityTOFormat = function(density) {
    let den = 'hdpi';
    switch(density) {
        case 72:
            den = 'xxhdpi';
            break;
        case 48:
            den = 'xhdpi';
            break;
        case 36:
            den = 'hdpi';
            break;
        case 24:
            den = 'mdpi';
            break;
    }
    return den
};

// сравнивание двух массивов
module.exports.arraysInCommon = function(arrays) {
    let i, common,
        L = arrays.length, min = Infinity;
    while(L) {
        if(arrays[--L].length < min) {
            min = arrays[L].length;
            i   = L;
        }
    }
    common = arrays.splice(i, 1)[0];
    return common.filter(function(itm, indx) {
        if(common.indexOf(itm) === indx) {
            return arrays.every(function(arr) {
                return arr.indexOf(itm) !== -1;
            });
        }
    });
};

// подменяем имя
module.exports.reNameFriendINFO = function(frId, arrFr, namedefault) {

    for(let m = 0; m < arrFr.length; m++)//проходим по всем пользователям из профиля
    {
        if(frId + '' === arrFr[m].IdFriend + '') {
            if(arrFr[m].name) {
                namedefault = arrFr[m].name//Подменяем имя
            }
        }
    }
    return namedefault
};

// подменяем картинку
module.exports.reImgFriendINFO = function(frId, arrFr, imgdefault) {
    let contactId = '';
    let photo     = imgdefault;
    for(let m = 0; m < arrFr.length; m++)//проходим по всем пользователям из профиля
    {
        if(frId + '' === arrFr[m].IdFriend + '') {
            if(arrFr[m].contactId) {
                if(arrFr[m].contactId != '-1') {
                    contactId = arrFr[m].contactId//Подменяем имя
                }
            }
        }
    }
    return {photo: photo, contactId: contactId}
};

// переворот массива ключ=значение
module.exports.array_flip = function(trans) {
    let key, tmp_ar = {};
    for(key in trans) {
        tmp_ar[trans[key]] = key;
    }
    return tmp_ar;
};

// Поиск в массиве значения
module.exports.arraySearch       = function(val, arr) {
    for(let i = 0; i < arr.length; i++)
        if(arr[i].toString() === val.toString())
            return i;
    return false;
};

// Удаление элемента массива по значению
module.exports.arrayDelValue     = function(val, arr) {
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] + '' === val + '') {
            delete arr[i]
        }
    }
    return arr.filter(function(e) {
        return e
    });
};

// рандомное число
module.exports.getRandomArbitary = function(min, max) {
    return Math.random() * (max - min) + min;
};

// Кол-во ключей в массиве
module.exports.getKeysCount      = function(obj) {
    let counter = 0;
    for(let key in obj) {
        counter++;
    }
    return counter;
};

// Градусы
function g2r(gradus) {
    return gradus * Math.PI / 180;
}

// Радианы
function r2g(radian) {
    return radian * 180 / Math.PI;
}

// Находим дистанцию по координатам
module.exports.get_dist2 = function(lat1, lon1, lat2, lon2) {
    let e_radius = 6372795;
    dLat         = g2r(lat2 - lat1);
    dLon         = g2r(lon2 - lon1);
    lat1         = g2r(lat1);
    lat2         = g2r(lat2);
    a            = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    c            = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    d            = e_radius * c;
    return d
};

module.exports.array_diff = function(a1, a2) {
    let a = [], diff = [];
    for(let i = 0; i < a1.length; i++)
        a[a1[i]] = true;
    for(let i = 0; i < a2.length; i++)
        if(a[a2[i]]) delete a[a2[i]];
        else a[a2[i]] = true;
    for(let k in a)
        diff.push(k);
    return diff;
};

// Parses the string into variables
module.exports.parse_str = function(str, array) {
    let glue1 = '=';
    let glue2 = '&';

    let array2 = str.split(glue2);
    let array3 = [];
    for(let x = 0; x < array2.length; x++) {
        let tmp                  = array2[x].split(glue1);
        array3[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
    }

    if(array) {
        array = array3;
    } else {
        return array3;
    }
};

// аналог PHP explode
module.exports.explode = function(delimiter, string) {
    let emptyArray = {0: ''};

    if(arguments.length !== 2
        || typeof arguments[0] === 'undefined'
        || typeof arguments[1] === 'undefined') {
        return null;
    }

    if(delimiter === ''
        || delimiter === false
        || delimiter === null) {
        return false;
    }

    if(typeof delimiter === 'function'
        || typeof delimiter === 'object'
        || typeof string === 'function'
        || typeof string === 'object') {
        return emptyArray;
    }

    if(delimiter === true) {
        delimiter = '1';
    }

    return string.toString().split(delimiter.toString());
};

// обрезка строки
module.exports.str_replace = function(search, replace, subject) {
    return subject.split(search).join(replace);
};

// вывести из массива уникальные значения
module.exports.unique = function unique(arr) {
    let obj = {};
    for(let key in arr)
        obj[arr[key]] = true;
    return Object.keys(obj);
};

module.exports.uniq_userId = function(inArr) {
    let outArr = inArr.sort(function(a, b) {
        return a.userID < b.userID ? -1 : 1;
    }).reduce(function(arr, el) {
        if(!arr.length || arr[arr.length - 1].userID !== el.userID) {
            arr.push(el);
        }
        return arr;
    }, []);
    return outArr;
};

module.exports.uniq_room = function(inArr) {
    let t = [];
    let r = [];
    for(let i = 0; i < inArr.length; i++) {
        t[inArr[i].room] = inArr[i];
    }
    for(let key in t) {
        r.push(t[key])
    }
    return r;
};

module.exports.unique_IdFriend = function(inArr) {
    let outArr = inArr.sort(function(a, b) {
        return a.IdFriend < b.IdFriend ? -1 : 1;
    }).reduce(function(arr, el) {
        if(!arr.length || arr[arr.length - 1].IdFriend !== el.IdFriend) {
            arr.push(el);
        }
        return arr;
    }, []);
    return outArr;
};

module.exports.unique_id = function(inArr) {
    let t = [];
    let r = [];
    for(let i = 0; i < inArr.length; i++) {
        if(inArr[i]) {
            t[inArr[i]._id] = inArr[i];
        }
    }
    for(let key in t) {
        r.push(t[key])
    }
    return r;
};

module.exports.array_unique = function(inputArr) {
    let key      = '',
        tmp_arr2 = [],
        val      = '';

    let __array_search = function(needle, haystack) {
        let fkey = '';
        for(fkey in haystack) {
            if(haystack.hasOwnProperty(fkey)) {
                if((haystack[fkey] + '') === (needle + '')) {
                    return fkey;
                }
            }
        }
        return false;
    };

    for(key in inputArr) {
        if(inputArr.hasOwnProperty(key)) {
            val = inputArr[key];
            if(false === __array_search(val, tmp_arr2)) {
                tmp_arr2[key] = val;
            }
        }
    }

    return tmp_arr2;
};

// Replace all occurrences of the search string with the replacement string
module.exports.str_replace = function(search, replace, subject) {
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni

    if(!(replace instanceof Array)) {
        replace = new Array(replace);
        // If search	is an array and replace	is a string, then this replacement string is used for every value of search
        if(search instanceof Array) {
            while(search.length > replace.length) {
                replace[replace.length] = replace[0];
            }
        }
    }

    if(!(search instanceof Array)) search = new Array(search);
    // If replace has fewer values than search , then an empty string is used for the rest of replacement values
    while(search.length > replace.length) {
        replace[replace.length] = '';
    }

    // If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
    if(subject instanceof Array) {
        for(k in subject) {
            subject[k] = str_replace(search, replace, subject[k]);
        }
        return subject;
    }

    for(let k = 0; k < search.length; k++) {
        let i = subject.indexOf(search[k]);
        while(i > -1) {
            subject = subject.replace(search[k], replace[k]);
            i       = subject.indexOf(search[k], i);
        }
    }

    return subject;
};

module.exports.genTetHash = function(d1, d2) {
    let w1  = parseInt(d1, 16);
    let w2  = parseInt(d2, 16);
    let ret = '';
    if(w1 < w2) {
        ret = md5(d1 + d2)
    }
    else {
        ret = md5(d2 + d1)
    }
    return ret;
};

module.exports.SmartPeriod = function(gmt) {
    let chas = new Date().getUTCHours() + gmt
    // let chas = sputnikLaunch.getUTCHours() + gmt
    placechat.config.SmartPushConfig;

    for(let i = 0; i < placechat.config.SmartPushConfig.length; i++) {
        if(placechat.config.SmartPushConfig[i].periodStart < chas &&
            placechat.config.SmartPushConfig[i].periodEnd > chas) {
            return i;
        }
    }
};

// Encodes data with MIME base64
module.exports.base64_encode = function(data) {
    //
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Bayron Guevara

    let b64                                 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    let o1, o2, o3, h1, h2, h3, h4, bits, i = 0, enc = '';

    // pack three octets into four hexets
    do {
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        enc += b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while(i < data.length);

    switch(data.length % 3) {
        case 1:
            enc = enc.slice(0, -2) + '==';
            break;
        case 2:
            enc = enc.slice(0, -1) + '=';
            break;
    }

    return enc;
};

module.exports.trim = function(str, charlist) {
    try {
        charlist = !charlist ? ' \\s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
        let re   = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
        return str.replace(re, '');
    }
    catch(err) {
        console.log('[ALERT] Функция ТРИМ отработала с ошибкой');
        console.log(err);
        console.log(str);
        return str;
    }

};

module.exports.dateToStringFormat = function(date) {
    // console.error(date)
    if(typeof(date) !== 'object') {
        let s = placechat.funct.dateToDateFormat(date);
        return placechat.funct.dateToStringFormat(s);
    }

    if(!date) {
        return '02.02.2000';
    }

    try {
        let d = date.getDate();
        let m = date.getMonth();
        let y = date.getFullYear();
        if(d < 10) {
            d = '0' + d
        }
        if(m < 10) {
            m = '0' + m
        }
        return d + '.' + m + '.' + y;
    } catch(err) {
        console.log('[ALERT] не распарсил дату');
        console.error(err);
        return '02.02.2000';
    }
};

module.exports.dateToDateFormat = function(date) {
    if(typeof(date) === 'object') {
        let s = placechat.funct.dateToStringFormat(date);
        return placechat.funct.dateToDateFormat(s);
    }
    let dat = date.split('.');
    if(dat.length !== 3) {
        let dat = date.split('/');
        if(dat.length !== 3) {
            dat = [];
            return new Date(2000, 2, 2)
        }
    }
    let v1 = parseInt(dat[0]);
    let v2 = parseInt(dat[1]);
    let v3 = parseInt(dat[2]);
    if(v1 > 0 && v2 > 0 && v3 > 0) {
        return new Date(v3, v2, v1)
    }
    return new Date(2000, 2, 2)
};

// Поиск в массиве
module.exports.find = function(arr, value) {
    let i = 0;
    while(i < arr.length) {
        if(String(arr[i]) !== String(value)) {
            i++;
        } else {
            return i + 1
        }
    }
    return 0;
};

module.exports.dateConvert = function(unix_timestamp) {
    if(typeof unix_timestamp === 'string') {
        return new Date(parseInt(unix_timestamp.toString().substr(0, 8), 16) * 1000)
    }

    if(typeof unix_timestamp === 'number') {
        return new Date(unix_timestamp * 1000);
    }
};

// Поиск в массиве
module.exports.findObject = function(arr, key, value) {
    let i = 0;
    while(i < arr.length) {
        if(arr[i][key] === value) {
            return i
        }
        i++;
    }
    return false;
};

module.exports.getOnlineUsers = function(ws) {
    let statusOnline = ws.uStatus === 't' ? 'students' : 'teachers';
    let countOnline = global.UCOUNTERS[statusOnline];
    if (global.USERS[ws.uid].status.indexOf(statusOnline.substr(0, 1)) !== -1) {
        countOnline--;
    }
    return countOnline;
};
