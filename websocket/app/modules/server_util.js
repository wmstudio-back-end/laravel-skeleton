module.exports = function() {
    let self              = this;
    self.obj              = function() {
        this.success = true;
        this.data    = {};
        this.errors  = [];
        this.method  = "";
    };
    self.logPackageSchema = function() {
        this.user_id       = 0;
        this.app_id        = 0;
        this.date          = 0;
        this.item          = '';
        this.cost          = 0;
        this.item_title    = '';
        this.order_id      = 0;
        this.platform_type = '';
    };
    self.reportError      = function(method, errorMessage) {
        let result     = new self.obj();
        result.success = false;
        result.method  = method;
        result.errors.push(errorMessage);
        //result.methodId = global.gfserver.router.clientControllers.client_methods['server_error'];
        return result;
    };

    self.reportSuccess = function(method, data) {
        let result    = new self.obj();
        result.data   = data;
        result.method = method;
        return result;
    };

    self.clientPacket = function(method, data) {
        let result    = new self.obj();
        result.data   = data;
        result.method = method;
        return result;
    };

    self.serverError = function(method, errorMessage) {
        let result    = new self.obj();
        result.method = method;
        result.data   = {message: errorMessage.toString()}
        return result;
    };

    self.random_string = function(len) {
        let key  = '';
        let keys = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
            'x', 'y', 'z'];

        for(let i = 0; i < len; i++) {
            key += keys[Math.floor(Math.random() * keys.length)];
        }

        return key;
    };

    self.gracefullStop = function() {
        global.gfserver.needToStopServer = true;
        let queue                        = global.gfserver.cacheModule.getSaveQueue();
        let haveUsers                    = false;
        for(let player in queue) {
            haveUsers = true;
            global.gfserver.disconectProcess.playersCount += 1;
            //Попробуем закрыть сокет а сохранение сработает автоматом при его закрытии <- херовая идея, т.к
            //игрок может нахоится на другой вкладке в браузере, в этом случае риск что сокет не закроеться до закрытия сервера         
            global.gfserver.router.kick(queue[player], 666, function(ppid) {
                //Статус отключений ведется у плеера, хотя логично было бы перенести сюда
            });
        }
        if(!haveUsers) {
            let data = {
                "saved":   global.gfserver.disconectProcess.savedUsers,
                "residue": global.gfserver.disconectProcess.playersCount,
                "process": process.pid
            }
            global.gfserver.router.masterControllers.worker_requests.request("mortality", "worker_ready_for_death", data, null);
        }

    };

    return self;
};