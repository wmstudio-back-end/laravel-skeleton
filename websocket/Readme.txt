Исоздящие
  Снаружи комнаты
    * Первичный исходящий звонок
    Call(uid).then(result).catch(error, errorCode)
      * Сбросить первичный исходящий звонок
      AbortCall(uid).then(result).catch(error, errorCode)
    * Ответ на входящий звонок
    AnswerCall(uid, answer).then(result, admin).catch(error, errorCode)

  Вынутри комнаты
    * Вторичный исходящий звонок
    SecondaryCall(uid).then(result).catch(error, errorCode)
      * Сбросить вторичный исходящий звонок
      AbortSecondaryCall(uid).then(result).catch(error, errorCode)
    * Положить трубку во время звонка
    RejectCall({}).then(result).catch(error, errorCode)
    * Отправить сообщение
    sendMessage(data).then(result).catch(error, errorCode)

Входящие
  * Входящий звонок
  onCall(remoteUid, sound)
  * Ответ на исходящий звонок (взяли трубку)
  onAnswer(remoteUid, admin)
  * Пользователь userId положил трубку
  onReject(remoteUid)
  * Завершение звонка (или в комнате мы остались одни, или админ положил трубку)
  onDestroy()
  * В комнату подключился пользователь
  onJoin(remoteId, remoteName, createOffer)
  * В комнату пришло сообщение
  onMessage(data)

RTC
  * Отправка данных видеопотока
  sendData(uid, message)
  * Прием данных видеопотока
  remoteData(data)

UIFunctions
  * Отправить сообщение в комнату
  sendMessage(message)
  * Позвонить userId
  callTo(userId)
  * Ответить на вызов
  answerTo(userid, answer)
  * Отклонить исходящий звонок (еще не взяли трубку)
  abortCall(userId)
  * Завершить звонок
  endCall()

UIEvents
  * Сообщение отправленно
  onSendMessage(message)
  * Новое сообщение
  onMessage(data: {uid, name, message})
  * Инициализация звонка (вывод модалки)
  onCall({remoteId, isCaller, isSecondary})
  * Начало звонка
  onStartCall({remoteId, isAdmin})
  * Пользователь remoteId отключился от звонка
  onDisconnectUser(remoteId)
  * Звонок завершен
  onEndCall()
  * Генерация видео / контроллеров аудио-видео
  elVidGenerator({id, name}) {
    типы данных: HTMLElement || jquery
    валидные ответы (в квадратных скобочках не боязательные значения):
    1) video
    2) {
      video,
      [audioControl],
      [videoControl],
    }
    3) [
      video,
      [audioControl],
      [videoControl],
    ]
  }
