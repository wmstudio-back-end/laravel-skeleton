@php
    use Modules\Users\Entities\UserAddSettings;
    $user = Auth::user();
@endphp
@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('user-find.teacher.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('user-find.teacher.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="findTeacher-wrap">
                <p class="findTeacher-text">{!! ___('user-find.teacher.find') !!}</p>
                <div class="subscription-sort">
                    <div class="sort-wrap">
                        <div class="sort">
                            <input id="filter-all" type="radio" name="search-type" value="all" checked="checked">
                            <label for="filter-all" class="sort-btn btn">
                                {!! ___('user-find.find.all') !!}
                            </label>
                        </div>
                        <div class="sort">
                            <input id="filter-online" type="radio" name="search-type" value="online">
                            <label for="filter-online" class="sort-btn btn">
                                <span id="online-count">{{ $user->getOnlineCount() ?: '' }}</span>
                                {!! ___('user-find.find.online') !!}
                            </label>
                        </div>
                        @php $cntFavorites =  $user->getFavoritesCount() @endphp
                        @if ($cntFavorites)
                            <div class="sort">
                                <input id="filter-favorites" type="radio" name="search-type" value="favorites">
                                <label for="filter-favorites" class="sort-btn btn">
                                    <span id="favorites-count">{{ $cntFavorites }}</span>
                                    {!! ___('user-find.find.favorites') !!}
                                </label>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="language-search">
                    <input id="search" type="text" name="search" placeholder="{{ ___('user-find.teacher.search') }}">
                    <button id="clearSearchBtn" type="button" class="closeSearch hidden"></button>
                    <button id="searchBtn" type="button"></button>
                </div>
            </div>

        </div>
    </div>

    <div class="teachStyle-wrap">
        <div class="row">
            <div class="teachStyle">
                <div class="teachStyle-title">{!! ___('user-find.teacher.filter') !!}:</div>
                <div class="btn2">{!! ___('user-info.dialect') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'dialect')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="dialect[{{ $key }}]" id="dialect-{{ $key }}">
                                <label for="dialect-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="dialectLang-list">
                            <input class="filter-param" type="checkbox" name="no-dialect" id="no-dialect">
                            <label for="no-dialect" class="dialectLang">{!! ___('global.no') !!}</label>
                        </div>
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2">{!! ___('user-find.lesson-type') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'lesson_type')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="lesson-type[{{ $key }}]" id="lesson-type-{{ $key }}">
                                <label for="lesson-type-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2 teachStyle-pdr">{!! ___('user-find.teacher.lang-level') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'lang_level')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="lang-level[{{ $key }}]" id="lang-level-{{ $key }}">
                                <label for="lang-level-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2 teachStyle-pdr">{!! ___('user-find.speak-langs') !!}
                    <div class="btn2field">
                        @foreach(getLocales() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="speak-langs[{{ $key }}]" id="speak-langs-{{ $key }}">
                                <label for="speak-langs-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div class="search-line wrap-flex">
                <div>
                    <h3>{!! ___('user-find.teacher.students') !!}</h3>
                </div>
                <div id="found-count" class="search-line-text">
                    <p>{!! ___('user-find.teacher.found', [
                        'count' => '<span id="found-counter">0</span>',
                    ]) !!}</p>
                </div>
                <div class="search-line-ic">
                    <a class="btnTab sli1-ic"></a>
                    <a class="btnTab sli2-ic active"></a>
                </div>
            </div>
        </div>
        <div class="rowCol">
            <div id="card-container" class="many-card" data-action="{{ route('find.students') }}"></div>
            <button id="find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials.user.card', ['userLink' => route('user.student', ['login' => 'user-id'])])
@stop
