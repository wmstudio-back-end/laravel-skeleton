@php use Modules\Topics\Entities\TopicsGroup; @endphp
@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('topics.my.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('topics.my.description') !!}
        </div>
    </div>

    <div class="supportSubtitle">
        <div class="row">
            <div class="topicTalk">
                <h3>{!! ___('topics.my.topics.selected', [
                    'count' => '<span id="topic-count">' . $topicsCount . '</span>',
                    'ending' => '<span id="topic-count-ending">' . numEnding($topicsCount, [
                        ___('topics.my.topics.ending.one'),
                        ___('topics.my.topics.ending.two'),
                        ___('topics.my.topics.ending.five'),
                    ]) . '</span>',
                    'topics' => '<span id="topic-count-topic">' . numEnding($topicsCount, [
                        ___('topics.my.topics.topics.one'),
                        ___('topics.my.topics.topics.two'),
                        ___('topics.my.topics.topics.five'),
                    ]) . '</span>',
                ]) !!}</h3>
                <div>
                    <button type="button" id="open-modal-create" class="btn discuss">{!! ___('topics.my.create') !!}</button>
                </div>

            </div>
        </div>
    </div>
    <div class="findTeacher">
        <div class="row">
            <div class="issue">
                <p>{!! ___('topics.find.select') !!}:</p>
            </div>
            <div class="answer">
                @foreach($topicsGroups as $group)
                    @php /* @var $group TopicsGroup */ @endphp
                    <div class="helpStudent">
                        <input type="radio" name="topic-group" id="topic-group-{{ $group->id }}" value="{{ $group->id }}">
                        <label class="btn press" for="topic-group-{{ $group->id }}">{{
                            transDef($group->name, TopicsGroup::TRANS_GROUP . '.' . TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias)
                        }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row topicForm">
        <div id="topic-tags"></div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div
                id="card-container"
                class="many-card"
                data-action="{{ route('topics.my') }}"
                data-check-action="{{ route('topics.check') }}"
            ></div>
            <button id="find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    @include('modals.topic-content')
    @include('modals.topic-create')
@stop

@section('scripts')
    @parent
    <script id="topics-groups-ang-tags" type="text/javascript">
        window.topicsGroups = {!! json_encode($topicsTags) !!};
        window.mimeTypes = {!! json_encode(array_map(function($val){ return 'image/' . $val; }, $mimes)) !!};
        window.createdTopics = {
            ending: {
                one: '{{ ___('topics.my.topics.ending.one') }}',
                two: '{{ ___('topics.my.topics.ending.two') }}',
                five: '{{ ___('topics.my.topics.ending.five') }}',
            },
            topic: {
                one: '{{ ___('topics.my.topics.topics.one') }}',
                two: '{{ ___('topics.my.topics.topics.two') }}',
                five: '{{ ___('topics.my.topics.topics.five') }}',
            },
        };
    </script>
    <script id="topic-card" type="text/x-custom-template">
        <div class="card topicCard">
            <div data-id="topic-card-image" class="topicImg"></div>
            <div class="wrap-complexity">
                <div class="complexity">{!! ___('topics.find.difficulty') !!}
                    <select data-id="difficulty" class="rating" data-theme="fontawesome-stars">
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div data-id="topic-card-name" class="mrB25"></div>
            <div class="card-edit">
                <button type="button" data-id="topic-card-button" class="btn discuss">{!! ___('global.edit') !!}</button>
                <form action="{{ route('topics.my.edit', ['action' => 'delete']) }}" method="post">
                    @csrf
                    <button type="submit" name="topic-id" data-id="topic-card-remove-button" class="btn removeBtn">{!! ___('global.delete') !!}</button>
                </form>
            </div>
        </div>
    </script>
@stop
