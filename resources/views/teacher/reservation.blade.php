@php /* @var $reservations \Modules\Reservation\Entities\Reservation */ @endphp
@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('reservation.teacher.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('reservation.teacher.description') !!}
        </div>
    </div>
    <div class="findTeacher shadowNone">
        <div class="row">
            <div class="wrapBooking-count">
                <div class="booking-count">
                    <p
                        id="count-reserved"
                        data-one="{{ ___('reservation.dialogs-endings.one') }}"
                        data-two="{{ ___('reservation.dialogs-endings.two') }}"
                        data-five="{{ ___('reservation.dialogs-endings.five') }}"
                    >{!! ___('reservation.teacher.dialogs', [
                        'count' => '<span class="count">' . $countReserved . '</span>',
                        'dialog' => '<span class="dialog">' . numEnding($countReserved, [
                            ___('reservation.dialogs-endings.one'),
                            ___('reservation.dialogs-endings.two'),
                            ___('reservation.dialogs-endings.five'),
                        ]) . '</span>',
                    ]) !!}</p>
                </div>
                <button type="button" data-modal-id="reservation-modal-teacher" class="btn book">
                    {!! ___('reservation.teacher.free-time') !!}
                </button>
            </div>
        </div>
    </div>
    <div class="findTeacher">
        <div class="row">
            <div class="findTeacher-wrap">
                <div class="bookingSort">
                    <p class="sortText">{!! ___('reservation.reserved.title') !!}</p>
                    <div class="sort-wrap btn2">
                        <input
                            id="date-interval"
                            type="text"
                            placeholder="{{ ___('reservation.reserved.title') }}"
                            readonly="readonly"
                        >
                        <div class="btn2field datePicker-field">
                            <input id="dateInterval" type="text" readonly="readonly">
                            <div class="clearDate">
                                <button id="setDate" type="button" class="ok">
                                    {!! ___('reservation.reserved.set') !!}
                                </button>
                                <button id="clearDate" type="button" class="clear">
                                    {!! ___('reservation.reserved.clear') !!}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="language-search">
                    <input id="reservation-name" type="text" placeholder="{{ ___('reservation.teacher.search') }}">
                    <button type="button" class="closeSearch reservation-search-clear"></button>
                    <button type="button" class="reservation-search-change"></button>
                </div>
            </div>
        </div>
    </div>
    <div class="listTeachers reservation-list">
        <div class="rowCol">
            <div
                id="reservation-cards"
                class="many-card"
                data-action="{{ route('user.reservation') }}"
            ></div>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    @include('modals.reservation-teacher', ['firstTime' => $firstTime])
@stop

@section('scripts')
    @parent
    @include('partials.user.reservationCard', ['userLink' => route('user.student', ['login' => 'user-id'])])
    <script id="reservations" type="text/javascript">
        window.reservationTimeLine = {!! json_encode($freeTime) !!};
    </script>
@stop
