@php
    use Modules\Users\Entities\UserAddSettings;
    /* @var $user \Modules\Users\Entities\User */
    $selfUser = Auth::user();
@endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('user-info.student.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            <p>{!! ___('user-info.description') !!}</p>
        </div>
    </div>

    <div class="findTeacher">
        <input id="user-id" type="hidden" value="{{ $user->id }}">
        <div class="row">
            <div class="many-card cardInfo">
                <a href="{{ route('home') }}" class="return">
                    <span class="return-ic"></span>
                    {!! ___('dictionary.back') !!}
                </a>
                <div class="card cardBooking-info">
                    <div class="card-face bookingCard-face">
                        <div class="card-face-img">
                            <img src="{{ $user->getAvatar() }}">
                            <span data-line-status-id="{{ $user->id }}" class="{{ isUserOnline($user->id) ? 'online' : 'offline' }}"></span>
                        </div>
                        <div class="minInfo">
                            <p class="name">{{ $user->getFullName() }}</p>
                            <div class="city">
                                <p>{{  Modules\Translations\Constants\Languages::getCountry($user->addon('country')) }}</p>
                            </div>
                            @php $dialects = UserAddSettings::where('control_type', 'dialect')
                                ->first()->transValue($user->addon('st_dialect')); @endphp
                            @if(count($dialects))
                                <div class="dialect">
                                    {!! ___('user-info.dialect') !!}:
                                    <span>{{ implode(', ', $dialects) }}</span>
                                </div>
                            @endif
                            @php $langLevel = UserAddSettings::where('control_type', 'lang_level')
                                ->first()->transValue($user->addon('st_lang_level') ?: -1); @endphp
                            @if($langLevel)
                                <div class="tag">
                                    <div class="tag__text">{{ $langLevel }}</div>
                                </div>
                            @endif

                            <div class="showAll notify">
                                <input
                                    id="update-watch-list"
                                    type="checkbox"
                                    data-action="{{ route('watch-list', ['userId' => $user->id]) }}"
                                    @if(Auth::user()->getWatchList($user->id))checked="checked"@endif
                                >
                                <label for="update-watch-list" class="showAll-btn">{!! ___('user-info.notify-when-online', [
                                    'person' => mb_strtolower(Modules\Users\Constants\Cambly::getStatus('s')),
                                ]) !!}</label>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="card-status cardStudent-info">
                    <div class="like">
                        <span class="hear set-favorites{{ isset($selfUser->addon('favorites')[$user->id]) ? ' active' : ''
                        }}" data-user-id="{{ $user->id }}"></span>
                    </div>
                    <div class="card-status-cont">
                        <button class="phone linc-column call-to-user{{ isUserOnline($user->id) ? '' : ' hidden' }}" data-user-id="{{ $user->id }}">
                            <span></span>
                        </button>
                        <button class="postal linc-column msg-send-message" data-user-id="{{ $user->id }}">
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div class="blockStudent-info">
                <div class="aboutStudent">
                    <div class="studentInfo-title">
                        <p>{!! ___('user-info.student.title') !!}</p>
                    </div>
                    <div class="studentInfo-info">
                        <div class="wrap-contentInfo">
                            @php $locates = getLocales(); @endphp
                            @if (isset($locates[$user->addon('natural_lang')]))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.natural-lang') !!}</p>
                                    <div class="tag">
                                        <div class="tag__text">{{ $locates[$user->addon('natural_lang')] }}</div>
                                    </div>
                                </div>
                            @endif
                            @if(count($dialects))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.dialect-adighe') !!}</p>
                                    @foreach($dialects as $val)
                                        <div class="tag">
                                            <div class="tag__text">{{ $val }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @php
                                $speakLangs = $user->addon('speak_langs');
                                if (!is_array($speakLangs)) {
                                    $speakLangs = [];
                                }
                                if (isset($speakLangs[$user->addon('natural_lang')])) {
                                    unset($speakLangs[$user->addon('natural_lang')]);
                                }
                            @endphp
                            @if(count($speakLangs))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.speak-langs') !!}</p>
                                    @foreach($speakLangs as $key => $val)
                                        <div class="tag">
                                            <div class="tag__text">{{ $locates[$key] }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @if($langLevel)
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.student.lang-level') !!}</p>
                                    <div class="tag">
                                        <div class="tag__text">{{ $langLevel }}</div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="wrap-shortInfo">
                            @if($user->addon('st_about_me'))
                                <div class="shortInfo">
                                    <p class="shortInfo-text">{!! ___('user-info.about-me') !!}</p>
                                    <p>{!! str_replace("\n", "<br>", $user->addon('st_about_me')) !!}</p>
                                </div>
                            @endif
                            @if($user->addon('st_learn_targets'))
                                <div class="shortInfo">
                                    <p class="shortInfo-text">{!! ___('user-info.student.learn-targets') !!}</p>
                                    <p>{!! str_replace("\n", "<br>", $user->addon('st_learn_targets')) !!}</p>
                                </div>
                            @endif
                        </div>


                    </div>
                </div>

                @php
                    $preferLessonType = UserAddSettings::where('control_type', 'lesson_type')
                        ->first()->transValue($user->addon('st_lesson_type'));
                @endphp
                @if(count($preferLessonType))
                    <div class="aboutStudent aboutStudent-training">
                        <div class="studentInfo-title">
                            <p>{!! ___('user-info.learning') !!}</p>
                        </div>
                        <div class="studentInfo-info">
                            <div class="wrap-contentInfo">
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.lesson-type') !!}</p>
                                    @foreach($preferLessonType as $val)
                                        <div class="tag">
                                            <div class="tag__text">{{ $val }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
