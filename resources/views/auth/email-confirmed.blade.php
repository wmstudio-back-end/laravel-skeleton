@extends('layouts.main-no-auth')

@section('title', ___('auth.email-confirmed'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="tariffs">
                <div class="min-row">
                    <div class="wrapBookModal-title mb-3">
                        <p class="bookModal-title">{!! ___('auth.last-step') !!}</p>
                    </div>
                    <form action="{{ route('last-step') }}" method="post">
                        @csrf
                        <div class="wrapper">
                            <div class="doorInpForm mb-4">
                                <div class="doorInp-wrap">
                                    <div class="doorInp">
                                        <p><label for="name">
                                            {!! ___('auth.name') !!}
                                        </label></p>
                                        <input
                                            id="name"
                                            type="text"
                                            placeholder="{!! ___('auth.name') !!}"
                                            name="cam-name"
                                            class="name-regex{{ $errors->has('cam-name') ? ' is-invalid' : '' }}"
                                            value="{{ old('cam-name') ?: $user->addon('name') }}"
                                            required="required"
                                            autofocus="autofocus"
                                            minlength="2"
                                        >
                                        @if ($errors->has('cam-name'))
                                            <p class="message error">
                                                {{ str_replace('cam-name', "\"" . ___('auth.name') . "\""
                                                , $errors->first('cam-name')) }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p><label for="surname">
                                            {!! ___('auth.surname') !!}
                                        </label></p>
                                        <input
                                            id="surname"
                                            type="text"
                                            placeholder="{!! ___('auth.surname') !!}"
                                            name="cam-surname"
                                            class="name-regex{{ $errors->has('cam-surname') ? ' is-invalid' : '' }}"
                                            value="{{ old('cam-surname') ?: $user->addon('surname') }}"
                                            required="required"
                                            minlength="2"
                                        >
                                        @if ($errors->has('cam-surname'))
                                            <p class="message error">
                                                {{ str_replace('cam-surname', "\"" . ___('auth.surname')
                                                . "\"", $errors->first('cam-surname')) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="doorInp-wrap">
                                    <div id="phone-target" class="doorInp">
                                        @php $phone = phoneFormat($user->addon('phone'), false, true) @endphp
                                        <p><label for="phone-code">
                                            {!! ___('auth.phone') !!}
                                        </label></p>
                                        <input
                                            id="phone-code"
                                            type="text"
                                            name="cam-phone-code"
                                            class="phone-code{{ $errors->has('cam-phone') ? ' is-invalid' : '' }}"
                                            data-error-target="phone-number"
                                            placeholder="+___"
                                            value="{{ old('cam-phone-code') ?: $phone['cc'] }}"
                                            required="required"
                                        >
                                        <input
                                            id="phone-number"
                                            type="text"
                                            name="cam-phone-number"
                                            class="phone-number{{ $errors->has('cam-phone') ? ' is-invalid' : '' }}"
                                            placeholder="(___) ___-__-__"
                                            value="{{ old('cam-phone-number') ?: $phone['number'] }}"
                                            required="required"
                                        >
                                        @if ($errors->has('cam-phone'))
                                            <p class="message error">
                                                {{ str_replace('cam-phone', "\"" . ___('auth.phone') . "\""
                                                , $errors->first('cam-phone')) }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p>&nbsp;</p>
                                        <div class="register">
                                            <button class="btn">{!! ___('auth.register') !!}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection