@extends('layouts.main-no-auth')

@section('title', ___('auth.register'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="tariffs">
                <div class="wrapper auth">
                    <div class="wrapBookModal-title mb-3">
                        <p class="bookModal-title">{!! old('id') && old('provider')
                            ? ___('auth.continue-register')
                            : ___('auth.register') !!}</p>
                        <p class="bookModal-subtitle">{!! ___('auth.register-social') !!}</p>
                    </div>
                    @php $social = getAvailableSocial(); @endphp
                    @if(count($social))
                        <div class="wrap-wrapToils mb-4">
                            <div class="toilsLink">
                                @foreach($social as $socName)
                                    <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                        @csrf
                                        <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        @if(old('id'))
                            <input type="hidden" name="id" value="{{ old('id') }}">
                        @endif
                        @if(old('first_name'))
                            <input type="hidden" name="first_name" value="{{ old('first_name') }}">
                        @endif
                        @if(old('middle_name'))
                            <input type="hidden" name="middle_name" value="{{ old('middle_name') }}">
                        @endif
                        @if(old('last_name'))
                            <input type="hidden" name="last_name" value="{{ old('last_name') }}">
                        @endif
                        @if(old('social-email'))
                            <input type="hidden" name="social-email" value="{{ old('social-email') }}">
                        @endif
                        @if(old('provider'))
                            <input type="hidden" name="provider" value="{{ old('provider') }}">
                        @endif
                        <div class="wrapper">
                            <div class="doorInpForm mb-4">
                                <div class="doorInp-wrap">
                                    <div class="doorInp">
                                        <p><label for="login">
                                            {!! ___('auth.login') !!}
                                        </label></p>
                                        <input
                                            id="login"
                                            type="text"
                                            placeholder="{!! ___('auth.login') !!}"
                                            name="cam-login"
                                            @if($errors->has('cam-login'))class="is-invalid"@endif
                                            value="{{ old('cam-login') }}"
                                            required="required"
                                            autofocus="autofocus"
                                            minlength="5"
                                            tabindex="1"
                                        >
                                        @if ($errors->has('cam-login'))
                                            <p class="message error">
                                                {{ str_replace('cam-login', "\"" . ___('auth.login')
                                                . "\"", $errors->first('cam-login')) }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p><label for="email">
                                            {!! ___('auth.email') !!}
                                        </label></p>
                                        <input
                                            id="email"
                                            type="email"
                                            placeholder="{!! ___('auth.email') !!}"
                                            name="cam-email"
                                            @if($errors->has('cam-email'))class="is-invalid"@endif
                                            value="{{ old('cam-email') }}"
                                            required="required"
                                            tabindex="2"
                                        >
                                        @if ($errors->has('cam-email'))
                                            <p class="message error">
                                                {{ str_replace('cam-email', "\"" . ___('auth.email')
                                                . "\"", $errors->first('cam-email')) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="doorInp-wrap">
                                    <div class="doorInp">
                                        <p><label for="password">
                                            {!! ___('auth.password') !!}
                                        </label></p>
                                        <input
                                            id="password"
                                            type="password"
                                            placeholder="{!! ___('auth.password') !!}"
                                            name="cam-password"
                                            @if($errors->has('cam-password'))class="is-invalid"@endif
                                            required="required"
                                            minlength="6"
                                            tabindex="3"
                                        >
                                        @if ($errors->has('cam-password'))
                                            <p class="message error">
                                                {{ str_replace('cam-password', "\"" . ___('auth.password')
                                                . "\"", $errors->first('cam-password')) }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p><label for="confirm">
                                            {!! ___('auth.password-confirmation') !!}
                                        </label></p>
                                        <input
                                            id="confirm"
                                            type="password"
                                            placeholder="{!! ___('auth.password-confirmation') !!}"
                                            name="cam-password_confirmation"
                                            @if($errors->has('cam-password_confirmation'))class="is-invalid"@endif
                                            required="required"
                                            tabindex="4"
                                        >
                                        @if ($errors->has('cam-password_confirmation'))
                                            <p class="message error">
                                                {{ str_replace('cam-password_confirmation',
                                                    "\"" . ___('auth.password-confirmation') .
                                                    "\"", $errors->first('cam-password_confirmation')) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="doorInp-wrap">
                                    <div class="doorInp">
                                        <p><label for="referral">
                                            {!! ___('auth.ref-link') !!}
                                        </label></p>
                                        <input
                                            id="referral"
                                            type="text"
                                            placeholder="{!! ___('auth.ref-link') !!}"
                                            name="cam-ref-link"
                                            @if($errors->has('cam-ref-link'))class="is-invalid"@endif
                                            value="{{ old('cam-ref-link') }}"
                                            tabindex="5"
                                        >
                                        @if ($errors->has('cam-ref-link'))
                                            <p class="message error">
                                                {{ $errors->first('cam-ref-link') }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p><label for="how-know">
                                            {!! ___('auth.how-know-about-us') !!}
                                        </label></p>
                                        <div id="how-know-error-target" class="doorInpSel">
                                            <select
                                                id="how-know"
                                                name="cam-how-know"
                                                class="select2{{ $errors->has('cam-how-know') ?' is-invalid' : ''}}"
                                                data-error-target="how-know-error-target"
                                                tabindex="6"
                                            >
                                                <option value="" class="no-value">{!! ___('global.select-no-value') !!}</option>
                                                @php $variants = \Modules\Users\Entities\UserAddSettings::
                                                    where('control_type', 'how_know_about_us')
                                                    ->first()->transValue(); @endphp
                                                @foreach($variants as $key => $val)
                                                    <option
                                                            value="{{ $key }}"
                                                            @if(old('cam-how-know') == $key)selected="selected"@endif
                                                    >{{ $val }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @if ($errors->has('cam-how-know'))
                                            <p class="message error">
                                                {{ str_replace('cam-how-know', "\"" .
                                                ___('auth.how-know-about-us') . "\""
                                                , $errors->first('cam-how-know')) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="doorInp-wrap">
                                    <center>{!! NoCaptcha::display() !!}</center>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <p class="message error">
                                            {{ $errors->first('g-recaptcha-response') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="consent mb-4">
                                @if(!empty($personalData = setting('personal_data')))
                                    {!! ___('auth.personal-data-processing-with-link', [
                                        'link' => $personalData,
                                    ]) !!}
                                @else
                                    {!! ___('auth.personal-data-processing') !!}
                                @endif
                            </div>
                            <div class="register mb-4">
                                <button class="btn">{!! ___('auth.enter-register') !!}</button>
                            </div>
                            <div class="wrapDoor-text textModal">
                                <p class="bookModal-subtitle">{!! ___('auth.already-register') !!}</p>
                                <a href="{{ route('login') }}" class="no-decor">{!! ___('auth.enter') !!}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    {!! NoCaptcha::renderJs(app()->getLocale()) !!}
@stop