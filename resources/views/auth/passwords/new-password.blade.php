@extends('layouts.main-no-auth')

@section('title', ___('auth.register'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="tariffs">
                <div class="wrapper auth">
                    <div class="wrapBookModal-title mb-3">
                        <p class="bookModal-title">{!! ___('auth.restore') !!}</p>
                    </div>
                    <form action="{{ route('password.reset', ['token' => $token]) }}" method="post">
                        @csrf
                        <div class="wrapper">
                            <div class="doorInpForm mb-4">
                                <div class="doorInp-wrap">
                                    <div class="doorInp">
                                        <p><label for="password">
                                            {!! ___('auth.new-password') !!}
                                        </label></p>
                                        <input
                                            id="password"
                                            type="password"
                                            placeholder="{!! ___('auth.new-password') !!}"
                                            name="cam-password"
                                            @if($errors->has('cam-password'))class="is-invalid"@endif
                                            required="required"
                                            minlength="6"
                                            tabindex="3"
                                        >
                                        @if ($errors->has('cam-password'))
                                            <p class="message error">
                                                {{ str_replace('cam-password', "\"" . ___('auth.password')
                                                . "\"", $errors->first('cam-password')) }}
                                            </p>
                                        @endif
                                    </div>
                                    <div class="doorInp">
                                        <p><label for="confirm">
                                            {!! ___('auth.password-confirmation') !!}
                                        </label></p>
                                        <input
                                            id="confirm"
                                            type="password"
                                            placeholder="{!! ___('auth.password-confirmation') !!}"
                                            name="cam-password_confirmation"
                                            @if($errors->has('cam-password_confirmation'))class="is-invalid"@endif
                                            required="required"
                                            tabindex="4"
                                        >
                                        @if ($errors->has('cam-password_confirmation'))
                                            <p class="message error">
                                                {{ str_replace('cam-password_confirmation',
                                                    "\"" . ___('auth.password-confirmation') .
                                                    "\"", $errors->first('cam-password_confirmation')) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="register mb-4">
                                <button class="btn">{!! ___('auth.enter-register') !!}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
