@php $seconds = 10 @endphp
@extends('layouts.main-no-auth')

@section('title', ___('auth.register'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="testList-wrap active">
                <div class="testIssue-wrap">
                    <div class="testIssueTitle mt-5">
                        <h3>{!! ___('auth.reset-ready.header') !!}</h3>
                    </div>
                    <div class="testIssueTitle">
                        <p>{!! ___('auth.reset-ready.text', [
                            'seconds' => '<span id="redirect-seconds">' . $seconds . "</span>"
                        ]) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        let seconds = {{ $seconds }},
            el = document.getElementById('redirect-seconds'),
            timer = setInterval(function(){
                el.textContent = --seconds;
                if (seconds <= 0) {
                    clearInterval(timer);
                    location.href = '/';
                }
            }, 1000);

    </script>
@stop
