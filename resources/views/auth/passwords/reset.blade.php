@extends('layouts.main-no-auth')

@section('title', ___('auth.login'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="tariffs">
                <div class="wrapper auth">
                    <div class="wrapBookModal-title">
                        <p class="bookModal-title">{!! ___('auth.restore') !!}</p>
                        @if(old('social-error'))
                            <p class="message error">{{ old('social-error') }}</p>
                        @endif
                    </div>
                    <form action="{{ route('password.reset', ['token' => null]) }}" method="post">
                        @csrf
                        <div class="doorInpForm">
                            <div class="doorInp-wrap">
                                <div class="doorInp">
                                    <p><label for="restore-email">
                                            {!! ___('auth.email') !!}
                                        </label></p>
                                    <input
                                            id="restore-email"
                                            type="email"
                                            placeholder="{!! ___('auth.email') !!}"
                                            name="cam-email"
                                            value="{{ old('cam-email') }}"
                                            data-error-id="cam-email"
                                            required="required"
                                            autofocus="autofocus"
                                            minlength="5"
                                    >
                                    @if ($errors->has('cam-email'))
                                        <p class="message error">
                                            {{ str_replace('cam-email ', '', $errors->first('cam-email')) }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="doorInp-wrap">
                                <center>{!! NoCaptcha::display() !!}</center>
                                @if ($errors->has('g-recaptcha-response'))
                                    <p class="message error">
                                        {{ $errors->first('g-recaptcha-response') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="consentDoor">
                            <a href="{{ route('login') }}">{!! ___('auth.already-registered') !!}</a>
                        </div>
                        <div class="register doorReg mb-5">
                            <button class="btn">{!! ___('auth.enter-restore') !!}</button>
                        </div>
                    </form>
                    <div class="wrapDoor-text">
                        <p class="bookModal-subtitle">{!! ___('auth.enter-social') !!}</p>
                    </div>
                    @php $social = getAvailableSocial(); @endphp
                    @if(count($social))
                        <div class="wrap-wrapToils">
                            <div class="toilsLink">
                                @foreach($social as $socName)
                                    <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                        @csrf
                                        <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="wrapDoor-text textModal">
                        <p class="bookModal-subtitle">{!! ___('auth.need-register') !!}</p>
                        <a href="{{ route('register') }}" class="no-decor" data-modal-id="regModal">{!! ___('auth.register') !!}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    {!! NoCaptcha::renderJs(app()->getLocale()) !!}
@stop
