@extends('layouts.main-no-auth')

@section('title', ___('auth.login'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="tariffs">
                <div class="wrapper auth">
                    <div class="wrapBookModal-title">
                        <p class="bookModal-title">{!! ___('auth.enter') !!}</p>
                        @if(old('social-error'))
                            <p class="message error">{{ old('social-error') }}</p>
                        @endif
                    </div>
                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        @if(old('referral'))
                            <input id="referral" type="hidden" name="cam-ref-link" value="{{ old('referral') }}">
                        @endif
                        <div class="doorInpForm mb-4">
                            <div class="doorInp-wrap">
                                <div class="doorInp">
                                    <p><label for="login">
                                        {!! ___('auth.email-login') !!}
                                    </label></p>
                                    <input
                                            id="login"
                                            type="text"
                                            placeholder="{!! ___('auth.email-login') !!}"
                                            name="cam-login"
                                            @if($errors->has('cam-login'))class="is-invalid"@endif
                                            value="{{ old('cam-login') }}"
                                            required="required"
                                            autofocus="autofocus"
                                            minlength="5"
                                            tabindex="1"
                                    >
                                    @if ($errors->has('cam-login'))
                                        <p class="message error">
                                            {{ $errors->first('cam-login') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="doorInp">
                                    <p><label for="password">
                                        {!! ___('auth.password') !!}
                                    </label></p>
                                    <input
                                            id="password"
                                            type="password"
                                            placeholder="{!! ___('auth.password') !!}"
                                            name="cam-password"
                                            required="required"
                                            minlength="6"
                                            tabindex="2"
                                    >
                                </div>
                            </div>
                            <div class="doorInp-wrap">
                                <div class="doorInp">
                                    <div class="dialectLang-list">
                                        <input type="checkbox" name="cam-remember" id="remember"{{ old('cam-remember') ? ' checked' : '' }}>
                                        <label for="remember" class="dialectLang" tabindex="3">{!! ___('auth.remember-me') !!}</label>
                                    </div>
                                </div>
                                <div class="doorInp">
                                    <a href="{{ route('password.reset', ['token' => null]) }}" class="forgot-password" tabindex="4">{!! ___('auth.forgot-password') !!}</a>
                                </div>
                            </div>
                        </div>
                        <div class="register doorReg mb-4">
                            <button type="submit" class="btn" tabindex="5">{!! ___('auth.enter-login') !!}</button>
                        </div>
                    </form>
                    <div class="wrapDoor-text">
                        <p class="bookModal-subtitle">{!! ___('auth.enter-social') !!}</p>
                    </div>
                    @php $social = getAvailableSocial(); @endphp
                    @if(count($social))
                        <div class="wrap-wrapToils mb-4">
                            <div class="toilsLink">
                                @foreach($social as $socName)
                                    <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                        @csrf
                                        <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="wrapDoor-text textModal">
                        <p class="bookModal-subtitle">{!! ___('auth.need-register') !!}</p>
                        <a href="{{ route('register') }}" class="no-decor">{!! ___('auth.register') !!}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
