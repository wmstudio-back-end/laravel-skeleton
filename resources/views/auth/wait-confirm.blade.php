@extends('layouts.main-no-auth')

@section('title', ___('auth.register'))

@section('content')
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="testList-wrap active">
                <div class="testIssue-wrap">
                    <div class="testIssueTitle mt-5">
                        <h3>{!! ___('auth.email-confirm-sent.header') !!}</h3>
                    </div>
                    <div class="testIssueTitle">
                        <p>{!! ___('auth.email-confirm-sent.text', [
                            'email' => $email,
                        ]) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection