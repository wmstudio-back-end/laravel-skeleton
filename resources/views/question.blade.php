@php
    /* @var $question \Modules\FAQ\Entities\FAQ */
    $transKey = \Modules\FAQ\Entities\FAQ::TRANS_GROUP;
    $layout = Auth::check() ? 'main' : 'main-no-auth';
@endphp

@extends('layouts.' . $layout)

@section('content')
    @if (Auth::check())
        <div class="subscription">
            <div class="booking-title">
                <h6>{!! ___('questions.questions') !!}</h6>
            </div>
            <div class="booking-subtitle">{!! ___('questions.description') !!}</div>
        </div>
    @else
        <div class="supportTitle">
            <div class="row">
                <div class="wrapper">
                    <h4>{!! ___('questions.questions') !!}</h4>
                </div>
            </div>
        </div>
        <div class="supportSubtitle">
            <div class="row">
                <div class="wrapper">{!! ___('questions.description') !!}</div>
            </div>
        </div>
    @endif
    {{--<div class="supportSearch">
        <div class="supportSearch-fon"></div>
        <div class="row">
            <div class="wrapper">
                <h3 class="marH3">{!! ___('questions.search.label') !!}</h3>
                <div class="language-search">
                    <input type="text" placeholder="{{ ___('questions.search.placeholder') }}">
                    <button type="button"></button>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="questionsSheet response">
                <div class="backList-wrap">
                    <div class="backList">
                        <a href="{{ route('faq') }}" class="link-over"></a>
                        <span class="backList-ic"></span>
                        <p>{!! ___('questions.back-to-all') !!}</p>
                    </div>
                </div>
                <div class="wrapper marBot40">
                    <p class="text fontS20">{{ transDef($question->name, $transKey . '.' . $question->group . '.' . $question->alias . '.name') }}</p>
                    <p class="subtext">{!! ___('questions.section') !!}: {{ transDef($question->group, 'faq.groups.' . $question->group) }}</p>
                </div>
                @php
                    $defaults = json_decode($question->content, true);
                    $showInfoBlock = (isset($defaults['info']['title']) && isset($defaults['info']['content']));
                @endphp
                @if(count($defaults['advice']) || $showInfoBlock)
                    <div class="response-text{{ !$showInfoBlock ? ' mb-5' : '' }}">
                        @foreach($defaults['advice'] as $key => $value)
                            @php
                                $value = ___($transKey . '.' . $question->group . '.' . $question->alias . '.content.advice.' . $key);
                                $strings = explode("\n", $value);
                                $title = array_shift($strings);
                                if (count($strings)) {
                                    $value = implode("<br>", $strings);
                                } else {
                                    $value = null;
                                }
                            @endphp
                            <p class="text">{{ $title }}</p>
                            @if(isset($value))
                                <p>{!! $value !!}</p>
                            @endif
                        @endforeach
                        @if($showInfoBlock)
                            <div class="tableText">
                                <span class="tableText-ic"></span>
                                <p class="text">{!! ___($transKey . '.' . $question->group . '.' . $question->alias . '.content.info.title') !!}</p>
                                <p>{!! str_replace("\n", "<br>", ___($transKey . '.' . $question->group . '.' . $question->alias . '.content.info.content')) !!}</p>
                            </div>
                        @endif
                    </div>
                @endif
                @if(count($defaults['manual']))
                    <div class="instruction-wrap">
                        <p class="instruction-title">{!! ___('questions.instruction') !!}</p>
                        @foreach($defaults['manual'] as $key => $value)
                            <div class="instruction">
                                <span class="instructionNum">{{ $key + 1 }}</span>
                                <p>{!! str_replace("\n", "<br>", ___($transKey . '.' . $question->group . '.' . $question->alias . '.content.manual.' . $key)) !!}</p>
                            </div>
                        @endforeach
                    </div>
                @endif
                @if(!Session::has('faq.' . $question->group . '.' . $question->alias))
                    <form action="{{ route('faq.useful', [
                        'group' => $question->group,
                        'alias' => $question->alias,
                    ]) }}" class="usefully" method="post">
                        @csrf
                        <p>{!! ___('questions.was-useful') !!}</p>
                        <button type="submit" name="useful" value="1" class="btn usefullyBtn">
                            {!! ___('global.yes') !!}
                        </button>
                        <button type="submit" name="useful" value="0" class="btn usefullyBtn">
                            {!! ___('global.no') !!}
                        </button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection