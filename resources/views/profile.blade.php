@php $user = Auth::user(); @endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! ___('profile.profile') !!}</h6>
        </div>
        <div class="booking-subtitle">
            @include('errors.message')
            {!! ___('profile.description') !!}
        </div>
    </div>
    <div class="findTeacher">
        <div class="row">
            <div class="wrapper">
                <div class="profile-text">
                    <p>{!! ___('profile.type.title') !!}</p>
                </div>
                <div class="wrapTabProf">
                    @if($user->hasStatus('s'))
                        <div
                            data-profile="profile-student"
                            class="btn select-profile tabProf{{ $user->getStatus() === 's' ? ' active' : '' }}"
                        >{{ Modules\Users\Constants\Cambly::getStatus('s') }}</div>
                    @endif
                    @if($user->hasStatus('t'))
                        <div
                            data-profile="profile-teacher"
                            class="btn select-profile tabProf{{ $user->getStatus() === 't' ? ' active' : '' }}"
                        >{{ Modules\Users\Constants\Cambly::getStatus('t') }}</div>
                    @elseif($user->hasStatus('q'))
                        <div
                            data-profile="profile-teacher"
                            class="btn select-profile tabProf not-confirmed{{ $user->getStatus() === 'q' ? ' active' : '' }}"
                        >{{ Modules\Users\Constants\Cambly::getStatus('t') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <form id="account-form" action="{{ route('user.profile') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="profile-type">
            @if($user->hasStatus('s'))
                <div id="profile-student" class="listTeachers{{ $user->getStatus() === 's' ? ' active' : '' }}">
                    <div class="row">
                        <div class="profileList">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.student.dialect') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'name' => 'st_dialect',
                                    'type' => 'checkbox',
                                    'label' => ___('profile.type.student.dialect'),
                                ])
                            </div>
                        </div>
                        <div class="profileList">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.student.lang-level') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectLevel',
                                    'class' => 'dialectLevel',
                                    'labelClass' => 'dialectLangRadio',
                                    'name' => 'st_lang_level',
                                    'type' => 'radio',
                                    'label' => ___('profile.type.student.lang-level'),
                                ])
                            </div>
                        </div>
                        <div class="profileList">
                            @include('partials.user.profile.textarea', [
                                'name' => 'st_learn_targets',
                                'label' => ___('profile.type.student.learn-targets'),
                            ])
                        </div>
                        <div class="profileList">
                            @include('partials.user.profile.textarea', [
                                'name' => 'st_about_me',
                                'label' => ___('profile.type.student.about-me'),
                            ])
                        </div>
                        <div class="profileList">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.student.lesson-type') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectLevel',
                                    'name' => 'st_lesson_type',
                                    'type' => 'checkbox',
                                    'label' => ___('profile.type.student.lesson-type'),
                                ])
                            </div>
                        </div>
                        <button type="submit" class="btn profileSave">{!! ___('global.save') !!}</button>
                    </div>
                </div>
            @endif
            @if($user->hasStatus('t') || $user->hasStatus('q'))
                <div id="profile-teacher" class="listTeachers{{ ($user->getStatus() === 't' || $user->getStatus() === 'q')
                    ? ' active' : '' }}">
                    <div class="row">
                        @if($user->hasStatus('q') && !$profileTeacherFull)
                            <div class="wrapper marBot">
                                {!! ___('profile.type.teacher.info') !!}
                            </div>
                        @elseif($user->hasStatus('q') && $profileTeacherFull)
                            <div class="wrapper marBot">
                                {!! ___('profile.type.teacher.info-wait-moderation') !!}
                            </div>
                        @endif
                        <div class="profileList{{ $user->addon('tc_profession') == null ? ' empty-teacher-field' : '' }}">
                            @include('partials.user.profile.input', [
                                'name' => 'tc_profession',
                                'label' => ___('profile.type.teacher.profession'),
                            ])
                        </div>
                        <div class="profileList{{ $user->addon('tc_dialect') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.dialect') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'name' => 'tc_dialect',
                                    'type' => 'checkbox',
                                    'label' => ___('profile.type.teacher.dialect')
                                ])
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_exp') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.exp') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectWork-wrap',
                                    'labelClass' => 'dialectWork btn',
                                    'name' => 'tc_exp',
                                    'type' => 'radio',
                                    'label' => ___('profile.type.teacher.exp')
                                ])
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_education') == null ? ' empty-teacher-field' : '' }}">
                            @include('partials.user.profile.textarea', [
                                'name' => 'tc_education',
                                'label' => ___('profile.type.teacher.education'),
                            ])
                        </div>
                        <div class="profileList{{ $user->addon('tc_about_me') == null ? ' empty-teacher-field' : '' }}">
                            @include('partials.user.profile.textarea', [
                                'name' => 'tc_about_me',
                                'label' => ___('profile.type.teacher.about-me'),
                            ])
                        </div>
                        <div class="profileList{{ $user->addon('tc_style') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.teach-style') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectLevel',
                                    'class' => 'dialectLevel',
                                    'labelClass' => 'dialectLangRadio',
                                    'name' => 'tc_style',
                                    'type' => 'radio',
                                    'label' => ___('profile.type.teacher.teach-style')
                                ])
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_prefer_level') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.prefer-level') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectLevel',
                                    'class' => 'dialectLangRadio',
                                    'list' => \Modules\Users\Entities\UserAddSettings::where('control_type', 'lang_level')->first()->transValue(),
                                    'name' => 'tc_prefer_level',
                                    'type' => 'checkbox',
                                    'label' =>  ___('profile.type.teacher.prefer-level'),
                                ])
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_lesson_type') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.lesson-type') !!}</p>
                            </div>
                            <div class="fillAcc">
                                @include('partials.user.profile.list-input', [
                                    'divClass' => 'dialectLevel',
                                    'name' => 'tc_lesson_type',
                                    'type' => 'checkbox',
                                    'label' => ___('profile.type.teacher.lesson-type')
                                ])
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_diploma_url') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <p>{!! ___('profile.type.teacher.diploma.title') !!}</p>
                            </div>
                            <div class="fillAcc" title="{!! ___('profile.type.teacher.diploma.limit', [
                                        'limit' => \Modules\Users\Entities\User::MAX_DIPLOMA_FILE_SIZE
                                    ]) !!}">
                                <input
                                    name="tc_diploma_url"
                                    type="file"
                                    class="hidden"
                                    accept=".pdf,.jpg,.png,.bmp"
                                    data-err-format="{{ ___('profile.type.teacher.diploma.err-format') }}"
                                >
                                @if($user->addon('tc_diploma_url') == null)
                                    {!! ___('profile.type.teacher.diploma.description') !!}
                                @else
                                    <a href="{{ $user->addon('tc_diploma_url') }}" target="_blank">
                                        {!! ___('profile.type.teacher.diploma.link') !!}
                                    </a>
                                @endif
                                <button type="button" class="downloadLink diploma-upload">
                                    <span class="download-ic"></span>
                                    {!! ___('profile.type.teacher.diploma.upload') !!}
                                </button>
                                <span id="fileName"></span>
                            </div>
                        </div>
                        <div class="profileList{{ $user->addon('tc_video_url') == null ? ' empty-teacher-field' : '' }}">
                            <div  class="nameFill">
                                <label for="tc_video_url">{!! ___('profile.type.teacher.video.title') !!}</label>
                            </div>
                            <div class="fillAcc">
                                <div class="videoText">
                                    {!! ___('profile.type.teacher.video.description') !!}
                                    <input
                                        type="text"
                                        id="tc_video_url"
                                        name="tc_video_url"
                                        class="fillAcc-link{{ $errors->has('tc_video_url') ? ' is-invalid' : '' }}"
                                        placeholder="{{ ___('profile.type.teacher.video.placeholder') }}"
                                        value="{{ $user->addon('tc_video_url') }}"
                                    >
                                    @if ($errors->has('tc_video_url'))
                                        <p class="message error">
                                            {{ str_replace('tc video url', '"' . $label . '"', $errors->first('tc_video_url')) }}
                                        </p>
                                    @endif
                                </div>
                                <div class="videoIc-wrap">
                                    <span class="videoIc"></span>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn profileSave">{!! $user->hasStatus('q')
                            ? ___('profile.type.teacher.send-for-moderation')
                            : ___('global.save')
                        !!}</button>
                    </div>
                </div>
            @endif
        </div>
    </form>
@endsection