<header>
    <div class="header-wrap">
        <div class="large-row">
            <div class="flex">
                <div class="wrap-logo">
                    <a href="{{ route('home') }}" class="logo">
                        <img src="{{ asset('img/logo.svg') }}">
                    </a>
                </div>
                <div class="hamburger mob hamburger--emphatic">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
                <ul class="menu-h">
                    @include('partials.menu.header', ['menuMame' => 'dbMenu'])
                </ul>

                <div class="right-h">
                    <ul>
                        @if(($user = Auth::user()) && $user->getStatus() === 't')
                            @if(!isRoute('take-money') && ($balance = $user->addon('tc_balance') ?: 0))
                                <li class="wrap_button">
                                    <a href="{{ route('payments.history') }}" class="btn submitSubscribe">{!!___('tariffs.take-money', [
                                        'count' => $balance,
                                        'currency' => ___('tariffs.currency'),
                                    ]) !!}</a>
                                </li>
                            @endif
                        @elseif(!isRoute('tariffs'))
                            <li class="wrap_button">
                                @if($user && $user->getStatus() === 's' && $user->getActivePayment())
                                    <a href="{{ route('tariffs') }}" class="btn submitSubscribe">{!!___('tariffs.renew-subscription') !!}</a>
                                @elseif(!$user || ($user && !empty($user->getStatus())))
                                    <a href="{{ route('tariffs') }}" class="btn submitSubscribe">{!!___('tariffs.subscribe') !!}</a>
                                @endif
                            </li>
                        @endif
                        @if(!($user || isRoute(['login', 'password.reset', 'register', 'wait-confirm', 'email-confirm', 'last-step'])))
                            <li class="wrap_door">
                                <a href="{{ route('login') }}" data-modal-id="doorModal">
                                    <div class="wrap_navIc">
                                        <span class="navIc"></span>
                                    </div>
                                    <div class="right-h_door">
                                        <p>{!! ___('auth.enter') !!}</p>
                                    </div>
                                </a>
                            </li>
                        @endif
                        <li id="select-lang" class="wrap_language" style="opacity: 0">
                            <select class="language select2" data-color="white">
                                @foreach(getLocales() as $key => $name)
                                    <option value="{{ $key }}"{{ App::getLocale() == $key ? ' selected="selected"' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
