<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    @include('layouts.favicons')

    @php $title = setting('main_title'); @endphp
    @if (trim($__env->yieldContent('title')) && trim($__env->yieldContent('title')) != config('admin.name'))
        <title>@yield('title') | {{ $title }}</title>
    @else
        <title>{{ $title }}</title>
    @endif
    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title h1.title {
            font-size: 100px;
            margin: 0;
        }

        .title p.title {
            margin: 0;
            font-size: 50px;
        }

        .title .title:focus,
        .title .title:active {
            outline: none;
        }
        .title a,
        .title button.title {
            text-decoration: none;
            color: #B0BEC5;
        }

        .title button.title {
            background: none;
            padding: 0;
            border: none;
            font-family: Lato;
            font-size: 30px;
            font-weight: 100;
            cursor: pointer;
        }

        .title a.title,
        .title button.title {
            font-size: 30px;
            float: right;
            margin: 50px 0 0 30px;
        }
        .title a:hover,
        .title button.title:hover {
            color: #72c1e8;
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="content">
            @yield('content')
        </div>
    </div>
</body>
</html>