@php $user = \Auth::user(); @endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.favicons')

    @yield('seo')

    @php
        $title = null;
        if (url()->current() !== url('/')) {
            $title = getMenu('dbMenu')->render([
                'onlyText' => true,
                'reverse' => true,
                'separator' => '|',
            ]);
        }
        if (trim($__env->yieldContent('title'))) {
            $title = trim($__env->yieldContent('title')) . ($title ? ' | ' . $title : '');
        }
        $mainTitle = setting('main_title') ?: config('app.name', 'Laravel');
    @endphp
    @if ($title)
        <title>{{ $title }} | {{ $mainTitle }}</title>
    @else
        <title>{{ $mainTitle }}</title>
    @endif

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    @include('layouts.header')

    <div class="content">

        <div class="large-row">

            <div class="wrap-sidebar">
                @include('layouts.sidebar')

                <div class="sidebar-right">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    @if ($user->getStatus())
        @include('modals.chat')
        @include('modals.videoChat')
    @endif

    @include('layouts.footer')

    <div class="large-row modal-row">
        @include('modals.reviews')
        @yield('modals')
    </div>

    @if(!Request::is(config('admin.defaults.prefix') . '/*'))
        @if(setting('yandex_metrika_enabled') && !setting('yandex_metrika_demo'))
            {!! metrikaCounter() !!}
        @endif
        @if(count($availableAdminRoutes = getAvailableAdminRoutes()))
            <div class="absolute-link">
                <a href="{{ route($availableAdminRoutes[0]) }}" class="admin">{{ ___('global.dashboard') }}</a>
            </div>
        @endif
    @endif
    <script id="lang-and-status" type="text/javascript">
        window.currLang = '{{ App::getLocale() }}';
        window.showRating = {{ config('view.show-rating') ? 'true' : 'false' }};
        window.token = '{{ csrf_token() }}';
        window.favoritesAction = '{{ route('favorites') }}';
        window.favorites = {!! json_encode($user->getFavorites()) !!};
        window.ratingAction = '{{ route('rating') }}';
        @if($user->getStatus() !== null)
            window.userStatus = '{{ $user->getStatus() }}';
            window.userWatchList = {!! json_encode($user->getWatchList()) !!};
            @if ($user->getStatus() === 's')
                window.availableTime = {!! $user->getAvailableTime() !!};
            @endif
        @endif
    </script>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
