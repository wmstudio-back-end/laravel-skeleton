<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.favicons')

    @php
        $seo = getSeo(app('router')->getCurrentRoute()->getActionName());
    @endphp
    @if(isset($seo['seo_keywords']))
        <meta name="keywords" content="{{ $seo['seo_keywords'] }}">
    @endif
    @if(isset($seo['seo_description']))
        <meta name="description" content="{{ $seo['seo_description'] }}">
    @endif

    @php
        $title = isset($seo['seo_title']) ? $seo['seo_title'] : getMenu('dbMenu')->render([
            'onlyText' => true,
            'reverse' => true,
            'separator' => '|',
        ]);
        $mainTitle = setting('main_title') ?: config('app.name', 'Laravel');
    @endphp
    @if ($title)
        <title>{{ $title }} | {{ $mainTitle }}</title>
    @else
        <title>{{ $mainTitle }}</title>
    @endif

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    @include('layouts.header')

    <div class="content">
        @yield('content')
    </div>

    @include('layouts.footer-no-auth')

    @if (trim($__env->yieldContent('modals')) || !isRoute(['login', 'password.reset', 'register', 'wait-confirm', 'email-confirm', 'last-step']))
        <div class="large-row">
            @if(!isRoute(['login', 'register', 'wait-confirm', 'email-confirm', 'last-step']))
                @include('modals.login-register')
            @endif
            @yield('modals')
        </div>
    @endif

    @if(setting('yandex_metrika_enabled') && !setting('yandex_metrika_demo'))
        {!! metrikaCounter() !!}
    @endif
    <script id="lang-and-status" type="text/javascript">
        window.currLang = '{{ App::getLocale() }}';
        window.showRating = {{ config('view.show-rating') ? 'true' : 'false' }};
        window.token = '{{ csrf_token() }}';
        window.favoritesAction = '{{ route('favorites') }}';
        window.ratingAction = '{{ route('rating') }}';
    </script>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
</body>
</html>
