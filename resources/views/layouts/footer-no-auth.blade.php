<footer>
    <div class="indent">
        <div class="row">
            <div class="wrap-flex">
                <div class="footer-portal">
                    {!! ___('global.footer.portal') !!}
                </div>
                <div class="footer-info">
                    <div class="footer-info-link">
                        <p class="uppercase">{!! ___('global.footer.info.link.title') !!}</p>
                        @include('partials.menu.footer', ['menuMame' => 'dbMenu'])
                    </div>
                    <div class="footer-additionally-link">
                        <p class="uppercase">{!! ___('global.footer.info.additionally-link.title') !!}</p>
                        @php $privacyPolicy = setting('privacy_policy') @endphp
                        @if(!empty($privacyPolicy))
                            <a href="{{ url($privacyPolicy) }}" class="first-upper">{!!
                                ___('global.footer.info.additionally-link.privacy_policy') !!}</a>
                        @endif
                    </div>
                </div>
                <div class="footer-notify">
                    @if(!empty($vk) || !empty($fb) || !empty($tw))
                        <div class="footer-social-link">
                            <p class="uppercase">{!! ___('global.footer.notify.social-link.title') !!}</p>
                            @if(!empty($vk))
                                <a href="{{ $vk }}" target="_blank"><span class="vk"></span></a>
                            @endif
                            @if(!empty($fb))
                                <a href="{{ $fb }}" target="_blank"><span class="fBook"></span></a>
                            @endif
                            @if(!empty($tw))
                                <a href="{{ $tw }}" target="_blank"><span class="twit"></span></a>
                            @endif
                        </div>
                    @endif
                </div>
                @php
                    $vk = setting('social_vk');
                    $fb = setting('social_fb');
                    $tw = setting('social_tw');
                    $in = setting('social_in');
                @endphp
                @if(!empty($vk) || !empty($fb) || !empty($tw) || !empty($in))
                    <div class="footer-social-link">
                        <p class="uppercase">{!! ___('global.footer.notify.social-link.title') !!}</p>
                        @if(!empty($vk))
                            <a href="{{ url($vk) }}" target="_blank"><span class="vk"></span></a>
                        @endif
                        @if(!empty($fb))
                            <a href="{{ url($fb) }}" target="_blank"><span class="fBook"></span></a>
                        @endif
                        @if(!empty($tw))
                            <a href="{{ url($tw) }}" target="_blank"><span class="twit"></span></a>
                        @endif
                        @if(!empty($in))
                            <a href="{{ url($in) }}" target="_blank"><span class="in"></span></a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="copyright">
        <p>Copyright {{ '@' . date_format(new \DateTime(), 'Y') }}. All Rights Reserved.</p>
    </div>
</footer>
