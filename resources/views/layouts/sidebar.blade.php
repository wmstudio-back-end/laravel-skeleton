@php
$user = Auth::user();
$status = $user->getStatus();
@endphp
<div class="sidebar {{ $status == 's' ? 'student' : 'teacher' }}">
    <div class="wrap-prof">
        <div class="profile-face">
            <img src="{{ $user->getAvatar() }}">
            <p>{{ $user->getFullName() }}</p>
        </div>
        @if($status)
            <div class="{{ $status == 's' ? 'studentSelect' : 'teacherList' }}">
                @php
                    $statuses = $user->addon('status');
                    $allStatuses = false;
                    if (strpos($statuses, 's') !== false && strpos($statuses, 't') !== false) {
                        $allStatuses = true;
                    }
                @endphp
                @if($allStatuses)
                    <select id="change-profile-select" class="select2" data-color="{{ $status == 's' ? 'green' : 'blue' }}">
                        <option
                            value="s"
                            @if($status == 's')
                                selected="selected"
                            @else
                                data-change-status="{{ route('user.status.change', ['status' => 's']) }}"
                            @endif
                        >{{ \Modules\Users\Constants\Cambly::getStatus('s') }}</option>
                        <option
                            value="t"
                            @if($status == 't')
                                selected="selected"
                            @else
                                data-change-status="{{ route('user.status.change', ['status' => 't']) }}"
                            @endif
                        >{{ \Modules\Users\Constants\Cambly::getStatus('t') }}</option>
                    </select>
                @else
                    <div class="sidebar-select btn {{ $status == 's'
                        ? 'stud'
                        : ($status == 'q'
                            ? (bool)$user->addon('tc_was_permitted')
                                ? 'rejected'
                                : 'not-confirmed'
                            : '') }}">
                        {{ \Modules\Users\Constants\Cambly::getStatus($status == 'q' ? 't' : $status) }}
                    </div>
                @endif
            </div>
        @endif
    </div>
    @if($status === 't')
        <div class="sidebar-block ccount-prof">
            <a href="{{ route('topics.my') }}" class="link-over"></a>
            <div class="teach-topic ic"></div>
            <p>{!! ___('global.sidebar.my-topics') !!}</p>
        </div>
    @elseif($status === 's')
        <div class="sidebar-block ccount-prof">
            <a href="{{ route('payments') }}" class="link-over"></a>
            <div class="sidebar-payments ic"></div>
            <p>{!! ___('global.sidebar.payments') !!}</p>
        </div>
    @endif
    @if(getPermissions('user.profile'))
        <div class="sidebar-block ccount-prof">
            <a href="{{ route('user.profile') }}" class="link-over"></a>
            <div class="{{ $status == 't' ? 'teach' : 'sidebar' }}-prof ic"></div>
            <p>{!! ___('global.sidebar.profile') !!}</p>
        </div>
    @endif
    <div class="sidebar-block ccount-acc">
        <a href="{{ route('user.account') }}" class="link-over"></a>
        <div class="{{ $status == 't' ? 'teach' : 'sidebar' }}-acc ic"></div>
        <p>{!! ___('global.sidebar.account') !!}</p>
    </div>
    <div class="sidebar-block ccount-output">
        <a href="{{ route('logout') }}" class="link-over"></a>
        <div class="{{ $status == 't' ? 'teach' : 'sidebar' }}-output ic"></div>
        <p>{!! ___('global.sidebar.logout') !!}</p>
    </div>
</div>