@php /* @var $review \Modules\Reviews\Entities\Review */ @endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @php
        $seo = getSeo(app('router')->getCurrentRoute()->getActionName());
    @endphp
    @if(isset($seo['seo_keywords']))
        <meta name="keywords" content="{{ $seo['seo_keywords'] }}">
    @endif
    @if(isset($seo['seo_description']))
        <meta name="description" content="{{ $seo['seo_description'] }}">
    @endif

    @php $title = isset($seo['seo_title']) ? $seo['seo_title'] : (setting('main_title') ?: config('app.name', 'Laravel')); @endphp
    <title>{{ $title }}</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="do-nicescrol">
    @include('layouts.header')

    <div class="content">
        <div class="homeScreen">
            <div class="homeScreen_fon-ic">
                <span></span>
            </div>
            <div class="row">
                <div class="letter">
                    <div class="column">
                        {!! ___('landing.homeScreen.header') !!}
                        {!! ___('landing.homeScreen.learn-language') !!}
                        <a href="{{ route('register') }}" class="button" data-modal-id="regModal">{!! ___('landing.homeScreen.button') !!}</a>
                    </div>
                    <div class="column-tv">
                        <span class="monitor"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="advantages">
            <div class="row">
                <div class="wrap-h2">
                    <h2>{!! ___('landing.advHeader.header') !!}</h2>
                </div>
                <div class="wrap-flex">
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_time"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advHeader.list.first.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advHeader.list.first.content') !!}
                        </div>
                    </div>
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_sms"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advHeader.list.second.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advHeader.list.second.content') !!}
                        </div>
                    </div>
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_phone"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advHeader.list.third.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advHeader.list.third.content') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="someone">
            <div class="row">
                <div class="wrap-h2">
                    <h2>{!! ___('landing.someone.header') !!}</h2>
                </div>
                <div class="wrap-flex">
                    <div class="column-flatbed"></div>
                    <div class="column-join">
                        <div class="ic_men-join">
                            <span></span>
                            <div>
                                {!! ___('landing.someone.first-title') !!}
                            </div>
                        </div>
                        <div class="ic_girl-join">
                            <span></span>
                            <div>
                                {!! ___('landing.someone.second-title') !!}
                            </div>
                        </div>
                        <div class="column-text">
                            {!! ___('landing.someone.content') !!}
                        </div>
                        <a href="{{ route('register') }}" class="join-button" data-modal-id="regModal">{!! ___('landing.someone.button') !!}</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="theme">
            <span class="theme-icDb"></span>
            <div class="row">
                <div class="wrap-h2">
                    <h2>{!! ___('landing.theme.header') !!}</h2>
                </div>
                <div class="wrap-flex flex-padding">
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo1.png') }}">
                        <div class="theme-topic business">
                            {!! ___('landing.theme.business') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo2.png') }}">
                        <div class="theme-topic news-sport">
                            {!! ___('landing.theme.world_news') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo3.png') }}">
                        <div class="theme-topic game-culture">
                            {!! ___('landing.theme.entertainment') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo4.png') }}">
                        <div class="theme-topic study-literature">
                            {!! ___('landing.theme.science_and_technology') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo5.png') }}">
                        <div class="theme-topic news-sport">
                            {!! ___('landing.theme.sport') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo6.png') }}">
                        <div class="theme-topic game-culture">
                            {!! ___('landing.theme.culture&traditions') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo7.png') }}">
                        <div class="theme-topic study-literature">
                            {!! ___('landing.theme.literature') !!}
                        </div>
                    </div>
                    <div class="theme-photo">
                        <img src="{{ asset('/img/theme-photo8.png') }}">
                        <div class="theme-topic food">
                            {!! ___('landing.theme.food_and_cooking') !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>

        @if($reviews->count())
            <div class="comments">
                <div class="row">
                    <div class="wrap-h2">
                        <h2>{!! ___('landing.comments.header') !!}</h2>
                    </div>
                    <div class="comments-slick">
                        <div class="commas"></div>
                        <div class="slider-nav{{ $reviews->count() > 4 ? ' moved-track' : '' }}">
                            @foreach($reviews as $review)
                                <div class="item">
                                    <div class="item-wrap">
                                        <img src="{{ $review->user->getAvatar() }}">
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="slider-for">
                            @foreach($reviews as $review)
                                @php
                                    $user = $review->user;
                                    $status = $review->user->getStatus();
                                    $country = Modules\Translations\Constants\Languages::getCountry($user->addon('country'));
                                    $dialects = \Modules\Users\Entities\UserAddSettings::where('control_type', 'dialect')
                                        ->first()->transValue($user->addon(($status === 't' ? 'tc' : 'st') . '_dialect'));
                                    if (count($dialects)) {
                                        $dialects = ', <span class="fontM">'
                                            . mb_strtolower(___('user-info.dialect')) . ' '
                                            . implode(', ', $dialects) . '</span>';
                                    } else {
                                        $dialects = '';
                                    }
                                @endphp
                                <div>
                                    <div class="comments-slick-rating">
                                        <div class="rating">
                                            <select
                                                class="rating"
                                                data-theme="fontawesome-stars-o"
                                                data-readonly="true"
                                                data-init="{{ $review->rating }}"
                                            >
                                                @for ($i = 1; $i <= $review::MAX_RATING; $i++)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <p class="latif">
                                            <strong>{{ $user->getFullName() }}</strong>
                                            {!! $status === 't'
                                                ? ___('landing.comments.teacher')
                                                : ___('landing.comments.student')
                                            !!}
                                        </p>
                                        <p><span class="rating-ic"></span>{!! $country . $dialects !!}</p>
                                    </div>
                                    <div class="comments-slick-com">
                                        <span></span>
                                        <p>{!! str_replace("\n", '<br>', strip_tags(str_replace('  ', '&nbsp;&nbsp;', $review->content))) !!}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="comments-training">
                            <a href="{{ route('register') }}" data-modal-id="regModal">{!! ___('landing.comments.button') !!}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="works">
            <div class="row">
                <div class="wrap-h2">
                    <h2>{!! ___('landing.works.header') !!}</h2>
                </div>
                <div class="tabs">
                    @php $steps = config('transDefault.landing.works.steps'); @endphp
                    @foreach($steps as $key => $step)
                        <input id="tab{{ $key }}" type="radio" name="tabs"{{ (reset($steps) == $step) ? ' checked="checked"' : '' }}>
                        <label for="tab{{ $key }}">
                            <span>{{ $key }}</span>
                            {!! ___('landing.works.steps.' . $key . '.title') !!}
                        </label>
                    @endforeach

                    <section id="content-tab1">
                        <div class="works-tv">
                            <img src="{{ asset('/img/works-tv-img1.png') }}">
                        </div>
                    </section>
                    <section id="content-tab2">
                        <div class="works-tv">
                            <img src="{{ asset('/img/works-tv-img2.png') }}">
                        </div>
                    </section>
                    <section id="content-tab3">
                        <div class="works-tv">
                            <img src="{{ asset('/img/works-tv-img3.png') }}">
                        </div>
                    </section>
                    <section id="content-tab4">
                        <div class="works-tv">
                            <img src="{{ asset('/img/works-tv-img4.png') }}">
                        </div>
                    </section>
                    <div class="tabs-next" id="tabs-next">
                        <span class="tabs-line"></span>
                        <div><span></span></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="teacher mt-5">
            <div class="row wrap-h2">
                <div>
                    <h2>{!! ___('landing.teacher.header') !!}</h2>
                </div>
            </div>
            <div class="teacher-flex">
                <div class="teacher-video">
                    <div>
                        <img src="{{ asset('/img/stubs/video_1.jpg') }}">
                        <span></span>
                    </div>
                </div>
                <div class="teacher-video">
                    <div>
                        <img src="{{ asset('/img/stubs/video_2.jpg') }}">
                        <span></span>
                    </div>
                </div>
                <div class="teacher-video">
                    <div>
                        <img src="{{ asset('/img/stubs/video_3.jpg') }}">
                        <span></span>
                    </div>
                </div>

            </div>
            <div class="wrap_teacher-search">
                <a href="{{ route('register') }}" class="teacher-search" data-modal-id="regModal">{!! ___('landing.teacher.button') !!}</a>
            </div>
        </div>

        <div class="advantages">
            <div class="row">
                <div class="wrap-h2">
                    <h2>{!! ___('landing.advFooter.header') !!}</h2>
                </div>
                <div class="wrap-flex">
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_timeManagement"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advFooter.list.first.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advFooter.list.first.content') !!}
                        </div>
                    </div>
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_opportunity"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advFooter.list.second.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advFooter.list.second.content') !!}
                        </div>
                    </div>
                    <div class="column-flex">
                        <div class="advantages-ic">
                            <span class="advantages-ic_portal"></span>
                        </div>
                        <div class="title">
                            <p>{!! ___('landing.advFooter.list.third.title') !!}</p>
                        </div>
                        <div class="wrap-text">
                            {!! ___('landing.advFooter.list.third.content') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer-no-auth')

    <div class="large-row">
        @include('modals.login-register')
    </div>

    {!! metrikaCounter() !!}
    <script src="{{ mix('js/app.js') }}"></script>
    {!! NoCaptcha::renderJs(app()->getLocale()) !!}
</body>
</html>
