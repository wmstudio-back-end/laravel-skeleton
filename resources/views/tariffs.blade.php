@php
    $auth = Auth::check();
    $layout = $auth ? 'main' : 'main-no-auth';
@endphp

@extends('layouts.' . $layout)

@php
    $currencies = getCurrencies();
    $activeCurrency = Session::get('user-currency');
    if (!isset($currencies[$activeCurrency])) {
        if (isset($currencies['RUB'])) {
            $activeCurrency = 'RUB';
        } else {
            $activeCurrency = key($currencies);
        }
    }
    $currencySymbol = $currencies[$activeCurrency]['symbol'];
@endphp

@section('content')
    <div class="subscription{{ !$auth ? ' no-auth' : '' }}">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! ___('tariffs.tariffs') !!}</h6>
        </div>
        <div class="booking-subtitle">
            @include('errors.message')
            {!! ___('tariffs.description') !!}
        </div>
    </div>
    @if($errors->count())
        <div class="findTeacher">
            <div class="row">
                <div class="subscribe-errors">
                    @php $messages = array_column($errors->getMessages(), 0); @endphp
                    @foreach($messages as $message)
                        <p>{{ $message }}</p>
                    @endforeach
                </div>
            </div>
        </div>
    @elseif(!$session)
        <div class="findTeacher">
            <div class="row">
                <h3>{!! ___('tariffs.not-changed') !!}</h3>
            </div>
        </div>
    @endif
    <div class="questionsSheet-wrap">
        <div class="wrapTariffs">
            <div class="row">
                <form method="post" action="{{ route('tariff.subscribe') }}">
                    @csrf
                    <input type="hidden" id="promo-discount" name="promo-discount" value="no">
                    <div class="tariffs">
                        <div class="wayLearning">
                            <div class="paragraph">
                                <span class="around">1</span>
                                <p class="paragraph-title">{!! ___('tariffs.method') !!}</p>
                            </div>
                            <div class="formStudies">
                                <div class="quotient-wrap">
                                    <input
                                        type="radio"
                                        id="private"
                                        @if ($selected['group-discount'] === 'no')checked="checked"@endif
                                        name="group-discount"
                                        value="no"
                                        data-res-text="{{ ___('tariffs.result.method.personal') }}"
                                    >
                                    <label for="private" class="quotient btn select-type">
                                        <span class="quotient-ic"></span>
                                        {!! ___('tariffs.methods.personal.name') !!}
                                    </label>
                                    <div class="explanation" id="explanation1">
                                        <span class="explanation-ic"></span>
                                        <p>{!! ___('tariffs.methods.personal.description') !!}</p>
                                    </div>
                                </div>
                                <div class="quotient-wrap">
                                    <input
                                        type="radio"
                                        id="groups"
                                        @if ($selected['group-discount'] == $groupDiscount)checked="checked"@endif
                                        name="group-discount"
                                        value="{{ $groupDiscount }}"
                                        data-res-text="{{ ___('tariffs.result.method.group') }}"
                                    >
                                    <label for="groups" class="quotient btn select-type">
                                        <span class="group-ic"></span>
                                        {!! ___('tariffs.methods.group.name') !!}
                                    </label>
                                    <div class="explanation" id="explanation2">
                                        <span class="explanation-ic groupsExplanation-ic"></span>
                                        <p>{!! ___('tariffs.methods.group.description')
                                            . ($groupDiscount > 0 ? ' ' . ___('tariffs.methods.group.discount', ['percents' => $groupDiscount]) : '') !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="wayLearning">
                            <div class="paragraph">
                                <span class="around">2</span>
                                <p class="paragraph-title">{!! ___('tariffs.studyingTime') !!}</p>
                            </div>
                            <div class="planWeek">
                                <select id="select-minutes" name="minutes-per-day" class="select2" data-color="white">
                                    @foreach($countMinutes as $val)
                                        <option
                                            value="{{ $val }}"
                                            @if ($selected['minutes-per-day'] === $val)selected="selected"@endif
                                        >{{ $val }} {!! ___('tariffs.minutesPerDay') !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="planWeek">
                                <select id="select-count" name="lessons-per-week" class="select2" data-color="white">
                                    @foreach($countDays as $val)
                                        <option
                                            value="{{ $val }}"
                                            @if ($selected['lessons-per-week'] === $val)selected="selected"@endif
                                        >{{ $val }} {!! ___('tariffs.lessonsPerWeek', [
                                            'count' => numEnding($val, [
                                                ___('tariffs.lessonEndings.one'),
                                                ___('tariffs.lessonEndings.two'),
                                                ___('tariffs.lessonEndings.five'),
                                            ])
                                        ]) !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="periodStudy">
                            <div class="wrapParagraph">
                                <div class="paragraph">
                                    <span class="around">3</span>
                                    <p class="paragraph-title">{!! ___('tariffs.period') !!}</p>
                                </div>
                            </div>

                            <div class="wrap-currency">
                                <div class="currency-text">{!! ___('tariffs.change-currency') !!}</div>
                                <div class="wrap-curr">
                                    <div class="currency">
                                        @foreach($currencies as $currency => $val)
                                            <label
                                                class="currency-name{{ $currency == $activeCurrency ? ' active' : '' }} currency-change"
                                                data-name="{{ $currency }}"
                                                data-symbol="{!! $val['symbol'] !!}"
                                                data-diff="{{ $val['diff'] }}"
                                            >
                                                @include('partials.currency', [
                                                    'currency' => $val['symbol'],
                                                    'name' => $val['name'],
                                                    'size' => 22,
                                                ])
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            @php $weekMinutes = $selected['minutes-per-day'] * $selected['lessons-per-week']; @endphp
                            @foreach($tariffs as $name => $value)
                                <input
                                    type="radio"
                                    id="{{ $name }}"
                                    name="tariff-name"
                                    data-period="{{ $value['period'] }}"
                                    data-period-weeks="{{ $value['period-weeks'] }}"
                                    data-discount="{{ $value['discount'] }}"
                                    value="{{ $name }}"
                                    @if ($selected['tariff-name'] === $name)checked="checked"@endif
                                >
                                <div class="wrapMonth">
                                    <label for="{{ $name }}" class="link-over select-period"></label>
                                    <h3>{!! ___('dictionary.' . $value['name']) !!}</h3>
                                    <p class="tariff-period">{!! $value['period'] . ' ' . numEnding($value['period'], [
                                        ___('tariffs.periodEndings.one'),
                                        ___('tariffs.periodEndings.two'),
                                        ___('tariffs.periodEndings.five'),
                                    ]) !!}</p>
                                    <p class="colorGr">{!! ___('tariffs.paymentDescription') !!}</p>
                                    <div class="wrapCalendar-ic">
                                        @php
                                            switch ($name) {
                                                case 'tariff-1':
                                                    echo '<span class="calendar-ic"></span>';
                                                    break;
                                                case 'tariff-2':
                                                    echo '<span class="quarter-ic"></span>';
                                                    echo '<p class="discount">' . $value['discount'] . '%</p>';
                                                    break;
                                                case 'tariff-3':
                                                    echo '<span class="semester-ic"></span>';
                                                    echo '<p class="discount discount20">' . $value['discount'] . '%</p>';
                                                    break;
                                                case 'tariff-4':
                                                    echo '<span class="year-ic"></span>';
                                                    echo '<p class="discount discount25">' . $value['discount'] . '%</p>';
                                                    break;
                                            }
                                        @endphp
                                    </div>
                                    <div class="price-wrap">
                                        @php $price = $pricePerMinute * $weekMinutes * $value['period-weeks']; @endphp
                                        @if($value['discount'])
                                            <p class="colorGr through">
                                                <span class="originalPrice">{{ number_format($price, 2, ',', ' ') }}</span>
                                                <span class="currency-symbol">{!! $currencySymbol !!}</span>
                                            </p>
                                        @endif
                                        <p class="price">
                                            <span class="discountPrice">{{ number_format($price * ((100 - $value['discount']) / 100),
                                                2, ',', ' ') }}</span>
                                            <span class="currency-symbol">{!! $currencySymbol !!}</span>
                                        </p>
                                        @if($value['discount'])
                                            <p class="colorGr">{!! ___('tariffs.discount') !!} {{ $value['discount'] }}%</p>
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                            <div class="periodStudy-selected">
                                <div class="wrapH5">
                                    <h5>{!! ___('tariffs.result.header') !!}</h5>
                                </div>
                                <div class="table-wrap">
                                    <table border="1" cellpadding="5" class="table">
                                        <tr>
                                            <td>1</td>
                                            <td>{!! ___('tariffs.result.method.header') !!}</td>
                                            <td id="selected-type">{!! $selected['group-discount'] === 'no'
                                                ? ___('tariffs.result.method.personal')
                                                : ___('tariffs.result.method.group')
                                            !!}</td>
                                            <td rowspan="4">
                                                @php $price = $pricePerMinute * $weekMinutes; @endphp
                                                <p>{!! ___('tariffs.result.sum') !!}:
                                                    <span id="originalPrice">{{ number_format($price * $tariffs[$selected['tariff-name']]['period-weeks'],
                                                        2, ',', ' ') }}</span>
                                                    <span class="currency-symbol">{!! $currencySymbol !!}</span>/{!!
                                                        ___('tariffs.periodEndings.one') !!}
                                                </p>
                                                <p>{!! ___('tariffs.discount') !!}:
                                                    <span id="discount">{{ $tariffs[$selected['tariff-name']]['discount'] }}</span>%
                                                </p>
                                                <p style="display: none">{!! ___('tariffs.discountPromo') !!}:
                                                    <span id="discountPromo">{{ $selected['promo-discount'] === 'no'
                                                        ? 0
                                                        : $selected['promo-discount']
                                                    }}</span>%
                                                </p>
                                                <p style="display: none">{!! ___('tariffs.discountGroup') !!}:
                                                    <span id="groupDiscount">{{ $selected['group-discount'] === 'no'
                                                        ? 0
                                                        : $selected['group-discount']
                                                    }}</span>%
                                                </p>
                                                <p class="table-result">{!! ___('tariffs.result.total') !!}:
                                                    <span id="discountPrice">{{
                                                        number_format(
                                                            ($price * $tariffs[$selected['tariff-name']]['period-weeks'])
                                                            * ((100 - $tariffs[$selected['tariff-name']]['discount']) / 100)
                                                            * ((100 - ($selected['group-discount'] === 'no' ? 0 : $selected['group-discount'])) / 100)
                                                            * ((100 - ($selected['promo-discount'] === 'no' ? 0 : $selected['promo-discount'])) / 100),
                                                            2, ',', ' ')
                                                     }}</span>
                                                    <span class="currency-symbol">{!! $currencySymbol !!}</span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>{!! ___('tariffs.result.minutesPerDay') !!}</td>
                                            <td>
                                                <span id="minutesPerLesson">{!! $selected['minutes-per-day'] . ' ' . ___('tariffs.minutesPerDay') !!}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>{!! ___('tariffs.result.lessonsPerWeek') !!}</td>
                                            <td>
                                                <span id="countLessons">
                                                    {!! $selected['lessons-per-week'] . ' ' .
                                                        ___('tariffs.lessonsPerWeek', [
                                                            'count' => numEnding($selected['lessons-per-week'], [
                                                                ___('tariffs.lessonEndings.one'),
                                                                ___('tariffs.lessonEndings.two'),
                                                                ___('tariffs.lessonEndings.five'),
                                                            ])
                                                        ])
                                                    !!}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>{!! ___('tariffs.result.period') !!}</td>
                                            <td>
                                                <span id="period">
                                                    {!! $tariffs[$selected['tariff-name']]['period'] . ' '
                                                        . numEnding($tariffs[$selected['tariff-name']]['period'], [
                                                            ___('tariffs.periodEndings.one'),
                                                            ___('tariffs.periodEndings.two'),
                                                            ___('tariffs.periodEndings.five'),
                                                        ])
                                                    !!}
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="subscribe-wrap">
                            <button type="submit" class="btn subscribe">{!! ___('tariffs.subscribe') !!}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        window.pricePerMinute = '{{ $pricePerMinute }}';
    </script>
@stop
