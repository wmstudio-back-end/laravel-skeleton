@extends('laravel.layout')

@section('title', 'Sockets test')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mb-2">
                    <div class="card-header">Users list</div>
                    <div class="card-body user-list">
                        @foreach ($users as $user)
                            <button data-userid="{{ $user->id }}" class="mb-1 btn btn-block btn-action btn-secondary" type="button">{{ $user->getFullName() }}</button>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <button id="video-close" type="button" class="btn btn-block btn-action btn-danger" disabled="disabled">Отключиться</button>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Test sockets {!! ($guest = Session::get('guest')) ? "<i>$guest</i>" : '' !!}</div>
                    <div class="card-body">
                        <div class="video-container">
                            <video id="remoteVideo" autoplay="true" playsinline="playsinline"></video>
                            <video id="localVideo" autoplay="true" muted="muted" playsinline="playsinline"></video>
                        </div>
                    </div>
                </div>
            </div>
            <div id="log" class="col-md-8 offset-md-4"></div>
        </div>
    </div>
@endsection