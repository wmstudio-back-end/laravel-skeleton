<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <link href="{!! mix('css/laravel.css') !!}" rel="stylesheet" type="text/css">
    <style>
        button.ready {
            position: absolute;
            top: 11px;
            right: 15px;
        }
        .btn.btn-sm.btn-xsm {
            padding: 0.1rem 0.3rem;
            font-size: 0.8rem;
            line-height: 1.4;
            border-radius: 0.5rem;
        }
        .btn.btn-sm.btn-xsm:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }
        .list-group-item > a > span {
            padding-top: 2px;
        }
    </style>
</head>
<body>
    <div class="position-ref full-height">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="navbar-brand">{{ ($user = Auth::user()) ? $user->getFullName() : 'Меню' }}</div>
                <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                ><span class="navbar-toggler-icon"></span></button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('videoChat') }}">Демо видео чат</a>
                        </li>
                        @auth
                            @if(Auth::user()->hasAdminPermissions())
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.dashboard') }}">Панель управления</a>
                                </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}">Выход</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a
                                    class="nav-link dropdown-toggle"
                                    href="#"
                                    id="login-register"
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >Авторизация</a>
                                <div class="dropdown-menu" aria-labelledby="login-register">
                                    <a class="dropdown-item" href="{{ route('login') }}">Вход в систему</a>
                                    <a class="dropdown-item" href="{{ route('register') }}">Регистрация</a>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
            </nav>

            <div class="row">
                <div class="col-md-6 mb-2">
                    <div class="card">
                        <div class="card-header">
                            <center>Верстка</center>
                            <button type="button" class="btn btn-outline-info btn-sm btn-xsm ready" data-state="hide" style="display: none;">Скрыть готовые</button>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach($makeupLinks as $link)
                                <li class="list-group-item {{ $link['ready'] ? 'ready' : '' }}">
                                    <a href="{{ $link['url'] }}" target="_blank">
                                        {{ $link['name'] }}
                                        <span class="btn-{!! ($lastDate == $link['date']) ? 'success' : 'primary' !!}">
                                            {{ $link['date']->format("(d.m.Y H:i:s)") }}
                                        </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 mb-2">
                    <div class="card">
                        <div class="card-header">
                            <center>Результат</center>
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach($links as $name => $link)
                                <li class="list-group-item">
                                    <a href="{{ $link }}" target="_blank">{{ $name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! metrikaCounter() !!}
    <script src="{!! mix('js/laravel.js') !!}" type="text/javascript"></script>
</body>
</html>
