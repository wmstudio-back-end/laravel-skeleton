@php $user = \Auth::user(); @endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @php
        $title = null;
        if (url()->current() !== url('/')) {
            $title = getMenu('dbMenu')->render([
                'onlyText' => true,
                'reverse' => true,
                'separator' => '|',
            ]);
        }
        if (trim($__env->yieldContent('title'))) {
            $title = trim($__env->yieldContent('title')) . ($title ? ' | ' . $title : '');
        }
        $mainTitle = setting('main_title') ?: config('app.name', 'Laravel');
    @endphp
    @if ($title)
        <title>{{ $title }} | {{ $mainTitle }}</title>
    @else
        <title>{{ $mainTitle }}</title>
    @endif

    <style>
        .uidStatus1{
            margin: 10px;
            background-color: #0BB9BB;
            border: 1px solid #CCCCCC;
        }
        .uidStatus0{
            margin: 10px;
            background-color: red;
            border: 1px solid #CCCCCC;
        }
        table{
            border: 1px solid #CCCCCC;
            width: 800px;
            margin: 0 auto;
        }
        table tr td{
            border: 1px solid #CCCCCC;
        }
        video {2
            border: 1px solid #000;
        }
    </style>
    @yield('styles')
</head>
<body>
<video id="local_1" autoplay></video>
<video id="l2" autoplay></video>

<br>
    <video style="border: 1px solid #ffe921;width: 50px" id="localVideo" autoplay muted></video>
    <video style="border: 1px solid #4e2af9;width: 150px" id="remoteVideo" autoplay></video>
    <video style="border: 1px solid #000;width: 150px"  id="remoteVideo2" autoplay></video>
<br>
    <button id="cancelCall" style="display: none;width: 100px;height: 100px">ОТМЕНИТЬ ЗВОНОК</button>

    <button id="IncomingCall_on"  style="display: none;width: 100px;height: 100px">ВЗЯТЬ ТРУБКУ</button>
    <button id="IncomingCall_Off"  style="display: none;width: 100px;height: 100px">ПОЛОЖИТЬ ТРУБКУ</button>


    <table>
        <tr>
            <td  id="connection_num" style="width: 20%;vertical-align: top"></td>
            <td>
                <div>
                    <div>ActiveDialog ID <span id="activeDialog" style="font-size: 22px;color: #0000cc"></span></div>


                    <div id="messageHistory" style="height: 400px"></div>
                </div>

                <div>
                    <textarea rows="5" style="width: 100%" id="message"></textarea><br/>
                    <button onclick="window.FC.sendMessage();">Send</button>
                    <button id="testBtn">Mute/UnMute</button>
                    <button id="stopCall">Положить трубку</button>
                </div>
            </td>
        </tr>

    </table>

    <script id="lang-and-status" type="text/javascript">
        window.currLang = '{{ App::getLocale() }}';
        window.showRating = {{ config('view.show-rating') ? 'true' : 'false' }};
        window.token = '{{ csrf_token() }}';
        window.favoritesAction = '{{ route('favorites') }}';
        window.favorites = {!! json_encode($user->getFavorites()) !!};
        window.ratingAction = '{{ route('rating') }}';
        @if($user->getStatus() !== null)
            window.userStatus = '{{ $user->getStatus() }}';
            window.userWatchList = {!! json_encode($user->getWatchList()) !!};
            @if ($user->getStatus() === 's')
                window.availableTime = {!! $user->getAvailableTime() !!};
            @endif
        @endif
    </script>
    <script src="{{ mix('js/app.js') }}">

    </script>

</body>
</html>
