@php
    /* @var $test Modules\Tests\Entities\Test */
    /* @var $question Modules\Tests\Entities\TestQuestion */
    $promoDiscount = 0;
@endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!!___('test-lang-level.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!!___('test-lang-level.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="wrapper">
                <div class="profile-text">
                    <p>{!!___('test-lang-level.change-test') !!}</p>
                </div>
                <div class="wrapTabProf">
                    @foreach($tests as $test)
                        <div id="test-{{ $test->id }}" data-test-id="{{ $test->id }}" class="btn testTabs{{
                                $tests->first()->id === $test->id ? ' active' : '' }}">
                            {{ transDef($test->name, Modules\Tests\Entities\Test::TRANS_GROUP . '.' . $test->alias) }}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div id="lang-tests" class="testList-wrap" data-action="{{ route('tests') }}">
                @php
                    $test = $tests->first();
                    $first = $compiled[$test->id];
                    $result = isset($first['result']) ? $first['result'] : false;
                    if ($result) {
                        $percents = $first['percents'];
                    } else {
                        $percents = round(($first['answeredQuestions'] / $first['countQuestions']) * 100);
                    }
                    $question = reset($first['questions']);
                @endphp
                <div class="testComplete question-not-complete"@if($result) style="opacity: 0"@endif>
                    <button
                        id="send-tests-results"
                        data-action="{{ route('tests.save-results') }}"
                        type="button"
                        class="btn completeBtn send-tests-results"
                    >{!!___('test-lang-level.suspend.stop') !!}</button>
                    <div class="test-issue send-tests-results">?
                        <div class="test-answer">
                            <p>{!!___('test-lang-level.suspend.description') !!}</p>
                            <span class="test-answer-ic"></span>
                        </div>
                    </div>
                </div>
                <div class="testPercent">
                    @php
                        $resultType = $result && isset($first['resultType']) ? $first['resultType'] : false;
                        if ($resultType !== false) {
                            switch ($resultType) {
                                case 0:
                                    $resultType = 'result-good';
                                    break;
                                case 1:
                                    $resultType = 'result-normal';
                                    break;
                                case 2:
                                    $resultType = 'result-bad';
                                    break;
                            }
                        }
                    @endphp
                    <span id="test-percents" style="width: {{ $percents }}%"{{ $resultType !== false
                        ? ' class=' . $resultType : '' }}></span>
                </div>
                <div class="testIssue-wrap">
                    <div class="testIssueTitle">
                        <p class="question-not-complete"@if($result) style="opacity: 0"@endif>{!!___('test-lang-level.change-answer') !!}</p>
                        <h3>
                            <span class="testIssue-title question-not-complete"@if($result) style="opacity: 0"@endif>
                                <span id="question-number">{{ $first['answeredQuestions'] + 1 }}</span>.
                            </span>
                            <span id="question-text">{!! $result ?: $question['question'] !!}</span>
                        </h3>
                    </div>
                    <div id="question-answers">
                        @if(!$result)
                            @foreach($question['answers'] as $key => $answer)
                                <div class="testChoice-wrap">
                                    <div class="dialectLang-list">
                                        <input
                                            type="radio"
                                            id="question-answer-{{$key}}"
                                            name="question-answer"
                                            value="{{ $key }}"
                                        >
                                        <label
                                            for="question-answer-{{$key}}"
                                            class="dialectLangRadio"
                                        >{{ $answer }}</label>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="testBtn-wrap question-not-complete"@if($result) style="opacity: 0"@endif>
                    <button id="question-skip" type="button" class="btn testBtn">
                        {!!___('test-lang-level.skip') !!}
                    </button>
                    <button id="question-next" type="button" class="btn testBtn testBtn-next" disabled="disabled">
                        {!!___('test-lang-level.next') !!}
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script id="question-data" type="text/javascript">
        window.langTests = {
            @foreach($compiled as $testId => $testValue)
                @php
                    $result = 'null';
                    if (
                        isset($testValue['result'])
                        && isset($testValue['resultType'])
                        && isset($testValue['percents'])
                    ) {
                        $result = json_encode([
                            'percents' => $testValue['percents'],
                            'resultType' => $testValue['resultType'],
                            'text' => $testValue['result'],
                        ]);
                    }
                @endphp
                '{{ $testId }}': {
                    answeredQuestions: {{ $testValue['answeredQuestions'] }},
                    countQuestions: {{ $testValue['countQuestions'] }},
                    result: {!! $result !!},
                    questions: [
                        @foreach($testValue['questions'] as $question)
                            {
                                id: {{ $question['id'] }},
                                question: '{{ $question['question'] }}',
                                answers: [
                                    @foreach($question['answers'] as $key => $val)
                                        {
                                            id: {{ $key }},
                                            value: '{{ $val }}',
                                        },
                                    @endforeach
                                ],
                            },
                        @endforeach
                    ]
                },
            @endforeach
        };
    </script>
@stop
