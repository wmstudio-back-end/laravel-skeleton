@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! ___('tariffs.payment.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            <p>{!! ___('tariffs.payment.success') !!}</p>
        </div>
    </div>

    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="questionsSheet">
                <div class="wrapH5">
                    @php
                        try {
                            $tariffName = json_decode(setting($info['tariff']['name']), true)['name'];
                        } catch (\Exception $e) {
                            $tariffName = $info['period'];
                        }
                        $tariffName = transDef($info['tariff']['name'], "dictionary.$tariffName");
                    @endphp
                    <h5>{!! ___('tariffs.tariff') . ' "' . $tariffName . '"' !!}</h5>
                </div>
                <div class="table-wrap">
                    <table border="1" cellpadding="5" class="table">
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{!! ___('tariffs.result.method.header') !!}</td>
                                <td>{!! $info['group-discount'] ? ___('tariffs.result.method.group') : ___('tariffs.result.method.personal') !!}</td>
                                <td rowspan="4">
                                    @if($info['tariff']['discount'])
                                        <p>{!! ___('tariffs.discount') . ': ' . $info['tariff']['discount'] . '%' !!}</p>
                                    @endif
                                    @if($info['promo-discount'])
                                        <p>{!! ___('tariffs.discountPromo') . ': ' . $info['promo-discount'] . '%' !!}</p>
                                    @endif
                                    @if($info['group-discount'])
                                        <p>{!! ___('tariffs.discountGroup') . ': ' . $info['group-discount'] . '%' !!}</p>
                                    @endif
                                    @php $currency = $info['currency']; @endphp
                                    <p class="table-result">{!! ___('tariffs.result.total') . ': '
                                        . number_format($info['tariff']['summary']['price'], 2, ',', ' ')
                                        . (mb_strlen($currency) === 1 ? $currency : ' ' . $currency) !!}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>{!! ___('tariffs.result.minutesPerDay') !!}</td>
                                <td>{!! $info['minutes-per-day'] . ' ' . ___('tariffs.minutesPerDay') !!}</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>{!! ___('tariffs.result.lessonsPerWeek') !!}</td>
                                <td>{!! $info['lessons-per-week'] . ' ' .
                                    ___('tariffs.lessonsPerWeek', [
                                        'count' => numEnding($info['lessons-per-week'], [
                                            ___('tariffs.lessonEndings.one'),
                                            ___('tariffs.lessonEndings.two'),
                                            ___('tariffs.lessonEndings.five'),
                                        ])
                                    ])
                                !!}</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>{!! ___('tariffs.result.period') !!}</td>
                                <td>{!! $info['tariff']['period'] . ' '
                                    . numEnding($info['tariff']['period'], [
                                        ___('tariffs.periodEndings.one'),
                                        ___('tariffs.periodEndings.two'),
                                        ___('tariffs.periodEndings.five'),
                                    ])
                                !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <center class="payment"><p><small>{!! ___('global.redirect.link', [
                    'page' => '<a href="' . route('payments') . '">' . ___('tariffs.tariffs') . '</a>',
                ]) !!}</small></p></center>
            </div>
        </div>
    </div>
@endsection