@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('payments.teacher.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('payments.teacher.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="balanceText">
                <div class="balanceText-wrap">
                    <p>{!! ___('payments.teacher.balance') !!}</p>
                    @php $balanceStr = number_format($balance, fmod($balance, 1) ? 2 : 0, ',', ' ') @endphp
                    <span class="balance-number mx-2">{{ $balanceStr . ___('dictionary.currency.rub') }}</span>
                </div>
                @if($balance >= setting('min_amount'))
                    <a
                        href="#"
                        target="_blank"
                        class="btn balanceBtn"
                        title="{{ ___('payments.teacher.cash-out-unavailable') }}"
                    >{!! ___('payments.teacher.cash-out') !!}</a>
                @endif
            </div>

        </div>
    </div>

    <div class="wrapTariffs my-5">
        <div class="row">
            <div class="balanceTable">
                <h3>{!! ___('payments.teacher.table.title') !!}</h3>
                <table border="1" cellspacing="1">
                    <thead>
                        <tr>
                            <th>{!! ___('payments.teacher.table.number') !!}</th>
                            <th>{!! ___('payments.teacher.table.date') !!}</th>
                            <th>{!! ___('payments.teacher.table.sum') !!}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $key => $payment)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $payment->date->format('d.m.Y H:i:s') }}</td>
                            @php $cash = number_format($payment->cash, fmod($payment->cash, 1) ? 2 : 0, ',', ' ') @endphp
                            <td><span>{{ $cash . ___('dictionary.currency.rub') }}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection