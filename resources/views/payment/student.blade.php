@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('payments.student.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('payments.student.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="balanceText">
                <div class="balanceText-wrap">
                    @if($minutes && $periodEnd)
                        <p>{!! ___('payments.student.paid-time.less') !!}</p>
                        <span class="balance-number mx-2">{{ $minutes }}</span>
                        <p>{{ numEnding($minutes, [
                            ___('payments.student.minutes.default.one'),
                            ___('payments.student.minutes.default.two'),
                            ___('payments.student.minutes.default.five'),
                        ]) . ' ' . ___('payments.student.paid-time.for') }}</p>
                        <span class="balance-number mx-2">{{ $periodEnd->format('d.m.Y H:i:s') }}</span>
                    @else
                        <p>{!! ___('payments.student.no-paid-time') !!}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="wrapTariffs my-5">
        <div class="row">
            <div class="balanceTable">
                @if($payments->count())
                    <h3>{!! ___('payments.student.table.title') !!}</h3>
                    <table border="1" cellspacing="1">
                        <thead>
                            <tr>
                                <th>{!! ___('payments.student.table.number') !!}</th>
                                <th>{!! ___('payments.student.table.date') !!}</th>
                                <th>{!! ___('payments.student.table.sum') !!}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $now = new DateTime() @endphp
                        @foreach($payments as $key => $payment)
                            <tr class="info" title="{{ ___('payments.student.table.paid-date') }} {{ $payment->updated_at }}">
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $payment->from->format('d.m.Y H:i:s') }} - {{ $payment->to->format('d.m.Y H:i:s') }}</td>
                                <td>
                                    @php
                                        $currency = $payment->currency;
                                        $price = number_format($payment->tariff_info['tariff']['summary']['price'], 2, ',', ' ');
                                    @endphp
                                    @if(mb_strlen($currency) === 1)
                                        <span>{{ $price . $currency }}</span>
                                    @else
                                        <span>{{ $price }}</span> {{ $currency }}
                                    @endif
                                </td>
                            </tr>
                            <tr class="details">
                                <td colspan="3">
                                    <table border="1" >
                                        <thead>
                                            <tr>
                                                <th>{!! ___('payments.student.table.sub-table.number') !!}</th>
                                                <th>{!! ___('payments.student.table.sub-table.date') !!}</th>
                                                <th>{!! ___('payments.student.table.sub-table.time-left') !!}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($payment->periods as $key => $period)
                                            <tr>
                                                <td>
                                                    @if ($period->from < $now && $period->to > $now)
                                                        <span>{{ $key + 1 }}</span>
                                                    @else
                                                        {{ $key + 1 }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($period->to > $now)<span>@endif
                                                    {{ $period->from->format('d.m.Y H:i:s') }} - {{ $period->to->format('d.m.Y H:i:s') }}
                                                    @if ($period->to > $now)</span>@endif
                                                </td>
                                                <td>
                                                    @php
                                                        $sMinutes = ceil($period->available_time / 60);
                                                        $less = ($period->available_time / 60) < $sMinutes;
                                                        $sStart = $less ? ___('payments.student.less') . ' ' : '';
                                                        $sEnd = $less
                                                            ? numEnding($sMinutes, [
                                                                ___('payments.student.minutes.default.one'),
                                                                ___('payments.student.minutes.default.two'),
                                                                ___('payments.student.minutes.default.five'),
                                                            ])
                                                            : numEnding($sMinutes, [
                                                                ___('payments.student.minutes.less.one'),
                                                                ___('payments.student.minutes.less.two'),
                                                                ___('payments.student.minutes.less.five'),
                                                            ]);
                                                    @endphp
                                                    @if ($period->from < $now && $period->to > $now)
                                                        <span>{{ $sStart . $sMinutes . ' ' . $sEnd }}</span>
                                                    @else
                                                        {{ $sStart }} <span>{{ $sMinutes }}</span> {{ $sEnd }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                @endif
            </div>
        </div>
    </div>
@endsection