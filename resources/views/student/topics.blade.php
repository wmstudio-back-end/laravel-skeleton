@php use Modules\Topics\Entities\TopicsGroup; @endphp
@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('topics.find.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('topics.find.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="issue">
                <p>{!! ___('topics.find.select') !!}:</p>
            </div>
            <div class="answer">
                @foreach($topicsGroups as $group)
                    @php /* @var $group TopicsGroup */ @endphp
                    <div class="helpStudent">
                        <input type="radio" name="topic-group" id="topic-group-{{ $group->id }}" value="{{ $group->id }}">
                        <label class="btn press" for="topic-group-{{ $group->id }}">{{
                            transDef($group->name, TopicsGroup::TRANS_GROUP . '.' . TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias)
                        }}</label>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row topicForm">
        <div id="topic-tags"></div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div
                id="card-container"
                class="many-card"
                data-action="{{ route('topics.find') }}"
                data-check-action="{{ route('topics.check') }}"
            ></div>
            <button type="button" id="find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    @include('modals.topic-content')
    @include('modals.topic')
@stop

@section('scripts')
    @parent
    <script id="topics-groups-ang-tags" type="text/javascript">
        window.topicsGroups = {!! json_encode($topicsTags) !!};
    </script>
    <script id="topic-card" type="text/x-custom-template">
        <div class="card topicCard">
            <div data-id="topic-card-image" class="topicImg"></div>
            <div class="wrap-complexity">
                <div class="complexity">{!! ___('topics.find.difficulty') !!}
                    <select data-id="difficulty" class="rating" data-theme="fontawesome-stars">
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
            <div data-id="topic-card-name" class="mrB25"></div>
            <div class="card-status-cont">
                <button type="button" data-id="topic-card-button" class="btn discuss">{!! ___('topics.find.discuss') !!}</button>
            </div>
        </div>
    </script>
    @include('partials.user.card')
@stop
