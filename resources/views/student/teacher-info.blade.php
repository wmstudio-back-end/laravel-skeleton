@php
    use Modules\Users\Entities\UserAddSettings;
    /* @var $user \Modules\Users\Entities\User */
    $selfUser = Auth::user();
@endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('user-info.teacher.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            <p>{!! ___('user-info.description') !!}</p>
        </div>
    </div>

    <div class="findTeacher">
        <input id="user-id" type="hidden" value="{{ $user->id }}">
        <div class="row">
            <div class="many-card cardInfo">
                <a href="{{ route('home') }}" class="return">
                    <span class="return-ic"></span>
                    {!! ___('dictionary.back') !!}
                </a>
                <div class="card cardBooking-info">
                    <div class="card-face bookingCard-face">
                        <div class="card-face-img">
                            <img src="{{ $user->getAvatar() }}">
                            <span data-line-status-id="{{ $user->id }}" class="{{ isUserOnline($user->id) ? 'online' : 'offline' }}"></span>
                        </div>
                        <div class="minInfo">
                            @if(config('view.show-rating'))
                                <div class="card-rating">
                                    <select class="rating" data-theme="fontawesome-stars-o" data-readonly="true" data-init="{{ $user->getRating() }}">
                                        <option value="">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            @endif
                            <p class="name">{{ $user->getFullName() }}</p>
                            <div class="city">
                                <p>{{  Modules\Translations\Constants\Languages::getCountry($user->addon('country')) }}</p>
                            </div>
                            @php $dialects = UserAddSettings::where('control_type', 'dialect')
                                ->first()->transValue($user->addon('tc_dialect')); @endphp
                            @if(count($dialects))
                                <div class="dialect">
                                    {!! ___('user-info.dialect') !!}:
                                    <span>{{ implode(', ', $dialects) }}</span>
                                </div>
                            @endif
                            <div class="showAll notify">
                                <input
                                        id="update-watch-list"
                                        type="checkbox"
                                        data-action="{{ route('watch-list', ['userId' => $user->id]) }}"
                                        @if(Auth::user()->getWatchList($user->id))checked="checked"@endif
                                >
                                <label for="update-watch-list" class="showAll-btn">{!! ___('user-info.notify-when-online', [
                                    'person' => mb_strtolower(Modules\Users\Constants\Cambly::getStatus('t')),
                                ]) !!}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-status cardStudent-info">
                    <div class="like">
                        <span class="hear set-favorites{{ isset($selfUser->addon('favorites')[$user->id]) ? ' active' : ''
                        }}" data-user-id="{{ $user->id }}"></span>
                    </div>
                    {{--<div class="card-status-text">
                        @php
                            $scheduleStatus = 'will-busy';
                            $scheduleTime = 12;
                        @endphp
                        <p>{!! isset($scheduleTime)
                            ? ___('user-info.teacher.schedule' . $scheduleStatus, [
                                'minutes' => $scheduleTime,
                            ]) : ___('user-info.teacher.schedule' . $scheduleStatus)
                        !!}</p>
                    </div>--}}
                    <div class="card-status-cont">
                        <button class="phone linc-column call-to-user{{ isUserOnline($user->id) ? '' : ' hidden' }}" data-user-id="{{ $user->id }}">
                            <span></span>
                        </button>
                        <button class="postal linc-column msg-send-message" data-user-id="{{ $user->id }}">
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="infoStudentModal-tabs StTch-info">
        <input type="radio" name="iSM-tabs1" id="iSM-tabs1_1" checked="checked">
        <div class="iSM-tabs iSM-tabsInfo">
            <label for="iSM-tabs1_1" class="btn sort-btn">{!! ___('user-info.teacher.sections.info') !!}</label>
        </div>
        <input type="radio" name="iSM-tabs1" id="iSM-tabs2_1">
        @php $videoUrl = $user->addon('tc_video_url') @endphp
        @if($videoUrl)
            <div class="iSM-tabs iSM-tabsVideo">
                <label for="iSM-tabs2_1" class="btn sort-btn">{!! ___('user-info.teacher.sections.video') !!}</label>
            </div>
        @endif
        <input type="radio" name="iSM-tabs1" id="iSM-tabs3_1">
        <div class="iSM-tabs iSM-tabsSchedule">
            <label for="iSM-tabs3_1" class="btn sort-btn">{!! ___('user-info.teacher.sections.schedule') !!}</label>
        </div>
        <hr class="iSM-hr">
        <div class="iSM-tabsContent" id="iSM-tabs1-content_1">
            <div class="blockStudent-info">
                <div class="aboutStudent">
                    <div class="studentInfo-title">
                        <p>{!! ___('user-info.teacher.title') !!}</p>
                    </div>
                    <div class="studentInfo-info">
                        <div class="wrap-contentInfo">
                            @php $locates = getLocales(true); @endphp
                            @if (isset($locates[$user->addon('natural_lang')]))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.natural-lang') !!}</p>
                                    <div class="tag">
                                        <div class="tag__text">{{ $locates[$user->addon('natural_lang')] }}</div>
                                    </div>
                                </div>
                            @endif
                            <div class="contentInfo">
                                <p class="contentInfo-text">{!! ___('user-info.dialect-adighe') !!}</p>
                                @foreach($dialects as $val)
                                    <div class="tag">
                                        <div class="tag__text">{{ $val }}</div>
                                    </div>
                                @endforeach
                            </div>
                            @php
                                $speakLangs = $user->addon('speak_langs');
                                if (!is_array($speakLangs)) {
                                    $speakLangs = [];
                                }
                                if (isset($speakLangs[$user->addon('natural_lang')])) {
                                    unset($speakLangs[$user->addon('natural_lang')]);
                                }
                            @endphp
                            @if(count($speakLangs))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.speak-langs') !!}</p>
                                    @foreach($speakLangs as $key => $val)
                                        <div class="tag">
                                            <div class="tag__text">{{ $locates[$key] }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @php $teachStyle = UserAddSettings::where('control_type', 'tc_style')
                                    ->first()->transValue($user->addon('tc_style')); @endphp
                            @if (!is_array($teachStyle))
                                <div class="contentInfo">
                                    <p class="contentInfo-text">{!! ___('user-info.teacher.tc-style') !!}</p>
                                    <div class="tag">
                                        <div class="tag__text">{{ $teachStyle }}</div>
                                    </div>
                                </div>
                            @endif
                            @php $preferLevel = UserAddSettings::where('control_type', 'lang_level')
                                    ->first()->transValue($user->addon('tc_prefer_level')); @endphp
                            <div class="contentInfo">
                                <p class="contentInfo-text">{!! ___('user-info.teacher.prefer-level') !!}</p>
                                @foreach($preferLevel as $val)
                                    <div class="tag">
                                        <div class="tag__text">{{ $val }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="wrap-shortInfo">
                            <div class="shortInfo">
                                <p class="shortInfo-text">{!! ___('user-info.about-me') !!}</p>
                                <p>{!! str_replace("\n", "<br>", $user->addon('tc_about_me')) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="aboutStudent aboutStudent-training">
                    <div class="studentInfo-title">
                        <p>{!! ___('user-info.teacher.qualification.title') !!}</p>
                    </div>
                    <div class="studentInfo-info">
                        <div class="wrap-contentInfo">
                            <div class="wrap-shortInfo">
                                <div class="shortInfo">
                                    <p class="shortInfo-text">{!! ___('user-info.teacher.qualification.profession') !!}</p>
                                    <p>{{ $user->addon('tc_profession') }}</p>
                                </div>
                                @php $tcExp = UserAddSettings::where('control_type', 'tc_exp')
                                        ->first()->transValue($user->addon('tc_exp')); @endphp
                                @if (!is_array($tcExp))
                                    <div class="contentInfo">
                                        <p class="contentInfo-text">{!! ___('user-info.teacher.qualification.exp') !!}</p>
                                        <div class="tag">
                                            <div class="tag__text">{{ $tcExp }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="shortInfo">
                                    <p class="shortInfo-text">{!! ___('user-info.teacher.qualification.education') !!}</p>
                                    <p>{!! str_replace("\n", "<br>", $user->addon('tc_education')) !!}</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="aboutStudent aboutStudent-training">
                    <div class="studentInfo-title">
                        <p>{!! ___('user-info.learning') !!}</p>
                    </div>
                    <div class="studentInfo-info">
                        <div class="wrap-contentInfo">
                            @php $preferLessonType = UserAddSettings::where('control_type', 'lesson_type')
                                ->first()->transValue($user->addon('tc_lesson_type')); @endphp
                            <div class="contentInfo">
                                <p class="contentInfo-text">{!! ___('user-info.lesson-type') !!}</p>
                                @foreach($preferLessonType as $val)
                                    <div class="tag">
                                        <div class="tag__text">{{ $val }}</div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @if($videoUrl)
            <div class="iSM-tabsContent" id="iSM-tabs2-content_1">
                <div class="blockStudent-info">
                    <div class="teacher-flex">
                        @php $youtubeID = getYoutubeID($videoUrl); @endphp
                        @if ($youtubeID)
                            <div id="youtube-thumbnail" data-video-id="{{ $youtubeID }}" class="teacher-video">
                                <div>
                                    <img src="http://i3.ytimg.com/vi/{{ $youtubeID }}/hqdefault.jpg">
                                    <span id="play-video"></span>
                                </div>
                            </div>
                        @else
                            <iframe width="703" height="470" src="{{ $videoUrl }}"></iframe>
                        @endif
                    </div>
                    <div class="iSM-videoText">
                        <div class="videoTitle">
                            <p>{!! ___('user-info.teacher.video.title') !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="iSM-tabsContent" id="iSM-tabs3-content_1">
            <div class="blockStudent-info teacher-info">
                <div class="wrapPiker">
                    <div class="dataPiker-wrap">
                        <div class="dataPiker-title">
                            <p class="h7">{!! ___('user-info.teacher.reservation.date') !!}</p>
                        </div>
                        <div
                            id="reservationCalendar"
                            class="StTch-info_dataPiker"
                            @if(isset($firstTime))data-value="{{ $firstTime }}"@endif
                        ></div>
                    </div>
                    <div id="reserved-times" class="append-DataTimePiker">
                        <div class="mb-4">
                            <p class="h7">{!! ___('user-info.teacher.reservation.date', [
                                'date' => '<span id="selected-date"></span>',
                            ]) !!}</p>
                        </div>
                        <div id="time-element-container">
                            <div id="empty-time-element" class="bookingTime">
                                <div class="bookingFreeCancel">{!! ___('reservation.modal.empty-times') !!}</div>
                            </div>
                            <div
                                    id="time-element"
                                    class="bookingTime"
                                    data-busy-class="bookingColor"
                                    data-aborted-class="colorGr"
                            >
                                <div data-id="time" class="bookingTime-time">
                                    <span data-id="from"></span>{!! ___('reservation.modal.time-separator') !!}<span data-id="to"></span>
                                </div>
                                <div
                                    data-id="status"
                                    class="bookingFreeCancel"
                                    data-free="{{ ___('reservation.modal.status.free') }}"
                                    data-busy="{{ ___('reservation.modal.status.busy') }}"
                                ></div>
                                <button
                                    data-id="abort-btn"
                                    data-action="{{ route('user.reservation.actions') }}"
                                    data-reserve="{{ ___('reservation.modal.action.reserve') }}"
                                    data-abort="{{ ___('reservation.modal.action.abort') }}"
                                    class="btn resume-cancel"
                                    type="button"
                                >{!! ___('reservation.modal.action.abort') !!}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(config('view.show-rating'))
            <div class="teacherRating">
                <p>{!! ___('user-info.teacher.ratingTitle') !!}</p>
                @php
                    $rating = $user->addon('rating');
                    if (isset($rating['s'][$selfUser->id])) {
                        $rating = $rating['s'][$selfUser->id];
                    } else {
                        $rating = 0;
                    }
                @endphp
                <div class="rating">
                    <select class="rating" data-theme="fontawesome-stars-o" data-init="{{ $rating }}" data-hide-number data-user-id="{{ $user->id }}">
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('scripts')
    @parent
    <script id="reservations" type="text/javascript">
        window.reservationTimeLine = {!! json_encode($freeTime) !!};
    </script>
@endsection
