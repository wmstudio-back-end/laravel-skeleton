@php
    use Modules\Users\Entities\UserAddSettings;
    $user = Auth::user();
@endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <span class="subscription-fon"></span>
        <div class="row">
            <div class="subscription-column">
                <div class="wrap">
                    {!! ___('user-find.student.learn-adyghe') !!}
                    <a href="{{ route('tariffs') }}" class="btn subscribeBtn">{!! ___('tariffs.subscribe') !!}</a>
                </div>
            </div>
            <div class="level">
                <div
                    class="level-circle"
                    @if(empty($user->addon('avatar')))title="{{ ___('user-find.student.occupancy.description') }}"@endif
                >
                    <div><span>{{ $occupancyLevel }}</span></div>
                </div>
                <div class="level-fill">
                    {!! ___('user-find.student.occupancy.level') !!}
                    @if($occupancyLevel < 100)
                        <a href="{{ route('user.profile') }}" class="fillBtn">
                            {!! ___('user-find.student.occupancy.continue') !!}
                        </a>
                    @endif
                </div>
                <div class="level-lang">
                    {!! ___('user-find.student.check.title') !!}
                    <a href="{{ route('tests') }}" class="btn knowledgeBtn">{!! ___('user-find.student.check.check') !!}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="findTeacher-wrap">
                <p class="findTeacher-text">{!! ___('user-find.student.find') !!}</p>
                <div class="subscription-sort">
                    <div class="sort-wrap">
                        <div class="sort">
                            <input id="filter-all" type="radio" name="search-type" value="all" checked="checked">
                            <label for="filter-all" class="sort-btn btn">
                                {!! ___('user-find.find.all') !!}
                            </label>
                        </div>
                        <div class="sort">
                            <input id="filter-online" type="radio" name="search-type" value="online">
                            <label for="filter-online" class="sort-btn btn">
                                <span id="online-count">{{ $user->getOnlineCount() ?: '' }}</span>
                                {!! ___('user-find.find.online') !!}
                            </label>
                        </div>
                        @php $cntFavorites =  $user->getFavoritesCount() @endphp
                        @if ($cntFavorites)
                            <div class="sort">
                                <input id="filter-favorites" type="radio" name="search-type" value="favorites">
                                <label for="filter-favorites" class="sort-btn btn">
                                    <span id="favorites-count">{{ $cntFavorites }}</span>
                                    {!! ___('user-find.find.favorites') !!}
                                </label>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="language-search">
                    <input id="search" type="text" name="search" placeholder="{{ ___('user-find.student.search') }}">
                    <button id="clearSearchBtn" type="button" class="closeSearch hidden"></button>
                    <button id="searchBtn" type="button"></button>
                </div>
            </div>
        </div>
    </div>

    <div class="teachStyle-wrap">
        <div class="row">
            <div class="teachStyle">
                <div class="teachStyle-title">{!! ___('user-find.student.filter') !!}:</div>
                <div class="btn2">{!! ___('user-info.dialect') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'dialect')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="dialect[{{ $key }}]" id="dialect-{{ $key }}">
                                <label for="dialect-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2">{!! ___('user-find.lesson-type') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'lesson_type')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="lesson-type[{{ $key }}]" id="lesson-type-{{ $key }}">
                                <label for="lesson-type-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2 teachStyle-pdr">{!! ___('user-find.student.teach-style') !!}
                    <div class="btn2field">
                        @foreach(UserAddSettings::where('control_type', 'tc_style')->first()->transValue() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="teach-style[{{ $key }}]" id="teach-style-{{ $key }}">
                                <label for="teach-style-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2 teachStyle-pdr">{!! ___('user-find.speak-langs') !!}
                    <div class="btn2field">
                        @foreach(getLocales() as $key => $val)
                            <div class="dialectLang-list">
                                <input class="filter-param" type="checkbox" name="speak-langs[{{ $key }}]" id="speak-langs-{{ $key }}">
                                <label for="speak-langs-{{ $key }}" class="dialectLang">{{ $val }}</label>
                            </div>
                        @endforeach
                        <div class="btn2field-clean">
                            <button type="button" class="clean-linc clear-filter">
                                <span class="clean-ic"></span>
                                {!! ___('global.clear-selection') !!}
                            </button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
                <div class="btn2">{!! ___('user-find.student.datetime.title') !!}
                    <div class="btn2field datePicker-field">
                        <input id="dateInterval" type="text" readonly="readonly">
                        <input id="dates" type="hidden" class="filter-param-data">
                        <div class="clearDate">
                            <button id="setDate" type="button" class="ok">{!! ___('user-find.student.datetime.set') !!}</button>
                            <button id="clearDate" type="button" class="clear">{!! ___('user-find.student.datetime.clear') !!}</button>
                        </div>
                    </div>
                    <span class="quantity"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="listTeachers">
        <div class="row">
            <div class="search-line wrap-flex">
                <div>
                    <h3>{!! ___('user-find.student.teachers') !!}</h3>
                </div>
                <div class="search-line-text">
                    <p>{!! ___('user-find.student.found', [
                        'count' => '<span id="found-counter">0</span>',
                    ]) !!}</p>
                </div>
                <div class="search-line-ic">
                    <a class="btnTab sli1-ic"></a>
                    <a class="btnTab sli2-ic active"></a>
                </div>
            </div>
        </div>
        <div class="rowCol">
            <div id="card-container" class="many-card" data-action="{{ route('find.teachers') }}"></div>
            <button id="find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
@endsection

@section('scripts')
    @include('partials.user.card', ['userLink' => route('user.teacher', ['login' => 'user-id'])])
@stop
