@if(isset($seo['seo_title']))
    @section('title', $seo['seo_title'])
@endif
@if(isset($seo['seo_keywords']) || isset($seo['seo_title']))
    @section('seo')
        @if(isset($seo['seo_keywords']))
            <meta name="keywords" content="{{ $seo['seo_keywords'] }}">
        @endif
        @if(isset($seo['seo_description']))
            <meta name="description" content="{{ $seo['seo_description'] }}">
        @endif
    @endsection
@endif