@if (isset($menuMame) && !is_null($menuMame))
    @foreach(getMenu($menuMame) as $page)
        @if (!$page->accept() || $page->getClass() == 'hidden') @continue @endif
        @if ($page->isStatic())
            <li class="link-h">
                @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                <span class="static{{ $page->getClass() ? ' ' . $page->getClass() : '' }}">{!! $page->getLabel() !!}</span>
            </li>
        @else
            <li class="link-h{{ $page->isActive(true) ? ' active' : '' }}">
                <a
                    href="{{ $page->getHref() }}"
                    {{ ($page->getTarget() != "") ? ' target=' . $page->getTarget() : '' }}
                    @if($page->getClass())class="{{ $page->getClass() }}"@endif
                >
                    @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                    {!! $page->getLabel() !!}
                </a>
            </li>
        @endif
    @endforeach
@endif