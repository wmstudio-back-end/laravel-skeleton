@if (isset($menuMame) && !is_null($menuMame))
    @foreach(getMenu($menuMame) as $page)
        @if (!$page->accept() || $page->getClass() == 'hidden') @continue @endif
        @if ($page->isStatic())
            <a href="#" class="footer-link">
                @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                <span class="static first-upper{{ $page->getClass() ? ' ' . $page->getClass() : '' }}">{!! $page->getLabel() !!}</span>
            </a>
        @else
            <a
                href="{{ $page->getHref() }}"
                {{ ($page->getTarget() != "") ? ' target=' . $page->getTarget() : '' }}
                @if($page->getClass())class="{{ $page->getClass() }}"@endif
            >
                @if($page->get('icon') != null)<i class="{{ $page->get('icon') }}"></i>@endif
                <span class="first-upper">{!! $page->getLabel() !!}</span>
            </a>
        @endif
    @endforeach
@endif