@php $value = $user->addon($name); @endphp
<div  class="nameFill">
    <label for="{{ $name }}">{{ $label }}</label>
</div>
<div class="fillAcc">
    <textarea
        name="{{ $name }}"
        id="{{ $name }}"
        cols="30"
        rows="10"
        class="textarea{{ (isset($class) ? (' ' . $class) : '') . ($errors->has($name) ? ' is-invalid' : '') }}"
        placeholder="{{ isset($placeholder) ? $placeholder : $label }}"
        @if(!$errors->has($name) && isset($disabled) && $disabled)disabled="disabled"@endif
        @if(isset($required) && $required)required="required"@endif
    >{{ old($name) ?: $value }}</textarea>
    @if ($errors->has($name))
        <p class="message error">
            {{ str_replace(str_replace('_', ' ', $name), '"' . $label . '"', $errors->first($name)) }}
        </p>
    @endif
</div>