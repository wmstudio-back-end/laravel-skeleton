@php $value = $user->addon($name); @endphp
<div  class="nameFill vertical-mid">
    <label for="{{ $name }}">{{ $label }}</label>
</div>
<div class="fillAcc vertical-mid">
    <input
        type="{{ isset($type) ? $type : 'text' }}"
        id="{{ $name }}"
        name="{{ $name }}"
        class="fillAcc-link{{ (isset($class) ? (' ' . $class) : '') . ($errors->has($name) ? ' is-invalid' : '') }}"
        @if(!(isset($type) && $type == 'password'))placeholder="{{ isset($placeholder) ? $placeholder : $label }}"@endif
        @if(!$errors->has($name) && isset($disabled) && $disabled)disabled="disabled"@endif
        @if(isset($required) && $required)required="required"@endif
        value="{{ $value }}"
    >
    @if ($errors->has($name))
        <p class="message error">
            {{ str_replace(str_replace('_', ' ', $name), '"' . $label . '"', $errors->first($name)) }}
        </p>
    @endif
</div>