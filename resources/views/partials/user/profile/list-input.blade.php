@php
    if (!isset($list)) {
        $addon = $user->addon($name, true);
        $value = $addon->value ? json_decode($addon->value, true) : null;
        $addon = \Modules\Users\Entities\UserAddon::find($addon->addon_id);
        $list = \Modules\Users\Entities\UserAddSettings::where('control_type', $addon->control_type)->first()->transValue();
    } else {
        $value = $user->addon($name);
    }
    if (!$value && $type == 'checkbox') {
        $value = $list;
        foreach ($value as $key => $val) {
            $value[$key] = false;
        }
    }
@endphp
@if($type == 'checkbox')
    <input type="hidden" name="checkbox-{{ $name }}" value="{{ $name }}[]">
@elseif($type == 'radio')
    <input type="radio" class="hidden" name="{{ $name }}" value="" checked="checked">
@endif
@foreach($list as $key => $lang)
    <div class="dialectLang-list{{ isset($divClass) ? ' ' . $divClass : '' }}">
        <input
            type="{{ $type }}"
            @if(isset($class))class="{{ $class }}"@endif
            @if(isset($readonly) && $readonly)readonly="readonly"@endif
            id="{{ $name }}-{{ $key }}"
            name="{{ $name . ($type == 'checkbox' ? '[]' : '') }}"
            value="{{ $key }}"
            {{ $type == 'checkbox'
                ? old($name, null) !== null && is_array(old($name))
                    ? (in_array($key, old($name)) ? 'checked="checked"' : '')
                    : (is_array($value) && isset($value[$key]) && $value[$key])
                         ? 'checked="checked"'
                         : ''
                : old($name) == $key
                    ? 'checked="checked"'
                    : $value == $key
                        ? 'checked="checked"'
                        : ''
            }}
        >
        <label
                @if(!(isset($readonly) && $readonly))for="{{ $name }}-{{ $key }}"@endif
        class="{{ isset($labelClass) ? $labelClass : 'dialectLang' }}"
        >{{ $lang }}</label>
    </div>
@endforeach
@if ($errors->has($name))
    <p class="message error">
        {{ str_replace(str_replace('_', ' ', $name), '"' . $label . '"', $errors->first($name)) }}
    </p>
@endif