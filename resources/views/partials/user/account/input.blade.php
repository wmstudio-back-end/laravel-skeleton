@php
    if (isset($type) && $type == 'password') {
        $value = '';
    } else {
        if (old($name)) {
            $value = old($name);
        } else {
            if (!isset($value)) {
                $value = isset($addon) ? $addon : $name;
                $success = false;
                try {
                    $value = $user->addon($value);
                    $success = true;
                } catch (Exception $e) {
                    try {
                        $value = $user->$value;
                        $success = true;
                    } catch (Exception $e) {}
                }
                if (!$success) {
                    $value = '';
                }
            }
        }
    }
@endphp
<label for="{{ $name }}" class="accInfoLine-text">{{ $label }}</label>
<input
    type="{{ isset($type) ? $type : 'text' }}"
    id="{{ $name }}"
    name="{{ $name }}"
    class="accInfoLine-inp{{ (isset($class) ? (' ' . $class) : '') . ($errors->has($name) ? ' is-invalid' : '') }}"
    @if(!(isset($type) && $type == 'password'))placeholder="{{ isset($placeholder) ? $placeholder : $label }}"@endif
    @if(!$errors->has($name) && isset($disabled) && $disabled)disabled="disabled"@endif
    @if(isset($required) && $required)required="required"@endif
    value="{{ $value }}"
>
@if ($errors->has($name))
    <p class="message error">
        {{ str_replace(str_replace('_', ' ', $name), '"' . $label . '"', $errors->first($name)) }}
    </p>
@endif