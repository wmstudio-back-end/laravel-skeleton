@php
    $value = isset($addon) ? $addon : $name;
    $success = false;
    try {
        $value = $user->addon($value);
        $success = true;
    } catch (Exception $e) {
        try {
            $value = $user->$value;
            $success = true;
        } catch (Exception $e) {}
    }
    if (!$success) {
        $value = null;
    }
@endphp
<label for="{{ $name }}" class="accInfoLine-text">{{ $label }}</label>
<div id="{{ $name . '-error-target' }}" class="accInfoLine-inp{{ $errors->has($name) ? ' is-invalid' : '' }}">
    <select
            class="select2{{ (isset($class) ? ' ' . $class : '') . (isset($required) && $required ? ' hide-empty' : '') }}"
            id="{{ $name }}"
            name="{{ $name }}"
            data-align="left"
            data-error-target="{{ $name . '-error-target' }}"
            @if(isset($disabled) && $disabled)disabled="disabled"@endif
            @if(isset($required) && $required)required="required"@endif
    >
        <option value="">{{ isset($placeholder) ? $placeholder : $label }}</option>
        @if(isset($list))
            @foreach($list as $key => $val)
                <option
                        value="{{ $key }}"
                        {{ old($name) !== null
                            ? (old($name) == $key ? 'selected="selected"' : '')
                            : ($value == $key ? 'selected="selected"' : '')
                        }}
                >{!! $val !!}</option>
            @endforeach
        @else
            @if(old($name) !== null)
                <option>{{ old($name) }}</option>
            @elseif($value !== null)
                <option>{!! $value !!}</option>
            @else
                <option value="">{{ isset($placeholder) ? $placeholder : $label }}</option>
            @endif
        @endif
    </select>
</div>
@if ($errors->has($name))
    <p class="message error">
        {{ str_replace(str_replace('_', ' ', $name), '"' . $label . '"', $errors->first($name)) }}
    </p>
@endif