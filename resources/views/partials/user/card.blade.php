<script id="user-card" type="text/x-custom-template">
    <div class="card">
        @if(isset($userLink))
            <a data-id="user-link" href="{{ $userLink }}" {{--data-modal-id="infoStudentModal"--}} class="link-over"></a>
        @endif
        <div class="card-face">
            <div class="card-face-img">
                <img data-id="user-avatar">
                <span data-id="user-line-status" class="offline"></span>
            </div>
            <div class="minInfo">
                <div class="card-rating">
                    <select class="rating" data-theme="fontawesome-stars-o" data-readonly="true" data-hide-number>
                        <option value="">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div data-id="user-name" class="name"></div>
                <div class="city">
                    <p data-id="user-country"></p>
                </div>
                <div class="dialect">
                    {!! ___('user-info.dialect') !!}:
                    <span data-id="user-dialects"></span>
                </div>
                <div class="tag">
                    <div
                        id="chats-ending"
                        class="hidden"
                        data-one="{{ ___('user-info.chats-count.one') }}"
                        data-two="{{ ___('user-info.chats-count.two') }}"
                        data-five="{{ ___('user-info.chats-count.five') }}"
                    ></div>
                    <div class="tag__text" data-id="user-chats"></div>
                </div>
                <div class="tag hidden" data-id="user-diploma-element">
                    <div class="tag-info">
                        <p data-id="user-diploma-name">{!! ___('user-info.teacher.has-diploma.name', [
                            'name' => '{user-name}'
                        ]) !!}</p>
                        <a href="#" data-id="user-diploma-description" target="_blank">
                            {!! ___('user-info.teacher.has-diploma.diploma') !!}
                        </a>
                        <span class="empty"></span>
                        <span class="tag-arrow"></span>
                    </div>
                    <div class="tag__text">{!! ___('user-info.teacher.diploma') !!}</div>
                </div>
            </div>
        </div>
        <div class="card-text">
            <p data-id="user-about-me"></p>
        </div>
        <div class="card-status">
            <div class="like">
                <span class="hear set-favorites"></span>
            </div>
            <div class="card-status-text">
                <p
                    id="schedule-status"
                    data-free="{{ ___('user-info.teacher.schedule.free') }}"
                    data-busy="{{ ___('user-info.teacher.schedule.busy') }}"
                    data-will-busy="{{ ___('user-info.teacher.schedule.will-busy', [
                        'minutes' => '{minutes}'
                    ]) }}"
                ></p>
            </div>
            <div class="card-status-cont">
                @if(isset($user) && $user->getStatus() == 's')
                    <button class="phone linc-column call-to-user hidden">
                        <span></span>
                    </button>
                @endif
                <button class="postal linc-column msg-send-message">
                    <span></span>
                </button>
            </div>
        </div>
    </div>
</script>
