@if($currency)
    @php if (!isset($size) || empty($size)) $size = 16; @endphp
    <svg xmlns="http://www.w3.org/2000/svg" width="{{ $size }}" height="{{ $size }}" viewBox="0 0 16 16">
        <style>
            .cls-1 {
                font-size: 14px;
                font-family: Circe;
                font-weight: 700;
                transform: scale(0.55);
                fill: #dddddd;
                transition: 0.3s;
            }
            .cls-2 {
                fill: transparent;
                stroke: #dddddd;
                stroke-width: 1;
                stroke-miterlimit: 10;
                transition: 0.3s;
            }
        </style>
        <circle class="cls-2" cx="8" cy="8" r="7.5"></circle>
        <text text-anchor="middle" class="cls-1" x="90%" y="115%">{!! $currency !!}</text>
    </svg>
    @if (isset($name)){!! $name !!}@endif
@endif