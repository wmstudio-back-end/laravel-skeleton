@php
    $user = Auth::user();
    $locales = getLocales()
@endphp

@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! ___('account.account') !!}</h6>
        </div>
        <div class="booking-subtitle">
            @include('errors.message')
            {!! ___('account.description') !!}
        </div>
    </div>
    <form id="account-form" action="{{ route('user.account') }}" method="post">
        @csrf
        <div class="findTeacher">
            <div class="row">
                <div class="accForm">
                    <div class="columnAcc">
                        <p class="accForm-title">{!! ___('account.change-status') !!}</p>
                        @php
                            $canTeach = (bool)$user->addon('tc_can_teach');
                            $wasPermitted = (bool)$user->addon('tc_was_permitted');
                        @endphp
                        <div class="select2-multi">
                            <select name="status" class="select2 select-status" data-color="multi" style="display: none;">
                                <option
                                    value=""
                                    @if($user->getStatus() === null)selected="selected"@endif
                                >{{ ___('global.no') }}</option>
                                <option
                                    value="s"
                                    @if($user->getStatus() === 's')selected="selected"@endif
                                >{{ Modules\Users\Constants\Cambly::getStatus('s') }}</option>
                                @if($canTeach)
                                    <option
                                        value="t"
                                        @if($user->getStatus() === 't')selected="selected"@endif
                                    >{{ Modules\Users\Constants\Cambly::getStatus('t') }}</option>
                                @else
                                    <option
                                        value="q"
                                        data-class="{{ $wasPermitted ? 'rejected' : 'not-confirmed' }}"
                                        @if($user->getStatus() === 'q')selected="selected"@endif
                                    >{{ Modules\Users\Constants\Cambly::getStatus('t') }}</option>
                                @endif
                            </select>
                        </div>
                        @if(!$canTeach && !$wasPermitted)
                            <div class="accForm-info">
                                {!! ___('account.tc-not-permitted-info') !!}
                            </div>
                        @endif
                    </div>
                    <div class="columnAcc columnAcc-center">
                        <img alt="" title="" id="avatar" class="photo" src="{{ $user->getAvatar() }}">
                        <input type="hidden" id="photo" name="photo">
                        <input id="choose-img" type="file" class="hidden" accept="{!! $mimes !!}">
                        <button id="choose-img-btn" type="button" class="uploadPhoto btn">
                            {!! ___('account.upload-photo') !!}
                        </button>
                    </div>
                    <div class="columnAcc columnAcc-right">
                        {{--<input type="button" class="removeAcc" value="{{ ___('account.drop-account') }}">--}}
                    </div>
                </div>
            </div>
        </div>

        <div class="wrap-accInfo">
            <div class="row">
                <div class="accInfo">
                    <div class="text accInfo-title">
                        <p>{!! ___('account.acc.description') !!}</p>
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'name',
                            'label' => ___('account.acc.name'),
                            'class' => 'name-regex',
                            'required' => true,
                        ])
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'surname',
                            'label' => ___('account.acc.surname'),
                            'class' => 'name-regex',
                            'required' => true,
                        ])
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'email',
                            'label' => ___('account.acc.email.label'),
                            'placeholder' => ___('account.acc.email.placeholder'),
                            'required' => true,
                        ])
                    </div>
                    <div class="accInfoLine">
                        @php $phone = phoneFormat($user->addon('phone'), false, true) @endphp
                        <label for="phone-code" class="accInfoLine-text">{{ ___('account.acc.phone') }}</label>
                        <div class="accInfoLine-inp-box">
                            <input
                                id="phone-code"
                                type="text"
                                name="phone-code"
                                class="accInfoLine-inp phone-code{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                data-error-target="phone-number"
                                placeholder="+___"
                                value="{{ old('cam-phone-code') ?: $phone['cc'] }}"
                                required="required"
                            >
                            <input
                                id="phone-number"
                                type="text"
                                name="phone-number"
                                class="accInfoLine-inp phone-number{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                placeholder="(___) ___-__-__"
                                value="{{ old('cam-phone-number') ?: $phone['number'] }}"
                                required="required"
                            >
                        </div>
                    </div>
                </div>

                <div class="accInfo">
                    <div class="text accInfo-title">
                        <p>{!! ___('account.pass.description') !!}</p>
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'password',
                            'label' => ___('account.pass.old-pass'),
                            'type' => 'password',
                        ])
                        @if(old('password-was-updated'))
                            <p class="message success">
                                {{ old('password-was-updated') }}
                            </p>
                        @endif
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'new-password',
                            'label' => ___('account.pass.new-pass'),
                            'type' => 'password',
                        ])
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.input', [
                            'name' => 'new-password_confirmation',
                            'label' => ___('account.pass.confirm-pass'),
                            'type' => 'password',
                        ])
                    </div>
                </div>

                <div class="accInfo">
                    <div class="text accInfo-title">
                        <p>{!! ___('account.settings.description') !!}</p>
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.select', [
                            'name' => 'country',
                            'label' => ___('account.settings.country'),
                            'placeholder' => ___('global.select-no-value'),
                            'required' => true,
                            'list' => Modules\Translations\Constants\Languages::getCountries(),
                        ])
                    </div>
                    <div class="accInfoLine">
                        <label for="city" class="accInfoLine-text">
                            {!! ___('account.settings.city') !!}
                        </label>
                        <div id="city-err-target" class="accInfoLine-inp{{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <select
                                class="select2 custom-init"
                                id="city"
                                name="city"
                                required="required"
                                data-error-target="city-err-target"
                                data-action="{{ route('api.get-cities') }}"
                            >
                                @if(old('city') != null)
                                    <option>{{ old('city') }}</option>
                                @elseif($user->addon('city') != null)
                                    <option>{{ $user->addon('city') }}</option>
                                @else
                                    <option value="">{!! ___('global.select-no-value') !!}</option>
                                @endif
                            </select>
                        </div>
                        @if ($errors->has('city'))
                            <p class="message error">
                                {{ str_replace('city', '"' . $label . '"', $errors->first('city')) }}
                            </p>
                        @endif
                    </div>
                    <div class="accInfoLine">
                        <p class="accInfoLine-text">{{ ___('account.settings.birthday.title') }}</p>
                        @php
                            $now = new DateTime();
                            $birthday = $user->addon('birthday') ? new \DateTime($user->addon('birthday')) : null;
                            $birthdayDay   = $birthday ? (int)$birthday->format('d') : null;
                            $birthdayMonth = $birthday ? (int)$birthday->format('m') : null;
                            $birthdayYear  = $birthday ? (int)$birthday->format('Y') : null;
                            $countDays = $birthday
                                ? cal_days_in_month(CAL_GREGORIAN, $birthdayMonth, $birthdayYear)
                                : cal_days_in_month(CAL_GREGORIAN, $now->format('m'), $now->format('Y'));
                        @endphp
                        <div class="accInfoLine-date">
                            <div class="accInfoLine-selData">
                                <select id="birthday-year" name="birthday-year" class="btn select2 hide-empty" required="required">
                                    <option value="">{{ ___('account.settings.birthday.year') }}</option>
                                    @for ($i = ((int)$now->format('Y') - 8); $i >= ((int)$now->format('Y') - 90); $i--)
                                        <option value="{{ $i }}"{{ $i === $birthdayYear ? ' selected="selected"' : '' }}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="accInfoLine-selData">
                                <select id="birthday-month" name="birthday-month" class="btn select2 hide-empty" required="required">
                                    <option value="">{{ ___('account.settings.birthday.month') }}</option>
                                    @for ($i = 1; $i <= 12; $i++)
                                        @php $month = $i < 10 ? '0' . $i : (string)$i @endphp
                                        <option value="{{ $month }}"{{ $i === $birthdayMonth ? ' selected="selected"' : '' }}>
                                            {{ ___('dictionary.months.' . $month) }}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                            <div class="accInfoLine-selData">
                                <select id="birthday-day" name="birthday-day" class="btn select2 hide-empty"
                                        {{ $birthdayDay ? '' : 'disabled="disabled"' }} required="required">
                                    <option value="">{{ ___('account.settings.birthday.day') }}</option>
                                    @if ($birthdayDay)
                                        @for ($i = 1; $i <= $countDays; $i++)
                                            <option value="{{ $i }}"{{ $i === $birthdayDay ? ' selected="selected"' : '' }}>{{ $i }}</option>
                                        @endfor
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.select', [
                            'name' => 'natural_lang',
                            'label' => ___('account.settings.natural-lang'),
                            'placeholder' => ___('global.select-no-value'),
                            'required' => true,
                            'list' => $locales,
                        ])
                    </div>
                    <div class="accInfoLine">
                        <p class="accInfoLine-text">{!! ___('account.settings.speak-langs') !!}</p>
                        <div class="wrapAccInfo-listLang">
                            @php
                                $oldSpeakLangs = old('speak_langs');
                                $speakLangs = \Auth::user()->addon('speak_langs');
                            @endphp
                            @foreach($locales as $key => $name)
                                <div class="accInfo-listLang">
                                    <input
                                        type="checkbox"
                                        id="speak_langs-{{ $key }}"
                                        value="{{ $key }}"
                                        name="speak_langs[{{ $key }}]"
                                        {{ isset($oldSpeakLangs[$key]) ? 'checked="checked"'
                                            : (isset($speakLangs[$key])
                                                ? 'checked="checked"'
                                                : ''
                                            )
                                        }}
                                    >
                                    <label for="speak_langs-{{ $key }}" class="listLang">{{ $name }}</label>
                                </div>
                            @endforeach
                        </div>
                        @if ($errors->has('speak_langs'))
                            <p class="message error">
                                {{ str_replace('speak_langs', '"' . $label . '"', $errors->first('speak_langs')) }}
                            </p>
                        @endif
                    </div>
                    <div class="accInfoLine">
                        @include('partials.user.account.select', [
                            'name' => 'lang',
                            'label' => ___('account.settings.lang'),
                            'placeholder' => ___('global.select-no-value'),
                            'required' => true,
                            'list' => $locales,
                        ])
                    </div>
                </div>

                <div class="accInfo">
                    <div class="text accInfo-title">
                        <p>{!! ___('account.social-description') !!}</p>
                    </div>
                    @php $social = getAvailableSocial(); @endphp
                    @if(count($social))
                        @foreach($social as $socName)
                            <div
                                class="accInfo-boxWeb{{ $user->addon('social_' . $socName) ? ' active' : ''}} add-social"
                                data-url="{{ route('social', ['provider' => $socName]) }}"
                            >
                                <input
                                    type="checkbox"
                                    name="social-{{ $socName }}-delete"
                                    class="hidden"
                                    value="1"
                                >
                                <span class="wrapToils">
                                    <span class="{{ $socName }}"></span>
                                </span>
                                <p>{!! ___('global.socials.' . $socName) !!}</p>
                                <span class="boxWeb-close active"></span>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <button type="button" id="account-save" class="btn accSave">{!! ___('global.save') !!}</button>
        </div>
    </form>
@endsection

@section('modals')
    @parent
    <div id="avatar-modal" class="modal">
        <div class="cropper-container mb-2">
            <img id="loaded-img" data-available-formats="{{ $mimes }}">
        </div>
        <button id="change-img-btn" class="uploadPhoto btn pull-right">{!! ___('account.ready-photo') !!}</button>
    </div>
@stop
