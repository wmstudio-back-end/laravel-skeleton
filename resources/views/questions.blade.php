@php
    $transKey = \Modules\FAQ\Entities\FAQ::TRANS_GROUP;
    $auth = Auth::check();
    $layout = $auth ? 'main' : 'main-no-auth';
@endphp

@extends('layouts.' . $layout)

@section('content')
    <div class="subscription{{ !$auth ? ' no-auth' : '' }}">
        <div class="booking-title">
            @include('errors.header')
            <h6>{!! ___('questions.questions') !!}</h6>
        </div>
        <div class="booking-subtitle">
            @include('errors.message')
            {!! ___('questions.description') !!}
        </div>
    </div>
    {{--<div class="supportSearch">
        <div class="supportSearch-fon"></div>
        <div class="row">
            <div class="wrapper">
                <h3 class="marH3">{!! ___('questions.search.label') !!}</h3>
                <div class="language-search">
                    <input type="text" placeholder="{{ ___('questions.search.placeholder') }}" name="alias">
                    <button type="button" class="closeSearch"></button>
                    <button type="button"></button>
                </div>
            </div>
        </div>
    </div>--}}
    <div class="questionsSheet-wrap">
        <div class="row">
            <div class="questionsSheet">
                @php
                    $halfCount = count($groups) / 2;
                    $counter = 0;
                @endphp
                <div class="questionsSheet-column">
                    @while($counter < $halfCount)
                        <div class="questions-wrap">
                            @php
                                reset($groups);
                                $group = key($groups);
                            @endphp
                            <p class="text">{{ ___($transKey . '.groups.' . $group) }}</p>
                            @php $group = array_shift($groups); @endphp
                            <ul class="six-visible">
                                @foreach($group as $question)
                                    @php /* @var $question \Modules\FAQ\Entities\FAQ */ @endphp
                                    <li>
                                        <a href="{{ route('faq.question', ['group' => $question->group, 'alias' => $question->alias]) }}">
                                            {{ transDef($question->name, $transKey . '.' . $question->group . '.' . $question->alias . '.name') }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            @if($group->count() > 6)
                                <button type="button" class="questions-link show-hide-all">
                                    <span class="show-questions">{!! ___('questions.display.show') !!}</span>
                                    <span class="hide-questions">{!! ___('questions.display.hide') !!}</span>
                                    ({{ $group->count() }})
                                </button>
                            @endif
                        </div>
                        @php $counter++; @endphp
                    @endwhile
                </div>
                <div class="questionsSheet-column widthQc">
                    @while(count($groups))
                        <div class="questions-wrap">
                            @php
                                reset($groups);
                                $group = key($groups);
                            @endphp
                            <p class="text">{{ ___($transKey . '.groups.' . $group) }}</p>
                            @php $group = array_shift($groups); @endphp
                            <ul class="six-visible">
                                @foreach($group as $question)
                                    @php /* @var $question \Modules\FAQ\Entities\FAQ */ @endphp
                                        <li>
                                            <a href="{{ route('faq.question', ['group' => $question->group, 'alias' => $question->alias]) }}">
                                                {{ transDef($question->name, $transKey . '.' . $question->group . '.' . $question->alias . '.name') }}
                                            </a>
                                        </li>
                                @endforeach
                            </ul>
                            @if($group->count() > 6)
                                <button type="button" class="questions-link show-hide-all">
                                    <span class="show-questions">{!! ___('questions.display.show') !!}</span>
                                    <span class="hide-questions">{!! ___('questions.display.hide') !!}</span>
                                    ({{ $group->count() }})
                                </button>
                            @endif
                        </div>
                    @endwhile
                </div>
                <center>
                    <button class="btn askQuestion" data-modal-id="question-modal">{!! ___('questions.ask') !!}</button>
                </center>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @parent
    @include('modals.question')
@stop
