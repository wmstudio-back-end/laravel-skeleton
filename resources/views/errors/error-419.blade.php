@extends('layouts.errors')

@section('title', '419')

@section('content')
    <div class="title">
        <h1 class="title">419</h1>
        <p class="title">{!! ___('messages.csrf', ['href' => url()->current()]) !!}</p>
        <button type="button" class="title" onclick="window.history.back()">{{ ___('dictionary.back') }}</button>
        <a class="title" href="{{ route('home') }}">{{ ___('dictionary.home') }}</a>
    </div>
@endsection