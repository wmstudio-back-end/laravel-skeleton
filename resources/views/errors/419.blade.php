@if (Request::is(config('admin.defaults.prefix') . '/*') && Auth::user() && Auth::user()->hasAdminPermissions())
    @include('admin::errors.419')
@else
    @include('errors.error-419')
@endif