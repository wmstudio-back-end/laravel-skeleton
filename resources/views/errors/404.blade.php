@if (Request::is(config('admin.defaults.prefix') . '/*') && Auth::user() && Auth::user()->hasAdminPermissions())
    @include('admin::errors.404')
@else
    @include('errors.error-404')
@endif