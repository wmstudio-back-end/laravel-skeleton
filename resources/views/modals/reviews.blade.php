<!-- Оставить отзыв о портале -->
<div id="reviews-modal" class="bookModal auth">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>
        <div class="min-row ask-question mt-5">
            <div class="pt-5">
                <div class="wrapBookModal-title">
                    <h2 class="bookModal-title">{!! ___('global.footer.info.additionally-link.reviews.title') !!}</h2>
                    <p class="bookModal-subtitle">
                        {!! ___('global.footer.info.additionally-link.reviews.description') !!}
                    </p>
                </div>
                <form action="{{ route('send.review') }}" method="post">
                    @csrf
                    <div class="wrapper">
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <label class="nameIn mb-0">
                                    {!! ___('global.footer.info.additionally-link.reviews.rating') !!}
                                </label>
                                <div class="rating" id="rating-target">
                                    <select
                                        class="rating"
                                        name="rev-rating"
                                        data-hide-number
                                        data-theme="fontawesome-stars-o"
                                        data-error-target="rating-target"
                                        required="required"
                                    ><option value="">0</option>
                                        @for ($i = 1; $i <= \Modules\Reviews\Entities\Review::MAX_RATING; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="doorInp">
                                <label for="content" class="nameIn">
                                    {!! ___('global.footer.info.additionally-link.reviews.message') !!}
                                </label>
                                <textarea id="content" name="rev-content" required="required">{{ old('ask-question') }}</textarea>
                            </div>
                        </div>
                        <div class="register">
                            <button type="submit" class="btn">{!! ___('global.footer.info.additionally-link.reviews.title') !!}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Оставить отзыв о портале -->
