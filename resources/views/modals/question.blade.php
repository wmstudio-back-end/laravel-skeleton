<!-- Задать вопрос -->
<div id="question-modal" class="bookModal auth">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>
        <div class="min-row ask-question">
            <div class="wrapBookModal-title mb-4">
                <h2 class="bookModal-title">{!! ___('questions.ask') !!}</h2>
                <p class="bookModal-subtitle">{!! ___('questions.modal.description') !!}</p>
            </div>
            <form action="{{ route('question.ask') }}" method="post">
                @csrf
                <div class="wrapper">
                    @if (Auth::check())
                        <div class="doorInp-wrap mb-4">
                            <div class="doorInp">
                                <label for="question" class="nameIn">{!! ___('questions.modal.question') !!}</label>
                                <textarea
                                    id="question"
                                    name="ask-question"
                                    required="required"
                                >{{ old('ask-question') }}</textarea>
                            </div>
                        </div>
                    @else
                        <div class="doorInp-wrap mb-3">
                            <div class="doorInp">
                                <label for="name" class="nameIn">{!! ___('questions.modal.name') !!}</label>
                                <input id="name" name="ask-name" type="text" required="required" value="{{ old('ask-name') }}">
                            </div>
                            <div class="doorInp">
                                <label for="email" class="nameIn">{!! ___('questions.modal.email') !!}</label>
                                <input id="email" name="ask-email" type="email" required="required" value="{{ old('ask-email') }}">
                            </div>
                            <div class="doorInp">
                                <label for="phone" class="nameIn">{!! ___('questions.modal.phone') !!}</label>
                                <input id="phone" name="ask-phone" type="text" class="phone"
                                       placeholder="+_ (___) ___-__-__" required="required" value="{{ old('ask-phone') }}">
                            </div>
                            <div class="doorInp mb-0">
                                <label for="question" class="nameIn">{!! ___('questions.modal.question') !!}</label>
                                <textarea id="question" name="ask-question" required="required">{{ old('ask-question') }}</textarea>
                            </div>
                        </div>
                        <div class="consent mb-3">
                            {!! ___('questions.modal.personal-data') !!}
                        </div>
                    @endif
                    <div class="register">
                        <button type="submit" class="btn">{!! ___('questions.modal.send') !!}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Задать вопрос -->
