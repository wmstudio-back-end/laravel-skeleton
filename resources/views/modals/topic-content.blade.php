<!-- Topic content -->
<div id="topic-content-modal" class="bookModal">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>

        <div class="wrapBookModal-title">
            <h3 id="modal-topic-name"></h3>
        </div>

        <div class="smallRow">
            <div id="modal-topic-container" class="topic-container"></div>
        </div>
    </div>
</div>
<!-- Topic content -->