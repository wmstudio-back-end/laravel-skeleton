<!-- login/register -->
<div id="regModal" class="bookModal auth">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>
        <div class="min-row">
            <div class="wrapBookModal-title mb-3">
                <p class="bookModal-title">{!! ___('auth.register') !!}</p>
                <p class="bookModal-subtitle">{!! ___('auth.register-social') !!}</p>
            </div>
            @php $social = getAvailableSocial(); @endphp
            @if(count($social))
                <div class="wrap-wrapToils mb-4">
                    <div class="toilsLink">
                        @foreach($social as $socName)
                            <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                @csrf
                                <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                            </form>
                        @endforeach
                    </div>
                </div>
            @endif
            <form action="{{ route('register') }}" method="post">
                @csrf
                <div class="wrapper">
                    <div class="doorInpForm mb-4">
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="login">
                                    {!! ___('auth.login') !!}
                                </label></p>
                                <input
                                    id="login"
                                    type="text"
                                    placeholder="{!! ___('auth.login') !!}"
                                    name="cam-login"
                                    required="required"
                                    autofocus="autofocus"
                                    minlength="5"
                                >
                            </div>
                            <div class="doorInp">
                                <p><label for="email">
                                    {!! ___('auth.email') !!}
                                </label></p>
                                <input
                                    id="email"
                                    type="email"
                                    placeholder="{!! ___('auth.email') !!}"
                                    name="cam-email"
                                    required="required"
                                >
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="password">
                                    {!! ___('auth.password') !!}
                                </label></p>
                                <input
                                    id="password"
                                    type="password"
                                    placeholder="{!! ___('auth.password') !!}"
                                    name="cam-password"
                                    required="required"
                                    minlength="6"
                                >
                            </div>
                            <div class="doorInp">
                                <p><label for="confirm">
                                    {!! ___('auth.password-confirmation') !!}
                                </label></p>
                                <input
                                    id="confirm"
                                    type="password"
                                    placeholder="{!! ___('auth.password-confirmation') !!}"
                                    name="cam-password_confirmation"
                                    required="required"
                                >
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="referral">
                                    {!! ___('auth.ref-link') !!}
                                </label></p>
                                <input
                                    id="referral"
                                    type="text"
                                    placeholder="{!! ___('auth.ref-link') !!}"
                                    name="cam-ref-link"
                                >
                            </div>
                            <div class="doorInp">
                                <p><label for="how-know">
                                    {!! ___('auth.how-know-about-us') !!}
                                </label></p>
                                <div id="how-know-error-target" class="doorInpSel">
                                    <select
                                        id="how-know"
                                        name="cam-how-know"
                                        class="select2{{ $errors->has('cam-how-know') ?' is-invalid' : ''}}"
                                        data-error-target="how-know-error-target"
                                        tabindex="6"
                                    >
                                        <option value="" class="no-value">{!! ___('global.select-no-value') !!}</option>
                                        @php $variants = \Modules\Users\Entities\UserAddSettings::
                                        where('control_type', 'how_know_about_us')
                                            ->first()->transValue(); @endphp
                                        @foreach($variants as $key => $val)
                                            <option
                                                    value="{{ $key }}"
                                                    @if(old('cam-how-know') == $key)selected="selected"@endif
                                            >{{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <center>{!! NoCaptcha::display() !!}</center>
                        </div>
                    </div>
                    <div class="consent mb-4">
                        @if(!empty($personalData = setting('personal_data')))
                            {!! ___('auth.personal-data-processing-with-link', [
                                'link' => $personalData,
                            ]) !!}
                        @else
                            {!! ___('auth.personal-data-processing') !!}
                        @endif
                    </div>
                    <div class="register mb-4">
                        <button class="btn">{!! ___('auth.enter-register') !!}</button>
                    </div>
                    <div class="wrapDoor-text textModal">
                        <p class="bookModal-subtitle">{!! ___('auth.already-register') !!}</p>
                        <a href="{{ route('login') }}" class="no-decor" data-modal-id="doorModal">{!! ___('auth.enter') !!}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="doorModal" class="bookModal auth">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>
        <div class="min-row">
            <div class="wrapBookModal-title">
                <p class="bookModal-title">{!! ___('auth.enter') !!}</p>
            </div>
            <div class="wrapper">
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <div class="doorInpForm">
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="login-email">
                                    {!! ___('auth.email-login') !!}
                                </label></p>
                                <input
                                    id="login-email"
                                    type="text"
                                    placeholder="{!! ___('auth.email-login') !!}"
                                    name="cam-login"
                                    data-error-id="cam-login"
                                    required="required"
                                    autofocus="autofocus"
                                    minlength="5"
                                    tabindex="1"
                                >
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="login-password">
                                    {!! ___('auth.password') !!}
                                </label></p>
                                <input
                                    id="login-password"
                                    type="password"
                                    placeholder="{!! ___('auth.password') !!}"
                                    name="cam-password"
                                    data-error-id="cam-login"
                                    required="required"
                                    minlength="6"
                                    tabindex="2"
                                >
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <div class="dialectLang-list">
                                    <input type="checkbox" name="cam-remember" id="remember" checked="checked">
                                    <label for="remember" class="dialectLang" tabindex="3">{!! ___('auth.remember-me') !!}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="consentDoor">
                        <a href="{{ route('password.reset', ['token' => null]) }}" tabindex="4" data-modal-id="restoreModal">{!! ___('auth.forgot-password') !!}</a>
                    </div>
                    <div class="register doorReg mb-5">
                        <button class="btn" tabindex="5">{!! ___('auth.enter-login') !!}</button>
                    </div>
                </form>
                <div class="wrapDoor-text">
                    <p class="bookModal-subtitle">{!! ___('auth.enter-social') !!}</p>
                </div>
                @php $social = getAvailableSocial(); @endphp
                @if(count($social))
                    <div class="wrap-wrapToils">
                        <div class="toilsLink">
                            @foreach($social as $socName)
                                <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                    @csrf
                                    <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                                </form>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="wrapDoor-text textModal">
                    <p class="bookModal-subtitle">{!! ___('auth.need-register') !!}</p>
                    <a href="{{ route('register') }}" class="no-decor" data-modal-id="regModal">{!! ___('auth.register') !!}</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="restoreModal" class="bookModal auth">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>
        <div class="min-row">
            <div class="wrapBookModal-title">
                <p class="bookModal-title">{!! ___('auth.restore') !!}</p>
            </div>
            <div class="wrapper">
                <form action="{{ route('password.reset', ['token' => null]) }}" method="post">
                    @csrf
                    <div class="doorInpForm">
                        <div class="doorInp-wrap">
                            <div class="doorInp">
                                <p><label for="restore-email">
                                    {!! ___('auth.email') !!}
                                </label></p>
                                <input
                                    id="restore-email"
                                    type="text"
                                    placeholder="{!! ___('auth.email') !!}"
                                    name="cam-email"
                                    required="required"
                                    autofocus="autofocus"
                                    minlength="5"
                                >
                            </div>
                        </div>
                        <div class="doorInp-wrap">
                            <center>{!! NoCaptcha::display() !!}</center>
                        </div>
                    </div>
                    <div class="consentDoor">
                        <a href="{{ route('password.reset', ['token' => null]) }}" data-modal-id="doorModal">{!! ___('auth.already-registered') !!}</a>
                    </div>
                    <div class="register doorReg mb-5">
                        <button class="btn">{!! ___('auth.enter-restore') !!}</button>
                    </div>
                </form>
                <div class="wrapDoor-text">
                    <p class="bookModal-subtitle">{!! ___('auth.enter-social') !!}</p>
                </div>
                @php $social = getAvailableSocial(); @endphp
                @if(count($social))
                    <div class="wrap-wrapToils">
                        <div class="toilsLink">
                            @foreach($social as $socName)
                                <form action="{{ route('social', ['provider' => $socName]) }}" method="post" class="social">
                                    @csrf
                                    <button type="submit" class="wrapToils"><span class="{{ $socName }}"></span></button>
                                </form>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="wrapDoor-text textModal">
                    <p class="bookModal-subtitle">{!! ___('auth.need-register') !!}</p>
                    <a href="{{ route('register') }}" class="no-decor" data-modal-id="regModal">{!! ___('auth.register') !!}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- login/register -->
