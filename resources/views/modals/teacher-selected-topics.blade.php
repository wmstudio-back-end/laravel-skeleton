@php use Modules\Topics\Entities\TopicsGroup; @endphp
<!-- Teacher selected topics -->
<div id="topic-modal-teacher" class="bookModal">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>

        <div class="wrapBookModal-title">
            <h3>{!! ___('topics.modal.teacher.title') !!}</h3>
            <p class="bookModal-subtitle">{!! ___('topics.modal.teacher.description.selected', [
                'count' => '<span id="modal-topic-count"></span>',
                'ending' => '<span id="modal-topic-count-ending"></span>',
            ]) !!}</p>
            <p class="bookModal-subtitle">{!! ___('topics.modal.teacher.description.help') !!}</p>
        </div>

        <div class="findTeacher">
            <div class="row">
                <div class="issue">
                    <p>{!! ___('topics.modal.teacher.select-group') !!}:</p>
                </div>
                <div class="answer">
                    @foreach($topicsGroups as $group)
                        @php /* @var $group TopicsGroup */ @endphp
                        <div class="helpStudent">
                            <input type="radio" name="modal-topic-group" id="modal-topic-group-{{ $group->id }}" value="{{ $group->id }}">
                            <label class="btn press" for="modal-topic-group-{{ $group->id }}">{{
                                transDef($group->name, TopicsGroup::TRANS_GROUP . '.' . TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias)
                            }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
</div>
        <div class="row topicForm">
            <div>
                <div id="modal-topic-tags"></div>
            </div>
        </div>

        <div class="smallRow">
            <div
                id="modal-card-container"
                class="many-card cardScroll"
                data-action="{{ route('topics.find') }}"
                data-check-action="{{ route('topics.check') }}"
            ></div>
            <button id="modal-find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
</div>
<!-- Teacher selected topics -->
