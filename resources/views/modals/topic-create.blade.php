@php
use Modules\Topics\Entities\TopicsGroup;
use Modules\Topics\Entities\Topic;
@endphp
@if(isset($topicsGroups))
    <!-- создание темы  + -->
    <div id="topic-modal-create" class="bookModal">
        <div class="bookModal-left"></div>
        <div class="bookModal-wrap">
            <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>

            <div class="wrapBookModal-title">
                <p
                    class="bookModal-title"
                    id="topic-modal-create-title"
                    data-add="{{ ___('topics.create.title.new') }}"
                    data-edit="{{ ___('topics.create.title.edit') }}"
                ></p>
                <p
                    class="bookModal-subtitle"
                    id="topic-modal-create-subtitle"
                    data-add="{{ ___('topics.create.subtitle.new') }}"
                    data-edit="{{ ___('topics.create.subtitle.edit') }}"
                ></p>
            </div>

            <form
                id="topic-modal-create-form"
                method="post" enctype="multipart/form-data"
                data-add="{{ route('topics.my.edit', ['action' => 'add']) }}"
                data-edit="{{ route('topics.my.edit', ['action' => 'edit']) }}"
                data-validate="{{ route('topics.my.edit', ['action' => 'validate']) }}"
            >
                @csrf
                <div class="findTeacher">
                    <div class="row evenly">
                        <div class="col">
                            <div class="issue">
                                <p>{!! ___('topics.create.select-group') !!}:</p>
                            </div>
                            <div class="answer">
                                <div class="helpStudent planWeek">
                                    <select
                                        id="topic-create-group"
                                        class="select2 hide-empty"
                                        name="topic-group"
                                        data-error-target="topic-group-error-target"
                                        data-color="white"
                                        required="required"
                                    >
                                        <option value="">{!! ___('global.select-no-value') !!}</option>
                                        @foreach($topicsGroups as $group)
                                            @php /* @var $group TopicsGroup */ @endphp
                                            <option value="{{ $group->id }}">
                                                {{ transDef($group->name,
                                                    TopicsGroup::TRANS_GROUP . '.' . TopicsGroup::TRANS_KEY_PREFIX . '.' . $group->alias) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="issue">
                                <p>{!! ___('topics.create.select-tag') !!}:</p>
                            </div>
                            <div class="answer">
                                <div class="helpStudent planWeek">
                                    <select
                                        id="topic-create-tag"
                                        class="select2 hide-empty"
                                        name="topic-tag"
                                        data-error-target="topic-tag-error-target"
                                        data-color="white"
                                        data-readonly="true"
                                        required="required"
                                    >
                                        <option value="">{!! ___('topics.create.first-select-group') !!}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="issue">
                                <p class="difficulty-info">{!! ___('topics.create.difficulty.select') !!}:</p>
                            </div>
                            <div class="answer">
                                <div class="helpStudent planWeek">
                                    <select
                                        id="topic-create-difficulty"
                                        class="select2"
                                        name="topic-difficulty"
                                        data-error-target="topic-difficulty-error-target"
                                        data-color="white"
                                        required="required"
                                    >
                                        <option value="1" selected="selected">
                                            {!! ___('topics.create.difficulty.very-easy') !!}
                                        </option>
                                        <option value="2">
                                            {!! ___('topics.create.difficulty.easy') !!}
                                        </option>
                                        <option value="3">
                                            {!! ___('topics.create.difficulty.medium') !!}
                                        </option>
                                        <option value="4">
                                            {!! ___('topics.create.difficulty.hard') !!}
                                        </option>
                                        <option value="5">
                                            {!! ___('topics.create.difficulty.very-hard') !!}
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="smallRow mt-4">
                    <div id="topic-create-errors" class="profileList errorList"></div>
                    <div class="profileList">
                        <div class="nameFill vertical-mid">
                            <label for="topic-create-name">{!! ___('topics.attributes.name') !!}</label>
                        </div>
                        <div class="fillAcc vertical-mid">
                            <input
                                    type="text"
                                    id="topic-create-name"
                                    name="topic-name"
                                    class="fillAcc-link"
                                    placeholder="{{ ___('topics.attributes.name') }}"
                                    required="required"
                            >
                        </div>
                    </div>
                    <div class="profileList">
                        <div class="nameFill vertical-mid">
                            <label>{!! ___('topics.create.content-label') !!}</label>
                        </div>
                        <div class="fillAcc vertical-mid flex-between">
                            <div class="topic-content-type">
                                <div class="answer">
                                    <input
                                            type="radio"
                                            name="topic-type"
                                            id="topic-create-content-type-link"
                                            value="{{ Topic::TYPE_LINK }}"
                                            checked="checked"
                                    >
                                    <label class="btn press" for="topic-create-content-type-link">
                                        {!! ___('topics.attributes.link') !!}
                                    </label>
                                </div>
                                <div class="answer label">{!! ___('topics.create.content-label-or') !!}</div>
                                <div class="answer">
                                    <input
                                            type="radio"
                                            name="topic-type"
                                            id="topic-create-content-type-text"
                                            value="{{ Topic::TYPE_CONTENT }}"
                                    >
                                    <label class="btn press" for="topic-create-content-type-text">
                                        {!! ___('topics.attributes.content') !!}
                                    </label>
                                </div>
                            </div>
                            <div class="topic-img-container">
                                <input type="file" id="topic-create-image-file" name="topic-image" accept="{!! implode(',', $mimes) !!}">
                                <img id="topic-create-image" data-src="{{ url('/img/topic-img-upload.png') }}">
                                <label
                                    for="topic-create-image-file"
                                    data-empty-text="{{ ___('topics.create.image.upload') }}"
                                    data-wrong-format="{{ ___('topics.create.image.wrong-format', [
                                        'extensions' => implode(', ', $mimes),
                                    ]) }}"
                                >{!! ___('topics.create.image.upload') !!}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="smallRow">
                    <div
                            class="topic-tab-link active"
                            data-type="{{ Topic::TYPE_LINK }}"
                    >
                        <input
                                type="text"
                                id="topic-create-content-link"
                                name="topic-link"
                                class="fillAcc-link"
                                placeholder="{{ ___('topics.create.url') }}"
                                required="required"
                        >
                    </div>
                    <div
                            class="topic-tab-content"
                            data-type="{{ Topic::TYPE_CONTENT }}"
                    >
                        <textarea
                                id="topic-create-content-text"
                                name="topic-content"
                                data-error-target="topic-content-error-target"
                                rows="3"
                                class="textarea summernote-simple"
                                placeholder="{{ ___('topics.create.content-label') }}"
                                required="required"
                                disabled="disabled"
                        ></textarea>
                        <div id="topic-content-error-target"></div>
                    </div>
                </div>
                <div class="smallRow center mt-5">
                    <button type="submit" class="btn discuss">{!! ___('global.save') !!}</button>
                </div>
            </form>
        </div>
    </div>
    <!-- создание темы  + -->
@endif