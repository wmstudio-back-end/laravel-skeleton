<div class="notification-wrap hidden">
    <div data-id="msg-loading" class="wait-loading hidden"></div>
    <input id="msg-token" type="hidden" value="{{ csrf_token() }}">
    <input id="msg-user-info" type="hidden" value="{{ route('chat.user-info', ['userId' => 'user-id']) }}">
    <div class="dialogBoxModal">
        <div class="chatsBox" data-action="{{ route('chat.users') }}">
            <div class="dialogBoxModal-menu">
                <div class="dialogBox-name wrapDialogBox-name">{{ ___('global.messages.title') }}</div>
                <div class="wrapCollapseClose">
                    <button type="button" class="collapse">
                        <span class="collapse-ic"></span>
                    </button>
                    <button type="button" class="dialogBoxClose"></button>
                </div>
            </div>
            <div class="dialogBox-wrap">
                <div class="dialogBox">
                    <div class="scroll-container">
                        <div data-id="msg-user-template" class="wrapChat open-messages">
                            <a data-id="msg-link" href="{{ route('user.' . ($user->getStatus() === 't' ? 'teacher' : 'student'),
                                            ['login' => 'user-id']) }}" class="dialogImg-wrap">
                                <div class="card-face-img">
                                    <img data-id="msg-user-avatar">
                                    <span data-id="user-line-status" class="offline"></span>
                                </div>
                            </a>
                            <div class="dialogText">
                                <p data-id="msg-name" class="name"></p>
                                <p data-id="msg-country" class="city"></p>
                            </div>
                            <span data-id="msg-new-messages" class="sms hidden"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dialogBoxChats" data-action="{{ route('chat.messages', ['userId' => 'user-id']) }}">
            <div class="dialogBoxModal-menu">
                <button type="button" class="wrapDialogBox-name">
                    <span class="dialogBox-name-ic"></span>
                    <div data-id="msg-name" class="dialogBox-name"></div>
                </button>
                <div class="wrapCollapseClose">
                    <button type="button" class="collapse">
                        <span class="collapse-ic"></span>
                    </button>
                    <button type="button" class="dialogBoxClose"></button>
                </div>
            </div>
            <div class="dialogBox-wrap">
                <div class="dialogBox">
                    <div class="alignBottom">
                        <div data-id="msg-date" class="dialogBox-date"></div>
                        <div data-id="receiver-container" class="fromMan">
                            <p class="msg-message"></p>
                            <span class="dialogBox-time msg-time"></span>
                            <span class="fromMan-ic"></span>
                        </div>
                        <div data-id="sender-container" class="fromMan fromMain">
                            <p class="msg-message"></p>
                            <span class="dialogBox-time msg-time"></span>
                            <span class="fromMan-rig-ic"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dialogBox-inp">
                <textarea id="new-message" rows="1" class="dialogBox-inp-text" placeholder="{{ ___('global.messages.mew-message') }}"></textarea>
            </div>
        </div>
        <span class="dialogBox-ic"></span>
    </div>
    <button type="button" class="notification linc-column">
        <span class="postal-ic"></span>
        <span id="msg-new-messages" class="notifyNum hidden"></span>
    </button>
</div>