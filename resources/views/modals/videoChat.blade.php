@php $user = Auth::user(); @endphp

<div class="callDialog-fon">
    <div class="callDialog">
        <div class="callDialog-video">
            <div class="callDialog-close smoothly hidden"></div>
            <div class="callDialog-self-video smoothly fast">
                <video id="localVideo" class="hidden not-init" local></video>
            </div>
            <div class="callDialog-call">
                <button id="call-answer" class="btn endCallBtn smoothly fast call" title="{{ ___('video.answer-call') }}"><span></span></button>
                <button id="reject-user" class="btn endCallBtn smoothly fast reject" title="{{ ___('video.reject-call') }}"><span></span></button>
            </div>
            <div id="video-container" class="callDialog-video-container"></div>
            <div id="video-controls" class="callDialog-controls-container smoothly hidden">
                @if($user->hasStatus('t'))
                    <div id="add-user" class="add-user-to-call hidden">
                        <select class="select2 custom-init" data-action="{{ route('find-f-o-users') }}">
                            <option value="">{{ ___('video.add-user') }}</option>
                        </select>
                    </div>
                @endif
                <button id="audio-ctrl" class="btn endCallBtn smoothly fast stream-control-audio" disabled="disabled"><span></span></button>
                <button id="video-ctrl" class="btn endCallBtn smoothly fast stream-control-video" disabled="disabled"><span></span></button>
                <button id="reject-call" class="btn endCallBtn smoothly fast reject call-reject" title="{{ ___('video.reject-call') }}"><span></span></button>
            </div>
        </div>
        <div class="callDialog-sms">
            <div class="dialogBoxModal-menu">
                <h5 id="call-dialog-name" class="callDialog-name"></h5>
            </div>
            <div class="dialogBox-wrap">
                <div class="dialogBox">
                    <div id="rtc-msg-container" class="alignBottom">
                        <div id="rtc-foreign" class="fromMan hidden">
                            <p class="rtc-message"></p>
                            <span class="dialogBox-time rtc-sender"></span>
                            <span class="fromMan-ic"></span>
                        </div>
                        <div id="rtc-self" class="fromMan fromMain">
                            <p class="rtc-message"></p>
                            <span class="dialogBox-time">{{ ___('global.messages.you') }}</span>
                            <span class="fromMan-rig-ic"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callDialog-write">
                <textarea
                    id="rtc-send-msg"
                    class="callDialog-write-sms"
                    placeholder="{{ ___('global.messages.mew-message') }}"
                    disabled="disabled"
                ></textarea>
            </div>
        </div>
    </div>
</div>
