<!-- Reservations for teachers -->
<div id="reservation-modal-teacher" class="bookModal">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>

        <div class="wrapBookModal-title">
            <p class="bookModal-title">{!! ___('reservation.modal.title') !!}</p>
        </div>
        <div class="myTime-wrap">
            <div class="wrapPiker">
                <div class="dataPiker-wrap">
                    <div
                        id="reservationCalendar"
                        class="StTch-info_dataPiker"
                        @if(isset($firstTime))data-value="{{ $firstTime }}"@endif
                    ></div>
                </div>
                <div id="add-reservations" class="append-DataTimePiker">
                    <div class="appendText-wrap ">
                        <p id="reservation-add-info" class="colorGr">
                            {!! ___('reservation.modal.description') !!}
                        </p>
                        <div id="reservation-add-time" style="display: none;">
                            <div class="appendText-wrap">
                                <div class="marBot">
                                    <p class="h7">{!! ___('reservation.modal.date') !!}</p>
                                </div>
                                <div class="addDate">
                                    <p class="marBot15">{!! ___('reservation.modal.add-date') !!}</p>
                                    <div id="add-time-date" class="btn dateBtn"></div>
                                </div>
                                <div class="addTime">
                                    <div class="add-time">
                                        <p class="marBot15 ml-1">{!! ___('reservation.modal.add-time') !!}</p>
                                        <div class="timeBtn-wrap">
                                            <select class="select2" data-live-search="false" name="add-hour">
                                                @for($hour = 0; $hour < 24; $hour++)
                                                    @php $hour = $hour > 9 ? $hour : '0' . $hour; @endphp
                                                    <option value="{{ $hour }}">{{ $hour }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <span>:</span>
                                        <div class="timeBtn-wrap">
                                            <select class="select2" name="add-minute">
                                                @for($minute = 0; $minute < 60; $minute+=15)
                                                    @php $minute = $minute > 9 ? $minute : '0' . $minute; @endphp
                                                    <option value="{{ $minute }}">{{ $minute }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="add-time ml-5">
                                        <p class="marBot15 ml-1">{!! ___('reservation.modal.duration') !!}</p>
                                        <div class="timeBtn">
                                            @php
                                                $countMinutes = explode("\n", setting('count_minutes'));
                                                $defMinutes = reset($countMinutes);
                                                foreach ($countMinutes as $key => $countMinute) {
                                                    if (strpos($countMinute, '*') !== false) {
                                                        $countMinutes[$key] = str_replace('*', '', $countMinute);
                                                        $defMinutes = $countMinutes[$key];
                                                    }
                                                }
                                            @endphp
                                            <select class="select2" name="add-duration">
                                                @foreach($countMinutes as $val)
                                                    <option value="{{ $val }}"{{ $val == $defMinutes ? ' selected="selected"' : '' }}>
                                                        {{ $val }}
                                                        {!! ___('topics.modal.student.minutes.five') !!}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button
                            id="add-time-btn"
                            class="btn appendBtn"
                            data-action="{{ route('user.reservation.add') }}"
                        >{!! ___('reservation.modal.add') !!}</button>
                        <button id="add-time-close-btn" class="btn resume-cancel" style="display: none;">
                            {!! ___('reservation.modal.abort') !!}
                        </button>
                        <p id="add-reservations-error" class="add-reservations-error"></p>
                    </div>
                </div>
                <div id="reserved-times" class="wrapTimeReport">
                    <div class="mb-4">
                        <p class="h7">{!! ___('reservation.modal.current-date', [
                            'date' => '<span id="selected-date"></span>',
                        ]) !!}</p>
                    </div>
                    <div id="time-element-container">
                        <div id="empty-time-element" class="bookingTime">
                            <div class="bookingFreeCancel">{!! ___('reservation.modal.empty-times') !!}</div>
                        </div>
                        <div
                            id="time-element"
                            class="bookingTime"
                            data-busy-class="bookingColor"
                        >
                            <div data-id="time" class="bookingTime-time">
                                <span data-id="from"></span>{!! ___('reservation.modal.time-separator') !!}<span data-id="to"></span>
                            </div>
                            <div
                                data-id="status"
                                class="bookingFreeCancel"
                                data-free="{{ ___('reservation.modal.status.free') }}"
                                data-busy="{{ ___('reservation.modal.status.busy') }}"
                            ></div>
                            <button
                                data-id="abort-btn"
                                data-action="{{ route('user.reservation.actions') }}"
                                class="btn resume-cancel"
                                type="button"
                            >{!! ___('reservation.modal.action.abort') !!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Reservations for teachers -->
