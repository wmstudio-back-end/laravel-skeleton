<!-- темы обсуждение студента  + -->
<div id="topic-modal" class="bookModal">
    <div class="bookModal-left"></div>
    <div class="bookModal-wrap">
        <button type="button" title="{{ ___('global.close') }}" class="close-modal" data-modal-close></button>

        <div class="wrapBookModal-title">
            <p class="bookModal-title" id="topic-modal-title"></p>
            <p class="bookModal-subtitle">{!! ___('topics.modal.student.select') !!}</p>
        </div>

        <div class="booking-subtitle">
            <p
                id="topic-modal-minutes"
                data-minutes-one="{{ ___('topics.modal.student.minutes.one') }}"
                data-minutes-two="{{ ___('topics.modal.student.minutes.two') }}"
                data-minutes-five="{{ ___('topics.modal.student.minutes.five') }}"
            >{!! ___('topics.modal.student.minutes.title', [
                'count' => '<span id="modal-topic-free-minutes"></span>',
                'minutes' => '<span id="modal-topic-minutes-ending"></span>',
            ]) !!}</p>
            <p>{!! ___('topics.modal.student.lets-start') !!}</p>
        </div>
        <div class="wrapper marBot">
            <p>{!! ___('topics.modal.student.list-teachers') !!}:</p>
        </div>

        <div class="smallRow">
            <div
                id="topic-modal-user-cards-container"
                class="many-card cardScroll"
                data-action="{{ route('topics.teachers') }}"
            ></div>
            <button id="modal-topic-find-more" class="lookYet hidden">{!! ___('user-find.show-more') !!}</button>
        </div>
    </div>
</div>
<!-- темы обсуждение студента  - -->
