@extends('layouts.main')

@section('content')
    <div class="subscription">
        <div class="booking-title">
            <h6>{!! ___('free-time.title') !!}</h6>
        </div>
        <div class="booking-subtitle">
            {!! ___('free-time.description') !!}
        </div>
    </div>

    <div class="findTeacher">
        <div class="row">
            <div class="wrapper">
                @if ($freeTime || $users->count())
                    @if ($freeTime)
                        <div class="free-time-info">
                            <p>{!! ___('free-time.free-time.less') !!}</p>
                            <span class="balance-number mx-2">{{ $freeTime }}</span>
                            <p>{{ numEnding($freeTime, [
                                ___('free-time.minutes.one'),
                                ___('free-time.minutes.two'),
                                ___('free-time.minutes.five'),
                            ]) . ' ' . ___('free-time.free-time.for') }}</p>
                        </div>
                    @else
                        <p>{!! ___('free-time.no-free-time') !!}</p>
                    @endif
                @else
                    {!! ___('free-time.did-not-have') !!}
                @endif
            </div>
        </div>
    </div>

    <div class="wrap-shares">
        <div class="row">
            @if (old('validEmails'))
                <div id="send-success">
                    <center>
                        <p>{{ ___('free-time.send-success') }}: {{ old('validEmails') }}</p>
                    </center>
                </div>
            @endif
            @if(old('invalidEmails') || old('existingUsers') )
                <div class="send-error">
                    <center>
                        @if(old('invalidEmails'))
                            <p>{{ ___('free-time.invalid-emails') }}: {{ old('invalidEmails') }}</p>
                        @endif
                        @if(old('existingUsers'))
                            <p>{{ ___('free-time.already-registered') }}: {{ old('existingUsers') }}</p>
                        @endif
                    </center>
                </div>
            @endif
            @if($users->count())
                <div class="invitedGuest">
                    {!! ___('free-time.invited-list') !!}
                </div>
                <div class="rowCol">
                    <div class="many-card bookingMany-card">
                        @foreach($users as $user)
                            <div class="card cardBooking">
                                @if($user->hasStatus('t'))
                                    <a
                                        href="{{ route('user.teacher', ['login' => strtolower($user->login)]) }}"
                                        {{--data-modal-id="infoStudentModal"--}}
                                        class="link-over"
                                    ></a>
                                @endif
                                <div class="card-face bookingCard-face">
                                    <div class="card-face-img">
                                        <img src="{{ $user->getAvatar() }}">
                                        <span data-line-status-id="{{ $user->id }}" class="offline"></span>
                                    </div>
                                    <div class="minInfo">
                                        @if(config('view.show-rating'))
                                            <div class="card-rating">
                                                <select
                                                        class="rating"
                                                        data-theme="fontawesome-stars-o"
                                                        data-readonly="true"
                                                        data-init="{{ $user->getRating() }}"
                                                >
                                                    <option value="">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        @endif
                                        <div class="name">{{ $user->getFullName() }}</div>
                                        @php
                                            $city = \Modules\Translations\Constants\Languages::getCountry($user->addon('country'));
                                            $dialects= \Modules\Users\Entities\UserAddSettings::where('control_type', 'dialect')
                                                ->first()->transValue($user->addon('st_dialect'));
                                        @endphp
                                        @if($city || count($dialects))
                                            <div class="city">
                                                @if($city)
                                                    <p>{{ $city }}</p>
                                                @endif
                                                @if(count($dialects))
                                                    <span class="dialectSpan">({!! ___('user-info.dialect') !!}: <span>{{
                                                        implode(', ', $dialects)
                                                    }}</span>)</span>
                                                @endif
                                            </div>
                                        @endif
                                        <div class="tag">
                                            @php
                                                $chats = rand(1,1500);
                                                $sign = $chats > 10 ? '>' : '';
                                                if ($chats > 1000) {
                                                    $chats = round($chats / 1000) * 1000;
                                                } else if ($chats > 100) {
                                                    $chats = round($chats / 100) * 100;
                                                } else if ($chats > 10) {
                                                    $chats = round($chats / 10) * 10;
                                                }
                                                $chats = $sign . ($sign === '' ? '' : ' ') . $chats . ' '
                                                    . numEnding($chats, [
                                                        ___('user-info.chats-count.one'),
                                                        ___('user-info.chats-count.two'),
                                                        ___('user-info.chats-count.five'),
                                                    ]);
                                            @endphp
                                            <div class="tag__text">{{ $chats }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-text bookingCard-free-time">
                                    <div class="data">
                                        <p>{!! $user->addon('st_about_me') !!}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="invitedGuest">
                    {!! ___('free-time.no-invited') !!}
                </div>
            @endif
            <div class="shares">
                <div class="shares-coll">
                    <div class="shares-ic"><span class="bringFriend"></span></div>
                    <div class="shares-text">
                        <p>{!! ___('free-time.info.bring-friend.title') !!}</p>
                        {!! ___('free-time.info.bring-friend.description') !!}
                    </div>
                </div>

                <div class="shares-coll">
                    <div class="shares-ic"><span class="referral"></span></div>
                    <div class="shares-text">
                        <p>{!! ___('free-time.info.share-referral.title') !!}</p>
                        {!! ___('free-time.info.share-referral.description') !!}
                    </div>
                </div>

                <div class="sharing with-copy-link">
                    <div class="toilsLink">
                        @php($referralLink = route('referral-link', ['token' => $referral]))
                        <button
                            id="copy-referral-link"
                            type="button"
                            class="wrapToils colorTether"
                            title="{{ $referralLink }}"
                            data-referral="{{ $referralLink }}"
                        ><span class="tether"></span></button>
                        <p class="toilsText">{{ ___('free-time.info.share-referral.copy-link') }}</p>
                    </div>
                    <div class="specifyMail">
                        <form method="post" action="{{ route('send-invitations') }}">
                            @csrf
                            <input
                                type="text"
                                name="emails"
                                class="specifyEmail"
                                placeholder="{{ ___('free-time.info.share-referral.emails') }}"
                                value="{{ old('invalidEmails') }}"
                            >
                            <button type="submit" class="btn sendBtn">
                                {!! ___('free-time.info.share-referral.send') !!}
                            </button>
                        </form>
                    </div>
                    {{--<div class="toils">
                        <div class="toilsLink">
                            <a href="#" target="_blank" class="wrapToils"><span class="vk"></span></a>
                            <a href="#" target="_blank" class="wrapToils"><span class="fs"></span></a>
                            <a href="#" target="_blank" class="wrapToils"><span class="ok"></span></a>
                            <a href="#" target="_blank" class="wrapToils"><span class="tw"></span></a>
                            <a href="#" target="_blank" class="wrapToils"><span class="g"></span></a>
                            <a href="#" target="_blank" class="wrapToils"><span class="in"></span></a>
                        </div>
                    </div>--}}
                </div>

                <div class="shares-coll">
                    <div class="shares-ic"><span class="represent"></span></div>
                    <div class="shares-text">
                        <p>{!! ___('free-time.info.become-representative.title') !!}</p>
                        {!! ___('free-time.info.become-representative.description') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
