window.toastr = toastr;
window.$ = $;

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 500,
    "timeOut": 2000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

function numEnding(number, one, two, five) {
    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
        return five;
    }
    number %= 10;
    if (number == 1) {
        return one;
    }
    if (number >= 2 && number <= 4) {
        return two;
    }
    return five;
}

function ajaxError(data) {
    let response = JSON.parse(data.responseText);
    if (response.message !== undefined && response.message !== '') {
        let header = (response.msgHeader !== undefined) ? response.msgHeader : null;
        toastr.error(response.message, header);
    }
    console.log(data);
}

function checkHtml(text) {
    return /<[a-z][\s\S]*>/i.test(text);
}

$(document).ready(function(){
    $.fn.datetimepicker.defaults.icons = {
        time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-calendar-check-o',
            clear: 'fa fa-trash-o',
            close: 'fa fa-times'
    };
    $.fn.datetimepicker.defaults.locale = 'ru';
    $.fn.datetimepicker.defaults.format = 'DD.MM.YYYY HH:mm';
    $('input.datetimepicker').datetimepicker({
        sideBySide: true,
    });
    $('input.datepicker').datetimepicker({
        format: "DD.MM.YYYY",
    });

    $.fn.select2.defaults.set( "theme", "bootstrap4" );
    $.fn.select2.defaults.set('language', 'ru');
    $('select.selectpicker').each(function (key, val) {
        let ajax = false,
            url = $(this).data('action'),
            onSelect = $(val).data('select'),
            minimumInput = $(this).data('minimum-input'),
            placeholder = $(this).data('placeholder'),
            allowClear = $(this).data('allow-clear');

        if (allowClear == null) {
            allowClear = false;
        }

        if (minimumInput == null) {
            minimumInput = url != null ? 3 : 0;
        }

        if (onSelect != null) {
            onSelect = eval(onSelect);
            if ($.isFunction(onSelect)) {
                $(val).on('select2:select', onSelect);
            }
        }

        if ($(this).hasClass('ajax-select') && $(this).data('token') != null && url != null) {
            ajax = true;
        }
        $(val).select2({
            containerCssClass: $(val).data('class'),
            minimumResultsForSearch: $(val).data('live-search') == false ? -1 : 0,
            minimumInputLength: minimumInput,
            placeholder: placeholder,
            allowClear: allowClear,
            ajax: ajax ? {
                url: url,
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function(params) {
                    let extraName = $(this).data('extra-name'),
                        extraVal = $(this).data('extra-value'),
                        result = [
                            {
                                name: '_token',
                                value: $(this).data('token'),
                            },
                            {
                                name: 'search',
                                value: params.term
                            },
                        ];
                    if (extraName != null && extraVal != null) {
                        result.push({
                            name: extraName,
                            value: extraVal,
                        });
                    }

                    return result;
                },
                processResults: function (data) {
                    let result = [];
                    if (data.error != null) {
                        result.push({
                            id: '',
                            text: data.error,
                        })
                    } else {
                        result = data.list;
                    }
                    return {
                        results: result
                    };
                },
                error: function(data) {
                    console.log(data);
                },
                cache: true,
            } : null,
            tags: $(val).hasClass('select-with-input'),
        });
    });
    $(".sortable-with-handle").sortable({handle:'.sort-handler'}).on('sortstop', function(){
        let childs = $(this).children();
        childs.find('.after-sort-hide-first').show();
        childs.first().find('.after-sort-hide-first').hide();
    });
    $(".sortable").sortable();

    $('.ajaxSubmit').submit(function(e){
        e.preventDefault();

        let values = $(this).serializeArray();
        values = values.concat(
            $(this).find('input[type=checkbox]:not(:checked)').map(
                function() {
                    return {"name": this.name, "value": 0}
                }).get()
        );

        jQuery.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            data: values,
            success: function (data) {
                if (data.message !== undefined && data.message !== '') {
                    let type = 'error';
                    if (data.error) {
                        console.log(data.message);
                    } else {
                        type = 'success';
                    }
                    let header = (data.msgHeader != undefined) ? data.msgHeader : null;

                    toastr[type](data.message, header);
                }
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    });


    let bindNotificationsHtml = function(data){
        $('span.notifications-count').text(data.length);
        if (data.length) {
            $('a.nav-link.notification-toggle').removeClass('hidden');
            let html = '';
            $.each(data, function(ind, val){
                html = html + "<form method=\"post\" action=\"/admin\" class=\"ajaxNotifications\">";
                html = html + "<input type=\"hidden\" name=\"_token\" value=\"" + val.csrf  + "\">";
                html = html + "<input type=\"hidden\" name=\"redirect\" value=\"" + window.location.href + "\">";
                html = html + "<input type=\"hidden\" name=\"action\" value=\"" + val.action + "\">";
                html = html + "<button class=\"btn btn-link dropdown-item\" type=\"submit\">";
                html = html + (val.img === undefined ? '' :
                    "<img alt=\"image\" src=\"" + val.img + "\" class=\"rounded-circle dropdown-item-img\">");
                html = html + (val.icon === undefined ? '' :
                    "<i class=\"" + val.icon + "\"></i>");
                html = html + "<div class=\"dropdown-item-desc\">";
                html = html + val.text + "</div></button></form>";
            });
            $('ul.navbar-nav .dropdown-list-content').html(html);
            $('.ajaxNotifications').submit(ajaxNotifications);
        } else {
            $('a.nav-link.notification-toggle').addClass('hidden');
            $('ul.navbar-nav .dropdown-list-content').html('');
            $('ul.navbar-nav .dropdown-menu.dropdown-list.dropdown-menu-right').removeClass('show');
        }
    };
    let ajaxNotifications = function(e){
        e.preventDefault();

        let values = $(this).serializeArray();
        values = values.filter(x => x.name !== 'redirect');

        jQuery.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            data: values,
            success: function (data) {
                bindNotificationsHtml(data);
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    };
    $('.ajaxNotifications').submit(ajaxNotifications);
    let token = $('#ajaxToken').val();
    $('#ajaxToken').remove();
    setInterval(function(){
        jQuery.ajax({
            url: '/admin',
            type: 'POST',
            dataType: 'json',
            data: {
                _token: token,
                action: 'get',
            },
            success: function (data) {
                bindNotificationsHtml(data);
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    }, 60000 * 5);

    $("[data-checkboxes]").each(function() {
        let me = $(this),
            group = me.data('checkboxes'),
            role = me.data('checkbox-role');
        me.change(function() {
            let all = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"])'),
                checked = $('[data-checkboxes="' + group + '"]:not([data-checkbox-role="dad"]):checked'),
                dad = $('[data-checkboxes="' + group + '"][data-checkbox-role="dad"]'),
                total = all.length,
                checked_length = checked.length;
            if (role == 'dad') {
                if (me.is(':checked')) {
                    all.prop('checked', true)
                } else {
                    all.prop('checked', false)
                }
            } else {
                if (checked_length >= total) {
                    dad.prop('checked', true)
                } else {
                    dad.prop('checked', false)
                }
            }
        })
    });

    $('.parse-data-table').dataTable({
        ordering: true,
        columnDefs: [{
            sortable: false,
            orderable: false,
            targets: "no-sort"
        }],
        order: [0, 'asc'],
        language: {
            processing:     "Подождите...",
            search:         "Поиск:",
            lengthMenu:     "Показать _MENU_ записей",
            info:           "Записи с _START_ по _END_ из _TOTAL_ записей",
            infoEmpty:      "Нет записей",
            infoFiltered:   "(отфильтровано из _MAX_ записей)",
            infoPostFix:    "",
            loadingRecords: "Загрузка записей...",
            zeroRecords:    "Записи отсутствуют.",
            emptyTable:     "В таблице отсутствуют данные",
            paginate: {
                first:      "Первая",
                previous:   "Предыдущая",
                next:       "Следующая",
                last:       "Последняя",
            },
            aria: {
                sortAscending:  ": активировать для сортировки столбца по возрастанию",
                sortDescending: ": активировать для сортировки столбца по убыванию",
            },
        },
        bLengthChange: false,
        iDisplayLength: 50,
    }).on('draw.dt', function(){
        $('.open-trans-modal').unbind('click').bind('click', openTransModal);
        $('form.transRemoveAjax').unbind('submit').bind('submit', transRemoveAjax);
    });

    let transModal = $('#trans-key-modal'),
        transContent = $('#trans-value'),
        transForm = $('#trans-form'),
        openTransModal = function(){
            let group = $(this).data('group'),
                key = $(this).data('key'),
                locale = $(this).data('locale'),
                value = $(this).data('value');
            $('#trans-original-value').text(value);
            transForm.attr('action', transForm.data('action').replace('replace-group', group));
            $('#trans-key').val(key);
            $('#trans-locale').val(locale);
            transContent.summernote('code', value);
            transModal.modal('show');
        };
    $('.open-trans-modal').bind('click', openTransModal);

    transModal.on('shown.bs.modal', function(){
        let html = checkHtml(transContent.val());

        if ((html && codeView(transModal)) || !html && !codeView(transModal)) {
            transContent.summernote('codeview.toggle');
        }
    }).on('hidden.bs.modal', function(){
        $('#trans-original-value').text('');
        transForm.removeAttr('action');
        $('#trans-key').val('');
        $('#trans-locale').val('');
        transContent.summernote('code', '');
        if (codeView(transModal)) {
            transContent.summernote('codeview.toggle');
        }
    });

    $('[data-dismiss="modal"]').click(function(){
        $(this).parents('.modal').modal("hide");
    });

    $('textarea.summernote').parents('form').submit(function(e) {
        $(this).find('textarea.summernote').each(function(key, val){
            $(this).val($(this).summernote('code'));
        });
    });

    $('.transEditAjax').submit(function(e){
        e.preventDefault();

        let modal = $(this).parents('.modal'),
            textArea = modal.find('textarea.summernote');
        textArea.text(textArea.summernote('code'));
        let values = $(this).serializeArray();

        jQuery.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            data: values,
            success: function (data) {
                if (data.message !== undefined && data.message !== '') {
                    let type = 'error';
                    if (data.error) {
                        console.log(data.message);
                    } else {
                        type = 'success';
                    }
                    let header = (data.msgHeader != undefined) ? data.msgHeader : null;

                    let text = textArea.text();
                    if (text.length > 50) {
                        text = text.substr(0,50) + '...'
                    }
                    if (text === '') {
                        text = 'Empty';
                    }
                    let key = $('#trans-key').val(),
                        locate = $('#trans-locale').val();

                    $('button[data-key="' + key + '"][data-locale="' + locate + '"]').text(text).data('value', textArea.text());
                    modal.modal('hide');
                    toastr[type](data.message, header);

                }
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    });

    let transRemoveAjax = function(e){
        e.preventDefault();

        swal({
            title: "Вы уверенны?",
            text: "После удаления перевод будет невозможно восстановить!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Нет, не удалять.",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "Да, удалить!",
                    value: true,
                    visible: true,
                    className: "swal-delete-confirm",
                    closeModal: false,
                }
            },
        }).then((willDelete) => {
            if (willDelete) {
                let key = $(this).find('input[name="key"]').val(),
                    values = $(this).serializeArray();

                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: values,
                    success: function (data) {
                        if (data.message !== undefined && data.message !== '') {
                            let type = 'error';
                            if (data.error) {
                                console.log(data.message);
                            } else {
                                type = 'success';
                            }
                            let header = (data.msgHeader != undefined) ? data.msgHeader : null;

                            $('tr[data-id="' + key + '"]').remove();
                            swal.close();
                            toastr[type](data.message, header);
                        }
                    },
                    error: function (data) {
                        ajaxError(data);
                    }
                });
            }
        });
    };
    $('form.transRemoveAjax').submit(transRemoveAjax);

    $('.custom-file-input').change(function(e){
        let label = $(this).next().prop('tagName').toLowerCase();
        if (label === 'label') {
            if (this.files.length) {
                let files = [];
                for (let i = 0; i < this.files.length; i++) {
                    files.push(this.files[i].name)
                }
                $(this).next().text(files.join(', '));
            } else {
                $(this).next().text('Выберите файлы');
            }
        }
    });

    $('#modal-file-edit').on('shown.bs.modal', function () {
        $('#modal-file-new-alias').focus();
    });
    $('.control-btn-edit').click(function(e){
        e.preventDefault();

        let modal = $('#modal-file-edit'),
            alias = $(this).data('alias'),
            id = $(this).data('id');

        modal.find('#modal-file-title').text('Файл "' + alias + '"');
        modal.find('#modal-file-id').val(id);
        modal.find('#modal-file-new-alias').val(alias);

        modal.modal('show');
    });

    $('#modal-file-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).find('#modal-file-id').val();

        $('#modal-file-edit').modal('hide');
        jQuery.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            dataType: 'json',
            data: $(this).serializeArray(),
            success: function (data) {
                if (data.error) {
                    console.log(data.message);
                } else {
                    let btn = $('button[data-id="' + id + '"]');
                    btn.attr('data-alias', data.newAlias)
                        .data('alias', data.newAlias)
                        .parents('.file-box').find('.control-file-link').attr('href',  location.protocol
                        + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/' + data.newAlias)
                        .find('.file-name').text(data.newAlias);

                    swal("Выполнено", data.message, "success");
                }
            },
            error: function (data) {
                ajaxError(data);
            }
        });
        $(this).find('input').val('');
    });

    $('.file-delete-form').submit(function (e) {
        e.preventDefault();

        let fileBox = $(this).parents('.file-box')

        swal({
            title: "Вы уверенны?",
            text: "После удаления файл будет невозможно восстановить!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Нет, не удалять.",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "Да, удалить!",
                    value: true,
                    visible: true,
                    className: "swal-delete-confirm",
                    closeModal: false,
                }
            },
        }).then((willDelete) => {
            if (willDelete) {
                jQuery.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: $(this).serializeArray(),
                    success: function (data) {
                        if (data.error) {
                            console.log(data.message);
                        } else {
                            fileBox.fadeOut('slow', function(){
                                $(this).remove();
                            });
                            swal("Удалено!", data.message, "success");
                        }
                    },
                    error: function (data) {
                        ajaxError(data);
                    }
                });
            }
        });
    });

    $('.control-file-link').click(function(e){
        if (e.which == 1) {
            e.preventDefault();

            $(this).parents('.file-box').toggleClass('selected');
        }
    });

    $('.control-file-link').dblclick(function(e){
        e.preventDefault();

        let win = window.open($(this).attr('href'), '_blank');
        if(win){
            win.focus();
        }
    });

    $('#files-sel-all').click(function (e) {
        e.preventDefault();

        $('.file-box').addClass('selected');
    });

    $('#files-sel-inverse').click(function (e) {
        e.preventDefault();

        $('.file-box').toggleClass('selected');
    });

    $('#files-sel-delete').click(function (e) {
        e.preventDefault();

        let serialize = [
                {
                    name: '_token',
                    value: $(this).data('csrf'),
                },
                {
                    name: 'action',
                    value: 'deleteManyFiles'
                },
            ],
            $selected = $('.file-box.selected'),
            action = $(this).data('action'),
            count = 0;
        $selected.each(function(key, val){
            serialize.push({name: 'data[]', value: $(val).find('form input[name="fileId"]').val()});
            count++;
        });

        if (count) {
            swal({
                title: "Вы уверенны, что хотите удалить " + count + " " + numEnding(count, 'файл', 'файла', 'файлов') + "?",
                text: "После удаления файлы будет невозможно восстановить!",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Нет, не удалять.",
                        value: false,
                        visible: true,
                        closeModal: true,
                    },
                    confirm: {
                        text: "Да, удалить!",
                        value: true,
                        visible: true,
                        className: "swal-delete-confirm",
                        closeModal: false,
                    }
                },
            }).then((willDelete) => {
                if (willDelete) {
                    jQuery.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'json',
                        data: serialize,
                        success: function (data) {
                            if (data.error) {
                                console.log(data.message);
                            } else {
                                $selected.fadeOut('slow', function(){
                                    $(this).remove();
                                });
                                swal("Удалено!", data.message, "success");
                            }
                        },
                        error: function (data) {
                            ajaxError(data);
                        }
                    });
                }
            });
        }
    });

    $('#menu-sortable').on("sortstop", function() {
        let sortable = [
            {
                name: '_token',
                value: $(this).data('token'),
            }
        ];
        $(this).children().each(function(id, val) {
            sortable.push({
                name: 'sortable[]',
                value: $(val).data('id'),
            })
        });
        jQuery.ajax({
            url: $(this).data('action'),
            type: 'POST',
            dataType: 'json',
            data: sortable,
            success: function (data) {
                let type = 'error';
                if (data.error) {
                    console.log(data.message);
                } else {
                    type = 'success';
                }
                let header = (data.msgHeader !== undefined) ? data.msgHeader : null;

                toastr[type](data.message, header);
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    });

    function changeState(checkboxes, init){
        checkboxes.each(function(key, val){
            let el = $(val),
                checked = el.data('checked');
            if (init === true) {
                checked--;
                if (checked === -2) {
                    checked = 1;
                }
            }
            switch(checked) {
                // unchecked, going checked
                case 0:
                    el.data('checked',1);
                    el.prop('checked',true);
                    el.prop('indeterminate',false);
                    el.val(1);
                    break;
                // checked, going indeterminate
                case 1:
                    el.data('checked',-1);
                    el.prop('checked',true);
                    el.prop('indeterminate',true);
                    el.val(-1);
                    break;
                // indeterminate, going unchecked
                default:
                    el.data('checked',0);
                    el.prop('indeterminate',false);
                    el.prop('checked',false);
                    el.val(1);
                    break;
            }
        });
    }

    let indCheckboxes = $('input[type=checkbox].indeterminate');
    changeState(indCheckboxes, true);
    indCheckboxes.click(function(e) {
        changeState($(this));
    });

    function listGroup(e) {
        let el = $(e.currentTarget);
        swal({
            title: "Добавить пользователя?",
            icon: "info",
            buttons: {
                cancel: {
                    text: "Нет.",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "Да.",
                    value: true,
                    visible: true,
                    closeModal: true,
                }
            },
        }).then((willAdd) => {
            if (willAdd) {
                jQuery.ajax({
                    url: el.data('url'),
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        _token: el.data('token'),
                        action: 'add-user',
                        id: el.val(),
                    },
                    success: function (data) {
                        if (data.error) {
                            if (data.message !== undefined && data.message !== '') {
                                let header = (data.msgHeader !== undefined) ? data.msgHeader : null;
                                toastr.error(data.message, header);
                            }
                            console.log(data.message);
                        } else {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        ajaxError(data);
                    }
                });
            } else {
                $(this).val(null).trigger('change');
            }
        });
    }

    let reviewModal = $('#modal-review-edit'),
        reviewId = reviewModal.find('#modal-review-id'),
        reviewContent = reviewModal.find('#modal-review-content'),
        readOnly = reviewContent.attr('readonly') !== undefined,
        codeView = function(modal){
            let editor = modal.find('.note-editor');
            return editor.length === 0 ? false : editor.hasClass('codeview');
        };
    if (readOnly) {
        reviewContent.summernote('destroy');
    }
    $('#modal-review-edit').on('shown.bs.modal', function(){
        let html = checkHtml(reviewContent.val());

        if (readOnly) {
            if (html) {
                reviewContent.summernote({
                    lang:        'ru-RU',
                    minHeight:   250,
                    toolbar:     [],
                });
                reviewContent.summernote('focus');
                reviewContent.summernote('disable');
            }
        } else {
            if ((html && codeView(reviewModal)) || !html && !codeView(reviewModal)) {
                reviewContent.summernote('codeview.toggle');
            }
        }
    }).on('hidden.bs.modal', function(){
        if (readOnly) {
            reviewContent.summernote('destroy');
        } else {
            if (codeView(reviewModal)) {
                reviewContent.summernote('codeview.toggle');
            }
        }
    });
    $('.review-edit-btn').click(function(e){
        e.preventDefault();

        let content = $(this).data('content'),
            id = $(this).data('id');

        if (reviewId.length) {
            reviewId.val(id);
        }
        reviewContent.val(content);
        if (!readOnly) {
            reviewContent.summernote('code', content);
        }

        reviewModal.modal('show');
    });

    $('#review-up, #review-toggle-active').click(function(){
        let action = $(this).data('post-action'),
            serialize = [
            {
                name: 'action',
                value: action,
            },
            {
                name: '_token',
                value: $(this).data('token'),
            }
        ], moveEls = [];
        $('input.review-selected:checked').each(function(key, val){
            serialize.push({
                name: 'reviewIds[]',
                value: $(val).parents('tr').data('id'),
            });
            moveEls.push($(val).parents('tr'));
        });
        jQuery.ajax({
            url: $(this).data('action'),
            type: 'POST',
            dataType: 'json',
            data: serialize,
            success: function (data) {
                if (data.message !== undefined && data.message !== '') {
                    let type = 'error';
                    if (data.error) {
                        console.log(data.message);
                    } else {
                        type = 'success';
                        switch(action) {
                            case 'upReviews':
                                let first = $('input.review-selected:not(:checked)').first().parents('tr');
                                if (first.length === 0) {
                                    location.reload();
                                }

                                for (let key in moveEls) {
                                    moveEls[key].insertBefore(first);
                                }
                                $("input.review-selected").prop("checked", false);
                                break;
                            case 'toggleActive':
                                let checked = $('input.review-selected:checked');
                                checked.each(function(key, val){
                                    let activeEl = $(val).parents('tr').first().find('.review-active'),
                                        active = activeEl.hasClass('badge-success');
                                    if (active) {
                                        activeEl.removeClass('badge-success').addClass('badge-danger').text('Не активен');
                                    } else {
                                        activeEl.removeClass('badge-danger').addClass('badge-success').text('Активен');
                                    }
                                });
                                checked.prop("checked", false);
                                break;
                        }
                    }
                    let header = (data.msgHeader != undefined) ? data.msgHeader : null;

                    toastr[type](data.message, header);
                }
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    });

    $('#review-delete').click(function(){
        let serialize = [
            {
                name: '_token',
                value: $(this).data('token'),
            }
        ], rmEls = [];
        $('input.review-selected:checked').each(function(key, val){
            serialize.push({
                name: 'reviewIds[]',
                value: $(val).parents('tr').data('id'),
            });
            rmEls.push($(val).parents('tr'));
        });
        jQuery.ajax({
            url: $(this).data('action'),
            type: 'POST',
            dataType: 'json',
            data: serialize,
            success: function (data) {
                if (data.message !== undefined && data.message !== '') {
                    let type = 'error';
                    if (data.error) {
                        console.log(data.message);
                    } else {
                        type = 'success';
                        for (let key in rmEls) {
                            rmEls[key].remove();
                        }
                    }
                    let header = (data.msgHeader != undefined) ? data.msgHeader : null;

                    toastr[type](data.message, header);
                }
            },
            error: function (data) {
                ajaxError(data);
            }
        });
    });

    $('input[data-for]').on("keypress", function(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            $('.btn.user-add-setting[data-for="' + $(this).data('for') + '"]').click();
        } else if (e.keyCode === 10) {
            $('form.user-add-setting').submit();
        }
    });

    let newUserSettingsCounter = 0;
    $('.btn.user-add-setting').click(function(){
        let dataFor = $(this).data('for'),
            input = $('input[data-for="' + dataFor + '"]'),
            val = input.val();
        if (val) {
            let appendVal =
                "<div class=\"custom-control custom-checkbox middle\">" +
                "    <input\n" +
                "        type=\"checkbox\"\n" +
                "        class=\"custom-control-input\"\n" +
                "        id=\"" + dataFor + '-' + (++newUserSettingsCounter) + "\"\n" +
                "        name=\"" + dataFor + "[new]" + "[]\"\n" +
                "        value=\"" + val + "\"\n" +
                "        checked=\"checked\"\n" +
                "    >\n" +
                "    <label\n" +
                "        class=\"custom-control-label user-add-setting\"\n" +
                "    >" + val + "</label>\n" +
                "</div>";
            $('#' + dataFor).append(appendVal);
            input.val('');
            $('label.user-add-setting').click(function(){
                $(this).parents('.custom-control.custom-checkbox.middle').remove();
            })
        }
    });

    $('form.user-add-setting input[type=checkbox]').click(function(){
        let checked = $(this).data('checked');
        if (checked) {
            $(this).data('checked', false);
            $(this).prop('indeterminate', false);
            $(this).prop('checked', false);
        } else {
            $(this).data('checked', true);
            $(this).prop('indeterminate', true);
            $(this).prop('checked', true);
        }
    });

    let metrikaChat = $('#metrika-line-chart');
    if (metrikaChat.length === 1 && window.metrikaData != null) {
        let chartColors = {
            purple: '#574B90',
            green: '#28a745',
            yellow: '#ffc107',
            red: '#dc3545',
            black: '#343a40',
        };
        let ctx = metrikaChat[0].getContext('2d');
        let myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: window.metrikaData.labels,
                datasets: [{
                    label: 'Количество посетителей',
                    data: window.metrikaData.users,
                    backgroundColor: chartColors.purple,
                    borderColor: chartColors.purple,
                    fill: false,
                },{
                    label: 'Количество новых посетителей',
                    data: window.metrikaData.newUsers,
                    backgroundColor: chartColors.green,
                    borderColor: chartColors.green,
                    fill: false,
                },{
                    label: 'Глубина просмотров',
                    data: window.metrikaData.pageDepth,
                    backgroundColor: chartColors.yellow,
                    borderColor: chartColors.yellow,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Месяц'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Значение'
                        }
                    }]
                }
            }
        });
    }

    let removeTextArea = function(e){
        let dataType = $(this).parents('.faq-type').data('class'),
            dataId = $(this).prev().data('id');
        $(this).parents('.tab-content').find('.tab-pane').each(function(key, val){
            $(val).find('[data-class=' + dataType + '] textarea[data-id="' + dataId + '"]').parent().remove();
        });
    };
    $('.faq-add-textarea').click(function(){
        let appendTo = $(this).prev().data('class');
        $(this).parents('.tab-content').find('.tab-pane').each(function(key, val){
            let append = $(val).find('[data-class=' + appendTo + ']'),
                textArea = append.find('textarea').first().clone();
            textArea.addClass('can-delete').attr('data-id', (append.find('textarea').last().data('id') + 1)).val('');
            let div = $("<div class=\"textarea-can-delete\">" +
                "<button type=\"button\" class=\"btn btn-danger btn-action btn-sm textarea-delete\" tabindex=\"-1\">" +
                    "<i class=\"fa fa-times\"></i>" +
                "</button></div>").prepend(textArea);
            append.append(div);
            $('.textarea-can-delete .textarea-delete').unbind('click').bind('click', removeTextArea);
        });
    });
    $('.textarea-can-delete .textarea-delete').bind('click', removeTextArea);

    $('.topic-filter').change(function(){
        let group = $('#topic-group').val(),
            tag = $('#topic-tag').val(),
            user = $('#topic-user').val();
        if (group != null || tag != null || user != null) {
            let query = '?'
                + (group !== '' ? 'group=' + group + '&' : '')
                + (tag !== '' ? 'tag=' + tag + '&' : '')
                + ((user != null) ? 'user=' + user + '&' : '');
            query = query.substr(0, query.length - 1);
            window.location = location.origin + location.pathname + query;
        }
    });

    $('.topic-type-select').click(function(){
        let el = $(this);
        $(this).find('input[name="topic-type"]').prop('checked', true);
        $($(this).attr('href')).find('[name="topic-content"]').removeAttr('readonly').prop('readonly', false);
        setTimeout(function(){
            $('.tab-content .tab-pane:not(.active) [name="topic-content"]').attr('readonly', 'readonly').prop('readonly', true);
        }, 500);
    });

    $('input[data-test-all-questions]').change(function(){
        let displayCount = $('input[data-test-display-count]');
        if ($(this).is(':checked')) {
            displayCount.attr('disabled', 'disabled');
        } else {
            displayCount.removeAttr('disabled');
        }
    });

    let questionDeleteAnswer = function(){
        let answers = $(this).parents('.question-answers');
        $(this).parents('.form-group').first().remove();
        answers.find('.question-correct-answer').each(function(key, val){
            $(val).find('input').attr('id', 'answer' + key);
            $(val).find('label').attr('for', 'answer' + key);
        });
        answers.find('textarea').each(function(key, val){
            $(val).attr('placeholder', 'Ответ №' + (key + 1));
        });
    };
    $('.question-add-answer').click(function(){
        let answers = $('.question-answers textarea'),
            template =
            '<div class="form-group">\n' +
            '  <button type="button" class="btn btn-danger btn-action btn-sm question-delete-answer" tabindex="-1"><i class="fa fa-times"></i></button>\n' +
            '  <div class="custom-control custom-radio question-correct-answer">\n' +
            '    <input\n' +
            '      id="answer' + answers.length + '"\n' +
            '      type="radio"\n' +
            '      tabindex="-1"' +
            '      value="' + answers.length + '"' +
            '      name="correct-answer"\n' +
            '      class="custom-control-input"\n' +
            '    >\n' +
            '    <label class="custom-control-label" for="answer' + answers.length + '">Верный</label>\n' +
            '  </div>\n' +
            '  <textarea\n' +
            '    name="answers[]"\n' +
            '    type="text"\n' +
            '    class="form-control mb-2"\n' +
            '    placeholder="Ответ №' + (answers.length + 1) + '"\n' +
            '    rows="3"\n' +
            '    required="required"\n' +
            '  ></textarea>\n' +
            '</div>';
        $('.question-extra-answers').append(template);
        $('.question-delete-answer').unbind('click').bind('click', questionDeleteAnswer);
    });
    $('.question-delete-answer').bind('click', questionDeleteAnswer);

    $('form.trans-group-remove button[type="submit"]').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        let form = $(this).parents('form.trans-group-remove').first();

        swal({
            title: "Вы уверенны?",
            text: "После удаления группы все ее переводы будет невозможно восстановить!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Нет, не удалять.",
                    value: false,
                    visible: true,
                    closeModal: true,
                },
                confirm: {
                    text: "Да, удалить!",
                    value: true,
                    visible: true,
                    className: "swal-delete-confirm",
                    closeModal: true,
                }
            },
        }).then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
        });
    });
});
