import {tagCheck} from './modules/findTopics';
import {numEnding} from "../modules/helpers";

export default function topicsTeacher() {
    $(document).ready(function(){
        let modalPrefix = 'modal-',
            template = $($('#topic-card').remove().html()),
            topicEndings = window.topicEndings,
            onCardBtnClick = function(){
                $('.btn.discuss[data-id="topic-card-button"]').unbind('click').bind('click', function(){
                    if (!$(this).hasClass('author')) {
                        let button = $(this),
                            topic = $(this).parents('.card.topicCard').data('topic-id'),
                            setChecked = !($(this).hasClass('checked')) ? 1 : 0,
                            container = $('#card-container');
                        $.ajax({
                            url: container.data('check-action'),
                            type: 'POST',
                            dataType: 'json',
                            data: [
                                {
                                    name: '_token',
                                    value: window.token,
                                },
                                {
                                    name: 'topic',
                                    value: topic,
                                },
                                {
                                    name: 'checked',
                                    value: setChecked,
                                },
                            ],
                            success: function (data) {
                                if (data.error != null) {
                                    console.log(data.error);
                                } else if (data.checked != null) {
                                    if (data.checked) {
                                        button.removeClass('not-checked').addClass('checked').text(window.themeButtonTexts.checked);
                                    } else {
                                        if (button.parents('#topic-modal-teacher').length) {
                                            button.parents('.card').first().remove()
                                        } else {
                                            button.removeClass('checked').addClass('not-checked').text(window.themeButtonTexts.notChecked);
                                        }
                                    }
                                }
                                if (data.count != null) {
                                    $('#topic-count').text(data.count);
                                    let topicEnding = numEnding(data.count, topicEndings.one, topicEndings.two, topicEndings.five);
                                    $('#topic-count-ending').text(topicEnding);
                                    if (modalPrefix !== '') {
                                        $('#' + modalPrefix + 'topic-count').text(data.count);
                                        $('#' + modalPrefix + 'topic-count-ending').text(topicEnding);
                                    }
                                }
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });
                    }
                });
            };
        tagCheck(template, onCardBtnClick);
        tagCheck(template, onCardBtnClick, modalPrefix);

        $('[data-modal-id="topic-modal-teacher"]').click(function(){
            let modal = $('#' + $(this).data('modal-id') + '.bookModal');
            modal.find('#modal-card-container').html('');
            modal.find('#modal-topic-tags').html('');
            modal.find('input[name="modal-topic-group"]').removeAttr('checked').prop('checked', false);
            modal.find('#modal-find-more').addClass('hidden');
        });
    });
};
