import {addWatch} from "../modules/statusMonitor";

export default function studentCard() {
    $(document).ready(function() {
        let userId = $('input#user-id').val(),
            input   = $('#update-watch-list'),
            action  = input.data('action'),
            token   = window.token,
            checked = input.prop('checked'),
            process = false;
        addWatch(userId);
        input.change(function(e) {
            $(this).prop('checked', checked);
            if(!process) {
                process  = true;
                let data = [
                    {
                        name:  '_token',
                        value: token,
                    },
                ];
                if(checked) {
                    data.push({
                        name:  'action',
                        value: 'delete',
                    })
                }
                $.ajax({
                    url:      action,
                    type:     'POST',
                    dataType: 'json',
                    data:     data,
                    success:  function(data) {
                        if(data) {
                            checked = !checked;
                            input.prop('checked', checked);
                        }
                        process = false;
                    },
                    error:    function(data) {
                        console.log(data);
                        process = false;
                    }
                });
            }
        })
    });
};
