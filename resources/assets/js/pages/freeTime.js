export default function freeTime() {
    $(document).ready(function(){
        $('#copy-referral-link').click(function(){
            const tmpEl = document.createElement('textarea');
            tmpEl.value = $(this).data('referral');
            tmpEl.setAttribute('readonly', '');
            tmpEl.style.position = 'absolute';
            tmpEl.style.left = '-9999px';
            document.body.appendChild(tmpEl);
            const selected =
                      document.getSelection().rangeCount > 0
                          ? document.getSelection().getRangeAt(0)
                          : false;
            tmpEl.select();
            document.execCommand('copy');
            document.body.removeChild(tmpEl);
            if (selected) {
                document.getSelection().removeAllRanges();
                document.getSelection().addRange(selected);
            }
        });

        let sendSuccess = $('#send-success');
        if (sendSuccess.length) {
            setTimeout(function(){
                sendSuccess.slideUp('slow', function(){
                    sendSuccess.remove();
                })
            }, 10000)
        }
    });
};
