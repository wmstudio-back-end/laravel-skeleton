function showPrice(price){
    return price.toLocaleString('ru-RU', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
}

export default function tariffs() {
    $(document).ready(function(){
        let getDiscount = function(discount){return discount === 'no' ? 0 : discount;},
            originalPricePerMinute = parseInt(window.pricePerMinute),
            pricePerMinute = null,
            currencyChange = $('.currency-change'),
            currencySymbol = $('.currency-symbol'),
            currencies = {}, activeCurrency = null;
        currencyChange.each(function(key, val) {
            let $val = $(val),
                name = $val.data('name'),
                diff = $val.data('diff');
            if (typeof diff === 'string') {
                diff = parseFloat(diff.replace(',', '.'));
            }
            currencies[name] = {
                symbol: $val.data('symbol'),
                diff:   diff,
            };
            if ($val.hasClass('active')) {
                activeCurrency = name;
            }
        });
        let changeCurrency = function(currency) {
            if (currency != null && (currency === activeCurrency || currencies[currency] == null)) {
                return false;
            }
            if (currency != null) {
                activeCurrency = currency;
                $.ajax({
                    url:      '/set-currency',
                    type:     'POST',
                    dataType: 'json',
                    data:     [
                        {
                            name:  '_token',
                            value: token,
                        },
                        {
                            name:  'currency',
                            value: activeCurrency,
                        },
                    ],
                    success:  function(data) {
                        if (data.error) {
                            console.log(data.error);
                        }
                    },
                    error:    console.log
                });
            }
            pricePerMinute = originalPricePerMinute * currencies[activeCurrency].diff;
            currencySymbol.text(currencies[activeCurrency].symbol);
            setAllPrices();

            return true;
        };
        let setPrice = function() {
            let checked = $('input[name="tariff-name"]:checked'),
                selectedMinutesEl = $('#select-minutes'),
                minutesPerLesson = selectedMinutesEl.val(),
                selectedLessonsEl = $('#select-count'),
                countLessons = selectedLessonsEl.val(),
                period = checked.data('period'),
                periodWeeks = parseFloat(checked.data('period-weeks').toString().replace(',', '.')),
                discount = checked.data('discount'),
                groupDiscount = getDiscount($('input[name="group-discount"]:checked').val()),
                promoDiscount = getDiscount($('input#promo-discount').val()),
                weekMinutes = minutesPerLesson * countLessons,
                price = pricePerMinute * weekMinutes * periodWeeks,
                discountPrice = price
                    * ((100 - discount) / 100)
                    * ((100 - groupDiscount) / 100)
                    * ((100 - promoDiscount) / 100);
            $('#originalPrice').text(showPrice(price));
            $('#discount').text(discount);
            if (promoDiscount > 0) {
                $('#discountPromo').text(groupDiscount).parent().show();
            } else {
                $('#discountPromo').parent().hide();
            }
            if (groupDiscount > 0) {
                $('#groupDiscount').text(groupDiscount).parent().show();
            } else {
                $('#groupDiscount').parent().hide();
            }
            $('#selected-type').text($('input[name="group-discount"]:checked').data('res-text'));
            $('#discountPrice').text(showPrice(discountPrice));
            $('#minutesPerLesson').text(selectedMinutesEl.find('option:selected').text());
            $('#countLessons').text(selectedLessonsEl.find('option:selected').text());
            $('#period').text(checked.next().find('.tariff-period').text());
        };
        let setAllPrices = function() {
            let minutesPerLesson = $('#select-minutes').val(),
                countLessons = $('#select-count').val(),
                groupDiscount = getDiscount($('input[name="group-discount"]:checked').val()),
                promoDiscount = getDiscount($('input#promo-discount').val());
            $('input[name="tariff-name"]').each(function(key, val){
                let periodWeeks = parseFloat($(val).data('period-weeks').toString().replace(',', '.')),
                    discount = $(val).data('discount'),
                    weekMinutes = minutesPerLesson * countLessons,
                    price = pricePerMinute * weekMinutes * periodWeeks
                        * ((100 - groupDiscount) / 100)
                        * ((100 - promoDiscount) / 100),
                    discountPrice = price
                        * ((100 - discount) / 100);
                $(val).next().find('.originalPrice').text(showPrice(price));
                $(val).next().find('.discountPrice').text(showPrice(discountPrice));
            });
            setPrice();
        };
        $('input[name="tariff-name"]').change(setPrice);
        $('input[name="group-discount"], #select-minutes, #select-count').change(setAllPrices);
        currencyChange.click(function(){
            let $this = $(this),
                currency = $this.data('name');
            if (changeCurrency(currency)) {
                currencyChange.removeClass('active');
                $this.addClass('active');
            }
        });
        changeCurrency();
    });
};