import {render} from "./modules/reservationChange";
import {findReservations} from "./modules/findReservations";

export default function reservation() {
    $(document).ready(function(){
        let modal = $('#reservation-modal-teacher');
        $('#reservations').remove();
        if (modal.length) {
            let datePicker = modal.find('#reservationCalendar'),
                timeBlock = modal.find('#reserved-times'),
                addTimeBlock = modal.find('#add-reservations');
            render(datePicker, timeBlock, addTimeBlock);
        }

        let searchDatePickerValues = $('input#date-interval'),
            searchDatePickerInput = $('input#dateInterval'),
            searchDatePicker = null,
            searchDates = [],
            dateSeparator = ' - ';
        searchDatePickerValues.click(function(){
            $(this).addClass('active');
            $(this).next().addClass('active');
        });
        searchDatePicker = searchDatePickerInput.datepicker({
            minDate: new Date(),
            inline: true,
            timepicker: true,
            multipleDates: 2,
            multipleDatesSeparator: dateSeparator,
            minutesStep: 5,
            onSelect: function onSelect(text, dates) {
                if (dates.length === 2) {
                    if (dates[0] > dates[1]) {
                        text = text.split(dateSeparator);
                        text = text[1] + dateSeparator + text[0];
                    }
                } else if (dates.length === 1) {
                    text = dates[0].format('d.m.y');
                }
                searchDatePickerInput.val(text);
            },
        }).data('datepicker');
        findReservations(searchDatePicker);

        let hideDatePicker = function(){
            searchDatePickerValues.removeClass('active').next().removeClass('active');
        };
        $('#setDate').click(function(){
            let val = searchDatePickerInput.val();
            if (val) {
                searchDates = val.split(dateSeparator);
                searchDatePickerValues.val(val).trigger('change');
            } else {
                $('#clearDate').click();
            }
            hideDatePicker();
        });
        $('#clearDate').click(function(){
            searchDatePicker.clear();
            searchDatePickerValues.val('').trigger('change');
            hideDatePicker();
        });

        $(document).mouseup(function (e){
            let next = searchDatePickerValues.next();
            if (!next.is(e.target) // если клик был не по нашему блоку
                && next.has(e.target).length === 0) { // и не по его дочерним элементам
                hideDatePicker(); // скрываем его
            }
        });
    });
};