export default function paymentError() {
    $(document).ready(function(){
        let timer = $('#redirect-seconds'),
            redirect = $('#redirect-to');
        if (timer.length === 1 && redirect.length === 1) {
            let seconds = parseInt(timer.text());
            redirect = redirect.attr('href');
            if (seconds < 10) {
                seconds = 10;
            }
            let timeInterval = setInterval(function(){
                seconds--;
                timer.text(seconds);
                if (seconds < 1) {
                    clearInterval(timeInterval);
                    location.href = redirect;
                }
            }, 1000);
        }
    });
};