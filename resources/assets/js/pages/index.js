import landing from './landing';
import findStudents from './findStudents';
import findTeachers from './findTeachers';
import tariffs from './tariffs';
import questions from './questions';
import account from './account';
import profile from './profile';
import tests from './tests';
import topicsStudent from './topicsStudent';
import topicsTeacher from './topicsTeacher';
import teacherCreatedTopics from './teacherCreatedTopics';
import reservation from './reservation';
import studentCard from './studentCard';
import teacherCard from './teacherCard';
import payments from './payments';
import paymentError from './paymentError';
import freeTime from './freeTime';
// todo удалить тестовые чаты
import testChats from './testChats';

let exports = {
    tariffs,
    questions,
    account,
    profile,
    tests,
    teacherCreatedTopics,
    reservation,
    studentCard,
    teacherCard,
    payments,
    paymentError,
    freeTime,
    testChats,
    options: {
        paths: {
            landing: '',
            findStudents: '',
            findTeachers: '',
            topicsStudent: 'topics',
            topicsTeacher: 'topics',
            teacherCreatedTopics: 'my-topics',
            studentCard: /\/student\/[a-z0-9]+(?:[\-|\_][a-z0-9])*/gi,
            teacherCard: /\/teacher\/[a-z0-9]+(?:[\-|\_][a-z0-9])*/gi,
            paymentError: 'payment/error',
            freeTime: 'free-time',
            testChats: 'test-chats',
        }
    }
};
switch (window.userStatus) {
    case 's':
        exports.findTeachers = findTeachers;
        exports.topicsStudent = topicsStudent;
        break;
    case 't':
        exports.findStudents = findStudents;
        exports.topicsTeacher = topicsTeacher;
        break;
    default:
        exports.landing = landing;
        break;
}

export default exports;
