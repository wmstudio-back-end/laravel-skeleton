export default function profile() {
    $(document).ready(function(){
        $('.select-profile').click(function(){
            $('.select-profile').removeClass('active');
            $(this).addClass('active');
            $('.listTeachers').removeClass('active');
            $('#' + $(this).data('profile')).addClass('active');
        });

        $('.downloadLink.diploma-upload').click(function(){
            $('span#fileName').text('');
           $('input[name="tc_diploma_url"]').val(null).click();
        });

        let hideErrText;
        $('input[name="tc_diploma_url"]').change(function(){
            let accept = $(this).attr('accept').split(',');
            if (this.files.length === 1) {
                let file = this.files[0],
                    fileNameEl = $('span#fileName');
                fileNameEl.removeClass('message error');
                clearTimeout(hideErrText);
                if (accept.indexOf('.' + file.name.split('.').pop()) === -1) {
                    fileNameEl.addClass('message error').text($(this).data('err-format'));
                    $(this).val(null);
                    hideErrText = setTimeout(function(){
                        fileNameEl.text('');
                    }, 1000 * 15);
                } else {
                    fileNameEl.text(file.name);
                }
            }
            console.log(this.files);
        })
    });

    window.onload = function(){
        if ($('#profile-student .is-invalid, #profile-student .message.error').length === 0
            && $('#profile-teacher .is-invalid, #profile-teacher .message.error').length > 0) {
            $('[data-profile="profile-teacher"]').click();
        }
        let scrollTo = $('form.was-validated :invalid, form .is-invalid, form .message.error'),
            found = false, i = 0;
        while(!found && i < scrollTo.length) {
            if ($(scrollTo[i]).is(':visible')) {
                scrollTo = $(scrollTo[i]);
                found = true;
            } else {
                i++;
            }
        }
        if (!scrollTo.length) {
            scrollTo = $('form .message.success:visible');
            if (scrollTo.length) {
                scrollTo = scrollTo.first().prev();
            }
        }
        if (scrollTo.length) {
            $('html, body').animate({
                scrollTop: ((scrollTo.first().hasClass('error')
                    ? scrollTo.first().parents('.fillAcc').first().offset().top
                    : scrollTo.first().offset().top
                ) - 50)
            }, 1000);
        }
    };
};