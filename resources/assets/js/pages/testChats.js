import {socket} from "../globals";
var USERS        = {}
var activeDialog = null
var isOnline     = false
var connection   = {}
window.socket    = socket
window.FC        = {}
export default function testChats() {
    $(document).ready(function () {
        socket.Video.localVideo.video = document.getElementById('localVideo');

        $('#cancelCall').click(CallUserOff);
        $('#IncomingCall_on').click(IncomingCall_on);
        $('#IncomingCall_Off').click(IncomingCall_Off);
        socket.ready(() => {
            var obs = []
            console.log('-----------', socket.profile.id)
            socket.profile.id == 1 ? obs = [2, 3, 4, 5] : null
            socket.profile.id == 2 ? obs = [1, 3, 4, 5] : null
            socket.profile.id == 3 ? obs = [1, 2, 4, 5] : null
            socket.addObserv(obs).then(data => {
                for (var i = 0; i < data.users.length; i++) {
                    setUserStatusOnline(data.users[i].uid, data.users[i].online)
                }
            })
            socket.observable.on('message', data => {
                console.log("NEW MESSAGE",data)
           switch (data.method){
               case 'RTC/IncomingCall':
                   if (data.data.val==true){
                       $('#IncomingCall_on').attr('dir',data.data.uid)
                       $('#IncomingCall_Off').attr('dir',data.data.uid)
                       $('#IncomingCall_on').show()
                       $('#IncomingCall_Off').show()
                   }else{
                       $('#IncomingCall_on').hide()
                       $('#IncomingCall_Off').hide()
                   }

                   break;
               case 'RTC/answerCall':
                   if (data.data.val==false){
                       $('#cancelCall').hide()
                   }
                   break;
           }
            })

        })


    });
}
function IncomingCall_on() {
    let nextEvent = function(){
        $('#IncomingCall_on').hide();
        $('#IncomingCall_Off').hide();
        socket.answerCall($('#IncomingCall_on').attr('dir'), true);
    };
    socket.Video.getUserMedia().then(function(){
        nextEvent();
    }).catch(function(){
        socket.Video.getUserMedia({audio:true}).then(function(){
            nextEvent();
        }).catch(function(){
            console.log('Can\'t get user media!');
        });
    });
}
function IncomingCall_Off() {
    $('#IncomingCall_on').hide()
    $('#IncomingCall_Off').hide()
    socket.answerCall($('#IncomingCall_on').attr('dir'), false)
}
function CallUserOn() {
    $('#cancelCall').show()
    socket.CallUser(activeDialog, false)
}
function CallUserOff() {
    $('#cancelCall').hide()
    socket.CallUser(activeDialog, true)
}
function setUserStatusOnline(uid, status) {
    if (!USERS[uid]) {
        var USER = {
            uid:      uid,
            online:   true,
            elStatus: document.createElement('div')
        }
        USER.elStatus.setAttribute("id", "userId" + uid);
        USER.elStatus.addEventListener("click", function () {
            activeDialog = uid;
            var call = $("<span>").html(activeDialog+"  позвонить ");
            call.click(CallUserOn)
            $('#activeDialog').html(call)
        });
        $(USER.elStatus).html("userId-" + uid)
        USERS[uid] = USER
        if (status) {
            USER.elStatus.setAttribute("class", "uidStatus1");
        }
        if (!status) {
            USER.elStatus.setAttribute("class", "uidStatus0");
        }
        $('#connection_num').append(USER.elStatus)
    } else {
        if (status) {
            USERS[uid].elStatus.setAttribute("class", "uidStatus1");
        }
        if (!status) {
            USERS[uid].elStatus.setAttribute("class", "uidStatus0");
        }

    }
    $('#stopCall').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        socket.CallUser(activeDialog, false);
        socket.Video.pc.close();

    });

}
