export default function tests() {
    $(document).ready(function(){
        let langTests = window.langTests,
            testObj = $('#lang-tests'),
            percentsEl = $('#test-percents'),
            questionNumberEl = $('#question-number'),
            quersionTextEl = $('#question-text'),
            questionAnswersEl = $('#question-answers'),
            questionNotComplete = $('.question-not-complete'),
            nextBtn = $('#question-next'),
            sendResultsEl = $('.send-tests-results'),
            action = testObj.data('action'),
            token = window.token,
            activeTest = parseInt($('.testTabs.active').attr('id').substr(5)),
            activeQuestion = 0,
            answers = {},
            answerChange = function(){
                sendResultsEl.css({opacity: 1});
                nextBtn.removeAttr('disabled').prop('disabled', false);
            },
            setQuestion = function(test, id) {
                if (activeTest !== test) {
                    let activeTestEl = $('#test-' + test);
                    if (activeTestEl.length !== 1 || langTests[test] == null) {
                        return false;
                    }
                    $('.testTabs').removeClass('active');
                    activeTestEl.addClass('active');
                    activeTest = test;
                    activeQuestion = id = 0;
                }
                if (langTests[test].result != null) {
                    quersionTextEl.html(langTests[test].result.text);
                    questionAnswersEl.html('');
                    questionNotComplete.css({opacity: 0});
                    let resultClass = '';
                    if (langTests[test].result.resultType != null && langTests[test].result.resultType !== false) {
                        switch (langTests[test].result.resultType) {
                            case 0:
                                resultClass = 'result-good';
                                break;
                            case 1:
                                resultClass = 'result-normal';
                                break;
                            case 2:
                                resultClass = 'result-bad';
                                break;
                        }
                    }
                    percentsEl.css({width: langTests[test].result.percents + '%'}).removeAttr('class').addClass(resultClass);
                } else if (id != null && langTests[test].questions[id] != null) {
                    let percents = Math.round((langTests[test].answeredQuestions / langTests[test].countQuestions) * 100);
                    percentsEl.css({width: percents + '%'});
                    questionNumberEl.text(langTests[test].answeredQuestions + 1);
                    quersionTextEl.html(langTests[test].questions[id].question);
                    questionAnswersEl.html('');
                    $.each(langTests[test].questions[id].answers, function(key, val){
                        questionAnswersEl.append($(
                            '<div class="testChoice-wrap">\n' +
                            '  <div class="dialectLang-list">\n' +
                            '    <input\n' +
                            '      type="radio"\n' +
                            '      id="question-answer-' + val.id + '"\n' +
                            '      name="question-answer"\n'+
                            '      value="' + val.id + '"\n' +
                            '    >\n' +
                            '    <label\n' +
                            '      for="question-answer-' + val.id + '"\n' +
                            '      class="dialectLangRadio"\n' +
                            '    >' + val.value + '</label>\n' +
                            '  </div>\n' +
                            '</div>'
                        ));
                        questionNotComplete.css({opacity: 1});
                        percentsEl.removeAttr('class');
                        nextBtn.attr('disabled', 'disabled').prop('disabled', true);
                        $('#question-answers input[name="question-answer"]').change(answerChange);
                    });
                }
            },
            serializeAnswers = function(){
                let serializeData = [
                    {
                        name: '_token',
                        value: token,
                    }
                ];
                for (let test in answers) {
                    for (let question in answers[test]) {
                        serializeData.push({
                            name: 'test[' + test + '][' + question + ']',
                            value: answers[test][question],
                        });
                    }
                }

                return serializeData;
            },
            nextQuestion = function() {
                let nextId = activeQuestion + 1;
                if (langTests[activeTest].questions[nextId] != null) {
                    activeQuestion = nextId;
                    langTests[activeTest].answeredQuestions++;
                    setQuestion(activeTest, activeQuestion);
                } else {
                    $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'json',
                        data: serializeAnswers(),
                        success: function (data) {
                            for (let id in data) {
                                id = parseInt(id);
                                if (langTests[id] != null) {
                                    percentsEl.css({width: data[id].percents + '%'});
                                    let resultClass = '';
                                    if (data[id].resultType != null && data[id].resultType !== false) {
                                        switch (data[id].resultType) {
                                            case 0:
                                                resultClass = 'result-good';
                                                break;
                                            case 1:
                                                resultClass = 'result-normal';
                                                break;
                                            case 2:
                                                resultClass = 'result-bad';
                                                break;
                                        }
                                    }
                                    percentsEl.removeAttr('class').addClass(resultClass);
                                    langTests[id].result = data[id];
                                    if (id === activeTest) {
                                        setQuestion(id);
                                    }
                                }
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            };
        $('#question-data').remove();
        for (let testId in langTests) {
            answers[testId] = {};
        }
        $('#question-skip').click(function(){
            answers[activeTest][langTests[activeTest].questions[activeQuestion].id] = null;
            nextQuestion();
        });
        nextBtn.click(function(){
            let checked = $('#question-answers input:checked');
            if (checked.length === 1) {
                answers[activeTest][langTests[activeTest].questions[activeQuestion].id] = checked.val();
                nextQuestion();
            }
        });
        $('#question-answers input[name="question-answer"]').change(answerChange);
        $('[data-test-id]').click(function(){
            let id = $(this).data('test-id');
            if (id != null) {
                setQuestion(id);
            }
        });
        $('#send-tests-results').click(function(){
            let action = $(this).data('action');
            if (action != null) {
                $.ajax({
                    url: action,
                    type: 'POST',
                    dataType: 'json',
                    data: serializeAnswers(),
                    success: function (data) {
                        if (data.result === true) {
                            sendResultsEl.css({opacity: 0});
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        });
    });
};
