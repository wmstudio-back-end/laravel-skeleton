import {tagCheck} from './modules/findTopics';
import {renderCard} from "./modules/user";
import {numEnding, ratingInit} from "../modules/helpers";

export default function topicsStudent() {
    $(document).ready(function(){
        let availableTime = window.availableTime,
            template = $($('#topic-card').remove().html()),
            modal = $('#topic-modal'),
            minutesEndings = {
                one: modal.find('#topic-modal-minutes').data('minutes-one'),
                two: modal.find('#topic-modal-minutes').data('minutes-two'),
                five: modal.find('#topic-modal-minutes').data('minutes-five'),
            },
            page = 0, lastPage = null,
            container = modal.find('#topic-modal-user-cards-container'),
            action = container.data('action'),
            token = window.token,
            findBtn = modal.find('#modal-topic-find-more'),
            userCard = $($('#user-card').remove().html()),
            topicId,
            chatsEndings = {
                one: userCard.find('#chats-ending.hidden').data('one'),
                two: userCard.find('#chats-ending.hidden').data('two'),
                five: userCard.find('#chats-ending.hidden').data('five'),
            },
            scheduleStatuses = {
                free: userCard.find('#schedule-status').data('free'),
                busy: userCard.find('#schedule-status').data('busy'),
                willBusy: userCard.find('#schedule-status').data('will-busy'),
            };
        modal.find('#topic-modal-minutes').removeAttr('id')
            .removeAttr('data-minutes-one').removeAttr('data-minutes-two').removeAttr('data-minutes-five');
        userCard.find('#chats-ending.hidden').remove();
        userCard.find('#schedule-status').removeAttr('data-free').removeAttr('data-busy').removeAttr('data-will-busy');
        let findMore = function(){
            if ((typeof topicId === 'number' && Number.isInteger(topicId)) && page !== lastPage) {
                $.ajax({
                    url: action,
                    type: 'POST',
                    dataType: 'json',
                    data: [
                        {
                            name: '_token',
                            value: token,
                        },
                        {
                            name: 'page',
                            value: page + 1,
                        },
                        {
                            name: 'topic',
                            value: topicId,
                        },
                    ],
                    success: function (data) {
                        page = data.current;
                        lastPage = data.last;
                        for (let i = 0; i < data.users.length; i++) {
                            let card = renderCard(userCard.clone(), data.users[i], chatsEndings, scheduleStatuses);

                            container.append(card);
                            if (lastPage > page) {
                                findBtn.removeClass('hidden').show();
                            } else {
                                findBtn.addClass('hidden').hide();
                            }
                        }

                        ratingInit(container);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        };
        findBtn.click(findMore);

        tagCheck(template, function(){
            $('.btn.discuss[data-id="topic-card-button"]').unbind('click').bind('click', function(){
                if (modal.length) {
                    container.html('');
                    page = 0; lastPage = null;
                    topicId = $(this).parents('.card.topicCard').data('topic-id');
                    topicId = parseInt(topicId);

                    // Количество оставшихся минут
                    let minutes = Math.round(availableTime / 60);
                    modal.find('#modal-topic-free-minutes').text(minutes);
                    modal.find('#modal-topic-minutes-ending').text(
                        numEnding(minutes, minutesEndings.one, minutesEndings.two, minutesEndings.five)
                    );

                    findMore();

                    $('body').addClass('modal-opened');
                    modal.addClass('active');
                }
            });
        });

        $('#topic-modal [data-modal-close], #topic-modal .bookModal-left').click(function(){
            topicId = null;
            modal.find('#modal-topic-free-minutes').text(0);
            modal.find('#modal-topic-minutes-ending').text(minutesEndings.five);
        });
    });
};
