import {tagCheck} from './modules/findTopics';
import {numEnding} from "../modules/helpers";
let summernoteLangs = {
        ar: 'ar-AR',
        en: 'en-US',
        ru: 'ru-RU',
        tr: 'tr-TR',
    },
    summernoteLang = summernoteLangs[window.currLang];

export default function teacherCreatedTopics() {
    $(document).ready(function(){
        let template = $($('#topic-card').remove().html()),
            topicEndings = window.createdTopics,
            groups = window.topicsGroups,
            topicCreated = {
                count: $('#topic-count'),
                ending: $('#topic-count-ending'),
                topic: $('#topic-count-topic'),
            },
            setCountCreated = function(val){
                topicCreated.count.text(val);
                topicCreated.ending.text(numEnding(val, topicEndings.ending.one, topicEndings.ending.two, topicEndings.ending.five));
                topicCreated.topic.text(numEnding(val, topicEndings.topic.one, topicEndings.topic.two, topicEndings.topic.five));
            },
            getGroupByTag = function(tag){
                let result = null;
                for (let id in groups) {
                    if (result === null) {
                        for (let tagId in groups[id]) {
                            if (tag === parseInt(tagId)) {
                                result = id;
                                break;
                            }
                        }
                    } else {
                        break;
                    }
                }
                return result;
            },
            selectGroup = $('#topic-create-group'),
            selectTag = $('#topic-create-tag'),
            selectDifficulty = $('#topic-create-difficulty'),
            inputName = $('#topic-create-name'),
            radioType = $('input[type="radio"][name="topic-type"]'),
            inputLink = $('#topic-create-content-link'),
            textContent = $('#topic-create-content-text'),
            imgImage = $('#topic-create-image'),
            imgFile = $('#topic-create-image-file'),
            imgLabel = imgImage.next(),
            errBlock = $('#topic-create-errors'),
            selectGroupNotSelected = selectTag.children().first().text(),
            selectNoValue = selectGroup.find('option[value=""]').text(),
            mimeTypes = window.mimeTypes,
            openModal = function(topicCard) {
                let edit = (topicCard != null && topicCard.length === 1),
                    action = edit ? 'edit' : 'add';
                $('.bookModal').removeClass('active');
                let modal = $('#topic-modal-create.bookModal');
                if (modal.length) {
                    let topicTag = edit ? topicCard.data('topic-tag') : '',
                        topicGroup = edit ? getGroupByTag(topicTag) : '',
                        topicDifficulty = edit ? topicCard.find('select[data-id="difficulty"]').val() : 1,
                        topicName = edit ? topicCard.find('[data-id="topic-card-name"]').text() : '',
                        topicImage = edit
                            ? topicCard.find('[data-id="topic-card-image"] img').attr('src') : imgImage.data('src'),
                        topicLink = edit ? (
                            (topicCard.find('[data-id="topic-card-image"] a').length === 1)
                                ? topicCard.find('[data-id="topic-card-image"] a').attr('href')
                                : ''
                        ) : '',
                        topicContent = edit ? (
                            (topicCard.find('button[data-modal-id="topic-content-modal"]').length === 1)
                                ? topicCard.find('button[data-modal-id="topic-content-modal"]').data('content')
                                : ''
                        ) : '';

                    if (edit) {
                        modal.find('form').first().append($('<input id="topic-modal-edit-topic-id" type="hidden" name="topic-id" value="'
                            + topicCard.data('topic-id') +'">'));
                    }
                    modal.find('#topic-modal-create-form').attr('action', function(){
                        return $(this).data(action);
                    });
                    modal.find('#topic-modal-create-title').text(function(){
                        return $(this).data(action);
                    });
                    modal.find('#topic-modal-create-subtitle').text(function(){
                        return $(this).data(action);
                    });
                    selectGroup.val(topicGroup).trigger('change').trigger('select2:select');
                    selectTag.val(topicTag).trigger('change');
                    selectDifficulty.val(topicDifficulty).trigger('change');
                    inputName.val(topicName);
                    if (topicLink === '' && topicContent !== '') {
                        radioType.filter('#topic-create-content-type-text').next().click();
                    } else {
                        radioType.filter('#topic-create-content-type-link').next().click();
                    }
                    inputLink.val(topicLink);
                    textContent.val(topicContent).summernote('code', topicContent);
                    if (imgImage.data('src') !== topicImage) {
                        imgImage.data('topic-src', topicImage);
                    }
                    imgImage.attr('src', topicImage);

                    $('body').addClass('modal-opened');
                    modal.addClass('active');
                }
            };
        selectGroup.on('select2:select', function(){
            let selected = $(this).val();
            selectTag.html('');
            if (groups[selected] == null) {
                selectTag.append($('<option value="">' + selectGroupNotSelected + '</option>'));
            } else {
                selectTag.append($('<option value="">' + selectNoValue + '</option>'));
                for (let id in groups[selected]) {
                    selectTag.append($('<option value="' + id + '">' + groups[selected][id] + '</option>'));
                }
            }
        });
        imgFile.change(function(){
            let validMime = false;
            if (mimeTypes != null && mimeTypes instanceof Array) {
                validMime = $.inArray(this.files[0].type, mimeTypes) !== -1
            }
            if (this.files.length === 1 && validMime) {
                let reader = new FileReader();
                let fileName = this.files[0].name;
                reader.readAsDataURL(this.files[0]);
                reader.onload = function () {
                    console.log(reader.result);
                    imgImage.attr('src', reader.result);
                    imgLabel.text(fileName);
                };
                reader.onerror = function (error) {
                    console.log('Error: ', error);
                };
            } else {
                if (imgImage.data('topic-src') != null) {
                    imgImage.attr('src', imgImage.data('topic-src'));
                } else {
                    imgImage.attr('src', imgImage.data('src'));
                }
                if (this.files.length !== 1) {
                    imgLabel.removeClass('error').text(imgLabel.data('empty-text'));
                } else {
                    imgLabel.addClass('error').html(imgLabel.data('wrong-format'));
                    $(this).val(null);
                }
            }
        });

        let contentTabs = $('.smallRow [class^="topic-tab-"]');
        $('.topic-content-type input[type="radio"]').change(function(){
            contentTabs.removeClass('active').children('[name]').each(function(key, val){
                $(val).attr('disabled', 'disabled').prop('disabled', true);
                if ($(val).hasClass('summernote-simple')) {
                    $(val).summernote('disable');
                }
            });
            let content = contentTabs.filter('[data-type="' + $(this).val() + '"]').addClass('active').children('[name]');
            content.removeAttr('disabled').prop('disabled', false);
            if (content.hasClass('summernote-simple')) {
                content.summernote('enable');
            }
        });

        $('#open-modal-create').click(function(){
            openModal(null);
        });
        $(".summernote-simple").each(function(key, val){
            let placeholder = $(val).attr('placeholder');
            if (placeholder == null || placeholder === '') {
                placeholder = '......';
            }
            $(val).summernote({
                lang: summernoteLang,
                placeholder: placeholder,
                minHeight: 150,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['para', ['paragraph']]
                ],
            });
            if ($(val).prop('disabled')) {
                $(val).summernote('disable');
            }
        });

        $('#topic-modal-create [data-modal-close], #topic-modal-create .bookModal-left').click(function(){
            $('#topic-modal-edit-topic-id').remove();
            let modal = $('#topic-modal-create');
            modal.find('#topic-modal-create-form').removeAttr('action').removeClass('was-validated').unValidate();
            modal.find('#topic-modal-create-title').text('');
            modal.find('#topic-modal-create-subtitle').text('');
            selectGroup.val('').trigger('change').trigger('select2:select');
            // selectTag.val(topicTag).trigger('change');
            selectDifficulty.val(1).trigger('change');
            inputName.val('');
            radioType.filter('#topic-create-content-type-link').next().click();
            inputLink.val('');
            textContent.val('').summernote('code', '');
            imgImage.attr('src', imgImage.data('src')).data('topic-src', null);
            imgFile.val('');
            errBlock.html('');
        });

        $('#topic-modal-create-form').submit(function(e){
            if ($(this).hasClass('valid')) {
                if ($('#topic-create-content-type-text').prop('checked')) {
                    textContent.val(textContent.summernote('code'));
                }
            } else {
                e.preventDefault();
                e.stopPropagation();
                let form = $(this),
                    serializeForValidator = function(){
                        let type = radioType.filter(':checked'),
                            file = imgFile.prop('files').length === 1 ? imgFile.prop('files')[0] : null,
                            edit = form.find('#topic-modal-edit-topic-id'),
                            result = [
                                {
                                    name: '_token',
                                    value: form.find('input[type="hidden"][name="_token"]').val(),
                                },
                                {
                                    name: 'edit',
                                    value: edit.length === 1 ? 1 : 0,
                                },
                                {
                                    name: 'topic-tag',
                                    value: selectTag.val(),
                                },
                                {
                                    name: 'topic-name',
                                    value: inputName.val(),
                                },
                            ];
                        if (edit.length === 1) {
                            result.push({
                                name: 'topic-id',
                                value: edit.val(),
                            });
                        }
                        if (type.length === 1) {
                            result.push({
                                name: 'topic-type',
                                value: type.val(),
                            });
                            if (type.attr('id') === 'topic-create-content-type-link') {
                                result.push({
                                    name: 'topic-link',
                                    value: inputLink.val(),
                                });
                            } else {
                                result.push({
                                    name: 'topic-content',
                                    value: textContent.summernote('code'),
                                });
                            }
                        } else {
                            result.push({
                                name: 'topic-type',
                                value: null,
                            });
                        }
                        if (file instanceof File) {
                            result.push({
                                name: 'topic-image-type',
                                value: file.type,
                            });
                            result.push({
                                name: 'topic-image-size',
                                value: file.size,
                            });
                        }

                        return result;
                    };
                if (this.checkValidity()) {
                    $.ajax({
                        url: form.data('validate'),
                        type: 'POST',
                        dataType: 'json',
                        data: serializeForValidator(),
                        success: function (data) {
                            errBlock.html('');
                            if (data.errors === false) {
                                form.addClass('valid').submit();
                            } else {
                                for (let i in data.errors) {
                                    errBlock.append($('<p>' + data.errors[i] + '</p>'));
                                }
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            }
        });

        tagCheck(template, function(){
            $('.btn.discuss[data-id="topic-card-button"]').unbind('click').bind('click', function(){
                openModal($(this).parents('.card.topicCard').first());
            });
            $('.btn.removeBtn[data-id="topic-card-remove-button"]').each(function(key, val){
                $(val).val($(this).parents('.card.topicCard').first().data('topic-id'));
            });
        }, null, true, true);
    });
};