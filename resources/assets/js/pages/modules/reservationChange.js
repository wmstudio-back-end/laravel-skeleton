let timeLine;

export function freeTime(reservationId) {
    let found = false;
    for (let date in timeLine) {
        if (!found) {
            for (let id in timeLine[date]) {
                if (timeLine[date][id].id === reservationId && timeLine[date][id].user != null) {
                    timeLine[date][id].user = null;
                    found = true;
                    break;
                }
            }
        }
    }

    return found;
}

export function render(datePicker, timeBlock, addTimeBlock) {
    let addReservationsError = $('.append-DataTimePiker p.add-reservations-error'),
        selectedDateEl = timeBlock.find('#selected-date'),
        timeContainer = timeBlock.find('#time-element-container'),
        timeEl = timeContainer.find('#time-element').remove().clone().removeAttr('id'),
        timeElBusyClass = timeEl.data('busy-class'),
        statusElStatuses = {
            busy: timeEl.find('[data-id="status"]').data('busy'),
            free: timeEl.find('[data-id="status"]').data('free'),
        },
        timeElEmpty = timeContainer.find('#empty-time-element').remove().clone().removeAttr('id'),
        dateInit = datePicker.data('value') || datePicker.val(),
        parentModal = datePicker.parents('.bookModal');
    addReservationsError.click(function(){
        $(this).removeClass('active');
    });
    timeLine = window.reservationTimeLine;
    timeEl.removeAttr('data-busy-class').find('[data-id="status"]').removeAttr('data-busy').removeAttr('data-free');
    let addTime = function(){
        let date = addTimeBlock.find('#add-time-date').text(),
            serialize = [
            {
                name: '_token',
                value: window.token,
            },
            {
                name: 'date',
                value: date
                    + ' ' + addTimeBlock.find('select[name="add-hour"]').val()
                    + ':' + addTimeBlock.find('select[name="add-minute"]').val(),
            },
            {
                name: 'duration',
                value: addTimeBlock.find('select[name="add-duration"]').val(),
            },
        ];
        $.ajax({
            url: addTimeBlock.find('#add-time-btn').data('action'),
            type: 'POST',
            dataType: 'json',
            data: serialize,
            success: function (data) {
                if (data.error == null) {
                    timeLine[date] = data;
                    selectDate(date);
                } else {
                    addReservationsError.text(data.error).addClass('active');
                    setTimeout(function(){
                        addReservationsError.text('').removeClass('active');
                    }, 15 * 1000);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    };
    let openAddTime = function(){
        if (addTimeBlock != null) {
            if (datePicker.val() !== '') {
                addTimeBlock.find('#add-time-date').text(datePicker.val());
            }
            addTimeBlock.find('#add-time-close-btn').show();
            addTimeBlock.find('#reservation-add-time').show();
            addTimeBlock.find('#reservation-add-info').hide();
            addTimeBlock.find('#add-time-btn').unbind('click').bind('click', addTime);
        }
    };
    let closeAddTime = function(){
        if (addTimeBlock != null) {
            addTimeBlock.find('#add-time-close-btn').hide();
            addTimeBlock.find('#reservation-add-time').hide();
            addTimeBlock.find('#reservation-add-info').show();
            addTimeBlock.find('#add-time-btn').unbind('click').bind('click', openAddTime);
        }
    };
    if (addTimeBlock != null) {
        addTimeBlock.find('#add-time-btn').click(openAddTime);
        addTimeBlock.find('#add-time-close-btn').click(closeAddTime);
        let highLightEl = datePicker.find('.datepicker').first();
        highLightEl.addClass('highlight');
        addTimeBlock.find('#add-time-date').click(function(){
            highLightEl.toggleClass('highlight-active');
            setTimeout(function(){
                highLightEl.removeClass('highlight-active');
            }, 500);
        })
    }
    let selectDate = function(date){
        selectedDateEl.text(date);
        if (addTimeBlock != null) {
            addTimeBlock.find('#add-time-date').text(date);
        }
        timeContainer.html('');
        if (timeLine[date] == null) {
            timeContainer.append(timeElEmpty.clone());
        } else {
            for (let i in timeLine[date]) {
                let el = timeEl.clone();
                el.attr('data-reservation-id', timeLine[date][i].id)
                    .data('reservation-id', timeLine[date][i].id)
                    .data('date', date);
                el.find('[data-id="from"]').text(timeLine[date][i].from);
                el.find('[data-id="to"]').text(timeLine[date][i].to);
                let statusEl = el.find('[data-id="status"]'),
                    abortBtn = el.find('[data-id="abort-btn"]'),
                    abortTexsts = {
                        reserve: abortBtn.data('reserve'),
                        abort: abortBtn.data('abort'),
                    };
                abortBtn.removeAttr('data-reserve').removeAttr('data-abort');
                if (timeLine[date][i].user != null) {
                    el.addClass(timeElBusyClass);
                    statusEl.text(statusElStatuses.busy);
                    if (window.userStatus === 's') {
                        abortBtn.text(abortTexsts.abort);
                    }
                } else {
                    statusEl.text(statusElStatuses.free);
                    if (window.userStatus === 's') {
                        abortBtn.text(abortTexsts.reserve);
                    }
                }

                timeContainer.append(el);
            }
            let abortBtns = timeContainer.find('[data-id="abort-btn"]');
            if (abortBtns.length) {
                abortBtns.unbind('click').bind('click', function(){
                    let card = $(this).parents('.bookingTime'),
                        reservationId = card.data('reservation-id'),
                        date = card.data('date'),
                        token = window.token;
                    $.ajax({
                        url: $(this).data('action'),
                        type: 'POST',
                        dataType: 'json',
                        data: [
                            {
                                name: '_token',
                                value: token,
                            },
                            {
                                name: 'reservation-id',
                                value: reservationId,
                            },
                        ],
                        success: function (data) {
                            timeLine[date] = data;
                            selectDate(date);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });
            }
        }
    };

    if (parentModal.length) {
        parentModal.on('modal:open', function(){
            selectDate(datePicker.val())
        });
    }
    if (dateInit === '') {
        dateInit = new Date();
    } else {
        dateInit = new Date(dateInit)
    }
    datePicker.datepicker({
        inline: true,
        minDate: new Date(),
        onSelect: function(e, date){
            selectDate(date.format('d.m.y'));
        },
        onRenderCell: function (date, cellType){
            if (cellType === 'day' && timeLine[date.format('d.m.y')] != null) {
                return {
                    html: date.getDate() + '<span class="dp-note"></span>'
                }
            }
        },
    }).data('datepicker').selectDate(dateInit);
    if (datePicker.val() !== '' && typeof datePicker.val() === 'string') {
        selectDate(datePicker.val());
    }
}
