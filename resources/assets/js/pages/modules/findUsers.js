import {renderCard} from './user';
import {ratingInit} from "../../modules/helpers";
import {setWatch} from "../../modules/statusMonitor";

export default function findUsers() {
    $(document).ready(function(){
        let page = 0, lastPage = null,
            findFavorites = false, findOnline = false,
            container = $('#card-container'),
            findBtn = $('#find-more'),
            datePickerValues = $('input#dateInterval'),
            datePickerInput = $('input#dates'),
            datePicker = null,
            dateSeparator = ' - ',
            dates = [],
            totalUsers = $('#found-counter'),
            action = container.data('action'),
            token = window.token,
            template = $($('#user-card').remove().html()),
            chatsEndings = {
                one: template.find('#chats-ending.hidden').data('one'),
                two: template.find('#chats-ending.hidden').data('two'),
                five: template.find('#chats-ending.hidden').data('five'),
            },
            scheduleStatuses = {
                free: template.find('#schedule-status').data('free'),
                busy: template.find('#schedule-status').data('busy'),
                willBusy: template.find('#schedule-status').data('will-busy'),
            };
        template.find('#chats-ending.hidden').remove();
        template.find('#schedule-status').removeAttr('data-free').removeAttr('data-busy').removeAttr('data-will-busy');
        let serializeFilter = function(){
            let result = [
                {
                    name: '_token',
                    value: token,
                },
                {
                    name: 'page',
                    value: page + 1,
                },
            ];
            if (findFavorites) {
                result.push({
                    name: 'favorites',
                    value: true,
                });
            } else if (findOnline) {
                result.push({
                    name: 'online',
                    value: true,
                });
            }
            $('input.filter-param:checked').each(function(key, val){
                result.push({
                    name: $(val).attr('name'),
                    value: true,
                });
            });
            let search = $('#search').val();
            if (search) {
                result.push({
                    name: 'search',
                    value: search,
                });
            }
            if (datePickerInput.length) {
                if (dates.length) {
                    result.push({
                        name: 'date-from',
                        value: dates[0],
                    });
                    if (dates.length === 2) {
                        result.push({
                            name: 'date-to',
                            value: dates[1],
                        });
                    }
                }
            }

            return result;
        };
        let findMore = function(clearContainer = false){
            if (page !== lastPage) {
                $.ajax({
                    url: action,
                    type: 'POST',
                    dataType: 'json',
                    data: serializeFilter(),
                    success: function (data) {
                        page = data.current;
                        lastPage = data.last;
                        totalUsers.text(data.totalUsers);
                        if (clearContainer === true) {
                            container.html('');
                        }
                        for (let i = 0; i < data.users.length; i++) {
                            container.append(
                                renderCard(template.clone(), data.users[i], chatsEndings, scheduleStatuses)
                            );
                            if (lastPage > page) {
                                findBtn.removeClass('hidden').show();
                            } else {
                                findBtn.addClass('hidden').hide();
                            }
                        }

                        ratingInit(container);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            }
        };

        findMore();
        findBtn.click(findMore);

        $('.clear-filter').click(function(){
            $(this).parents('.btn2field').find('input.filter-param:checked').click();
        });

        let search = function(){
            page = 0; lastPage = null;
            let filterCount = $(this).parents('.btn2').find('input:checked').length;
            $(this).parents('.btn2field').next().text(filterCount ? filterCount : '');
            searchInput.attr('placeholder', searchInput.val() || nullSearchVal);
            findMore(true);
        };
        let searchInput = $('#search'),
            nullSearchVal = searchInput.attr('placeholder'),
            clearSearchBtn = $('#clearSearchBtn');
        $('#searchBtn').click(search);
        clearSearchBtn.click(function(){
            $(this).addClass('hidden');
            searchInput.val('');
            search();
        });
        searchInput.keypress(function(e){
            if(e.which === 13){
                e.preventDefault();
                e.stopPropagation();
                search();
            }
        }).keyup(function(){
            if (this.value.length && clearSearchBtn.hasClass('hidden')) {
                clearSearchBtn.removeClass('hidden');
            } else if (!this.value.length && !clearSearchBtn.hasClass('hidden')) {
                clearSearchBtn.addClass('hidden');
            }
        });
        $('input.filter-param').change(search);
        datePicker = datePickerInput.datepicker({
            minDate: new Date(),
            inline: true,
            timepicker: true,
            multipleDates: 2,
            multipleDatesSeparator: dateSeparator,
            minutesStep: 5,
            onSelect: function onSelect(text, dates) {
                if (dates[0] > dates[1]) {
                    text = text.split(dateSeparator);
                    text = text[1] + dateSeparator + text[0];
                }
                datePickerInput.val(text);
            },
        }).data('datepicker');
        $('#setDate').click(function(){
            let val = datePickerInput.val();
            if (val) {
                dates = val.split(dateSeparator);
                datePickerValues.val(val);
                $(this).parents('.datePicker-field').next().text(dates.length);
            } else {
                $('#clearDate').click();
            }
        });
        $('#clearDate').click(function(){
            datePicker.clear();
            datePickerValues.val('');
            $(this).parents('.datePicker-field').next().text('');
        });

        $('input[name="search-type"]').change(function(){
            switch(this.value) {
                case 'online':
                    findOnline = true;
                    findFavorites = false;
                    break;
                case 'favorites':
                    findOnline = false;
                    findFavorites = true;
                    break;
                default:
                    findOnline = false;
                    findFavorites = false;
                    break;
            }
            setWatch(false);
            page = 0; lastPage = null;
            findMore(true);
        });
    });
};
