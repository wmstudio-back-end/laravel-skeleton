import {ratingInit} from '../../modules/helpers'

export function tagCheck(template, onCardBtnClick, prefix, searchAll, canTagUncheck) {
    if (prefix == null) {
        prefix = '';
    }
    let page = 0, lastPage = null,
        container = $('#' + prefix + 'card-container'),
        findBtn = $('#' + prefix + 'find-more'),
        action = container.data('action'),
        token = window.token,
        groups = window.topicsGroups,
        tags = $('#' + prefix + 'topic-tags'),
        selectedTag = null;
    $('#topics-groups-ang-tags').remove();
    let serializeFilter = function(){
        let topicTag = $('input[name="' + prefix + 'topic-group-tag"]:checked'),
            result = [
            {
                name: '_token',
                value: token,
            },
            {
                name: 'page',
                value: page + 1,
            },
            {
                name: 'tag',
                value: topicTag.length === 1 ? topicTag.val() : null,
            },
        ];
        if (prefix !== '') {
            result.push({
                name: 'onlySelected',
                value: true,
            });
        } else if (window.userStatus === 't' && prefix === '' && $('input[name="topics-hide-selected"]:checked').length) {
            result.push({
                name: 'hideSelected',
                value: true,
            });
        }

        return result;
    };
    let findMore = function(clearContainer = false){
        if (page !== lastPage) {
            $.ajax({
                url: action,
                type: 'POST',
                dataType: 'json',
                data: serializeFilter(),
                success: function (data) {
                    page = data.current;
                    lastPage = data.last;
                    if (clearContainer === true) {
                        container.html('');
                    }
                    for (let id in data.topics) {
                        let card = template.clone(),
                            button;
                        card.attr('data-topic-id', id).data('topic-id', id).data('topic-tag', data.topics[id].tag);

                        card.find('[data-id="difficulty"] option[value="' + data.topics[id].difficulty + '"]').attr('selected', 'selected');
                        card.find('[data-id="topic-card-name"]').text(data.topics[id].name);
                        if (data.topics[id].link != null) {
                            card.find('[data-id="topic-card-image"]').html(
                                '<a href="' + data.topics[id].link + '" rel="nofollow" target="_blank">' +
                                '  <img src="' + data.topics[id].image + '">' +
                                '</a>'
                            );
                        } else if (data.topics[id].content != null) {
                            button = $(
                                '<button data-modal-id="topic-content-modal">' +
                                '  <img src="' + data.topics[id].image + '">' +
                                '</button>'
                            );
                            button.data('content', data.topics[id].content);
                            card.find('[data-id="topic-card-image"]').html('').append(button);
                        }
                        if (window.userStatus === 't' && window.themeButtonTexts != null) {
                            button = card.find('[data-id="topic-card-button"]');
                            if (data.topics[id].author === true) {
                                button.addClass('author').text(window.themeButtonTexts.author);
                            } else if (data.topics[id].checked === true) {
                                button.addClass('checked').text(window.themeButtonTexts.checked);
                            } else {
                                button.addClass('not-checked').text(window.themeButtonTexts.notChecked);
                            }
                        }

                        container.append(card);
                        if (lastPage > page) {
                            findBtn.removeClass('hidden').show();
                        } else {
                            findBtn.addClass('hidden').hide();
                        }
                        if (typeof onCardBtnClick === 'function') {
                            onCardBtnClick();
                        }
                        container.find('img').bind('error', function(){
                            $(this).attr('src', '/img/topic-no-img.png');
                        })
                    }

                    ratingInit(container);
                    container.find('[data-modal-id="topic-content-modal"]').click(function(e){
                        e.preventDefault();
                        $('.bookModal').removeClass('active');

                        let name = $(this).parents('.card').first().find('[data-id="topic-card-name"]').text(),
                            content = $(this).data('content'),
                            modal = $('#' + $(this).data('modal-id') + '.bookModal');
                        if (modal.length) {
                            modal.find('#modal-topic-name').text(name);
                            modal.find('#modal-topic-container').html(content);
                            modal.find('#modal-topic-container').find('*').each(function(key, val){
                                let style = $(val).attr('style');
                                if (style != null && style.indexOf('text-align: justify') !== -1) {
                                    $(val).css({
                                        'text-align': '',
                                    });
                                }
                                $(val).css({
                                    'font-family': '',
                                });
                                if (parseInt($(val).css('font-size')) >= 24) {
                                    $(val).css({
                                        color: '#30b7ff',
                                        'font-weight': 'bold',
                                    });
                                }
                                style = $(val).attr('style');
                                if (style === '') {
                                    $(val).removeAttr('style');
                                }
                            });
                            $('body').addClass('modal-opened');
                            modal.addClass('active')
                        }
                    });
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    };
    if (groups != null) {
        $('input[name="' + prefix + 'topic-group"]').change(function(){
            let group = $(this).val();
            tags.html('');
            if (groups[group] != null) {
                for (let tag in groups[group]) {
                    tags.append($(
                        '<div class="choiceSubject">\n' +
                        '  <input type="radio" id="' + prefix + 'topic-group-tag-' + tag + '" name="' + prefix + 'topic-group-tag" value="' + tag + '"' +
                        (selectedTag === tag ? ' checked="checked"' : '') + '>\n' +
                        '  <label for="' + prefix + 'topic-group-tag-' + tag + '" class="btn">' + groups[group][tag] + '</label>\n' +
                        '</div>\n'
                    ));
                }
                $('input[name="' + prefix + 'topic-group-tag"]').unbind('change').bind('change', function(){
                    page = 0; lastPage = null; selectedTag = $(this).val();
                    findMore(true);
                });
                if (canTagUncheck === true) {
                    $('label[for^="topic-group-tag-"]').unbind('click').bind('click', function(e){
                        let input = $('#' + $(this).attr('for'));
                        if (input.prop('checked')) {
                            e.preventDefault();
                            e.stopPropagation();
                            input.prop('checked', false).change();
                        }
                    });
                }
            }
        });
        findBtn.click(findMore);
        if (searchAll === true) {
            findMore();
        }
        if (window.userStatus === 't' && prefix === '') {
            $('#topics-hide-selected').change(function(){
                if ($('input[name="topic-group-tag"]:checked').length) {
                    page = 0; lastPage = null;
                    findMore(true);
                }
            })
        }
    }
}
