import {numEnding} from '../../modules/helpers'
import {renderCard} from "./user";
import {freeTime} from "./reservationChange";

export function findReservations(datePicker) {
    let container = $('#reservation-cards'),
        countReserved = $('#count-reserved'),
        reservedDialogs = {
            one: countReserved.data('one'),
            two: countReserved.data('one'),
            five: countReserved.data('five'),
        },
        action = container.data('action'),
        token = window.token,
        search = $('#reservation-name'),
        template = $($('#user-reservation-card').remove().html()),
        chatsEndings = {
            one: template.find('#chats-ending').data('one'),
            two: template.find('#chats-ending').data('two'),
            five: template.find('#chats-ending').data('five'),
        },
        minuteEndings = {
            one: template.find('[data-id="reservation-duration"]').data('minutes-one'),
            two: template.find('[data-id="reservation-duration"]').data('minutes-two'),
            five: template.find('[data-id="reservation-duration"]').data('minutes-five'),
        };
    countReserved.removeAttr('data-one').removeAttr('data-two').removeAttr('data-five');
    template.find('#chats-ending').remove();
    template.find('[data-id="reservation-duration"]').removeAttr('minutes-one').removeAttr('minutes-two').removeAttr('minutes-five');
    template = {
        date: template.filter('#reservation-date').removeAttr('id'),
        card: template.filter('#reservation-card').removeAttr('id'),
    };
    let serializeFilter = function(){
        let result = [
                {
                    name: '_token',
                    value: token,
                },
            ];
        if (datePicker.selectedDates.length) {
            if (datePicker.selectedDates.length === 1) {
                result.push({
                    name: 'dateFrom',
                    value: datePicker.selectedDates[0].format('d.m.y'),
                });
            } else {
                let dateFrom, dateTo;
                if (datePicker.selectedDates[0] < datePicker.selectedDates[1]) {
                    dateFrom = datePicker.selectedDates[0];
                    dateTo   = datePicker.selectedDates[1];
                } else {
                    dateFrom = datePicker.selectedDates[1];
                    dateTo   = datePicker.selectedDates[0];
                }
                result.push({
                    name: 'dateFrom',
                    value: dateFrom.format('d.m.y h:i'),
                });
                result.push({
                    name: 'dateTo',
                    value: dateTo.format('d.m.y h:i'),
                });
            }

        }
        if (search.val() != null && search.val() !== '') {
            result.push({
                name: 'name',
                value: search.val(),
            });
        }

        return result;
    };
    let filter = function(){
        search.data('val', null);
        $.ajax({
            url: action,
            type: 'POST',
            dataType: 'json',
            data: serializeFilter(),
            success: function (data) {
                container.html('');
                if (data.length) {
                    let lastDate = null;
                    for (let i in data) {
                        let date = new Date(data[i].time),
                            dateEl = '',
                            card = renderCard(template.card.clone(), data[i].reserved, chatsEndings, null, {
                                id: data[i].id,
                                time: data[i].time,
                                duration: data[i].duration,
                            }, minuteEndings);
                        if (lastDate !== date.format('d.m.y')) {
                            lastDate = date.format('d.m.y');
                            dateEl = template.date.clone();
                            dateEl.text(lastDate);
                        }

                        container.append(dateEl).append(card);
                    }

                    $('[data-id="reservation-revoke"]').unbind('click').bind('click', function(){
                        let btn = $(this);
                        console.log(freeTime(btn.data('reservation-id')));
                        $.ajax({
                            url: btn.data('action'),
                            type: 'POST',
                            dataType: 'json',
                            data: [
                                {
                                    name: '_token',
                                    value: window.token,
                                },
                                {
                                    name: 'reservation-id',
                                    value: btn.data('reservation-id'),
                                },
                                {
                                    name: 'only-action',
                                    value: 1,
                                },
                            ],
                            success: function (data) {
                                if (data.error !== false) {
                                    btn.parents('.card.cardBooking').remove();
                                    let reserved = parseInt(countReserved.find('.count').text()) - 1;
                                    countReserved.find('.count').text(reserved);
                                    countReserved.find('.dialog').text(numEnding(reserved, reservedDialogs));
                                } else {
                                    console.log(data);
                                }
                            },
                            error: function (data) {
                                console.log(data);
                            }
                        });
                    });
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    };
    search.change(function(){
        if ($(this).val() !== $(this).data('val')) {
            $(this).data('val', $(this).val());
            filter();
        }
    });
    $('#date-interval').change(function(){
        filter();
    });
    $('.reservation-search-clear').click(function(){
        search.val('').trigger('change');
    });
    $('.reservation-search-change').click(function(){
        search.trigger('change');
    });
    filter();
}
