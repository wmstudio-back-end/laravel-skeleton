import {favoritesInit, numEnding, random} from "../../modules/helpers";
import {bindSendMessageButton, sendMsgClass} from "../../modules/chat";
import {isOnline} from "../../modules/statusMonitor";
import {bindCallToUserButton, callClass} from "../../modules/videoChat";

export function getFullName(user, middleName) {
    let result = '';
    if (user.name != null && user.name !== '') {
        result = user.name;
        if (middleName === true && user.middle_name != null && user.middle_name !== '') {
            result = result + ' ' + user.middle_name;
        }
        if (user.surname != null && user.surname !== '') {
            result = user.surname + ' ' + result;
        }
        return result;
    } else {
        if (user.login != null && user.login !== '') {
            return user.login;
        } else {
            return null;
        }
    }
}

export function renderCard(card, user, chatsEndings, scheduleStatuses, reservation, minuteEndings) {
    if (chatsEndings == null) {
        chatsEndings = {
            one: 'чат',
            two: 'чата',
            five: 'чатов',
        };
    }
    if (scheduleStatuses == null) {
        scheduleStatuses = {
            free: 'Свободен',
            busy: 'Занят',
            willBusy: 'Будет занят через <span>{minutes}</span> минут',
        };
    }
    card.data('id', user.login);

    card.find('[data-id="user-avatar"]').attr('src', user.avatar);

    let link = card.find('a[data-id="user-link"]');
    if (link.length) {
        link.attr('href', link.attr('href').replace('user-id', user.login.toLowerCase()));
    }

    card.find('[data-id="user-name"]').text(getFullName(user));

    card.find('[data-id="user-country"]').text(user.country);

    let dialects = card.find('[data-id="user-dialects"]');
    let prefix = 'st_';
    if (user.status === 't') {
        prefix = 'tc_';
    }

    if (user[prefix + 'dialect'] != null) {
        dialects.text(Object.values(user[prefix + 'dialect']).join(', '));
        if (Object.keys(user[prefix + 'dialect']).length) {
            dialects.parent().removeClass('hidden');
        } else {
            dialects.parent().addClass('hidden');
        }
    }

    let userAboutMe = card.find('[data-id="user-about-me"]');
    if (userAboutMe.length) {
        card.find('[data-id="user-about-me"]').text(user[prefix + 'about_me'].toString().replace("\n", "<br>"));
    }

    if (user.tc_diploma_url != null && user.tc_diploma_url !== '') {
        let diplomaEl = card.find('[data-id="user-diploma-element"]').removeClass('hidden');
        diplomaEl.find('[data-id="user-diploma-name"]').text(function(){
            return $(this).text().replace('{user-name}', getFullName(user));
        });
        diplomaEl.find('[data-id="user-diploma-description"]').attr('href', user.tc_diploma_url);
    }

    let chats = user.chats,
        sign = chats > 10 ? '>' : '';
    if (chats > 1000) {
        chats = Math.floor(chats / 1000) * 1000;
    } else if (chats > 100) {
        chats = Math.floor(chats / 100) * 100;
    } else if (chats > 10) {
        chats = Math.floor(chats / 10) * 10;
    }
    chats = sign + (sign === '' ? '' : ' ') + chats + ' ' + numEnding(chats, chatsEndings);
    card.find('[data-id="user-chats"]').text(chats);

    let scheduleStatus = card.find('#schedule-status');
    if (user.status === 't') {
        if (scheduleStatus.length) {
            let schedule = random(1, 3);
            if (user.busy === true) {
                schedule = scheduleStatuses.busy;
            } else if (user.busy === false) {
                schedule = scheduleStatuses.free;
            } else {
                schedule = scheduleStatuses.willBusy.replace('{minutes}', user.busy);
            }

            scheduleStatus.html(schedule);
        }
    } else {
        scheduleStatus.remove();
    }

    if (window.showRating !== false && user.status === 't' && user.rating != null) {
        card.find('select.rating').data('init', user.rating);
    } else {
        card.find('select.rating').parents('.card-rating').first().remove();
    }

    // reservation, minuteEndings
    if (reservation != null) {
        if (minuteEndings == null) {
            minuteEndings = {
                one: 'минута',
                two: 'минуты',
                five: 'минут',
            };
        }
        card.find('[data-id="reservation-revoke"]').data('reservation-id', reservation.id);

        let reservationTime = card.find('[data-id="reservation-time"]'),
            reservationDuration = card.find('[data-id="reservation-duration"]'),
            date = new Date(reservation.time);
        reservationTime.find('.reservation-date').text(date.format('d.m.y'));
        reservationTime.find('.reservation-time').text(date.format('h:i'));

        reservationDuration.find('.reservation-duration').text(reservation.duration);
        reservationDuration.find('.reservation-minutes').text(numEnding(reservation.duration, minuteEndings));
    }

    let onlineStatus = card.find('[data-id="user-line-status"]'),
        callBtn = card.find('.' + callClass);
    onlineStatus.attr('data-line-status-id', user.id);
    callBtn.attr('data-user-id', user.id);
    isOnline(user.id).then(function(online) {
        if (online) {
            onlineStatus.removeClass('offline').addClass('online');
            callBtn.removeClass('hidden').unbind('click').bind('click', bindCallToUserButton);
        } else {
            onlineStatus.removeClass('online').addClass('offline');
            callBtn.addClass('hidden').unbind('click');
        }
    }).catch(console.log);

    // Отправка сообщений и видео чат
    card.find('.' + sendMsgClass).data('user-id', user.id).click(bindSendMessageButton);

    // Избранные
    let favorites = card.find('.set-favorites');
    if (window.favorites[user.id] === true) {
        favorites.addClass('active');
    }
    favorites.data('user-id', user.id);
    favoritesInit(favorites);

    return card;
}
