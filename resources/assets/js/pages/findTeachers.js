import findUsers from './modules/findUsers';

export default function findTeachers() {
    return findUsers();
};