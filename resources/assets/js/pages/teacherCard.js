import {render} from "./modules/reservationChange";
import studentCard from "./studentCard";

export default function teacherCard() {
    studentCard();
    $(document).ready(function(){
        let datePicker = $('#reservationCalendar'),
            timeBlock = $('#reserved-times');
        render(datePicker, timeBlock, null);

        let youtube = $('#youtube-thumbnail');
        if (youtube.length) {
            let play = youtube.find('#play-video'),
                youtubeID = youtube.data('video-id');
            play.click(function(){
                youtube.parent().html('')
                    .append($(`<iframe width="703" height="470" src="https://www.youtube.com/embed/${youtubeID}"></iframe>`));
            })
        }
    });
};
