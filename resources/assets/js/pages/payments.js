export default function payments() {
    $(document).ready(function(){
        let speed = 'fast',
            details = $('.balanceTable table tbody tr.details');
        $('.balanceTable table tbody tr.info').click(function(){
            let next = $(this).next();
            if (!next.is(':visible')) {
                details.hide(speed, function(){
                    next.show(speed);
                })
            } else {
                next.hide(speed);
            }
        });
    });
};