import {daysInMonth} from "../modules/helpers";

export default function account() {
    $(document).ready(function(){
        // ------ Загрузка фото, обрезка, и помещение в input ------
        let usrCropper,
            imgLoaded = $('#loaded-img'),
            availableMimes = imgLoaded.data('available-formats').split(',').map(function(val){ return 'image/' + val}),
            avatarModal = $('#avatar-modal'),
            chooseImg = $('#choose-img'),
            accountSave = $('#account-save'),
            avatarImg = $('#avatar'),
            avatarInput = $('#photo'),
            chooseImgBtn = $('#choose-img-btn'),
            birthday = {
                day:   $('#birthday-day'),
                month: $('#birthday-month'),
                year:  $('#birthday-year'),
            };
        chooseImgBtn.data('original-text', chooseImgBtn.text());
        avatarImg.data('original-src', avatarImg.attr('src'));
        avatarModal.on('modal:after-close', function(){
            usrCropper.cropper('destroy');
            usrCropper = null;
        });
        chooseImg.change(function(e){
            if (this.files.length === 1 && availableMimes.indexOf(this.files[0].type) !== -1) {
                let file = this.files[0],
                    reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function(evt){
                    if( evt.target.readyState === FileReader.DONE) {
                        chooseImgBtn.text(file.name);
                        imgLoaded[0].src = evt.target.result;

                        usrCropper = imgLoaded.cropper({
                            aspectRatio: 1,
                            viewMode: 2,
                            dragMode: 'move',
                            autoCropArea: 1
                        });
                        avatarModal.modal({
                            clickClose: false,
                        });
                    }
                };
            } else {
                chooseImgBtn.text(chooseImgBtn.data('original-text'));
                avatarImg.attr('src', avatarImg.data('original-src'));
                avatarInput.val(null);
            }
        });
        $('.custom-modal').click(function(e){
            e.preventDefault();
            e.stopPropagation();
        });
        chooseImgBtn.click(function(){
            chooseImg.val(null).trigger('change').click();
        });

        $('#change-img-btn').click(function(){
            if (usrCropper != null) {
                let img = usrCropper.cropper('getCroppedCanvas').toDataURL();
                avatarImg.attr('src', img);
                avatarInput.val(img);
                $('a[rel="modal:close"]').first().click();
            }
        });
        // ------ Загрузка фото, обрезка, и помещение в input ------

        // ------ Удаление привязки соц.сети ------
        $(".boxWeb-close").click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.add-social').removeClass('active').find('input').first().attr('checked','checked').prop('checked', true);
            $('.add-social:not(.active)').unbind('click').bind('click', addSocial);
        });
        // ------ Удаление привязки соц.сети ------

        // ------ Добавление привязки к соц.сетям ------
        let addSocial = function(){
            let removeSocial = $(this).find('input:checked');
            if (removeSocial.length) {
                removeSocial.first().removeAttr('checked').prop('checked', false);
                $(this).addClass('active');
            } else {
                let url = $(this).data('url'),
                    token = $('input[name="_token"]').first().val();
                if (url != null && token != null) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        data: [
                            {
                                name: '_token',
                                value: token,
                            }, {
                                name: 'redirect-to',
                                value: location.href,
                            }
                        ],
                        success: function (data) {
                            if (data.error) {
                                console.log(data.message);
                            } else if (data.redirect != null) {
                                location.href = data.redirect;
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            }
        };
        $('.add-social:not(.active)').click(addSocial);
        // ------ Добавление привязки к соц.сетям ------

        let defaults = {},
            breakEx = {},
            form = $('form#account-form'),
            disabled = form.find('[disabled]');
        disabled.removeAttr('disabled').prop('disabled', false);
        form.serializeArray().forEach(function(val){
            defaults[val.name] = val.value;
        });
        disabled.attr('disabled', 'disabled').prop('disabled', true);
        accountSave.click(function(){
            let hasUpdates = false,
                disabled = form.find('[disabled]');
            disabled.removeAttr('disabled').prop('disabled', false);
            try {
                form.find('input[type="password"]').removeAttr('required');
                form.find('input[type="password"]').each(function (key, val) {
                    if ($(val).val() !== '') {
                        form.find('input[type="password"]').attr('required', 'required');
                        throw breakEx;
                    }
                });
            } catch (e) {
                if (e !== breakEx) throw e;
            }
            try {
                form.serializeArray().forEach(function(val){
                    if (defaults[val.name] == null || defaults[val.name] !== val.value) {
                        hasUpdates = true;
                        throw breakEx;
                    }
                });
            } catch (e) {
                if (e !== breakEx) throw e;
            }
            if (hasUpdates || form.serializeArray().length !== defaults.length) {
                form.submit();
            }
            disabled.attr('disabled', 'disabled').prop('disabled', true);
        });
        form.find('input').keypress(function(e) {
            if(e.which === 13) {
                e.preventDefault();
                e.stopPropagation();
                accountSave.click();
            }
        });

        let selectCity = $('select#city');
        selectCity.select2({
            containerCssClass: 'select2-align-left',
            dropdownCssClass: 'select2-align-left',
            minimumInputLength: $(this).data('minimum-input') != null ? $(this).data('minimum-input') : 3,
            templateSelection: function(data){
                if (data.id === '') {
                    return $('<span class="select2-selection__placeholder">' + data.text + '</span>');
                }

                return $(data.text);
            },
            templateResult: function(data){
                if (data.id === '') {
                    if(!$(data.element).parents('select').hasClass('hide-empty')) {
                        return data.text;
                    }
                } else {
                    if (data.element != null) {
                        return $($(data.element).html());
                    } else {
                        return $(data.text);
                    }
                }
            },
            ajax: {
                url: selectCity.data('action'),
                dataType: 'json',
                delay: 250,
                method: 'post',
                data: function(params) {
                    return [
                        {
                            name: '_token',
                            value: window.token,
                        },
                        {
                            name: 'country',
                            value: $('select[name="country"]').val(),
                        },
                        {
                            name: 'search',
                            value: params.term
                        }
                    ];
                },
                processResults: function (data) {
                    let result = [];
                    if (data.error != null) {
                        result.push({
                            id: '',
                            text: data.error,
                        })
                    } else {
                        for (let key in data) {
                            let val = "<div class=\"user-city\"><b>" + data[key].name + "</b></div><i>" + data[key].region + "</i>";
                            result.push({
                                id: val,
                                text: val,
                            })
                        }
                    }
                    return {
                        results: result
                    };
                },
                cache: true
            }
        });

        let emptyDay = birthday.day.find('option').first(),
            setMonths = function() {
                let oldVal = birthday.day.val();
                birthday.day.html('').append(emptyDay);
                if (birthday.month.val() === '' || birthday.year.val() === '') {
                    birthday.day.prop("disabled", true);
                } else {
                    birthday.day.prop("disabled", false);
                    for (let i = 1; i <= daysInMonth(parseInt(birthday.month.val()), parseInt(birthday.year.val())); i++) {
                        birthday.day.append($(`<option value="${i}"${oldVal == i ? ' selected="selected"' : ''}>${i}</option>`));
                    }
                }
            };
        birthday.month.change(setMonths);
        birthday.year.change(setMonths);
    });

    window.onload = function(){
        let scrollTo = $('form.was-validated :invalid, form .is-invalid');
        if (!scrollTo.length) {
            scrollTo = $('form .message.success');
            if (scrollTo.length) {
                scrollTo = scrollTo.first().prev();
            }
        }
        if (scrollTo.length) {
            $('html, body').animate({
                scrollTop: (scrollTo.first().offset().top -50)
            }, 500);
        }
    };
};
