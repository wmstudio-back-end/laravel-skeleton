export default function landing() {
    $(document).ready(function(){
        // ------ слайдер ------
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            adaptiveHeight: true,
        });
        $('.slider-nav').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            pauseOnHover: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            cssEase: 'linear',
            asNavFor: '.slider-for',
            dots: true,
            focusOnSelect: true,
            centerMode: true,
            infinite: true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 981,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    }
                },
                {
                    breakpoint: 641,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        // ------ слайдер ------

        // ------ переключатель табов (как это работает) ------
        $('#tabs-next').click(function(){
            let nextId = parseInt($('.tabs input:checked').attr('id').substr(3,1)) + 1;
            if (nextId > 4) {
                nextId = 1;
            }
            $('.tabs label[for="tab' + nextId + '"]').click();
        });
        // ------ переключатель табов (как это работает) ------

        // ------ скроллим в начало страницы, если пустой хэш ------
        if(!window.location.hash) {
            window.scrollTo(0,0);
        }
        // ------ скроллим в начало страницы, если пустой хэш ------
    });
};
