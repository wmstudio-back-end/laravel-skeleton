require('slick-carousel/slick/slick.min');
require('jquery.maskedinput/src/jquery.maskedinput');
require('select2/dist/js/select2.full.min');
require('air-datepicker/dist/js/datepicker.min');
require('select2/dist/js/i18n/ar');
require('select2/dist/js/i18n/ru');
require('select2/dist/js/i18n/en');
require('select2/dist/js/i18n/tr');
require('cropperjs/dist/cropper.min');
require('jquery-cropper/dist/jquery-cropper.min');
require('jquery-modal/jquery.modal.min');
require('jquery-bar-rating/dist/jquery.barrating.min');
require('bs4-summernote/dist/summernote-lite');
require('bs4-summernote/dist/lang/summernote-ru-RU');
require('bs4-summernote/dist/lang/summernote-ar-AR');
require('bs4-summernote/dist/lang/summernote-tr-TR');
require("inputmask/dist/inputmask/jquery.inputmask");

import ioConnection from './modules/socket/connetcion';

let socket = null;
if(window.userStatus === 's' || window.userStatus === 't') {
    socket = new ioConnection();
}
export {socket};

function setDefaults() {
    toastr.options = {
        "closeButton":       true,
        "debug":             false,
        "newestOnTop":       false,
        "progressBar":       true,
        "positionClass":     "toast-top-right",
        "preventDuplicates": false,
        "onclick":           null,
        "showDuration":      300,
        "hideDuration":      500,
        "timeOut":           2000,
        "showEasing":        "swing",
        "hideEasing":        "linear",
        "showMethod":        "fadeIn",
        "hideMethod":        "fadeOut"
    };

    $('#lang-and-status').remove();
}

export function init(modules) {
    setDefaults();
    let loadedModules = {},
        logModule     = function(module, element) {
            element = element[0].toUpperCase() + element.substr(1);
            if(loadedModules[module] === undefined) {
                loadedModules[module] = [];
            }
            loadedModules[module].push(element);
        };
    for(let module in modules) {
        let options = {};
        if(modules[module].options != null) {
            options = modules[module].options;
            delete modules[module].options;
        }
        for(let element in modules[module]) {
            if(module === 'Pages') {
                let modulePath = options.paths[element] != null ? options.paths[element] : element;
                let thisPage   = false;
                if(typeof modulePath === 'string') {
                    thisPage = location.pathname === '/' + modulePath;
                } else {
                    thisPage = !!location.pathname.match(modulePath);
                }
                if(thisPage) {
                    modules[module][element]();
                    logModule(module, element);
                }
            } else {
                modules[module][element]();
                logModule(module, element);
            }
        }
    }
    console.log('Loaded Modules:', loadedModules);
}
