let datepickerLanguages = {
    ru: {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
        daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        today: 'Сегодня',
        clear: 'Очистить',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    },
    en: {
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'mm.dd.yyyy',
        timeFormat: 'hh:ii aa',
        firstDay: 0
    },
    ar: {
        days: ["الاثنين", "الخميس", "الخميس", "الأحد", "الثلاثاء", "الجمعة", "الأحد", "الأربعاء", "السبت ", "الاثنين", "الخميس", "السبت "],
        daysShort: ["ن", "خ", "خ", "ح", "ث", "ج", "ح", "ر", "س", "ن", "خ", "س"],
        daysMin: ["ن", "خ", "خ", "ح", "ث", "ج", "ح", "ر", "س", "ن", "خ", "س"],
        months: ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"],
        monthsShort: ["ينا", "فبر", "مار", "أبر", "ماي", "يون", "يول", "أغس", "سبت", "أكت", "نوف", "ديس"],
        today: 'اليوم',
        clear: 'نظيف',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    },
    tr: {
        days: ["Pazartesi", "Perşembe", "Perşembe", "Pazar", "Salı", "Cuma", "Pazar", "Çarşamba", "Cumartesi", "Pazartesi", "Perşembe", "Cumartesi"],
        daysShort: ["Pzt", "Prş", "Prş", "Paz", "Sal", "Cum", "Paz", "Çrş", "Cts", "Pzt", "Prş", "Cts"],
        daysMin: ["Pzt", "Prş", "Prş", "Paz", "Sal", "Cum", "Paz", "Çrş", "Cts", "Pzt", "Prş", "Cts"],
        months: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
        monthsShort: ["Oca", "Şub", "Mar", "Nis", "May", "Haz", "Tem", "Ağu", "Eyl", "Eki", "Kas", "Ara"],
        today: 'Bugün',
        clear: 'Temiz',
        dateFormat: 'dd.mm.yyyy',
        timeFormat: 'hh:ii',
        firstDay: 1
    },
};

Date.prototype.format = function(format){
    if (format.indexOf('d') !== -1) {
        format = format.replace('d', (this.getDate() > 9 ? this.getDate() : '0' + this.getDate()));
    }
    if (format.indexOf('m') !== -1) {
        let month = this.getMonth() + 1;
        format = format.replace('m', (month > 9 ? month : '0' + month));
    }
    if (format.indexOf('y') !== -1) {
        format = format.replace('y', this.getFullYear());
    }

    if (format.indexOf('h') !== -1) {
        format = format.replace('h', (this.getHours() > 9 ? this.getHours() : '0' + this.getHours()));
    }
    if (format.indexOf('i') !== -1) {
        format = format.replace('i', (this.getMinutes() > 9 ? this.getMinutes() : '0' + this.getMinutes()));
    }
    if (format.indexOf('s') !== -1) {
        format = format.replace('s', (this.getSeconds() > 9 ? this.getSeconds() : '0' + this.getSeconds()));
    }

    return format;
};

let lang = window.currLang;
if (datepickerLanguages[lang] != null) {
    $.fn.datepicker.language['ru'] = datepickerLanguages[lang];
    $.fn.select2.defaults.set('language', lang);
}


export default function dates() {
    $(document).ready(function() {
        $('#datePicker_modal').datepicker({
            inline: true,
        });
        $(".dateCalendar").click(function () {
            $("#datePicker_modal").show()
        });

        $('.dataPiker').keypress(function(e){
            e.preventDefault();
            e.stopPropagation();
        }).datepicker();
    });
}