import {socket} from '../globals';
import {isOnline} from "./statusMonitor";

let wrap                                  = $('.notification-wrap'),
    chatsActive                           = wrap.length > 0,
    users                                 = {},
    page                                  = 0,
    lastPage                              = null,
    newMessages                           = 0,
    totalMessages                         = $('#msg-new-messages'),
    userInfoAction                        = $('#msg-user-info').remove().val(),
    token                                 = ($('#msg-token').remove()).val(),
    newMessage                            = $('#new-message'),
    chatList                              = $(".chatsBox"),
    chatListAction                        = chatList.data('action'),
    chatBox                               = $(".dialogBoxChats"),
    messageListAction                     = chatBox.data('action'),
    remoteUserName                        = chatBox.find('.dialogBoxModal-menu [data-id="msg-name"]'),
    chatModal                             = $(".dialogBoxModal"),
    userList                              = chatList.find('.dialogBox-wrap .dialogBox .scroll-container'),
    messageList                           = chatBox.find('.dialogBox-wrap .dialogBox .alignBottom'),
    loading                               = $('.notification-wrap [data-id="msg-loading"]').removeClass('hidden').remove(),
    userListContainer                     = chatList.find('.dialogBox'),
    userTmp                               = userListContainer.find('[data-id="msg-user-template"]').remove(),
    userLinkTmp                           = userTmp.find('[data-id="msg-link"]').attr('href'),
    messagesTmp                           = {
        date:     messageList.find('[data-id="msg-date"]').remove(),
        receiver: messageList.find('[data-id="receiver-container"]').remove(),
        sender:   messageList.find('[data-id="sender-container"]').remove(),
    },
    usersLoading = false, messagesLoading = false,
    sendMsgClass = 'msg-send-message';

export { sendMsgClass  }

$.fn.removeLoading   = function() {
    $(this).find('[data-id="msg-loading"]').remove();
};

let insertUser       = function(userId, prepend) {
    let user = userList.find('[data-user-id="' + userId + '"]');
    if(user.length) {
        if(users[userId].newMessages) {
            user.find('[data-id="msg-new-messages"]')
                .text(users[userId].newMessages).removeClass('hidden');
        } else {
            user.find('[data-id="msg-new-messages"]').text('').addClass('hidden');
        }
        userList.prepend(user);
    } else {
        user = userTmp.clone();
        user.attr('data-user-id', userId);
        user.find('[data-id="msg-link"]').attr(
            'href',
            userLinkTmp.replace(
                '/user-id',
                '/' + users[userId].login.toLowerCase()
            )
        );
        user.find('[data-id="msg-user-avatar"]').attr('src', users[userId].avatar);

        let onlineStatus = user.find('[data-id="user-line-status"]');
        onlineStatus.attr('data-line-status-id', userId);
        isOnline(userId).then(function(online) {
            onlineStatus.removeAttr('class').addClass(online === true ? 'online' : 'offline');
        }).catch(console.log);

        user.find('[data-id="msg-name"]').text(users[userId].name).attr('title', users[userId].name);
        if(users[userId].country != null && users[userId].country !== '') {
            user.find('[data-id="msg-country"]').text(users[userId].country).attr('title', users[userId].country);
        } else {
            user.find('[data-id="msg-country"]').text('---').removeAttr('title');
        }
        if(users[userId].newMessages) {
            user.find('[data-id="msg-new-messages"]').text(users[userId].newMessages).removeClass('hidden');
        } else {
            user.find('[data-id="msg-new-messages"]').text('').addClass('hidden');
        }
        if(prepend === true) {
            userList.prepend(user);
        } else {
            userList.append(user);
        }
        user.bind('click', bindOpenChat);
    }
};

let findMoreUsers    = function() {
    if(!usersLoading && page !== lastPage) {
        usersLoading = true;
        userList.append(loading);
        $.ajax({
            url:      chatListAction,
            type:     'POST',
            dataType: 'json',
            data:     [
                {
                    name:  '_token',
                    value: token,
                },
                {
                    name:  'page',
                    value: page + 1,
                },
            ],
            success:  function(data) {
                page     = data.current;
                lastPage = data.last;
                userList.removeLoading();
                for(let i in data.users) {
                    users[i] = data.users[i];
                    insertUser(i);
                }

                if(wrap.hasClass('hidden')) {
                    wrap.removeClass('hidden');
                }
                usersLoading = false;
            },
            error:    function(data) {
                console.log(data);
                usersLoading = false;
            }
        });
    }
};

let insertMessage    = function(msg, append) {
    append     = append === true;
    let userId = chatBox.data('user-id');
    if(userId != null) {
        let date      = (new Date(msg.created_at)).format('d.m.y'),
            time      = (new Date(msg.created_at)).format('h:i'),
            dataBlock = messageList.find('[data-date-block="' + date + '"]');
        if(dataBlock.length === 0) {
            dataBlock = $('<div data-date-block="' + date + '" class="alignBottom"></div>');
            if(append) {
                messageList.append(messagesTmp.date.clone().text(date)).append(dataBlock);
            } else {
                messageList.prepend(dataBlock).prepend(messagesTmp.date.clone().text(date));
            }
        }
        let message;
        if(msg.sender_id === userId) {
            message = messagesTmp.receiver.clone();
        } else {
            message = messagesTmp.sender.clone();
        }
        message.find('.msg-message').html(msg.message.replace('\n', '<br>'));
        message.find('.msg-time').text(time);

        if(append) {
            dataBlock.append(message);
            messageList.parent()[0].scrollTop = messageList.parent()[0].scrollHeight;
        } else {
            dataBlock.prepend(message);
        }
    }
};

let findMoreMessages = function() {
    let userId = chatBox.data('user-id');
    if(!messagesLoading && users[userId].msgPage !== users[userId].msgLastPage) {
        messagesLoading = true;
        messageList.prepend(loading);
        $.ajax({
            url:      messageListAction.replace('/user-id/', '/' + userId + '/'),
            type:     'POST',
            dataType: 'json',
            data:     [
                {
                    name:  '_token',
                    value: token,
                },
                {
                    name:  'page',
                    value: users[userId].msgPage + 1,
                },
            ],
            success:  function(data) {
                users[userId].msgPage     = data.current;
                users[userId].msgLastPage = data.last;
                let oldHeight             = messageList.height();
                messageList.removeLoading();
                for(let i in data.messages) {
                    insertMessage(data.messages[i]);
                }
                if(users[userId].msgPage === 1) {
                    messageList.parent()[0].scrollTop = messageList.parent()[0].scrollHeight;
                } else {
                    messageList.parent()[0].scrollTop = messageList.parent()[0].scrollHeight - oldHeight;
                }
                messagesLoading = false;
            },
            error:    function(data) {
                console.log(data);
                messagesLoading = false;
            }
        });
    }
};

let openChat = function(userId) {
    newMessages -= users[userId].newMessages;
    totalMessages.text(newMessages);
    if(newMessages) {
        totalMessages.removeClass('hidden');
    } else {
        totalMessages.addClass('hidden');
    }
    users[userId].newMessages = 0;
    userList.find(`[data-user-id="${userId}"] [data-id="msg-new-messages"]`)
        .text(users[userId].newMessages).addClass('hidden');

    remoteUserName.text(users[userId].name);
    let lastId = chatBox.data('user-id');
    if (lastId !== userId) {
        chatBox.addClass('active').data('user-id', userId);
        users[userId].msgPage     = 0;
        users[userId].msgLastPage = null;
        messageList.html('');
    }
    chatList.addClass('active');
    findMoreMessages();
    newMessage.focus();
};

// Open Chat
let bindOpenChat = function() {
    let userId = $(this).data('user-id');
    openChat(userId);
};

let updateUser = function(userId, newMessage, showMessages) {
    if(newMessage === true) {
        users[userId].newMessages++;
        newMessages++;
        totalMessages.text(newMessages);
        if(newMessages) {
            totalMessages.removeClass('hidden');
        } else {
            totalMessages.addClass('hidden');
        }
        insertUser(userId, true);
    } else if (!userList.find('[data-user-id="' + userId+ '"]').length) {
        insertUser(userId, true);
    }
    if (showMessages === true) {
        chatModal.addClass("active");
        openChat(userId);
    }
};

let addNewUser = function(userId, newMessage, showMessages) {
    $.ajax({
        url:      userInfoAction.replace('/user-id/', '/' + userId + '/'),
        type:     'POST',
        dataType: 'json',
        data:     [
            {
                name:  '_token',
                value: token,
            },
        ],
        success:  function(data) {
            users[userId] = data;
            updateUser(userId, newMessage, showMessages);
        },
        error:    function(data) {
            console.log(data);
        }
    });
};

export function bindSendMessageButton() {
    let userId = $(this).data('user-id');
    if (users[userId] != null) {
        updateUser(userId, false, true);
    } else {
        addNewUser(userId, false, true);
    }
}

export default function chat() {
    $(document).ready(function() {
        if (chatsActive) {
            // Socket
            socket.observable.on('message', function(data) {
                if (data.method === 'Message/sendMessage') {
                    data = data.data;
                    if(messageList.is(':visible') && chatBox.data('user-id') === data.sender_id) {
                        insertMessage(data, true);
                        updateUser(data.sender_id);
                    } else {
                        if(users[data.sender_id] != null) {
                            updateUser(data.sender_id, true);
                        } else {
                            addNewUser(data.sender_id, true);
                        }
                    }
                }
            });

            // Close chat
            $(".wrapDialogBox-name").click(function() {
                let userId = chatBox.data('user-id');
                chatBox.removeClass('active').data('user-id', null);
                chatList.removeClass('active');

                users[userId].msgPage     = 0;
                users[userId].msgLastPage = null;
                messageList.html('');
            });

            // Open/close chat box
            $(".notification").click(function() {
                chatModal.toggleClass("active");
            });

            // Close chat box
            $(".dialogBoxClose").click(function() {
                chatModal.removeClass("active");
            });

            // Maximize/minimize chat box
            $('.wrapCollapseClose .collapse').click(function() {
                chatModal.toggleClass('maxi');
            });

            newMessage.keydown(function(e) {
                if(e.keyCode === 13) {
                    if(e.shiftKey) {
                        if(typeof this.selectionStart === "number") {
                            if(this.selectionStart === this.selectionEnd && this.selectionStart === this.value.length) {
                                this.value += '\n';
                                this.scrollTop = this.scrollHeight;
                            } else {
                                let position        = this.selectionStart;
                                this.value          = this.value.substr(0, position) + '\n'
                                    + this.value.substr(this.selectionEnd);
                                this.selectionStart = this.selectionEnd = position + 1;
                                this.blur();
                                this.focus();
                            }
                        }
                    } else if(this.value.trim().length) {
                        let userId = chatBox.data('user-id');
                        if(userId != null) {
                            socket.sendMessage(userId, this.value);
                            let newMessage = {
                                sender_id:   null,
                                receiver_id: userId,
                                message:     this.value,
                                status:      1,
                                created_at:  (new Date()).format('y/m/d h:i:s'),
                            };
                            this.value     = '';
                            insertMessage(newMessage, true);
                            insertUser(userId);
                        }
                    }
                    return false;
                }

                // Set field height
                this.style.height = 'inherit';
                let computed      = window.getComputedStyle(this);
                this.style.height = (parseInt(computed.getPropertyValue('border-top-width'), 10)
                    + parseInt(computed.getPropertyValue('padding-top'), 10)
                    + this.scrollHeight
                    + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                    + parseInt(computed.getPropertyValue('border-bottom-width'), 10) - 60) + 'px';
            }).keyup(function() {
                if(!this.value.length) {
                    this.style.height = '24px';
                }
            });

            findMoreUsers();
            userList.parent().scroll(function() {
                if(($(this).scrollTop() + $(this).height()) === userList.height()) {
                    findMoreUsers();
                }
            });
            messageList.parent().scroll(function() {
                if($(this).scrollTop() === 0) {
                    findMoreMessages();
                }
            });
            $('.' + sendMsgClass).click(bindSendMessageButton);
        }
    });
}
