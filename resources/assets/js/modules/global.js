import {favoritesInit, ratingInit} from './helpers';

export default function global() {
    $(document).ready(function(){
        // ------ hamburger (переключение шапки на мобильную версию) ------
        $(".hamburger").click(function () {
            $(this).toggleClass("is-active");
            $(".menu-h").toggleClass("animated fadeIn");
            $(".right-h").toggleClass("animated fadeIn");
        });
        // ------------------------ hamburger ------------------------

        // ------ переключение списка в таблицу ------
        $(".btnTab").click(function () {
            $('.btnTab').removeClass('active');
            $(this).toggleClass('active');
            $('.many-card').removeClass('active');
        });
        $(".btnTab.sli1-ic").click(function () {
            $('.many-card').addClass('active');
        });
        // ------ переключение списка в таблицу ------

        // ------ перехват события клика на пустую ссылку ------
        $('a[href="#"]').click(function(e){
            e.preventDefault();
            return false;
        });
        // ------ перехват события клика на пустую ссылку ------

        // ------ выпадающий список (стиль преподавания) ------
        $(".teachStyle .btn2").click(function () {
            $(this).addClass('active').find(".btn2field").addClass('active');
        });
        // ------ выпадающий список (стиль преподавания) ------

        // ------ меню преподавателя в модальном окне ------
        $(".infoStudentModal-menuWrap").click(function () {
            $(".infoStudentModal-menu").toggleClass("active")
        });
        // ------ меню преподавателя в модальном окне ------

        // ------ событие клика по веб-документу ------
        $(document).mouseup(function (e){
            let menu = $(".infoStudentModal-menu.active"),
                btn = $(".teachStyle .btn2.active .btn2field.active");
            if (!menu.is(e.target) // если клик был не по нашему блоку
                && menu.has(e.target).length === 0) { // и не по его дочерним элементам
                menu.removeClass('active'); // скрываем его
            }
            if (!btn.is(e.target) // если клик был не по нашему блоку
                && btn.has(e.target).length === 0) { // и не по его дочерним элементам
                btn.removeClass('active').parents(".btn2").first().removeClass('active'); // скрываем его
            }
        });
        // ------ событие клика по веб-документу ------

        // ------ открыть модальное окно ------
        $('[data-modal-id]').click(function(e){
            e.preventDefault();
            $('.bookModal').removeClass('active');
            let modal = $('#' + $(this).data('modal-id') + '.bookModal');
            if (modal.length) {
                $('body').addClass('modal-opened');
                modal.addClass('active');
                modal.trigger('modal:open');
            }
        });
        // ------ открыть модальное окно ------

        // ------ закрыть модальное окно ------
        $('[data-modal-close], .bookModal-left').click(function (e) {
            e.preventDefault();
            $('body').removeClass('modal-opened');
            $(this).parents('.bookModal').removeClass('active').trigger('modal:close');
        });
        // ------ закрыть модальное окно ------

        // ------ видеозвонок ------
        $(".callDialog-open").click(function () {
            $(".callDialog-fon").addClass('active');
            $('body').addClass('modal-opened');
        });
        $(".callDialog-close").click(function () {
            $(".callDialog-fon").removeClass('active');
            $('body').removeClass('modal-opened');
        });
        // ------ видеозвонок ------

        // ------ маска для имен ------
        $(".name-regex").inputmask({
            regex: '^[a-zA-Z][a-zA-Z0-9- \.]*$',
            placeholder: '',
        });
        // ------ маска для имен ------

        // ------ маска для номеров телефонов ------
        $(".phone-code").inputmask("+9{1,3}");
        $(".phone-number").mask("(999) 999-99-99");
        // ------ маска для номеров телефонов ------

        // ------ Показ и скрытие ошибкок ------
        let errHeader = $('.booking-title:has(.error-header)'),
            errMesage = $('.booking-subtitle:has(.error-message)');
        if (errHeader.length) {
            errHeader.css({
                'background-color': '#dc616c',
            });
            setTimeout(function(){
                errHeader.removeAttr('style').find('.error-header').remove();
            }, 1000 * 15);
        }
        if (errMesage.length) {
            errMesage.css({
                'background-color': '#ff898e',
            });
            setTimeout(function(){
                errMesage.removeAttr('style').find('.error-message').remove();
            }, 1000 * (errHeader.length ? 25 : 15));
        }
        // ------ Показ и скрытие ошибкок ------

        // ------ Смена статуса ------
        $('#change-profile-select').change(function(){
            let changeUrl = $('#change-profile-select').find('option:selected').data('change-status');
            if (changeUrl != null) {
                window.location = changeUrl;
            }
        });
        // ------ Смена статуса ------

        // ------ Рейтинг ------
        ratingInit();
        // ------ Рейтинг ------

        // ------ Избранные ------
        favoritesInit();
        // ------ Избранные ------
    });
}
