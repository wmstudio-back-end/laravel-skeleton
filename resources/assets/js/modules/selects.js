$.fn.select2.defaults.set('minimumResultsForSearch', 10);

export default function selects() {
    $(document).ready(function() {
        let templateNoValue = function(data){
            if (data.id === '') {
                return $('<span class="select2-selection__placeholder">' + data.text + '</span>');
            } else {
                return data.text;
            }
        };
        let templateNoResultValue = function(data){
            if (data.id === '') {
                if(!$(data.element).parents('select').hasClass('hide-empty')) {
                    return data.text;
                }
            } else {
                return data.text;
            }
        };

        $('select.select2:not(.custom-init)').each(function(key, val){
            let containerClass = '',
                dropdownClass,
                liveSearch = $(val).data('live-search');
            liveSearch = (liveSearch === false || liveSearch === 0) ? -1 : 10;
            dropdownClass = $(val).data('color');
            if (dropdownClass != null){
                switch (dropdownClass) {
                    case 'green':
                        dropdownClass = 'green-dropdown';
                        break;
                    case 'white':
                        dropdownClass = 'white-dropdown';
                        break;
                    case 'blue':
                        dropdownClass = 'blue-dropdown';
                        break;
                    case 'multi':
                        dropdownClass = 'multi-dropdown';
                        break;
                    default:
                        dropdownClass = '';
                        break;
                }
            }
            if ($(val).data('align') === 'left') {
                containerClass = 'select2-align-left';
                dropdownClass = (dropdownClass === '' ? '' : dropdownClass + ' ') + 'select2-align-left';
            }

            let tmpNoValue = templateNoValue;
            if ($(val).hasClass('select-status')) {
                liveSearch = -1;
                tmpNoValue = function(state) {
                    let optClass = $(state.element).data('class');
                    return $('<span' + (optClass != null ? ' class="' + optClass + '"' : '') + '>'
                        + state.text + '</span>');
                };
            }
            $(val).select2({
                minimumResultsForSearch: liveSearch,
                containerCssClass: containerClass,
                dropdownCssClass: dropdownClass,
                templateSelection: tmpNoValue,
                templateResult: templateNoResultValue,
            });
        });

        $('.select-status').on('select2:select', function(){
            let selected = $(this).next().find('ul.select2-selection__rendered .select2-selection__choice');
            selected.removeClass('with-coma');
            if (selected.length > 1) {
                selected.first().addClass('with-coma');
            }
        }).trigger('select2:select');

            // ------ select language ------
            $("#select-lang select.language").on('select2:select', function(){
                $('#select-lang').find('span.select2-selection__rendered').text($(this).val());
                window.location = '/select-locate/' + $(this).val();
            });
            $('#select-lang').find('span.select2-selection__rendered').text($('#select-lang').find('option[selected]').val());
            $("#select-lang").removeAttr('style');
            // ------ select language ------
    });
}