export default function selects() {
    $(document).ready(function() {
        // ------ Валидация форм ------
        let checkValid = function(e){
            let needFormClass = true,
                val;
            if (e instanceof $.Event) {
                val = this;
            } else {
                val = e;
                needFormClass = false;
            }
            if (!needFormClass || $(val).parents('form').first().hasClass('was-validated')) {
                $(val).removeClass('is-invalid is-valid').addClass('is-' + (val.checkValidity() ? '' : 'in') + 'valid');
                let target;
                if ($(val).data('error-target') != null) {
                    target = $('#' + $(val).data('error-target'));
                } else {
                    target = $(val);
                }
                if (target.length === 1) {
                    if (val.checkValidity() === false) {
                        if (target.next().hasClass('message')) {
                            target.next().addClass('error').text(val.validationMessage);
                        } else {
                            target.after('<p class="message error">' + val.validationMessage + '</p>');
                        }
                    } else {
                        target.removeClass('is-invalid');
                        if (target.next().hasClass('message')) {
                            target.next().hide();
                        }
                    }
                }
            }
            if (needFormClass && !$(val).parents('form').first().hasClass('was-validated')) {
                $(val).unbind('change', checkValid).unbind('keyup', checkValid);
            }
        };
        $.fn.unValidate = function(){
            let result = false;
            if ($(this).prop('tagName') === 'FORM') {
                $(this).find('.is-valid, .is-invalid').removeClass('is-invalid is-valid');
                $(this).find('p.message').each(function (key, val) {
                    if ($(val).hasClass('error')) {
                        $(val).remove();
                    } else {
                        $(val).hide();
                    }
                });
                $(this).find('[name]:required').unbind('change', checkValid).unbind('keyup', checkValid);
                result = true;
            }

            return result;
        };
        $('form').attr('novalidate', 'novalidate').submit(function(e){
            if (this.checkValidity() === false) {
                e.preventDefault();
                e.stopPropagation();

                $(this).find('[name]:required').each(function(key, val){
                    // ------ Валидация подтверждения пароля------
                    // if ($(val).attr('name').indexOf('_confirmation') !== -1) {
                    //     if ($(val).val() !==
                    //         $(val).parents('form').find('[name=' + $(val).attr('name').replace('_confirmation', '') + ']').val()) {
                    //         val.setCustomValidity(' ');
                    //     } else {
                    //         val.setCustomValidity('');
                    //     }
                    // }
                    // ------ Валидация подтверждения пароля------

                    checkValid(val);
                    if (
                        $(val).prop('tagName') === 'SELECT'
                        || ($(val).prop('tagName') === 'INPUT' && $(val).attr('type').toLowerCase() === 'radio')
                        || ($(val).prop('tagName') === 'INPUT' && $(val).attr('type').toLowerCase() === 'checkbox')
                    ) {
                        $(val).unbind('change', checkValid).bind('change', checkValid);
                    } else {
                        $(val).unbind('change', checkValid).unbind('keyup', checkValid)
                            .bind('change', checkValid).bind('keyup', checkValid);
                    }
                });
                $(this).addClass('was-validated');
                let body = $('html, body');
                if (!body.hasClass('modal-opened')) {
                    body.animate({
                        scrollTop: ($(this).find(':invalid, .is-invalid').first().offset().top -50)
                    }, 500);
                }
            }
        });
        $('input[required]').focus(function(){
            if ($(this).parents('form').hasClass('was-validated')
                && $(this).next().hasClass('message')) {
                $(this).next().hide();
            }
        }).focusout(function(){
            if ($(this).next().hasClass('message')
                && this.checkValidity() === false
                && $(this).parents('form').hasClass('was-validated')) {
                $(this).next().show().text(this.validationMessage);
            }
        });
        // ------ Валидация форм ------

        // ------ Кликаем по label если в она фокусе и нажали enter ------
        $('label').keypress(function(e){
            if (e.which === 13) {
                $(this).click();
            }
        });
        // ------ Кликаем по label если в фокусе нажали enter ------

        // ------ AJAX Авторизация (потом и регистрация) ------
        // $('form.ajaxAuth').submit(function(e){
        //     if (!this.checkValidity() === false) {
        //         e.preventDefault();
        //         e.stopPropagation();
        //
        //         $.ajax({
        //             url: $(this).attr('action'),
        //             type: 'POST',
        //             dataType: 'json',
        //             data: $(this).serializeArray(),
        //             success: function (data) {
        //                 if (data.auth && data.redirect != null && data.errors == null) {
        //                     window.location = data.redirect;
        //                 } else if (data.errors != null) {
        //                     $('input[name="' + Object.keys(data.errors)[0] + '"]').parents('form')
        //                         .removeClass('was-validated')
        //                         .find('input').removeClass('is-invalid')
        //                         .find('input[type="password"]').val('');
        //                     $.each(data.errors, function(key, val){
        //                         let input = $('input[name="' + key + '"]'),
        //                             error = val.shift();
        //                         $('input[data-error-id="' + key + '"]').addClass('is-invalid');
        //                         if (input.next().hasClass('message')) {
        //                             input.next().removeClass('success').addClass('error').text(error).show();
        //                         } else {
        //                             input.after('<p class="message error">' + error + '</p>');
        //                         }
        //                     });
        //                 }
        //             },
        //             error: function (data) {
        //                 console.log(data);
        //             }
        //         });
        //     }
        // });
        // ------ AJAX Авторизация (потом и регистрация) ------

        // ------ Передаем реферальную ссылку при регистрации через соц.сети ------
        $('form.social').submit(function(e){
            let referral = $('input#referral');
            if (referral.length && referral.val()) {
                referral = referral.clone();
                referral.attr('type', 'hidden');
                $(this).prepend(referral);
            }
        });
        // ------ Передаем реферальную ссылку при регистрации через соц.сети ------
    });
}