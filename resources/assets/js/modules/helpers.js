export function numEnding(number, one, two, five) {
    let endings = {
        one: '',
        two: '',
        five: '',
    };
    if (typeof one === 'string') {
        endings.one = one;
        endings.two = two;
        endings.five = five;
    } else if (typeof one === 'object' && Array.isArray(one) && one.length === 3) {
        endings.one = one[0];
        endings.two = one[1];
        endings.five = one[2];
    } else if (typeof one === 'object' && !Array.isArray(one) && one.one != null && one.two != null && one.five != null) {
        endings.one = one.one;
        endings.two = one.two;
        endings.five = one.five;
    } else {
        return '';
    }

    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
        return endings.five;
    }
    number %= 10;
    if (number === 1) {
        return endings.one;
    }
    if (number >= 2 && number <= 4) {
        return endings.two;
    }
    return endings.five;
}

export function random(min, max, int = true) {
    let rand = Math.random() * (max - min) + min;
    if (int) {
        rand = Math.round(rand);
    }

    return rand;
}

export function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}

let ratingUpdate = false,
    ratingAction = window.ratingAction,
    token = window.token;
export function ratingInit(parent, readonly) {
    let select = 'select.rating',
        rating = parent != null ? parent.find(select) : $(select);
    rating.each(function(key, val) {
        let customTheme = $(val).data('theme'),
            customReadonly = $(val).data('readonly'),
            customInitial = $(val).data('init'),
            onSelect = function(){};
        if (readonly === false || readonly === true) {
            customReadonly = readonly;
        }
        if (customReadonly == null) {
            customReadonly = false;
        }
        if (customTheme == null) {
            customTheme = 'card_rating';
        }
        if (customInitial == null) {
            customInitial = null;
        } else {
            if (typeof customInitial === 'string' && customInitial.indexOf(',') !== -1) {
                customInitial = customInitial.replace(',', '.');
            }
            onSelect = function(){
                if (!ratingUpdate) {
                    let userId = this.$elem.data('user-id');
                    if (userId != null) {
                        ratingUpdate = true;
                        let el = this.$elem;
                        $.ajax({
                            url:      ratingAction,
                            type:     'POST',
                            dataType: 'json',
                            data:     [
                                {
                                    name:  '_token',
                                    value: token,
                                },
                                {
                                    name:  'userId',
                                    value: userId,
                                },
                                {
                                    name:  'rating',
                                    value: el.val(),
                                },
                            ],
                            error: function(data) {
                                console.log(data);
                            },
                            complete: function() {
                                ratingUpdate = false;
                            }
                        });
                    }
                }
            };
        }
        $(val).barrating({
            theme: customTheme,
            onSelect: onSelect,
            initialRating: customInitial,
            readonly: customReadonly,
        });
        if (customInitial != null) {
            $(val).next().find('.br-current-rating').text(customInitial);
            $(val).next().mouseleave(function(){
                $(this).find('.br-current-rating').text(customInitial);
            });
        }
    });
}

let favoritesUpdate = false,
    favoritesAction = window.favoritesAction,
    favoritesCount = $('#favorites-count'),
    bindFavorites = function() {
        if (!favoritesUpdate) {
            let userId = $(this).data('user-id');
            if (userId != null) {
                favoritesUpdate = true;
                let el = $(this);
                $.ajax({
                    url:      favoritesAction,
                    type:     'POST',
                    dataType: 'json',
                    data:     [
                        {
                            name:  '_token',
                            value: token,
                        },
                        {
                            name:  'userId',
                            value: userId,
                        },
                    ],
                    success: function(data) {
                        if (data.result === true) {
                            el.addClass('active');
                        } else {
                            el.removeClass('active');
                        }
                        if (favoritesCount.length) {
                            favoritesCount.text(data.count || '');
                        }
                    },
                    error: function(data) {
                        console.log(data);
                    },
                    complete: function() {
                        favoritesUpdate = false;
                    }
                });
            }
        }
    };
export function favoritesInit(el) {
    if (el == null) {
        el = $('.set-favorites');
    } else if (!el.hasClass('set-favorites')) {
        el = el.find('.set-favorites');
    }
    if (el.length) {
        el.unbind('click', bindFavorites).bind('click', bindFavorites);
    }
}

export function jsonClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}
