import io from 'socket.io-client/dist/socket.io';
import Call from "./call";
import VideoStream from "./videoStream";

let EventEmitter = require('events').EventEmitter;
let call = null;

class ioConnection {
    constructor() {
        this.protocol   = process.env.MIX_NODE_PROTOCOL || 'https';
        this.host       = process.env.MIX_NODE_HOST || '127.0.0.1';
        this.port       = (process.env.NODE_ENV === 'production' ? process.env.MIX_NODE_PROD_PORT : process.env.MIX_NODE_DEV_PORT) || 9090;
        this.requestId  = 0;
        this.observable = new EventEmitter();
        this.queue      = {};
        this.profile    = {};

        this.connection = {};
        this.listeners  = {};

        this.start();
    }

    start() {
        this.connection = io.connect(location.host + ':' + this.port);
        this.connection.timeOut = setTimeout(function() {
            console.error('Can\'t connect to socket server...');
            if (call != null) {
                call.destroy();
                call = {};
            }
            this.connection.close();
        }.bind(this), 5000);
        this.connection.on('connect', function() {
            clearTimeout(this.connection.timeOut);
        }.bind(this));

        this.connection.on('RTC/remoteData', function(data) {
            if (call.Video != null) {
                console.log('exec remoteData');
                call.Video.onGetRtc(data);
            } else {
                console.log('no exec remoteData');
            }
        }.bind(this));

        this.connection.on('message', function(data) {
            console.log('[RESP]', data);
            if (this.queue[data.requestId] && data.error) {
                console.log(data.error);
            } else if (this.queue[data.requestId]) {
                this.queue[data.requestId].emit("req", data.data);
                delete this.queue[data.requestId];
            } else {
                this.observable.emit("message", data);
            }
        }.bind(this));

        this.connection.on('user_online', function(data) {
            this.observable.emit("user_online", data)
        }.bind(this));

        this.connection.on('auth', function(data) {
            console.log('AUTH', data);
            this.profile = data;
        }.bind(this));

        this.observable.on('message', function(data) {
            if (data.method != null) {
                let method = data.method.replace('RTC/on', '').toLowerCase();
                if (typeof call.on[method] === "function") {
                    call.on[method].bind(call)(data.data);
                }
            }
        });
    }

    send(method, data, ...args) {
        console.log('[SEND {' + method + '}] ', data, args);
        this.requestId++;
        let req = {
            method:    method,
            data:      data,
            requestId: this.requestId,
            args:      args.length > 0 ? args : undefined,
        };

        this.connection.emit('message', req);
        this.queue[this.requestId] = new EventEmitter();
        return new Promise((resolve, reject) => {
            this.queue[this.requestId].on("req", data => {
                return resolve(data)
            })
        });
    }
}

ioConnection.prototype.sendMessage = function(id, text, ...args) {
    return new Promise((resolve, reject) => {
        if (id && text) {
            this.send('Message/sendMessage', {id, text}, args).then(data => {
                resolve(data)
            })
        } else {
            reject('empty id or text')
        }

    })
};
// ioConnection.prototype.getCountOnline = function(...args) {
//     return new Promise((resolve, reject) => {
//         this.send('Users/getCountOnline', {}, args).then(data => {
//             resolve(data)
//         })
//     })
// };
// ioConnection.prototype.getOnline = function(data, ...args) {
//     return new Promise((resolve, reject) => {
//         this.send('Users/getOnline', data, args).then(data => {
//             resolve(data)
//         })
//     })
// };
ioConnection.prototype.setObserv   = function(uids, ...args) {
    return new Promise((resolve, reject) => {
        this.send('Users/setObserv', {uids}, args).then(data => {
            resolve(data)
        })
    })
};
ioConnection.prototype.addObserv   = function(uids, ...args) {
    return new Promise((resolve, reject) => {
        this.send('Users/addObserv', {uids}, args).then(data => {
            resolve(data)
        })
    })
};
ioConnection.prototype.getMessages = function(id, skip, limit, ...args) {
    return new Promise((resolve, reject) => {
        this.send('Message/getMessages', {id, skip, limit}, args).then(data => {
            resolve(data)
        })
    })
};

ioConnection.prototype.initCallModule = function(options) {
    if (options.localVideo != null) {
        let localVideo   = options.localVideo.video || options.localVideo[0] || null,
            audioControl = options.localVideo.audioControl || options.localVideo[1] || null,
            videoControl = options.localVideo.videoControl || options.localVideo[2] || null;
        if(options.localVideo instanceof HTMLElement) {
            localVideo = options.localVideo;
        }

        if (localVideo != null) {
            call = new Call(this);

            call.Video.localVideo = new VideoStream([
                localVideo,
                audioControl,
                videoControl
            ], null);

            if (options.sounds != null) {
                let callSound = options.sounds.call || options.sounds[0] || null;
                if (callSound.src != null && callSound.timeout) {
                    call.sounds.call         = Audio.createSound(callSound.src, callSound.timeout);
                    call.sounds.call.preload = 'metadata';
                }

                let ringtoneSound = options.sounds.ringtone || options.sounds[1] || null;
                if (ringtoneSound.src != null && ringtoneSound.timeout) {
                    call.sounds.ringtone         = Audio.createSound(ringtoneSound.src, ringtoneSound.timeout);
                    call.sounds.ringtone.preload = 'metadata';
                }
            }
        }
    }
};

ioConnection.prototype.uiEvents = {};
ioConnection.prototype.callUiEvent = function(name, data){
    if (typeof this.uiEvents[name] === "function") {
        if (data == null) {
            return this.uiEvents[name]();
        } else {
            return this.uiEvents[name](data);
        }
    }
};
ioConnection.prototype.uiFunctions = {};

export default ioConnection;