Audio.createSound = function(src, timeout) {
    let sound           = new Audio(src);
    sound.repeatTimeout = timeout || null;
    sound.timer         = null;

    return sound;
};

Audio.prototype.start = function() {
    if(this.repeatTimeout != null) {
        this.timer = setInterval(function() {
            this.play();
        }.bind(this), this.repeatTimeout);
    }
    this.play();
    return this;
};

Audio.prototype.stop = function() {
    this.pause();
    this.currentTime = 0;
    if(this.isSound()) {
        clearInterval(this.timer);
        this.timer = null;
    }
    return this;
};

Audio.prototype.isSound = function() {
    return this.timer !== undefined && this.repeatTimeout !== undefined;
};