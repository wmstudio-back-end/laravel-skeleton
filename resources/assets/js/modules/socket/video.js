import VideoStream from './videoStream';
import Call from "./call";

const peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.stunprotocol.org:3478'},
        {'urls': 'stun:stun.l.google.com:19302'},
        { 'url': 'stun:stun4.l.google.com:19302' },
        { 'url': 'stun:stunserver.org' },
        { 'url': 'stun:stun.softjoys.com' },
        { 'url': 'stun:stun.voiparound.com' },
        { 'url': 'stun:stun.voipbuster.com' },
        { 'url': 'stun:stun.voipstunt.com' },
        { 'url': 'stun:stun.voxgratia.org' },
        { 'url': 'stun:stun.xten.com' },
        {
            'url': 'turn:numb.viagenie.ca',
            'credential': 'muazkh',
            'username': 'webrtc@live.com'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=udp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        },
        {
            'url': 'turn:192.158.29.39:3478?transport=tcp',
            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
            'username': '28224511:1379330808'
        }
    ]
};

let Video = function(Call) {
    this.call               = Call;
    this.localVideo         = {};
    this.config             = {
        audio: true,
        video: {
            mandatory: {},
            optional: [
                {minWidth:712},
                {maxWidth: 1280},
                {minHeight: 400},
                {maxHeight: 720},
            ],
        },
    };
    this.PeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;

    this.IceCandidate       = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
    this.SessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
    navigator.getUserMedia  = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

    this.callTo = null;
};

Video.prototype.getUserMedia = function(remoteUid, config) {
    if (config == null) {
        config = this.config;
    }
    let gotStream = function(stream) {
        if (this.localVideo instanceof VideoStream) {
            this.localVideo.stream = stream;
            this.localVideo.init();
        } else {
            this.localVideo = new VideoStream(this.localVideo, stream);
        }
        this.createPeerConnection(remoteUid);
    }.bind(this);

    return new Promise((resolve, reject) => {
        try {
            let getUserMedia = function(config) {
                let conf = {
                    audio: !!config.audio,
                    video: !!config.video,
                };
                navigator.getUserMedia(
                    conf,
                    (function(stream) {
                        gotStream(stream);
                        resolve();
                    }),
                    console.log
                );
            };
            if (navigator.mediaDevices.getUserMedia || navigator.getUserMedia) {
                if (navigator.mediaDevices.getUserMedia) {

                    navigator.mediaDevices.getUserMedia(config).catch(function(error) {
                        if (error.name !== "NotFoundError") {
                            throw error;
                        }
                        return navigator.mediaDevices.enumerateDevices().then(function(devices) {
                            console.log(devices);
                            let cam = devices.find(function(device) {
                                return device.kind === "videoinput";
                            }) || false;
                            let mic = devices.find(function(device) {
                                return device.kind === "audioinput";
                            }) || false;
                            let constraints = {video:cam && config.video, audio:mic && config.audio};
                            return navigator.mediaDevices.getUserMedia(constraints);
                        });
                    }).then(function(stream) {
                        gotStream(stream);
                        resolve();
                    }).catch(function() {
                        getUserMedia(config);
                    });
                } else {
                    getUserMedia(config);
                }
            }
        } catch (e) {
            reject(e);
        }
    });
};

Video.prototype.createPeerConnection = function(remoteUid) {
    if (this.localVideo instanceof VideoStream && this.localVideo.stream != null) {
        let pc = new this.PeerConnection(peerConnectionConfig);

        pc.addStream(this.localVideo.stream);
        pc.onicecandidate             = this.gotIceCandidate.bind(this);
        pc.onaddstream                = this.gotRemoteStream.bind(this);
        pc.oniceconnectionstatechange = this.stateChange.bind(this);
        pc.uid                        = remoteUid;
        if (this.call.users[remoteUid] == null) {
            this.call.users[remoteUid] = {};
        }
        this.call.users[remoteUid].peerConnection = pc;
        this.call.socket.observable.emit('createPeerConnectionToUser-' + remoteUid, false);
    } else {
        console.log('Something wrong with local video', this.localVideo);
    }
};

Video.prototype.createOffer = function(uid) {
    return new Promise((resolve, reject) => {
        this.call.users[uid].peerConnection
            .createOffer({'mandatory': {'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true}})
            .then(function(description) {
                this.call.users[uid].peerConnection
                    .setLocalDescription(description)
                    .then(function(){
                        this.sendRtc(uid, description);
                        resolve();
                    }.bind(this))
                    .catch(reject);
            }.bind(this)).catch(reject);
    });
};

Video.prototype.createAnswer = function(uid) {
    this.call.users[uid].peerConnection
        .createAnswer({'mandatory': {'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true}})
        .then(function(description) {
            this.call.users[uid].peerConnection
                .setLocalDescription(description)
                .then(function(){
                    this.sendRtc(uid, description);
                }.bind(this))
                .catch(console.log);
        }.bind(this))
        .catch(console.log);
};

Video.prototype.gotIceCandidate = function(event) {
    if (event.candidate) {
        this.sendRtc(event.target.uid, {
            type:      'candidate',
            label:     event.candidate.sdpMLineIndex,
            id:        event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });
    }
};

Video.prototype.gotRemoteStream = function(event) {
    let video, audioControl, videoControl,
        uid       = event.target.uid,
        generated = this.call.socket.callUiEvent('elVidGenerator', {id: uid, name: this.call.users[uid].name});
    if(generated instanceof HTMLElement || generated instanceof jquery) {
        video = generated;
    }

    let convertToDOM = function(selector) {
        if (selector != null && selector instanceof jquery) {
            selector = selector[0];
        }
        return selector;
    };

    video        = convertToDOM(video || generated.video || generated[0] || null);
    audioControl = convertToDOM(generated.audioControl || generated[1] || null);
    videoControl = convertToDOM(generated.videoControl || generated[2] || null);

    this.call.users[uid].remoteStream = new VideoStream(
        {
            video,
            audioControl,
            videoControl,
        },
        event.stream
    );
    if (event.stream) {
        this.call.socket.send('RTC/videoInit', {});
    }
};

Video.prototype.stateChange = function(e) {
    switch (e.target.iceConnectionState) {
        // case 'closed':
        case 'failed':
            console.log('Remote stream error: connection failed!');
            // this.call.socket.DisconnectUser(e.target.uid);
            break;
    }
};

Video.prototype.onGetRtc = function(data) {
    if (this.call.users[data.remoteUid] == null) {
        this.call.users[data.remoteUid] = {};
    }
    if (this.call.users[data.remoteUid].peerConnection == null) {
        if (this.call.users[data.remoteUid].rtcStack == null) {
            this.call.users[data.remoteUid].rtcStack = [];
        }
        this.call.users[data.remoteUid].rtcStack.push(data);
        // Чистим старые листнеры
        this.call.socket.observable.emit('createPeerConnectionToUser-' + data.remoteUid, false);
        new Promise((resolve, reject) => {
            this.call.socket.observable.once('createPeerConnectionToUser-' + data.remoteUid, function(result) {
                result ? resolve() : reject();
            });
        }).then(function(){
            if (this.call.users[data.remoteUid].rtcStack) {
                for (let i in this.call.users[data.remoteUid].rtcStack) {
                    this.onGetRtc(this.call.users[data.remoteUid].rtcStack[i]);
                }
                delete this.call.users[data.remoteUid].rtcStack;
            }
        }.bind(this)).catch(function(){});
    } else {
        console.log('[RTC/GetData]', data.message);
        switch (data.message.type) {
            case 'offer':
                this.call.users[data.remoteUid].peerConnection
                    .setRemoteDescription(new this.SessionDescription(data.message))
                    .then(function(){
                        this.createAnswer(data.remoteUid);
                    }.bind(this))
                    .catch(function(e){
                        console.log('createAnswer', e);
                    });
                break;
            case 'answer':
                this.call.users[data.remoteUid].peerConnection
                    .setRemoteDescription(new this.SessionDescription(data.message)).catch(console.log);
                break;
            case 'candidate':
                this.call.users[data.remoteUid].peerConnection.addIceCandidate(new this.IceCandidate({
                    sdpMLineIndex: data.message.label,
                    candidate:     data.message.candidate,
                })).catch(console.log);
                break;
        }
    }
};

Video.prototype.sendRtc = function(uid, message) {
    console.log('[SEND TO user-' + uid + ']', message);

    this.call.socket.connection.emit('RTC/sendData', {uid, message});
};

Video.prototype.destroy = function() {};

export default Video;