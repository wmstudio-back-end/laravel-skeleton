import Video from "./video";

let audioControlClick = function(e) {
    e.preventDefault();
    e.stopPropagation();
    this.toggleAudio();
};

let videoControlClick = function(e) {
    e.preventDefault();
    e.stopPropagation();
    this.toggleVideo();
};

/**
 * @param el
 * @returns HTMLElement|bool
 */
let validElement = function(el) {
    if (el instanceof HTMLElement) {
        return el;
    } else if (el instanceof jquery) {
        return el[0];
    }
    return false;
};

let setState = function(el, state) {
    if(el != null) {
        if(state === null) {
            el.setAttribute('disabled', 'disabled');
        } else {
            el.removeAttribute('disabled');
            if(state) {
                el.classList.add('active');
            } else {
                el.classList.remove('active');
            }
        }
    }
};

let VideoStream = function(opts, stream) {
    this.stream = stream;
    this.audio = this.video = this.audioEnabled = this.videoEnabled = null;
    this.init();

    this.setVideoEl(opts.video || opts[0] || null);
    this.setAudioControlEl(opts.audioControl || opts[1] || null);
    this.setVideoControlEl(opts.videoControl || opts[2] || null);
};

VideoStream.prototype.init = function(){
    let oldAudioEnabled = this.audioEnabled;
    let oldVideoEnabled = this.videoEnabled;

    if (this.stream == null) {
        this.audio = this.video = null;
    } else {
        if (this.el != null) {
            this.el.classList.remove('not-init');
            this.el.srcObject = this.stream;
        }

        this.audio = this.stream.getAudioTracks().length;
        this.video = this.stream.getVideoTracks().length;
    }

    if (typeof oldAudioEnabled === "boolean" && typeof this.audio === "boolean") {
        this.audioEnabled = oldAudioEnabled;
    } else {
        this.audioEnabled = this.audio ? true : null;
    }
    this.setAudioState(this.audioEnabled);

    if (typeof oldVideoEnabled === "boolean" && typeof this.video === "boolean") {
        this.videoEnabled = oldVideoEnabled;
    } else {
        this.videoEnabled = this.video ? true : null;
    }
    this.setVideoState(this.videoEnabled);
};

VideoStream.prototype.setVideoEl = function(video) {
    video = validElement(video);
    if (video !== false) {
        if(video.getAttribute('local') != null) {
            if (this.stream != null) {
                video.classList.remove('not-init');
            }
            video.muted = true;
        }
        video.autoplay  = this.el != null ? this.el.autoplay : true;
        video.srcObject = this.stream;
        this.el = video;
    }
};

VideoStream.prototype.setAudioControlEl = function(audioControl) {
    audioControl = validElement(audioControl);
    if (audioControl !== false) {
        $(audioControl).bind('click', audioControlClick.bind(this));
        this.audioControl = audioControl;
        this.setAudioState(this.audioEnabled);
    }
};

VideoStream.prototype.setVideoControlEl = function(videoControl) {
    videoControl = validElement(videoControl);
    if (videoControl !== false) {
        $(videoControl).bind('click', videoControlClick.bind(this));
        this.videoControl = videoControl;
        this.setVideoState(this.videoEnabled);
    }
};

VideoStream.prototype.setAudioState = function(state) {
    setState(this.audioControl, state);
};

VideoStream.prototype.setVideoState = function(state) {
    setState(this.videoControl, state);
};

let toggleTracks = function(enabled, tracks) {
    for(let i in tracks) {
        tracks[i].enabled = enabled;
    }
};

VideoStream.prototype.toggleAudio = function() {
    let result = null;
    if(this.audio) {
        toggleTracks((this.audioEnabled = !this.audioEnabled), this.stream.getAudioTracks());
        result = this.audioEnabled;
    }
    this.setAudioState(result);
    return this;
};

VideoStream.prototype.toggleVideo = function() {
    let result = null;
    if(this.video) {
        toggleTracks((this.videoEnabled = !this.videoEnabled), this.stream.getVideoTracks());
        result = this.videoEnabled;
    }
    this.setVideoState(result);
    return this;
};

VideoStream.prototype.stop = function(){
    if (this.el != null) {
        this.el.srcObject = null;
        this.el.src = null;
    }
    if (this.stream != null) {
        let tracks = this.stream.getTracks();
        for (let i in tracks) {
            tracks[i].stop();
        }
        this.stream = null;
    }

    if (this.audioControl != null) {
        this.setAudioState(null);
        $(this.audioControl).unbind('click', audioControlClick);
    }
    if (this.videoControl != null) {
        this.setVideoState(null);
        $(this.videoControl).unbind('click', videoControlClick);
    }
};

export default VideoStream;