import Video from "./video";

require('./audioAdditional');

let Call = function(io) {
    this.socket = io;
    this.first  = true;
    this.sounds = {
        call:     null,
        ringtone: null,
    };
    this.timer = null;
    this.users  = {};
    this.Video  = new Video(this);
    this.socket.uiFunctions = {
        sendMessage: this.sendMessage.bind(this),
        callTo: this.callTo.bind(this),
        answerTo: this.answerTo.bind(this),
        abortCall: this.abortCall.bind(this),
        endCall: this.endCall.bind(this),
    };
};

// Sounds
Call.prototype.ringtonePlay = function() {
    if (this.sounds.ringtone != null && this.sounds.ringtone.isSound()) {
        this.sounds.ringtone.start();
    }
    return this;
};

Call.prototype.callPlay = function() {
    if (this.sounds.call != null && this.sounds.call.isSound()) {
        this.sounds.call.start();
    }
    return this;
};

Call.prototype.stopSounds = function() {
    if (this.sounds.call != null) {
        this.sounds.call.stop();
    }
    if (this.sounds.ringtone != null) {
        this.sounds.ringtone.stop();
    }
    return this;
};

Call.prototype.sendMessage = function(message) {
    this.socket.send('RTC/sendMessage', {message})
        .then(function(data) {
            if (data.result === true) {
                this.socket.callUiEvent('onSendMessage', message);
            }
        }.bind(this)).catch(console.log);
};

Call.prototype.callTo = function(uid) {
    if (this.first) {
        console.log('Call to user ' + uid + '[firstCall]');
        this.socket.send('RTC/call', {uid})
            .then(function() {
                this.callPlay();
                this.socket.callUiEvent('onCall', {
                    remoteId: uid,
                    isCaller: true,
                    isSecondary: false,
                })
            }.bind(this)).catch(console.log);
    } else {
        console.log('Call to user ' + uid + '[secondaryCall]');
        this.socket.send('RTC/secondaryCall', {uid})
            .then(function() {
                this.socket.callUiEvent('onCall', {
                    remoteId: uid,
                    isCaller: true,
                    isSecondary: true,
                })
            }.bind(this)).catch(console.log);
    }
};

Call.prototype.answerTo = function(uid) {
    this.socket.observable.emit('answer-to-' + uid, {answer: true});
};

Call.prototype.abortCall = function(uid) {
    if (this.socket.observable.listeners('answer-to-' + uid).length) {
        this.socket.observable.emit('answer-to-' + uid, {answer: false});
        this.on.destroy.bind(this)();
    } else if (uid != null) {
        if (this.first) {
            this.socket.send('RTC/abortCall', {uid});
            this.on.destroy.bind(this)();
        } else {
            this.socket.send('RTC/abortSecondaryCall', {uid});
            this.on.reject.bind(this)({remoteUid: uid});
        }
    }
};

Call.prototype.endCall = function(){
    this.socket.send('RTC/rejectCall', {}).then(console.log).catch(console.log);
    this.on.destroy.bind(this)();
};

Call.prototype.on = {
    message: function(data) {
        this.socket.callUiEvent('onMessage', data);
    },
    call: function(data) {
        let remoteId = data.remoteUid;
        this.socket.observable.removeAllListeners('answer-to-' + remoteId);
        new Promise((resolve, reject) => {
            this.socket.observable.once('answer-to-' + remoteId, function(data) {
                resolve(data.answer);
            }.bind(this));
        }).then(function(answer) {
            this.socket.send('RTC/answerCall', {uid: remoteId, answer})
                .then(function(data) {
                    if (answer && data.result === true) {
                        this.first = false;
                        this.stopSounds();
                        this.socket.callUiEvent('onStartCall', data.admin);
                        if (data.timeLeft != null) {
                            console.log('set end call timeout', data.timeLeft * 1000);
                            this.timer = setTimeout(function(){
                                console.log('end call by timeout');
                                this.endCall();
                            }.bind(this), data.timeLeft * 1000);
                        }
                    } else {
                        this.on.destroy.bind(this)();
                    }
                }.bind(this));
        }.bind(this)).catch(console.log);

        if (data.sound) {
            this.ringtonePlay();
        }
        this.socket.callUiEvent('onCall', {
            remoteId: data.remoteUid,
            isCaller: false,
            isSecondary: false,
        });
    },
    answer: function(data) {
        this.first = false;
        this.stopSounds();
        if (data.timeLeft != null) {
            console.log('set end call timeout', data.timeLeft * 1000);
            this.timer = setTimeout(function(){
                console.log('end call by timeout');
                this.endCall();
            }.bind(this), data.timeLeft * 1000);
        }
        this.socket.callUiEvent('onStartCall', data.admin);
    },
    join: function(data) {
        let remoteUid = data.remoteUid;
        if (this.users[remoteUid] == null) {
            this.users[remoteUid] = {
                name: data.remoteName,
            };
        } else {
            this.users[remoteUid].name = data.remoteName;
        }

        if (this.Video.localVideo.stream != null) {
            if (this.users[remoteUid].peerConnection == null) {
                this.Video.createPeerConnection(remoteUid);
            }
            if (data.createOffer) {
                this.Video.createOffer(remoteUid);
            }
        } else {
            this.Video.getUserMedia(remoteUid).then(function() {
                if (data.createOffer) {
                    this.Video.createOffer(remoteUid);
                }
            }.bind(this)).catch(console.log);
        }
    },
    reject: function(data) {
        if (this.users[data.remoteUid] != null) {
            if (this.users[data.remoteUid].peerConnection != null) {
                this.users[data.remoteUid].peerConnection.close();
            }
            if (this.users[data.remoteUid].remoteStream != null) {
                this.users[data.remoteUid].remoteStream.stop();
            }
            delete this.users[data.remoteUid];
        }
        this.socket.callUiEvent('onDisconnectUser', data.remoteUid);
    },
    destroy: function(data) {
        for (let uid in this.users) {
            this.on.reject.bind(this)({remoteUid: uid});
        }
        this.Video.localVideo.stop();
        this.stopSounds();
        this.socket.callUiEvent('onEndCall');
        this.first = true;
        if (this.timer != null) {
            clearTimeout(this.timer);
            this.timer = null;
        }
    },
};

Call.prototype.destroy = function() {
    for (let func in this.uiFunctions) {
        this.uiFunctions[func] = function(){};
    }
};

export default Call;