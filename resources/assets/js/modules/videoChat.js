import {socket} from '../globals';

let callClass = 'call-to-user';

let bindCallToUserButton = function() {
    socket.uiFunctions.callTo($(this).data('user-id'));
};

export {callClass, bindCallToUserButton};

export default function videoChat() {
    $(document).ready(function() {
        let localVideo = $('#localVideo'),
            hideChat = false;
        socket.initCallModule({
            localVideo: [
                localVideo,
                $('#audio-ctrl'),
                $('#video-ctrl'),
            ],
            sounds: {
                call:     {src: '/media/call.mp3', timeout: 4000},
                ringtone: {src: '/media/ringtone.mp3', timeout: 4000},
            }
        });
        let msgElements = {
                container: $('#rtc-msg-container'),
                self:      $('#rtc-self').removeClass('hidden').remove(),
                foreign:   $('#rtc-foreign').removeClass('hidden').remove(),
                send:      $('#rtc-send-msg'),
            },
            callDialogModal = $(".callDialog-fon"),
            closeModalBtn = $(".callDialog-close"),
            callBtns = $('.callDialog-call'),
            videoContainer = $('#video-container'),
            videoControls = $('#video-controls'),
            addUser = $('#add-user'),
            findUsers = addUser.find('select.select2'),
            noAvatar = 'https://laravel.loc/img/no-avatar.png',
            answerBtn = $('#call-answer'),
            rejectUser = $('#reject-user'),
            rejectCall = $('#reject-call'),
            callDialogName = $('#call-dialog-name'),
            body = $('body'),
            onVideoClick = function(){
                $(this).toggleClass('smoothly active').parent().toggleClass('active');
            },
            updateMembers = function(user) {
                let userEl = callDialogName.find('[data-user-id="' + user.id + '"]');
                if (userEl.length) {
                    if (user.name == null) {
                        userEl.addClass('disconnected');
                    } else {
                        userEl.removeClass('disconnected');
                    }
                } else {
                    if (user.name != null) {
                        callDialogName.append($('<span data-user-id="' + user.id + '">' + user.name + '</span>'));
                    }
                }
            };

        msgElements.send.keydown(function(e) {
            if(e.keyCode === 13) {
                if(e.shiftKey) {
                    if(typeof this.selectionStart === "number") {
                        if(this.selectionStart === this.selectionEnd && this.selectionStart === this.value.length) {
                            this.value += '\n';
                            this.scrollTop = this.scrollHeight;
                        } else {
                            let position        = this.selectionStart;
                            this.value          = this.value.substr(0, position) + '\n'
                                + this.value.substr(this.selectionEnd);
                            this.selectionStart = this.selectionEnd = position + 1;
                            this.blur();
                            this.focus();
                        }
                    }
                } else if(this.value.trim().length) {
                    let message = this.value.trim();
                    this.value     = '';
                    socket.uiFunctions.sendMessage(message);
                }
                return false;
            }
        });

        socket.uiEvents.onSendMessage = function(message) {
            let msg = msgElements.self.clone();
            msg.find('.rtc-message').html(message.replace(/\n/g, '<br>').trim());
            msgElements.container.append(msg);
        };
        socket.uiEvents.onMessage = function(data) {
            let msg = msgElements.foreign.clone();
            msg.find('.rtc-message').html(data.message.replace(/\n/g, '<br>').trim());
            msg.find('.rtc-sender').text(data.name);
            msgElements.container.append(msg);
        };

        let rejectUserClick = function(){
            rejectUser.unbind('click', rejectUserClick);
            socket.uiFunctions.abortCall(rejectUser.data('user-id'));
            callBtns.addClass('hidden');
        };

        answerBtn.click(function(){
            socket.uiFunctions.answerTo($(this).data('user-id'));
        });

        socket.uiEvents.onCall = function(data) {
            let firstCall = function(){
                body.addClass('hidden-video-chat-dialog');
                callDialogModal.addClass('active');
                localVideo.addClass('hidden');
                videoControls.addClass('hidden');
                addUser.addClass('hidden');
                closeModalBtn.addClass('hidden');
                callBtns.find('.user').remove();
                callDialogName.find('[data-user-id]').remove();
                updateMembers(socket.profile);
            };
            try {
                if (data.isCaller === true) {
                    answerBtn.addClass('hidden');
                    answerBtn.data('user-id', null);
                    if (data.isSecondary) {
                        callBtns.addClass('secondary-call');
                    } else {
                        callBtns.removeClass('secondary-call');
                        firstCall();
                    }
                } else {
                    answerBtn.removeClass('hidden');
                    answerBtn.data('user-id', data.remoteId);
                    callBtns.removeClass('secondary-call');
                    firstCall();
                }
            } finally {
                callBtns.removeClass('hidden');
                rejectUser.data('user-id', data.remoteId).unbind('click', rejectUserClick).bind('click', rejectUserClick);
                body.addClass('modal-opened');
            }
        };

        socket.uiEvents.onStartCall = function(admin) {
            msgElements.send.removeAttr('disabled');
            rejectUser.unbind('click', rejectUserClick);
            callBtns.removeClass('secondary-call').addClass('hidden');
            localVideo.removeClass('hidden');
            videoControls.removeClass('hidden');
            if (!hideChat) {
                body.removeClass('hidden-video-chat-dialog');
            }
            if (admin) {
                addUser.removeClass('hidden');
            }
        };
        socket.uiEvents.onEndCall = function(){
            msgElements.send.attr('disabled', 'disabled');
            if (findUsers.length) {
                findUsers.select2('close');
            }
            closeModalBtn.removeClass('hidden');
            callBtns.addClass('hidden');
            localVideo.addClass('hidden');
            videoControls.addClass('hidden');
            addUser.addClass('hidden');
            videoContainer.html('');
            if (hideChat || !(callDialogName.find('[data-user-id]').length > 1)) {
                closeModalBtn.click();
            }
            callDialogName.children().addClass('disconnected');
        };
        socket.uiEvents.elVidGenerator = function(user) {
            let video = document.createElement('video'),
                videoParent = $('<div class="callDialog-video-content smoothly" data-video-call-uid="' + user.id + '"></div>'),
                containerChilds = videoContainer.children(),
                childLength = containerChilds.length;
            if (childLength) {
                videoContainer.removeClass('active');
                containerChilds.removeClass('active');
            } else {
                videoContainer.addClass('active');
                videoParent.addClass('active');
            }
            videoContainer.append(videoParent.append(video));
            if (childLength) {
                videoContainer.find('.callDialog-video-content')
                    .unbind('click', onVideoClick)
                    .bind('click', onVideoClick);
            } else {
                videoContainer.find('.callDialog-video-content')
                    .unbind('click', onVideoClick);
            }

            updateMembers(user);
            return {
                video,
                audioControl: null,
                videoControl: null,
            };
        };
        socket.uiEvents.onDisconnectUser = function(userId) {
            updateMembers({id: userId});
            $('[data-video-call-uid="' + userId + '"]').remove();
            if (parseInt(rejectUser.data('user-id')) === userId) {
                rejectUser.unbind('click', rejectUserClick);
                callBtns.addClass('hidden');
            }
        };
        rejectCall.click(function(){
            socket.uiFunctions.endCall();
        });

        closeModalBtn.click(function () {
            callDialogModal.removeClass('active');
            body.removeClass('modal-opened');
        });

        if (addUser.length) {
            findUsers.select2({
                dropdownCssClass: 'add-user-to-call smoothly',
                minimumInputLength: $(this).data('minimum-input') || 3,
                templateSelection: function(data){
                    if (data.id === '') {
                        if (data.text !== '') {
                            $(data.element).parent().next().attr('title', data.text);
                        }
                        return $('<span class="select2-selection__placeholder add-user"></span>');
                    }

                    return $(data.text);
                },
                templateResult: function(data){
                    if (data.id === '') {
                        if(!$(data.element).parents('select').hasClass('hide-empty')) {
                            return data.text;
                        }
                    } else {
                        if (data.element != null) {
                            return $($(data.element).html());
                        } else {
                            return $(data.text);
                        }
                    }
                },
                ajax: {
                    url: findUsers.data('action'),
                    dataType: 'json',
                    delay: 250,
                    method: 'post',
                    data: function(params) {
                        let res = [
                            {
                                name: '_token',
                                value: window.token,
                            },
                            {
                                name: 'search',
                                value: params.term,
                            },
                        ];

                        callDialogName.find(':not(.disconnected)').each(function(key, val) {
                            res.push({
                                name: 'excluded[]',
                                value: $(val).data('user-id'),
                            });
                        });

                        return res;
                    },
                    processResults: function (data) {
                        let result = [];
                        if (data.error != null) {
                            result.push({
                                id: '',
                                text: data.error,
                            })
                        } else {
                            for (let key in data) {
                                let avatar = data[key].avatar;
                                if (avatar === noAvatar) {
                                    avatar = '<span class="user"></span>';
                                } else {
                                    avatar = '<img class="user" src="' + avatar + '">';
                                }
                                let val = '<div class="found-users-to-call"><div class="avatar">' + avatar
                                    + '</div><b class="name">' + data[key].name + "</b></div>";
                                result.push({
                                    id: key,
                                    text: val,
                                })
                            }
                        }
                        return {
                            results: result
                        };
                    },
                    cache: true
                }
            }).on('select2:select', function(e){
                let selected = $(e.params.data.text);
                this.value = '';
                $(this).trigger('change');
                selected = {
                    avatar: selected.find('.avatar').html(),
                    name: selected.find('.name').text(),
                };
                let user = $(selected.avatar).attr('title', selected.name);
                callBtns.find('.user').remove();
                callBtns.prepend(user);
                callBtns.addClass('secondary-call').removeClass('hidden');

                socket.uiFunctions.callTo(e.params.data.id);
            }).on('select2:opening', function(){
                videoControls.addClass('hover');
            }).on('select2:close', function(){
                videoControls.removeClass('hover');
            });
        }
    });
}
