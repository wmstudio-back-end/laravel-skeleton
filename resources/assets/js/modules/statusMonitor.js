import {socket} from "../globals";
import {jsonClone} from "./helpers";
import {bindCallToUserButton} from "./videoChat";

let watchList = {},
    userStatuses = {};

export function isOnline(userId) {
    return new Promise((resolve, reject) => {
        try {
            if(watchList[userId] != null) {
                resolve(!!watchList[userId]);
            } else if(userStatuses[userId] != null) {
                resolve(!!userStatuses[userId]);
            } else {
                addWatch(userId).then(function(data){
                    let online = false,
                        found = false;
                    for (let i in data.users) {
                        if (data.users[i].uid == userId) {
                            online = data.users[i].online;
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        resolve(online);
                    } else {
                        reject({reason: 'id ' + userId + ' not found', data});
                    }
                }).catch(reject);
            }
        } catch(e) {
            reject(e);
        }
    });
}

function updateStatuses(data) {
    try {
        let updateUIDs = {};
        for(let i in data.users) {
            if(watchList[data.users[i].uid] != null) {
                watchList[data.users[i].uid] = data.users[i].online;
            } else {
                userStatuses[data.users[i].uid] = data.users[i].online;
            }
            updateUIDs[data.users[i].uid] = data.users[i].online;
        }
        showStatuses(updateUIDs);
        return true;
    } catch(e) {
        return false;
    }
}

function showStatuses(uids) {
    try {
        let updateUIDs = {};
        if(typeof uids === 'object' && !Array.isArray(uids)) {
            updateUIDs = uids;
        } else {
            updateUIDs = {...watchList, ...userStatuses};
        }
        for(let uid in updateUIDs) {
            $('[data-line-status-id="' + uid + '"]').removeAttr('class').addClass(updateUIDs[uid] ? 'online' : 'offline');
            let callBtn = $('.call-to-user[data-user-id="' + uid + '"]');
            if(updateUIDs[uid]) {
                callBtn.removeClass('hidden').unbind('click').bind('click', bindCallToUserButton);
            } else {
                callBtn.addClass('hidden').unbind('click');
            }
        }
        return true;
    } catch(e) {
        return false;
    }
}

export function setWatch(uids) {
    return new Promise((resolve, reject) => {
        try {
            let sendUIDs = jsonClone(watchList);
            if(typeof uids === 'object') {
                if(Array.isArray(uids) || Object.keys(uids)[0] === 0) {
                    for(let i in uids) {
                        sendUIDs[uids[i]] = true;
                    }
                } else {
                    for(let i in uids) {
                        sendUIDs[i] = true;
                    }
                }
            } else if(typeof uids === 'number') {
                sendUIDs[uids] = true;
            } else if(uids === false) {
                userStatuses = {};
            }
            sendUIDs = Object.keys(sendUIDs);
            if(sendUIDs.length) {
                socket.setObserv(sendUIDs).then(function(data) {
                    if (updateStatuses(data)) {
                        resolve(data);
                    } else {
                        reject(false);
                    }
                }).catch(console.log);
            }
        } catch(e) {
            reject(e)
        }
    });
}

export function addWatch(uids) {
    return new Promise((resolve, reject) => {
        let sendUIDs = {};
        if (typeof uids === 'object') {
            if (Array.isArray(uids) || Object.keys(uids)[0] === 0) {
                for(let i in uids) {
                    sendUIDs[uids[i]] = true;
                }
            } else {
                for(let i in uids) {
                    sendUIDs[i] = true;
                }
            }
        } else if (
            typeof uids === 'string' && !isNaN(parseInt(uids))
            || typeof uids === 'number'
        ) {
            sendUIDs[uids] = true;
        }
        sendUIDs = Object.keys(sendUIDs);
        if (sendUIDs.length) {
            socket.addObserv(sendUIDs).then(function(data) {
                if (updateStatuses(data)) {
                    resolve(data);
                } else {
                    reject(false);
                }
            }).catch(console.log);
        } else {
            reject('uids not found');
        }
    });
}

export default function statusMonitor() {
    $(document).ready(function(){
        watchList = window.userWatchList || {};
        setWatch();

        let countOnline = $('#online-count');
        socket.observable.on("user_online", (data) => {
            countOnline.text(data.count > 0 ? data.count : '');
            updateStatuses({users: [data]});
            console.log("USER ONLINE", data);
        });
    });
}
