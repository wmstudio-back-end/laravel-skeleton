import Pages from './pages';
import chat from './modules/chat';
import videoChat from './modules/videoChat';
import dates from './modules/dates';
import selects from './modules/selects';
import form from './modules/form';
import statusMonitor from './modules/statusMonitor';
import global from './modules/global';

let App = {
    Pages,
    Modules: {
        global,
        dates,
        selects,
        form,
    },
};

if (window.userStatus === 's' || window.userStatus === 't') {
    App.Modules.chat = chat;
    App.Modules.videoChat = videoChat;
    App.Modules.statusMonitor = statusMonitor;
}

import {init} from "./globals";
init(App);
