const logLoadedNames = true;

export function isProd() {
    return window.env === 'production';
}

function setDefaults() {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": 300,
        "hideDuration": 500,
        "timeOut": 2000,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}

export function init(modules) {
    require('bootstrap/dist/js/bootstrap.bundle.min');

    setDefaults();
    let loadedModules = {};
    for (var key in modules) {
        for (let i = 0; i < modules[key].length; i++) {
            modules[key][i]();
            if (!isProd() && logLoadedNames !== undefined && logLoadedNames) {
                let name = modules[key][i].name;
                name = name[0].toUpperCase() + name.substr(1);
                if (loadedModules[key] === undefined) {
                    loadedModules[key] = [];
                }
                loadedModules[key].push(name);
            }
        }
    }
    if (logLoadedNames !== undefined && logLoadedNames) {
        console.log(loadedModules, 'All modules loaded!');
    }
}