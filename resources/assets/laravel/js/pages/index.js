import home from './home'
import videoChat from './videoChat'

export default [
    home,
    videoChat,
]