export default function home() {
    $(document).ready(function(){
        let countReady = $('li.list-group-item.ready').length,
            countBaget = ' <span class="badge badge-danger badge-pill">' + countReady + '</span>';
        if (countReady) {
            $('button.ready').html('Скрыть готовые' + countBaget).show();
        }
        $('button.ready').click(function(){
            $('li.list-group-item.ready').toggle(300);
            if ($(this).data('state') === 'hide') {
                $(this).data('state', 'show')
                    .removeClass('btn-outline-info')
                    .addClass('btn-outline-success')
                    .html('Показать готовые' + countBaget);
            } else {
                $(this).data('state', 'hide')
                    .removeClass('btn-outline-success')
                    .addClass('btn-outline-info')
                    .html('Скрыть готовые' + countBaget);
            }
        });
    })
};