import io from 'socket.io-client/dist/socket.io';
let userStatus = {
    busy:      0,
    free:      1,
    canStream: 2,
};

function log(msg){
    let log = $('#log');
    let old = log.html();
    log.html(old + "<pre class=\"log\">" + msg + "</pre>");
}

export default function videoChat() {
    if (location.pathname === '/chat') {
        $(document).ready(function(){
            let localStream,
                localStreamInit  = false,
                localVideo       = document.getElementById('localVideo'),
                remoteVideo      = document.getElementById('remoteVideo'),
                mediaConstraints = {
                    audio: true,
                    video: {
                        mandatory: {},
                    }
                },
                peerConnection,
                peerConnectionConfig = {
                    iceServers: [
                        {'urls': 'stun:stun.stunprotocol.org:3478'},
                        {'urls': 'stun:stun.l.google.com:19302'},
                        {'urls': 'stun:stun4.l.google.com:19302'},
                        {'urls': 'stun:stunserver.org'},
                        {'urls': 'stun:stun.softjoys.com'},
                        {'urls': 'stun:stun.voiparound.com'},
                        {'urls': 'stun:stun.voipbuster.com'},
                        {'urls': 'stun:stun.voipstunt.com'},
                        {'urls': 'stun:stun.voxgratia.org'},
                        {'urls': 'stun:stun.xten.com'},
                        {
                            'urls': 'turn:numb.viagenie.ca',
                            'credential': 'muazkh',
                            'username': 'webrtc@live.com'
                        },
                        {
                            'urls': 'turn:192.158.29.39:3478?transport=udp',
                            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                            'username': '28224511:1379330808'
                        },
                        {
                            'urls': 'turn:192.158.29.39:3478?transport=tcp',
                            'credential': 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                            'username': '28224511:1379330808'
                        }
                    ]
                },
                signals          = [],
                signalsHandleRun = false,
                guestsCounter    = 0,
                userId,
                call             = false,
                selfBusy         = false;

            window.URL = window.URL || window.webkitURL;
            // navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
            window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
            window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

            let socket = io.connect(location.host + ':9090');
            socket.on('connect', function () {
                socket.on('getStatus', function(){
                    console.log('setStatus')
                    socket.emit('setStatus', userStatus.canStream);
                });
                socket.on('consoleMsg', function(message){
                    console.log('consoleMsg')
                    console.log(message);
                });

                socket.on('streamInit', function(stream){
                    console.log('streamInit')
                    userId = stream.userId;
                    signals.push(stream.data);
                    initUserMedia(signalsHandle);
                });
                socket.on('streamClose', function(){
                    console.log('streamClose')
                    try {
                        closeConnection(true);
                    } catch (e) {
                        log("on streamClose\n" + e.name + ": " + e.message);
                    }
                });

                socket.on('online', function(users){
                    $('button[data-userid]').removeClass('btn-success btn-warning').addClass('btn-secondary');
                    let guests = 0;
                    let userIsset = false;
                    for (let id in users) {
                        if (id == userId) {
                            userIsset = true;
                        }
                        let btn = $('button[data-userid="' + id + '"]');
                        if (btn.length) {
                            if (users[id] > 1) {
                                btn.removeClass('btn-secondary btn-warning').addClass('btn-success');
                            } else {
                                btn.removeClass('btn-secondary btn-success').addClass('btn-warning');
                            }
                        } else {
                            guestsCounter++;
                            $('.user-list').append("<button data-userid=\"" +
                                id + "\" class=\"mb-1 btn btn-block btn-action btn-"
                                + (users[id] > 1 ? 'success' : 'warning') + " guest\" type=\"button\">Guest " +
                                guestsCounter + "</button>");
                            guests++;
                        }
                        if (guests !== $('button[data-userid].guest').length) {
                            $('button[data-userid].guest').each(function(key, val){
                                let id = $(val).data('userid');
                                if (users[id] == null) {
                                    $(val).remove();
                                }
                            });
                        }
                    }
                    if (!userIsset && userId != null) {
                        closeConnection();
                    }

                    $('button[data-userid]').unbind('click');
                    $('button[data-userid].btn-success').click(callTo);
                });
                socket.on('disconnect', function(){
                    try {
                        $('button[data-userid]').removeClass('btn-success btn-warning').addClass('btn-secondary');
                        $('button[data-userid].guest').remove();
                        closeConnection();
                    } catch (e) {
                        log("Disconnected from socket.io.\n" + e.name + ": " + e.message);
                    }
                })

            });

            function callTo(){
                console.log('callTo')
                userId = $(this).data('userid');
                initUserMedia(function(){
                    start(true);
                })
            }

            function signalsHandle() {
                console.log('signalsHandle')
                if (localStream != null && !signalsHandleRun) {
                    signalsHandleRun = true;
                    if (!peerConnection) start(false);
                    while (signals.length > 0) {
                        let signal = signals.shift();
                        if (signal.sdp) {
                            peerConnection.setRemoteDescription(new window.RTCSessionDescription(signal.sdp)).then(function(){
                                // Only create answers in response to offers
                                if (signal.sdp.type === 'offer') {
                                    peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
                                }
                            }).catch(errorHandler);
                        } else if (signal.ice) {
                            peerConnection.addIceCandidate(new window.RTCIceCandidate(signal.ice)).catch(errorHandler);
                        }
                        if (!selfBusy) {
                            selfBusy = true;
                            socket.emit('setStatus', userStatus.busy);
                        }
                    }
                    signalsHandleRun = false;
                }
            }

            function initUserMedia(callback) {
                console.log('initUserMedia')
                $('#video-init').attr('disabled', 'disabled');
                $('#video-close').removeAttr('disabled');
                call = true;
                if (localStream == null && !localStreamInit) {
                    localStreamInit = true;
                    if (navigator.mediaDevices.getUserMedia) {
                        try {
                            navigator.mediaDevices.getUserMedia(mediaConstraints).then(function (stream) {
                                localStream = stream;
                                try {
                                    localVideo.srcObject = stream;
                                } catch (e) {
                                    localVideo.src = window.URL.createObjectURL(stream);
                                }
                                signalsHandle();
                                if (typeof callback === 'function') {
                                    callback();
                                }
                            }).catch(errorHandler);
                        } catch (e) {
                            log("setLocalStream\n" + e.name + ": " + e.message);
                        }
                    } else {
                        alert('Your browser does not support getUserMedia API');
                    }
                } else {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            }

            function start(isCaller) {
                console.log('start')
                try {
                    peerConnection                = new window.RTCPeerConnection(peerConnectionConfig);
                    peerConnection.onicecandidate = gotIceCandidate;
                    peerConnection.ontrack        = gotRemoteStream;
                    peerConnection.addStream(localStream);
                    if (isCaller) {
                        peerConnection.createOffer().then(createdDescription).catch(errorHandler);
                    }
                } catch (e) {
                    log("start\n" + e.name + ": " + e.message);
                }
            }

            function gotIceCandidate(event) {
                console.log('gotIceCandidate')
                if (event.candidate != null) {
                    socket.emit('streamInit', {
                        userId: userId,
                        data: {'ice': event.candidate},
                    });
                }
            }

            function createdDescription(description) {
                console.log('createdDescription')
                peerConnection.setLocalDescription(description).then(function () {
                    socket.emit('streamInit', {
                        userId: userId,
                        data: {'sdp': peerConnection.localDescription},
                    });
                }).catch(errorHandler);
            }

            function gotRemoteStream(event) {
                console.log('gotRemoteStream')
                try {
                    remoteVideo.srcObject = event.streams[0];
                } catch (e) {
                    remoteVideo.src = window.URL.createObjectURL(event.streams[0]);
                }
            }

            function errorHandler(error) {
                console.log(error);
            }


            function closeConnection(remoteClose) {
                console.log('closeConnection')
                $('#video-init').removeAttr('disabled');
                $('#log').html('');
                if (call) {
                    call = false;
                    $('#video-close').attr('disabled', 'disabled');
                    if (remoteClose !== true) {
                        socket.emit('streamClose', {});
                    }
                    userId = null;
                    selfBusy = false;

                    localVideo.srcObject = null;
                    $(localVideo).removeAttr('src');
                    remoteVideo.srcObject = null;
                    $(remoteVideo).removeAttr('src');
                    if (localStream) {
                        if (localStream.active) {
                            localStream.getTracks().forEach(function (track) {track.stop();});
                        }
                        localStream = null;
                    }
                    localStreamInit = false;
                    if (peerConnection != null) {
                        peerConnection.close();
                        peerConnection = null;
                    }
                }
            }

            $('#video-close').click(function() {
                closeConnection();
            });
        });
    }
};