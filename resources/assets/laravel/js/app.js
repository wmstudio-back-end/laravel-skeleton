import Pages from './pages';

let App = {
    Pages: Pages,
};

import {init} from "./globals";
init(App);