## INSTALLATION

Clone git repository

```bash
$ git clone https://IvIefisTofel@bitbucket.org/IvIefisTofel/laravel-skeleton.git dir-name
$ cd dir-name
```

Install composer dependencies

```bash
$ composer install
```

Copy `.env` file and generate application key

```bash
$ php -r "file_exists('.env') || copy('.env.example', '.env');"
$ php artisan key:generate
```

Configure database settings in your `.env` file

```bash
$ php artisan migrate
```

### Assets

For automatically generation javascript, css and sass you'll need to install npm dependencies

For installation dev-dependencies, `libpng-dev` is required

```bash
// if `libpng-dev` is not installed
$ sudo apt install libpng-dev
// then
$ npm install
```

To display notifications, you'll need `libnotify-bin`

```bash
$ sudo apt install libnotify-bin
```

Settings file for access generator is `./webpack.mix.js`

By default to generate assets for development build use

```bash
$ npm run dev
or
$ npm run watch
```

And for production build use

```bash
$ npm run-script prod
```